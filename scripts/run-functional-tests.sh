#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

"${SCRIPTPATH}/generate-config.py"

TESTS=()

# PHASE 2 v2
TESTS+=('accounts_test-plan_v2-4' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_accounts-config.json)
TESTS+=('credit-cards_test-plan_v2-1' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_credit_cards-config.json)
TESTS+=('invoice-financings_test-plan_v2-3' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_invoice_financings-config.json)
TESTS+=('customer-business_test-plan_v2-2' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_customer_business-config.json)
TESTS+=('customer-personal_test-plan_v2-2' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_customer_personal-config.json)
TESTS+=('financings_test-plan_v2-3' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_financings-config.json)
TESTS+=('loans_test-plan_v2-4' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_loans-config.json)
TESTS+=('unarranged-accounts-overdraft_test-plan_v2-3' ./scripts/configs/phase2/v2/generated/generated-phase2_v2_unarranged_accounts_overdraft-config.json)

# PHASE 2 v3
TESTS+=('consents_test-plan_v3-2' ./scripts/configs/phase2/v3/generated/generated-phase2_v3_consents-config.json)
TESTS+=('resources_test-plan_v3' ./scripts/configs/phase2/v3/generated/generated-phase2_v3_resources-config.json)

# PHASE 3 v4
TESTS+=('payments_test-plan_v4' ./scripts/configs/phase3/v4/generated/generated-phase3_v4_payments-config.json)

# PHASE 3 automatic-payments v2
TESTS+=('automatic-payments_test-plan_v2' ./scripts/configs/phase3/automatic-payments/v2/payments/generated/generated-phase3_v2_automatic_payments-config.json)
TESTS+=('automatic-pix-payments_test-plan_v2' ./scripts/configs/phase3/automatic-payments/v2/pix-payments/generated/generated-phase3_v2_automatic_pix_payments-config.json)


# PHASE 3 no-redirect-payments v1
TESTS+=('no-redirect-payments_test-plan_v1' ./scripts/configs/phase3/no-redirect-payments/v1/generated/generated-phase3_v1_no_redirect_payments-config.json)

# PHASE 3 no-redirect-payments v2
TESTS+=('no-redirect-payments_test-plan_v2' ./scripts/configs/phase3/no-redirect-payments/v2/generated/generated-phase3_v2_no_redirect_payments-config.json)

# PHASE 4b v1

TESTS+=('bank-fixed-incomes_test-plan_v1' ./scripts/configs/phase4b/v1/generated/generated-phase4b_v1_bank_fixed_incomes-config.json)
TESTS+=('credit-fixed-incomes_test-plan_v1' ./scripts/configs/phase4b/v1/generated/generated-phase4b_v1_credit_fixed_incomes-config.json)
TESTS+=('exchanges_test-plan' ./scripts/configs/phase4b/v1/generated/generated-phase4b_v1_exchanges-config.json)
TESTS+=('funds_test-plan' ./scripts/configs/phase4b/v1/generated/generated-phase4b_v1_funds-config.json)
TESTS+=('treasure-titles_test-plan' ./scripts/configs/phase4b/v1/generated/generated-phase4b_v1_treasure_titles-config.json)
TESTS+=('variable-incomes_test-plan' ./scripts/configs/phase4b/v1/generated/generated-phase4b_v1_variable_incomes-config.json)



TESTS+=(--expected-skips-file ./scripts/configs/expected-skips.json)
TESTS+=(--expected-failures-file ./scripts/configs/ignored-failures.json)

printf "%s\0" "${TESTS[@]}" | xargs -0 ./scripts/run-test-plan.py
