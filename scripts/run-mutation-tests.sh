#!/bin/bash

# Get list of changed java files

TARGET_CLASSES=(
    "net.openid.conformance.openbanking_brasil.testmodules.support.*"
)

EXCLUDED_CLASSES=(
    "net.openid.conformance.openbanking_brasil.testmodules.support.sequences.*"
    "net.openid.conformance.openbanking_brasil.testmodules.support.enums.*"
    "net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.*"
)

if [[ -z "$DIFF_PARAMS" ]]; then
    DIFF_PARAMS=origin/master...
fi

echo $DIFF_PARAMS
DIFF_FILES=$(git diff --name-only --diff-filter=AMR "$DIFF_PARAMS" | grep  ".*java$")

TEST_CLASSES=""
echo "Running pitest against these classes:"
for FILE in $DIFF_FILES ; do
    # Convert the file path to a class path
    CLASS_PATH=${FILE//\//.}
    CLASS_PATH=${CLASS_PATH//src.main.java./}
    CLASS_PATH=${CLASS_PATH/.java/}

    SKIP_CLASS=false
    for PATTERN in "${EXCLUDED_CLASSES[@]}"; do
            if [[ $CLASS_PATH == $PATTERN ]]; then
                SKIP_CLASS=true
                break
            fi
    done

    if [ "$SKIP_CLASS" = true ]; then
        continue
    fi

    for PATTERN in "${TARGET_CLASSES[@]}"; do
            if [[ $CLASS_PATH == $PATTERN ]]; then
                echo "$CLASS_PATH"
                TEST_CLASSES+="$CLASS_PATH,"
                break
            fi
    done

done

# if classes are present we execute pitest
if [[ -n "$TEST_CLASSES" ]]; then
    # remove last comma
    TEST_CLASSES=${TEST_CLASSES%?}

    EXIT_CODE=0
    if ! mvn pitest:mutationCoverage -DtargetClasses="$TEST_CLASSES" ; then
        EXIT_CODE=1
    fi

    echo "Report is available at https://raidiam-conformance.gitlab.io/-/open-finance/certification/-/jobs/${CI_JOB_ID}/artifacts/target/pit-reports/index.html"
    exit $EXIT_CODE

else
     echo "No test classes found, skipping pitest run."
fi

