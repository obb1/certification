#!/bin/bash
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

"${SCRIPTPATH}/generate-config.py"

function usage() {
    echo "Usage: $0 -t <testplan_name> -c <config_file_path> [-o <overrides_file_path>]"
    exit 1
}

while getopts ":t:c:o:" opt; do
    case $opt in
        t) TEST_PLAN="$OPTARG";;
        c) CONFIG_FILE="$OPTARG";;
        o) OVERRIDES_FILE="$OPTARG";;
        \?) echo "Invalid option: -$OPTARG" >&2; usage;;
        :) echo "Option -$OPTARG requires an argument." >&2; usage;;
    esac
done

if [ -z "$TEST_PLAN" ] || [ -z "$CONFIG_FILE" ]; then
    usage
fi

if [ -z "$OVERRIDES_FILE" ]; then
    OVERRIDES_FILE="./scripts/configs/overrides.json"
fi

cp "$OVERRIDES_FILE" "./scripts/configs/overrides.json"

TESTS=()

TESTS+=("$TEST_PLAN" "$CONFIG_FILE")

TESTS+=(--expected-failures-file ./scripts/configs/ignored-failures.json)
TESTS+=(--expected-skips-file ./scripts/configs/expected-skips.json)


printf "%s\0" "${TESTS[@]}" | xargs -0 ./scripts/run-test-plan.py --verbose
