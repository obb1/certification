#!/usr/bin/env python3

import sys, os
import jinja2
# get filename from first arg
filename = sys.argv[1]

cs_env = os.environ.get('CS_ENV', 'dev')
cs_img = os.environ.get('CS_IMG', 'dev')

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
template = templateEnv.get_template(filename)
outputText = template.render({
    "env": cs_env,
    "image": cs_img,
    })

print(outputText)
