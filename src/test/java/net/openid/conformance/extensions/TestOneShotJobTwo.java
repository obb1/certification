package net.openid.conformance.extensions;

import net.openid.conformance.extensions.oneshot.OneShot;
import net.openid.conformance.extensions.oneshot.OneShotJob;

import java.util.concurrent.CountDownLatch;

public class TestOneShotJobTwo implements OneShotJob {

	private CountDownLatch latch;

	public TestOneShotJobTwo(CountDownLatch latch) {
		this.latch = latch;
	}

	@Override
	public void execute() {
		latch.countDown();
	}
}
