package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.PlainJWT;
import net.minidev.json.JSONObject;
import net.openid.conformance.security.AuthenticationFacade;
import org.junit.Test;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.io.IOException;

import static net.openid.conformance.extensions.yacs.ServletInputStreamFactory.withBody;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrgOwnershipVerifierTests {

	@Test
	public void serverConfigNotProvided() throws IOException {

		AuthenticationFacade facade = mock(AuthenticationFacade.class);
		ParticipantsService participantsService = mock(ParticipantsService.class);

		EnhancedVerification verifier = new OrgOwnershipEnhancedVerification(facade, participantsService, false);

		JsonObject config = new JsonObject();

		VerificationResult result = verifier.allowed(withBody(config));

		assertEquals(result.getDecision(), VerificationResult.Decision.SOFT_FAIL);

	}

	@Test
	public void authorisationServerIdNotProvided() throws IOException {

		AuthenticationFacade facade = mock(AuthenticationFacade.class);
		when(facade.getContextAuthentication()).thenReturn(idToken());
		ParticipantsService participantsService = mock(ParticipantsService.class);
		when(participantsService.orgsFor(any())).thenReturn(AliasHandlerTests.theOrgs());

		EnhancedVerification verifier = new OrgOwnershipEnhancedVerification(facade, participantsService, false);

		String authorisationServerId = "";
		JsonObject config = new JsonObject();
		JsonObject serverConfig = new JsonObject();
		serverConfig.addProperty("authorisationServerId", authorisationServerId);
		config.add("server", serverConfig);

		VerificationResult result = verifier.allowed(withBody(config));

		assertEquals(result.getDecision(), VerificationResult.Decision.SOFT_FAIL);

	}

	@Test
	public void unknownAuthorisationServerIdProvidedOnConfig() throws IOException {

		AuthenticationFacade facade = mock(AuthenticationFacade.class);
		when(facade.getContextAuthentication()).thenReturn(idToken());
		ParticipantsService participantsService = mock(ParticipantsService.class);
		when(participantsService.orgsFor(any())).thenReturn(AliasHandlerTests.theOrgs());

		EnhancedVerification verifier = new OrgOwnershipEnhancedVerification(facade, participantsService, false);

		String authorisationServerId = "abcdefg1234567";
		JsonObject config = new JsonObject();
		JsonObject serverConfig = new JsonObject();
		serverConfig.addProperty("authorisationServerId", authorisationServerId);
		config.add("server", serverConfig);

		VerificationResult result = verifier.allowed(withBody(config));

		assertEquals(result.getDecision(), VerificationResult.Decision.SOFT_FAIL);

	}

	@Test
	public void correctAuthorisationServerIdProvidedOnConfig() throws IOException {

		AuthenticationFacade facade = mock(AuthenticationFacade.class);
		when(facade.getContextAuthentication()).thenReturn(idToken());
		ParticipantsService participantsService = mock(ParticipantsService.class);
		when(participantsService.orgsFor(any())).thenReturn(AliasHandlerTests.theOrgs());

		EnhancedVerification verifier = new OrgOwnershipEnhancedVerification(facade, participantsService, false);

		String authorisationServerId = "896e82c7-fcfd-4bf3-ad11-e45e36009921";
		JsonObject config = new JsonObject();
		JsonObject serverConfig = new JsonObject();
		serverConfig.addProperty("authorisationServerId", authorisationServerId);
		config.add("server", serverConfig);

		VerificationResult result = verifier.allowed(withBody(config));

		assertEquals(result.getDecision(), VerificationResult.Decision.SOFT_PASS);

	}

	Authentication idToken() {

		JSONObject tpf = new JSONObject();
		JSONObject oad = new JSONObject();
		oad.appendField("f8993e33_8f0c_5530_9207_2d9cf8454ee3", new JSONObject());
		tpf.appendField("org_access_details", oad);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.claim("trust_framework_profile", tpf)
			.build();
		return new OIDCAuthenticationToken("sub",
			"iss", null, null,
			new PlainJWT(claims), null, null);
	}

}
