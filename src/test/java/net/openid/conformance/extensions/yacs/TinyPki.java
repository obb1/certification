package net.openid.conformance.extensions.yacs;

import com.google.common.collect.Lists;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.SignedJWT;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;

/**
 * A very small library for generating certs for tests. It isn't in any way a viable PKI
 */
public class TinyPki {

	private final Provider provider;
	private final KeyPairGenerator keyPairGenerator;
	private Bundle issuer;

	public TinyPki(String issuerName, Provider provider) throws Exception {
		this.provider = provider;
		keyPairGenerator = KeyPairGenerator.getInstance("RSA", new BouncyCastleProvider());
		keyPairGenerator.initialize(2048);
		issuer = createIssuer(issuerName);
	}

	public Bundle getIssuer() {
		return issuer;
	}

	/*
	  This generates a cert with some subjectDN matter necessary for the PCM event emitter tests.
	  It does not generate a cert which is fully compliant with the FAPI cert profiles.
	 */
	public Bundle issueCert(String commonName, String orgId, String ssId) throws OperatorCreationException, NoSuchAlgorithmException, CertIOException, CertificateException {
		Date startDate = Date
			.from(Instant.now());

		Date endDate = Date
			.from(Instant.now().plus(1, ChronoUnit.DAYS));

		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		BigInteger serialNum = new BigInteger(Long.toString(new SecureRandom().nextLong()));

		X500Name issuerSubject = new JcaX509CertificateHolder(issuer.getCertificate()).getSubject();

		StringBuilder sb = new StringBuilder().append("CN=").append(commonName);

		if(orgId != null) {
			sb.append(", OU=").append(orgId);
		}
		if(ssId != null) {
			sb.append(", UID=").append(ssId);
		}
		X500Name subject = new X500Name(sb.toString());
		ContentSigner rootCertContentSigner = new JcaContentSignerBuilder("SHA256withRSA")
			.setProvider(provider).build(this.issuer.getKeyPair().getPrivate());
		X509v3CertificateBuilder certificateBuilder =
			new JcaX509v3CertificateBuilder(issuerSubject, serialNum,
				startDate, endDate, subject, keyPair.getPublic());

		JcaX509ExtensionUtils rootCertExtUtils = new JcaX509ExtensionUtils();
		certificateBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(true));
		certificateBuilder.addExtension(Extension.subjectKeyIdentifier, false,
			rootCertExtUtils.createSubjectKeyIdentifier(keyPair.getPublic()));



		X509CertificateHolder rootCertHolder = certificateBuilder.build(rootCertContentSigner);
		X509Certificate cert = new JcaX509CertificateConverter().setProvider(provider).getCertificate(rootCertHolder);
		return new Bundle(this.issuer, cert, keyPair);
	}

	public TrustManager[] trustStore() {
		return issuer.trustStore();
	}


	private Bundle createIssuer(String issuerName) throws Exception {
		Date startDate = Date
			.from(Instant.now());

		Date endDate = Date
			.from(Instant.now().plus(1, ChronoUnit.DAYS));

		KeyPair rootKeyPair = keyPairGenerator.generateKeyPair();
		BigInteger rootSerialNum = new BigInteger(Long.toString(new SecureRandom().nextLong()));

		X500Name rootCertIssuer = new X500Name("CN=" + issuerName);
		X500Name rootCertSubject = rootCertIssuer;
		ContentSigner rootCertContentSigner = new JcaContentSignerBuilder("SHA256withRSA")
			.setProvider(provider).build(rootKeyPair.getPrivate());
		X509v3CertificateBuilder rootCertBuilder =
			new JcaX509v3CertificateBuilder(rootCertIssuer, rootSerialNum,
				startDate, endDate, rootCertSubject, rootKeyPair.getPublic());

		JcaX509ExtensionUtils rootCertExtUtils = new JcaX509ExtensionUtils();
		rootCertBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(true));
		rootCertBuilder.addExtension(Extension.subjectKeyIdentifier, false,
			rootCertExtUtils.createSubjectKeyIdentifier(rootKeyPair.getPublic()));

		X509CertificateHolder rootCertHolder = rootCertBuilder.build(rootCertContentSigner);
		X509Certificate rootCert = new JcaX509CertificateConverter().setProvider(provider).getCertificate(rootCertHolder);
		return new Bundle(null, rootCert, rootKeyPair);
	}

	public static class Bundle {

		private Bundle issuer;
		private X509Certificate certificate;
		private KeyPair keyPair;

		public Bundle(Bundle issuer, X509Certificate certificate, KeyPair keyPair) {
			this.issuer = issuer;
			this.certificate = certificate;
			this.keyPair = keyPair;
			if(issuer==null) {
				this.issuer = this;
			}
		}

		public KeyPair getKeyPair() {
			return keyPair;
		}

		public X509Certificate getCertificate() {
			return certificate;
		}

		public Bundle getIssuer() {
			return issuer;
		}

		public KeyManager[] keyManagers() throws Exception {
			ArrayList<X509Certificate> chain = Lists.newArrayList(certificate);
			if (issuer != null) {
			chain.add(issuer.getCertificate());
			}

			KeyStore keystore = KeyStore.getInstance("JKS");
			keystore.load(null);
			keystore.setCertificateEntry("cert-alias", certificate);
			keystore.setKeyEntry("key-alias", keyPair.getPrivate(), "changeit".toCharArray(), chain.toArray(new Certificate[chain.size()]));

			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keystore, "changeit".toCharArray());

			return keyManagerFactory.getKeyManagers();
		}

		public TrustManager[] trustStore() {
			X509TrustManager x509TrustManager =	new X509TrustManager() {

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[] {issuer.getCertificate()};
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					System.out.println("Server check in " + certificate.getSubjectX500Principal());
				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					System.out.println("Client check in " + certificate.getSubjectX500Principal());
				}
			};
			return new TrustManager[] {x509TrustManager};
		}

		public void sign(SignedJWT jwt) throws JOSEException {
			JWSSigner signer = new RSASSASigner(keyPair.getPrivate());
			jwt.sign(signer);
		}
	}

}
