package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.PlainJWT;
import net.minidev.json.JSONObject;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.io.IOException;

import static net.openid.conformance.extensions.yacs.ServletInputStreamFactory.withBody;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CertificationManagerVerificationTests {

    @Test
    public void hardPassIfIdTokenHasCertManagerClaim() throws IOException {

        AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
        when(authenticationFacade.getContextAuthentication()).thenReturn(idToken(true, true));

        EnhancedVerification ev = new CertificationManagerVerification(authenticationFacade, false);
        JsonObject config = new JsonObject();
        JsonObject resource = new JsonObjectBuilder().addField("brazilCpf", "notarealcpf").build();
        config.add("resource", resource);
        VerificationResult allowed = ev.allowed(withBody(config));

        assertEquals(VerificationResult.Decision.HARD_PASS, allowed.getDecision());

    }

	@Test
	public void noCommentIfIdTokenHasNoCertManagerClaim() throws IOException {

		AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
		when(authenticationFacade.getContextAuthentication()).thenReturn(idToken(false, false));

		EnhancedVerification ev = new CertificationManagerVerification(authenticationFacade, false);
		JsonObject config = new JsonObject();
		JsonObject resource = new JsonObjectBuilder().addField("brazilCpf", "notarealcpf").build();
		config.add("resource", resource);
		VerificationResult allowed = ev.allowed(withBody(config));

		assertEquals(VerificationResult.Decision.NO_COMMENT, allowed.getDecision());

	}

	@Test
	public void noCommentIfIdTokenHasFalseCertManagerClaim() throws IOException {

		AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
		when(authenticationFacade.getContextAuthentication()).thenReturn(idToken(false, true));

		EnhancedVerification ev = new CertificationManagerVerification(authenticationFacade, false);
		JsonObject config = new JsonObject();
		JsonObject resource = new JsonObjectBuilder().addField("brazilCpf", "notarealcpf").build();
		config.add("resource", resource);
		VerificationResult allowed = ev.allowed(withBody(config));

		assertEquals(VerificationResult.Decision.NO_COMMENT, allowed.getDecision());

	}

	@Test
	public void noCommentIfIdTokenHasNoTrustFrameworkProfile() throws IOException {

		AuthenticationFacade authenticationFacade = mock(AuthenticationFacade.class);
		when(authenticationFacade.getContextAuthentication()).thenReturn(new OIDCAuthenticationToken("sub",
			"iss", null, null,
			new PlainJWT(new JWTClaimsSet.Builder().build()), null, null));

		EnhancedVerification ev = new CertificationManagerVerification(authenticationFacade, false);
		JsonObject config = new JsonObject();
		JsonObject resource = new JsonObjectBuilder().addField("brazilCpf", "notarealcpf").build();
		config.add("resource", resource);
		VerificationResult allowed = ev.allowed(withBody(config));

		assertEquals(VerificationResult.Decision.NO_COMMENT, allowed.getDecision());

	}



	Authentication idToken(boolean manager, boolean managerPresent) {

		JSONObject tpf = new JSONObject();
		if(managerPresent) {
			tpf.appendField("certification_manager", manager);
		}
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.claim("trust_framework_profile", tpf)
			.build();
		return new OIDCAuthenticationToken("sub",
			"iss", null, null,
			new PlainJWT(claims), null, null);
	}

}
