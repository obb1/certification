package net.openid.conformance.extensions.yacs;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EndpointDecorationServiceTest {


	private static Stream<Arguments> decoratedEndpoints() {
		return Stream.of(
			Arguments.of("/open-banking/enrollments/v2/enrollments/urn:bancoex:C1DD3312", "/open-banking/enrollments/v2/enrollments/{enrollmentId}"),
			Arguments.of("/open-banking/enrollments/v2/enrollments/urn:bancoex:C1DD3312/fido-registration-options", "/open-banking/enrollments/v2/enrollments/{enrollmentId}/fido-registration-options"),
			Arguments.of("/open-banking/credit-cards-accounts/v2/accounts/123abc/bills/456def/transactions", "/open-banking/credit-cards-accounts/v2/accounts/{creditCardAccountId}/bills/{billId}/transactions"),
			Arguments.of("/bank-bank/open-banking/enrollments/v2/enrollments/urn:bancoex:C1DD3312","/open-banking/enrollments/v2/enrollments/{enrollmentId}")
		);
	}


	@ParameterizedTest
	@MethodSource("decoratedEndpoints")
	void decorateEndpoint(String undecoratedEndpoint, String expectedDecoratedEndpoint) {
		EndpointDecorationService service = new EndpointDecorationService();
		String decorateEndpoint = service.decorateEndpoint(undecoratedEndpoint);
		assertEquals(expectedDecoratedEndpoint, decorateEndpoint);
	}
}
