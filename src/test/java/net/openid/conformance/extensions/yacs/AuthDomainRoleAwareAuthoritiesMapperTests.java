package net.openid.conformance.extensions.yacs;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.Test;
import org.mitre.openid.connect.client.OIDCAuthoritiesMapper;
import org.mitre.openid.connect.client.SubjectIssuerGrantedAuthority;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AuthDomainRoleAwareAuthoritiesMapperTests {

	@Test
	public void beingNeitherAdminNorPfvpcNoAccess() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("iss")
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(1, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream().findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("iss", granted.getIssuer());

	}

	@Test
	public void beingIssuedByAdminIssuerNotEnough() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("adminiss")
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(1, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream().findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("adminiss", granted.getIssuer());

	}

	@Test
	public void beingIssuedByAdminIssuerInCorrectGroup() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("adminiss")
			.claim("groups", List.of("admingroup"))
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(2, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream()
			.filter(g -> g instanceof SubjectIssuerGrantedAuthority)
			.findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("adminiss", granted.getIssuer());

		authorities.stream()
			.filter(a -> a.getAuthority().equals("ROLE_ADMIN"))
			.findAny().orElseThrow(() -> new AssertionError("Should have admin role"));

	}

	@Test
	public void beingPfvpcGetsMeIn() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("iss")
			.claim("trust_framework_profile",
				Map.of("org_access_details", Map.of("org1",
					Map.of("domain_role_details",
						Map.of("0", Map.of("status", "Active", "contact_role", "PFVPC")
						)))))
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(3, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream()
			.filter(g -> g instanceof SubjectIssuerGrantedAuthority)
			.findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("iss", granted.getIssuer());

		authorities.stream()
			.filter(a -> a.getAuthority().equals("ROLE_USER"))
			.findAny().orElseThrow(() -> new AssertionError("Should have user role"));

	}

	@Test
	public void myPfvpcRoleMustBeActive() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("iss")
			.claim("trust_framework_profile",
				Map.of("org_access_details", Map.of("org1",
					Map.of("domain_role_details",
						Map.of("0", Map.of("status", "Inactive", "contact_role", "PFVPC")
						)))))
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(1, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream()
			.filter(g -> g instanceof SubjectIssuerGrantedAuthority)
			.findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("iss", granted.getIssuer());

	}

	@Test
	public void certificationManagerOverrulesAll() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("iss")
			.claim("trust_framework_profile",
				Map.of("certification_manager", true))
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(4, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream()
			.filter(g -> g instanceof SubjectIssuerGrantedAuthority)
			.findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("iss", granted.getIssuer());

		authorities.stream()
			.filter(a -> a.getAuthority().equals("ROLE_USER"))
			.findAny().orElseThrow(() -> new AssertionError("Should have user role"));

	}

	@Test
	public void certificationManagerMustBeTrue() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("iss")
			.claim("trust_framework_profile",
				Map.of("certification_manager", false))
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(1, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream()
			.filter(g -> g instanceof SubjectIssuerGrantedAuthority)
			.findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("iss", granted.getIssuer());

	}

	@Test
	public void noOtherRoleMatters() {

		OIDCAuthoritiesMapper mapper = new AuthDomainRoleAwareOBBAuthoritiesMapper("admingroup", "adminiss", false);
		JWTClaimsSet claims = new JWTClaimsSet.Builder()
			.subject("sub")
			.issuer("iss")
			.claim("trust_framework_profile",
				Map.of("org_access_details", Map.of("org1",
					Map.of("domain_role_details",
						Map.of("0", Map.of("status", "Active", "contact_role", "PTC")
						)))))
			.build();
		SignedJWT jwt = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.PS256).build(), claims);
		Collection<? extends GrantedAuthority> authorities = mapper.mapAuthorities(jwt, null);

		assertEquals(1, authorities.size());
		SubjectIssuerGrantedAuthority granted = (SubjectIssuerGrantedAuthority) authorities.stream()
			.filter(g -> g instanceof SubjectIssuerGrantedAuthority)
			.findFirst().orElseThrow(() -> new AssertionError("No grants at all"));

		assertNotNull(granted);
		assertEquals("sub", granted.getSubject());
		assertEquals("iss", granted.getIssuer());

	}

}
