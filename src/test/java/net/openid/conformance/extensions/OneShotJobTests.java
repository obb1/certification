package net.openid.conformance.extensions;

import net.openid.conformance.extensions.oneshot.OneShotJob;
import net.openid.conformance.extensions.oneshot.OneShotRunner;
import net.openid.conformance.extensions.oneshot.OneShotService;
import org.junit.Test;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import java.util.Set;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OneShotJobTests {

	@Test
	public void allJobsShouldRunOnce() throws InterruptedException {

		CountDownLatch latch = new CountDownLatch(2);
		OneShotService service = mock(OneShotService.class);

		when(service.hasExecuted(anyString())).thenReturn(false);

		Set<OneShotJob> jobs = Set.of(new TestOneShotJobOne(latch), new TestOneShotJobTwo(latch));

		OneShotRunner runner = new OneShotRunner(new SyncTaskExecutor(), service, jobs);
		runner.runOutstandingJobs();

		boolean countedDown = latch.await(3000, java.util.concurrent.TimeUnit.MILLISECONDS);
		long count = latch.getCount();

		assertTrue(countedDown);
		assertEquals(0, count);

	}

	@Test
	public void alreadyRunJobsIgnored() throws InterruptedException {

		CountDownLatch latch = new CountDownLatch(2);
		OneShotService service = mock(OneShotService.class);

		when(service.hasExecuted(TestOneShotJobOne.class.getName())).thenReturn(true);
		when(service.hasExecuted(TestOneShotJobTwo.class.getName())).thenReturn(false);

		Set<OneShotJob> jobs = Set.of(new TestOneShotJobOne(latch), new TestOneShotJobTwo(latch));

		OneShotRunner runner = new OneShotRunner(new SyncTaskExecutor(), service, jobs);
		runner.runOutstandingJobs();

		boolean countedDown = latch.await(500, java.util.concurrent.TimeUnit.MILLISECONDS);
		long count = latch.getCount();

		assertFalse(countedDown);
		assertEquals(1, count);
	}

}
