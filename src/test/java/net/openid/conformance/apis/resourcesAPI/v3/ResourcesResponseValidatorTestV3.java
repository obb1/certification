package net.openid.conformance.apis.resourcesAPI.v3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v3.ResourcesResponseValidatorV3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class ResourcesResponseValidatorTestV3 extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3.json")
	public void validateStructure() {
		run(new ResourcesResponseValidatorV3());
	}
}
