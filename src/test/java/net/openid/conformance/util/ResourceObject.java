package net.openid.conformance.util;

import com.google.gson.JsonObject;

public class ResourceObject {
	private final String key;
	private final String path;

	private String stringValue;

	private JsonObject objectValue;

	private final boolean isString;


	public ResourceObject(String key, String path, String stringValue) {
		this.key = key;
		this.path = path;
		this.stringValue = stringValue;
		this.isString = true;
	}

	public ResourceObject(String key, String path, JsonObject objectValue) {
		this.key = key;
		this.path = path;
		this.objectValue = objectValue;
		this.isString = false;
	}

	public String getKey() {
		return key;
	}

	public String getPath() {
		return path;
	}

	public String getStringValue() {
		return stringValue;
	}

	public JsonObject getObjectValue() {
		return objectValue;
	}

	public boolean isString() {
		return isString;
	}

}
