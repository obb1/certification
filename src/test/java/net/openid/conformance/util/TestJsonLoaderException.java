package net.openid.conformance.util;

public class TestJsonLoaderException extends RuntimeException {

	public TestJsonLoaderException(String message) {
		super(message);
	}
}
