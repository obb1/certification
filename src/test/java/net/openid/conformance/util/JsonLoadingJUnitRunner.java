package net.openid.conformance.util;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JsonLoadingJUnitRunner extends BlockJUnit4ClassRunner {

	public static final String WRAPPED = "__WRAPPED_JsonArray__";

	public JsonLoadingJUnitRunner(Class<?> testClass) throws InitializationError {
		super(testClass);
	}

	private ResourceObject createResourceObject(String key, String path, String resourcePath, boolean isString, String methodName) {
		key = key.equals("null") ? null : key;
		path = path.equals("null") ? null : path;
		if (isString) {
			String rawJson = getRawJsonFromResource(resourcePath, methodName);
			return new ResourceObject(key, path, rawJson);
		} else {
			JsonObject jsonObject = getJsonObjectFromResource(resourcePath, methodName);
			return new ResourceObject(key, path, jsonObject);
		}
	}

	@Override
	protected Statement methodInvoker(FrameworkMethod method, Object test) {

		UseResurce[] useResources = method.getMethod().getAnnotationsByType(UseResurce.class);

		if (useResources.length == 0) {
			useResources = super.getTestClass().getJavaClass().getAnnotationsByType(UseResurce.class);
		}

		if (useResources.length == 0) {
			return super.methodInvoker(method, test);
		}

		try {
			Field[] fields = FieldUtils.getAllFields(getTestClass().getJavaClass());
			Field jsonObjectWithoutKeyField = getJsonObjectWithoutKeyField(fields, method.getName());
			Field jsonObjectsToAddField = getJsonObjectsToAddField(fields, method.getName());


			List<ResourceObject> resourcesWithKey = Arrays.stream(useResources)
				.filter(a -> !a.key().equals("null"))
				.map(a -> createResourceObject(a.key(), a.path(), a.value(), a.isAsString(), method.getName()))
				.collect(Collectors.toList());

			setFieldWIthValue(method.getName(), test, jsonObjectsToAddField, resourcesWithKey);

			Optional<String> optionalResourceWithoutKey = Arrays.stream(useResources)
				.filter(a -> a.key().equals("null"))
				.map(UseResurce::value)
				.findFirst();

			if (optionalResourceWithoutKey.isPresent()) {
				JsonObject objectWithoutField = getJsonObjectFromResource(optionalResourceWithoutKey.get(), method.getName());
				setFieldWIthValue(method.getName(), test, jsonObjectWithoutKeyField, objectWithoutField);
			}

		} catch (TestJsonLoaderException e) {
			return new FailingStatement(e.getMessage());
		}

		Statement statement = super.methodInvoker(method, test);
		return withBefores(method, test, statement);
	}


	private JsonObject getJsonObjectFromResource(String resource, String methodName) throws TestJsonLoaderException {
		String rawJson = getRawJsonFromResource(resource, methodName);

		JsonObject jsonObject;
		if (new JsonParser().parse(rawJson).isJsonArray()) {
			jsonObject = convertArrayToJsonObject(rawJson);
		} else {
			jsonObject = new JsonParser().parse(rawJson).getAsJsonObject();
		}

		if (jsonObject == null) {
			throw new TestJsonLoaderException(String.format("Unable to load JSON document %s in test %s", resource, methodName));
		}

		return jsonObject;
	}

	private String getRawJsonFromResource(String resource, String methodName) throws TestJsonLoaderException {
		String resourceOverride = System.getProperty("resource.override");
		String rawJson;
		if (resourceOverride != null) {
			try {
				rawJson = Files.readString(Path.of(resourceOverride), Charset.defaultCharset());
			} catch (IOException e) {
				throw new TestJsonLoaderException(String.format("Unable to override JSON document with %s in test %s", resourceOverride, methodName));
			}

		} else {
			try {
				rawJson = IOUtils.resourceToString(resource, Charset.defaultCharset(), getClass().getClassLoader());
			} catch (IOException e) {
				throw new TestJsonLoaderException(String.format("Unable to load JSON document %s in test %s", resource, methodName));
			}
		}
		return rawJson;
	}

	private Field getJsonObjectWithoutKeyField(Field[] fields, String methodName) {
		Field field = Arrays.stream(fields)
			.filter(f -> f.getType().isAssignableFrom(JsonObject.class))
			.filter(f -> f.getName().equals("jsonObject"))
			.findFirst()
			.orElseThrow(() -> new TestJsonLoaderException(String.format("No suitable JsonObject field present on %s", methodName)));
		field.setAccessible(true);
		return field;
	}

	private Field getJsonObjectsToAddField(Field[] fields, String methodName) {
		Field field = Arrays.stream(fields)
			.filter(f -> f.getType().isAssignableFrom(List.class))
			.filter(f -> f.getName().equals("objectsToAdd"))
			.findFirst()
			.orElseThrow(() -> new TestJsonLoaderException(String.format("No suitable Map field present on %s", methodName)));
		field.setAccessible(true);
		return field;
	}


	private void setFieldWIthValue(String methodName, Object targetObject, Field field, Object value) {
		try {
			field.set(targetObject, value);
		} catch (IllegalAccessException e) {
			throw new TestJsonLoaderException(String.format("Unable to set %s field %s in test %s", field.getType(), field.getName(), methodName));
		}
	}

	private JsonObject convertArrayToJsonObject(String rawJson) {
		JsonObject result = new JsonObject();
		result.add(WRAPPED, new JsonParser().parse(rawJson));
		return result;
	}

	private static class FailingStatement extends Statement {

		private final String message;

		FailingStatement(String message, Object... args) {
			this.message = String.format(message, args);
		}

		@Override
		public void evaluate() throws Throwable {
			throw new AssertionError(message);
		}
	}

}
