package net.openid.conformance.openbanking_brasil.opendata.oasValidators.acquiringServices;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetAcquiringServicesPersonalOASValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/acquiringServices/PersonalAcquiringServicesResponseV1.json")
	public void testHappyPath() {
		GetAcquiringServicesPersonalOASValidator cond = new GetAcquiringServicesPersonalOASValidator();
		run(cond);
	}
}
