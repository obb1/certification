package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class PostPaymentsConsentOASValidatorV4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(201);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4Ok.json")
	public void happyPath() {
		run(new PostPaymentsConsentOASValidatorV4());
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4ScheduleAndDate.json")
	public void unhappyPathDateAndSchedulePresent() {
		ConditionError e = runAndFail(new PostPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.date and payment.schedule cannot both be present; they are mutually exclusive."));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4NoProxy.json")
	public void unhappyPathProxy1() {
		ConditionError e = runAndFail(new PostPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.details.proxy is required when payment.details.localInstrument is [INIC, DICT, QRDN, QRES]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4ManuWithProxy.json")
	public void unhappyPathProxy2() {
		ConditionError e = runAndFail(new PostPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.details.proxy cannot be present when payment.details.localInstrument has the value MANU"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4CreditorNoIssuer.json")
	public void unhappyPathCreditorNoIssuer() {
		ConditionError e = runAndFail(new PostPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("payment.details.creditorAccount.issuer is required when payment.details.creditorAccount.accountType is [CACC, SVGS]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentV4DebtorNoIssuer.json")
	public void unhappyPathDebtorNoIssuer() {
		ConditionError e = runAndFail(new PostPaymentsConsentOASValidatorV4());
		assertThat(e.getMessage(), containsString("debtorAccount.issuer is required when debtorAccount.accountType is [CACC, SVGS]"));
	}
}
