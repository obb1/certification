package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.AbstractEditRecurringPaymentsAutomaticConsentBody;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class AbstractEditRecurringPaymentsAutomaticConsentBodyTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String JSON_BASE_PATH = "jsonRequests/automaticPayments/consents/brazilPaymentConsent";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPaymentConsent";

	protected abstract AbstractEditRecurringPaymentsAutomaticConsentBody condition();

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithAutomaticRecurringConfigurationAllFields.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(condition());

		JsonObject automatic = environment.getElementFromObject(KEY,
				PATH + ".data.recurringConfiguration.automatic").getAsJsonObject();

		assertAutomaticObject(automatic);
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		environment.putObject(KEY, new JsonObject());
		generalUnhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		generalUnhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "MissingRecurringConfiguration.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingRecurringConfiguration() {
		generalUnhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithSweepingField.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingAutomatic() {
		generalUnhappyPathTest();
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}

	protected void generalUnhappyPathTest() {
		unhappyPathTest("Unable to find data.recurringConfiguration.automatic in consents payload");
	}

	protected abstract void assertAutomaticObject(JsonObject automatic);
}
