package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetUnarrangedAccountsOverdraftPaymentsV2N3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/payments.json")
	public void happyPath() {
		run(new GetUnarrangedAccountsOverdraftPaymentsV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/paymentsNoOverParcel.json")
	public void happyPathNoOverParcel() {
		run(new GetUnarrangedAccountsOverdraftPaymentsV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/paymentsNoChargeAdditionalInfo.json")
	public void unhappyPathNoChargeAdditionalInfo() {
		ConditionError e = runAndFail(new GetUnarrangedAccountsOverdraftPaymentsV2n3OASValidator());
		assertThat(e.getMessage(), containsString("chargeAdditionalInfo is required when chargeType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/paymentsNoInstalmentId.json")
	public void unhappyPathNoInstalmentId() {
		ConditionError e = runAndFail(new GetUnarrangedAccountsOverdraftPaymentsV2n3OASValidator());
		assertThat(e.getMessage(), containsString("instalmentId is required when isOverParcelPayment is false"));
	}


}
