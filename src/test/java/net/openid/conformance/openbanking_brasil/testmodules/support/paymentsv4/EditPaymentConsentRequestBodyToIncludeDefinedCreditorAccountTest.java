package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public class EditPaymentConsentRequestBodyToIncludeDefinedCreditorAccountTest extends AbstractJsonResponseConditionUnitTest {

	private static final String ISPB = "12345678";
	private static final String ISSUER = "1774";
	private static final String NUMBER = "1234567890";
	private static final String ACCOUNT_TYPE = "CACC";
	private static final String JSON_BASE_PATH = "jsonRequests/payments/consents/v4/postPaymentConsentRequest";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Before
	public void init() {
		JsonObject resource = new JsonObjectBuilder()
			.addField("creditorAccountIspb", ISPB)
			.addField("creditorAccountIssuer", ISSUER)
			.addField("creditorAccountNumber", NUMBER)
			.addField("creditorAccountAccountType", ACCOUNT_TYPE)
			.build();
		environment.putObject("resource", resource);
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "V4.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new EditPaymentConsentRequestBodyToIncludeDefinedCreditorAccount());

		JsonObject creditorAccount = environment
			.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.creditorAccount")
			.getAsJsonObject();

		assertEquals(ISPB, OIDFJSON.getString(creditorAccount.get("ispb")));
		assertEquals(ISSUER, OIDFJSON.getString(creditorAccount.get("issuer")));
		assertEquals(NUMBER, OIDFJSON.getString(creditorAccount.get("number")));
		assertEquals(ACCOUNT_TYPE, OIDFJSON.getString(creditorAccount.get("accountType")));
	}

	@Test
	public void unhappyPathTestMissingData() {
		JsonObject resource = environment.getObject("resource");
		resource.add("brazilPaymentConsent", new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutPaymentV4.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingPayment() {
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutPaymentDetailsV4.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDetails() {
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingResource() {
		assertThrowsExactly(AssertionError.class, () -> {
			run(new EditPaymentConsentRequestBodyToIncludeDefinedCreditorAccount());
		});
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new EditPaymentConsentRequestBodyToIncludeDefinedCreditorAccount());
		assertTrue(error.getMessage().contains("Could not find data.payment.details inside payment consent request body"));
	}
}
