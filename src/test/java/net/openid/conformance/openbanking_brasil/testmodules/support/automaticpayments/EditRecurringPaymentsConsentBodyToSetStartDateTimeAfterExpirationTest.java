package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetStartDateTimeAfterExpiration;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToSetStartDateTimeAfterExpirationTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/v2/brazilPaymentConsentWith";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value= BASE_FILE_PATH + "SweepingFieldV2.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new EditRecurringPaymentsConsentBodyToSetStartDateTimeAfterExpiration());

		JsonObject data = environment
			.getElementFromObject(KEY, PATH + ".data")
			.getAsJsonObject();

		JsonObject sweeping = data
			.getAsJsonObject("recurringConfiguration")
			.getAsJsonObject("sweeping");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		assertDate(formatter, sweeping, "startDateTime", 10);
		assertDate(formatter, data, "expirationDateTime", 5);
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data field in consents payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find data field in consents payload");
	}

	@Test
	@UseResurce(value = BASE_FILE_PATH + "outRecurringConfigurationV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingRecurringConfiguration() {
		unhappyPathTest("The recurringConfiguration is not set to sweeping");
	}

	@Test
	@UseResurce(value = BASE_FILE_PATH + "AutomaticFieldV2.json", key = KEY, path = PATH)
	public void unhappyPathTestNotSweeping() {
		unhappyPathTest("The recurringConfiguration is not set to sweeping");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentsConsentBodyToSetStartDateTimeAfterExpiration());
		assertTrue(error.getMessage().contains(message));
	}

	private void assertDate(DateTimeFormatter formatter, JsonObject data, String field, int daysToFuture) {
		String dateTimeString = OIDFJSON.getString(data.get(field));
		LocalDateTime dateTime = LocalDateTime.parse(dateTimeString, formatter);
		LocalDate date = dateTime.toLocalDate();
		LocalDate expectedDate = LocalDate.now(ZoneId.of("UTC")).plusDays(daysToFuture);
		assertEquals(expectedDate, date);
	}
}
