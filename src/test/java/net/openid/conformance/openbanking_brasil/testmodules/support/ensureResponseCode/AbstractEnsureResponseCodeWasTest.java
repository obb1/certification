package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractEnsureResponseCodeWasTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPath(){
		AbstractEnsureResponseCodeWas condition = condition();
		setStatus(happyPathResponseCode());
		run(condition);

	}

	@Test
	public void unhappyPath(){
		AbstractEnsureResponseCodeWas condition = condition();
		setStatus(unhappyPathResponseCode());
		ConditionError error = runAndFail(condition);
		Assert.assertTrue(error.getMessage().contains("Failed to obtain expected status"));
	}

	protected abstract AbstractEnsureResponseCodeWas condition();
	protected abstract int happyPathResponseCode();
	protected abstract int unhappyPathResponseCode();

}
