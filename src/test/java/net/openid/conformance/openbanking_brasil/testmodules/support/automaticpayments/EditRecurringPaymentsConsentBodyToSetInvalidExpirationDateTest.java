package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToSetInvalidExpirationDateTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTest() {
		EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate condition = new EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate();
		run(condition);

		JsonObject data = environment
			.getElementFromObject("resource", "brazilPaymentConsent.data")
			.getAsJsonObject();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		assertDate(formatter, data, "startDateTime", 10);
		assertDate(formatter, data, "expirationDateTime", 5);
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject("resource", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate condition = new EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate condition = new EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	private void assertDate(DateTimeFormatter formatter, JsonObject data, String field, int daysToFuture) {
		String dateTimeString = OIDFJSON.getString(data.get(field));
		LocalDateTime dateTime = LocalDateTime.parse(dateTimeString, formatter);
		LocalDate date = dateTime.toLocalDate();
		LocalDate expectedDate = LocalDate.now(ZoneId.of("UTC")).plusDays(daysToFuture);
		assertEquals(expectedDate, date);
	}
}
