package net.openid.conformance.openbanking_brasil.testmodules.support.payments;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;



public class GenerateNewE2EIDTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathObjectTest() throws IOException {

		assertEndToEndIdWith("test_config.json");

	}

	@Test
	public void happyPathArrayTest() throws IOException {

		assertEndToEndIdWith("test_config_payments_v4.json");

	}

	public void assertEndToEndIdWith(String fileName) throws IOException {
		String rawJson = IOUtils.resourceToString(fileName, Charset.defaultCharset(), getClass().getClassLoader());
		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);
		environment.putString("ispb", "00000000");

		GenerateNewE2EID condition = new GenerateNewE2EID();
		run(condition);

		String ispb = environment.getString("ispb");
		OffsetDateTime currentDateTime = OffsetDateTime.now(ZoneOffset.UTC);
		String formattedCurrentDateTime = currentDateTime.format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"));
		String randomString = environment.getString("endToEndId").substring(21);
		String idealEndToEndId = String.format(
			"E%s%s%s",
			ispb,
			formattedCurrentDateTime,
			randomString
		);

		Assert.assertEquals(idealEndToEndId,environment.getString("endToEndId"));

	}

}
