package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PrepareToMakeACallToEnrollmentsEndpointTest extends AbstractJsonResponseConditionUnitTest {

	private static final String ENROLLMENTS_URL = "https://www.example.com/open-banking/enrollments/v1/enrollments";
	private static final String ENROLLMENT_ID = "urn:bancoex:C1DD33123";

	public void happyPathTestPostEnrollments() {
		environment.putString("config", "resource.enrollmentsUrl", ENROLLMENTS_URL);
		run(new PrepareToPostEnrollments());
		assertEquals(ENROLLMENTS_URL, environment.getString("enrollments_url"));
		assertEquals("POST", environment.getString("http_method"));
		assertEquals(true, environment.getBoolean("expects_self_link"));
	}

	@Test
	public void happyPathTestFetchEnrollments() {
		happyPathTest(new PrepareToFetchEnrollmentsRequest(),
			"GET", "", true);
	}

	@Test
	public void happyPathTestPatchEnrollments() {
		happyPathTest(new PrepareToPatchEnrollments(),
			"PATCH", "", true);
	}

	@Test
	public void happyPathTestPostFidoRegistration() {
		happyPathTest(new PrepareToPostFidoRegistration(),
			"POST", "/fido-registration", false);
	}

	@Test
	public void happyPathTestPostFidoRegistrationOptions() {
		happyPathTest(new PrepareToPostFidoRegistrationOptions(),
			"POST", "/fido-registration-options", false);
	}

	@Test
	public void happyPathTestPostFidoSignOptions() {
		happyPathTest(new PrepareToPostFidoSignOptions(),
			"POST", "/fido-sign-options", false);
	}

	@Test
	public void happyPathTestPostRiskSignals() {
		happyPathTest(new PrepareToPostRiskSignals(),
			"POST", "/risk-signals", false);
	}

	public void happyPathTestPostConsentsAuthorise() {
		environment.putString("config", "resource.enrollmentsUrl", ENROLLMENTS_URL);
		environment.putString("consent_id", ENROLLMENT_ID);
		run(new PrepareToPostConsentsAuthorize());
		String expectedUrl = String.format(
			"%s/%s/authorise",
			ENROLLMENTS_URL.replaceFirst("enrollments$", "consents"),
			ENROLLMENT_ID
		);
		assertEquals(expectedUrl, environment.getString("enrollments_url"));
		assertEquals("POST", environment.getString("http_method"));
		assertEquals(false, environment.getBoolean("expects_self_link"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConfig() {
		environment.putString("enrollment_id", ENROLLMENT_ID);
		PrepareToFetchEnrollmentsRequest condition = new PrepareToFetchEnrollmentsRequest();
		run(condition);
	}

	public void unhappyPathTestMissingEnrollmentId() {
		environment.putObject("config", new JsonObject());
		unhappyPathTest(new PrepareToFetchEnrollmentsRequest(),
			"enrollmentId is missing from environment");
	}

	public void unhappyPathTestMissingConsentId() {
		environment.putObject("config", new JsonObject());
		unhappyPathTest(new PrepareToPostConsentsAuthorize(),
			"consentId is missing from environment");
	}

	@Test
	public void unhappyPathTestMissingResource() {
		environment.putString("enrollment_id", ENROLLMENT_ID);
		environment.putObject("config", new JsonObject());
		unhappyPathTest(new PrepareToFetchEnrollmentsRequest(),
			"enrollments url missing from configuration");
	}

	@Test
	public void unhappyPathTestMissingEnrollmentsUrl() {
		environment.putString("enrollment_id", ENROLLMENT_ID);
		environment.putObject("config", "resource", new JsonObject());
		unhappyPathTest(new PrepareToFetchEnrollmentsRequest(),
			"enrollments url missing from configuration");
	}

	private void happyPathTest(
		AbstractPrepareToMakeACallToEnrollmentsEndpoint condition,
		String expectedMethod, String expectedEndpoint, boolean expectsSelfLink
	) {
		environment.putString("config", "resource.enrollmentsUrl", ENROLLMENTS_URL);
		environment.putString("enrollment_id", ENROLLMENT_ID);

		run(condition);
		String expectedUrl = String.format("%s/%s", ENROLLMENTS_URL, ENROLLMENT_ID) + expectedEndpoint;
		assertEquals(expectedUrl, environment.getString("enrollments_url"));
		assertEquals(expectedMethod, environment.getString("http_method"));
		assertEquals(expectsSelfLink, environment.getBoolean("expects_self_link"));
	}

	private void unhappyPathTest(AbstractPrepareToMakeACallToEnrollmentsEndpoint condition, String errorMessage) {
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
