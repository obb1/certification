package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateInvalidFAPIInteractionIdTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		CreateInvalidFAPIInteractionId condition = new CreateInvalidFAPIInteractionId();
		run(condition);
		assertEquals("123456", environment.getString("fapi_interaction_id"));
	}
}
