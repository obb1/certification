package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SetProtectedResourceUrlToRecurringPaymentsEndpointTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "config";
	private static final String PATH = "resource.resourceUrl";
	private static final String BASE_URL = "https://www.example.com/";
	private static final String ERROR_MESSAGE = "Base url path has not been correctly provided. It must match the regex";

	@Test
	public void happyPathTest() {
		environment.putString(KEY, PATH, BASE_URL + "automatic-payments/v1/recurring-payments");
		SetProtectedResourceUrlToRecurringPaymentsEndpoint condition = new SetProtectedResourceUrlToRecurringPaymentsEndpoint();
		run(condition);
	}

	@Test
	public void unhappyPathTestDifferentBaseApi() {
		environment.putString(KEY, PATH, BASE_URL + "payments/v1/payments");
		SetProtectedResourceUrlToRecurringPaymentsEndpoint condition = new SetProtectedResourceUrlToRecurringPaymentsEndpoint();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(ERROR_MESSAGE));
	}
}
