package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddTransactionIdentificationTest extends AbstractJsonResponseConditionUnitTest {
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";
	private static final String expectedIdentification = "E00038166201907261559y6j6";

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpf.json", key=KEY, path=PATH)
	public void happyPathSingleTest() {
		AddTransactionIdentification condition = new AddTransactionIdentification();
		run(condition);

		String identification = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.transactionIdentification"));
		assertEquals(expectedIdentification, identification);
	}

	@Test
	@UseResurce(value="jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key=KEY, path=PATH)
	public void happyPathArrayTest() {
		AddTransactionIdentification condition = new AddTransactionIdentification();
		run(condition);

		JsonArray data = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray();

		for (int i = 0 ; i < data.size(); i++) {

			String identification = OIDFJSON.getString(data.get(i).getAsJsonObject().get("transactionIdentification"));
			assertEquals(expectedIdentification, identification);
		}
	}

}
