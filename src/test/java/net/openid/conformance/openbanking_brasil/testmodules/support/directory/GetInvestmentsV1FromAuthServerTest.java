package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import org.junit.Before;

public class GetInvestmentsV1FromAuthServerTest extends AbstractGetXFromAuthServerTest {

	@Before
	@Override
	public void init(){
		environment.putString("investment-api", "test-investment");
		super.init();
	}

	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/test-investment/v1/investments";
	}

	@Override
	protected String getApiFamilyType() {
		return "test-investment";
	}

	@Override
	protected String getApiVersion() {
		return "1.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetInvestmentsV1FromAuthServer();
	}
}
