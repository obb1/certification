package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mongodb.client.MongoClient;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.info.TestPlanService;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.token.TokenService;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static net.openid.conformance.openbanking_brasil.testmodules.support.directory.EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes.FamilyTypeRule;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypesTests extends AbstractJsonResponseConditionUnitTest {

	@MockBean
	ParticipantsService participantsService;

	@MockBean
	MongoClient mongoClient;


	@MockBean
	ServerInfoTemplate serverInfoTemplate;

	@MockBean
	TestInfoService testInfoService;

	@MockBean
	TestPlanService testPlanService;

	@MockBean
	TokenService tokenService;

	@MockBean
	EventLog eventLog;

	private final List<String> MANDATORY_SCOPES = List.of(
		"invoice-financings",
		"financings",
		"loans",
		"unarranged-accounts-overdraft",
		"bank-fixed-incomes",
		"credit-fixed-incomes",
		"variable-incomes",
		"treasure-titles",
		"funds",
		"exchanges"
	);

	@Before
	public void init() throws IOException {
		environment.putObject("server", new JsonObject());
		environment.putObject("authorisation_server", new JsonObject());
		JsonObject config = new JsonObject();
		JsonObject resource = new JsonObject();
		resource.addProperty("brazilOrganizationId","org1");
		config.add("resource",resource);
		environment.putObject("config", config);

		String org = IOUtils.resourceToString("jsonResponses/participantsEndpoint/participantsResponseMultipleServers.json", Charset.defaultCharset(), getClass().getClassLoader());
		List<Map> all = new Gson().fromJson(org, new TypeToken<List<Map>>(){}.getType());
		Mockito.when(participantsService.orgsFor(Set.of("org1"))).thenReturn(Set.copyOf(all));
	}

	@Test
	public void noScopesSupported() {
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		ConditionError e = runAndFail(condition);
		assertEquals(getErrorMessage("scopes_supported field missing from server response"), e.getMessage());
	}

	@Test
	public void noApiResources() {
		addScopeSupported(List.of("test"));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		ConditionError e = runAndFail(condition);
		assertEquals(getErrorMessage("ApiResources field missing from authorisation server"), e.getMessage());
	}


	@Test
	public void mandatoryScopesAreMissingWithDADOS() {
		addMandatoryScopes().remove(0);
		createServer(List.of(createApiResource("test")));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		ConditionError e = runAndFail(condition);
		assertTrue(e.getMessage().contains("Authorisation Server is missing mandatory scopes"));
	}

	@Test
	public void mandatoryScopesAreMissingNoDADOS() {
		environment.putString("config", "resource.brazilOrganizationId", "org2");
		addMandatoryScopes().remove(0);
		createServer(List.of(createApiResource("test")));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		run(condition);
	}


	@Test
	public void familyTypeValidationIsSkippedForMandatoryScopes() {
		addMandatoryScopes();
		createServer(List.of());
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		run(condition);
	}

	@Test
	public void errorIsNotThrownIfNonStandardScopeIsProvided() {
		addMandatoryScopes().add("test");
		createServer(List.of(createApiResource("discovery")));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		run(condition);
	}

	@Test
	public void allFamilyTypesAreRequiredForSpecificScopes() {
		addMandatoryScopes().add("payments");
		createServer(List.of(
			createApiResource("payments-consents"),
			createApiResource("payments-pix")
		));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		run(condition);

		addMandatoryScopes().add("customers");
		createServer(List.of(
			createApiResource("customers-personal"),
			createApiResource("customers-business")
		));
		run(condition);
	}


	@Test
	public void atLeastOneFamilyTypeIsRequiredForSpecificScopes() {
		addMandatoryScopes().add("customers");
		createServer(List.of(
			createApiResource("customers-personal")
		));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		run(condition);

		createServer(List.of(
			createApiResource("customers-business")
		));
	}

	@Test
	public void errorIsThrownIfApiFamiliesDontCorrespondToScopes() {
		JsonArray scopes = addMandatoryScopes();
		addScopeSupported(scopes, List.of("customers", "payments"));
		createServer(List.of(
			createApiResource("payments-consents"),
			createApiResource("customers-business")
		));

		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();

		ConditionError e = runAndFail(condition);
		assertTrue(e.getMessage().contains("The API family types from auth server do not have a family type corresponding to a scope from well-known"));

		createServer(List.of(
			createApiResource("payments-consents"),
			createApiResource("payments-pix")
		));

		e = runAndFail(condition);
		assertTrue(e.getMessage().contains("The API family types from auth server do not have a family type corresponding to a scope from well-known"));
	}

	@Test
	public void mandatoryScopesAreMissingAndApiFamiliesDontCorrespondToScopes() {
		JsonArray scopes = addMandatoryScopes();
		addScopeSupported(scopes, List.of("customers", "payments"));
		scopes.remove(0);
		createServer(List.of(
			createApiResource("payments-consents"),
			createApiResource("customers-business")
		));

		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();

		ConditionError e = runAndFail(condition);
		assertTrue(e.getMessage().contains("Authorisation Server is missing mandatory scopes"));
		assertTrue(e.getMessage().contains("The API family types from auth server do not have a family type corresponding to a scope from well-known"));
	}

	@Test
	public void errorIsThrownIfScopeDoesNotCorrespondToFamilyType() {
		addMandatoryScopes();
		createServer(List.of(
			createApiResource("payments-consents"),
			createApiResource("payments-pix")
		));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();

		ConditionError e = runAndFail(condition);
		assertTrue(e.getMessage().contains("The scopes supported from well-known do not have a scope corresponding to a family type from the auth server"));
	}

	@Test
	public void ignoreIfFamilyTypeIsNotMapped() {
		addMandatoryScopes();
		createServer(List.of(
			createApiResource("unmapped-family-type")
		));
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();
		run(condition);
	}

	@Test
	public void weCanValidateAllScopesAndFamilyTypes() {
		EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes condition = new EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes();

		getScopeToFamilyTypeMap().forEach((scope, familyTypes) -> {
			addMandatoryScopes().add(scope);
			createServer(familyTypes.stream().map(this::createApiResource).collect(Collectors.toList()));
			run(condition);
		});
	}


	// Family Type Rule tests



	@Test
	public void weCanValidateSingleRuleWithOneFamilyType(){
		FamilyTypeRule rule = new FamilyTypeRule("expected_family_type");

		List<String> result = rule.validate(List.of("expected_family_type"));
		assertEquals(0, result.size());

		result = rule.validate(List.of("wrong_family_type"));
		assertEquals(1, result.size());
		assertEquals("[expected_family_type]", result.get(0));

	}


	@Test
	public void weCanValidateSingleRuleWithMultipleFamilyTypes(){
		FamilyTypeRule rule = new FamilyTypeRule(List.of("expected_family_type", "expected_family_type_2"));

		List<String> result = rule.validate(List.of("expected_family_type", "expected_family_type_2"));
		assertEquals(0, result.size());

		result = rule.validate(List.of("expected_family_type"));
		assertEquals(1, result.size());
		assertEquals("expected_family_type_2", result.get(0));

		result = rule.validate(List.of("expected_family_type_2"));
		assertEquals(1, result.size());
		assertEquals("expected_family_type", result.get(0));

		result = rule.validate(List.of("wrong_family_type"));
		assertEquals(1, result.size());
		assertEquals("[expected_family_type, expected_family_type_2]", result.get(0));
	}


	@Test
	public void weCanValidateMultipleRulesWithOneFamilyType(){
		FamilyTypeRule rule = new FamilyTypeRule()
			.add(List.of("expected_family_type"))
			.add(List.of("expected_family_type_2"));

		List<String> result = rule.validate(List.of("expected_family_type"));
		assertEquals(0, result.size());

		result = rule.validate(List.of("expected_family_type_2"));
		assertEquals(0, result.size());

		result = rule.validate(List.of("expected_family_type", "expected_family_type_2"));
		assertEquals(0, result.size());

		result = rule.validate(List.of("wrong_family_type"));
		assertEquals(1, result.size());
		assertEquals("[expected_family_type] OR [expected_family_type_2]", result.get(0));

	}

	@Test
	public void weCanValidateMultipleRulesWithMultipleFamilyTypes(){
		FamilyTypeRule rule = new FamilyTypeRule()
			.add(List.of("expected_family_type_1", "expected_family_type_2"))
			.add(List.of("expected_family_type_3", "expected_family_type_4"));

		List<String> result = rule.validate(List.of("expected_family_type_1", "expected_family_type_2"));
		assertEquals(0, result.size());

		result = rule.validate(List.of("expected_family_type_3", "expected_family_type_4"));
		assertEquals(0, result.size());

		result = rule.validate(List.of("expected_family_type_1", "expected_family_type_2", "expected_family_type_3", "expected_family_type_4"));
		assertEquals(0, result.size());


		result = rule.validate(List.of("expected_family_type_1"));
		assertEquals(1, result.size());
		assertEquals("expected_family_type_2", result.get(0));

		result = rule.validate(List.of("expected_family_type_2"));
		assertEquals(1, result.size());
		assertEquals("expected_family_type_1", result.get(0));

		result = rule.validate(List.of("expected_family_type_3"));
		assertEquals(1, result.size());
		assertEquals("expected_family_type_4", result.get(0));

		result = rule.validate(List.of("expected_family_type_4"));
		assertEquals(1, result.size());
		assertEquals("expected_family_type_3", result.get(0));

		result = rule.validate(List.of("wrong_family_type"));
		assertEquals(1, result.size());
		assertEquals("[expected_family_type_1, expected_family_type_2] OR [expected_family_type_3, expected_family_type_4]", result.get(0));
	}


	private JsonObject createApiResource(String apiFamilyType) {
		JsonObject resource = new JsonObject();
		resource.addProperty("ApiFamilyType", apiFamilyType);
		return resource;
	}

	private void createServer(List<JsonObject> apiResources) {
		JsonArray apiResourcesArray = new JsonArray();
		apiResources.forEach(apiResourcesArray::add);

		JsonObject authorisationServer = new JsonObject();
		authorisationServer.add("ApiResources", apiResourcesArray);
		environment.putObject("authorisation_server", authorisationServer);
	}

	private JsonArray addScopeSupported(JsonArray scopesArray, List<String> scopes) {
		scopes.forEach(scopesArray::add);
		JsonObject server = new JsonObject();
		server.add("scopes_supported", scopesArray);
		environment.putObject("server", server);
		return scopesArray;
	}


	private JsonArray addScopeSupported(List<String> scopes) {
		JsonArray scopesArray = new JsonArray();
		scopes.forEach(scopesArray::add);
		JsonObject server = new JsonObject();
		server.add("scopes_supported", scopesArray);
		environment.putObject("server", server);
		return scopesArray;
	}

	private JsonArray addMandatoryScopes() {
		return addScopeSupported(MANDATORY_SCOPES);
	}


	private String getErrorMessage(String message) {
		return String.format("EnsureScopesSupportedFromWellKnownCorrespondToAuthServerApiFamilyTypes: %s", message);
	}

	private Map<String, List<String>> getScopeToFamilyTypeMap() {
		Map<String, List<String>> scopeToFamilyTypeMap = new HashMap<>();

		/* Phase 2 */
		scopeToFamilyTypeMap.put("consents", List.of("consents"));
		scopeToFamilyTypeMap.put("resources", List.of("resources"));

		scopeToFamilyTypeMap.put("customers", List.of(
			"customers-personal",
			"customers-business"
		));

		scopeToFamilyTypeMap.put("credit-cards-accounts", List.of("credit-cards-accounts"));
		scopeToFamilyTypeMap.put("accounts", List.of("accounts"));
		scopeToFamilyTypeMap.put("loans", List.of("loans"));
		scopeToFamilyTypeMap.put("financings", List.of("financings"));
		scopeToFamilyTypeMap.put("unarranged-accounts-overdraft", List.of("unarranged-accounts-overdraft"));
		scopeToFamilyTypeMap.put("invoice-financings", List.of("invoice-financings"));

		/* Phase 3 */
		scopeToFamilyTypeMap.put("payments", List.of(
			"payments-consents",
			"payments-pix"
		));

		scopeToFamilyTypeMap.put("recurring-payments", List.of(
			"payments-recurring-consents",
			"payments-pix-recurring-payments"
		));

		/* Phase 4B */
		scopeToFamilyTypeMap.put("bank-fixed-incomes", List.of("bank-fixed-incomes"));
		scopeToFamilyTypeMap.put("credit-fixed-incomes", List.of("credit-fixed-incomes"));
		scopeToFamilyTypeMap.put("variable-incomes", List.of("variable-incomes"));
		scopeToFamilyTypeMap.put("treasure-titles", List.of("treasure-titles"));
		scopeToFamilyTypeMap.put("funds", List.of("funds"));

		return scopeToFamilyTypeMap;
	}



}
