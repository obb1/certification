package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractPaymentIdFromLastPaymentTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PATH = "jsonResponses/paymentInitiation/pixByPayments/v4/";

	@Test
	@UseResurce(BASE_PATH + "standardPostPaymentsV4ResponseWith2Payments.json")
	public void happyPathTest() {
		ExtractPaymentIdFromLastPayment condition = new ExtractPaymentIdFromLastPayment();
		run(condition);
		assertEquals("TXpRMU9UQTROMWhZV2xSU1FUazJSMDf", environment.getString("payment_id"));
	}

	@Test
	@UseResurce(BASE_PATH + "emptyPostPaymentsV4Response.json")
	public void unhappyPathTestEmptyPayload() {
		ExtractPaymentIdFromLastPayment condition = new ExtractPaymentIdFromLastPayment();
		IndexOutOfBoundsException exception = assertThrows(IndexOutOfBoundsException.class, () -> run(condition));
		String expectedMessage = "Index -1 out of bounds for length 0";
		assertTrue(exception.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(BASE_PATH + "postPaymentsV4ResponseMissingPaymentId.json")
	public void unhappyPathTestMissingPaymentId() {
		ExtractPaymentIdFromLastPayment condition = new ExtractPaymentIdFromLastPayment();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "Could not extract payment ID from last payment";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
