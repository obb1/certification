package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.JsonUtils;
import net.openid.conformance.util.UseResurce;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.text.ParseException;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateFidoRegistrationRequestBodyToRequestEntityClaimsTest extends AbstractJsonResponseConditionUnitTest {

	private final static String EC_KID = "ec_id";
	private final static String RSA_KID = "rsa_id";

	private final static Supplier<JsonObject> CLIENT_DATA = () -> new JsonObjectBuilder().addField("test", "test").build();

	public static final String ENCODED_ATTESTATION_OBJECT = "encoded_attestation_object";

	@Before
	public void init() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException {
		setJwt(true);

		Security.addProvider(new BouncyCastleProvider());
		environment.putObject("client_data", CLIENT_DATA.get());
		environment.putString("encoded_attestation_object", ENCODED_ATTESTATION_OBJECT);


		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC", "BC");
		keyPairGenerator.initialize(new ECGenParameterSpec("prime256v1"));
		KeyPair ecKeyPair = keyPairGenerator.generateKeyPair();
		JWK ecJwk = new ECKey.Builder(Curve.P_256, (ECPublicKey) ecKeyPair.getPublic()).privateKey(ecKeyPair.getPrivate()).keyID(EC_KID).build();


		keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair rsaKeyPair = keyPairGenerator.generateKeyPair();
		JWK rsaKwk = new RSAKey.Builder((RSAPublicKey) rsaKeyPair.getPublic()).privateKey(rsaKeyPair.getPrivate()).keyID(RSA_KID).build();

		environment.putObject("rsa_fido_keys_jwk", JsonUtils.createBigDecimalAwareGson().fromJson(rsaKwk.toJSONString(), JsonObject.class));
		environment.putObject("ec_fido_keys_jwk", JsonUtils.createBigDecimalAwareGson().fromJson(ecJwk.toJSONString(), JsonObject.class));
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void happyPathWithRsa() throws ParseException {
		environment.mapKey("fido_keys_jwk", "rsa_fido_keys_jwk");
		run(new CreateFidoRegistrationRequestBodyToRequestEntityClaims());
		JsonObject requestBody = environment.getObject("resource_request_entity_claims");
		validateRequestBody(RSA_KID, requestBody, false);
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void happyPathWithEc() throws ParseException {
		environment.mapKey("fido_keys_jwk", "ec_fido_keys_jwk");
		run(new CreateFidoRegistrationRequestBodyToRequestEntityClaims());
		JsonObject requestBody = environment.getObject("resource_request_entity_claims");
		validateRequestBody(EC_KID, requestBody, false);
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsNoAuthenticatorSelection.json")
	public void happyPathWithoutAuthenticatorSelection() throws ParseException {
		environment.mapKey("fido_keys_jwk", "ec_fido_keys_jwk");
		run(new CreateFidoRegistrationRequestBodyToRequestEntityClaims());
		JsonObject requestBody = environment.getObject("resource_request_entity_claims");
		validateRequestBody(EC_KID, requestBody, false);
	}

	@Test
	public void unhappyPathMissingBody() {
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathInvalidJwt() {
		environment.putObject(
			CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya1.b2.c3").build()
		);
		unhappyPathTest("Error parsing JWT response");
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void unhappyPathMissingKid() {
		environment.getObject("ec_fido_keys_jwk").remove("kid");
		unhappyPathTest("Could not find kid in the fido_keys_jwk");
	}

	protected void unhappyPathTest(String errorMessage) {
		environment.mapKey("fido_keys_jwk", "ec_fido_keys_jwk");
		ConditionError error = runAndFail(new CreateFidoRegistrationRequestBodyToRequestEntityClaims());
		assertTrue(error.getMessage().contains(errorMessage));
	}

	private void validateRequestBody(String kid, JsonObject requestBody, boolean hasExtensions) throws ParseException {
		JsonObject registrationOptionsResponseBody = BodyExtractor.bodyFrom(environment, "resource_endpoint_response_full")
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> new AssertionError("Test Failed - no resource was provided"));


		String id = Base64.getEncoder().withoutPadding().encodeToString(kid.getBytes(StandardCharsets.UTF_8));
		JsonObjectBuilder fidoRegistrationRequestBuilder = new JsonObjectBuilder()
			.addField("data.id", id)
			.addField("data.rawId", id)
			.addField("data.type", "public-key")
			.addField("data.response.clientDataJSON", Base64.getEncoder().withoutPadding().encodeToString(CLIENT_DATA.get().toString().getBytes(StandardCharsets.UTF_8)))
			.addField("data.response.attestationObject", ENCODED_ATTESTATION_OBJECT);


		Optional.ofNullable(registrationOptionsResponseBody.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("authenticatorSelection"))
			.map(authenticatorSelection -> authenticatorSelection.get("authenticatorAttachment"))
			.map(OIDFJSON::getString)
			.ifPresent(authenticatorAttachment -> fidoRegistrationRequestBuilder.addField("data.authenticatorAttachment", authenticatorAttachment));

		if (hasExtensions) {
			fidoRegistrationRequestBuilder.addFields("data.clientExtensionResults", Map.of(
				"additionalProp1", "string",
				"additionalProp2", "string",
				"additionalProp3", "string"
			));
		}

		JsonObject expectedBody = fidoRegistrationRequestBuilder.build();

		assertEquals(expectedBody, requestBody);

	}


}
