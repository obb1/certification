package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensurePaymentListDates;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.AbstractEnsurePaymentListDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.EnsureTwoPaymentsForToday;

import java.time.LocalDate;
import java.time.ZoneId;

public class EnsureTwoPaymentsForTodayTest extends AbstractEnsurePaymentListDatesTest {

	@Override
	protected AbstractEnsurePaymentListDates condition() {
		return new EnsureTwoPaymentsForToday();
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.now(ZoneId.of("UTC"));
	}

	@Override
	protected int expectedAmount() {
		return 2;
	}
}
