package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasRejected;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsurePaymentConsentStatusWasRejectedTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationRejectedConsentResponse.json")
	public void happyPathTest() {
		run(new EnsurePaymentConsentStatusWasRejected());
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v2/paymentInitiationAuthorizedConsentResponse.json")
	public void unhappyPathTest() {
		ConditionError error = runAndFail(new EnsurePaymentConsentStatusWasRejected());
		assertTrue(error.getMessage().contains("Consent not in the expected state"));
	}
}
