package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetBusinessIdentificationsV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/customers/v1/business/identifications";
	}

	@Override
	protected String getApiFamilyType() {
		return "customers-business";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetBusinessIdentificationsV2Endpoint();
	}
}
