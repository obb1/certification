package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SetPaymentsDataToBeJsonArrayTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		SetPaymentsDataToBeJsonArray condition = new SetPaymentsDataToBeJsonArray();
		run(condition);
		assertEquals("ok", environment.getString("payment_is_array"));
	}
}
