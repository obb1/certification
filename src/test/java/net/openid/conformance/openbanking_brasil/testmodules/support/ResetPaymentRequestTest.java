package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ResetPaymentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectInvalidCreditorAccountToPayment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ResetPaymentRequestTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	public void happyPathSingleTest() {
		environment.putString("previous_currency", "GBP");
		environment.putString("previous_amount","1001.10");
		ResetPaymentRequest condition = new ResetPaymentRequest();
		run(condition);

		String currency = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.payment").getAsJsonObject().get("currency"));
		String amount = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.payment").getAsJsonObject().get("amount"));
		assertEquals(currency, environment.getString("previous_currency"));
		assertEquals(amount, environment.getString("previous_amount"));
	}

	@Test
	@UseResurce(value="test_resource_payments_v4.json", key="resource")
	public void happyPathArrayTest(){
		environment.putString("previous_currency", "GBP");
		environment.putString("previous_amount","1001.10");
		ResetPaymentRequest condition = new ResetPaymentRequest();
		run(condition);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");
		for (int i = 0 ; i < data.size(); i++) {
			String currency = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("payment").getAsJsonObject().get("currency"));
			String amount = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("payment").getAsJsonObject().get("amount"));
			assertEquals(currency, environment.getString("previous_currency"));
			assertEquals(amount, environment.getString("previous_amount"));
		}
	}
}
