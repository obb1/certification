package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetUnarrangedAccountsOverdraftV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/unarranged-accounts-overdraft/v2/contracts";
	}

	@Override
	protected String getApiFamilyType() {
		return "unarranged-accounts-overdraft";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetUnarrangedAccountsOverdraftV2Endpoint();
	}
}
