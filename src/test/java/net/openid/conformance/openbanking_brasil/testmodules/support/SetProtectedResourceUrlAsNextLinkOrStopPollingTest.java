package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SetProtectedResourceUrlAsNextLinkOrStopPollingTest extends AbstractJsonResponseConditionUnitTest {

	private static final String NEXT = "https://api.banco.com.br/open-banking/api/v1/resource";

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3.json")
	public void happyPathTestWithNext() {
		run(new SetProtectedResourceUrlAsNextLinkOrStopPolling());
		assertEquals(NEXT, environment.getString("protected_resource_url"));
		assertFalse(environment.getBoolean("no_next"));
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3OnlySelfLink.json")
	public void happyPathTestWithoutNext() {
		run(new SetProtectedResourceUrlAsNextLinkOrStopPolling());
		assertNull(environment.getString("protected_resource_url"));
		assertTrue(environment.getBoolean("no_next"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponseEnvKey() {
		run(new SetProtectedResourceUrlAsNextLinkOrStopPolling());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(SetProtectedResourceUrlAsNextLinkOrStopPolling.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(SetProtectedResourceUrlAsNextLinkOrStopPolling.RESPONSE_ENV_KEY,
			new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3MissingLinks.json")
	public void unhappyPathTestMissingLinks() {
		unhappyPathTest("No links object found");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new SetProtectedResourceUrlAsNextLinkOrStopPolling());
		assertTrue(error.getMessage().contains(message));
	}
}
