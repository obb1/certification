package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AddVariantUserToConfigTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void testOperationalPersonalUserHappyPath() {
		environment.putString("config", "resource.brazilCpfOperational", "random_cpf");
		AddOperationalUserToConfig cond = new AddOperationalUserToConfig();

		run(cond);

		assertEquals("random_cpf", environment.getString("config", "resource.brazilCpf"));
		assertNull(environment.getString("config", "resource.brazilCnpj"));
	}

	@Test
	public void testOperationalBusinessUserHappyPath() {
		environment.putString("config", "resource.brazilCpfOperational", "random_cpf");
		environment.putString("config", "resource.brazilCnpjOperationalBusiness", "random_cnpj");
		AddOperationalUserToConfig cond = new AddOperationalUserToConfig();

		run(cond);

		assertEquals("random_cpf", environment.getString("config", "resource.brazilCpf"));
		assertEquals("random_cnpj", environment.getString("config", "resource.brazilCnpj"));
	}

	@Test
	public void testOperationalPaginationUserHappyPath() {
		environment.putString("config", "resource.brazilCpfPaginationList", "random_cpf");
		AddPaginationUserToConfig cond = new AddPaginationUserToConfig();

		run(cond);

		assertEquals("random_cpf", environment.getString("config", "resource.brazilCpf"));
		assertNull(environment.getString("config", "resource.brazilCnpj"));
	}

	@Test
	public void testPaginationBusinessUserHappyPath() {
		environment.putString("config", "resource.brazilCpfPaginationList", "random_cpf");
		environment.putString("config", "resource.brazilCnpjPaginationList", "random_cnpj");
		AddPaginationUserToConfig cond = new AddPaginationUserToConfig();

		run(cond);

		assertEquals("random_cpf", environment.getString("config", "resource.brazilCpf"));
		assertEquals("random_cnpj", environment.getString("config", "resource.brazilCnpj"));
	}

	@Test
	public void testNoUser() {
		environment.putString("config", "resource.brazilCpf", "random_cpf");
		environment.putString("config", "resource.brazilCnpj", "random_cnpj");
		AddOperationalUserToConfig cond = new AddOperationalUserToConfig();

		run(cond);

		assertEquals("random_cpf", environment.getString("config", "resource.brazilCpf"));
		assertEquals("random_cnpj", environment.getString("config", "resource.brazilCnpj"));
	}
}
