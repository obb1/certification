package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

public class ValidatePaymentAndConsentHaveSamePropertiesTest extends AbstractJsonResponseConditionUnitTest {

	//not matcing amount
	//not matching currency

	@Test
	public void happyPathArrayTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);
		environment.putString("payment_is_array", "ok");


		ValidatePaymentAndConsentHaveSameProperties condition = new ValidatePaymentAndConsentHaveSameProperties();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);
	}
	@Test
	public void happyPathSingleTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4_single.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);

		ValidatePaymentAndConsentHaveSameProperties condition = new ValidatePaymentAndConsentHaveSameProperties();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);
	}

	@Test
	public void unhappyPathSingleTestAmount() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4_single.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);

		JsonObject paymentObj = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonObject("data").getAsJsonObject("payment");
		paymentObj.addProperty("amount","10.12");
		ValidatePaymentAndConsentHaveSameProperties condition = new ValidatePaymentAndConsentHaveSameProperties();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		try {
			condition.execute(environment);
			fail("Should have thrown an error");
		} catch(ConditionError e) {
			assertThat(e.getMessage(), containsString("Payment and consent do not have matching amounts"));
		}
	}

	@Test
	public void unhappyPathSingleTestCurrency() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4_single.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();

		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);

		JsonObject paymentObj = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonObject("data").getAsJsonObject("payment");
		paymentObj.addProperty("currency","PLN");
		ValidatePaymentAndConsentHaveSameProperties condition = new ValidatePaymentAndConsentHaveSameProperties();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		try {
			condition.execute(environment);
			fail("Should have thrown an error");
		} catch(ConditionError e) {
			assertThat(e.getMessage(), containsString("Payment and consent do not have matching currency"));
		}
	}

}
