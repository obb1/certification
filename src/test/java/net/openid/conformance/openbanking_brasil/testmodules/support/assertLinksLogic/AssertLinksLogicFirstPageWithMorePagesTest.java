package net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AssertLinksLogicFirstPageWithMorePagesTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/links/CompleteFirstPageResponse.json")
	public void happyPathTest() {
		run(new AssertLinksLogicFirstPageWithMorePages());
	}

	@Test
	@UseResurce("jsonResponses/links/CompleteFirstPageOmittingPage1Response.json")
	public void happyPathTestOmittingPageParamForSelf() {
		run(new AssertLinksLogicFirstPageWithMorePages());
	}

	@Test
	@UseResurce("jsonResponses/links/FirstPageWithPrevResponse.json")
	public void unhappyPathTestWithPrev() {
		unhappyPathTest("First page should not contain \"prev\" link");
	}

	@Test
	@UseResurce("jsonResponses/links/GenericMissingSelfAndPrevResponse.json")
	public void unhappyPathTestMissingSelf() {
		unhappyPathTest("The self link is mandatory for any response");
	}

	@Test
	@UseResurce("jsonResponses/links/GenericMissingNextAndPrevResponse.json")
	public void unhappyPathTestMissingNext() {
		unhappyPathTest("The next link is mandatory if it is not the last page");
	}

	@Test
	@UseResurce("jsonResponses/links/FirstPageSelfAndNextNotSuccessiveResponse.json")
	public void unhappyPathTestSelfAndNextNotSuccessive() {
		unhappyPathTest("The \"page\" parameter from next should be equals to the one from self + 1");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new AssertLinksLogicFirstPageWithMorePages());
		assertTrue(error.getMessage().contains(message));
	}
}
