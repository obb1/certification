package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.setDate;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToToday;

import java.time.LocalDate;
import java.time.ZoneId;

public class EditRecurringPaymentsBodyToSetDateToTodayTest extends AbstractEditRecurringPaymentsBodyToSetDateTest {

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate condition() {
		return new EditRecurringPaymentsBodyToSetDateToToday();
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.now(ZoneId.of("UTC-3"));
	}
}
