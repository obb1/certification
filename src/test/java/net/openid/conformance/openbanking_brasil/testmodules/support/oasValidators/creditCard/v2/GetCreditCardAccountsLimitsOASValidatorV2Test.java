package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GetCreditCardAccountsLimitsOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/limits.json")
	public void happyPath() {
		run(new GetCreditCardAccountsLimitsOASValidatorV2());
	}


	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/limitsNoLimitAmount.json")
	public void noLimitAmount() {
		ConditionError e = runAndFail(new GetCreditCardAccountsLimitsOASValidatorV2());
		assertTrue(e.getMessage().contains("limitAmount is required when isLimitFlexible is false"));
	}


	@Test
	@UseResurce("jsonResponses/creditCard/creditCardV2/limitsNoLimitReason.json")
	public void noLimitReason() {
		ConditionError e = runAndFail(new GetCreditCardAccountsLimitsOASValidatorV2());
		assertTrue(e.getMessage().contains("limitAmount.limitAmountReason is required when limitAmount.amount is 0.0"));

	}
}
