package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SetIncorrectAmountInPaymentsDataArrayTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_CONSENTS_PATH_HAPPY = "jsonRequests/payments/consents/correctPaymentConsentRequestArray";
	private static final String BASE_PAYMENTS_PATH = "jsonRequests/payments/payments/paymentWithDataArrayWith";
	private static final String KEY = "resource";
	private static final String CONSENTS_PATH = "brazilPaymentConsent";
	private static final String PAYMENTS_PATH = "brazilPixPayment";

	private static final String AMOUNT_REGEX = "\\d+\\.\\d{2}";

	@Test
	@UseResurce(value = BASE_CONSENTS_PATH_HAPPY + "DailyScheduleWith5Payments.json", key = KEY, path = CONSENTS_PATH)
	@UseResurce(value = BASE_PAYMENTS_PATH + "5Payments.json", key = KEY, path = PAYMENTS_PATH)
	public void happyPathTest() {
		SetIncorrectAmountInPaymentsDataArray condition = new SetIncorrectAmountInPaymentsDataArray();
		run(condition);

		String consentAmount = OIDFJSON.getString(
			environment.getObject(KEY)
				.getAsJsonObject(CONSENTS_PATH)
				.getAsJsonObject("data")
				.getAsJsonObject("payment")
				.get("amount")
		);
		String paymentAmount = OIDFJSON.getString(
			environment.getObject(KEY)
				.getAsJsonObject(PAYMENTS_PATH)
				.getAsJsonArray("data")
				.get(0).getAsJsonObject()
				.getAsJsonObject("payment")
				.get("amount")
		);

		assertTrue(Pattern.matches(AMOUNT_REGEX, consentAmount));
		assertTrue(Pattern.matches(AMOUNT_REGEX, paymentAmount));
		assertNotEquals(consentAmount, paymentAmount);
	}
}
