package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnsureTokenResponseWasAFailureTest extends AbstractJsonResponseConditionUnitTest {


	@Test
	public void happyPath(){
		environment.putObject("token_endpoint_response", new JsonObject());
		EnsureTokenResponseWasAFailure cond = new EnsureTokenResponseWasAFailure();
		run(cond);
	}

	@Test
	public void unhappyPath() {
		environment.putString("token_endpoint_response", "access_token", "test");
		EnsureTokenResponseWasAFailure cond = new EnsureTokenResponseWasAFailure();

		ConditionError e = runAndFail(cond);
		assertEquals(String.format("%s: Was expecting a failure but a new access_token was issued", cond.getClass().getSimpleName()), e.getMessage());
	}

}
