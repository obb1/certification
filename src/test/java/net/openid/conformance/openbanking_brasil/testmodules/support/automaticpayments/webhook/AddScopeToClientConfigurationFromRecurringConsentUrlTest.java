package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.AddScopeToClientConfigurationFromRecurringConsentUrl;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddScopeToClientConfigurationFromRecurringConsentUrlTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_URL = "https://www.example.com/consents/v4/consents";
	private static final String RECURRING_CONSENT_URL = "https://www.example.com/automatic-payments/v1/recurring-consents";
	private static final String INVALID_URL = "https://www.example.com/payments/v3/pix/payments";

	@Before
	public void init() {
		JsonObject config = new JsonObjectBuilder()
			.addField("resource", new JsonObject())
			.addField("client", new JsonObject())
			.build();
		environment.putObject("config", config);
	}

	@Test
	public void happyPathTestConsents() {
		happyPathTest(CONSENT_URL, "consents");
	}

	@Test
	public void happyPathTestRecurringConsents() {
		happyPathTest(RECURRING_CONSENT_URL, "recurring-payments");
	}

	@Test
	public void unhappyPathTestMissingConsentUrl() {
		unhappyPathTest("Could not find consent URI in the client configuration");
	}

	@Test
	public void unhappyPathTestInvalidUrl() {
		setUrl(INVALID_URL);
		unhappyPathTest("Invalid consent URI was provided");
	}

	private void happyPathTest(String url, String expectedScope) {
		setUrl(url);
		AddScopeToClientConfigurationFromRecurringConsentUrl condition = new AddScopeToClientConfigurationFromRecurringConsentUrl();
		run(condition);

		String scope = getScope();
		assertEquals(expectedScope, scope);
	}

	private void unhappyPathTest(String expectedMessage) {
		AddScopeToClientConfigurationFromRecurringConsentUrl condition = new AddScopeToClientConfigurationFromRecurringConsentUrl();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	private void setUrl(String url) {
		environment.putString("config", "resource.consentUrl", url);
	}

	private String getScope() {
		return environment.getString("config", "client.scope");
	}
}
