package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateRiskSignalsRequestBodyToRequestEntityClaimsTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPath(){
		CreateRiskSignalsRequestBodyToRequestEntityClaims condition = new CreateRiskSignalsRequestBodyToRequestEntityClaims();
		run(condition);
		JsonObject request = environment.getObject("resource_request_entity_claims");

		JsonObject expectedRequestObject = new JsonObjectBuilder().addFields("data", Map.of(
			"deviceId", "5ad82a8f-37e5-4369-a1a3-be4b1fb9c034",
			"isRootedDevice", false,
			"screenBrightness", 90,
			"elapsedTimeSinceBoot", 28800000,
			"osVersion", "16.6",
			"userTimeZoneOffset", "-03",
			"language", "pt",
			"accountTenure", "2023-01-01"
		)).addFields("data.screenDimensions", Map.of(
			"height", 1080,
			"width", 1920
		)).build();

		assertEquals(expectedRequestObject, request);

	}

}
