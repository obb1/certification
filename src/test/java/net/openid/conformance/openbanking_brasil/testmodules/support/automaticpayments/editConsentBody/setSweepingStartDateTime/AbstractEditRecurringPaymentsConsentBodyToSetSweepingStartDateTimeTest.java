package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setSweepingStartDateTime;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate.AbstractEditRecurringPaymentsConsentBodyToSetStartDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime.AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeTest extends AbstractJsonResponseConditionUnitTest {

	protected abstract AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime condition();
	protected abstract LocalDateTime expectedDateTime();

	@Test
	@UseResurce(value = "jsonRequests/automaticPayments/consents/v2/brazilPaymentConsentWithSweepingFieldV2.json", key = "resource", path = "brazilPaymentConsent")
	public void happyPathTest() {
		run(condition());

		String startDateTimeStr = OIDFJSON.getString(environment
			.getElementFromObject("resource", "brazilPaymentConsent.data.recurringConfiguration.sweeping.startDateTime"));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime startDateTime = LocalDateTime.parse(startDateTimeStr, formatter);

		assertEquals(expectedDateTime(), startDateTime);
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		environment.putObject("resource", new JsonObject());
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains("Unable to find data field in consents payload"));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains("Unable to find data field in consents payload"));
	}
}
