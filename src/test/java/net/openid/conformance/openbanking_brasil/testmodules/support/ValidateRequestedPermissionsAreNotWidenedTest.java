package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum.ACCOUNTS_OVERDRAFT_LIMITS_READ;
import static net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum.ACCOUNTS_READ;
import static net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum.RESOURCES_READ;
import static org.mockito.Mockito.mock;

;

public class ValidateRequestedPermissionsAreNotWidenedTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setEnv(){
		JsonObject client = new JsonObject();
		environment.putObject("client", client);
	}

	@Test
	@UseResurce("jsonResponses/consent/v3/GetConsentResponseV3.json")
	public void happyPath() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissions(ACCOUNTS_READ,
			ACCOUNTS_OVERDRAFT_LIMITS_READ,
			RESOURCES_READ);
		permissionsBuilder.build();
		ValidateRequestedPermissionsAreNotWidened condition = new ValidateRequestedPermissionsAreNotWidened();
		run(condition);
	}

}
