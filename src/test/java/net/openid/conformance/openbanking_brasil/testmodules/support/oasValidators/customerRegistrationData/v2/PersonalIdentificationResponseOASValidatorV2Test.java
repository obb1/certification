package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class PersonalIdentificationResponseOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/personIdentificationV2/naturalPersonIdentificationResponseOK-V2.1.0.json")
	public void testHappyPath() {
		PersonalIdentificationResponseOASValidatorV2 cond = new PersonalIdentificationResponseOASValidatorV2();
		run(cond);

		// Marital status constraint.
		JsonObject invalidMaritalStatusData = new JsonObject();
		invalidMaritalStatusData.addProperty("maritalStatusCode", "OUTRO");
		assertThrows(ConditionError.class, () -> cond.assertMaritalStatusConstraint(invalidMaritalStatusData));

		// CPF and passport constraint.
		JsonObject invalidCpfPassportData = new JsonObject();
		assertThrows(ConditionError.class, () -> cond.assertCpfAndPassportConstraint(invalidCpfPassportData));

		// Phone constraint.
		JsonObject phone = new JsonObject();
		phone.addProperty("type", "OUTRO");
		JsonArray phones = new JsonArray();
		phones.add(phone);
		JsonObject contacts = new JsonObject();
		contacts.add("phones", phones);
		JsonObject invalidPhoneData = new JsonObject();
		invalidPhoneData.add("contacts", contacts);
		assertThrows(ConditionError.class, () -> cond.assertPhonesConstraint(invalidPhoneData));
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/personIdentificationV2/errors/naturalPersonIdentificationInvalidResponse-V2.1.0.json")
	public void testUnhappyPath() {
		PersonalIdentificationResponseOASValidatorV2 cond = new PersonalIdentificationResponseOASValidatorV2();
		runAndFail(cond);
	}
}
