package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createPatchConsentEdition;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class AbstractCreatePatchRecurringConsentsForEditionRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	protected static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
	protected static final String LOGGED_USER_IDENTIFICATION = "11111111111";
	protected static final String BUSINESS_ENTITY_IDENTIFICATION = "11111111111111";

	protected abstract AbstractCreatePatchRecurringConsentsForEditionRequestBody condition();

	@Test
	public void happyPathTestWithoutResourceFields() {
		happyPathTest(false);
	}

	@Test
	public void happyPathTestWithResourceFields() {
		happyPathTest(true);
	}

	protected void happyPathTest(boolean hasResourceFields) {
		prepareEnvForTest(hasResourceFields);
		run(condition());
		validateRequestBody(hasResourceFields);
		validateEnvironmentFields();
	}

	protected void prepareEnvForTest(boolean hasResourceFields) {
		JsonObject resource = new JsonObject();
		if (hasResourceFields) {
			resource.addProperty("loggedUserIdentification", LOGGED_USER_IDENTIFICATION);
			resource.addProperty("businessEntityIdentification", BUSINESS_ENTITY_IDENTIFICATION);
		}
		environment.putObject("resource", resource);
	}

	protected void validateRequestBody(boolean hasResourceFields) {
		JsonObject requestBody = environment.getObject("consent_endpoint_request");
		JsonObject data = requestBody.getAsJsonObject("data");

		if (hasRiskSignals()) {
			assertObjectField(data, "riskSignals");
		} else {
			assertFalse(data.has("riskSignals"));
		}

		assertTrue(data.has("creditors") && data.get("creditors").isJsonArray());
		JsonArray creditors = data.getAsJsonArray("creditors");
		assertStringField(creditors.get(0).getAsJsonObject(), "name", getExpectedName());

		if (hasLoggedUser()) {
			JsonObject loggedUser = assertObjectField(data, "loggedUser");
			JsonObject loggedUserDocument = assertObjectField(loggedUser, "document");
			assertStringField(loggedUserDocument, "identification", getExpectedLoggedUserIdentification(hasResourceFields));
			assertStringField(loggedUserDocument, "rel", "CPF");
		} else {
			assertFalse(data.has("loggedUser"));
		}

		if (getExpectedExpirationDateTime() != null) {
			assertStringField(data, "expirationDateTime", getExpectedExpirationDateTime());
		}

		if (getExpectedMaxVariableAmount() != null) {
			JsonObject recurringConfiguration = assertObjectField(data, "recurringConfiguration");
			JsonObject automatic = assertObjectField(recurringConfiguration, "automatic");
			assertStringField(automatic, "maximumVariableAmount", getExpectedMaxVariableAmount());
		}

		if (hasResourceFields) {
			JsonObject businessEntity = assertObjectField(data, "businessEntity");
			JsonObject businessEntityDocument = assertObjectField(businessEntity, "document");
			assertStringField(businessEntityDocument, "identification", BUSINESS_ENTITY_IDENTIFICATION);
			assertStringField(businessEntityDocument, "rel", "CNPJ");
		}
	}

	protected JsonObject assertObjectField(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}

	protected void assertStringField(JsonObject obj, String field, String expectedValue) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		String value = OIDFJSON.getString(obj.get(field));
		assertEquals(expectedValue, value);
	}

	protected void validateEnvironmentFields() {
		if (getExpectedMaxVariableAmount() != null) {
			assertEquals(environment.getString("consent_edition_amount"), getExpectedMaxVariableAmount());
		}
		if (getExpectedExpirationDateTime() != null) {
			assertEquals(environment.getString("consent_edition_expiration"), getExpectedExpirationDateTime());
		}
	}

	protected String getExpectedName() {
		return "Openflix";
	}

	protected String getExpectedLoggedUserIdentification(boolean hasResourceFields) {
		return hasResourceFields ? LOGGED_USER_IDENTIFICATION : DictHomologKeys.PROXY_EMAIL_CPF;
	}

	protected boolean hasLoggedUser() {
		return true;
	}

	protected boolean hasRiskSignals() {
		return true;
	}

	protected abstract String getExpectedExpirationDateTime();
	protected abstract String getExpectedMaxVariableAmount();
}
