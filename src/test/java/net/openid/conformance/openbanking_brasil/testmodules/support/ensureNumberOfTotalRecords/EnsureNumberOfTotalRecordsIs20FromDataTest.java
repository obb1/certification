package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public class EnsureNumberOfTotalRecordsIs20FromDataTest extends AbstractEnsureNumberOfTotalRecordsFromDataTest{

	@Override
	protected int getTotalRecordsComparisonAmount() {
		return 20;
	}

	@Override
	protected TotalRecordsComparisonOperator getComparisonMethod() {
		return TotalRecordsComparisonOperator.EQUAL;
	}

	@Override
	protected AbstractEnsureNumberOfTotalRecordsFromData getCondition() {
		return new EnsureNumberOfTotalRecordsIs20FromData();
	}
}
