package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import static org.junit.jupiter.api.Assertions.*;

public class EnsureNumberOfTotalRecordsIs25FromDataTest extends AbstractEnsureNumberOfTotalRecordsFromDataTest{

	@Override
	protected int getTotalRecordsComparisonAmount() {
		return 25;
	}

	@Override
	protected TotalRecordsComparisonOperator getComparisonMethod() {
		return TotalRecordsComparisonOperator.EQUAL;
	}

	@Override
	protected AbstractEnsureNumberOfTotalRecordsFromData getCondition() {
		return new EnsureNumberOfTotalRecordsIs25FromData();
	}
}
