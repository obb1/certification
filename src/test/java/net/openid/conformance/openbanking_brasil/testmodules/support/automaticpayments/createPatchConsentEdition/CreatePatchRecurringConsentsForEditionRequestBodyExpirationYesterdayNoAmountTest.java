package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createPatchConsentEdition;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyExpirationYesterdayNoAmount;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class CreatePatchRecurringConsentsForEditionRequestBodyExpirationYesterdayNoAmountTest extends AbstractCreatePatchRecurringConsentsForEditionRequestBodyTest {

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody condition() {
		return new CreatePatchRecurringConsentsForEditionRequestBodyExpirationYesterdayNoAmount();
	}

	@Override
	protected String getExpectedExpirationDateTime() {
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = currentDateTime.minusDays(1);
		return dateTime.format(DATE_TIME_FORMATTER);
	}

	@Override
	protected String getExpectedMaxVariableAmount() {
		return null;
	}
}
