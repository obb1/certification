package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.paymentReference;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForMensalPayment;
import org.junit.Test;

public class SetPaymentReferenceForMensalPaymentTest extends AbstractSetPaymentReferenceTest {

	@Override
	protected AbstractSetPaymentReference condition() {
		return new SetPaymentReferenceForMensalPayment();
	}

	@Test
	public void happyPathTest() {
		for (int i = 1; i <= 12; i++) {
			happyPathTest(String.format("2024-%02d-01", i), String.format("M%02d-2024", i));
		}
	}
}
