package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.checkCurrentTime;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.checkCurrentTime.AbstractCheckCurrentTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.checkCurrentTime.CheckCurrentTimeAfter12PM;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.checkCurrentTime.CheckCurrentTimeBefore12AM;
import org.junit.Test;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertTrue;

public class AbstractCheckCurrentTimeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void test() {
		ZoneId timeZone = ZoneId.of("America/Sao_Paulo");
		ZonedDateTime now = ZonedDateTime.now(timeZone);
		LocalTime currentTime = now.toLocalTime();

		LocalTime midnight = LocalTime.of(0, 0);
		LocalTime almostMidday = LocalTime.of(11, 59, 59);

		boolean isBeforeMidday = currentTime.isAfter(midnight) && currentTime.isBefore(almostMidday);

		AbstractCheckCurrentTime passingCondition = isBeforeMidday
			? new CheckCurrentTimeBefore12AM()
			: new CheckCurrentTimeAfter12PM();

		AbstractCheckCurrentTime failingCondition = isBeforeMidday
			? new CheckCurrentTimeAfter12PM()
			: new CheckCurrentTimeBefore12AM();

		run(passingCondition);

		ConditionError error = runAndFail(failingCondition);
		assertTrue(error.getMessage().contains("Current time is not inside the accepted interval"));
	}
}
