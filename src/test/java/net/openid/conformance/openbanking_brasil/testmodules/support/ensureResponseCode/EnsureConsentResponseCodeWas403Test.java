package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseCodeWas403Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureConsentResponseCodeWas403();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.FORBIDDEN.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}
}
