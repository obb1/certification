package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureResourceResponseCodeWas400Or422Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureResourceResponseCodeWas400or422();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.OK.value();
	}
}
