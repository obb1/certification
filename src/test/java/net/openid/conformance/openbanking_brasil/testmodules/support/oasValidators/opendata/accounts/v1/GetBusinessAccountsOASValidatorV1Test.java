package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.accounts.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GetBusinessAccountsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/accounts/v1/GetBusinessAccounts200Response.json")
	public void happyPath() {
		run(new GetBusinessAccountsOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/opendata/accounts/v1/GetBusinessAccounts200ResponseMissingServiceBundles.json")
	public void unhappyPathMissingServiceBundles() {
		unhappyPath("serviceBundles", "type is [CONTA_DEPOSITO_A_VISTA, CONTA_POUPANCA]");
	}

	@Test
	@UseResurce("jsonResponses/opendata/accounts/v1/GetBusinessAccounts200ResponseMissingOpeningClosingChannelsAdditionalInfo.json")
	public void unhappyPathMissingOpeningClosingChannelsAdditionalInfo() {
		unhappyPath("openingClosingChannelsAdditionalInfo", "openingClosingChannels has OUTROS in it");
	}

	@Test
	@UseResurce("jsonResponses/opendata/accounts/v1/GetBusinessAccounts200ResponseMissingSavingAccount.json")
	public void unhappyPathMissingSavingAccount() {
		unhappyPath("savingAccount", "type is CONTA_POUPANCA");
	}

	protected void unhappyPath(String requiredField, String when) {
		ConditionError error = runAndFail(new GetBusinessAccountsOASValidatorV1());
		assertTrue(error.getMessage().contains(String.format("%s is required when %s", requiredField, when)));
	}
}
