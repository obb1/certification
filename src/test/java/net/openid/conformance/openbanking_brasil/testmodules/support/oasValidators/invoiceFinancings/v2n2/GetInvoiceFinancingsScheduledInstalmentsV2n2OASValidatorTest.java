package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


public class GetInvoiceFinancingsScheduledInstalmentsV2n2OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/instalments.json")
	public void happyPath() {
		run(new GetInvoiceFinancingsScheduledInstalmentsV2n2OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/instalmentsNoTotalNumberOfInstalments.json")
	public void unhappyPathNoTotalNumberOfInstalments() {
		ConditionError e = runAndFail(new GetInvoiceFinancingsScheduledInstalmentsV2n2OASValidator());
		assertThat(e.getMessage(), containsString("totalNumberOfInstalments is required when typeNumberOfInstalments is [DIA, SEMANA, MES, ANO]"));
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/instalmentsNoTotalNumberOfInstalments2.json")
	public void unhappyPathNoTotalNumberOfInstalments2() {
		ConditionError e = runAndFail(new GetInvoiceFinancingsScheduledInstalmentsV2n2OASValidator());
		assertThat(e.getMessage(), containsString("totalNumberOfInstalments is required when typeContractRemaining is not SEM_PRAZO_REMANESCENTE"));

	}

}

