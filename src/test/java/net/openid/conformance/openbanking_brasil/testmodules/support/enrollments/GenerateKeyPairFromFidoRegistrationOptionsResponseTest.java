package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.RSAKey;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateFidoRegistrationRequestBodyToRequestEntityClaims;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import java.security.KeyPair;
import java.security.Security;
import java.text.ParseException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.notNullValue;

public class GenerateKeyPairFromFidoRegistrationOptionsResponseTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void evaluateES256KeyPairCreation() {
		run(new GenerateKeyPairFromFidoRegistrationOptionsResponse());
		JsonObject fidoKeysJwk = environment.getObject("fido_keys_jwk");
		assertThat(fidoKeysJwk, notNullValue());
		try {
			KeyPair keyPair = ECKey.parse(fidoKeysJwk.toString()).toKeyPair();
			assertThat(keyPair.getPublic(), notNullValue());
			assertThat(keyPair.getPrivate(), notNullValue());

		} catch (ParseException e) {
			throw new AssertionError("Could not parse ECKey", e);
		} catch (JOSEException e) {
			throw new AssertionError("Could not create key pair from ECKey", e);
		}
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsRSAKey.json")
	public void evaluateRS256KeyPairCreation() {
		run(new GenerateKeyPairFromFidoRegistrationOptionsResponse());
		JsonObject fidoKeysJwk = environment.getObject("fido_keys_jwk");
		assertThat(fidoKeysJwk, notNullValue());
		try {
			KeyPair keyPair = RSAKey.parse(fidoKeysJwk.toString()).toKeyPair();
			assertThat(keyPair.getPublic(), notNullValue());
			assertThat(keyPair.getPrivate(), notNullValue());

		} catch (ParseException e) {
			throw new AssertionError("Could not parse ECKey", e);
		} catch (JOSEException e) {
			throw new AssertionError("Could not create key pair from ECKey", e);
		}
	}

	@Test
	public void testUnhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}

	@Test
	public void unhappyPathInvalidJwt() {
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObjectBuilder().addField("body", "eya1.b2.c3").build());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Error parsing JWT response"));
	}

	@Test
	public void unhappyPathNoPubKeyCredParams() {
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObjectBuilder().addField("body", "{}").build());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not find data.pubKeyCredParams JSON array in the fido-registration-options response"));
	}

	@Test
	public void unhappyPathEmptyPubKeyCredParams() {
		JsonObject data = new JsonObject();
		data.add("pubKeyCredParams",new JsonArray());
		JsonObject body = new JsonObject();
		body.add("data",data);
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObjectBuilder().addField("body", body.toString()).build());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("pubKeyCredParams JSON array is empty"));
	}

	@Test
	public void unhappyPathParamsNoAlg() {
		JsonObject pubKeyCredParam = new JsonObject();
		pubKeyCredParam.addProperty("type","public-key");
		JsonArray pubKeyCredParams = new JsonArray();
		pubKeyCredParams.add(pubKeyCredParam);
		JsonObject data = new JsonObject();
		data.add("pubKeyCredParams",pubKeyCredParams);
		JsonObject body = new JsonObject();
		body.add("data",data);
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObjectBuilder().addField("body", body.toString()).build());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not find alg in the pubKeyCredParam"));
	}

	@Test
	public void unhappyPathParamsNoType() {
		JsonObject pubKeyCredParam = new JsonObject();
		pubKeyCredParam.addProperty("alg",2);
		JsonArray pubKeyCredParams = new JsonArray();
		pubKeyCredParams.add(pubKeyCredParam);
		JsonObject data = new JsonObject();
		data.add("pubKeyCredParams",pubKeyCredParams);
		JsonObject body = new JsonObject();
		body.add("data",data);
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObjectBuilder().addField("body", body.toString()).build());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not find type in the pubKeyCredParam"));
	}

	@Test
	public void unhappyPathWrongAlg() {
		JsonObject pubKeyCredParam = new JsonObject();
		pubKeyCredParam.addProperty("alg",2);
		pubKeyCredParam.addProperty("type","public-key");
		JsonArray pubKeyCredParams = new JsonArray();
		pubKeyCredParams.add(pubKeyCredParam);
		JsonObject data = new JsonObject();
		data.add("pubKeyCredParams",pubKeyCredParams);
		JsonObject body = new JsonObject();
		body.add("data",data);
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObjectBuilder().addField("body", body.toString()).build());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not find supported public key algorithm in the pubKeyCredParams array"));
	}

	@Test
	public void unhappyPathWrongType() {
		JsonObject pubKeyCredParam = new JsonObject();
		pubKeyCredParam.addProperty("alg",-8);
		pubKeyCredParam.addProperty("type","private-key");
		JsonArray pubKeyCredParams = new JsonArray();
		pubKeyCredParams.add(pubKeyCredParam);
		JsonObject data = new JsonObject();
		data.add("pubKeyCredParams",pubKeyCredParams);
		JsonObject body = new JsonObject();
		body.add("data",data);
		environment.putObject(CreateFidoRegistrationRequestBodyToRequestEntityClaims.RESPONSE_ENV_KEY, new JsonObjectBuilder().addField("body", body.toString()).build());
		GenerateKeyPairFromFidoRegistrationOptionsResponse condition = new GenerateKeyPairFromFidoRegistrationOptionsResponse();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not find supported public key algorithm in the pubKeyCredParams array"));
	}

}
