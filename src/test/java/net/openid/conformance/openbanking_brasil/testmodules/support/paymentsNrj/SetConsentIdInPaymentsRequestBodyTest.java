package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SetConsentIdInPaymentsRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_ID = "urn:example:123abc";

	@Test
	@UseResurce(value = "jsonRequests/payments/payments/paymentWithDataObject.json", key = "resource", path = "brazilPixPayment")
	public void happyPathTestJsonObject() {
		environment.putString("consent_id", CONSENT_ID);
		run(new SetConsentIdInPaymentsRequestBody());
		JsonObject data = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonObject();
		assertEquals(CONSENT_ID, OIDFJSON.getString(data.get("consentId")));
	}

	@Test
	@UseResurce(value = "jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key = "resource", path = "brazilPixPayment")
	public void happyPathTestJsonArray() {
		environment.putString("consent_id", CONSENT_ID);
		run(new SetConsentIdInPaymentsRequestBody());
		JsonArray data = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray();
		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject();
			assertEquals(CONSENT_ID, OIDFJSON.getString(payment.get("consentId")));
		}
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResource() {
		environment.putString("consent_id", CONSENT_ID);
		run(new SetConsentIdInPaymentsRequestBody());
	}

	@Test(expected = AssertionError.class)
	@UseResurce(value = "jsonRequests/payments/payments/paymentWithDataObject.json", key = "resource", path = "brazilPixPayment")
	public void unhappyPathTestMissingConsentId() {
		run(new SetConsentIdInPaymentsRequestBody());
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putString("consent_id", CONSENT_ID);
		environment.putObject("resource", new JsonObject());
		unhappyPathTest("Could not find payment data in the environment");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putString("consent_id", CONSENT_ID);
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest("Could not find payment data in the environment");
	}

	@Test
	public void unhappyPathTestInvalidObject() {
		environment.putString("consent_id", CONSENT_ID);
		environment.putString("resource", "brazilPixPayment.data", "example");
		unhappyPathTest("Payment is not of a valid type");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new SetConsentIdInPaymentsRequestBody());
		assertTrue(error.getMessage().contains(message));
	}
}
