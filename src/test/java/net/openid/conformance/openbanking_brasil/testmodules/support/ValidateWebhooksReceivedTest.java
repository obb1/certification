package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooksOptionalAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooksReceivedThree;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringPaymentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringPaymentWebhooksReceivedThree;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ValidateEnrollmentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AbstractValidateWebhooksReceived;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.ValidatePaymentConsentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.ValidatePaymentConsentWebhooksOptionalAmount;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.ValidatePaymentWebhooks;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateWebhooksReceivedTest extends AbstractJsonResponseConditionUnitTest {

	private static final String INTERACTION_ID = "123456789";
	private static final String CONSENT_ID = "urn:abc123:def456";
	private static final String PAYMENT_ID = "abcdef123456";
	private static final String ENROLLMENT_ID = "urn:abc123:def456";
	private static final String BASE_PATH = "open-banking/webhook/v1/";
	private static final String CONSENT_PATH = BASE_PATH + "payments/v4/consents/" + CONSENT_ID;
	private static final String PAYMENT_PATH = BASE_PATH + "payments/v4/pix/payments/" + PAYMENT_ID;
	private static final String ENROLLMENT_PATH = BASE_PATH + "enrollments/v1/enrollments/" + ENROLLMENT_ID;
	private static final String RECURRING_CONSENT_PATH = BASE_PATH + "automatic-payments/v1/recurring-consents/" + CONSENT_ID;
	private static final String RECURRING_PAYMENT_PATH = BASE_PATH + "automatic-payments/v1/pix/recurring-payments/" + PAYMENT_ID;

	@Test
	public void happyPathTestConsent() {
		happyPathTest(new ValidatePaymentConsentWebhooks(), "consent", "consent_id", CONSENT_ID, CONSENT_PATH);
	}

	@Test
	public void happyPathTestConsentOptionalAmount() {
		happyPathTestOptional(new ValidatePaymentConsentWebhooksOptionalAmount(), "consent", "consent_id", CONSENT_ID, CONSENT_PATH);
	}

	@Test
	public void happyPathTestPayment() {
		happyPathTest(new ValidatePaymentWebhooks(), "payment", "payment_id", PAYMENT_ID, PAYMENT_PATH);
	}

	@Test
	public void happyPathTestEnrollment() {
		happyPathTest(new ValidateEnrollmentWebhooks(), "enrollment", "enrollment_id", ENROLLMENT_ID, ENROLLMENT_PATH);
	}

	@Test
	public void happyPathTestRecurringConsent() {
		happyPathTest(new ValidateRecurringConsentWebhooks(), "recurring_consent", "consent_id", CONSENT_ID, RECURRING_CONSENT_PATH);
	}

	@Test
	public void happyPathTestRecurringConsentOptionalAmount() {
		happyPathTestOptional(new ValidateRecurringConsentWebhooksOptionalAmount(), "recurring_consent", "consent_id", CONSENT_ID, RECURRING_CONSENT_PATH);
	}

	@Test
	public void happyPathTestRecurringConsentReceivedThree() {
		happyPathTestThree(new ValidateRecurringConsentWebhooksReceivedThree(), "recurring_consent", "consent_id", CONSENT_ID, RECURRING_CONSENT_PATH);
	}

	@Test
	public void happyPathTestRecurringPayment() {
		happyPathTest(new ValidateRecurringPaymentWebhooks(), "recurring_payment", "payment_id", PAYMENT_ID, RECURRING_PAYMENT_PATH);
	}

	@Test
	public void happyPathTestRecurringPaymentReceivedThree() {
		happyPathTestThree(new ValidateRecurringPaymentWebhooksReceivedThree(), "recurring_payment", "payment_id", PAYMENT_ID, RECURRING_PAYMENT_PATH);
	}

	@Test
	public void unhappyPathTestMissingId() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		environment.removeNativeValue("consent_id");
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "consent_id not found");
	}

	@Test
	public void unhappyPathTestWebhooksObject() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		environment.removeObject("webhooks_received_consent");
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Webhooks not found");
	}

	@Test
	public void unhappyPathTestWrongWebhooksAmount() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonArray webhooks = environment.getElementFromObject("webhooks_received_consent", "webhooks").getAsJsonArray();
		webhooks.add(new JsonObject());
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Expected 1 webhooks, but received 2");
	}

	@Test
	public void unhappyPathTestWebhookMissingInteractionId() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject headers = getWebhook("consent").getAsJsonObject("headers");
		headers.remove("x-webhook-interaction-id");
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Headers returned in webhook response do not contain x-webhook-interaction-id");
	}

	@Test
	public void unhappyPathTestWebhookMissingContentType() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject headers = getWebhook("consent").getAsJsonObject("headers");
		headers.remove("content-type");
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Headers returned in webhook response do not contain content-type");
	}

	@Test
	public void unhappyPathTestWebhookContentTypeIsNotJson() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject headers = getWebhook("consent").getAsJsonObject("headers");
		headers.addProperty("content-type", "application/jwt");
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "content-type not found or not equal to application/json");
	}

	@Test
	public void unhappyPathTestWebhookMissingBody() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		webhook.remove("body");
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "No webhook body found in the webhook request");
	}

	@Test
	public void unhappyPathTestWebhookMissingData() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		JsonObject body = getBody("consent");
		body.remove("data");
		webhook.addProperty("body", body.toString());
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Webhook response body does not contain data field");
	}

	@Test
	public void unhappyPathTestWebhookMissingTimeStamp() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		JsonObject body = getBody("consent");
		JsonObject data = body.getAsJsonObject("data");
		data.remove("timestamp");
		webhook.addProperty("body", body.toString());
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Webhook response body does not contain timestamp field");
	}

	@Test
	public void unhappyPathTestWebhookTimeStampTooEarly() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		JsonObject body = getBody("consent");
		JsonObject data = body.getAsJsonObject("data");
		data.addProperty("timestamp", Instant.now().minusSeconds(120).toString());
		webhook.addProperty("body", body.toString());
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Webhook timestamp is not within the time of the POST request and current time");
	}

	@Test
	public void happyPathTestWebhookTimeStampEqualToStart() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		JsonObject body = getBody("consent");
		JsonObject data = body.getAsJsonObject("data");
		data.addProperty("timestamp", Instant.now().minusSeconds(10).truncatedTo(ChronoUnit.SECONDS).toString());
		webhook.addProperty("body", body.toString());
		run(new ValidatePaymentConsentWebhooks());
	}
	@Test
	public void happyPathTestWebhookTimeStampEqualToNow() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		JsonObject body = getBody("consent");
		JsonObject data = body.getAsJsonObject("data");
		data.addProperty("timestamp", Instant.now().toString());
		webhook.addProperty("body", body.toString());
		run(new ValidatePaymentConsentWebhooks());
	}

	@Test
	public void unhappyPathTestWebhookTimeStampTooLate() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		JsonObject body = getBody("consent");
		JsonObject data = body.getAsJsonObject("data");
		data.addProperty("timestamp", Instant.now().plusSeconds(120).toString());
		webhook.addProperty("body", body.toString());
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Webhook timestamp is not within the time of the POST request and current time");
	}

	@Test
	public void unhappyPathTestConsentIdNotMatching() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		environment.putString("consent_id", CONSENT_ID + "x");
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Consent ID in webhook path does not match the consent ID in the request");
	}

	@Test
	public void unhappyPathTestConsentPathNotMatching() {
		prepareEnv("consent", "consent_id", CONSENT_ID, CONSENT_PATH);
		JsonObject webhook = getWebhook("consent");
		webhook.addProperty("path", PAYMENT_PATH);
		unhappyPathTest(new ValidatePaymentConsentWebhooks(), "Consent ID in webhook path does not match the consent ID in the request");
	}

	@Test
	public void unhappyPathTestPaymentIdNotMatching() {
		prepareEnv("payment", "payment_id", PAYMENT_ID, PAYMENT_PATH);
		environment.putString("payment_id", PAYMENT_ID + "x");
		unhappyPathTest(new ValidatePaymentWebhooks(), "Payment ID in webhook path does not match the payment ID in the request");
	}

	@Test
	public void unhappyPathTestPaymentPathNotMatching() {
		prepareEnv("payment", "payment_id", PAYMENT_ID, PAYMENT_PATH);
		JsonObject webhook = getWebhook("payment");
		webhook.addProperty("path", CONSENT_PATH);
		unhappyPathTest(new ValidatePaymentWebhooks(), "Payment ID in webhook path does not match the payment ID in the request");
	}

	@Test
	public void unhappyPathTestEnrollmentIdNotMatching() {
		prepareEnv("enrollment", "enrollment_id", ENROLLMENT_ID, ENROLLMENT_PATH);
		environment.putString("enrollment_id", ENROLLMENT_ID + "x");
		unhappyPathTest(new ValidateEnrollmentWebhooks(), "Enrollment ID in webhook path does not match the enrollment ID in the request");
	}

	@Test
	public void unhappyPathTestEnrollmentPathNotMatching() {
		prepareEnv("enrollment", "enrollment_id", ENROLLMENT_ID, ENROLLMENT_PATH);
		JsonObject webhook = getWebhook("enrollment");
		webhook.addProperty("path", CONSENT_PATH);
		unhappyPathTest(new ValidateEnrollmentWebhooks(), "Enrollment ID in webhook path does not match the enrollment ID in the request");
	}

	@Test
	public void unhappyPathTestRecurringConsentIdNotMatching() {
		prepareEnv("recurring_consent", "consent_id", CONSENT_ID, RECURRING_CONSENT_PATH);
		environment.putString("consent_id", CONSENT_ID + "x");
		unhappyPathTest(new ValidateRecurringConsentWebhooks(), "Consent ID in webhook path does not match the consent ID in the request");
	}

	@Test
	public void unhappyPathTestRecurringConsentPathNotMatching() {
		prepareEnv("recurring_consent", "consent_id", CONSENT_ID, RECURRING_CONSENT_PATH);
		JsonObject webhook = getWebhook("recurring_consent");
		webhook.addProperty("path", RECURRING_PAYMENT_PATH);
		unhappyPathTest(new ValidateRecurringConsentWebhooks(), "Consent ID in webhook path does not match the consent ID in the request");
	}

	@Test
	public void unhappyPathTestRecurringPaymentIdNotMatching() {
		prepareEnv("recurring_payment", "payment_id", PAYMENT_ID, RECURRING_PAYMENT_PATH);
		environment.putString("payment_id", PAYMENT_ID + "x");
		unhappyPathTest(new ValidateRecurringPaymentWebhooks(), "Payment ID in webhook path does not match the payment ID in the request");
	}

	@Test
	public void unhappyPathTestRecurringPaymentPathNotMatching() {
		prepareEnv("recurring_payment", "payment_id", PAYMENT_ID, RECURRING_PAYMENT_PATH);
		JsonObject webhook = getWebhook("recurring_payment");
		webhook.addProperty("path", RECURRING_CONSENT_PATH);
		unhappyPathTest(new ValidateRecurringPaymentWebhooks(), "Payment ID in webhook path does not match the payment ID in the request");
	}

	private JsonObject getWebhook(String type) {
		return environment.getElementFromObject("webhooks_received_" + type, "webhooks")
			.getAsJsonArray().get(0).getAsJsonObject();
	}

	private JsonObject getBody(String type) {
		String bodyString = OIDFJSON.getString(getWebhook(type).get("body"));
		return JsonParser.parseString(bodyString).getAsJsonObject();
	}

	private void prepareEnv(String type, String idPath, String id, String path) {
		environment.putString(String.format("time_of_%s_request", type), Instant.now().minusSeconds(10).toString());
		environment.putString(idPath, id);

		JsonObject body = new JsonObjectBuilder()
			.addField("data.timestamp", Instant.now().minusSeconds(5).toString()).build();

		JsonObject webhook = new JsonObjectBuilder()
			.addFields("headers", Map.of(
				"x-webhook-interaction-id", INTERACTION_ID,
				"content-type", "application/json"
			))
			.addField("body", body.toString())
			.addField("path", path)
			.addField("type", type)
			.build();


		JsonArray webhooks = new JsonArray();
		webhooks.add(webhook);
		JsonObject webhooksObj = new JsonObject();
		webhooksObj.add("webhooks", webhooks);
		environment.putObject("webhooks_received_" + type, webhooksObj);
	}

	private void prepareEnvOptional(String type, String idPath, String id, String path) {
		environment.putString(String.format("time_of_%s_request", type), Instant.now().minusSeconds(10).toString());
		environment.putString(idPath, id);

		JsonObject body = new JsonObjectBuilder()
			.addField("data.timestamp", Instant.now().minusSeconds(5).toString()).build();

		JsonObject webhook1 = new JsonObjectBuilder()
			.addFields("headers", Map.of(
				"x-webhook-interaction-id", INTERACTION_ID,
				"content-type", "application/json"
			))
			.addField("body", body.toString())
			.addField("path", path)
			.addField("type", type)
			.build();

		JsonObject webhook2 = new JsonObjectBuilder()
			.addFields("headers", Map.of(
				"x-webhook-interaction-id", INTERACTION_ID,
				"content-type", "application/json"
			))
			.addField("body", body.toString())
			.addField("path", path)
			.addField("type", type)
			.build();

		JsonArray webhooks = new JsonArray();
		webhooks.add(webhook1);
		webhooks.add(webhook2);
		JsonObject webhooksObj = new JsonObject();
		webhooksObj.add("webhooks", webhooks);
		environment.putObject("webhooks_received_" + type, webhooksObj);
	}

	private void prepareEnvThree(String type, String idPath, String id, String path) {
		environment.putString(String.format("time_of_%s_request", type), Instant.now().minusSeconds(10).toString());
		environment.putString(idPath, id);

		JsonObject body = new JsonObjectBuilder()
			.addField("data.timestamp", Instant.now().minusSeconds(5).toString()).build();

		JsonObject webhook = new JsonObjectBuilder()
			.addFields("headers", Map.of(
				"x-webhook-interaction-id", INTERACTION_ID,
				"content-type", "application/json"
			))
			.addField("body", body.toString())
			.addField("path", path)
			.addField("type", type)
			.build();

		JsonArray webhooks = new JsonArray();
		webhooks.add(webhook);
		webhooks.add(webhook.deepCopy());
		webhooks.add(webhook.deepCopy());
		JsonObject webhooksObj = new JsonObject();
		webhooksObj.add("webhooks", webhooks);
		environment.putObject("webhooks_received_" + type, webhooksObj);
	}

	private void happyPathTest(AbstractValidateWebhooksReceived condition, String type, String idPath, String id, String path) {
		prepareEnv(type, idPath, id, path);
		run(condition);
	}

	private void happyPathTestOptional(AbstractValidateWebhooksReceived condition, String type, String idPath, String id, String path) {
		prepareEnvOptional(type, idPath, id, path);
		run(condition);
	}

	private void happyPathTestThree(AbstractValidateWebhooksReceived condition, String type, String idPath, String id, String path) {
		prepareEnvThree(type, idPath, id, path);
		run(condition);
	}

	private void unhappyPathTest(AbstractValidateWebhooksReceived condition, String expectedMessage) {
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
