package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.setDate;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToUserDefinedDate;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;

public class EditRecurringPaymentsBodyToSetDateToUserDefinedDateTest extends AbstractEditRecurringPaymentsBodyToSetDateTest {

	private static final String DATE = "2024-10-03";

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate condition() {
		return new EditRecurringPaymentsBodyToSetDateToUserDefinedDate();
	}

	@Override
	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void happyPathTest() {
		environment.putString("resource", "recurringPaymentDate", DATE);
		super.happyPathTest();
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void unhappyPathTestInvalidDate() {
		environment.putString("resource", "recurringPaymentDate", "2024-13-03");
		unhappyPathTest("The value defined for resource.recurringPaymentDate is not valid");

		environment.putString("resource", "recurringPaymentDate", "2024-10-32");
		unhappyPathTest("The value defined for resource.recurringPaymentDate is not valid");

		environment.putString("resource", "recurringPaymentDate", "03-10-2024");
		unhappyPathTest("The value defined for resource.recurringPaymentDate is not valid");

		environment.putString("resource", "recurringPaymentDate", "24-10-03");
		unhappyPathTest("The value defined for resource.recurringPaymentDate is not valid");
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.parse(DATE);
	}
}
