package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureResourceResponseCodeWas200Or406Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureResourceResponseCodeWas200or406();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.NOT_ACCEPTABLE.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}

}
