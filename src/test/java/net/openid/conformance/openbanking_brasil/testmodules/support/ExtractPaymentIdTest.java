package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class ExtractPaymentIdTest extends AbstractJsonResponseConditionUnitTest {
	private static final String BASE_PATH = "jsonResponses/paymentInitiation/pixByPayments/";
	private static final String expectedPaymentId = "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl";

	@Before public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce(BASE_PATH + "v3/PostPaymentsPixValidatorV3.json")
	public void happyPathSingleTest() {
		ExtractPaymentId condition = new ExtractPaymentId();
		run(condition);
		String paymentId = environment.getString("payment_id");
		assertEquals(expectedPaymentId, paymentId);
	}

	@Test
	@UseResurce(BASE_PATH + "v4/standardPostPaymentsV4ResponseWith2Payments.json")
	public void happyPathArrayTest() {
		ExtractPaymentId condition = new ExtractPaymentId();
		run(condition);
		String paymentId = environment.getString("payment_id");
		assertEquals(expectedPaymentId, paymentId);
	}

	@Test
	@UseResurce(BASE_PATH + "v4/postPaymentsV4ResponseMissingPaymentId.json")
	public void unhappyPathTestMissingPaymentId() {
		ExtractPaymentId condition = new ExtractPaymentId();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "Couldn't find payment ID in response";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
