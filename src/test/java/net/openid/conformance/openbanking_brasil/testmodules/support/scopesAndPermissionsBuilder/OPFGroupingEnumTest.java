package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFGroupingEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.ScopesEnum;
import org.junit.Test;

import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class OPFGroupingEnumTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void testGetPermissions() {
		assertEquals("PERSONAL_REGISTRATION_DATA",
			EnumSet.of(OPFPermissionsEnum.CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.PERSONAL_REGISTRATION_DATA.getPermissions());

		assertEquals("PERSONAL_ADDITIONAL_INFO",
			EnumSet.of(OPFPermissionsEnum.CUSTOMERS_PERSONAL_ADITTIONALINFO_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.PERSONAL_ADDITIONAL_INFO.getPermissions());

		assertEquals("BUSINESS_REGISTRATION_DATA",
			EnumSet.of(OPFPermissionsEnum.CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.BUSINESS_REGISTRATION_DATA.getPermissions());

		assertEquals("BUSINESS_ADDITIONAL_INFO",
			EnumSet.of(OPFPermissionsEnum.CUSTOMERS_BUSINESS_ADITTIONALINFO_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.BUSINESS_ADDITIONAL_INFO.getPermissions());

		assertEquals("ACCOUNTS_BALANCES",
			EnumSet.of(OPFPermissionsEnum.ACCOUNTS_READ, OPFPermissionsEnum.ACCOUNTS_BALANCES_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.ACCOUNTS_BALANCES.getPermissions());

		assertEquals("ACCOUNTS_LIMITS",
			EnumSet.of(OPFPermissionsEnum.ACCOUNTS_READ, OPFPermissionsEnum.ACCOUNTS_OVERDRAFT_LIMITS_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.ACCOUNTS_LIMITS.getPermissions());

		assertEquals("ACCOUNTS_TRANSACTIONS",
			EnumSet.of(OPFPermissionsEnum.ACCOUNTS_READ, OPFPermissionsEnum.ACCOUNTS_TRANSACTIONS_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.ACCOUNTS_TRANSACTIONS.getPermissions());

		assertEquals("CREDIT_CARDS_LIMITS",
			EnumSet.of(OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ, OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_LIMITS_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.CREDIT_CARDS_LIMITS.getPermissions());

		assertEquals("CREDIT_CARDS_TRANSACTIONS",
			EnumSet.of(OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ, OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.CREDIT_CARDS_TRANSACTIONS.getPermissions());

		assertEquals("CREDIT_CARDS_BILLS",
			EnumSet.of(OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_READ, OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_BILLS_READ, OPFPermissionsEnum.CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.CREDIT_CARDS_BILLS.getPermissions());

		assertEquals("CREDIT_OPERATIONS",
			EnumSet.of(OPFPermissionsEnum.LOANS_READ, OPFPermissionsEnum.LOANS_WARRANTIES_READ, OPFPermissionsEnum.LOANS_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.LOANS_PAYMENTS_READ,
				OPFPermissionsEnum.FINANCINGS_READ, OPFPermissionsEnum.FINANCINGS_WARRANTIES_READ, OPFPermissionsEnum.FINANCINGS_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.FINANCINGS_PAYMENTS_READ,
				OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ,
				OPFPermissionsEnum.INVOICE_FINANCINGS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_WARRANTIES_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ, OPFPermissionsEnum.INVOICE_FINANCINGS_PAYMENTS_READ,
				OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.CREDIT_OPERATIONS.getPermissions());

		assertEquals("Testing INVESTMENTS",
			EnumSet.of(OPFPermissionsEnum.BANK_FIXED_INCOMES_READ, OPFPermissionsEnum.CREDIT_FIXED_INCOMES_READ, OPFPermissionsEnum.FUNDS_READ, OPFPermissionsEnum.VARIABLE_INCOMES_READ, OPFPermissionsEnum.TREASURE_TITLES_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.INVESTMENTS.getPermissions());

		assertEquals("Testing EXCHANGES",
			EnumSet.of(OPFPermissionsEnum.EXCHANGES_READ, OPFPermissionsEnum.RESOURCES_READ),
			OPFGroupingEnum.EXCHANGES.getPermissions());
	     }

	@Test
	public void testGetIndividualScopes() {
		assertScopes(OPFGroupingEnum.PERSONAL_REGISTRATION_DATA, Set.of(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.PERSONAL_ADDITIONAL_INFO, Set.of(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.BUSINESS_REGISTRATION_DATA, Set.of(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.BUSINESS_ADDITIONAL_INFO, Set.of(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.ACCOUNTS_BALANCES, Set.of(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.ACCOUNTS_LIMITS, Set.of(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.ACCOUNTS_TRANSACTIONS, Set.of(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.CREDIT_CARDS_LIMITS, Set.of(OPFScopesEnum.CREDIT_CARDS_ACCOUNTS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.CREDIT_CARDS_TRANSACTIONS, Set.of(OPFScopesEnum.CREDIT_CARDS_ACCOUNTS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.CREDIT_CARDS_BILLS, Set.of(OPFScopesEnum.CREDIT_CARDS_ACCOUNTS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.CREDIT_OPERATIONS, Set.of(OPFScopesEnum.LOANS, OPFScopesEnum.FINANCINGS, OPFScopesEnum.UNARRANGED_ACCOUNTS_OVERDRAFT, OPFScopesEnum.INVOICE_FINANCINGS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.INVESTMENTS, Set.of(OPFScopesEnum.BANK_FIXED_INCOMES, OPFScopesEnum.CREDIT_FIXED_INCOMES, OPFScopesEnum.VARIABLE_INCOMES, OPFScopesEnum.TREASURE_TITLES, OPFScopesEnum.FUNDS, OPFScopesEnum.RESOURCES));
		assertScopes(OPFGroupingEnum.EXCHANGES, Set.of(OPFScopesEnum.EXCHANGES));
	}

	private void assertScopes(OPFGroupingEnum groupingEnum, Set<OPFScopesEnum> expectedScopes) {
		Set<ScopesEnum> actualScopes = groupingEnum.getScopes();
		assertEquals(expectedScopes, actualScopes);
	}



	}
