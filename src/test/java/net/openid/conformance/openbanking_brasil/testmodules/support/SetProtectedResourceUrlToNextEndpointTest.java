package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SetProtectedResourceUrlToNextEndpointTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsMaxPageSizeMetaGood1.json")
	public void happyPath(){
		run(new SetProtectedResourceUrlToNextEndpoint());
		String expectedLink = OIDFJSON.getString(environment.getElementFromObject("resource_endpoint_response_full","body_json.links.next"));
		String actualLink = environment.getString("protected_resource_url");
		Assert.assertEquals(actualLink,expectedLink);
	}
	@Test
	public void unhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		SetProtectedResourceUrlToNextEndpoint condition = new SetProtectedResourceUrlToNextEndpoint();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}

	@Test
	public void unhappyPathUnparseable() {
		JsonObject resource = new JsonObject();
		resource.addProperty("body", "ey1.b.c");
		environment.putObject("resource_endpoint_response_full", resource);
		SetProtectedResourceUrlToNextEndpoint condition = new SetProtectedResourceUrlToNextEndpoint();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not parse body"));
	}

	@Test
	@UseResurce("jsonResponses/consent/createConsentResponse/v2/createConsentResponseMissingOptionalLinks.json")
	public void unhappyPathNoNextLink() {
		SetProtectedResourceUrlToNextEndpoint condition = new SetProtectedResourceUrlToNextEndpoint();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("'Next' link not found in the response."));
	}

}
