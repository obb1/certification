package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class BusinessIdentificationOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/legaEntityIdentificationV2/legalEntityIdentificationResponse-V2.1.0.json")
	public void testHappyPath() {
		BusinessIdentificationOASValidatorV2 cond = new BusinessIdentificationOASValidatorV2();
		run(cond);

		// person type constraint.
		JsonObject party = new JsonObject();
		party.addProperty("personType", "PESSOA_JURIDICA");
		JsonArray parties = new JsonArray();
		parties.add(party);
		JsonObject invalidPersonTypeData = new JsonObject();
		invalidPersonTypeData.add("parties", parties);

		party.addProperty("personType", "PESSOA_JURIDICA");
		party.remove("companyName");
		assertThrows(ConditionError.class, () -> cond.assertPersonTypeConstraint(invalidPersonTypeData));

		party.addProperty("personType", "PESSOA_NATURAL");
		party.remove("civilName");
		assertThrows(ConditionError.class, () -> cond.assertPersonTypeConstraint(invalidPersonTypeData));

		// Phone constraint.
		JsonObject phone = new JsonObject();
		phone.addProperty("type", "OUTRO");
		JsonArray phones = new JsonArray();
		phones.add(phone);
		JsonObject contacts = new JsonObject();
		contacts.add("phones", phones);
		JsonObject invalidPhoneData = new JsonObject();
		invalidPhoneData.add("contacts", contacts);
		assertThrows(ConditionError.class, () -> cond.assertPhonesConstraint(invalidPhoneData));
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/legaEntityIdentificationV2/legalEntityIdentificationInvalidResponse-V2.1.0.json")
	public void testUnhappyPath() {
		BusinessIdentificationOASValidatorV2 cond = new BusinessIdentificationOASValidatorV2();
		runAndFail(cond);
	}
}
