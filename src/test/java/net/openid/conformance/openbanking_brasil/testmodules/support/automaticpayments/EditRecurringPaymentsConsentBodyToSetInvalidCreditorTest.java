package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetInvalidCreditor;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToSetInvalidCreditorTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";
	private static final String OLD_CPF = "11111111111";
	private static final String OLD_CNPJ = "58764789000137";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithCreditorWithCpf.json", key=KEY, path=PATH)
	public void happyPathTestCPF() {
		EditRecurringPaymentsConsentBodyToSetInvalidCreditor condition = new EditRecurringPaymentsConsentBodyToSetInvalidCreditor();
		run(condition);

		String newIdentification = OIDFJSON.getString(
			environment.getElementFromObject(KEY, PATH+".data.creditors")
				.getAsJsonArray()
				.get(0)
				.getAsJsonObject()
				.get("cpfCnpj")
		);
		assertNotEquals(OLD_CPF, newIdentification);
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTestCNPJ() {
		EditRecurringPaymentsConsentBodyToSetInvalidCreditor condition = new EditRecurringPaymentsConsentBodyToSetInvalidCreditor();
		run(condition);

		String newIdentification = OIDFJSON.getString(
			environment.getElementFromObject(KEY, PATH+".data.creditors")
				.getAsJsonArray()
				.get(0)
				.getAsJsonObject()
				.get("cpfCnpj")
		);
		assertNotEquals(OLD_CNPJ, newIdentification);
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject("resource", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetInvalidCreditor condition = new EditRecurringPaymentsConsentBodyToSetInvalidCreditor();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find a creditor inside data.creditors in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetInvalidCreditor condition = new EditRecurringPaymentsConsentBodyToSetInvalidCreditor();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find a creditor inside data.creditors in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentMissingCreditors.json", key=KEY, path=PATH)
	public void unhappyPathTestMissingCreditors() {
		EditRecurringPaymentsConsentBodyToSetInvalidCreditor condition = new EditRecurringPaymentsConsentBodyToSetInvalidCreditor();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find a creditor inside data.creditors in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithCreditorMissingCpfCnpj.json", key=KEY, path=PATH)
	public void unhappyPathTestMissingCpfCnpj() {
		EditRecurringPaymentsConsentBodyToSetInvalidCreditor condition = new EditRecurringPaymentsConsentBodyToSetInvalidCreditor();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find cpfCnpj field for the creditor";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithCreditorWithInvalidCpfCnpj.json", key=KEY, path=PATH)
	public void unhappyPathTestWithInvalidCpfCnpj() {
		EditRecurringPaymentsConsentBodyToSetInvalidCreditor condition = new EditRecurringPaymentsConsentBodyToSetInvalidCreditor();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "The value inside the cpfCnpj field is neither a CPF nor a CNPJ";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
