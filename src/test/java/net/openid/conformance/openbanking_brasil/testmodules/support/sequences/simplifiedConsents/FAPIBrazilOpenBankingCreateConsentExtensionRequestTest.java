package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FAPIBrazilOpenBankingCreateConsentExtensionRequestTest extends AbstractJsonResponseConditionUnitTest {

	private static final String EXPIRATION_DATE_TIME = "2024-02-01T16:09:58Z";
	private static final String CPF = "123";
	private static final String CNPJ = "456";

	@Test
	public void happyPathTestEmptyExpirationDateTimeWithoutCnpj() {
		happyPathTest(false, "");
	}

	@Test
	public void happyPathTestNonEmptyExpirationDateTimeWithCnpj() {
		happyPathTest(true, EXPIRATION_DATE_TIME);
	}

	@Test
	public void unhappyPathTestMissingCpf() {
		environment.putObject("config", new JsonObject());
		environment.putString("consent_extension_expiration_time", EXPIRATION_DATE_TIME);
		FAPIBrazilOpenBankingCreateConsentExtensionRequest condition = new FAPIBrazilOpenBankingCreateConsentExtensionRequest();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("CPF must be specified in the test configuration"));
	}

	public void happyPathTest(boolean withCnpj, String expirationDateTimeExpected) {
		JsonObjectBuilder configBuilder = new JsonObjectBuilder()
			.addField("resource.brazilCpf", CPF);
		if (withCnpj) {
			configBuilder.addField("resource.brazilCnpj", CNPJ);
		}
		environment.putObject("config", configBuilder.build());
		environment.putString("consent_extension_expiration_time", expirationDateTimeExpected);

		FAPIBrazilOpenBankingCreateConsentExtensionRequest condition = new FAPIBrazilOpenBankingCreateConsentExtensionRequest();
		run(condition);

		JsonObject request = environment.getObject("consent_endpoint_request");
		JsonObject data = assertObjectField(request, "data");

		if (!expirationDateTimeExpected.equals("")) {
			assertStringField(data, "expirationDateTime", expirationDateTimeExpected);
		}

		JsonObject loggedUser = assertObjectField(data, "loggedUser");
		JsonObject document = assertObjectField(loggedUser, "document");
		assertStringField(document, "identification", CPF);
		assertStringField(document, "rel", "CPF");

		if (withCnpj) {
			JsonObject businessEntity = assertObjectField(data, "businessEntity");
			JsonObject documentBusiness = assertObjectField(businessEntity, "document");
			assertStringField(documentBusiness, "identification", CNPJ);
			assertStringField(documentBusiness, "rel", "CNPJ");
		}
	}

	protected void assertStringField(JsonObject obj, String fieldName, String expectedValue) {
		assertTrue(obj.has(fieldName) && obj.get(fieldName).isJsonPrimitive() && obj.get(fieldName).getAsJsonPrimitive().isString());
		assertEquals(expectedValue, OIDFJSON.getString(obj.get(fieldName)));
	}

	protected JsonObject assertObjectField(JsonObject obj, String fieldName) {
		assertTrue(obj.has(fieldName) && obj.get(fieldName).isJsonObject());
		return obj.getAsJsonObject(fieldName);
	}
}
