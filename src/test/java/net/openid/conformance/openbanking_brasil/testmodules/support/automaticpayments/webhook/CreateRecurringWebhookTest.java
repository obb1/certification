package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.webhook;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringPaymentWebhookV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringPaymentWebhookV2;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateRecurringWebhookTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_URL = "https://www.example.com";
	private static final String BASE_URL_WITH_SUFFIX = BASE_URL + "/open-banking/webhook/v1/automatic-payments/";
	private static final String CONSENT_URL = BASE_URL_WITH_SUFFIX + "v1/recurring-consents/";
	private static final String PAYMENT_URL = BASE_URL_WITH_SUFFIX + "v1/pix/recurring-payments/";
	private static final String CONSENT_URL_V2 = BASE_URL_WITH_SUFFIX + "v2/recurring-consents/";
	private static final String PAYMENT_URL_V2 = BASE_URL_WITH_SUFFIX + "v2/pix/recurring-payments/";

	@Test
	public void happyPathTestConsent() {
		environment.putString("base_url", BASE_URL);
		CreateRecurringConsentWebhookV1 condition = new CreateRecurringConsentWebhookV1();
		run(condition);
		assertEquals(CONSENT_URL, environment.getString("webhook_uri_consent_id"));
	}

	@Test
	public void happyPathTestPayment() {
		environment.putString("base_url", BASE_URL);
		CreateRecurringPaymentWebhookV1 condition = new CreateRecurringPaymentWebhookV1();
		run(condition);
		assertEquals(PAYMENT_URL, environment.getString("webhook_uri_payment_id"));
	}

	@Test
	public void happyPathTestConsentV2() {
		environment.putString("base_url", BASE_URL);
		CreateRecurringConsentWebhookV2 condition = new CreateRecurringConsentWebhookV2();
		run(condition);
		assertEquals(CONSENT_URL_V2, environment.getString("webhook_uri_consent_id"));
	}

	@Test
	public void happyPathTestPaymentV2() {
		environment.putString("base_url", BASE_URL);
		CreateRecurringPaymentWebhookV2 condition = new CreateRecurringPaymentWebhookV2();
		run(condition);
		assertEquals(PAYMENT_URL_V2, environment.getString("webhook_uri_payment_id"));
	}
}
