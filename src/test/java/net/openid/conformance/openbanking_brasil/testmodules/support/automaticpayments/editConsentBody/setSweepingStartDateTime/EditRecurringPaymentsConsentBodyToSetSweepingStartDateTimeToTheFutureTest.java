package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setSweepingStartDateTime;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime.AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime.EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToTheFuture;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToTheFutureTest extends AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeTest {

	@Override
	protected AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime condition() {
		return new EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToTheFuture();
	}

	@Override
	protected LocalDateTime expectedDateTime() {
		return ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime().plusDays(10).truncatedTo(ChronoUnit.SECONDS);
	}
}
