package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetLoansV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/loans/v1/contracts";
	}

	@Override
	protected String getApiFamilyType() {
		return "loans";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetLoansV2Endpoint();
	}
}
