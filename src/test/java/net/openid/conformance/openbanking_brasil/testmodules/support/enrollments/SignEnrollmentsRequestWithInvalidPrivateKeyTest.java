package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class SignEnrollmentsRequestWithInvalidPrivateKeyTest {


	private final Environment environment = new Environment();
	private final SignEnrollmentsRequestWithInvalidPrivateKey condition = new SignEnrollmentsRequestWithInvalidPrivateKey();

	@Before
	public void setup() {
		JsonArray permissions = new JsonArray();
		permissions.add("PAYMENTS_INITIATE");

		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addFields("data.loggedUser.document", Map.of(
				"identification", "11111111111",
				"rel", "CPF")
			)
			.addFields("data.businessEntity.document", Map.of(
				"identification", "11111111111111",
				"rel", "CNPJ")
			)
			.addFields("data.debtorAccount", Map.of(
				"ispb", "12345678",
				"issuer", "1774",
				"number", "1234567890",
				"accountType", "CACC")
			)
			.addFields("data", Map.of(
				"permissions", permissions)
			);

		JsonObject resource_request_entity_claims = jsonObjectBuilder.build();

		environment.putObject("resource_request_entity_claims", resource_request_entity_claims);
	}

	@Test
	public void happyPathTest() throws ParseException {
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.execute(environment);
		String resource_request_entity = environment.getString("resource_request_entity");
		environment.putString("endpoint_response", "body", resource_request_entity);
		JsonObject afterSigning = BodyExtractor.bodyFrom(environment, "endpoint_response")
			.orElse(new JsonObject()).getAsJsonObject();
		JsonObject beforeSigning = environment.getObject("resource_request_entity_claims");
		assertEquals(beforeSigning, afterSigning);
	}
}
