package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.AbstractCreateAutomaticRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectUserDefinedIntervalAndDateAndIsRetryAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;
import org.junit.Test;

public class CreateAutomaticRecurringConfigurationObjectUserDefinedIntervalAndDateAndIsRetryAcceptedTest extends AbstractCreateAutomaticRecurringConfigurationObjectTest {

	@Override
	protected AbstractCreateAutomaticRecurringConfigurationObject condition() {
		return new CreateAutomaticRecurringConfigurationObjectUserDefinedIntervalAndDateAndIsRetryAccepted();
	}

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getExpectedInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected void prepareEnvironmentForHappyPathTest(boolean isCpf) {
		super.prepareEnvironmentForHappyPathTest(isCpf);
		environment.putString("config", "resource.interval", "SEMANAL");
		environment.putString("config", "resource.referenceStartDate", "2024-10-03");
		environment.putString("config", "resource.isRetryAccepted", isCpf ? "True" : "FALSE");
	}

	@Override
	protected void prepareEnvironmentWithCreditorAccount() {
		environment.putString("config", "resource.interval", "MENSAL");
		environment.putString("config", "resource.referenceStartDate", "2024-10-03");
		environment.putString("config", "resource.isRetryAccepted", "true");
	}

	@Override
	protected boolean hasFirstPayment() {
		return false;
	}

	@Override
	protected boolean hasFixedAmount() {
		return false;
	}

	@Test
	public void unhappyPathTestInvalidInterval() {
		prepareEnvironmentWithContractDebtor(true);
		environment.putString("config", "resource.referenceStartDate", "2024-10-03");
		environment.putString("config", "resource.isRetryAccepted", "false");

		environment.putString("config", "resource.interval", "BIMESTRAL");
		unhappyPathTest("The value defined for resource.interval is not valid");

		environment.putString("config", "resource.interval", "mensal");
		unhappyPathTest("The value defined for resource.interval is not valid");

		environment.putString("config", "resource.interval", "Mensal");
		unhappyPathTest("The value defined for resource.interval is not valid");

		environment.putString("config", "resource.interval", "");
		unhappyPathTest("The value defined for resource.interval is not valid");

		environment.getObject("config").get("resource").getAsJsonObject().remove("interval");
		unhappyPathTest("The value defined for resource.interval is not valid");
	}

	@Test
	public void unhappyPathTestInvalidReferenceStartDate() {
		prepareEnvironmentWithContractDebtor(true);
		environment.putString("config", "resource.isRetryAccepted", "tRUE");
		environment.putString("config", "resource.interval", "TRIMESTRAL");

		environment.putString("config", "resource.referenceStartDate", "2024-10-03T10:35:45Z");
		unhappyPathTest("The value defined for resource.referenceStartDate is not valid");

		environment.putString("config", "resource.referenceStartDate", "2024-09-31T10:35:45Z");
		unhappyPathTest("The value defined for resource.referenceStartDate is not valid");

		environment.putString("config", "resource.referenceStartDate", "2024-13-03");
		unhappyPathTest("The value defined for resource.referenceStartDate is not valid");

		environment.putString("config", "resource.referenceStartDate", "2024-10-32");
		unhappyPathTest("The value defined for resource.referenceStartDate is not valid");

		environment.putString("config", "resource.referenceStartDate", "");
		unhappyPathTest("The value defined for resource.referenceStartDate is not valid");

		environment.getObject("config").get("resource").getAsJsonObject().remove("referenceStartDate");
		unhappyPathTest("The value defined for resource.referenceStartDate is not valid");
	}

	@Test
	public void unhappyPathTestInvalidIsRetryAccepted() {
		prepareEnvironmentWithContractDebtor(false);
		environment.putString("config", "resource.interval", "SEMESTRAL");
		environment.putString("config", "resource.referenceStartDate", "2024-10-03");

		environment.putString("config", "resource.isRetryAccepted", "yes");
		unhappyPathTest("The value defined for resource.isRetryAccepted is not valid");

		environment.putString("config", "resource.isRetryAccepted", "");
		unhappyPathTest("The value defined for resource.isRetryAccepted is not valid");

		environment.getObject("config").get("resource").getAsJsonObject().remove("isRetryAccepted");
		unhappyPathTest("The value defined for resource.isRetryAccepted is not valid");
	}
}
