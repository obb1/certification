package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.AbstractEditRecurringPaymentsAutomaticConsentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.EditRecurringPaymentsAutomaticConsentBodyToAddBothFixedAndVariableAmounts;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentsAutomaticConsentBodyToAddBothFixedAndVariableAmountsTest extends AbstractEditRecurringPaymentsAutomaticConsentBodyTest {

	@Override
	protected AbstractEditRecurringPaymentsAutomaticConsentBody condition() {
		return new EditRecurringPaymentsAutomaticConsentBodyToAddBothFixedAndVariableAmounts();
	}

	@Override
	protected void assertAutomaticObject(JsonObject automatic) {
		assertTrue(automatic.has("fixedAmount"));
		assertTrue(automatic.has("maximumVariableAmount"));
		assertFalse(automatic.has("minimumVariableAmount"));
	}
}
