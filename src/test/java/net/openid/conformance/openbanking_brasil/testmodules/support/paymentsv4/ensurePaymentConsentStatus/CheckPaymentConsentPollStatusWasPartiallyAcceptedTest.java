package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckPaymentConsentPollStatusWasPartiallyAcceptedTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentValidatorV4ResponsePartiallyAccepted.json")
	public void validateStructurePostPaymentsConsentPartiallyAccepted() {
		CheckPaymentConsentPollStatusWasPartiallyAccepted condition = new CheckPaymentConsentPollStatusWasPartiallyAccepted();
		run(condition);
		assertFalse(environment.getBoolean("payment_proxy_check_for_reject"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/consent/v4/PostPaymentsConsentValidatorV4ResponseOK.json")
	public void validateStructurePostPaymentsConsentWrongState() {
		CheckPaymentConsentPollStatusWasPartiallyAccepted condition = new CheckPaymentConsentPollStatusWasPartiallyAccepted();
		run(condition);
		assertTrue(environment.getBoolean("payment_proxy_check_for_reject"));
	}
}
