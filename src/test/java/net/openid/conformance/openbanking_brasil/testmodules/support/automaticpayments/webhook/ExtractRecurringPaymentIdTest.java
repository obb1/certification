package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.webhook;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.ExtractRecurringPaymentId;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractRecurringPaymentIdTest extends AbstractJsonResponseConditionUnitTest {

	private static final String PAYMENT_ID = "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl";

	@Test
	@UseResurce("jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseOK.json")
	public void happyPathTest() {
		ExtractRecurringPaymentId condition = new ExtractRecurringPaymentId();
		run(condition);

		String paymentId = environment.getString("payment_id");
		assertEquals(PAYMENT_ID, paymentId);
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce("jsonResponses/automaticpayments/PostRecurringPixPaymentsResponseMissingPaymentId.json")
	public void unhappyPathTestMissingPaymentId() {
		unhappyPathTest();
	}

	private void unhappyPathTest() {
		ExtractRecurringPaymentId condition = new ExtractRecurringPaymentId();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains("Unable to find element data.recurringPaymentId in the response payload"));
	}
}
