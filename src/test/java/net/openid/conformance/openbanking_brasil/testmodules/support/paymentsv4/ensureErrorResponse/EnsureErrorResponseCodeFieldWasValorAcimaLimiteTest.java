package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureErrorResponse;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureErrorResponseCodeFieldWasValorAcimaLimiteTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
		environment.mapKey("endpoint_response", "resource_endpoint_response_full");
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseValorAcimaLimite.json")
	public void validateGood422ValorAcimaLimite() {
		EnsureErrorResponseCodeFieldWasValorAcimaLimite condition = new EnsureErrorResponseCodeFieldWasValorAcimaLimite();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/bad422ErrorNotJsonArrayResponse.json")
	public void validateNotArray422ValorAcimaLimite() {
		EnsureErrorResponseCodeFieldWasValorAcimaLimite condition = new EnsureErrorResponseCodeFieldWasValorAcimaLimite();
		ConditionError conditionError = runAndFail(condition);
		assertTrue(conditionError.getMessage().contains("Errors is not JSON array"));
	}

	@Test
	@UseResurce("jsonResponses/errors/422/bad422ErrorNotFoundResponse.json")
	public void validateNotFound422ValorAcimaLimite() {
		EnsureErrorResponseCodeFieldWasValorAcimaLimite condition = new EnsureErrorResponseCodeFieldWasValorAcimaLimite();
		ConditionError conditionError = runAndFail(condition);
		assertTrue(conditionError.getMessage().contains("Could not find error with expected code in the errors JSON array"));
	}
}
