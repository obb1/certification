package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsAutomaticObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;


public class AddRiskSignalsObjectToPixPaymentBodyTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpfMissingRiskSignals.json", key="resource", path="brazilPixPayment")
	@UseResurce(value= "jsonRequests/automaticPayments/payments/manualRiskSignals.json", key="resource", path="riskSignals")
	public void happyPathTestManual() {
		AddRiskSignalsManualObjectToPixPaymentBody condition = new AddRiskSignalsManualObjectToPixPaymentBody();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		run(condition);
		JsonObject extractedRiskSignals = environment
			.getElementFromObject("resource", "brazilPixPayment.data.riskSignals")
			.getAsJsonObject();
		JsonObject correctRiskSignals = environment
			.getElementFromObject("resource", "riskSignals")
			.getAsJsonObject();

		Assert.assertEquals(correctRiskSignals, extractedRiskSignals);
	}

	@Test
	public void unhappyPathTestMissingConsentManual() {
		environment.putObject("resource", new JsonObject());
		unhappyPathTestManual();
	}

	@Test
	public void unhappyPathTestMissingDataManual() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTestManual();
	}

	protected void unhappyPathTestManual() {
		AddRiskSignalsManualObjectToPixPaymentBody condition = new AddRiskSignalsManualObjectToPixPaymentBody();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Unable to find data in payments payload"));
	}

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpfMissingRiskSignals.json", key="resource", path="brazilPixPayment")
	@UseResurce(value= "jsonRequests/automaticPayments/payments/automaticRiskSignals.json", key="resource", path="riskSignals")
	public void happyPathTestAutomatic() {
		AddRiskSignalsAutomaticObjectToPixPaymentBody condition = new AddRiskSignalsAutomaticObjectToPixPaymentBody();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		run(condition);
		JsonObject extractedRiskSignals = environment
			.getElementFromObject("resource", "brazilPixPayment.data.riskSignals")
			.getAsJsonObject();
		JsonObject correctRiskSignals = environment
			.getElementFromObject("resource", "riskSignals")
			.getAsJsonObject();

		Assert.assertEquals(correctRiskSignals, extractedRiskSignals);
	}

	@Test
	public void unhappyPathTestMissingConsentAutomatic() {
		environment.putObject("resource", new JsonObject());
		unhappyPathTestAutomatic();
	}

	@Test
	public void unhappyPathTestMissingDataAutomatic() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTestAutomatic();
	}

	protected void unhappyPathTestAutomatic() {
		AddRiskSignalsAutomaticObjectToPixPaymentBody condition = new AddRiskSignalsAutomaticObjectToPixPaymentBody();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Unable to find data in payments payload"));
	}

}
