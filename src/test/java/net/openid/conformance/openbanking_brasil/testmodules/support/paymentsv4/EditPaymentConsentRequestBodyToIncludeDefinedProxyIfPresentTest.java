package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public class EditPaymentConsentRequestBodyToIncludeDefinedProxyIfPresentTest extends AbstractJsonResponseConditionUnitTest {

	private static final String PROXY = "email@email.com";
	private static final String JSON_BASE_PATH = "jsonRequests/payments/consents/v4/postPaymentConsentRequest";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Before
	public void init() {
		JsonObject resource = new JsonObjectBuilder()
			.addField("creditorProxy", PROXY)
			.build();
		environment.putObject("resource", resource);
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "V4.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new EditPaymentConsentRequestBodyToIncludeDefinedProxyIfPresent());

		String proxy = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.proxy"));

		assertEquals(PROXY, proxy);
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutProxyV4.json", key = KEY, path = PATH)
	public void happyPathTestWithoutProxy() {
		environment.getObject("resource").remove("creditorProxy");

		run(new EditPaymentConsentRequestBodyToIncludeDefinedProxyIfPresent());

		JsonObject details = environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details")
			.getAsJsonObject();
		assertFalse(details.has("proxy"));
	}

	@Test
	public void unhappyPathTestMissingData() {
		JsonObject resource = environment.getObject("resource");
		resource.add("brazilPaymentConsent", new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutPaymentV4.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingPayment() {
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutPaymentDetailsV4.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingDetails() {
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingResource() {
		assertThrowsExactly(AssertionError.class, () -> {
			run(new EditPaymentConsentRequestBodyToIncludeDefinedProxyIfPresent());
		});
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new EditPaymentConsentRequestBodyToIncludeDefinedProxyIfPresent());
		assertTrue(error.getMessage().contains("Could not find data.payment.details inside payment consent request body"));
	}
}
