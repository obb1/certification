package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensurePaymentListDates;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.AbstractEnsurePaymentListDates;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertTrue;

public abstract class AbstractEnsurePaymentListDatesTest extends AbstractJsonResponseConditionUnitTest {

	protected abstract AbstractEnsurePaymentListDates condition();
	protected abstract LocalDate expectedDate();
	protected abstract int expectedAmount();

	protected void insertMockedResponse(int amount, LocalDate date) {
		JsonObject response = new JsonObject();
		JsonArray data = new JsonArray();

		JsonObject payment = new JsonObject();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		if (date != null) {
			payment.addProperty("date", date.format(formatter));
		}

		data.add(payment);

		for (int i = 1; i < amount; i++) {
			data.add(payment.deepCopy());
		}

		response.add("data", data);

		environment.putString(AbstractEnsurePaymentListDates.RESPONSE_ENV_KEY, "body", response.toString());
	}

	@Test
	public void happyPathTest() {
		insertMockedResponse(expectedAmount(), expectedDate());
		run(condition());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(AbstractEnsurePaymentListDates.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject(AbstractEnsurePaymentListDates.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingDate() {
		insertMockedResponse(expectedAmount(), null);
		unhappyPathTest("Payment is missing the date field");
	}

	@Test
	public void unhappyPathTestUnmatchingAmount() {
		insertMockedResponse(expectedAmount() + 1, expectedDate());
		unhappyPathTest("The amount of payments present in the response is different from the expected");
	}

	@Test
	public void unhappyPathTestUnmatchingDate() {
		insertMockedResponse(expectedAmount(), expectedDate().plusDays(2));
		unhappyPathTest("Payment date is not valid");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
