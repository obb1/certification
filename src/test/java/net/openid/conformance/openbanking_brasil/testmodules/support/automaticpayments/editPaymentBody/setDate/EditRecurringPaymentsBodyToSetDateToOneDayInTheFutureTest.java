package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.setDate;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToOneDayInTheFuture;

import java.time.LocalDate;
import java.time.ZoneId;

public class EditRecurringPaymentsBodyToSetDateToOneDayInTheFutureTest extends AbstractEditRecurringPaymentsBodyToSetDateTest {

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate condition() {
		return new EditRecurringPaymentsBodyToSetDateToOneDayInTheFuture();
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.now(ZoneId.of("UTC-3")).plusDays(1);
	}
}
