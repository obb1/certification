package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setCreditors;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.AbstractEditRecurringPaymentsConsentBodyToSetCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.EditRecurringPaymentsConsentBodyToSetOnePessoaNaturalCreditor;

public class EditRecurringPaymentsConsentBodyToSetOnePessoaNaturalCreditorTest extends AbstractEditRecurringPaymentsConsentBodyToSetCreditorTest {

	@Override
	protected AbstractEditRecurringPaymentsConsentBodyToSetCreditor condition() {
		return new EditRecurringPaymentsConsentBodyToSetOnePessoaNaturalCreditor();
	}

	@Override
	protected int expectedAmountOfCreditors() {
		return 1;
	}

	@Override
	protected String expectedPersonType() {
		return "PESSOA_NATURAL";
	}
}
