package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAutomaticPaymentPixRecurringV2EndpointTest extends AbstractGetXFromAuthServerTest {

	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/automatic-payments/v2/pix/recurring-payments";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-pix-recurring-payments";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetAutomaticPaymentPixRecurringV2Endpoint();
	}
}

