package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import net.openid.conformance.condition.ConditionError;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class GetEnrollmentsV1EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/enrollments/v1/enrollments";
	}

	@Override
	protected String getApiFamilyType() {
		return "enrollments";
	}

	@Override
	protected String getApiVersion() {
		return "1.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetEnrollmentsV1Endpoint();
	}

	@Override
	public void happyPath() {
		addApiResourcesToServer(List.of(
			createCorrectResource()
		));

		run(getCondition());

		assertEquals(getEndpoint(), environment.getString("config", "resource.enrollmentsUrl"));
		assertNull(environment.getString("config", "resource.consentUrl"));
		assertNull(environment.getString("config", "resource.resourceUrl"));
	}

	@Override
	protected void assertUrlNotFound() {
		ConditionError e = runAndFail(getCondition());
		assertEquals(getErrorMessage(String.format("Unable to locate resource endpoint to continue")), e.getMessage());
	}
}
