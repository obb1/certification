package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRcvdOrPndg;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentStatusEnumV2;
import org.junit.Test;

public class AbstractEnsurePaymentStatusWasXTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void testMissingBody() {
		EnsurePaymentStatusWasRcvdOrPndg cond = new EnsurePaymentStatusWasRcvdOrPndg();
		runAndFail(cond);
	}

	private void setEnvironmentWithDataAsObject(PaymentStatusEnumV2 status) {
		JsonObject response = new JsonObject();
		JsonObject responseBody = new JsonObject();
		JsonObject data = new JsonObject();

		if(status != null) {
			data.addProperty("status", status.toString());
		}
		responseBody.add("data", data);
		response.addProperty("body", responseBody.toString());
		environment.putObject(
			AbstractEnsurePaymentStatusWasX.RESPONSE_ENV_KEY,
			response
		);
	}

	private void setEnvironmentWithDataAsArray(PaymentStatusEnumV2 status) {
		JsonObject response = new JsonObject();
		JsonObject responseBody = new JsonObject();
		JsonObject data = new JsonObject();
		JsonArray dataAsArray = new JsonArray();

		if(status != null) {
			data.addProperty("status", status.toString());
		}
		dataAsArray.add(data);
		responseBody.add("data", dataAsArray);
		response.addProperty("body", responseBody.toString());
		environment.putObject(
			AbstractEnsurePaymentStatusWasX.RESPONSE_ENV_KEY,
			response
		);
	}

	@Test
	public void testDataAsObjectHappyPath() {
		setEnvironmentWithDataAsObject(PaymentStatusEnumV2.RCVD);
		EnsurePaymentStatusWasRcvdOrPndg cond = new EnsurePaymentStatusWasRcvdOrPndg();
		run(cond);
	}

	@Test
	public void testDataAsObjectInvalidStatusPath() {
		setEnvironmentWithDataAsObject(PaymentStatusEnumV2.ACCP);
		EnsurePaymentStatusWasRcvdOrPndg cond = new EnsurePaymentStatusWasRcvdOrPndg();
		runAndFail(cond);
	}

	@Test
	public void testDataAsObjectMissingStatusPath() {
		setEnvironmentWithDataAsObject(null);
		EnsurePaymentStatusWasRcvdOrPndg cond = new EnsurePaymentStatusWasRcvdOrPndg();
		runAndFail(cond);
	}

	@Test
	public void testDataAsArrayHappyPath() {
		setEnvironmentWithDataAsArray(PaymentStatusEnumV2.RCVD);
		EnsurePaymentStatusWasRcvdOrPndg cond = new EnsurePaymentStatusWasRcvdOrPndg();
		run(cond);
	}

	@Test
	public void testDataAsArrayInvalidStatus() {
		setEnvironmentWithDataAsArray(PaymentStatusEnumV2.ACCP);
		EnsurePaymentStatusWasRcvdOrPndg cond = new EnsurePaymentStatusWasRcvdOrPndg();
		runAndFail(cond);
	}

	@Test
	public void testDataAsArrayMissingStatus() {
		setEnvironmentWithDataAsArray(null);
		EnsurePaymentStatusWasRcvdOrPndg cond = new EnsurePaymentStatusWasRcvdOrPndg();
		runAndFail(cond);
	}
}
