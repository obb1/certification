package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetCreditFixedIncomesListV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/list.json")
	public void happyPath() {
		run(new GetCreditFixedIncomesListV1OASValidator());
	}


}
