package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetFundsIdentificationV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/funds/v1.0.0/identification.json")
	public void happyPath() {
		run(new GetFundsIdentificationV1OASValidator());
	}


}
