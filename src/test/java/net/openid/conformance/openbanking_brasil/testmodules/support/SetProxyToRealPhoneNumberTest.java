package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SetProxyToRealPhoneNumberTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_FILE_BASE_PATH = "jsonRequests/payments/consents/";
	private static final String CONSENT_FILE_PATH = CONSENT_FILE_BASE_PATH + "correctPaymentConsentRequestWithoutSchedule.json";
	private static final String CONSENT_NO_PAYMENT_FILE_PATH = CONSENT_FILE_BASE_PATH + "paymentConsentRequestNoPayment.json";
	private static final String CONSENT_NO_DETAILS_FILE_PATH = CONSENT_FILE_BASE_PATH + "paymentConsentRequestNoDetails.json";

	private static final String PAYMENT_FILE_BASE_PATH = "jsonRequests/payments/payments/";
	private static final String PAYMENT_ARRAY_FILE_PATH = PAYMENT_FILE_BASE_PATH + "paymentWithDataArrayWith5Payments.json";
	private static final String PAYMENT_OBJECT_FILE_PATH = PAYMENT_FILE_BASE_PATH + "paymentWithDataObject.json";

	private static final String KEY = "resource";
	private static final String CONSENT_PATH = "brazilPaymentConsent";
	private static final String PAYMENT_PATH = "brazilPixPayment";

	@Test
	@UseResurce(value = CONSENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	@UseResurce(value = PAYMENT_OBJECT_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void happyPathTestObject() {
		happyPathTest(true);
	}

	@Test
	@UseResurce(value = CONSENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	@UseResurce(value = PAYMENT_ARRAY_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void happyPathTestArray() {
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = PAYMENT_OBJECT_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void unhappyPathTestMissingConsent() {
		unhappyPathTest("Unable to find resource.brazilPaymentConsent.data.payment");
	}

	@Test
	@UseResurce(value = PAYMENT_OBJECT_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void unhappyPathTestConsentMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		unhappyPathTest("Unable to find resource.brazilPaymentConsent.data.payment");
	}

	@Test
	@UseResurce(value = CONSENT_NO_PAYMENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	@UseResurce(value = PAYMENT_OBJECT_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void unhappyPathTestConsentMissingPayment() {
		environment.putObject("resource", "brazilPaymentConsent.data", new JsonObject());
		unhappyPathTest("Unable to find resource.brazilPaymentConsent.data.payment");
	}

	@Test
	@UseResurce(value = CONSENT_NO_DETAILS_FILE_PATH, key = KEY, path = CONSENT_PATH)
	@UseResurce(value = PAYMENT_OBJECT_FILE_PATH, key = KEY, path = PAYMENT_PATH)
	public void unhappyPathTestConsentMissingDetails() {
		unhappyPathTest("Unable to find resource.brazilPaymentConsent.data.payment.details");
	}

	@Test
	@UseResurce(value = CONSENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	public void unhappyPathTestMissingPayment() {
		unhappyPathTest("Unable to find resource.brazilPixPayment.data");
	}

	@Test
	@UseResurce(value = CONSENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	public void unhappyPathTestPaymentMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest("Unable to find resource.brazilPixPayment.data");
	}

	@Test
	@UseResurce(value = CONSENT_FILE_PATH, key = KEY, path = CONSENT_PATH)
	public void unhappyPathTestInvalidPaymentDataType() {
		environment.putString("resource", "brazilPixPayment.data", "invalid");
		unhappyPathTest("Invalid data type for payment request data");
	}

	protected void happyPathTest(boolean isObject) {
		SetProxyToRealPhoneNumber condition = new SetProxyToRealPhoneNumber();
		run(condition);

		JsonObject consentPaymentDetails = environment
			.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details")
			.getAsJsonObject();
		assertProxyField(consentPaymentDetails);

		JsonElement paymentDataElement = environment.getElementFromObject("resource", "brazilPixPayment.data");
		if (isObject) {
			assertProxyField(paymentDataElement.getAsJsonObject());
		} else {
			for (JsonElement paymentElement : paymentDataElement.getAsJsonArray()) {
				assertProxyField(paymentElement.getAsJsonObject());
			}
		}
	}

	protected void assertProxyField(JsonObject obj) {
		assertTrue(
			obj.has("proxy") &&
			obj.get("proxy").isJsonPrimitive() &&
			obj.get("proxy").getAsJsonPrimitive().isString()
		);
		assertEquals(DictHomologKeys.PROXY_PHONE_NUMBER, OIDFJSON.getString(obj.get("proxy")));
	}

	protected void unhappyPathTest(String expectedMessage) {
		SetProxyToRealPhoneNumber condition = new SetProxyToRealPhoneNumber();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
