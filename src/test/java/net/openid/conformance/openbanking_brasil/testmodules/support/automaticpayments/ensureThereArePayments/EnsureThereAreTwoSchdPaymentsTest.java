package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreTwoSchdPayments;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureThereAreTwoSchdPaymentsTest extends AbstractEnsureThereArePaymentsTest {

	@Override
	protected AbstractEnsureThereArePayments condition() {
		return new EnsureThereAreTwoSchdPayments();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "TwoSchd.json")
	public void happyPathTest() {
		super.happyPathTest();
	}
}
