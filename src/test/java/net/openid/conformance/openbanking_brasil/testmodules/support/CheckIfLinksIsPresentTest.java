package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CheckIfLinksIsPresentTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/PostEnrollmentsV1Ok.json")
	public void happyPathTestWithLinks() {
		happyPathTest(true);
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsAllExtensions.json")
	public void happyPathTestWithoutLinks() {
		happyPathTest(false);
	}

	@Test
	public void unhappyPathTestInvalidJwt() {
		environment.putString(CheckIfLinksIsPresent.RESPONSE_ENV_KEY, "body", "ey1.b.c");
		unhappyPathTest("Could not parse the body");
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponse() {
		run(new CheckIfLinksIsPresent());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(CheckIfLinksIsPresent.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	private void happyPathTest(boolean expectsLinksPresence) {
		run(new CheckIfLinksIsPresent());
		assertEquals(expectsLinksPresence, environment.getBoolean("is_links_present"));
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new CheckIfLinksIsPresent());
		assertTrue(error.getMessage().contains(message));
	}
}
