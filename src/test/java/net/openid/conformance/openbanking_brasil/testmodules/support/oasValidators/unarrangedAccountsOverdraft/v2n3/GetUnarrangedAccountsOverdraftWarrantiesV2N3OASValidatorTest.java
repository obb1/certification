package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetUnarrangedAccountsOverdraftWarrantiesV2N3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/unarrangedAccountsOverdraft/v2.1.0/warranties.json")
	public void happyPath() {
		run(new GetUnarrangedAccountsOverdraftWarrantiesV2n3OASValidator());
	}
}

