package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GenerateClientExtensionResultsTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsAllExtensions.json")
	public void happyPathTestAllTrue() {
		run(new GenerateClientExtensionResults());

		JsonObject extensionResults = environment.getObject("client_extension_results");
		assertTrue(OIDFJSON.getBoolean(extensionResults.get("appid")));
		assertTrue(OIDFJSON.getBoolean(extensionResults.get("appidExclude")));
		assertTrue(OIDFJSON.getBoolean(extensionResults.get("authnSel")));
		assertTrue(OIDFJSON.getBoolean(extensionResults.get("credBlob")));
		assertTrue(OIDFJSON.getBoolean(extensionResults.get("hmac-secret")));

		JsonArray uvm = extensionResults.getAsJsonArray("uvm");
		for (JsonElement uvmEl : uvm) {
			JsonObject uvmObj = uvmEl.getAsJsonObject();
			assertTrue(uvmObj.get("userVerificationMethod").getAsJsonPrimitive().isNumber());
			assertTrue(uvmObj.get("keyProtectionType").getAsJsonPrimitive().isNumber());
			assertTrue(uvmObj.get("matcherProtectionType").getAsJsonPrimitive().isNumber());
		}

		assertTrue(OIDFJSON.getBoolean(extensionResults.getAsJsonObject("credProps").get("rk")));
		assertTrue(OIDFJSON.getBoolean(extensionResults.getAsJsonObject("largeBlob").get("supported")));
		assertEquals("abcd1234", OIDFJSON.getString(extensionResults.get("txAuthSimple")));

		assertTrue(extensionResults.has("txAuthGeneric"));

		List<String> extensionsSupportedList = Arrays.asList(
			"appid", "txAuthSimple", "txAuthGeneric", "authnSel", "exts", "uvi", "loc", "uvm",
			"credProtect", "credBlob", "largeBlobKey", "minPinLength", "hmac-secret", "appidExclude",
			"credProps", "largeBlob", "payment"
		);
		JsonArray extensionsSupported = new Gson().toJsonTree(extensionsSupportedList).getAsJsonArray();
		assertEquals(extensionsSupported, extensionResults.getAsJsonArray("exts"));

		assertEquals("1234567", OIDFJSON.getString(extensionResults.get("uvi")));

		JsonObject loc = extensionResults.getAsJsonObject("loc");
		assertTrue(loc.get("accuracy").getAsJsonPrimitive().isNumber());
		assertTrue(loc.get("latitude").getAsJsonPrimitive().isNumber());
		assertTrue(loc.get("longitude").getAsJsonPrimitive().isNumber());
		assertTrue(loc.get("altitude").getAsJsonPrimitive().isNumber());
		assertTrue(loc.get("altitudeAccuracy").getAsJsonPrimitive().isNumber());
		assertTrue(loc.get("heading").getAsJsonPrimitive().isNumber());
		assertTrue(loc.get("speed").getAsJsonPrimitive().isNumber());

		assertEquals(1, OIDFJSON.getInt(extensionResults.get("credProtect")));
		assertEquals(4, OIDFJSON.getInt(extensionResults.get("minPinLength")));
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsAllExtensionsFalseOrMissing.json")
	public void happyPathTestAllFalseOrMissing() {
		run(new GenerateClientExtensionResults());
		JsonObject extensionResults = environment.getObject("client_extension_results");
		assertEquals(extensionResults, new JsonObject());
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsNoExtensions.json")
	public void happyPathTestMissingExtensions() {
		run(new GenerateClientExtensionResults());
		JsonObject extensionResults = environment.getObject("client_extension_results");
		assertEquals(extensionResults, new JsonObject());
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsAllExtensionsPlusInvalidOne.json")
	public void unhappyPathTestInvalidExtension() {
		unhappyPathTest("The extension identifier listed is not a valid one");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsLargeBlobWithRead.json")
	public void unhappyPathTestLargeBlobWithRead() {
		unhappyPathTest("The 'largeBlob' extension input should not contain neither 'read' nor 'write' fields during registration");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/PostFidoRegistrationOptionsLargeBlobWithWrite.json")
	public void unhappyPathTestLargeBlobWithWrite() {
		unhappyPathTest("The 'largeBlob' extension input should not contain neither 'read' nor 'write' fields during registration");
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		JsonObject resource = new JsonObject();
		resource.addProperty("body", "ey1.b.c");
		environment.putObject("resource_endpoint_response_full", resource);
		unhappyPathTest("Error parsing JWT response");
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponse() {
		run(new GenerateClientExtensionResults());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		unhappyPathTest("It was impossible to extract data from the fido-registration-options response body");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new GenerateClientExtensionResults());
		assertTrue(error.getMessage().contains(message));
	}
}
