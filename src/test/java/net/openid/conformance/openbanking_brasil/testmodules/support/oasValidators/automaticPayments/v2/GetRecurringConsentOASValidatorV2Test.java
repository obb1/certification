package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetRecurringConsentOASValidatorV2Test extends AbstractRecurringConsentOASValidatorV2Test {

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200Response.json")
	public void happyPathTestCompleteConsentResponse() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseMissingOptionalFields.json")
	public void happyPathTestMissingOptionalFieldsConsentResponse() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCode.json")
	public void unhappyPathTestAutomaticValidStatusNoDebtorAccountIbgeTownCode() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCodeStatusAuthorised.json")
	public void unhappyPathTestAutomaticAuthorisedNoDebtorAccountIbgeTownCode() {
		unhappyPathTest("debtorAccount.ibgeTownCode is required when recurringConfiguration is automatic and status is AUTHORISED, REVOKED or CONSUMED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCodeStatusRevoked.json")
	public void unhappyPathTestAutomaticRevokedNoDebtorAccountIbgeTownCode() {
		unhappyPathTest("debtorAccount.ibgeTownCode is required when recurringConfiguration is automatic and status is AUTHORISED, REVOKED or CONSUMED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCodeStatusConsumed.json")
	public void unhappyPathTestAutomaticConsumedNoDebtorAccountIbgeTownCode() {
		unhappyPathTest("debtorAccount.ibgeTownCode is required when recurringConfiguration is automatic and status is AUTHORISED, REVOKED or CONSUMED");
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponseRevokedNoRevocation.json")
	public void unhappyPathTestRevokedNoRevocation() {
		super.unhappyPathTestRevokedNoRevocation();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponseAuthorisedNoAuthorisedAtDateTime.json")
	public void unhappyPathTestAuthorisedNoAuthorisedAtDateTime() {
		super.unhappyPathTestAuthorisedNoAuthorisedAtDateTime();
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponsePartiallyAcceptedNoApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedNoApprovalDueDate() {
		unhappyPathTest("approvalDueDate is required when status is PARTIALLY_ACCEPTED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponsePartiallyAcceptedNoExpirationDateTimeValidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedNoExpirationDateTimeValidApprovalDueDate() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponsePartiallyAcceptedNoExpirationDateTimeInvalidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedNoExpirationDateTimeInvalidApprovalDueDate() {
		unhappyPathTest("When expirationDateTime is not specified, approvalDueDate should default to 30 days after creationDateTime");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponsePartiallyAcceptedLowExpirationDateTimeValidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedLowExpirationDateTimeValidApprovalDueDate() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponsePartiallyAcceptedLowExpirationDateTimeInvalidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedLowExpirationDateTimeInvalidApprovalDueDate() {
		unhappyPathTest("The approvalDueDate should not exceed neither the expirationDateTime nor the creationDateTime + 30 days");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponsePartiallyAcceptedHighExpirationDateTimeValidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedHighExpirationDateTimeValidApprovalDueDate() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringConsents200ResponsePartiallyAcceptedHighExpirationDateTimeInvalidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedHighExpirationDateTimeInvalidApprovalDueDate() {
		unhappyPathTest("The approvalDueDate should not exceed neither the expirationDateTime nor the creationDateTime + 30 days");
	}

	@Override
	protected int status() {
		return 200;
	}

	@Override
	protected AbstractRecurringConsentOASValidatorV2 validator() {
		return new GetRecurringConsentOASValidatorV2();
	}
}
