package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASV2ValidatorsTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.AbstractEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostFidoSignOptionsOASValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostFidoSignOptionsOASValidatorV2n1Test extends AbstractEnrollmentsOASV2n1ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2n1 validator() {
		return new PostFidoSignOptionsOASValidatorV2n1();
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoSignOptions201Response.json")
	public void happyPathTest201() {
		happyPathTest(201);
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoSignOptions422Response.json")
	public void happyPathTest422() {
		happyPathTest(422);
	}
}
