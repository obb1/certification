package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class BusinessQualificationResponseOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/legalEntityQualificationV2/legalEntityQualificationResponse-V2.1.0.json")
	public void testHappyPath() {
		BusinessQualificationResponseOASValidatorV2 cond = new BusinessQualificationResponseOASValidatorV2();
		run(cond);

		JsonObject mainEconomicActivity = new JsonObject();
		mainEconomicActivity.addProperty("isMain", true);
		JsonObject economicActivity = new JsonObject();
		economicActivity.addProperty("isMain", false);
		JsonArray economicActivities = new JsonArray();
		economicActivities.add(economicActivity);
		economicActivities.add(mainEconomicActivity);

		economicActivities.add(mainEconomicActivity);
		JsonObject invalidEconomicActivities = new JsonObject();
		invalidEconomicActivities.add("economicActivities", economicActivities);
		assertThrows(ConditionError.class, () -> cond.assertEconomicActivitiesConstraint(invalidEconomicActivities));

		economicActivities = new JsonArray();
		economicActivities.add(economicActivity);
		economicActivities.add(mainEconomicActivity);
		JsonObject validEconomicActivities = new JsonObject();
		validEconomicActivities.add("economicActivities", economicActivities);
		cond.assertEconomicActivitiesConstraint(validEconomicActivities);

		JsonObject informedRevenue = new JsonObject();
		informedRevenue.addProperty("frequency", "OUTROS");
		JsonObject invalidInformedRevenue = new JsonObject();
		invalidInformedRevenue.add("informedRevenue", informedRevenue);
		assertThrows(ConditionError.class, () -> cond.assertInformedRevenueConstraint(invalidInformedRevenue));
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/legalEntityQualificationV2/legalEntityQualificationResponse-V2.1.0-missingInformedRevenueFrequency.json")
	public void testHappyPathMissingInformedRevenueFrequency() {
		BusinessQualificationResponseOASValidatorV2 cond = new BusinessQualificationResponseOASValidatorV2();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/legalEntityQualificationV2/legalEntityQualificationInvalidResponse-V2.1.0.json")
	public void testUnhappyPath() {
		BusinessQualificationResponseOASValidatorV2 cond = new BusinessQualificationResponseOASValidatorV2();
		runAndFail(cond);
	}
}
