package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedBy.EnsureRecurringPaymentsConsentsRejectedByUsuario;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureRecurringPaymentsConsentsRejectedByTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonResponses/automaticpayments/PostRecurringConsents";
	private static final String MISSING_FIELD_MESSAGE = "Unable to find element data.rejection.rejectedBy in the response payload";

	@Test
	@UseResurce(BASE_FILE_PATH + "RejectedFromIniciadoraRejectedByUsuario.json")
	public void happyPathTest() {
		EnsureRecurringPaymentsConsentsRejectedByUsuario condition = new EnsureRecurringPaymentsConsentsRejectedByUsuario();
		run(condition);
	}

	@Test
	public void unhappyPathMissingResponse() {
		EnsureRecurringPaymentsConsentsRejectedByUsuario condition = new EnsureRecurringPaymentsConsentsRejectedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	public void unhappyPathMissingData() {
		environment.putObject(EnsureRecurringPaymentsConsentsRejectedByUsuario.RESPONSE_ENV_KEY, new JsonObject());
		EnsureRecurringPaymentsConsentsRejectedByUsuario condition = new EnsureRecurringPaymentsConsentsRejectedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRejectionField.json")
	public void unhappyPathMissingRejection() {
		EnsureRecurringPaymentsConsentsRejectedByUsuario condition = new EnsureRecurringPaymentsConsentsRejectedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRejectedByField.json")
	public void unhappyPathMissingRejectedBy() {
		EnsureRecurringPaymentsConsentsRejectedByUsuario condition = new EnsureRecurringPaymentsConsentsRejectedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RejectedFromIniciadoraRejectedByIniciadora.json")
	public void unhappyPathRejectedByNotMatchingExpected() {
		EnsureRecurringPaymentsConsentsRejectedByUsuario condition = new EnsureRecurringPaymentsConsentsRejectedByUsuario();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "rejectedBy returned in the response does not match the expected rejectedBy";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
