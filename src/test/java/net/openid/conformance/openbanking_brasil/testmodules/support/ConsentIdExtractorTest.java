package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConsentIdExtractorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponse.json")
	public void happyPathTest() {
		run(new ConsentIdExtractor());
		assertEquals("urn:bancoex:C1DD33123", environment.getString("consent_id"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConsentEndpointFull() {
		run(new ConsentIdExtractor());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject("consent_endpoint_response_full", new JsonObject());
		unhappyPathTest("Could not extract consentId from response body");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("consent_endpoint_response_full", new JsonObjectBuilder().addField("data", new JsonObject()).build());
		unhappyPathTest("Could not extract consentId from response body");
	}

	@Test
	@UseResurce("jsonResponses/consent/getConsentById/v2/getConsentByIdResponseMissingConsentId.json")
	public void unhappyPathTestMissingConsentId() {
		unhappyPathTest("Could not extract consentId from response body");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject("consent_endpoint_response_full",
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new ConsentIdExtractor());
		assertTrue(error.getMessage().contains(message));
	}
}
