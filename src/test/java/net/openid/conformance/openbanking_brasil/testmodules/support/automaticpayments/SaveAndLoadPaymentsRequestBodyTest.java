package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadPaymentsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SavePaymentsRequestBody;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SaveAndLoadPaymentsRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	private static final String FILE_PATH = "jsonRequests/automaticPayments/payments/v2/PostRecurringPaymentsPixRequestBody.json";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";
	private static final String SAVED_PATH = "savedBrazilPixPayment";

	@Test
	@UseResurce(value=FILE_PATH, key=KEY, path=PATH)
	public void happyPathTest() {
		// Use save condition
		SavePaymentsRequestBody saveCondition = new SavePaymentsRequestBody();
		run(saveCondition);

		// Check that the saved request is equals to the original one
		JsonObject originalRequest = environment.getElementFromObject(KEY, PATH).getAsJsonObject();
		JsonObject savedRequest = environment.getObject(SAVED_PATH);
		assertEquals(originalRequest, savedRequest);

		// Modify the original consent request to show that they are separate objects
		originalRequest.addProperty("example", "example");
		assertNotEquals(originalRequest, savedRequest);

		// Use load condition to override the original request
		LoadPaymentsRequestBody loadCondition = new LoadPaymentsRequestBody();
		run(loadCondition);

		// Check that the saved request is equals to the loaded one
		JsonObject loadedRequest = environment.getElementFromObject(KEY, PATH).getAsJsonObject();
		assertEquals(loadedRequest, savedRequest);

		// Modify the saved consent request to show that they are separate objects
		savedRequest.addProperty("example", "example");
		assertNotEquals(loadedRequest, savedRequest);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathLoadingWithoutSavingTest() {
		LoadPaymentsRequestBody loadCondition = new LoadPaymentsRequestBody();
		run(loadCondition);
	}

	@Test
	public void unhappyPathSaveMissingBrazilPaymentConsentTest() {
		environment.putObject("resource", new JsonObject());
		SavePaymentsRequestBody saveCondition = new SavePaymentsRequestBody();
		ConditionError error = runAndFail(saveCondition);

		String expectedMessage = "Unable to find payments body inside the resource";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
