package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseCodeWas401Or403Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureConsentResponseCodeWas401Or403();
	}

	@Override
	public void happyPath() {
		super.happyPath();
		AbstractEnsureResponseCodeWas condition = condition();
		setStatus(happyPathAlternativeResponseCode());
		run(condition);
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.UNAUTHORIZED.value();
	}

	protected int happyPathAlternativeResponseCode() {
		return HttpStatus.FORBIDDEN.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.OK.value();
	}

}
