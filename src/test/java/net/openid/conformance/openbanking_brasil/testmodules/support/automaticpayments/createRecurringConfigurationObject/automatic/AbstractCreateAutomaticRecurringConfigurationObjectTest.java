package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createRecurringConfigurationObject.automatic;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.AbstractCreateAutomaticRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class AbstractCreateAutomaticRecurringConfigurationObjectTest extends AbstractJsonResponseConditionUnitTest {

	protected abstract AbstractCreateAutomaticRecurringConfigurationObject condition();
	protected abstract RecurringPaymentsConsentsAutomaticPixIntervalEnum getExpectedInterval();

	protected static final String CONTRACT_DEBTOR_NAME = "John Doe";
	protected static final String CONTRACT_DEBTOR_IDENTIFICATION_CPF = "11111111111";
	protected static final String CONTRACT_DEBTOR_IDENTIFICATION_CNPJ = "11111111111111";
	protected static final String CONTRACT_DEBTOR_IDENTIFICATION_INVALID = "1111111111111";

	protected static final String CREDITOR_ACCOUNT_ISPB = "12345678";
	protected static final String CREDITOR_ACCOUNT_ISSUER = "1774";
	protected static final String CREDITOR_ACCOUNT_NUMBER = "1234567890";
	protected static final String CREDITOR_ACCOUNT_ACCOUNT_TYPE = "CACC";

	@Test
	public void happyPathTestCpf() {
		prepareEnvironmentForHappyPathTest(true);
		run(condition());
		assertRecurringConfigurationObject(true);
	}

	@Test
	public void happyPathTestCnpj() {
		prepareEnvironmentForHappyPathTest(false);
		run(condition());
		assertRecurringConfigurationObject(false);
	}

	@Test
	public void unhappyPathTestMissingContractDebtorName() {
		environment.putString("config", "resource.contractDebtorIdentification", CONTRACT_DEBTOR_IDENTIFICATION_CPF);
		prepareEnvironmentWithCreditorAccount();
		unhappyPathTest("Could not find contractDebtorName in the resource");
	}

	@Test
	public void unhappyPathTestMissingContractDebtorIdentification() {
		environment.putString("config", "resource.contractDebtorName", CONTRACT_DEBTOR_NAME);
		prepareEnvironmentWithCreditorAccount();
		unhappyPathTest("Could not find contractDebtorIdentification in the resource");
	}

	@Test
	public void unhappyPathTestInvalidContractDebtorIdentification() {
		environment.putString("config", "resource.contractDebtorIdentification", CONTRACT_DEBTOR_IDENTIFICATION_INVALID);
		environment.putString("config", "resource.contractDebtorName", CONTRACT_DEBTOR_NAME);
		prepareEnvironmentWithCreditorAccount();
		unhappyPathTest("Value set for contractDebtor identification is neither CPF nor CNPJ");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountIspb() {
		unhappyCreditorAccountFieldTest("Ispb");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountIssuer() {
		unhappyCreditorAccountFieldTest("Issuer");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountNumber() {
		unhappyCreditorAccountFieldTest("Number");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountAccountType() {
		unhappyCreditorAccountFieldTest("AccountType");
	}

	protected void prepareEnvironmentForHappyPathTest(boolean isCpf) {
		prepareEnvironmentWithContractDebtor(isCpf);
		prepareEnvironmentWithCreditorAccount();
	}

	protected void prepareEnvironmentWithContractDebtor(boolean isCpf) {
		String contractDebtorIdentification = isCpf ? CONTRACT_DEBTOR_IDENTIFICATION_CPF : CONTRACT_DEBTOR_IDENTIFICATION_CNPJ;
		environment.putString("config", "resource.contractDebtorName", CONTRACT_DEBTOR_NAME);
		environment.putString("config", "resource.contractDebtorIdentification", contractDebtorIdentification);
	}

	protected void prepareEnvironmentWithCreditorAccount() {
		environment.putString("config", "resource.creditorAccountIspb", CREDITOR_ACCOUNT_ISPB);
		environment.putString("config", "resource.creditorAccountIssuer", CREDITOR_ACCOUNT_ISSUER);
		environment.putString("config", "resource.creditorAccountNumber", CREDITOR_ACCOUNT_NUMBER);
		environment.putString("config", "resource.creditorAccountAccountType", CREDITOR_ACCOUNT_ACCOUNT_TYPE);
	}

	protected void prepareEnvironmentForMissingCreditorAccountTest(String missingField) {
		prepareEnvironmentForHappyPathTest(true);
		environment.getElementFromObject("config", "resource").getAsJsonObject().remove("creditorAccount" + missingField);
	}

	protected void assertRecurringConfigurationObject(boolean isCpf) {
		JsonObject recurringConfigObj = environment.getObject("recurring_configuration_object");
		JsonObject automatic = recurringConfigObj.getAsJsonObject("automatic");

		assertMandatoryFields(automatic, isCpf);
		assertOptionalFields(automatic);
		assertFirstPayment(automatic);
	}

	protected void assertMandatoryFields(JsonObject automatic, boolean isCpf) {
		assertStringField(automatic, "contractId");
		assertStringField(automatic, "referenceStartDate");

		JsonObject contractDebtor = assertObjectField(automatic, "contractDebtor");
		assertStringField(contractDebtor, "name", CONTRACT_DEBTOR_NAME);
		String expectedIdentification = isCpf ? CONTRACT_DEBTOR_IDENTIFICATION_CPF : CONTRACT_DEBTOR_IDENTIFICATION_CNPJ;
		String expectedRel = isCpf ? "CPF" : "CNPJ";
		JsonObject contractDebtorDocument = assertObjectField(contractDebtor, "document");
		assertStringField(contractDebtorDocument, "identification", expectedIdentification);
		assertStringField(contractDebtorDocument, "rel", expectedRel);

		assertTrue(
			automatic.has("isRetryAccepted") &&
			automatic.get("isRetryAccepted").isJsonPrimitive() &&
			automatic.get("isRetryAccepted").getAsJsonPrimitive().isBoolean()
		);
	}

	protected void assertOptionalFields(JsonObject automatic) {
		assertOptionalStringField(automatic, "interval", hasInterval());
		if (hasInterval()) {
			String interval = OIDFJSON.getString(automatic.get("interval"));
			assertEquals(getExpectedInterval().toString(), interval);
		}
		assertOptionalStringField(automatic, "fixedAmount", hasFixedAmount());
		assertOptionalStringField(automatic, "minimumVariableAmount", hasMinimumVariableAmount());
		assertOptionalStringField(automatic, "maximumVariableAmount", hasMaximumVariableAmount());
	}

	protected void assertFirstPayment(JsonObject automatic) {
		if (hasFirstPayment()) {
			JsonObject firstPayment = automatic.getAsJsonObject("firstPayment");
			assertStringField(firstPayment, "type", "PIX");
			assertStringField(firstPayment, "date");
			assertStringField(firstPayment, "currency", "BRL");
			assertStringField(firstPayment, "amount");

			JsonObject creditorAccount = assertObjectField(firstPayment, "creditorAccount");
			assertStringField(creditorAccount, "ispb", CREDITOR_ACCOUNT_ISPB);
			assertStringField(creditorAccount, "issuer", CREDITOR_ACCOUNT_ISSUER);
			assertStringField(creditorAccount, "number", CREDITOR_ACCOUNT_NUMBER);
			assertStringField(creditorAccount, "accountType", CREDITOR_ACCOUNT_ACCOUNT_TYPE);
		}
	}

	protected void assertStringField(JsonObject obj, String field) {
		assertTrue(
			obj.has(field) &&
				obj.get(field).isJsonPrimitive() &&
				obj.get(field).getAsJsonPrimitive().isString()
		);
	}

	protected void assertStringField(JsonObject obj, String field, String expectedValue) {
		assertStringField(obj, field);
		assertEquals(expectedValue, OIDFJSON.getString(obj.get(field)));
	}

	protected void assertOptionalStringField(JsonObject obj, String field, boolean hasField) {
		if (hasField) {
			assertTrue(
					obj.get(field).isJsonPrimitive() &&
					obj.get(field).getAsJsonPrimitive().isString()
			);
		} else {
			assertFalse(obj.has(field));
		}
	}

	protected JsonObject assertObjectField(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}

	protected boolean hasFirstPayment() {
		return true;
	}

	protected boolean hasInterval() {
		return getExpectedInterval() != null;
	}

	protected boolean hasFixedAmount() {
		return true;
	}

	protected boolean hasMinimumVariableAmount() {
		return false;
	}

	protected boolean hasMaximumVariableAmount() {
		return false;
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}

	protected void unhappyCreditorAccountFieldTest(String field) {
		if (hasFirstPayment()) {
			prepareEnvironmentForMissingCreditorAccountTest(field);
			unhappyPathTest(String.format("Could not find creditorAccount%s in the resource", field));
		}
	}
}
