package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.setDate;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToTenDaysInTheFuture;

import java.time.LocalDate;
import java.time.ZoneId;

public class EditRecurringPaymentsBodyToSetDateToTenDaysInTheFutureTest extends AbstractEditRecurringPaymentsBodyToSetDateTest {

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate condition() {
		return new EditRecurringPaymentsBodyToSetDateToTenDaysInTheFuture();
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.now(ZoneId.of("UTC-3")).plusDays(10);
	}
}
