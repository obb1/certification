package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateExtensionExpiryTimeInConsentResponseTest extends AbstractJsonResponseConditionUnitTest {

	private static final String RESOURCE_BASE_PATH = "jsonResponses/consent/v3/GetConsentResponse";
	private static final String EXPIRATION_DATE_TIME = "2021-05-21T08:30:00Z";
	private static final String WRONG_EXPIRATION_DATE_TIME = "2022-05-21T08:30:00Z";

	@Test
	@UseResurce(RESOURCE_BASE_PATH + "MissingExpirationDateTimeV3.json")
	public void happyPathTestEmptyExpirationDateTime() {
		happyPathTest("");
	}

	@Test
	@UseResurce(RESOURCE_BASE_PATH + "V3.json")
	public void happyPathTestNonEmptyExpirationDateTime() {
		happyPathTest(EXPIRATION_DATE_TIME);
	}

	@Test
	@UseResurce(RESOURCE_BASE_PATH + "V3.json")
	public void unhappyPathTestEmptyExpirationDateTime() {
		unhappyPathTest("", "expirationDateTime field was not sent, so it should not be present in the response");
	}

	@Test
	@UseResurce(RESOURCE_BASE_PATH + "V3.json")
	public void unhappyPathTestNonEmptyExpirationDateTime() {
		unhappyPathTest(WRONG_EXPIRATION_DATE_TIME, "expirationDateTime returned does not match the one sent in the request");
	}

	public void happyPathTest(String expirationDateTime) {
		environment.putString("consent_extension_expiration_time", expirationDateTime);
		ValidateExtensionExpiryTimeInConsentResponse condition = new ValidateExtensionExpiryTimeInConsentResponse();
		run(condition);
	}

	public void unhappyPathTest(String expirationDateTime, String errorMessage) {
		environment.putString("consent_extension_expiration_time", expirationDateTime);
		ValidateExtensionExpiryTimeInConsentResponse condition = new ValidateExtensionExpiryTimeInConsentResponse();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
