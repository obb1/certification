package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.setDate;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToTodayUtcMinus3;

import java.time.LocalDate;
import java.time.ZoneOffset;

public class EditRecurringPaymentsBodyToSetDateToTodayUtcMinus3Test extends AbstractEditRecurringPaymentsBodyToSetDateTest {

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate condition() {
		return new EditRecurringPaymentsBodyToSetDateToTodayUtcMinus3();
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.now(ZoneOffset.ofHours(-3));
	}
}
