package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddAudAsEnrollmentsUrlToRequestObjectTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		String enrollmentsUrl = "https://www.example.com/open-banking/enrollments/v1/enrollments";
		environment.putString("enrollments_url", enrollmentsUrl);
		environment.putObject("resource_request_entity_claims", new JsonObject());
		AddAudAsEnrollmentsUrlToRequestObject condition = new AddAudAsEnrollmentsUrlToRequestObject();
		run(condition);
		JsonObject requestClaims = environment.getObject("resource_request_entity_claims");
		assertTrue(
			requestClaims.has("aud") &&
			requestClaims.get("aud").isJsonPrimitive() &&
			requestClaims.get("aud").getAsJsonPrimitive().isString()
		);
		assertEquals(enrollmentsUrl, OIDFJSON.getString(requestClaims.get("aud")));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingEnrollmentsUrl() {
		environment.putObject("resource_request_entity_claims", new JsonObject());
		AddAudAsEnrollmentsUrlToRequestObject condition = new AddAudAsEnrollmentsUrlToRequestObject();
		run(condition);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResourceRequestEntityClaims() {
		String enrollmentsUrl = "https://www.example.com/open-banking/enrollments/v1/enrollments";
		environment.putString("enrollments_url", enrollmentsUrl);
		AddAudAsEnrollmentsUrlToRequestObject condition = new AddAudAsEnrollmentsUrlToRequestObject();
		run(condition);
	}
}
