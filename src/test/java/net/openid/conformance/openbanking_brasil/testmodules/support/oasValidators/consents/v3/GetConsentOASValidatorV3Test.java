package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;


public class GetConsentOASValidatorV3Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullConsentResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/consent/v3/GetConsentResponseV3.json")
	public void testHappyPath() {
		GetConsentOASValidatorV3 cond = new GetConsentOASValidatorV3();
		run(cond);
	}
}
