package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetInvoiceFinancingsWarrantiesV2n3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/warranties.json")
	public void happyPath() {
		run(new GetInvoiceFinancingsWarrantiesV2n3OASValidator());
	}
}

