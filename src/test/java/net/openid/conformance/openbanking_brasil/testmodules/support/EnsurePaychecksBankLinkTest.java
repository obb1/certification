package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EnsurePaychecksBankLinkTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/naturalPersonalRelationshipV2/naturalPersonRelationshipResponse-V2.1.0.json")
	public void testHappyPath() {
		EnsurePaychecksBankLink cond = new EnsurePaychecksBankLink();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/naturalPersonalRelationshipV2/naturalPersonRelationshipResponseWithoutPaychecks-V2.1.0.json")
	public void testUnhappyPath() {
		EnsurePaychecksBankLink cond = new EnsurePaychecksBankLink();

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("paychecksBankLink is empty"));
	}
}
