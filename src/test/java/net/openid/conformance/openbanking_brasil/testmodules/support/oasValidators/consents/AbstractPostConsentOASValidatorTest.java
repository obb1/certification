package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class AbstractPostConsentOASValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/consent/v3/400ErrorResponse.json")
	public void testBadRequestResponse() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullConsentResponseEnvKey.getKey());
		response.addProperty("status", 403);
		// Set up the environment.
		PostConsentOASValidatorV3 cond = new PostConsentOASValidatorV3();
		run(cond);
	}
}
