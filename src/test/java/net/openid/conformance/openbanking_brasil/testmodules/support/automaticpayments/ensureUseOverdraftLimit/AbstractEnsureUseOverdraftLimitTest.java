package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureUseOverdraftLimit;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit.AbstractEnsureUseOverdraftLimit;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class AbstractEnsureUseOverdraftLimitTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";
	protected abstract AbstractEnsureUseOverdraftLimit condition();

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(AbstractEnsureUseOverdraftLimit.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(AbstractEnsureUseOverdraftLimit.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingRecurringConfiguration200Response.json")
	public void unhappyPathTestMissingRecurringConfiguration() {
		unhappyPathTest("Could not extract useOverdraftLimit field from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void unhappyPathTestMissingNotAutomatic() {
		unhappyPathTest("Could not extract useOverdraftLimit field from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingUseOverdraftLimit200Response.json")
	public void unhappyPathTestMissingUseOverdraftLimit() {
		unhappyPathTest("Could not extract useOverdraftLimit field from response data");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
