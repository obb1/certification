package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


public class GetPaymentsPixOASValidatorV4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/GetPaymentsInitiationPixPaymentsValidatorV4Ok.json")
	public void happyPath() {
		run(new GetPaymentsPixOASValidatorV4());
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/GetPaymentsInitiationPixPaymentsValidatorV4ProxyWithManu.json")
	public void unhappyPathProxy1() {
		ConditionError e = runAndFail(new GetPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("proxy cannot be present when localInstrument has the value MANU"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/GetPaymentsInitiationPixPaymentsValidatorV4NoProxy.json")
	public void unhappyPathProxy2() {
		ConditionError e = runAndFail(new GetPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("proxy is required when localInstrument is [INIC, DICT, QRDN, QRES]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/GetPaymentsInitiationPixPaymentsValidatorV4CreditorNoIssuer.json")
	public void unhappyPathCreditorNoIssuer() {
		ConditionError e = runAndFail(new GetPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("creditorAccount.issuer is required when creditorAccount.accountType is [CACC, SVGS]"));
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/GetPaymentsInitiationPixPaymentsValidatorV4DebtorNoIssuer.json")
	public void unhappyPathDebtorNoIssuer() {
		ConditionError e = runAndFail(new GetPaymentsPixOASValidatorV4());
		assertThat(e.getMessage(), containsString("debtorAccount.issuer is required when debtorAccount.accountType is [CACC, SVGS]"));
	}
}
