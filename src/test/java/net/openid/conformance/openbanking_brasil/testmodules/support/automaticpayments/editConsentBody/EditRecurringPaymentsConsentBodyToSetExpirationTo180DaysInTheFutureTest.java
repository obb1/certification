package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFutureTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTest() {
		EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture condition = new EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture();
		run(condition);

		String expirationDateTimeString = OIDFJSON.getString(environment
			.getElementFromObject("resource", "brazilPaymentConsent.data.expirationDateTime"));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime expirationDateTime = LocalDateTime.parse(expirationDateTimeString, formatter);

		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime expectedDateTime = currentDateTime.plusDays(180);

		assertTrue(expirationDateTime.isBefore(expectedDateTime) || expirationDateTime.isEqual(expectedDateTime));
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject("resource", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture condition = new EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPaymentConsent", new JsonObject());
		EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture condition = new EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Unable to find data field in consents payload";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
