package net.openid.conformance.openbanking_brasil.testmodules.support.resourcesAPI.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrepareUrlForSavedResourceCallTests extends AbstractJsonResponseConditionUnitTest {
	JsonObject resource = new JsonObject();

	@Before
	public void init() {
		String consentUrl = "https://matls-api.mockbank.poc.raidiam.io/open-banking/consents/v3/consents";
		resource.addProperty("resource_api", "consents");
		resource.addProperty("resource_list_endpoint", "/consents");
		resource.addProperty("resource_endpoint", "/consents");
		resource.addProperty("id_type", "consentId");
		environment.putObject("resource_data", resource);
		environment.putString("config","resource.consentUrl", consentUrl);
	}

	@Test
	@UseResurce(value = "jsonResponses/consent/v3/GetConsentResponseV3.json")
	public void happyPathTestConsentsV3() {
		PrepareUrlForSavedResourceCall condition = new PrepareUrlForSavedResourceCall();
		run(condition);

		assertEquals("https://matls-api.mockbank.poc.raidiam.io/open-banking/consents/v3/consents",
			environment.getString("protected_resource_url"));
	}

	@Test
	@UseResurce(value = "jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3.json")
	public void happyPathTestAccountsV2() {
		resource.addProperty("resource_api", "accounts");
		resource.addProperty("id_type", "accountId");
		resource.addProperty("resource_list_endpoint", "/accounts");
		resource.addProperty("resource_endpoint", "/balances");
		PrepareUrlForSavedResourceCall condition = new PrepareUrlForSavedResourceCall();
		run(condition);

		assertEquals("https://matls-api.mockbank.poc.raidiam.io/open-banking/accounts/v2/balances",
			environment.getString("protected_resource_url"));
	}

	@Test
	@UseResurce(value = "jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3.json")
	public void happyPathTestLoansV2() {
		resource.addProperty("resource_api", "loans");
		resource.addProperty("id_type", "contractId");
		resource.addProperty("resource_list_endpoint", "/contracts");
		resource.addProperty("resource_endpoint", "/warranties");
		PrepareUrlForApiListForSavedResourceCall condition = new PrepareUrlForApiListForSavedResourceCall();
		run(condition);

		assertEquals("https://matls-api.mockbank.poc.raidiam.io/open-banking/loans/v2/contracts",
			environment.getString("protected_resource_url"));
	}
	@Test
	@UseResurce(value = "jsonResponses/resourcesAPI/v3/resourcesAPIResponseV3.json")
	public void happyPathTestResourcesV3() {
		resource.addProperty("resource_api", "resources");
		resource.addProperty("resource_list_endpoint", "/resources");
		resource.addProperty("resource_endpoint", "/resources");
		resource.addProperty("id_type", "resourceId");
		PrepareUrlForApiListForSavedResourceCall condition = new PrepareUrlForApiListForSavedResourceCall();
		run(condition);

		assertEquals("https://matls-api.mockbank.poc.raidiam.io/open-banking/resources/v3/resources",
			environment.getString("protected_resource_url"));
	}
}
