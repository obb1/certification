package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

public class OpenAPIJsonSchemaValidatorTest extends AbstractJsonResponseConditionUnitTest {

	private void setStatusCode(String envKey, Integer statusCode) {
		JsonObject response = environment.getObject(envKey);
		response.addProperty("status", statusCode);
	}

	@Test
	public void testAssertStringFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			"string"
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryStringFieldSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			"string"
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testAssertOptionalStringFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";

		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertStringFieldSchemaHappyPathNoConstraints() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringField(
			cond,
			"string",
			null,
			null,
			null,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertStringFieldSchemaInvalidPattern() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringField(
			cond,
			"string123",
			"^[a-z]{2,15}$",
			2,
			15,
			"string123"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("doesn't match the required pattern"));
	}

	@Test
	public void testAssertStringFieldSchemaInvalidMinLength() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			20,
			25,
			"string"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minLength"));
	}

	@Test
	public void testAssertStringFieldSchemaInvalidMaxLength() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			3,
			"string"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maxLength"));
	}

	@Test
	public void testAssertStringFieldSchemaInvalidEnum() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			"string123"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("does not match any given enumeration"));
	}

	private void prepareEnvForStringField(
		OpenAPIJsonSchemaValidatorImpl cond,
		String value,
		String pattern,
		Integer minLength,
		Integer maxLength,
		String enumString
	) {

		String stringFieldKey = "string_field";

		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, pattern);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_LENGTH_KEY, minLength);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_LENGTH_KEY, maxLength);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.ENUM_KEY, enumString != null ? List.of(enumString) : null);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(stringFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, value);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertDateFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForDateField(
			cond,
			"2024-01-01",
			null,
			0,
			50,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryDateFieldSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForDateField(
			cond,
			"2024-01-01",
			null,
			0,
			50,
			null
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testAssertDateFieldSchemaPatternOverridesDefault() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForDateField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			0,
			50,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertOptionalDatetimeFieldSchemaHappyPath() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String dateFieldKey = "date_field";

		Map<String, Object> dateFieldSchema = new HashMap<>();
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.FORMAT_KEY, OpenAPIJsonSchemaValidator.DATE_FORMAT);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(dateFieldKey, dateFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertDateFieldSchemaInvalidFormat() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForDateField(
			cond,
			"not a date",
			null,
			0,
			50,
			null
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("doesn't match the required pattern"));
	}

	private void prepareEnvForDateField(
		OpenAPIJsonSchemaValidatorImpl cond,
		String value,
		String pattern,
		Integer minLength,
		Integer maxLength,
		String enumString
	) {

		String dateFieldKey = "date_field";

		Map<String, Object> dateFieldSchema = new HashMap<>();
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.FORMAT_KEY, OpenAPIJsonSchemaValidator.DATE_FORMAT);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, pattern);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_LENGTH_KEY, minLength);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_LENGTH_KEY, maxLength);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.ENUM_KEY, enumString != null ? List.of(enumString) : null);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(dateFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(dateFieldKey, dateFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(dateFieldKey, value);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertDatetimeFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForDatetimeField(
			cond,
			"2020-07-21T08:30:00Z",
			null,
			0,
			50,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertDatetimeFieldSchemaInvalidFormat() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForDatetimeField(
			cond,
			"9999-99-99T08:30:00Z",
			null,
			0,
			50,
			null
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("doesn't match the required pattern"));
	}

	private void prepareEnvForDatetimeField(
		OpenAPIJsonSchemaValidatorImpl cond,
		String value,
		String pattern,
		Integer minLength,
		Integer maxLength,
		String enumString
	) {

		String dateFieldKey = "date_field";

		Map<String, Object> dateFieldSchema = new HashMap<>();
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.FORMAT_KEY, OpenAPIJsonSchemaValidator.DATETIME_FORMAT);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, pattern);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_LENGTH_KEY, minLength);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_LENGTH_KEY, maxLength);
		dateFieldSchema.put(OpenAPIJsonSchemaValidator.ENUM_KEY, enumString != null ? List.of(enumString) : null);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(dateFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(dateFieldKey, dateFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(dateFieldKey, value);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertIntegerFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerField(
			cond,
			10,
			2,
			15
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryIntegerSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerField(
			cond,
			10,
			2,
			15
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testAssertOptionalIntegerFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String integerFieldKey = "number_field";

		Map<String, Object> integerFieldSchema = new HashMap<>();
		integerFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.INTEGER_TYPE);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(integerFieldKey, integerFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertIntegerFieldSchemaHappyPathEmptyConstraints() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerField(
			cond,
			10,
			null,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertIntegerFieldSchemaInvalidMinValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerField(
			cond,
			10,
			11,
			15
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minimum"));
	}

	@Test
	public void testAssertIntegerFieldSchemaInvalidMaxValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerField(
			cond,
			10,
			2,
			8
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maximum"));
	}

	private void prepareEnvForIntegerField(
		OpenAPIJsonSchemaValidatorImpl cond,
		int value,
		Integer minValue,
		Integer maxValue
	) {

		String integerFieldKey = "number_field";

		Map<String, Object> integerFieldSchema = new HashMap<>();
		integerFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.INTEGER_TYPE);
		integerFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_VALUE_KEY, minValue);
		integerFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_VALUE_KEY, maxValue);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(integerFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(integerFieldKey, integerFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(integerFieldKey, value);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertNumberFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberField(
			cond,
			10.0,
			2.0,
			15.0
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryNumberSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberField(
			cond,
			10.0,
			2.0,
			15.0
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testOptionalAssertNumberFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		String numberFieldKey = "number_field";

		Map<String, Object> numberFieldSchema = new HashMap<>();
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.NUMBER_TYPE);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(numberFieldKey, numberFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertNumberFieldSchemaHappyPathEmptyConstraints() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberField(
			cond,
			10.0,
			null,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertNumberFieldSchemaInvalidMinValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberField(
			cond,
			10.0,
			11.0,
			15.0
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minimum"));
	}

	@Test
	public void testAssertNumberFieldSchemaInvalidMaxValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberField(
			cond,
			10.0,
			2.0,
			8.0
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maximum"));
	}

	private void prepareEnvForNumberField(
		OpenAPIJsonSchemaValidatorImpl cond,
		Number value,
		Number minValue,
		Number maxValue
	) {

		String numberFieldKey = "number_field";

		Map<String, Object> numberFieldSchema = new HashMap<>();
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.NUMBER_TYPE);
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_VALUE_KEY, minValue);
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_VALUE_KEY, maxValue);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(numberFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(numberFieldKey, numberFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(numberFieldKey, value);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertBooleanFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForBooleanField(
			cond,
			false
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryBooleanFieldSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForBooleanField(
			cond,
			false
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testOptionalAssertBooleanFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		String booleanFieldKey = "boolean_field";

		Map<String, Object> booleanFieldSchema = new HashMap<>();
		booleanFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.BOOLEAN_TYPE);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(booleanFieldKey, booleanFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	private void prepareEnvForBooleanField(
		OpenAPIJsonSchemaValidatorImpl cond,
		boolean value
	) {

		String booleanFieldKey = "boolean_field";

		Map<String, Object> booleanFieldSchema = new HashMap<>();
		booleanFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.BOOLEAN_TYPE);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(booleanFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(booleanFieldKey, booleanFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(booleanFieldKey, value);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertUntypedFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForUntypedField(cond, false);

		// Then
		run(cond);
	}

	@Test
	public void testAssertOptionalUntypedFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForUntypedField(cond, true);
		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryUntypedFieldSchemaUnhappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForUntypedField(cond, false);
		loadBodyInEnvironment(new JsonObject());

		// Then
		runAndFail(cond);
	}

	private void prepareEnvForUntypedField(
		OpenAPIJsonSchemaValidatorImpl cond,
		boolean isOptional
	) {

		String untypedFieldKey = "untyped_field";
		Map<String, Object> untypedFieldSchema = new HashMap<>();

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		if (!isOptional) {
			schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(untypedFieldKey));
		}
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(untypedFieldKey, untypedFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(untypedFieldKey, "string"); // Passing any type should work since it's untyped
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertStringArrayFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			1,
			3,
			"string"
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryStringArrayFieldSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			1,
			3,
			"string"
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testOptionalAssertStringArrayFieldSchemaHappyPath() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		String stringArrayFieldKey = "string_array_field";

		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> stringArrayFieldSchema = new HashMap<>();
		stringArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		stringArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, stringFieldSchema);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringArrayFieldKey, stringArrayFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertStringArrayFieldSchemaEmptyConstraints() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			null,
			null,
			null,
			null,
			null,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertStringArrayFieldSchemaInvalidPattern() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string123",
			"^[a-z]{2,15}$",
			2,
			15,
			1,
			3,
			"string123"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("doesn't match the required pattern"));
	}

	@Test
	public void testAssertStringArrayFieldSchemaInvalidMinLength() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			20,
			25,
			1,
			3,
			"string"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minLength"));
	}

	@Test
	public void testAssertStringArrayFieldSchemaInvalidMaxLength() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			3,
			1,
			3,
			"string"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maxLength"));
	}

	@Test
	public void testAssertStringArrayFieldSchemaInvalidMinItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			2,
			3,
			"string"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minItems"));
	}

	@Test
	public void testAssertStringArrayFieldSchemaInvalidMaxItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			0,
			-1,
			"string"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maxItems"));
	}

	@Test
	public void testAssertStringArrayFieldSchemaInvalidEnum() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForStringArrayField(
			cond,
			"string",
			"^[a-z]{2,15}$",
			2,
			15,
			1,
			3,
			"string123"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("does not match any given enumeration"));
	}

	@Test
	public void testAssertArrayWithOneOfSchemaHappyPath() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		List<Map<String, Object>> oneOfSchemas = List.of(
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field1", Map.of("type", "string"),
					"field2", Map.of("type", "string")
				),
				"required", List.of("field1", "field2")
			),
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field3", Map.of("type", "string"),
					"field4", Map.of("type", "string")
				),
				"required", List.of("field3", "field4")
			)
		);

		List<Map<String, Object>> items = List.of(
			Map.of("field1", "value1", "field2", "value2"),
			Map.of("field3", "value3", "field4", "value4")
		);


		prepareEnvForArrayWithOneOf(
			cond,
			"data",
			oneOfSchemas,
			items,
			0,
			10,
			true
		);

		run(cond);
	}

	@Test
	public void testAssertArrayWithOneOfSchemaInvalidItem() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		List<Map<String, Object>> oneOfSchemas = List.of(
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field1", Map.of("type", "string"),
					"field2", Map.of("type", "string")
				),
				"required", List.of("field1", "field2")
			),
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field3", Map.of("type", "string"),
					"field4", Map.of("type", "string")
				),
				"required", List.of("field3", "field4")
			)
		);

		List<Map<String, Object>> items = List.of(
			Map.of("field1", "value1", "field2", "value2"),
			Map.of("invalidField", "invalidValue")
		);

		prepareEnvForArrayWithOneOf(
			cond,
			"data",
			oneOfSchemas,
			items,
			0,
			10,
			true
		);

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("The field data is expected to have exactly one valid schema"));
	}

	@Test
	public void testAssertArrayWithOneOfSchemaInvalidMinItems() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		List<Map<String, Object>> oneOfSchemas = List.of(
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field1", Map.of("type", "string"),
					"field2", Map.of("type", "string")
				),
				"required", List.of("field1", "field2")
			),
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field3", Map.of("type", "string"),
					"field4", Map.of("type", "string")
				),
				"required", List.of("field3", "field4")
			)
		);

		List<Map<String, Object>> items = List.of(
			Map.of("field1", "value1", "field2", "value2"),
			Map.of("field3", "value3", "field4", "value4")
		);

		prepareEnvForArrayWithOneOf(
			cond,
			"data",
			oneOfSchemas,
			items,
			3,
			10,
			true
		);

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("The field data is expected to have at least 3 items"));
	}

	@Test
	public void testAssertArrayWithOneOfSchemaInvalidMaxItems() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		List<Map<String, Object>> oneOfSchemas = List.of(
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field1", Map.of("type", "string"),
					"field2", Map.of("type", "string")
				),
				"required", List.of("field1", "field2")
			),
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field3", Map.of("type", "string"),
					"field4", Map.of("type", "string")
				),
				"required", List.of("field3", "field4")
			)
		);

		List<Map<String, Object>> items = List.of(
			Map.of("field1", "value1", "field2", "value2"),
			Map.of("field3", "value3", "field4", "value4")
		);

		prepareEnvForArrayWithOneOf(
			cond,
			"data",
			oneOfSchemas,
			items,
			0,
			1,
			true
		);

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("The field data is expected to have at most 1 items"));
	}

	@Test
	public void testAssertArrayWithOneOfSchemaFieldIsNotAnArray() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		List<Map<String, Object>> oneOfSchemas = List.of(
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field1", Map.of("type", "string"),
					"field2", Map.of("type", "string")
				),
				"required", List.of("field1", "field2")
			),
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field3", Map.of("type", "string"),
					"field4", Map.of("type", "string")
				),
				"required", List.of("field3", "field4")
			)
		);

		prepareEnvForArrayWithOneOf(
			cond,
			"data",
			oneOfSchemas,
			"this is not an array",
			0,
			10,
			true
		);

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("The field data is expected to be an array"));
	}

	@Test
	public void testRequiredFieldNotPresentInArrayWithOneOf() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		List<Map<String, Object>> oneOfSchemas = List.of(
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field1", Map.of("type", "string"),
					"field2", Map.of("type", "string")
				),
				"required", List.of("field1", "field2")
			),
			Map.of(
				"type", "object",
				"properties", Map.of(
					"field3", Map.of("type", "string"),
					"field4", Map.of("type", "string")
				),
				"required", List.of("field3", "field4")
			)
		);

		prepareEnvForArrayWithOneOf(
			cond,
			"data",
			oneOfSchemas,
			List.of(),
			1,
			10,
			false
		);

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("The field data is required but not present"));
	}


	private void prepareEnvForStringArrayField(
		OpenAPIJsonSchemaValidatorImpl cond,
		String value,
		String pattern,
		Integer minLength,
		Integer maxLength,
		Integer minItems,
		Integer maxItems,
		String enumString
	) {

		String stringArrayFieldKey = "string_array_field";

		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, pattern);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_LENGTH_KEY, minLength);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_LENGTH_KEY, maxLength);
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.ENUM_KEY, enumString != null ? List.of(enumString) : null);

		Map<String, Object> stringArrayFieldSchema = new HashMap<>();
		stringArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		stringArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, stringFieldSchema);
		stringArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_ITEMS_KEY, minItems);
		stringArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_ITEMS_KEY, maxItems);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(stringArrayFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringArrayFieldKey, stringArrayFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonArray stringArray = new JsonArray();
		stringArray.add(value);

		JsonObject body = new JsonObject();
		body.add(stringArrayFieldKey, stringArray);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertIntegerArrayFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerArrayField(
			cond,
			10,
			2,
			15,
			1,
			3
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryIntegerArrayFieldSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerArrayField(
			cond,
			10,
			2,
			15,
			1,
			3
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testOptionalAssertIntegerArrayFieldSchemaHappyPath() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		String intArrayFieldKey = "int_array_field";

		Map<String, Object> intFieldSchema = new HashMap<>();
		intFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.INTEGER_TYPE);

		Map<String, Object> intArrayFieldSchema = new HashMap<>();
		intArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		intArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, intFieldSchema);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(intArrayFieldKey, intArrayFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertIntegerArrayFieldSchemaHappyPathEmptyConstraints() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerArrayField(
			cond,
			10,
			null,
			null,
			null,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertIntegerArrayFieldSchemaInvalidMinValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerArrayField(
			cond,
			10,
			11,
			15,
			1,
			3
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minimum"));
	}

	@Test
	public void testAssertIntegerArrayFieldSchemaInvalidMaxValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerArrayField(
			cond,
			10,
			2,
			8,
			1,
			3
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maximum"));
	}

	@Test
	public void testAssertIntegerArrayFieldSchemaInvalidMinItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerArrayField(
			cond,
			10,
			2,
			15,
			3,
			3
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minItems"));
	}

	@Test
	public void testAssertIntegerArrayFieldSchemaInvalidMaxItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForIntegerArrayField(
			cond,
			10,
			2,
			15,
			0,
			-1
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maxItems"));
	}

	private void prepareEnvForIntegerArrayField(
		OpenAPIJsonSchemaValidatorImpl cond,
		int value,
		Integer minValue,
		Integer maxValue,
		Integer minItems,
		Integer maxItems
	) {

		String integerArrayFieldKey = "integer_array_field";

		Map<String, Object> integerFieldSchema = new HashMap<>();
		integerFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.INTEGER_TYPE);
		integerFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_VALUE_KEY, minValue);
		integerFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_VALUE_KEY, maxValue);

		Map<String, Object> integerArrayFieldSchema = new HashMap<>();
		integerArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		integerArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, integerFieldSchema);
		integerArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_ITEMS_KEY, minItems);
		integerArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_ITEMS_KEY, maxItems);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(integerArrayFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(integerArrayFieldKey, integerArrayFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonArray integerArray = new JsonArray();
		integerArray.add(value);

		JsonObject body = new JsonObject();
		body.add(integerArrayFieldKey, integerArray);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertNumberArrayFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberArrayField(
			cond,
			10.0,
			2.0,
			15.0,
			1,
			3
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryNumberArrayFieldSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberArrayField(
			cond,
			10.0,
			2.0,
			15.0,
			1,
			3
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testOptionalAssertNumberArrayFieldSchemaHappyPath() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		String numberArrayFieldKey = "number_array_field";

		Map<String, Object> numberFieldSchema = new HashMap<>();
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.NUMBER_TYPE);

		Map<String, Object> numberArrayFieldSchema = new HashMap<>();
		numberArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		numberArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, numberFieldSchema);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(numberArrayFieldKey, numberArrayFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertNumberArrayFieldSchemaHappyPathEmptyConstraints() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberArrayField(
			cond,
			10.0,
			null,
			null,
			null,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertNumberArrayFieldSchemaInvalidMinValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberArrayField(
			cond,
			10.0,
			11.0,
			15.0,
			1,
			3
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minimum"));
	}

	@Test
	public void testAssertNumberArrayFieldSchemaInvalidMaxValue() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberArrayField(
			cond,
			10.0,
			2.0,
			8.0,
			1,
			3
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maximum"));
	}

	@Test
	public void testAssertNumberArrayFieldSchemaInvalidMinItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberArrayField(
			cond,
			10.0,
			2.0,
			15.0,
			3,
			3
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minItems"));
	}

	@Test
	public void testAssertNumberArrayFieldSchemaInvalidMaxItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForNumberArrayField(
			cond,
			10.0,
			2.0,
			15.0,
			0,
			-1
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maxItems"));
	}

	private void prepareEnvForNumberArrayField(
		OpenAPIJsonSchemaValidatorImpl cond,
		Number value,
		Number minValue,
		Number maxValue,
		Integer minItems,
		Integer maxItems
	) {

		String numberArrayFieldKey = "number_array_field";

		Map<String, Object> numberFieldSchema = new HashMap<>();
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.NUMBER_TYPE);
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_VALUE_KEY, minValue);
		numberFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_VALUE_KEY, maxValue);

		Map<String, Object> numberArrayFieldSchema = new HashMap<>();
		numberArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		numberArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, numberFieldSchema);
		numberArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_ITEMS_KEY, minItems);
		numberArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_ITEMS_KEY, maxItems);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(numberArrayFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(numberArrayFieldKey, numberArrayFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonArray numberArray = new JsonArray();
		numberArray.add(value);

		JsonObject body = new JsonObject();
		body.add(numberArrayFieldKey, numberArray);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertObjectArrayFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForObjectArrayField(
			cond,
			1,
			15
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertMandatoryObjectArrayFieldSchema() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		prepareEnvForObjectArrayField(
			cond,
			1,
			15
		);
		loadBodyInEnvironment(new JsonObject());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Unable to find element"));
	}

	@Test
	public void testAssertOptionalObjectArrayFieldSchemaHappyPath() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		String objArrayFieldKey = "obj_array_field";

		Map<String, Object> objFieldSchema = new HashMap<>();
		objFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);

		Map<String, Object> objArrayFieldSchema = new HashMap<>();
		objArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		objArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, objFieldSchema);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(objArrayFieldKey, objArrayFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		loadBodyInEnvironment(new JsonObject());

		// Then
		run(cond);
	}

	@Test
	public void testAssertObjectArrayFieldSchemaHappyPathEmptyConstraints() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForObjectArrayField(
			cond,
			null,
			null
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertObjectArrayFieldSchemaInvalidMinItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForObjectArrayField(
			cond,
			2,
			3
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("less than the required minItems"));
	}

	@Test
	public void testAssertObjectArrayFieldSchemaInvalidMaxItems() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForObjectArrayField(
			cond,
			0,
			-1
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("more than the required maxItems"));
	}

	private void prepareEnvForObjectArrayField(
		OpenAPIJsonSchemaValidatorImpl cond,
		Integer minItems,
		Integer maxItems
	) {

		String stringFieldKey = "string_field";
		String objectArrayFieldKey = "object_array_field";

		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> objectFieldSchema = new HashMap<>();
		objectFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		objectFieldSchema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(stringFieldKey));
		objectFieldSchema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));

		Map<String, Object> referenceSchema = new HashMap<>();
		referenceSchema.put(OpenAPIJsonSchemaValidator.REFERENCE_KEY, "ObjectSchema");

		Map<String, Object> objectArrayFieldSchema = new HashMap<>();
		objectArrayFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		objectArrayFieldSchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, referenceSchema);
		objectArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MIN_ITEMS_KEY, minItems);
		objectArrayFieldSchema.put(OpenAPIJsonSchemaValidator.MAX_ITEMS_KEY, maxItems);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(objectArrayFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(objectArrayFieldKey, objectArrayFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());
		doReturn(objectFieldSchema).when(cond).getSchema(anyString());

		JsonObject object = new JsonObject();
		object.addProperty(stringFieldKey, "string");

		JsonArray objectArray = new JsonArray();
		objectArray.add(object);

		JsonObject body = new JsonObject();
		body.add(objectArrayFieldKey, objectArray);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertAllOfFieldSchemaHappyPath() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForAllOfField(
			cond,
			"abc",
			"123",
			"^[a-z]{1,3}$",
			"^[1-9]{1,3}$"
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertAllOfFieldSchemaOnePatternIsWrong() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForAllOfField(
			cond,
			"abc",
			"abc",
			"^[a-z]{1,3}$",
			"^[1-9]{1,3}$"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Value from element string2 doesn't match the required pattern"));
	}

	@Test
	public void testAssertAllOfFieldSchemaOnePatternIsWrong2() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForAllOfField(
			cond,
			"123",
			"123",
			"^[a-z]{1,3}$",
			"^[1-9]{1,3}$"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Value from element string1 doesn't match the required pattern"));
	}

	@Test
	public void testAssertAllOfFieldSchemaBothPatternsAreWrong() {
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForAllOfField(
			cond,
			"123",
			"abc",
			"^[a-z]{1,3}$",
			"^[1-9]{1,3}$"
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Value from element string1 doesn't match the required pattern"));
	}

	@Test
	public void testAssertOneOfFieldSchemaHappyPath() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForOneOfField(
			cond,
			"string",
			"^[a-z]{1,10}$",
			"^[a-z]{11,20}$",
			false
		);

		// Then
		run(cond);
	}

	@Test
	public void testAssertOneOfFieldSchemaNoPatternMatches() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForOneOfField(
			cond,
			"string",
			"^[a-z]{11,20}$",
			"^[a-z]{21,30}$",
			false
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("expected to have exactly one valid schema"));
	}

	@Test
	public void testAssertOneOfFieldSchemaMoreThanOneMatches() {

		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForOneOfField(
			cond,
			"string",
			"^[a-z]{1,10}$",
			"^[a-z]{1,9}$",
			false
		);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("expected to have exactly one valid schema"));
	}

	@Test
	public void testAssertOneOfFieldSchemaFieldMissingRequired() {
		// Given
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForOneOfField(cond, null, "^[a-z]{1,10}$", "^[a-z]{11,20}$", false);

		// When
		ConditionError error = runAndFail(cond);

		// Then
		assertThat(error.getMessage(), containsString("is required but not present"));
	}

	@Test
	public void testAssertOneOfFieldSchemaFieldMissingOptional() {
		// Given
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		prepareEnvForOneOfField(cond, null, "^[a-z]{1,10}$", "^[a-z]{11,20}$", true);

		// When
		run(cond);

	}

	@Test
	public void testAdditionalPropertiesTrue() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));
		schema.put(OpenAPIJsonSchemaValidator.ADDITIONAL_PROPERTIES_KEY, true);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, "value");
		body.addProperty("additional_field", "additional_value");
		loadBodyInEnvironment(body);

		// Then
		run(cond);
	}

	@Test
	public void testAdditionalPropertiesFalse() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));
		schema.put(OpenAPIJsonSchemaValidator.ADDITIONAL_PROPERTIES_KEY, false);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, "value");
		body.addProperty("additional_field", "additional_value");
		loadBodyInEnvironment(body);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Additional properties are not allowed"));
	}

	@Test
	public void testAdditionalPropertiesSchema() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> additionalPropertiesSchema = new HashMap<>();
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, "^[a-z]+$");

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));
		schema.put(OpenAPIJsonSchemaValidator.ADDITIONAL_PROPERTIES_KEY, additionalPropertiesSchema);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, "value");
		body.addProperty("additional_field", "valid");
		loadBodyInEnvironment(body);

		// Then
		run(cond);

		// Test with invalid additional property
		body.addProperty("invalid_additional_field", "123");
		loadBodyInEnvironment(body);

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("doesn't match the required pattern"));
	}

	@Test
	public void testAdditionalPropertiesSchemaInvalidValue() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> additionalPropertiesSchema = new HashMap<>();
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, "^[a-z]+$");

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));
		schema.put(OpenAPIJsonSchemaValidator.ADDITIONAL_PROPERTIES_KEY, additionalPropertiesSchema);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, "value");
		body.addProperty("additional_field", "123"); // Invalid value

		loadBodyInEnvironment(body);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("doesn't match the required pattern"));
	}

	@Test
	public void testAdditionalPropertiesSchemaInvalidType() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> additionalPropertiesSchema = new HashMap<>();
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, "^[a-z]+$");

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));
		schema.put(OpenAPIJsonSchemaValidator.ADDITIONAL_PROPERTIES_KEY, additionalPropertiesSchema);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, "value");
		body.addProperty("additional_field", 123); // Invalid type

		loadBodyInEnvironment(body);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Field at additional_field must be a string but JsonPrimitive was found"));
	}

	@Test
	public void testAdditionalPropertiesSchemaInvalidNestedObject() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		Map<String, Object> nestedSchema = new HashMap<>();
		nestedSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		nestedSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, "^[a-z]+$");

		Map<String, Object> additionalPropertiesSchema = new HashMap<>();
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		additionalPropertiesSchema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of("nested_field", nestedSchema));

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));
		schema.put(OpenAPIJsonSchemaValidator.ADDITIONAL_PROPERTIES_KEY, additionalPropertiesSchema);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, "value");
		JsonObject invalidNestedObject = new JsonObject();
		invalidNestedObject.addProperty("nested_field", "123"); // Invalid nested value
		body.add("additional_field", invalidNestedObject);

		loadBodyInEnvironment(body);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("doesn't match the required pattern"));
	}

	@Test
	public void testAdditionalPropertiesMissingObject() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		String objectFieldKey = "object_field";
		Map<String, Object> objectFieldSchema = new HashMap<>();
		objectFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		objectFieldSchema.put(OpenAPIJsonSchemaValidator.ADDITIONAL_PROPERTIES_KEY,
			Map.of(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE));

		Map<String, Object> propertiesSchema = new HashMap<>();
		propertiesSchema.put(stringFieldKey, stringFieldSchema);
		propertiesSchema.put(objectFieldKey, objectFieldSchema);

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, propertiesSchema);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.addProperty(stringFieldKey, "value");

		loadBodyInEnvironment(body);

		// Then
		run(cond);
	}


	private void prepareEnvForOneOfField(
		OpenAPIJsonSchemaValidatorImpl cond,
		String value,
		String firstPattern,
		String secondPattern,
		boolean isOptional
	) {

		Map<String, Object> firstStringFieldSchema = new HashMap<>();
		firstStringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		firstStringFieldSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, firstPattern);

		Map<String, Object> secondStringFieldSchema = new HashMap<>();
		secondStringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);
		secondStringFieldSchema.put(OpenAPIJsonSchemaValidator.PATTERN_KEY, secondPattern);

		String oneOfFieldKey = "one_of_field";
		Map<String, Object> oneOfSchema = new HashMap<>();
		oneOfSchema.put(OpenAPIJsonSchemaValidator.ONE_OF_KEY, List.of(firstStringFieldSchema, secondStringFieldSchema));

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);

		// Mark the field as optional or required
		if (isOptional) {
			schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(oneOfFieldKey, oneOfSchema));
		} else {
			schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(oneOfFieldKey));
			schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(oneOfFieldKey, oneOfSchema));
		}

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		if (value != null) {
			body.addProperty(oneOfFieldKey, value);
		}
		loadBodyInEnvironment(body);
	}

	private void prepareEnvForAllOfField(
		OpenAPIJsonSchemaValidatorImpl cond,
		String value,
		String value2,
		String firstPattern,
		String secondPattern
	) {

		Map<String, Object> firstObjectSchema = new HashMap<>();
		firstObjectSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		firstObjectSchema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of("string1"));
		firstObjectSchema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(
			"string1", Map.of(
				OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE,
				OpenAPIJsonSchemaValidator.PATTERN_KEY, firstPattern
			)
		));

		Map<String, Object> secondObjectSchema = new HashMap<>();
		secondObjectSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		secondObjectSchema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of("string2"));
		secondObjectSchema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(
			"string2", Map.of(
				OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE,
				OpenAPIJsonSchemaValidator.PATTERN_KEY, secondPattern
			)
		));
		String allOfFieldKey = "all_of_field";
		Map<String, Object> allOfSchema = new HashMap<>();
		allOfSchema.put(OpenAPIJsonSchemaValidator.ALL_OF_KEY, List.of(firstObjectSchema, secondObjectSchema));

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(allOfFieldKey, allOfSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		JsonObject allOfField = new JsonObject();
		allOfField.addProperty("string1", value);
		allOfField.addProperty("string2", value2);
		body.add(allOfFieldKey, allOfField);
		loadBodyInEnvironment(body);
	}

	@Test
	public void testAssertOptionalObjectFieldSchemaHappyPath() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		String stringFieldKey = "string_field";
		Map<String, Object> stringFieldSchema = new HashMap<>();
		stringFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.STRING_TYPE);

		String objFieldKey = "object_field";
		Map<String, Object> objectFieldSchema = new HashMap<>();
		objectFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		objectFieldSchema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(stringFieldKey, stringFieldSchema));

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(objFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(objFieldKey, objectFieldSchema));

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		body.add(objFieldKey, new JsonObject());
		loadBodyInEnvironment(body);

		// Then
		run(cond);
	}

	@Test
	public void testInvalidType() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());
		String invalidFieldKey = "invalid_field";

		Map<String, Object> invalidFieldSchema = new HashMap<>();
		invalidFieldSchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, "invalid");

		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(invalidFieldKey, invalidFieldSchema));


		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = new JsonObject();
		loadBodyInEnvironment(body);

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("invalid type"));
	}

	@Test
	public void testNoStatusCode() {
		// When
		OpenAPIJsonSchemaValidatorImpl cond = spy(new OpenAPIJsonSchemaValidatorImpl());

		JsonObject responseBody = new JsonObject();
		responseBody.addProperty("body", new JsonObject().toString());
		environment.putObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey(), responseBody);
		doNothing().when(cond).loadOpenApiSpec(anyString());

		// Then
		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("Could not find the status code"));
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasValue2Boolean() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field1", "exists");
		data.addProperty("field2", true);
		cond.assertField1IsRequiredWhenField2HasValue2(data, "field1", "field2", true);
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasValue2BooleanOtherValue2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field1", "exists");
		data.addProperty("field2", false);
		cond.assertField1IsRequiredWhenField2HasValue2(data, "field1", "field2", true);
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasValue2BooleanNullValue2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field1", "exists");
		cond.assertField1IsRequiredWhenField2HasValue2(data, "field1", "field2", true);
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasValue2BooleanNoField1() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field2", true);
		try {
			cond.assertField1IsRequiredWhenField2HasValue2(data, "field1", "field2", true);
		} catch (ConditionError error) {
			assertThat(error.getMessage(), containsString("is required when"));
			return;
		}
		throw new AssertionError("The condition passed but we expected it to fail");

	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasValue2BooleanNoField1OtherValue2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field2", false);
		cond.assertField1IsRequiredWhenField2HasValue2(data, "field1", "field2", true);
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2ListValueNotPresent() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field1", "exists");
		data.addProperty("field2", "value");
		cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", List.of("some value", "other value"));
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2ListValuePresentNoField2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field1", "exists");
		cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", List.of("some value", "other value", "value"));
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2ListNoField1() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field2", "value");
		try {
			cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", List.of("some value", "other value"));
		} catch (ConditionError error) {
			assertThat(error.getMessage(), containsString("is required when"));
			return;
		}
		throw new AssertionError("The condition passed but we expected it to fail");
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2ListNoField1And2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		try {
			cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", List.of("some value", "other value"));
		} catch (ConditionError error) {
			assertThat(error.getMessage(), containsString("is required when"));
			return;
		}
		throw new AssertionError("The condition passed but we expected it to fail");
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2ListNoField1OtherValue() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field2", "value");
		cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", List.of("some value", "other value", "value"));
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2ValueNotPresent() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field1", "exists");
		data.addProperty("field2", "value");
		cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", "some value");
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2ValuePresentNoField2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field1", "exists");
		cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", "some value");
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2NoField1() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field2", "value");
		try {
			cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", "some value");
		} catch (ConditionError error) {
			assertThat(error.getMessage(), containsString("is required when"));
			return;
		}
		throw new AssertionError("The condition passed but we expected it to fail");
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2NoField1And2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		try {
			cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", "some value");
		} catch (ConditionError error) {
			assertThat(error.getMessage(), containsString("is required when"));
			return;
		}
		throw new AssertionError("The condition passed but we expected it to fail");
	}

	@Test
	public void testAssertField1IsRequiredWhenField2HasNotValue2NoField1OtherValue() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObject();
		data.addProperty("field2", "value");
		cond.assertField1IsRequiredWhenField2HasNotValue2(data, "field1", "field2", "value");
	}

	@Test
	public void assertField1isRequiredWhenField2IsNotPresent() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.addField("object1.value1", "value1")
			.build();

		cond.assertField1isRequiredWhenField2IsNotPresent(data, "object1.value1", "object2.value2");

	}

	@Test
	public void assertField1isRequiredWhenField2IsNotPresentUnhappy() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.build();

		try {
			cond.assertField1isRequiredWhenField2IsNotPresent(data, "object1.value1", "object2.value2");
		} catch (ConditionError e) {
			assertThat(e.getMessage(), containsString("Field 'object1.value1' is required when field 'object2.value2' is not present"));
			return;
		}
		throw new AssertionError("The condition passed but we expected it to fail");
	}

	@Test
	public void assertField1isRequiredWhenField2IsNotPresentHappy2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.addField("object2.value2", "value2")
			.build();
		cond.assertField1isRequiredWhenField2IsNotPresent(data, "object1.value1", "object2.value2");
	}

	@Test
	public void assertField1IsRequiredWhenField2HasValue2ArrayHappy() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.addField("object1.value1", "value1")
			.addField("object2.value2", "value2.1")
			.build();
		cond.assertField1IsRequiredWhenField2HasValue2(data, "object1.value1", "object2.value2", List.of("value2.1", "value2.2"));
	}

	@Test
	public void assertField1IsRequiredWhenField2HasValue2ArrayHappy2() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.addField("object1.value1", "value1")
			.addField("object2.value2", "value2.2")
			.build();
		cond.assertField1IsRequiredWhenField2HasValue2(data, "object1.value1", "object2.value2", List.of("value2.1", "value2.2"));
	}

	@Test
	public void assertField1IsRequiredWhenField2HasValue2ArrayHappy3() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.addField("object2.value2", "differentValue")
			.build();
		cond.assertField1IsRequiredWhenField2HasValue2(data, "object1.value1", "object2.value2", List.of("value2.1", "value2.2"));
	}

	@Test
	public void assertField1IsRequiredWhenField2HasValue2ArrayUnhappyField2IsNotPresent() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.addField("object1.value1", "value1")
			.build();
		cond.assertField1IsRequiredWhenField2HasValue2(data, "object1.value1", "object2.value2", List.of("value2.1", "value2.2"));
	}

	@Test
	public void assertField1IsRequiredWhenField2HasValue2ArrayUnhappyField1IsNotPresent() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
		JsonObject data = new JsonObjectBuilder()
			.addField("object2.value2", "value2.2")
			.build();
		try {
			cond.assertField1IsRequiredWhenField2HasValue2(data, "object1.value1", "object2.value2", List.of("value2.1", "value2.2"));
		} catch (ConditionError e) {
			assertThat(e.getMessage(), containsString("object1.value1 is required when object2.value2 is [value2.1, value2.2]"));
			return;
		}
		throw new AssertionError("The condition passed but we expected it to fail");
	}

	@Test
	public void assertField1IsMutuallyExclusiveWithField2HappyPath() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);

		JsonObject data = new JsonObjectBuilder()
			.addField("field1", "value1")
			.build();

		cond.assertField1IsMutuallyExclusiveWithField2(data, "field1", "field2");

		data = new JsonObjectBuilder()
			.addField("field2", "value2")
			.build();

		cond.assertField1IsMutuallyExclusiveWithField2(data, "field1", "field2");
	}

	@Test
	public void assertField1IsMutuallyExclusiveWithField2UnhappyPath() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);

		JsonObject data = new JsonObjectBuilder()
			.addField("field1", "value1")
			.addField("field2", "value2")
			.build();

		try {
			cond.assertField1IsMutuallyExclusiveWithField2(data, "field1", "field2");
		} catch (ConditionError e) {
			assertThat(e.getMessage(), containsString("field1 and field2 cannot both be present; they are mutually exclusive."));
			return;
		}

		throw new AssertionError("The condition passed but we expected it to fail");
	}

	@Test
	public void assertField2IsNotPresentWhenField1HasValue1HappyPath() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);

		JsonObject data = new JsonObjectBuilder()
			.addField("field1", "value1")
			.build();

		cond.assertField2IsNotPresentWhenField1HasValue1(data, "field1", "value1", "field2");

		data = new JsonObjectBuilder()
			.addField("field1", "differentValue")
			.addField("field2", "value2")
			.build();

		cond.assertField2IsNotPresentWhenField1HasValue1(data, "field1", "value1", "field2");
	}

	@Test
	public void assertField2IsNotPresentWhenField1HasValue1UnhappyPath() {
		OpenAPIJsonSchemaValidatorImpl cond = new OpenAPIJsonSchemaValidatorImpl();
		TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
		cond.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);

		JsonObject data = new JsonObjectBuilder()
			.addField("field1", "value1")
			.addField("field2", "value2")
			.build();

		try {
			cond.assertField2IsNotPresentWhenField1HasValue1(data, "field1", "value1", "field2");
		} catch (ConditionError e) {
			assertThat(e.getMessage(), containsString("field2 cannot be present when field1 has the value value1"));
			return;
		}

		throw new AssertionError("The condition passed but we expected it to fail");
	}


	private void loadBodyInEnvironment(JsonObject body) {
		JsonObject responseBody = new JsonObject();
		responseBody.addProperty("body", body.toString());

		environment.putObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey(), responseBody);
		setStatusCode(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey(), 201);
	}

	private void prepareEnvForArrayWithOneOf(
		OpenAPIJsonSchemaValidatorImpl cond,
		String arrayFieldKey,
		List<Map<String, Object>> oneOfSchemas,
		Object fieldValue,
		Integer minItems,
		Integer maxItems,
		boolean includeField
	) {
		Map<String, Object> arraySchema = createArraySchema(oneOfSchemas, minItems, maxItems);

		Map<String, Object> schema = createMainSchema(arrayFieldKey, arraySchema);

		doNothing().when(cond).loadOpenApiSpec(anyString());
		doReturn(schema).when(cond).getSchema(anyString(), anyString(), anyInt());

		JsonObject body = createRequestBody(arrayFieldKey, fieldValue, includeField);

		loadBodyInEnvironment(body);
	}

	private Map<String, Object> createArraySchema(
		List<Map<String, Object>> oneOfSchemas,
		Integer minItems,
		Integer maxItems
	) {
		Map<String, Object> arraySchema = new HashMap<>();
		arraySchema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.ARRAY_TYPE);
		arraySchema.put(OpenAPIJsonSchemaValidator.ITEMS_KEY, Map.of("oneOf", oneOfSchemas));
		arraySchema.put(OpenAPIJsonSchemaValidator.MIN_ITEMS_KEY, minItems);
		arraySchema.put(OpenAPIJsonSchemaValidator.MAX_ITEMS_KEY, maxItems);
		return arraySchema;
	}

	private Map<String, Object> createMainSchema(
		String arrayFieldKey,
		Map<String, Object> arraySchema
	) {
		Map<String, Object> schema = new HashMap<>();
		schema.put(OpenAPIJsonSchemaValidator.TYPE_KEY, OpenAPIJsonSchemaValidator.OBJECT_TYPE);
		schema.put(OpenAPIJsonSchemaValidator.REQUIRED_KEY, List.of(arrayFieldKey));
		schema.put(OpenAPIJsonSchemaValidator.PROPERTIES_KEY, Map.of(arrayFieldKey, arraySchema));
		return schema;
	}


	private JsonObject createRequestBody(
		String arrayFieldKey,
		Object fieldValue,
		boolean includeField
	) {
		JsonObject body = new JsonObject();

		if (includeField) {
			if (fieldValue instanceof List) {
				JsonArray jsonArray = createJsonArrayFromList((List<Map<String, Object>>) fieldValue);
				body.add(arrayFieldKey, jsonArray);
			} else {
				addPropertyToJsonObject(body, arrayFieldKey, fieldValue);
			}
		} else {
			body.addProperty("otherField", "value");
		}

		return body;
	}

	private JsonArray createJsonArrayFromList(List<Map<String, Object>> items) {
		JsonArray jsonArray = new JsonArray();
		items.stream()
			.map(this::createJsonObjectFromMap)
			.forEach(jsonArray::add);
		return jsonArray;
	}

	private JsonObject createJsonObjectFromMap(Map<String, Object> item) {
		JsonObject jsonItem = new JsonObject();
		item.forEach((key, value) -> addPropertyToJsonObject(jsonItem, key, value));
		return jsonItem;
	}

	private void addPropertyToJsonObject(JsonObject jsonObject, String key, Object value) {
		if (value instanceof String) {
			jsonObject.addProperty(key, (String) value);
		} else if (value instanceof Number) {
			jsonObject.addProperty(key, (Number) value);
		} else if (value instanceof Boolean) {
			jsonObject.addProperty(key, (Boolean) value);
		} else {
			throw new IllegalArgumentException("Unsupported field value type: " + value.getClass().getSimpleName());
		}
	}

	private static class OpenAPIJsonSchemaValidatorImpl extends OpenAPIJsonSchemaValidator {
		@Override
		protected String getPathToOpenApiSpec() {
			return "";
		}

		@Override
		protected String getEndpointPath() {
			return "";
		}

		@Override
		protected HttpMethod getEndpointMethod() {
			return HttpMethod.POST;
		}
	}
}
