package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureUpdatedAtDateTimeFieldWasUpdated;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureUpdatedAtDateTimeFieldWasUpdatedTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";
	protected static final String UPDATED_AT_DATE_TIME = "2021-05-21T08:30:00Z";
	protected static final String UPDATED_AT_DATE_TIME_FUTURE = "2021-05-21T08:30:10Z";
	protected static final String UPDATED_AT_DATE_TIME_PAST = "2021-05-21T08:29:50Z";

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void happyPathTest() {
		environment.putString("saved_instant", UPDATED_AT_DATE_TIME_PAST);
		run(new EnsureUpdatedAtDateTimeFieldWasUpdated());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void happyPathTestUpdatedAtDateTimeNotUpdated() {
		environment.putString("saved_instant", UPDATED_AT_DATE_TIME);
		run(new EnsureUpdatedAtDateTimeFieldWasUpdated());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putString("saved_instant", UPDATED_AT_DATE_TIME_PAST);
		environment.putObject(EnsureUpdatedAtDateTimeFieldWasUpdated.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putString("saved_instant", UPDATED_AT_DATE_TIME_PAST);
		environment.putObject(EnsureUpdatedAtDateTimeFieldWasUpdated.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "MissingUpdatedAtDateTime200Response.json")
	public void unhappyPathTestMissingUpdatedAtDateTime() {
		environment.putString("saved_instant", UPDATED_AT_DATE_TIME_PAST);
		unhappyPathTest("Could not extract updatedAtDateTime field from response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void unhappyPathTestUpdatedAtDateTimeUpdatedToThePast() {
		environment.putString("saved_instant", UPDATED_AT_DATE_TIME_FUTURE);
		unhappyPathTest("The current value for updatedAtDateTime should be equal or in the future in relation to the instant before the call to the API");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureUpdatedAtDateTimeFieldWasUpdated());
		assertTrue(error.getMessage().contains(message));
	}
}
