package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetVariableIncomesTransactionsCurrentV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/variableIncomes/v1.2.0/transactions.json")
	public void happyPath() {
		run(new GetVariableIncomesTransactionsCurrentV1OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/variableIncomes/v1.2.0/transactionsNoBrokerNoteId.json")
	public void unhappyPathNoBrokerNoteId() {
		ConditionError e = runAndFail(new GetVariableIncomesTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("brokerNoteId is required when transactionType is [COMPRA, VENDA]"));
	}

	@Test
	@UseResurce("jsonResponses/variableIncomes/v1.2.0/transactionsNoTransactionQuantity.json")
	public void unhappyPathNoTransactionQuantity() {
		ConditionError e = runAndFail(new GetVariableIncomesTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("transactionQuantity is required when transactionType is [COMPRA, VENDA]"));
	}

	@Test
	@UseResurce("jsonResponses/variableIncomes/v1.2.0/transactionsNoTypeAdditionalInfo.json")
	public void unhappyPathNoTypeAdditionalInfo() {
		ConditionError e = runAndFail(new GetVariableIncomesTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("transactionTypeAdditionalInfo is required when transactionType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/variableIncomes/v1.2.0/transactionsNoUnitPrice.json")
	public void unhappyPathNoUnitPrice() {
		ConditionError e = runAndFail(new GetVariableIncomesTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("transactionUnitPrice is required when transactionType is [COMPRA, VENDA"));
	}


}
