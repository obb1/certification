package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExtractInvestmentIDTest extends AbstractJsonResponseConditionUnitTest {


	@UseResurce("jsonResponses/funds/v1.0.0/list.json")
	@Test
	public void happyPath() {
		ExtractInvestmentID condition = new ExtractInvestmentID();
		run(condition);

		assertEquals(environment.getString("investmentId"), "92792126019929200000000000000000000000000");
	}

	@UseResurce("jsonResponses/funds/v1.0.0/listNoData.json")
	@Test
	public void unhappyPath() {
		ExtractInvestmentID condition = new ExtractInvestmentID();
		var error = runAndFail(condition);

		assertTrue(error.getMessage().contains("ExtractInvestmentID: data array does not have any investment"));
	}
}
