package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureConsentsRejection;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureConsentsRejectionReasonCodeWasContaNaoPermitePagamentoV4Test extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonResponses/paymentInitiation/consent/v4/GetPaymentsConsentV4Rejected";

	@Test
	@UseResurce(BASE_JSON_PATH + "ContaNaoPermitePagamento.json")
	public void happyPathTest() {
		run(new EnsureConsentsRejectionReasonCodeWasContaNaoPermitePagamentoV4());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "SaldoInsuficiente.json")
	public void unhappyPathTest() {
		ConditionError error = runAndFail(new EnsureConsentsRejectionReasonCodeWasContaNaoPermitePagamentoV4());
		assertTrue(error.getMessage().contains("Payment rejection code is not what was expected"));
	}
}
