package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditFidoRegistrationRequestBodyToAddClientExtensionResultsTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String RESOURCE_VALUE = "jsonRequests/payments/enrollments/postFidoRegistration.json";
	protected static final String RESOURCE_KEY = "resource_request_entity_claims";

	@Test
	@UseResurce(value = RESOURCE_VALUE, key = RESOURCE_KEY)
	public void happyPathTestNullClientExtensionResults() {
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = RESOURCE_VALUE, key = RESOURCE_KEY)
	public void happyPathTestEmptyClientExtensionResults() {
		environment.putObject("client_extension_results", new JsonObject());
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = RESOURCE_VALUE, key = RESOURCE_KEY)
	public void happyPathTestPopulatedClientExtensionResults() {
		addSimpleClientExtensionResultsToEnv();
		happyPathTest(true);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingRequest() {
		run(new EditFidoRegistrationRequestBodyToAddClientExtensionResults());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(RESOURCE_KEY, new JsonObject());
		addSimpleClientExtensionResultsToEnv();
		unhappyPathTest("Could not find data inside the request body");
	}

	protected void happyPathTest(boolean hasClientExtensionResults) {
		run(new EditFidoRegistrationRequestBodyToAddClientExtensionResults());
		JsonObject data = environment.getObject(RESOURCE_KEY).getAsJsonObject("data");
		if (hasClientExtensionResults) {
			assertTrue(data.has("clientExtensionResults"));
		} else {
			assertFalse(data.has("clientExtensionResults"));
		}
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditFidoRegistrationRequestBodyToAddClientExtensionResults());
		assertTrue(error.getMessage().contains(message));
	}

	protected void addSimpleClientExtensionResultsToEnv() {
		environment.putObject("client_extension_results",
			new JsonObjectBuilder().addField("appId", true).build()
		);
	}
}
