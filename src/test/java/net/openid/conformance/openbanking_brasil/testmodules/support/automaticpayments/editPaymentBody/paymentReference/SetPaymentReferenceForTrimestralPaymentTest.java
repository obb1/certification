package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.paymentReference;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForTrimestralPayment;
import org.junit.Test;

public class SetPaymentReferenceForTrimestralPaymentTest extends AbstractSetPaymentReferenceTest {

	@Override
	protected AbstractSetPaymentReference condition() {
		return new SetPaymentReferenceForTrimestralPayment();
	}

	@Test
	public void happyPathTestQ1() {
		happyPathTest("2024-03-01", "Q1-2024");
	}

	@Test
	public void happyPathTestQ2() {
		happyPathTest("2024-06-01", "Q2-2024");
	}

	@Test
	public void happyPathTestQ3() {
		happyPathTest("2024-09-01", "Q3-2024");
	}

	@Test
	public void happyPathTestQ4() {
		happyPathTest("2024-12-01", "Q4-2024");
	}
}
