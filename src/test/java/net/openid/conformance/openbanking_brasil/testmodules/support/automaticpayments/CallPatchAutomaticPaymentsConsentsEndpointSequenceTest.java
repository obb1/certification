package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import org.junit.Test;

public class CallPatchAutomaticPaymentsConsentsEndpointSequenceTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void test() {
		CallPatchAutomaticPaymentsConsentsEndpointSequence condSeq = new CallPatchAutomaticPaymentsConsentsEndpointSequence();
		condSeq.evaluate();
	}
}
