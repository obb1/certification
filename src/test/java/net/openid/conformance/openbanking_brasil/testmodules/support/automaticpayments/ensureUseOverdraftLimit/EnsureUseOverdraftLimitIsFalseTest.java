package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureUseOverdraftLimit;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit.AbstractEnsureUseOverdraftLimit;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit.EnsureUseOverdraftLimitIsFalse;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureUseOverdraftLimitIsFalseTest extends AbstractEnsureUseOverdraftLimitTest {

	@Override
	protected AbstractEnsureUseOverdraftLimit condition() {
		return new EnsureUseOverdraftLimitIsFalse();
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "UseOverdraftLimitFalse200Response.json")
	public void happyPathTest() {
		run(condition());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "200Response.json")
	public void unhappyPathTestMissingUseOverdraftLimitFalse() {
		unhappyPathTest("The useOverdraftLimit value is different from expected");
	}
}
