package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreTwoAcscPayments;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureThereAreTwoAcscPaymentsTest extends AbstractEnsureThereArePaymentsTest {

	@Override
	protected AbstractEnsureThereArePayments condition() {
		return new EnsureThereAreTwoAcscPayments();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "TwoAcsc.json")
	public void happyPathTest() {
		super.happyPathTest();
	}
}
