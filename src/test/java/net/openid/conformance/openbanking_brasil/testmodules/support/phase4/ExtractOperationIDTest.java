package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ExtractOperationIDTest extends AbstractJsonResponseConditionUnitTest {


	@UseResurce("jsonResponses/exchanges/OperationsListResponse.json")
	@Test
	public void happyPath() {
		ExtractOperationID condition = new ExtractOperationID();
		run(condition);

		assertThat(environment.getString("operationId"), equalTo("92792126019929240"));
	}

	@UseResurce("jsonResponses/exchanges/OperationsListResponseNoData.json")
	@Test
	public void unhappyPath() {
		ExtractOperationID condition = new ExtractOperationID();
		var error = runAndFail(condition);

		assertThat(error.getMessage(), equalTo("ExtractOperationID: data array does not contain an operation"));
	}
}
