package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PatchRecurringConsentOASValidatorV2Test extends AbstractRecurringConsentOASValidatorV2Test {

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200Response.json")
	public void happyPathTestCompleteConsentResponse() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponseMissingOptionalFields.json")
	public void happyPathTestMissingOptionalFieldsConsentResponse() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCode.json")
	public void unhappyPathTestAutomaticValidStatusNoDebtorAccountIbgeTownCode() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCodeStatusAuthorised.json")
	public void unhappyPathTestAutomaticAuthorisedNoDebtorAccountIbgeTownCode() {
		unhappyPathTest("debtorAccount.ibgeTownCode is required when recurringConfiguration is automatic and status is AUTHORISED, REVOKED or CONSUMED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCodeStatusRevoked.json")
	public void unhappyPathTestAutomaticRevokedNoDebtorAccountIbgeTownCode() {
		unhappyPathTest("debtorAccount.ibgeTownCode is required when recurringConfiguration is automatic and status is AUTHORISED, REVOKED or CONSUMED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponseAutomaticNoDebtorAccountIbgeTownCodeStatusConsumed.json")
	public void unhappyPathTestAutomaticConsumedNoDebtorAccountIbgeTownCode() {
		unhappyPathTest("debtorAccount.ibgeTownCode is required when recurringConfiguration is automatic and status is AUTHORISED, REVOKED or CONSUMED");
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponseRevokedNoRevocation.json")
	public void unhappyPathTestRevokedNoRevocation() {
		super.unhappyPathTestRevokedNoRevocation();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponseAuthorisedNoAuthorisedAtDateTime.json")
	public void unhappyPathTestAuthorisedNoAuthorisedAtDateTime() {
		super.unhappyPathTestAuthorisedNoAuthorisedAtDateTime();
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponsePartiallyAcceptedNoApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedNoApprovalDueDate() {
		unhappyPathTest("approvalDueDate is required when status is PARTIALLY_ACCEPTED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponsePartiallyAcceptedNoExpirationDateTimeValidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedNoExpirationDateTimeValidApprovalDueDate() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponsePartiallyAcceptedNoExpirationDateTimeInvalidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedNoExpirationDateTimeInvalidApprovalDueDate() {
		unhappyPathTest("When expirationDateTime is not specified, approvalDueDate should default to 30 days after creationDateTime");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponsePartiallyAcceptedLowExpirationDateTimeValidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedLowExpirationDateTimeValidApprovalDueDate() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponsePartiallyAcceptedLowExpirationDateTimeInvalidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedLowExpirationDateTimeInvalidApprovalDueDate() {
		unhappyPathTest("The approvalDueDate should not exceed neither the expirationDateTime nor the creationDateTime + 30 days");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponsePartiallyAcceptedHighExpirationDateTimeValidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedHighExpirationDateTimeValidApprovalDueDate() {
		run(validator());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PatchRecurringConsents200ResponsePartiallyAcceptedHighExpirationDateTimeInvalidApprovalDueDate.json")
	public void unhappyPathTestPartiallyAcceptedHighExpirationDateTimeInvalidApprovalDueDate() {
		unhappyPathTest("The approvalDueDate should not exceed neither the expirationDateTime nor the creationDateTime + 30 days");
	}

	@Override
	protected int status() {
		return 200;
	}

	@Override
	protected AbstractRecurringConsentOASValidatorV2 validator() {
		return new PatchRecurringConsentOASValidatorV2();
	}
}
