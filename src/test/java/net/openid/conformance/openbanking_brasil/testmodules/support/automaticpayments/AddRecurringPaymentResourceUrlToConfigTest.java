package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringPaymentResourceUrlToConfig;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddRecurringPaymentResourceUrlToConfigTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_URL = "https://www.example.com/open-banking/automatic-payments/v1/recurring-consents";

	@Test
	public void happyPathTest() {
		environment.putString("config", "resource.consentUrl", CONSENT_URL);
		AddRecurringPaymentResourceUrlToConfig condition = new AddRecurringPaymentResourceUrlToConfig();
		run(condition);
		String resourceUrl = OIDFJSON.getString(
			environment.getElementFromObject("config", "resource.resourceUrl")
		);
		assertTrue(resourceUrl.endsWith("/pix/recurring-payments"));
	}
}
