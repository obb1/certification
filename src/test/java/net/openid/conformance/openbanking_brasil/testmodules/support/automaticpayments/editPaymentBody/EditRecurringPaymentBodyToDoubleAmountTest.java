package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToDoubleAmount;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentBodyToDoubleAmountTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBody";
	protected static final String KEY = "resource";
	protected static final String PATH = "brazilPixPayment";

	@Test
	@UseResurce(value = BASE_JSON_PATH + "V2.json", key = KEY, path = PATH)
	public void happyPathTest() {
		run(new EditRecurringPaymentBodyToDoubleAmount());

		String expectedAmount = "200000.24";
		String amount = OIDFJSON.getString(environment.getElementFromObject(KEY, PATH + ".data.payment.amount"));

		assertEquals(expectedAmount, amount);
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data.payment in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find data.payment in payments payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingPaymentV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingPayment() {
		unhappyPathTest("Unable to find data.payment in payments payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "MissingPaymentAmountV2.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingPaymentAmount() {
		unhappyPathTest("Unable to find amount in payments payload");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentBodyToDoubleAmount());
		assertTrue(error.getMessage().contains(message));
	}
}
