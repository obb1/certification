package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractFirstUriFromSSASoftwareOriginUrisTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		JsonArray uris = new JsonArray();
		String uri = "https://www.example.com/test-mtls/a/obbsb";
		uris.add(uri);
		uris.add(uri + "1");
		environment.putObject(
			"software_statement_assertion",
			new JsonObjectBuilder().addField("claims.software_origin_uris", uris).build()
		);
		ExtractFirstUriFromSSASoftwareOriginUris condition = new ExtractFirstUriFromSSASoftwareOriginUris();
		run(condition);
		assertEquals(uri, environment.getString("software_origin_uri"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingSSA() {
		ExtractFirstUriFromSSASoftwareOriginUris condition = new ExtractFirstUriFromSSASoftwareOriginUris();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingClaims() {
		environment.putObject("software_statement_assertion", new JsonObject());
		unhappyPathTest("There is no software_origin_uris field inside the SSA");
	}

	@Test
	public void unhappyPathTestMissingSoftwareOriginUris() {
		environment.putObject("software_statement_assertion", "claims", new JsonObject());
		unhappyPathTest("There is no software_origin_uris field inside the SSA");
	}

	@Test
	public void unhappyPathTestEmptySoftwareOriginUris() {
		environment.putObject("software_statement_assertion",
			new JsonObjectBuilder().addField("claims.software_origin_uris", new JsonArray()).build());
		unhappyPathTest("software_origin_uris is an empty array");
	}

	private void unhappyPathTest(String errorMessage) {
		ExtractFirstUriFromSSASoftwareOriginUris condition = new ExtractFirstUriFromSSASoftwareOriginUris();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}
}
