package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.editPaymentRequestClaims;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims.EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfo;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfoTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "resource_request_entity_claims";
	private static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/PostRecurringPaymentsPixRequestBody";

	@Test
	@UseResurce(value = BASE_JSON_PATH + ".json", key = KEY)
	public void happyPathTestCacc() {
		run(new EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfo());

		JsonObject creditorAccount = environment.getElementFromObject(KEY, "data.creditorAccount").getAsJsonObject();

		String oldNumber = "1234567890";
		String number = OIDFJSON.getString(creditorAccount.get("number"));
		assertEquals(oldNumber + "0", number);

		String accountType = OIDFJSON.getString(creditorAccount.get("accountType"));
		assertEquals("SVGS", accountType);
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "SvgsMaxLengthNumber.json", key = KEY)
	public void happyPathTestSvgsMaxLengthNumber() {
		run(new EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfo());

		JsonObject creditorAccount = environment.getElementFromObject(KEY, "data.creditorAccount").getAsJsonObject();

		String number = OIDFJSON.getString(creditorAccount.get("number"));
		assertEquals("1234", number);

		String accountType = OIDFJSON.getString(creditorAccount.get("accountType"));
		assertEquals("CACC", accountType);
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithoutCreditorAccount.json", key = KEY)
	public void unhappyPathTestMissingCreditorAccount() {
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new EditRecurringPaymentRequestClaimsToSetDifferentCreditorAccountInfo());
		assertTrue(error.getMessage().contains("Unable to find data.creditorAccount in recurring-payments request payload"));
	}
}
