package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.checkConsentsVersion.CheckConsentsVersionV4;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class CheckConsentsVersionV4Test extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_URL = "https://www.example.com/open-banking/payments/";
	private static final String CONSENT_URL_V4 = "v4/consents/";
	private static final String CONSENT_URL_V3 = "v3/consents/";

	public void prepareEnvironment(String endOfUrl) {
		JsonObject config = new JsonObjectBuilder()
			.addField("resource.consentUrl", BASE_URL + endOfUrl)
			.build();
		environment.putObject("config", config);
	}

	@Test
	public void happyPathTest() {
		prepareEnvironment(CONSENT_URL_V4);
		CheckConsentsVersionV4 condition = new CheckConsentsVersionV4();
		run(condition);
	}

	@Test
	public void unhappyPathInvalidUrlTest() {
		prepareEnvironment("");
		CheckConsentsVersionV4 condition = new CheckConsentsVersionV4();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "consentUrl is not valid, please ensure that url matches";
		Assertions.assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void unhappyPathVersionNotMatchingTest() {
		prepareEnvironment(CONSENT_URL_V3);
		CheckConsentsVersionV4 condition = new CheckConsentsVersionV4();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Consents version does not match expected value";
		Assertions.assertTrue(error.getMessage().contains(expectedMessage));
	}
}
