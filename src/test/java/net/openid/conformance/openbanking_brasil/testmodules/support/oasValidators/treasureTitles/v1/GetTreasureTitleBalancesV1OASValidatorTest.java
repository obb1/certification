package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetTreasureTitleBalancesV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/treasureTitles/balances.json")
	public void happyPath() {
		run(new GetTreasureTitleBalancesV1OASValidator());
	}


}
