package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createPatchConsentEdition;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationNoAmountNoLoggedUser;

public class CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationNoAmountNoLoggedUserTest extends AbstractCreatePatchRecurringConsentsForEditionRequestBodyTest {

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody condition() {
		return new CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationNoAmountNoLoggedUser();
	}

	@Override
	protected String getExpectedExpirationDateTime() {
		return null;
	}

	@Override
	protected String getExpectedMaxVariableAmount() {
		return null;
	}

	@Override
	protected boolean hasLoggedUser() {
		return false;
	}
}
