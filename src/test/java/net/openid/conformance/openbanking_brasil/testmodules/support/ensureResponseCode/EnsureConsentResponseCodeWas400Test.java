package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseCodeWas400Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureConsentResponseCodeWas400();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.OK.value();
	}
}
