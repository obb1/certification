package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetPaymentAuthorisationFlowToHybridFlowTest extends AbstractJsonResponseConditionUnitTest {
	private static final String KEY = "config";
	private static final String PATH = "resource.brazilPixPayment";
	private static final String expectedFlow = "HYBRID_FLOW";

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpf.json", key=KEY, path=PATH)
	public void happyPathSingleTest() {
		SetPaymentAuthorisationFlowToHybridFlow condition = new SetPaymentAuthorisationFlowToHybridFlow();
		run(condition);

		String flow = OIDFJSON.getString(environment.getElementFromObject("config", "resource.brazilPixPayment.data.authorisationFlow"));
		assertEquals(expectedFlow, flow);
	}

	@Test
	@UseResurce(value="jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key=KEY, path=PATH)
	public void happyPathArrayTest() {
		SetPaymentAuthorisationFlowToHybridFlow condition = new SetPaymentAuthorisationFlowToHybridFlow();
		run(condition);

		JsonArray data = environment.getElementFromObject("config", "resource.brazilPixPayment.data").getAsJsonArray();

		for (int i = 0 ; i < data.size(); i++) {
			String flow = OIDFJSON.getString(data.get(i).getAsJsonObject().get("authorisationFlow"));
			assertEquals(expectedFlow, flow);
		}
	}

	@Test
	@UseResurce(value="test_config_no_data.json")
	public void unhappyPathNoData() {
		SetPaymentAuthorisationFlowToHybridFlow condition = new SetPaymentAuthorisationFlowToHybridFlow();
		ConditionError error = runAndFail(condition);

		assertEquals("SetPaymentAuthorisationFlowToHybridFlow: PixPayment object not found in config", error.getMessage());
	}
}
