package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;


public class GetLoansPaymentsV2N4OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/payments.json")
	public void happyPath() {
		run(new GetLoansPaymentsV2n4OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/paymentsNoOverParcel.json")
	public void happyPathNoOverParcel() {
		run(new GetLoansPaymentsV2n4OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/paymentsNoChargeAdditionalInfo.json")
	public void unhappyPathNoChargeAdditionalInfo() {
		ConditionError e = runAndFail(new GetLoansPaymentsV2n4OASValidator());
		assertThat(e.getMessage(), containsString("chargeAdditionalInfo is required when chargeType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/paymentsNoInstalmentId.json")
	public void unhappyPathNoInstalmentId() {
		ConditionError e = runAndFail(new GetLoansPaymentsV2n4OASValidator());
		assertThat(e.getMessage(), containsString("instalmentId is required when isOverParcelPayment is false"));
	}


}
