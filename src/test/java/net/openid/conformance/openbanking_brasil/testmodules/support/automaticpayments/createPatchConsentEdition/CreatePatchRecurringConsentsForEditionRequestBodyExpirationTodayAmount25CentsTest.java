package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createPatchConsentEdition;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyExpirationTodayAmount25Cents;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class CreatePatchRecurringConsentsForEditionRequestBodyExpirationTodayAmount25CentsTest extends AbstractCreatePatchRecurringConsentsForEditionRequestBodyTest {

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody condition() {
		return new CreatePatchRecurringConsentsForEditionRequestBodyExpirationTodayAmount25Cents();
	}

	@Override
	protected String getExpectedExpirationDateTime() {
		LocalDate currentDate = LocalDate.now(ZoneOffset.UTC);
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String currentDateStr = currentDate.format(dateFormatter);
		return String.format("%sT23:59:59Z", currentDateStr);
	}

	@Override
	protected String getExpectedMaxVariableAmount() {
		return "0.25";
	}
}
