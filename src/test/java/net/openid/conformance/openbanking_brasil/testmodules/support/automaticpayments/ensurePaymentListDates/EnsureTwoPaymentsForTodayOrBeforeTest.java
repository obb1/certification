package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensurePaymentListDates;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.AbstractEnsurePaymentListDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.EnsureTwoPaymentsForTodayOrBefore;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;

public class EnsureTwoPaymentsForTodayOrBeforeTest extends AbstractEnsurePaymentListDatesTest {

	@Override
	protected AbstractEnsurePaymentListDates condition() {
		return new EnsureTwoPaymentsForTodayOrBefore();
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.now(ZoneId.of("UTC-3"));
	}

	@Override
	protected int expectedAmount() {
		return 2;
	}

	@Test
	public void happyPathTestBefore() {
		insertMockedResponse(expectedAmount(), expectedDate().minusDays(1));
	}
}
