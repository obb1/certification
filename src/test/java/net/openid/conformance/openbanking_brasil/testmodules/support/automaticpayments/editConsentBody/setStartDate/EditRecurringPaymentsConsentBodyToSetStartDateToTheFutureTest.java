package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setStartDate;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate.AbstractEditRecurringPaymentsConsentBodyToSetStartDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate.EditRecurringPaymentsConsentBodyToSetStartDateToTheFuture;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class EditRecurringPaymentsConsentBodyToSetStartDateToTheFutureTest extends AbstractEditRecurringPaymentsConsentBodyToSetStartDateTest {

	@Override
	protected AbstractEditRecurringPaymentsConsentBodyToSetStartDate condition() {
		return new EditRecurringPaymentsConsentBodyToSetStartDateToTheFuture();
	}

	@Override
	protected LocalDateTime expectedDateTime() {
		return ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime().plusDays(10).truncatedTo(ChronoUnit.SECONDS);
	}
}
