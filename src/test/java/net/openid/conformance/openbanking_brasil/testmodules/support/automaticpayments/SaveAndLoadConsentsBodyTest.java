package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SaveConsentsBody;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SaveAndLoadConsentsBodyTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";
	private static final String SAVED_PATH = "savedBrazilPaymentConsent";

	@Before
	public void init() {
		environment.putObject("resource", new JsonObject());
	}

	@Test
	@UseResurce(value=BASE_FILE_PATH+"brazilPaymentConsentWithSweepingField.json", key=KEY, path=PATH)
	public void happyPathTest() {
		// Use save condition
		SaveConsentsBody saveCondition = new SaveConsentsBody();
		run(saveCondition);

		// Check that the saved request is equals to the original one
		JsonObject originalRequest = environment.getElementFromObject(KEY, PATH).getAsJsonObject();
		JsonObject savedRequest = environment.getObject(SAVED_PATH);
		assertEquals(originalRequest, savedRequest);

		// Modify the original consent request to show that they are separate objects
		originalRequest.addProperty("example", "example");
		assertNotEquals(originalRequest, savedRequest);

		// Use load condition to override the original request
		LoadConsentsBody loadCondition = new LoadConsentsBody();
		run(loadCondition);

		// Check that the saved request is equals to the loaded one
		JsonObject loadedRequest = environment.getElementFromObject(KEY, PATH).getAsJsonObject();
		assertEquals(loadedRequest, savedRequest);

		// Modify the saved consent request to show that they are separate objects
		savedRequest.addProperty("example", "example");
		assertNotEquals(loadedRequest, savedRequest);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathLoadingWithoutSavingTest() {
		LoadConsentsBody loadCondition = new LoadConsentsBody();
		run(loadCondition);
	}

	@Test
	public void unhappyPathSaveMissingBrazilPaymentConsentTest() {
		SaveConsentsBody saveCondition = new SaveConsentsBody();
		ConditionError error = runAndFail(saveCondition);

		String expectedMessage = "Unable to find consents body inside the resource";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
