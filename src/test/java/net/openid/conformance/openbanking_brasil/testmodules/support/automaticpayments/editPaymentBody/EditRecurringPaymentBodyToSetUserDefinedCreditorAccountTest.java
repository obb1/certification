package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentBodyToSetUserDefinedCreditorAccountTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBodyMissingCreditorAccountV2.json";

	protected static final String ISPB = "12345678";
	protected static final String ISSUER = "1774";
	protected static final String NUMBER = "1234567890";
	protected static final String ACCOUNT_TYPE = "CACC";

	@Before
	public void init() {
		environment.putString("resource", "creditorAccountIspb", ISPB);
		environment.putString("resource", "creditorAccountIssuer", ISSUER);
		environment.putString("resource", "creditorAccountNumber", NUMBER);
		environment.putString("resource", "creditorAccountAccountType", ACCOUNT_TYPE);
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void happyPathTest() {
		run(new EditRecurringPaymentBodyToSetUserDefinedCreditorAccount());

		JsonObject paymentData = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonObject();
		assertTrue(paymentData.has("creditorAccount") && paymentData.get("creditorAccount").isJsonObject());

		JsonObject creditorAccount = paymentData.getAsJsonObject("creditorAccount");
		assertStringField(creditorAccount, "ispb", ISPB);
		assertStringField(creditorAccount, "issuer", ISSUER);
		assertStringField(creditorAccount, "number", NUMBER);
		assertStringField(creditorAccount, "accountType", ACCOUNT_TYPE);
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void unhappyPathTestMissingIspb() {
		unhappyPathTestMissingCreditorAccountField("Ispb");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void unhappyPathTestMissingIssuer() {
		unhappyPathTestMissingCreditorAccountField("Issuer");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void unhappyPathTestMissingNumber() {
		unhappyPathTestMissingCreditorAccountField("Number");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void unhappyPathTestMissingAccountType() {
		unhappyPathTestMissingCreditorAccountField("AccountType");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentBodyToSetUserDefinedCreditorAccount());
		assertTrue(error.getMessage().contains(message));
	}

	protected void unhappyPathTestMissingCreditorAccountField(String field) {
		JsonObject resource = environment.getObject("resource");
		resource.remove("creditorAccount" + field);
		unhappyPathTest(String.format("Unable to find creditorAccount%s in the resource", field));
	}

	protected void assertStringField(JsonObject obj, String field, String expectedValue) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		String value = OIDFJSON.getString(obj.get(field));
		assertEquals(expectedValue, value);
	}
}
