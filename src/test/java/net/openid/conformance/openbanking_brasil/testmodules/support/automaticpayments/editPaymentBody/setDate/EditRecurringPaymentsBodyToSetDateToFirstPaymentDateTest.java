package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.setDate;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToFirstPaymentDate;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;

public class EditRecurringPaymentsBodyToSetDateToFirstPaymentDateTest extends AbstractEditRecurringPaymentsBodyToSetDateTest {

	protected static final String BASE_JSON_PATH = "environmentVariables/automaticPayments/recurringConfigurationObjectAutomatic";

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate condition() {
		return new EditRecurringPaymentsBodyToSetDateToFirstPaymentDate();
	}

	@Override
	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	@UseResurce(value = BASE_JSON_PATH + ".json", key = "recurring_configuration_object")
	public void happyPathTest() {
		super.happyPathTest();
	}

	@Override
	@Test
	@UseResurce(value = BASE_JSON_PATH + ".json", key = "recurring_configuration_object")
	public void unhappyPathTestMissingBrazilPixPayment() {
		super.unhappyPathTestMissingBrazilPixPayment();
	}

	@Override
	@Test
	@UseResurce(value = BASE_JSON_PATH + ".json", key = "recurring_configuration_object")
	public void unhappyPathTestMissingData() {
		super.unhappyPathTestMissingData();
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void unhappyPathTestMissingAutomatic() {
		environment.putObject("recurring_configuration_object", new JsonObject());
		unhappyPathTest("Unable to find firstPayment date inside automatic recurring configuration object");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	@UseResurce(value = BASE_JSON_PATH + "WithoutFirstPayment.json", key = "recurring_configuration_object")
	public void unhappyPathTestMissingFirstPayment() {
		unhappyPathTest("Unable to find firstPayment date inside automatic recurring configuration object");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	@UseResurce(value = BASE_JSON_PATH + "WithoutFirstPaymentDate.json", key = "recurring_configuration_object")
	public void unhappyPathTestMissingDate() {
		unhappyPathTest("Unable to find firstPayment date inside automatic recurring configuration object");
	}

	@Override
	protected LocalDate expectedDate() {
		return LocalDate.parse("2021-01-01");
	}
}
