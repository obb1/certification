package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo;


import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SetPaymentAmountTo05BRLOnConsentTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		JsonObject payment = new JsonObject();
		payment.addProperty("amount","1.50");
		JsonObject data = new JsonObject();
		data.add("payment",payment);
		JsonObject brazilPaymentConsent = new JsonObject();
		brazilPaymentConsent.add("data",data);
		JsonObject resource = new JsonObject();
		resource.add("brazilPaymentConsent",brazilPaymentConsent);
		environment.putObject("resource",resource);

		environment.putString("transaction_limit","10000.00");

	}

	@Test
	public void happyPathTest() {
		SetPaymentAmountTo05BRLOnConsent condition = new SetPaymentAmountTo05BRLOnConsent();
		run(condition);
		Assert.assertThat((environment.getString("resource","brazilPaymentConsent.data.payment.amount")), Matchers.is("0.50"));
	}

	@Test
	public void happyPathTestAmountsEqual() {
		JsonObject payment = new JsonObject();
		payment.addProperty("amount","0.50");
		JsonObject data = new JsonObject();
		data.add("payment",payment);
		JsonObject brazilPaymentConsent = new JsonObject();
		brazilPaymentConsent.add("data",data);
		JsonObject resource = new JsonObject();
		resource.add("brazilPaymentConsent",brazilPaymentConsent);
		environment.putObject("resource",resource);

		environment.putString("transaction_limit","10000.00");
		SetPaymentAmountTo05BRLOnConsent condition = new SetPaymentAmountTo05BRLOnConsent();
		run(condition);
		Assert.assertThat((environment.getString("resource","brazilPaymentConsent.data.payment.amount")), Matchers.is("0.50"));

	}

	@Test
	public void unhappyPathTestNoAmount() {
		environment.putObject("resource","brazilPaymentConsent.data.payment", new JsonObject());

		SetPaymentAmountTo05BRLOnConsent condition = new SetPaymentAmountTo05BRLOnConsent();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not parse amount"));
	}

	@Test
	public void unhappyPathTestNoBrazilPaymentConsent() {
		environment.putObject("resource","brazilPaymentConsent", new JsonObject());

		SetPaymentAmountTo05BRLOnConsent condition = new SetPaymentAmountTo05BRLOnConsent();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract"));
	}

}
