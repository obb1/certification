package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetInvoiceFinancingsPaymentsV2n3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/payments.json")
	public void happyPath() {
		run(new GetInvoiceFinancingsPaymentsV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/paymentsNoOverParcel.json")
	public void happyPathNoOverParcel() {
		run(new GetInvoiceFinancingsPaymentsV2n3OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/paymentsNoChargeAdditionalInfo.json")
	public void unhappyPathNoChargeAdditionalInfo() {
		ConditionError e = runAndFail(new GetInvoiceFinancingsPaymentsV2n3OASValidator());
		assertThat(e.getMessage(), containsString("chargeAdditionalInfo is required when chargeType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/paymentsNoInstalmentId.json")
	public void unhappyPathNoInstalmentId() {
		ConditionError e = runAndFail(new GetInvoiceFinancingsPaymentsV2n3OASValidator());
		assertThat(e.getMessage(), containsString("instalmentId is required when isOverParcelPayment is false"));
	}

	@Test
	@UseResurce("jsonResponses/invoiceFinancings/v2.2.0/paymentsInvalidPaymentId.json")
	public void unhappyPathInvalidPaymentId() {
		ConditionError e = runAndFail(new GetInvoiceFinancingsPaymentsV2n3OASValidator());
		assertThat(e.getMessage(), containsString("Value from element paymentId doesn't match the required pattern"));
	}
}
