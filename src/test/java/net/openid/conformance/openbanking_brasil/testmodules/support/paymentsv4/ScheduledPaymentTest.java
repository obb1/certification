package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithDateTooFarInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithDayAndTwoAheadPayload;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithRepeatedDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithStandardPayload;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithTooLittleDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule5DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule60DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule60DailyPaymentsStarting672DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule61DailyPaymentsStarting10DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule6DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly.Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly.Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.DayOfWeekEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule61WeeklyPaymentsForMondayStarting10DaysInTheFuture;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScheduledPaymentTest extends AbstractJsonResponseConditionUnitTest {

	public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Before
	public void init() {
		JsonObject resource = new JsonObjectBuilder()
			.addField("brazilPaymentConsent.data.payment", new JsonObject())
			.build();
		environment.putObject("resource", resource);
	}

	protected JsonObject getFromScheduleObject(String key) {
		return environment
			.getElementFromObject("resource", "brazilPaymentConsent.data.payment.schedule")
			.getAsJsonObject()
			.getAsJsonObject(key);
	}

	protected void assertQuantity(JsonObject scheduleObject, int quantity) {
		assertEquals(OIDFJSON.getInt(scheduleObject.get("quantity")), quantity);
	}

	protected void assertDistanceFromCurrentDate(int daysInTheFuture, String date) {
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		LocalDate scheduledDate = LocalDate.parse(date, FORMATTER);
		Duration difference = Duration.between(currentDate.atStartOfDay(), scheduledDate.atStartOfDay());
		assertEquals(daysInTheFuture, difference.toDays());
	}

	protected void assertStartDate(JsonObject scheduleObject, int daysInTheFuture) {
		assertDistanceFromCurrentDate(daysInTheFuture, OIDFJSON.getString(scheduleObject.get("startDate")));
	}

	protected void assertDailyFields(int quantity, int daysInTheFuture) {
		JsonObject daily = getFromScheduleObject("daily");
		assertQuantity(daily, quantity);
		assertStartDate(daily, daysInTheFuture);
	}

	protected void assertWeeklyFields(int quantity, int daysInTheFuture, DayOfWeekEnum dayOfWeek) {
		JsonObject weekly = getFromScheduleObject("weekly");
		assertQuantity(weekly, quantity);
		assertStartDate(weekly, daysInTheFuture);
		assertEquals(OIDFJSON.getString(weekly.get("dayOfWeek")), dayOfWeek.toString());
	}

	protected void assertMonthlyFields(int quantity, int daysInTheFuture, int dayOfMonth) {
		JsonObject monthly = getFromScheduleObject("monthly");
		assertQuantity(monthly, quantity);
		assertStartDate(monthly, daysInTheFuture);
		assertEquals(OIDFJSON.getInt(monthly.get("dayOfMonth")), dayOfMonth);
	}

	protected void assertCustomFields(List<Integer> listOfDaysInTheFuture) {
		JsonObject custom = getFromScheduleObject("custom");
		JsonArray dates = custom.getAsJsonArray("dates");
		for (int i = 0; i < listOfDaysInTheFuture.size(); i++) {
			String date = OIDFJSON.getString(dates.get(i));
			assertDistanceFromCurrentDate(listOfDaysInTheFuture.get(i), date);
		}
	}

	@Test
	public void test5DailyPayments1DayInTheFuture() {
		Schedule5DailyPaymentsStarting1DayInTheFuture condition = new Schedule5DailyPaymentsStarting1DayInTheFuture();
		run(condition);
		assertDailyFields(5, 1);
	}

	@Test
	public void test6DailyPayments1DayInTheFuture() {
		Schedule6DailyPaymentsStarting1DayInTheFuture condition = new Schedule6DailyPaymentsStarting1DayInTheFuture();
		run(condition);
		assertDailyFields(6, 1);
	}

	@Test
	public void test60DailyPayments1DayInTheFuture() {
		Schedule60DailyPaymentsStarting1DayInTheFuture condition = new Schedule60DailyPaymentsStarting1DayInTheFuture();
		run(condition);
		assertDailyFields(60, 1);
	}

	@Test
	public void test60DailyPayments672DaysInTheFuture() {
		Schedule60DailyPaymentsStarting672DaysInTheFuture condition = new Schedule60DailyPaymentsStarting672DaysInTheFuture();
		run(condition);
		assertDailyFields(60, 672);
	}

	@Test
	public void test61DailyPayments672DaysInTheFuture() {
		Schedule61DailyPaymentsStarting10DaysInTheFuture condition = new Schedule61DailyPaymentsStarting10DaysInTheFuture();
		run(condition);
		assertDailyFields(61, 10);
	}

	@Test
	public void test5MondayWeeklyPayments1DayInTheFuture() {
		Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture condition = new Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture();
		run(condition);
		assertWeeklyFields(5, 1, DayOfWeekEnum.SEGUNDA_FEIRA);
	}

	@Test
	public void test60MondayWeeklyPayments317DaysInTheFuture() {
		Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture condition = new Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture();
		run(condition);
		assertWeeklyFields(60, 317, DayOfWeekEnum.SEGUNDA_FEIRA);
	}

	@Test
	public void test61MondayWeeklyPayments10DaysInTheFuture() {
		Schedule61WeeklyPaymentsForMondayStarting10DaysInTheFuture condition = new Schedule61WeeklyPaymentsForMondayStarting10DaysInTheFuture();
		run(condition);
		assertWeeklyFields(61, 10, DayOfWeekEnum.SEGUNDA_FEIRA);
	}

	@Test
	public void test5MonthlyPaymentsFor31st1DayInTheFuture() {
		Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture condition = new Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture();
		run(condition);
		assertMonthlyFields(5, 1, 31);
	}

	@Test
	public void test21MonthlyPaymentsFor1st150DaysInTheFuture() {
		Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture condition = new Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture();
		run(condition);
		assertMonthlyFields(21, 150, 1);
	}

	@Test
	public void testCustomWithStandardPayload() {
		CustomSchedulePaymentsWithStandardPayload condition = new CustomSchedulePaymentsWithStandardPayload();
		run(condition);
		List<Integer> listOfDaysInTheFuture = List.of(1, 7, 32, 129, 500);
		assertCustomFields(listOfDaysInTheFuture);
	}

	@Test
	public void testCustomWithTooLittleDates() {
		CustomSchedulePaymentsWithTooLittleDates condition = new CustomSchedulePaymentsWithTooLittleDates();
		run(condition);
		List<Integer> listOfDaysInTheFuture = List.of(1);
		assertCustomFields(listOfDaysInTheFuture);
	}

	@Test
	public void testCustomWithDateTooFarInTheFuture() {
		CustomSchedulePaymentsWithDateTooFarInTheFuture condition = new CustomSchedulePaymentsWithDateTooFarInTheFuture();
		run(condition);
		List<Integer> listOfDaysInTheFuture = List.of(100, 732);
		assertCustomFields(listOfDaysInTheFuture);
	}

	@Test
	public void testCustomWithRepeatedDates() {
		CustomSchedulePaymentsWithRepeatedDates condition = new CustomSchedulePaymentsWithRepeatedDates();
		run(condition);
		List<Integer> listOfDaysInTheFuture = List.of(1, 1);
		assertCustomFields(listOfDaysInTheFuture);
	}

	@Test
	public void testCustomWithDayAndTwoAhead() {
		CustomSchedulePaymentsWithDayAndTwoAheadPayload condition = new CustomSchedulePaymentsWithDayAndTwoAheadPayload();
		run(condition);
		List<Integer> listOfDaysInTheFuture = List.of(1, 2);
		assertCustomFields(listOfDaysInTheFuture);
	}
}
