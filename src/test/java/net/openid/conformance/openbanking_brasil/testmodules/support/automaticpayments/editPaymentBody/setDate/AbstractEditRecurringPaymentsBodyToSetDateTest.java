package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.setDate;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class AbstractEditRecurringPaymentsBodyToSetDateTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String JSON_PATH = "jsonRequests/automaticPayments/payments/v2/postRecurringPaymentsRequestBodyV2.json";

	protected abstract AbstractEditRecurringPaymentsBodyToSetDate condition();
	protected abstract LocalDate expectedDate();

	@Test
	@UseResurce(value = JSON_PATH, key = "resource", path = "brazilPixPayment")
	public void happyPathTest() {
		run(condition());
		String dateStr = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.date"));
		LocalDate date = LocalDate.parse(dateStr);
		assertEquals(expectedDate(), date);
	}

	@Test
	public void unhappyPathTestMissingBrazilPixPayment() {
		environment.putObject("resource", new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest("Unable to find data in payments payload");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
