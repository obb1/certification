package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class PersonalRelationsResponseOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/naturalPersonalRelationshipV2/naturalPersonRelationshipResponse-V2.1.0.json")
	public void testHappyPath() {
		PersonalRelationsResponseOASValidatorV2 cond = new PersonalRelationsResponseOASValidatorV2();
		run(cond);

		// productsServicesTypes constraint.
		JsonArray productsServicesType = new JsonArray();
		productsServicesType.add("OUTROS");
		JsonObject invalidProductsServicesTypeData = new JsonObject();
		invalidProductsServicesTypeData.add("productsServicesType", productsServicesType);
		assertThrows(ConditionError.class, () -> cond.assertProductsServicesTypeAdditionalInfoConstraint(invalidProductsServicesTypeData));

		// accounts constraint.
		JsonObject account = new JsonObject();
		account.addProperty("type", "NOT_CONTA_PAGAMENTO_PRE_PAGA");
		JsonArray accounts = new JsonArray();
		accounts.add(account);
		JsonObject invalidAccountData = new JsonObject();
		invalidAccountData.add("accounts", accounts);
		assertThrows(ConditionError.class, () -> cond.assertAccountBranchCodeConstraint(invalidAccountData));
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/naturalPersonalRelationshipV2/naturalPersonRelationshipInvalidResponse-V2.1.0.json")
	public void testUnhappyPath() {
		PersonalRelationsResponseOASValidatorV2 cond = new PersonalRelationsResponseOASValidatorV2();
		runAndFail(cond);
	}
}
