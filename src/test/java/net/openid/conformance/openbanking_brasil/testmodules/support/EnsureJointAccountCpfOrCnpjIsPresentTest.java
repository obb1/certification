package net.openid.conformance.openbanking_brasil.testmodules.support;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class EnsureJointAccountCpfOrCnpjIsPresentTest  {

	@Test
	public void happyPath() {
		JsonObject config = new JsonObjectBuilder()
			.addField("conditionalResources.brazilCpfJointAccount", "76109277673")
			.build();

		Environment environment = new Environment();
		environment.putObject("config", config);

		EnsureJointAccountCpfOrCnpjIsPresent cond = new EnsureJointAccountCpfOrCnpjIsPresent();
		cond.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		cond.evaluate(environment);
		assertEquals("76109277673",
			environment.getString("config", "resource.brazilCpf"));

	}

	@Test
	public void happyPathNoCpf() {
		JsonObject config = new JsonObjectBuilder()
			.addField("conditionalResources.brazilCnpjJointAccount", "76109277673")
			.build();

		Environment environment = new Environment();
		environment.putObject("config", config);

		EnsureJointAccountCpfOrCnpjIsPresent cond = new EnsureJointAccountCpfOrCnpjIsPresent();
		cond.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		cond.evaluate(environment);
		assertEquals("76109277673",
			environment.getString("config", "resource.brazilCnpj"));

	}

	@Test
	public void unhappyPath() {
		Environment environment = new Environment();
		environment.putObject("config", new JsonObject());

		EnsureJointAccountCpfOrCnpjIsPresent cond = new EnsureJointAccountCpfOrCnpjIsPresent();
		cond.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.WARNING);

		Exception exception = assertThrows(ConditionError.class, () -> {
			cond.evaluate(environment);
		});

		assertTrue(exception.getMessage().contains("Brazil CPF and CNPJ for Joint Account field is empty. Institution is assumed to not have this functionality"));
	}

}
