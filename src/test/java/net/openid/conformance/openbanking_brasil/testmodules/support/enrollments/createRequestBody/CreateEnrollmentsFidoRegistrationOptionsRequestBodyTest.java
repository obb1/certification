package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class CreateEnrollmentsFidoRegistrationOptionsRequestBodyTest {

    private final String CERT = "-----BEGIN CERTIFICATE-----\n" +
        "MIIHADCCBeigAwIBAgIUHzap6tr22LPCl0gAz6jkUZbszwQwDQYJKoZIhvcNAQEL\n" +
        "BQAwcTELMAkGA1UEBhMCQlIxHDAaBgNVBAoTE09wZW4gQmFua2luZyBCcmFzaWwx\n" +
        "FTATBgNVBAsTDE9wZW4gQmFua2luZzEtMCsGA1UEAxMkT3BlbiBCYW5raW5nIFNB\n" +
        "TkRCT1ggSXNzdWluZyBDQSAtIEcxMB4XDTIyMDgxMjEyNDgwMFoXDTIzMDkxMTEy\n" +
        "NDgwMFowggEcMQswCQYDVQQGEwJCUjELMAkGA1UECBMCU1AxDzANBgNVBAcTBkxP\n" +
        "TkRPTjEcMBoGA1UEChMTT3BlbiBCYW5raW5nIEJyYXNpbDEtMCsGA1UECxMkNzRl\n" +
        "OTI5ZDktMzNiNi00ZDg1LThiYTctYzE0NmM4NjdhODE3MR8wHQYDVQQDExZtb2Nr\n" +
        "LXRwcC0xLnJhaWRpYW0uY29tMRcwFQYDVQQFEw40MzE0MjY2NjAwMDE5NzEdMBsG\n" +
        "A1UEDxMUUHJpdmF0ZSBPcmdhbml6YXRpb24xEzARBgsrBgEEAYI3PAIBAxMCVUsx\n" +
        "NDAyBgoJkiaJk/IsZAEBEyQxMDEyMDM0MC0zMzE4LTRiYWYtOTllMi0wYjU2NzI5\n" +
        "YzRhYjIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC5ILYWgl9nlspD\n" +
        "4+vfoZEPHg9STbCy3YgAYqan4tlIWOYqpgAkcuuma9zfk6f9SD3OCfmYyp4pXpT0\n" +
        "wdgwjxu9MTgixsuHPHYLMENO7/OGIHbmFXC2tONPId2OVkC9zdBxPTTtQ8tUQM3Y\n" +
        "rNV6pEWMukOIBYG9RcPklRl0FB+O0gTdkorg9RTkiBRIdDCiEn1h9Tzq+SF4mwpD\n" +
        "Mic85+VpCzot0nGnSx1xb0Wp7WWBPJeDip1pgPm1BL03NBPbyvsAkwklLXU0zZKz\n" +
        "KfW+vGgkGIvKDHREhr+aZPvTzeQ1oukc4S5yLBfgPXESIa9qyIO9GRozzH8IXNCx\n" +
        "4agzkTeNAgMBAAGjggLhMIIC3TAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQm8XFu\n" +
        "PrFYnrEgpmR+Z4hnce2ZWjAfBgNVHSMEGDAWgBSGf1itF/WCtk60BbP7sM4RQ99M\n" +
        "vjBMBggrBgEFBQcBAQRAMD4wPAYIKwYBBQUHMAGGMGh0dHA6Ly9vY3NwLnNhbmRi\n" +
        "b3gucGtpLm9wZW5iYW5raW5nYnJhc2lsLm9yZy5icjBLBgNVHR8ERDBCMECgPqA8\n" +
        "hjpodHRwOi8vY3JsLnNhbmRib3gucGtpLm9wZW5iYW5raW5nYnJhc2lsLm9yZy5i\n" +
        "ci9pc3N1ZXIuY3JsMCEGA1UdEQQaMBiCFm1vY2stdHBwLTEucmFpZGlhbS5jb20w\n" +
        "DgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMCMIIBqAYDVR0gBIIB\n" +
        "nzCCAZswggGXBgorBgEEAYO6L2QBMIIBhzCCATYGCCsGAQUFBwICMIIBKAyCASRU\n" +
        "aGlzIENlcnRpZmljYXRlIGlzIHNvbGVseSBmb3IgdXNlIHdpdGggUmFpZGlhbSBT\n" +
        "ZXJ2aWNlcyBMaW1pdGVkIGFuZCBvdGhlciBwYXJ0aWNpcGF0aW5nIG9yZ2FuaXNh\n" +
        "dGlvbnMgdXNpbmcgUmFpZGlhbSBTZXJ2aWNlcyBMaW1pdGVkcyBUcnVzdCBGcmFt\n" +
        "ZXdvcmsgU2VydmljZXMuIEl0cyByZWNlaXB0LCBwb3NzZXNzaW9uIG9yIHVzZSBj\n" +
        "b25zdGl0dXRlcyBhY2NlcHRhbmNlIG9mIHRoZSBSYWlkaWFtIFNlcnZpY2VzIEx0\n" +
        "ZCBDZXJ0aWNpY2F0ZSBQb2xpY3kgYW5kIHJlbGF0ZWQgZG9jdW1lbnRzIHRoZXJl\n" +
        "aW4uMEsGCCsGAQUFBwIBFj9odHRwOi8vcmVwb3NpdG9yeS5zYW5kYm94LnBraS5v\n" +
        "cGVuYmFua2luZ2JyYXNpbC5vcmcuYnIvcG9saWNpZXMwDQYJKoZIhvcNAQELBQAD\n" +
        "ggEBAGaESJ0UBfEB0mI8Fh98D6261BWUBdR9vdcD4IX53EubFvOCWE75skpYYyMz\n" +
        "s0dsoU6q/ivHVudhWUWaXCK9UNDgFHb8hE/YaDOoOJLRYllGq0qEyo8u0tJa0XmW\n" +
        "BfXMwNajEvlu3RdKWQ09x+KwEDjCIiJE7hK0cXReJuE6cDc5EPVjQ/fM7TBMQza0\n" +
        "hkZqJgA7555HCi8+k7bovGiV7i9sElvcuWKl2In3AWJke85K0zJaWRXsmkwFTE7i\n" +
        "nkob4yXb4SyGmCnlFEGUZMhqScAsiIrltE5cgLScRZrwymq+1rvYbqgUVOKLVzov\n" +
        "Js11ZIb3W0cRkjSGD9nXP3lfU4s=\n" +
        "-----END CERTIFICATE-----";

    @Test
    public void happyPathTest() {
        JsonObject config = new JsonObjectBuilder()
            .addField("mtls.cert", CERT)
            .build();

        Environment environment = new Environment();
        environment.putObject("config", config);

        CreateEnrollmentsFidoRegistrationOptionsRequestBody condition = new CreateEnrollmentsFidoRegistrationOptionsRequestBody();
        condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

        condition.evaluate(environment);

        JsonObject requestBody = environment.getObject("resource_request_entity_claims");

        JsonObject data = new JsonObjectBuilder()
            .addField("platform", "BROWSER")
            .addField("rp", "mock-tpp-1.raidiam.com")
            .build();
        JsonObject expectedBody = new JsonObject();
        expectedBody.add("data", data);

        assertEquals(requestBody, expectedBody);
    }
}
