package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;

public class SetSpecifiedValueToSpecifiedUrlParameterTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPath(){
		environment.putString("protected_resource_url", "https://api.example.com/resource");
		environment.putString("value", "12345");
		environment.putString("parameter", "id");
		run(new SetSpecifiedValueToSpecifiedUrlParameter());
		String updatedUrl = environment.getString("protected_resource_url");
		assertEquals("https://api.example.com/resource?id=12345", updatedUrl);
	}
	@Test
	public void unhappyPathWrongURISyntex() {
		environment.putString("protected_resource_url", "ht@tp://api.example.com/resource");
		environment.putString("value", "12345");
		environment.putString("parameter", "id");
		ConditionError error = runAndFail(new SetSpecifiedValueToSpecifiedUrlParameter());
		assertThat(error.getMessage(), containsString("Specified link is incorrect"));
	}


}
