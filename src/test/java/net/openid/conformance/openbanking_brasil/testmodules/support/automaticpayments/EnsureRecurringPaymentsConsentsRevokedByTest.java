package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedBy.EnsureRecurringPaymentsConsentsRevokedByUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedBy.EnsureRecurringPaymentsConsentsRevokedByDetentora;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureRecurringPaymentsConsentsRevokedByTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonResponses/automaticpayments/PostRecurringConsents";
	private static final String MISSING_FIELD_MESSAGE = "Unable to find element data.revocation.revokedBy in the response payload";

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromIniciadoraRevokedByUsuario.json")
	public void happyPathTestUsuario() {
		EnsureRecurringPaymentsConsentsRevokedByUsuario condition = new EnsureRecurringPaymentsConsentsRevokedByUsuario();
		run(condition);
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromDetentoraRevokedByDetentora.json")
	public void happyPathTestDetentora() {
		EnsureRecurringPaymentsConsentsRevokedByDetentora condition = new EnsureRecurringPaymentsConsentsRevokedByDetentora();
		run(condition);
	}

	@Test
	public void unhappyPathMissingResponse() {
		EnsureRecurringPaymentsConsentsRevokedByUsuario condition = new EnsureRecurringPaymentsConsentsRevokedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	public void unhappyPathMissingData() {
		EnsureRecurringPaymentsConsentsRevokedByUsuario condition = new EnsureRecurringPaymentsConsentsRevokedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRevocationField.json")
	public void unhappyPathMissingRevocation() {
		EnsureRecurringPaymentsConsentsRevokedByUsuario condition = new EnsureRecurringPaymentsConsentsRevokedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRevocationRevokedByField.json")
	public void unhappyPathMissingRevokedBy() {
		EnsureRecurringPaymentsConsentsRevokedByUsuario condition = new EnsureRecurringPaymentsConsentsRevokedByUsuario();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RevokedFromDetentoraRevokedByDetentora.json")
	public void unhappyPathRejectedByNotMatchingExpected() {
		EnsureRecurringPaymentsConsentsRevokedByUsuario condition = new EnsureRecurringPaymentsConsentsRevokedByUsuario();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "revokedBy returned in the response does not match the expected revokedBy";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
