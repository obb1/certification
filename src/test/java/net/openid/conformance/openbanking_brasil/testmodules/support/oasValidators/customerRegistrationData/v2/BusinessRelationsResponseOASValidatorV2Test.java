package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThrows;

public class BusinessRelationsResponseOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/corporateRelationshipV2/corporateRelationshipResponseOK-V2.1.0.json")
	public void testHappyPath() {
		BusinessRelationsResponseOASValidatorV2 cond = new BusinessRelationsResponseOASValidatorV2();
		run(cond);

		// accounts constraint.
		JsonObject account = new JsonObject();
		account.addProperty("type", "NOT_CONTA_PAGAMENTO_PRE_PAGA");
		JsonArray accounts = new JsonArray();
		accounts.add(account);
		JsonObject invalidAccountData = new JsonObject();
		invalidAccountData.add("accounts", accounts);
		assertThrows(ConditionError.class, () -> cond.assertAccountBranchCodeConstraint(invalidAccountData));
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/corporateRelationshipV2/errors/corporateRelationshipInvalidResponse-V2.1.0.json")
	public void testUnhappyPath() {
		BusinessRelationsResponseOASValidatorV2 cond = new BusinessRelationsResponseOASValidatorV2();
		runAndFail(cond);
	}
}
