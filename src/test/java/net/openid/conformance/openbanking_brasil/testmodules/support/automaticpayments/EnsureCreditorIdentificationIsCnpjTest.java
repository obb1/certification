package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureCreditorIdentificationIsCnpj;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureCreditorIdentificationIsCnpjTest extends AbstractJsonResponseConditionUnitTest {



	@Test
	public void happyPathTest() {
		environment.putString("resource", "creditorCpfCnpj", "11111111111111");
		run(new EnsureCreditorIdentificationIsCnpj());
	}

	@Test
	public void unhappyPathTestMissingCreditorCpfCnpj() {
		environment.putObject("resource", new JsonObject());
		unhappyPathTest("Unable to find \"creditorCpfCnpj\" in the resource");
	}

	@Test
	public void unhappyPathTestNotCnpj() {
		environment.putString("resource", "creditorCpfCnpj", "11111111111");
		unhappyPathTest("For automatic pix tests, the \"resource.creditorCpfCnpj\" field should be filled with a CNPJ");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureCreditorIdentificationIsCnpj());
		assertTrue(error.getMessage().contains(message));
	}
}
