package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class PostConsentOASValidatorV3Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullConsentResponseEnvKey.getKey());
		response.addProperty("status", 201);
	}

	@Test
	@UseResurce("jsonResponses/consent/createConsentResponse/v2/createConsentResponse.json")
	public void testHappyPath() {
		PostConsentOASValidatorV3 cond = new PostConsentOASValidatorV3();
		run(cond);
	}
}
