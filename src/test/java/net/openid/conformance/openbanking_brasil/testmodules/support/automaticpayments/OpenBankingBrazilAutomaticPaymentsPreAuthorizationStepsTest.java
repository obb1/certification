package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps;
import net.openid.conformance.sequence.client.CreateJWTClientAuthenticationAssertionAndAddToTokenEndpointRequest;
import org.junit.Test;

public class OpenBankingBrazilAutomaticPaymentsPreAuthorizationStepsTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void testTrue() {
		OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps condSeq = new OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps(
			CreateJWTClientAuthenticationAssertionAndAddToTokenEndpointRequest.class,
			true
		);
		condSeq.evaluate();
	}

	@Test
	public void testFalse() {
		OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps condSeq = new OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps(
			CreateJWTClientAuthenticationAssertionAndAddToTokenEndpointRequest.class,
			false
		);
		condSeq.evaluate();
	}
}
