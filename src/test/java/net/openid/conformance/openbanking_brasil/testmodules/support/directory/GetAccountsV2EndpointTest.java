package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAccountsV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/accounts/v2/accounts";
	}

	@Override
	protected String getApiFamilyType() {
		return "accounts";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetAccountsV2Endpoint();
	}
}
