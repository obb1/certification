package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setCreditors;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.AbstractEditRecurringPaymentsConsentBodyToSetCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.EditRecurringPaymentsConsentBodyToSetTwoPessoaJuridicaCreditors;

public class EditRecurringPaymentsConsentBodyToSetTwoPessoaJuridicaCreditorsTest extends AbstractEditRecurringPaymentsConsentBodyToSetCreditorTest {

	@Override
	protected AbstractEditRecurringPaymentsConsentBodyToSetCreditor condition() {
		return new EditRecurringPaymentsConsentBodyToSetTwoPessoaJuridicaCreditors();
	}

	@Override
	protected int expectedAmountOfCreditors() {
		return 2;
	}

	@Override
	protected String expectedPersonType() {
		return "PESSOA_JURIDICA";
	}
}
