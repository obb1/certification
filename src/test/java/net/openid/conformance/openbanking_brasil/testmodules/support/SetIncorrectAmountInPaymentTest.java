package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SetIncorrectAmountInPaymentTest extends AbstractJsonResponseConditionUnitTest {
	private static final String oldCurrency = "BRL";

	@Test
	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	public void happyPathSingleTest() {
		SetIncorrectAmountInPayment condition = new SetIncorrectAmountInPayment();
		run(condition);

		String consentAmount = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment").getAsJsonObject().get("amount"));
		String paymentAmount = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.payment").getAsJsonObject().get("amount"));

		BigDecimal newAmount;
			newAmount = new BigDecimal(consentAmount);
			newAmount = newAmount.add(new BigDecimal(100));

		assertEquals(paymentAmount, newAmount.toString());
	}

	@Test
	@UseResurce(value="test_resource_payments_v4.json", key="resource")
	public void happyPathArrayTest(){
		SetIncorrectAmountInPayment condition = new SetIncorrectAmountInPayment();
		run(condition);

		String consentAmount = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment").getAsJsonObject().get("amount"));
		BigDecimal newAmount;
		newAmount = new BigDecimal(consentAmount);
		newAmount = newAmount.add(new BigDecimal(100));

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {
			String paymentAmount = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("payment").get("amount"));
			assertNotEquals(paymentAmount, newAmount);
		}
	}

	@Test
	@UseResurce(value="test_resource_payments_v4_no_amount.json", key="resource")
	public void unhappyPathNoConsentCurrency() {
		SetIncorrectAmountInPayment condition = new SetIncorrectAmountInPayment();
		ConditionError error = runAndFail(condition);
		assertEquals("SetIncorrectAmountInPayment: Consent does not have an associated amount", error.getMessage());
	}

}
