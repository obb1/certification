package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SetIncorrectEndToEndIdTimeInThePaymentRequestBodyTest extends AbstractJsonResponseConditionUnitTest {
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";
	private static final String oldEndToEndId = "E9040088820210128000800123873170";

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpf.json", key=KEY, path=PATH)
	public void happyPathSingleTest() {
		SetIncorrectEndToEndIdTimeInThePaymentRequestBody condition = new SetIncorrectEndToEndIdTimeInThePaymentRequestBody();
		run(condition);

		String endToEndIdStart = oldEndToEndId.substring(0, 17);
		String endToEndIdEnd = oldEndToEndId.substring(21);
		String expectedEndToEndId = endToEndIdStart + "0100" + endToEndIdEnd;
		String newEndToEndId = OIDFJSON.getString(environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonObject("data").get("endToEndId"));
		assertEquals(expectedEndToEndId, newEndToEndId);
	}

	@Test
	@UseResurce(value="jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key=KEY, path=PATH)
	public void happyPathArrayTest() {
		SetIncorrectEndToEndIdTimeInThePaymentRequestBody condition = new SetIncorrectEndToEndIdTimeInThePaymentRequestBody();
		run(condition);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {
			String endToEndIdStart = oldEndToEndId.substring(0, 17);
			String endToEndIdEnd = oldEndToEndId.substring(21);
			String newEndToEndId = endToEndIdStart + "0100" + endToEndIdEnd;
			assertEquals(newEndToEndId, OIDFJSON.getString(data.get(i).getAsJsonObject().get("endToEndId")));
		}
	}

	@Test
	@UseResurce(value="test_config_no_data.json", key=KEY)
	public void unhappyPathNoData() {
		SetIncorrectEndToEndIdTimeInThePaymentRequestBody condition = new SetIncorrectEndToEndIdTimeInThePaymentRequestBody();
		ConditionError error = runAndFail(condition);
		assertEquals( "SetIncorrectEndToEndIdTimeInThePaymentRequestBody: Could not extract brazilPixPayment.data from the resource", error.getMessage());
	}

	@Test
	@UseResurce(value="jsonRequests/payments/payments/paymentWithDataArrayWithNoEndToEndId.json", key=KEY, path=PATH)
	public void unhappyPathNoE2EId() {
		SetIncorrectEndToEndIdTimeInThePaymentRequestBody condition = new SetIncorrectEndToEndIdTimeInThePaymentRequestBody();
		ConditionError error = runAndFail(condition);
		assertEquals( "SetIncorrectEndToEndIdTimeInThePaymentRequestBody: Could not extract endToEndId from the brazilPixPayment.data", error.getMessage());
	}
}
