package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractLastPaymentIdsTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_PATH = "jsonResponses/paymentInitiation/pixByPayments/v4/";

	@Test
	@UseResurce(BASE_PATH + "standardPostPaymentsV4ResponseWith5Payments.json")
	public void happyPathLastFivePaymentsTest() {
		ExtractFiveLastPaymentIds condition = new ExtractFiveLastPaymentIds();
		run(condition);
		JsonArray paymentIds = environment.getObject("payment_ids").getAsJsonArray("paymentIds");
		List<String> expectedPaymentIds = List.of(
			"TXpRMU9UQTROMWhZV2xSU1FUazJSMDl", "TXpRMU9UQTROMWhZV2xSU1FUazJSMDf", "TXpRMU9UQTROMWhZV2xSU1FUazJSMDG",
			"TXpRMU9UQTROMWhZV2xSU1FUazJSMDH", "TXpRMU9UQTROMWhZV2xSU1FUazJSMDP"
		);
		for (int i = 0; i < paymentIds.size(); i++) {
			String actualPaymentId = OIDFJSON.getString(paymentIds.get(i));
			assertEquals(expectedPaymentIds.get(i), actualPaymentId);
		}
	}

	@Test
	@UseResurce(BASE_PATH + "standardPostPaymentsV4ResponseWith2Payments.json")
	public void happyPathLastTwoPaymentsTest() {
		ExtractTwoLastPaymentIds condition = new ExtractTwoLastPaymentIds();
		run(condition);
		JsonArray paymentIds = environment.getObject("payment_ids").getAsJsonArray("paymentIds");
		List<String> expectedPaymentIds = List.of(
			"TXpRMU9UQTROMWhZV2xSU1FUazJSMDl", "TXpRMU9UQTROMWhZV2xSU1FUazJSMDf"
		);
		for (int i = 0; i < paymentIds.size(); i++) {
			String actualPaymentId = OIDFJSON.getString(paymentIds.get(i));
			assertEquals(expectedPaymentIds.get(i), actualPaymentId);
		}
	}

	@Test
	public void unhappyPathTestMissingResponse() {
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestInvalidJWT() {
		environment.putObject("resource_endpoint_response_full",
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the response body");
	}

	@Test
	@UseResurce(BASE_PATH + "standardPostPaymentsV4ResponseWith2Payments.json")
	public void unhappyPathTestLessThanFivePayments() {
		unhappyPathTest("The response payload is expected to have at least 5 payments in it");
	}

	@Test
	@UseResurce(BASE_PATH + "postPaymentsV4ResponseMissingPaymentId.json")
	public void unhappyPathTestMissingPaymentId() {
		unhappyPathTest("Could not extract payment ID from payment");
	}

	public void unhappyPathTest(String expectedMessage) {
		ExtractFiveLastPaymentIds condition = new ExtractFiveLastPaymentIds();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
