package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.productsNServices.financings.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetFinancingsOASValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/productsNServices/v1/financings/businessFinancingsResponseOKV1.0.1.json")
	public void happyPathBusiness() {
		run(new GetBusinessFinancingsOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/productsNServices/v1/financings/personalFinancingsResponseOKV1.0.1.json")
	public void happyPathPersonal() {
		run(new GetPersonalFinancingsOASValidatorV1());
	}
}
