package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureEnrollmentDoesNotContainExpirationDateTimeTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonResponses/enrollments/v2/GetEnrollments200Response";

	@Test
	@UseResurce(BASE_JSON_PATH + "NoExpirationDateTime.json")
	public void happyPathTest() {
		run(new EnsureEnrollmentDoesNotContainExpirationDateTime());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(EnsureEnrollmentDoesNotContainExpirationDateTime.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureEnrollmentDoesNotContainExpirationDateTime.RESPONSE_ENV_KEY,
			new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(EnsureEnrollmentDoesNotContainExpirationDateTime.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build());
		unhappyPathTest("Could not find data inside enrollment response body");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void unhappyPathTestHasExpirationDateTime() {
		unhappyPathTest("No expirationDateTime was expected in the enrollment response");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureEnrollmentDoesNotContainExpirationDateTime());
		assertTrue(error.getMessage().contains(message));
	}
}
