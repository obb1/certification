package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetResourcesOASValidatorV3Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/resourcesAPI/resourcesAPIResponseMultipleResources.json")
	public void happyPath() {
		run(new GetResourcesOASValidatorV3());
	}
}
