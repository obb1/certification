package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetVariableIncomesBalancesV1n21OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/variableIncomes/v1.2.0/balances.json")
	public void happyPath() {
		run(new GetVariableIncomesBalancesV1n21OASValidator());
	}


}
