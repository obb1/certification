package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.pixqrcode.PixQRCode;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class InjectQRCodeWithWrongAmountIntoConfigTest extends AbstractJsonResponseConditionUnitTest {
	private static final String oldAmount = "100000.12";
	private PixQRCode qrCode;

	@Before
	public void setUp() throws Exception {
		qrCode = new PixQRCode();
		qrCode.useStandardConfig();
		qrCode.setTransactionAmount(oldAmount);
	}

	@Test
	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	public void happyPathSingleTest() {
		InjectQRCodeWithWrongAmountIntoConfig condition = new InjectQRCodeWithWrongAmountIntoConfig();
		run(condition);

		String consentQrCode = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.qrCode"));
		String paymentQrCode = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.qrCode"));

		assertNotEquals(qrCode.toString(), consentQrCode);
		assertNotEquals(qrCode.toString(), paymentQrCode);
	}

	@Test
	@UseResurce(value="test_resource_payments_v4.json", key="resource")
	public void happyPathArrayTest() throws IOException {
		InjectQRCodeWithWrongAmountIntoConfig condition = new InjectQRCodeWithWrongAmountIntoConfig();
		run(condition);

		String consentQrCode = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details.qrCode"));
		assertNotEquals(qrCode.toString(), consentQrCode);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {
			String paymentQrCode = OIDFJSON.getString(data.get(i).getAsJsonObject().get("qrCode"));
			assertNotEquals(qrCode.toString(), paymentQrCode);
		}
	}
}
