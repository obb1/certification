package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractRecurringConsentIdFromConsentEndpointResponse;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExtractRecurringConsentIdFromConsentEndpointResponseTest extends AbstractJsonResponseConditionUnitTest {

	private static final String RESOURCE_BASE_PATH = "jsonResponses/automaticpayments/GetRecurringConsentsResponse";
	private static final String MISSING_FIELD_MESSAGE = "Could not find data.recurringConsentId";
	private static final String CONSENT_ID = "urn:GwSyboYbAOZb2GO5qGYffhVhl-8q:/dziVcd2EZX7,kwnOy.uZvvnK,q@GXqPbQn;yMM+SZoLf8(k_EeWWtC2Wm;7?l3o-o;-)a:i,#Ei/cOalp'";

	@Test
	@UseResurce(RESOURCE_BASE_PATH + "OK.json")
	public void happyPathTest() {
		ExtractRecurringConsentIdFromConsentEndpointResponse condition = new ExtractRecurringConsentIdFromConsentEndpointResponse();
		run(condition);

		assertEquals(CONSENT_ID, environment.getString("consent_id"));
	}

	@Test
	public void unhappyPathMissingConsentResponseTest() {
		ExtractRecurringConsentIdFromConsentEndpointResponse condition = new ExtractRecurringConsentIdFromConsentEndpointResponse();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not extract body from response";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(RESOURCE_BASE_PATH + "MissingRecurringConsentId.json")
	public void unhappyPathMissingRecurringConsentIdTest() {
		ExtractRecurringConsentIdFromConsentEndpointResponse condition = new ExtractRecurringConsentIdFromConsentEndpointResponse();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}
}
