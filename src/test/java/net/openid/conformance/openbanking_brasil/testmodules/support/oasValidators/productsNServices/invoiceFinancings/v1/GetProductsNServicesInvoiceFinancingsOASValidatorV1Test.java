package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.productsNServices.invoiceFinancings.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetProductsNServicesInvoiceFinancingsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/productsNServices/v1/invoiceFinancings/personalInvoiceFinancingsResponseOK.json")
	public void testHappyPathPersonal() {
		GetProductsNServicesPersonalInvoiceFinancingsOASValidatorV1 cond = new GetProductsNServicesPersonalInvoiceFinancingsOASValidatorV1();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/productsNServices/v1/invoiceFinancings/businessInvoiceFinancingsResponseOK.json")
	public void testHappyPathBusiness() {
		GetProductsNServicesBusinessInvoiceFinancingsOASValidatorV1 cond = new GetProductsNServicesBusinessInvoiceFinancingsOASValidatorV1();
		run(cond);
	}

}
