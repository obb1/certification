package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bonds.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.capitalizationBonds.v2.GetCapitalizationBondsOASValidatorV2;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class GetCapitalizationBondsOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/capitalizationBonds/V2/CapitalizationBondsResponseV2.json")
	public void evaluate() {
		run(new GetCapitalizationBondsOASValidatorV2());
	}

	@Test
	@UseResurce("jsonResponses/opendata/capitalizationBonds/V2/CapitalizationBondsResponseV2(totalRecordsNOTFound).json")
	public void totalRecordsNOTFound() {
		GetCapitalizationBondsOASValidatorV2 condition = new GetCapitalizationBondsOASValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createElementNotFoundMessage("totalRecords", condition.getApiName())));
	}

	@Test
	@UseResurce("jsonResponses/opendata/capitalizationBonds/V2/CapitalizationBondsResponseV2(linkSelfNOTFound).json")
	public void linksSelfNOTFound() {
		GetCapitalizationBondsOASValidatorV2 condition = new GetCapitalizationBondsOASValidatorV2();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createElementNotFoundMessage("self", condition.getApiName())));
	}
}

