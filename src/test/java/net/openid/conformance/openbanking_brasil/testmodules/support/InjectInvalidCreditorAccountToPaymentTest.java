package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectInvalidCreditorAccountToPayment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InjectInvalidCreditorAccountToPaymentTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	public void happyPathSingleTest() {
		InjectInvalidCreditorAccountToPayment condition = new InjectInvalidCreditorAccountToPayment();
		run(condition);

		String issuer = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.creditorAccount").getAsJsonObject().get("issuer"));
		String number = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.creditorAccount").getAsJsonObject().get("number"));
		String accountType = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.creditorAccount").getAsJsonObject().get("accountType"));
		String ispb = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.creditorAccount").getAsJsonObject().get("ispb"));

		assertEquals(issuer, DictHomologKeys.PROXY_EMAIL_BRANCH_NUMBER);
		assertEquals(number, DictHomologKeys.PROXY_EMAIL_ACCOUNT_MISMATCHED_NUMBER);
		assertEquals(accountType, DictHomologKeys.PROXY_EMAIL_ACCOUNT_TYPE);
		assertEquals(ispb, DictHomologKeys.PROXY_EMAIL_MISMATCHED_ISPB);
	}

	@Test
	@UseResurce(value="test_resource_payments_v4.json", key="resource")
	public void happyPathArrayTest(){
		InjectInvalidCreditorAccountToPayment condition = new InjectInvalidCreditorAccountToPayment();
		run(condition);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");
		for (int i = 0 ; i < data.size(); i++) {

			String issuer = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").getAsJsonObject().get("issuer"));
			String number = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").getAsJsonObject().get("number"));
			String accountType = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").getAsJsonObject().get("accountType"));
			String ispb = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("creditorAccount").getAsJsonObject().get("ispb"));

			assertEquals(issuer, DictHomologKeys.PROXY_EMAIL_BRANCH_NUMBER);
			assertEquals(number, DictHomologKeys.PROXY_EMAIL_ACCOUNT_MISMATCHED_NUMBER);
			assertEquals(accountType, DictHomologKeys.PROXY_EMAIL_ACCOUNT_TYPE);
			assertEquals(ispb, DictHomologKeys.PROXY_EMAIL_MISMATCHED_ISPB);
		}
	}
}
