package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.Gson;
import com.mongodb.client.MongoClient;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.info.TestPlanService;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.token.TokenService;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;


@SpringBootTest
@RunWith(SpringRunner.class)
public class EnsureWellKnownUriIsRegisteredTest extends AbstractJsonResponseConditionUnitTest {


	@MockBean
	ParticipantsService participantsService;

	@MockBean
	MongoClient mongoClient;


	@MockBean
	ServerInfoTemplate serverInfoTemplate;

	@MockBean
	TestInfoService testInfoService;

	@MockBean
	TestPlanService testPlanService;

	@MockBean
	TokenService tokenService;

	@MockBean
	EventLog eventLog;


	@Before
	public void init() throws IOException {
		String org = IOUtils.resourceToString("jsonResponses/participantsEndpoint/participantsResponse.json", Charset.defaultCharset(), getClass().getClassLoader());
		Map all = new Gson().fromJson(org, Map.class);
		Mockito.when(participantsService.orgsFor(Set.of("org1"))).thenReturn(Set.of(all));
		environment.putString("config", "resource.brazilOrganizationId", "org1");
	}


	@Test
	public void weCanFilterWithWellKnownUri() {
		environment.putString("config", "server.discoveryUrl", "https://url1.com");
		EnsureWellKnownUriIsRegistered cond = new EnsureWellKnownUriIsRegistered();
		run(cond);
	}

	@Test
	public void weCanFilterWithAuthorisationServerId() {
		environment.putString("authorisationServerId", "org1-server3");
		EnsureWellKnownUriIsRegistered cond = new EnsureWellKnownUriIsRegistered();
		run(cond);
	}

	@Test
	public void authorisationServerIdFilterHasPriority() {
		environment.putString("config", "server.discoveryUrl", "does not exist");
		environment.putString("authorisationServerId", "org1-server2");
		EnsureWellKnownUriIsRegistered cond = new EnsureWellKnownUriIsRegistered();
		run(cond);
	}

	@Test
	public void weGetErrorIfServerWithSpecifiedWellKnownDoesNotExist() {
		environment.putString("config", "server.discoveryUrl", "does not exist");
		EnsureWellKnownUriIsRegistered cond = new EnsureWellKnownUriIsRegistered();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("EnsureWellKnownUriIsRegistered: Could not find Authorisation Server with provided Well-Known URL in the Directory Participants List", e.getMessage());
	}

	@Test
	public void weGetErrorIfServerWithSpecifiedServerIdDoesNotExist() {
		environment.putString("config", "server.discoveryUrl", "https://url1.com");
		environment.putString("authorisationServerId", "does not exist");
		EnsureWellKnownUriIsRegistered cond = new EnsureWellKnownUriIsRegistered();
		ConditionError e = runAndFail(cond);
		Assert.assertEquals("EnsureWellKnownUriIsRegistered: Could not find Authorisation Server with provided Authorisation Server ID in the Directory Participants List", e.getMessage());
	}

}
