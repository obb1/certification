package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetCreditCardsAccountsV2EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/credit-cards-accounts/v2/accounts";
	}

	@Override
	protected String getApiFamilyType() {
		return "credit-cards-accounts";
	}

	@Override
	protected String getApiVersion() {
		return "2.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetCreditCardsAccountsV2Endpoint();
	}
}
