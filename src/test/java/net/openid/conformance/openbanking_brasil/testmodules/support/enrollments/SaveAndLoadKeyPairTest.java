package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SaveAndLoadKeyPairTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		String originalKid = "123";
		String newKid = "abc";

		JsonObject keyPair = new JsonObjectBuilder().addField("kid", originalKid).build();
		environment.putObject("fido_keys_jwk", keyPair);

		// Test that when keyPair is edited, the saved one does not change
		run(new SaveKeyPair());
		keyPair.addProperty("kid", newKid);
		JsonObject savedKeyPair = environment.getObject("saved_fido_keys_jwk");
		assertEquals(originalKid, OIDFJSON.getString(savedKeyPair.get("kid")));

		// Test that loading the saved keyPair actually updates the env variable
		run(new LoadOldKeyPair());
		keyPair = environment.getObject("fido_keys_jwk");
		assertEquals(originalKid, OIDFJSON.getString(keyPair.get("kid")));

		// Test that, after loading, the current keyPair and the savedKeyPair are the same object
		keyPair.addProperty("kid", newKid);
		savedKeyPair = environment.getObject("saved_fido_keys_jwk");
		assertEquals(newKid, OIDFJSON.getString(savedKeyPair.get("kid")));
	}
}
