package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithRealEmailIntoConfig;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.mockito.Mockito.mock;

public class InjectQRCodeWithRealRealEmailIntoConfigTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathArrayTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4_no_qrcodes.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();

		environment.putObject("resource", resourceConfig);
		environment.putString("payment_is_array","ok");

		InjectQRCodeWithRealEmailIntoConfig condition = new InjectQRCodeWithRealEmailIntoConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);

		JsonArray paymentConfigArray = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray();

		for (JsonElement element : paymentConfigArray) {
			JsonObject paymentConfig = element.getAsJsonObject();
			Assert.assertNotNull(paymentConfig.get("qrCode"));
		}
	}

	@Test
	public void happyPathSingleTest() throws IOException {
		String rawJson = IOUtils.resourceToString("test_config_payments_v4_single_no_qrCodes.json", Charset.defaultCharset(), getClass().getClassLoader());

		JsonObject config = new JsonParser().parse(rawJson).getAsJsonObject();

		Environment environment = new Environment();
		JsonObject resourceConfig = config.get("resource").getAsJsonObject();
		environment.putObject("resource", resourceConfig);

		InjectQRCodeWithRealEmailIntoConfig condition = new InjectQRCodeWithRealEmailIntoConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);
		condition.evaluate(environment);
		JsonObject paymentConfig = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonObject();

		Assert.assertNotNull(paymentConfig.get("qrCode"));
	}


}
