package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseCodeWas400Or422Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureConsentResponseCodeWas400Or422();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.UNPROCESSABLE_ENTITY.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.OK.value();
	}

}
