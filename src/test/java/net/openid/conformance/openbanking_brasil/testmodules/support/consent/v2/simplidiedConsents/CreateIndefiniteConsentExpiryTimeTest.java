package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplidiedConsents;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;

public class CreateIndefiniteConsentExpiryTimeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value = "jsonRequests/consents/v3/consentRequestNoExpirationDateTime.json", key = "consent_endpoint_request")
	public void happyPathTest() {
		run(new CreateIndefiniteConsentExpiryTime());
		String expirationDateTimeStr = OIDFJSON.getString(
			environment.getObject("consent_endpoint_request")
				.getAsJsonObject("data")
				.get("expirationDateTime")
		);
		LocalDateTime expirationDateTime = LocalDateTime.parse(expirationDateTimeStr, DateTimeFormatter.ISO_DATE_TIME);
		LocalDate expirationDate = expirationDateTime.toLocalDate();

		LocalDate indefiniteDate = LocalDate.of(2300, 1, 1);

		assertEquals(expirationDate, indefiniteDate);
	}

	@Test
	@UseResurce(value = "jsonResponses/account/transactions/accountTransactionsResponseWithoutData.json", key = "consent_endpoint_request")
	public void unhappyPathTestMissingConsentRequest() {
		CreateIndefiniteConsentExpiryTime condition = new CreateIndefiniteConsentExpiryTime();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Consent Endpoint Request does not have data field"));
	}

}
