package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class AbstractRecurringConsentOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/";

	protected abstract int status();
	protected abstract AbstractRecurringConsentOASValidatorV2 validator();

	@Before
	public void setUp() {
		setStatus(status());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseAutomaticWithMultipleCreditors.json")
	public void unhappyPathTestAutomaticWithMultipleCreditors() {
		unhappyPathTest("There should only be one creditor when recurringConfiguration is set to automatic");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseAutomaticWithPessoaNaturalCreditor.json")
	public void unhappyPathTestAutomaticWithPessoaNaturalCreditor() {
		unhappyPathTest("The only creditor should be of type PESSOA_JURIDICA when recurringConfiguration is set to automatic");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseAuthorisedNoDebtorAccount.json")
	public void unhappyPathTestAuthorisedNoDebtorAccount() {
		unhappyPathTest("debtorAccount is required when status is [AUTHORISED, REVOKED, CONSUMED]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseRevokedNoDebtorAccount.json")
	public void unhappyPathTestRevokedNoDebtorAccount() {
		unhappyPathTest("debtorAccount is required when status is [AUTHORISED, REVOKED, CONSUMED]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseConsumedNoDebtorAccount.json")
	public void unhappyPathTestConsumedNoDebtorAccount() {
		unhappyPathTest("debtorAccount is required when status is [AUTHORISED, REVOKED, CONSUMED]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseDebtorAccountCACCNoIssuer.json")
	public void unhappyPathTestDebtorAccountCACCNoIssuer() {
		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseDebtorAccountSVGSNoIssuer.json")
	public void unhappyPathTestDebtorAccountSVGSNoIssuer() {
		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseRejectedNoRejection.json")
	public void unhappyPathTestRejectedNoRejection() {
		unhappyPathTest("rejection is required when status is REJECTED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseRevokedNoRevocation.json")
	public void unhappyPathTestRevokedNoRevocation() {
		unhappyPathTest("revocation is required when status is REVOKED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseAuthorisedNoAuthorisedAtDateTime.json")
	public void unhappyPathTestAuthorisedNoAuthorisedAtDateTime() {
		unhappyPathTest("authorisedAtDateTime is required when status is AUTHORISED");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseAutomaticFixedAndMaximumVariableAmounts.json")
	public void unhappyPathTestAutomaticFixedAndMaximumVariableAmounts() {
		unhappyPathTest("The recurring payment can either have a fixed amount or a variable amount, not both");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseAutomaticFixedAndMinimumVariableAmounts.json")
	public void unhappyPathTestAutomaticFixedAndMinimumVariableAmounts() {
		unhappyPathTest("The recurring payment can either have a fixed amount or a variable amount, not both");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseAutomaticFixedAndMinimumAndMaximumVariableAmounts.json")
	public void unhappyPathTestAutomaticFixedAndMinimumAndMaximumVariableAmounts() {
		unhappyPathTest("The recurring payment can either have a fixed amount or a variable amount, not both");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseFirstPaymentCreditorAccountCACCNoIssuer.json")
	public void unhappyPathTestFirstPaymentCreditorAccountCACCNoIssuer() {
		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseFirstPaymentCreditorAccountSVGSNoIssuer.json")
	public void unhappyPathTestFirstPaymentCreditorAccountSVGSNoIssuer() {
		unhappyPathTest("issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseSweepingDayNoLimits.json")
	public void unhappyPathTestSweepingDayNoLimits() {
		unhappyPathTest("day must have at least one of quantityLimit or transactionLimit.");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseSweepingWeekNoLimits.json")
	public void unhappyPathTestSweepingWeekNoLimits() {
		unhappyPathTest("week must have at least one of quantityLimit or transactionLimit.");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseSweepingMonthNoLimits.json")
	public void unhappyPathTestSweepingMonthNoLimits() {
		unhappyPathTest("month must have at least one of quantityLimit or transactionLimit.");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseSweepingYearNoLimits.json")
	public void unhappyPathTestSweepingYearNoLimits() {
		unhappyPathTest("year must have at least one of quantityLimit or transactionLimit.");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseVrpDayNoLimits.json")
	public void unhappyPathTestVrpDayNoLimits() {
		unhappyPathTest("day must have at least one of quantityLimit or transactionLimit.");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseVrpWeekNoLimits.json")
	public void unhappyPathTestVrpWeekNoLimits() {
		unhappyPathTest("week must have at least one of quantityLimit or transactionLimit.");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseVrpMonthNoLimits.json")
	public void unhappyPathTestVrpMonthNoLimits() {
		unhappyPathTest("month must have at least one of quantityLimit or transactionLimit.");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostRecurringConsents201ResponseVrpYearNoLimits.json")
	public void unhappyPathTestVrpYearNoLimits() {
		unhappyPathTest("year must have at least one of quantityLimit or transactionLimit.");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(validator());
		assertTrue(error.getMessage().contains(message));
	}
}
