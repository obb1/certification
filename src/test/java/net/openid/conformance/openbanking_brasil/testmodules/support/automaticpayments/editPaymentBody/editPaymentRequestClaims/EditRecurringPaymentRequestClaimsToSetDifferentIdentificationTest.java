package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.editPaymentRequestClaims;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims.EditRecurringPaymentRequestClaimsToSetDifferentIdentification;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentRequestClaimsToSetDifferentIdentificationTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "resource_request_entity_claims";
	private static final String BASE_JSON_PATH = "jsonRequests/automaticPayments/payments/v2/PostRecurringPaymentsPixRequestBody";

	@Test
	@UseResurce(value = BASE_JSON_PATH + "CpfDocument.json", key = KEY)
	public void happyPathTestCpf() {
		run(new EditRecurringPaymentRequestClaimsToSetDifferentIdentification());

		String oldCpf = "11111111111";
		String newCpf = OIDFJSON.getString(environment.getElementFromObject(KEY, "data.document.identification"));
		assertNotEquals(oldCpf, newCpf);
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + ".json", key = KEY)
	public void happyPathTestCnpj() {
		run(new EditRecurringPaymentRequestClaimsToSetDifferentIdentification());

		String oldCnpj = "11111111111111";
		String newCnpj = OIDFJSON.getString(environment.getElementFromObject(KEY, "data.document.identification"));
		assertNotEquals(oldCnpj, newCnpj);
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data.document in recurring-payments request payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithoutDocument.json", key = KEY)
	public void unhappyPathTestMissingDocument() {
		unhappyPathTest("Unable to find data.document in recurring-payments request payload");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "WithoutIdentification.json", key = KEY)
	public void unhappyPathTestMissingIdentification() {
		unhappyPathTest("Unable to find identification field for the document");
	}

	@Test
	@UseResurce(value = BASE_JSON_PATH + "InvalidIdentification.json", key = KEY)
	public void unhappyPathTestInvalidIdentification() {
		unhappyPathTest("The value inside the identification field is neither a CPF nor a CNPJ");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentRequestClaimsToSetDifferentIdentification());
		assertTrue(error.getMessage().contains(message));
	}
}
