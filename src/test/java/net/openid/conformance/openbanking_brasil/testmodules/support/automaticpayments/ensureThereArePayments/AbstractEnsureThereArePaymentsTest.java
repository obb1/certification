package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class AbstractEnsureThereArePaymentsTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/GetRecurringPaymentsList200Response";

	protected abstract AbstractEnsureThereArePayments condition();

	@Test
	public void happyPathTest() {
		run(condition());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(AbstractEnsureThereArePayments.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject(AbstractEnsureThereArePayments.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ThreeAcsc.json")
	public void unhappyPathTestAmountNotMatching() {
		unhappyPathTest("The amount of payments returned is not what was expected");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "TwoMissingStatus.json")
	public void unhappyPathTestMissingStatus() {
		unhappyPathTest("Payment does not have status field");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "TwoNonExistingStatus.json")
	public void unhappyPathTestInvalidStatus() {
		unhappyPathTest("Invalid status in payment");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "TwoCanc.json")
	public void unhappyPathTestStatusNotMatching() {
		unhappyPathTest("Unexpected status found in payment");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
