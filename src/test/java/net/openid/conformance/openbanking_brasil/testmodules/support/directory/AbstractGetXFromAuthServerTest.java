package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public abstract class AbstractGetXFromAuthServerTest extends AbstractGetEndpointFromAuthServerTest {

	@Before
	public void init() {
		environment.putObject("config", "resource", new JsonObject());
	}

	@Test
	public void happyPath() {
		addApiResourcesToServer(List.of(
			createCorrectResource()
		));

		run(getCondition());

		if (isResource()) {
			assertEquals(getEndpoint(), environment.getString("config", "resource.resourceUrl"));
			assertNull(environment.getString("config", "resource.consentUrl"));
		} else {
			assertEquals(getEndpoint(), environment.getString("config", "resource.consentUrl"));
			assertNull(environment.getString("config", "resource.resourceUrl"));
		}
	}

	@Test
	public void unhappyPathWrongEndpoint(){
		addApiResourcesToServer(List.of(
			createCorrectResourceWithOverride(null, null, "test")
		));
		assertUrlNotFound();
	}

	@Test
	public void unhappyPathWrongVersion(){
		addApiResourcesToServer(List.of(
			createCorrectResourceWithOverride(null, "test", null)
		));
		assertUrlNotFound();
	}

	@Test
	public void unhappyPathWrongFamilyType(){
		addApiResourcesToServer(List.of(
			createCorrectResourceWithOverride("test", null, null)
		));
		assertUrlNotFound();
	}

	protected void assertUrlNotFound() {
		ConditionError e = runAndFail(getCondition());
		String type = isResource() ? "resource" : "consent";
		assertEquals(getErrorMessage(String.format("Unable to locate %s endpoint to continue", type)), e.getMessage());
	}


	protected JsonObject createCorrectResource() {
		return createApiResource(getApiFamilyType(), getApiVersion(), List.of(getEndpoint()));
	}

	private JsonObject createCorrectResourceWithOverride(String familyType, String version, String url) {
		familyType = Strings.isNullOrEmpty(familyType) ? getApiFamilyType() : familyType;
		version = Strings.isNullOrEmpty(version) ? getApiVersion() : version;
		url = Strings.isNullOrEmpty(url) ? getEndpoint() : url;
		return createApiResource(familyType, version, List.of(url));
	}



	protected abstract String getEndpoint();

	protected abstract String getApiFamilyType();

	protected abstract String getApiVersion();

	protected abstract boolean isResource();

	@Override
	protected abstract AbstractGetXFromAuthServer getCondition();


}
