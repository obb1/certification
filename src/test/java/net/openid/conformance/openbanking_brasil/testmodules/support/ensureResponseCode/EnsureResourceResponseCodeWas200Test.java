package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureResourceResponseCodeWas200Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureResourceResponseCodeWas200();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.OK.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}

}
