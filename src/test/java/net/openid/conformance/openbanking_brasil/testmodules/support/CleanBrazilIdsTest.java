package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CleanBrazilIdsTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		environment.putObject("config", new JsonObject());
	}

	@Test
	public void testValidCpf() {
		var cond = new CleanBrazilIds();

		run(cond);

		environment.putString("config", "resource.brazilCpf", "76109277673");
		run(cond);
		assertEquals("76109277673", environment.getString("config", "resource.brazilCpf"));

		environment.putString("config", "resource.brazilCpf", "761.092.776-73");
		run(cond);
		assertEquals("76109277673", environment.getString("config", "resource.brazilCpf"));
	}

	@Test
	public void testInvalidCpf() {
		var cond = new CleanBrazilIds();

		environment.putString("config", "resource.brazilCpf", "7610927767x");
		runAndFail(cond);

		environment.putString("config", "resource.brazilCpf", "761092776730000");
		runAndFail(cond);
	}

	@Test
	public void testValidCnpj() {
		var cond = new CleanBrazilIds();

		run(cond);

		environment.putString("config", "resource.brazilCnpj", "47761642000177");
		run(cond);
		assertEquals("47761642000177", environment.getString("config", "resource.brazilCnpj"));

		environment.putString("config", "resource.brazilCnpj", "47.761.642/0001-77");
		run(cond);
		assertEquals("47761642000177", environment.getString("config", "resource.brazilCnpj"));
	}

	@Test
	public void testInvalidCnpj() {
		var cond = new CleanBrazilIds();

		environment.putString("config", "resource.brazilCnpj", "x7761642000177");
		runAndFail(cond);

		environment.putString("config", "resource.brazilCnpj", "477616420001770000");
		runAndFail(cond);
	}
}
