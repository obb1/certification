package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureErrorResponseCode;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasCampoNaoPermitido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasDetalheEdicaoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasDetalheTentativaInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasForaPrazoPermitido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimiteValorTotalConsentimentoExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode.EnsureErrorResponseCodeFieldWasLimiteValorTransacaoConsentimentoExcedido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureErrorResponseCodeFieldWasTests extends AbstractJsonResponseConditionUnitTest {

	private static final String PATH = "jsonResponses/automaticpayments/paymentErrors/PostRecurringPaymentsErrorCode";

	@Before
	public void init() {
		environment.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoQuantidadeExcedido.json")
	public void happyPathLimitePeriodoQuantidadeExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido condition = new EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoValorExcedido.json")
	public void unhappyPathLimitePeriodoQuantidadeExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido condition = new EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not find error with expected code in the errors JSON array";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoValorExcedido.json")
	public void happyPathLimitePeriodoValorExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido condition = new EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoQuantidadeExcedido.json")
	public void unhappyPathLimitePeriodoValorExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido condition = new EnsureErrorResponseCodeFieldWasLimitePeriodoValorExcedido();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not find error with expected code in the errors JSON array";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(PATH+"CampoNaoPermitido.json")
	public void happyPathCampoNaoPermitidoTest() {
		EnsureErrorResponseCodeFieldWasCampoNaoPermitido condition = new EnsureErrorResponseCodeFieldWasCampoNaoPermitido();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"DetalheTentativaInvalido.json")
	public void happyPathDetalheTentativaInvalidoTest() {
		EnsureErrorResponseCodeFieldWasDetalheTentativaInvalido condition = new EnsureErrorResponseCodeFieldWasDetalheTentativaInvalido();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"ForaPrazoPermitido.json")
	public void happyPathForaPrazoPermitidoTest() {
		EnsureErrorResponseCodeFieldWasForaPrazoPermitido condition = new EnsureErrorResponseCodeFieldWasForaPrazoPermitido();
		run(condition);
	}

	@UseResurce(PATH+"ValorTransacaoConsentimentoExcedido.json")
	public void happyPathLimiteValorTransacaoConsentimentoExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimiteValorTransacaoConsentimentoExcedido condition = new EnsureErrorResponseCodeFieldWasLimiteValorTransacaoConsentimentoExcedido();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoQuantidadeExcedido.json")
	public void unhappyPathCampoNaoPermitidoTest() {
		EnsureErrorResponseCodeFieldWasCampoNaoPermitido condition = new EnsureErrorResponseCodeFieldWasCampoNaoPermitido();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not find error with expected code in the errors JSON array";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@UseResurce(PATH+"LimitePeriodoQuantidadeExcedido.json")
	public void unhappyPathLimiteValorTransacaoConsentimentoExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimiteValorTransacaoConsentimentoExcedido condition = new EnsureErrorResponseCodeFieldWasLimiteValorTransacaoConsentimentoExcedido();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not find error with expected code in the errors JSON array";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(PATH+"LimiteValorTotalConsentimentoExcedido.json")
	public void happyPathLimiteValorTotalConsentimentoExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimiteValorTotalConsentimentoExcedido condition = new EnsureErrorResponseCodeFieldWasLimiteValorTotalConsentimentoExcedido();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoQuantidadeExcedido.json")
	public void unhappyPathLimiteValorTotalConsentimentoExcedidoTest() {
		EnsureErrorResponseCodeFieldWasLimiteValorTotalConsentimentoExcedido condition = new EnsureErrorResponseCodeFieldWasLimiteValorTotalConsentimentoExcedido();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not find error with expected code in the errors JSON array";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(PATH+"DetalheEdicaoInvalido.json")
	public void happyPathDetalheEdicaoInvalidoTest() {
		EnsureErrorResponseCodeFieldWasDetalheEdicaoInvalido condition = new EnsureErrorResponseCodeFieldWasDetalheEdicaoInvalido();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoValorExcedido.json")
	public void unhappyPathDetalheEdicaoInvalidoTest() {
		EnsureErrorResponseCodeFieldWasDetalheEdicaoInvalido condition = new EnsureErrorResponseCodeFieldWasDetalheEdicaoInvalido();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not find error with expected code in the errors JSON array";
		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	@UseResurce(PATH+"FaltamSinaisObrigatoriosPlataforma.json")
	public void happyPathFaltamSinaisObrigatoriosPlataformaTest() {
		EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma condition = new EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma();
		run(condition);
	}

	@Test
	@UseResurce(PATH+"LimitePeriodoValorExcedido.json")
	public void unhappyPathFaltamSinaisObrigatoriosPlataformaTest() {
		EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma condition = new EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "Could not find error with expected code in the errors JSON array";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
