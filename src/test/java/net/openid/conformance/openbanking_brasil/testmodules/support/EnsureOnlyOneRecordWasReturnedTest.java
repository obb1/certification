package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;

public class EnsureOnlyOneRecordWasReturnedTest extends AbstractJsonResponseConditionUnitTest {


	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentGood.json")
	@Test
	public void happyPath() {
		EnsureOnlyOneRecordWasReturned cond = new EnsureOnlyOneRecordWasReturned();
		run(cond);
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentGoodMultiple.json")
	@Test
	public void moreThan2Returned() {
		EnsureOnlyOneRecordWasReturned cond = new EnsureOnlyOneRecordWasReturned();
		ConditionError conditionError = runAndFail(cond);
		Assert.assertThat(conditionError.getMessage(), containsString("Array from element data is more than the required maxItems on the EnsureOnlyOneRecordWasReturned API response"));
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentEmptyDataGood.json")
	@Test
	public void emptyDataWasReturned() {
		EnsureOnlyOneRecordWasReturned cond = new EnsureOnlyOneRecordWasReturned();
		ConditionError conditionError = runAndFail(cond);
		Assert.assertThat(conditionError.getMessage(), containsString("Array from element data is less than the required minItems on the EnsureOnlyOneRecordWasReturned API response"));
	}

}
