package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsRejectionReasonEnum;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecurringPaymentsRejectionReasonEnumTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		assertEquals(21, RecurringPaymentsRejectionReasonEnum.toSet().size());
	}
}
