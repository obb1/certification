package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class ExtractUserIdFromFidoRegistrationOptionsResponseTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void init() {
		setJwt(true);
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptions.json")
	public void evaluateUserIdExtraction() {
		run(new ExtractUserIdFromFidoRegistrationOptionsResponse());
		String userId = environment.getString("user_id");
		assertThat(userId, equalTo("testUserId"));
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsNoUser.json")
	public void evaluateUserIdExtractionWithoutUser() {
		ConditionError e = runAndFail(new ExtractUserIdFromFidoRegistrationOptionsResponse());
		assertThat(e.getMessage(), containsString("Could not extract data.user.id from the registration options response"));
	}

	@Test
	@UseResurce("jsonRequests/payments/enrollments/getFidoRegistrationOptionsNoUserId.json")
	public void evaluateUserIdExtractionWithoutUserId() {
		ConditionError e = runAndFail(new ExtractUserIdFromFidoRegistrationOptionsResponse());
		assertThat(e.getMessage(), containsString("Could not extract data.user.id from the registration options response"));
	}


}
