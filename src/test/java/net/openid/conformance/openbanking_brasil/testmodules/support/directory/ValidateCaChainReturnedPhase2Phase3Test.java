package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.ValidateCaChainReturnedPhase2Phase3;
import net.openid.conformance.testmodule.Environment;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import static org.mockito.Mockito.mock;


public class ValidateCaChainReturnedPhase2Phase3Test extends AbstractJsonResponseConditionUnitTest {

	class TestCondition extends ValidateCaChainReturnedPhase2Phase3 {

		@Override
		protected RestTemplate createRestTemplate(Environment env) throws UnrecoverableKeyException, KeyManagementException, CertificateException, InvalidKeySpecException, NoSuchAlgorithmException, KeyStoreException, IOException {
			return new TestRestTemplate();
		}


	}

	class TestRestTemplate extends RestTemplate {
		@Override
		public <T> ResponseEntity<T> exchange(String url, HttpMethod method, @Nullable HttpEntity<?> requestEntity, Class<T> responseType, Object... uriVariables) throws RestClientException{
			T body = (T) new String("Mock Response");
			return new ResponseEntity<>(body, HttpStatus.OK);
		}
	}


	@Before
	public void init() {
		JsonObject config = new JsonObject();
		environment.putObject("config", config);
		JsonObject resource = new JsonObject();
		resource.addProperty("consentUrl", "https://my-super-secure-bank/open-banking/payments/v4/consents");
		environment.getObject("config").add("resource", resource);
		JsonObject server = new JsonObject();
		server.addProperty("token_endpoint", "mockValue");
		server.addProperty("registration_endpoint", "mockValue2");
		environment.putObject("server", server);
	}

	public JsonObject createApiEndpoint(String apiDiscoveryId, String apiEndpoint) {
		JsonObject apiDiscoveryEndpoint = new JsonObject();
		apiDiscoveryEndpoint.addProperty("ApiDiscoveryId", apiDiscoveryId);
		apiDiscoveryEndpoint.addProperty("ApiEndpoint", apiEndpoint);
		return apiDiscoveryEndpoint;
	}
	public JsonObject createApiResource(String apiResourceId, String apiVersion, boolean familyComplete, String apiCertificationUri, String certificationStatus, String certificationStartDate, String certificationExpirationDate, String apiFamilyType, List<JsonObject> apiDiscoveryEndpoints) {
		JsonObject apiResource = new JsonObject();
		apiResource.addProperty("ApiResourceId", apiResourceId);
		apiResource.addProperty("ApiVersion", apiVersion);
		apiResource.addProperty("FamilyComplete", familyComplete);
		apiResource.addProperty("ApiCertificationUri", apiCertificationUri);
		apiResource.addProperty("CertificationStatus", certificationStatus);
		apiResource.addProperty("CertificationStartDate", certificationStartDate);
		apiResource.addProperty("CertificationExpirationDate", certificationExpirationDate);
		apiResource.addProperty("ApiFamilyType", apiFamilyType);

		JsonArray apiDiscoveryEndpointsArray = new JsonArray();
		for (JsonObject apiDiscoveryEndpoint : apiDiscoveryEndpoints) {
			apiDiscoveryEndpointsArray.add(apiDiscoveryEndpoint);
		}
		apiResource.add("ApiDiscoveryEndpoints", apiDiscoveryEndpointsArray);

		return apiResource;
	}

	protected void createAuthServerWithApiResources(JsonArray apiResources) {
		JsonObject authServer = new JsonObject();
		authServer.add("ApiResources", apiResources);
		environment.putObject("authorisation_server", authServer);
	}

	@Test
	public void test() {
		JsonObject apiResource = createApiResource("326d1950-c031-4dbb-be6a-1618128421e8", "1.0.1", true, null, "Self-Certified", null, null, "consents", List.of(createApiEndpoint("83a4c8ce-b78e-4ab4-9d81-19fd79b367b8", "https://od1.openbanking-hml.cds.com.br/open-banking/consents/v1/consents")));
		JsonArray apiResources = new JsonArray();
		apiResources.add(apiResource);
		createAuthServerWithApiResources(apiResources);
		TestCondition cond = new TestCondition();
		cond.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.SUCCESS);
		cond.evaluate(environment);
	}

}
