package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.setConfigurationFieldsForRetryTest;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.SetConfigurationFieldsForExtradayRetryUnacceptedRetryTest;

public class SetConfigurationFieldsForExtradayRetryUnacceptedRetryTestTest extends AbstractSetConfigurationFieldsForRetryTestTest {

	@Override
	protected AbstractSetConfigurationFieldsForRetryTest condition() {
		return new SetConfigurationFieldsForExtradayRetryUnacceptedRetryTest();
	}

	@Override
	protected String expectedPrefix() {
		return "extradayRetryUnaccepted";
	}
}
