package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAutomaticPaymentRecurringConsentV1EndpointTest extends AbstractGetXFromAuthServerTest {

	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/automatic-payments/v1/recurring-consents";
	}

	@Override
	protected String getApiFamilyType() {
		return "payments-recurring-consents";
	}

	@Override
	protected String getApiVersion() {
		return "1.0.0";
	}

	@Override
	protected boolean isResource() {
		return false;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetAutomaticPaymentRecurringConsentV1Endpoint();
	}
}
