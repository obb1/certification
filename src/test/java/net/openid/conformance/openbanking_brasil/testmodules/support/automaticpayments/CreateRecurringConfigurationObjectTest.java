package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateEmptySweepingRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithAllFields;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithAllLimitsSetToOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithDailyLimits;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateRecurringConfigurationObjectTest extends AbstractJsonResponseConditionUnitTest {

	private static final String AMOUNTS_FORMAT = "\\d+\\.\\d{2}";
	private static final String START_DATE_TIME_FORMAT = "^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])T(?:[01]\\d|2[0123]):(?:[012345]\\d):(?:[012345]\\d)Z$";

	@Test
	public void createSweepingRecurringConfigurationObjectWithAllFieldsHappyPath() {
		CreateSweepingRecurringConfigurationObjectWithAllFields condition = new CreateSweepingRecurringConfigurationObjectWithAllFields();
		run(condition);

		validateRecurringConfigurationObject(true, true);
	}

	@Test
	public void createSweepingRecurringConfigurationObjectWithAllLimitsSetToOneHappyPath() {
		CreateSweepingRecurringConfigurationObjectWithAllLimitsSetToOne condition = new CreateSweepingRecurringConfigurationObjectWithAllLimitsSetToOne();
		run(condition);

		validateRecurringConfigurationObject(true, true);
	}

	@Test
	public void createSweepingRecurringConfigurationObjectWithOnlyAmountHappyPath() {
		CreateSweepingRecurringConfigurationObjectWithOnlyAmount condition = new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
		run(condition);

		validateRecurringConfigurationObject(false, false);
	}

	@Test
	public void createSweepingRecurringConfigurationObjectWithOnlyAmountSettingValueInEnvironment() {
		environment.putString("recurring_total_amount", "300.00");
		CreateSweepingRecurringConfigurationObjectWithOnlyAmount condition = new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
		run(condition);

		validateRecurringConfigurationObject(false, false);
		assertEquals("300.00", environment.getString("recurring_configuration_object", "sweeping.totalAllowedAmount"));
	}

	@Test
	public void createSweepingRecurringConfigurationObjectWithDailyLimitsHappyPath() {
		CreateSweepingRecurringConfigurationObjectWithDailyLimits condition = new CreateSweepingRecurringConfigurationObjectWithDailyLimits();
		run(condition);

		validateRecurringConfigurationObject(false, true);
	}

	@Test
	public void createEmptySweepingRecurringConfigurationObjectHappyPath() {
		CreateEmptySweepingRecurringConfigurationObject condition = new CreateEmptySweepingRecurringConfigurationObject();
		run(condition);
		validateEmptySweepingConfiguration();
	}

	private void validateRecurringConfigurationObject(boolean allFields, boolean dailyLimits) {
		JsonObject recurringConfiguration = environment.getObject("recurring_configuration_object");

		assertTrue(recurringConfiguration.has("sweeping"));

		JsonObject sweeping = recurringConfiguration.getAsJsonObject("sweeping");
		assertTrue(sweeping.has("totalAllowedAmount"));
		assertTrue(OIDFJSON.getString(sweeping.get("totalAllowedAmount")).matches(AMOUNTS_FORMAT));
		assertTrue(sweeping.has("startDateTime"));
		assertTrue(OIDFJSON.getString(sweeping.get("startDateTime")).matches(START_DATE_TIME_FORMAT));

		if (dailyLimits) {
			assertTrue(sweeping.has("transactionLimit"));
			assertTrue(OIDFJSON.getString(sweeping.get("transactionLimit")).matches(AMOUNTS_FORMAT));

			assertTrue(sweeping.has("periodicLimits"));
			JsonObject periodicLimits = sweeping.getAsJsonObject("periodicLimits");
			assertTrue(periodicLimits.has("day"));
			validatePeriodicLimitsField(periodicLimits, "day");
		}

		if (allFields) {
			JsonObject periodicLimits = sweeping.getAsJsonObject("periodicLimits");
			assertTrue(periodicLimits.has("week"));
			validatePeriodicLimitsField(periodicLimits, "week");
			assertTrue(periodicLimits.has("month"));
			validatePeriodicLimitsField(periodicLimits, "month");
			assertTrue(periodicLimits.has("year"));
			validatePeriodicLimitsField(periodicLimits, "year");
		}
	}

	private void validatePeriodicLimitsField(JsonObject periodicLimits, String fieldName) {
		JsonObject fieldObj = periodicLimits.getAsJsonObject(fieldName);

		assertTrue(fieldObj.has("quantityLimit"));
		assertTrue(fieldObj.get("quantityLimit").getAsJsonPrimitive().isNumber());

		assertTrue(fieldObj.has("transactionLimit"));
		assertTrue(OIDFJSON.getString(fieldObj.get("transactionLimit")).matches(AMOUNTS_FORMAT));
	}

	private void validateEmptySweepingConfiguration() {
		JsonObject recurringConfiguration = environment.getObject("recurring_configuration_object");
		JsonObject sweeping = recurringConfiguration.getAsJsonObject("sweeping");

		assertNotNull(sweeping);
		assertTrue(sweeping.keySet().isEmpty());
	}
}
