package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureExpirationDateTimeFieldIsUnaltered;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureExpirationDateTimeFieldIsUnalteredTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String ORIGINAL_DATE_TIME = "2023-05-21T08:30:00Z";
	protected static final String ALTERED_DATE_TIME = "2024-06-30T11:25:34Z";
	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/consents/GetRecurringConsents";

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void happyPathTest() {
		prepareForTest(true);
		run(new EnsureExpirationDateTimeFieldIsUnaltered());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		prepareForTest(true);
		environment.putObject(EnsureExpirationDateTimeFieldIsUnaltered.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		prepareForTest(true);
		environment.putObject(EnsureExpirationDateTimeFieldIsUnaltered.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "SweepingMissingExpirationDateTime200Response.json")
	public void unhappyPathTestMissingExpirationDateTime() {
		prepareForTest(true);
		unhappyPathTest("Unable to find expirationDateTime field in the response data");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "Sweeping200Response.json")
	public void unhappyPathTestExpirationDateTimeAltered() {
		prepareForTest(false);
		unhappyPathTest("The expirationDateTime field has been altered");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureExpirationDateTimeFieldIsUnaltered());
		assertTrue(error.getMessage().contains(message));
	}

	protected void prepareForTest(boolean isOriginalExpiration) {
		String expirationDateTime = isOriginalExpiration ? ORIGINAL_DATE_TIME : ALTERED_DATE_TIME;
		environment.putString("expiration_date_time", expirationDateTime);
	}
}
