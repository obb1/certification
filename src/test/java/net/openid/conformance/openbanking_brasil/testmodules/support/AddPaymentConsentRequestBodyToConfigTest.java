package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

public class AddPaymentConsentRequestBodyToConfigTest {

	@Test
	public void constructConsentFromMinimalConfig() throws IOException, JSONException {

		JsonObject config = new JsonObjectBuilder()
			.addField("resource.loggedUserIdentification", "76109277673")
			.addField("resource.debtorAccountIspb", "12345678")
			.addField("resource.debtorAccountIssuer", "6272")
			.addField("resource.debtorAccountNumber", "94088392")
			.addField("resource.debtorAccountType", "CACC")
			.addField("resource.paymentAmount", "100.00")
			.build();



		Environment environment = new Environment();
		environment.putObject("config", config);

		AddPaymentConsentRequestBodyToConfig condition = new AddPaymentConsentRequestBodyToConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		condition.evaluate(environment);

		JsonObject consent = environment.getElementFromObject("config", "resource.brazilPaymentConsent").getAsJsonObject();

		String expectedJson = IOUtils.resourceToString("payment_consent_request.json", Charset.defaultCharset(), getClass().getClassLoader());

		JSONAssert.assertEquals(expectedJson, consent.toString(), new CustomComparator(
			JSONCompareMode.LENIENT,
			new Customization("data.payment.date", (o1, o2) -> true)
		));

	}

	@Test
	public void constructConsentFromConfigWithBusinessEntity() throws IOException, JSONException {

		JsonObject config = new JsonObjectBuilder()
			.addField("resource.loggedUserIdentification", "76109277673")
			.addField("resource.debtorAccountIspb", "12345678")
			.addField("resource.debtorAccountIssuer", "6272")
			.addField("resource.debtorAccountNumber", "94088392")
			.addField("resource.debtorAccountType", "CACC")
			.addField("resource.paymentAmount", "100.00")
			.addField("resource.businessEntityIdentification", "123456789")
			.build();



		Environment environment = new Environment();
		environment.putObject("config", config);

		AddPaymentConsentRequestBodyToConfig condition = new AddPaymentConsentRequestBodyToConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		condition.evaluate(environment);

		JsonObject consent = environment.getElementFromObject("config", "resource.brazilPaymentConsent").getAsJsonObject();

		String expectedJson = IOUtils.resourceToString("payment_consent_request_with_business_entity.json", Charset.defaultCharset(), getClass().getClassLoader());

		JSONAssert.assertEquals(expectedJson, consent.toString(), new CustomComparator(
			JSONCompareMode.LENIENT,
			new Customization("data.payment.date", (o1, o2) -> true)
		));

	}


	@Test
	public void failsIfMandatoryConfigMissing() throws IOException, JSONException {

		JsonObject config = new JsonObjectBuilder()
			.addField("resource.debtorAccountIspb", "12345678")
			.addField("resource.debtorAccountIssuer", "6272")
			.addField("resource.debtorAccountNumber", "94088392")
			.addField("resource.debtorAccountType", "CACC")
			.addField("resource.paymentAmount", "100.00")
			.build();

		Environment environment = new Environment();
		environment.putObject("config", config);

		AddPaymentConsentRequestBodyToConfig condition = new AddPaymentConsentRequestBodyToConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);

		try {
			condition.evaluate(environment);
			fail("Should not pass");
		} catch (ConditionError ce) {
			assertEquals("AddPaymentConsentRequestBodyToConfig: Unable to find element resource.loggedUserIdentification in config", ce.getMessage());
		}

	}

	@Test
	public void constructConsentFromExistingConsent() throws IOException, JSONException {

		JsonObject existingConsentJson = new JsonObject();
		JsonObject data = new JsonObject();

		JsonObject loggedUser = new JsonObject();
		JsonObject document = new JsonObject();
		document.addProperty("identification", "76109277673");
		document.addProperty("rel", "CPF");
		loggedUser.add("document", document);
		data.add("loggedUser", loggedUser);

		JsonObject creditor = new JsonObject();
		creditor.addProperty("personType", "PESSOA_NATURAL");
		creditor.addProperty("cpfCnpj", "99991111140");
		creditor.addProperty("name", "Joao Silva");
		data.add("creditor", creditor);

		JsonObject payment = new JsonObject();
		payment.addProperty("type", "PIX");
		payment.addProperty("currency", "BRL");
		payment.addProperty("amount", "100.00");

		JsonObject details = new JsonObject();
		details.addProperty("proxy", "cliente-a00001@pix.bcb.gov.br");
		details.addProperty("localInstrument", "DICT");

		JsonObject creditorAccount = new JsonObject();
		creditorAccount.addProperty("ispb", "99999004");
		creditorAccount.addProperty("issuer", "0001");
		creditorAccount.addProperty("number", "12345678");
		creditorAccount.addProperty("accountType", "CACC");
		details.add("creditorAccount", creditorAccount);
		payment.add("details", details);
		data.add("payment", payment);

		JsonObject debtorAccount = new JsonObject();
		debtorAccount.addProperty("ispb", "12345678");
		debtorAccount.addProperty("issuer", "6272");
		debtorAccount.addProperty("number", "94088392");
		debtorAccount.addProperty("accountType", "CACC");
		data.add("debtorAccount", debtorAccount);

		existingConsentJson.add("data", data);

		JsonObject config = new JsonObjectBuilder()
			.addField("resource.brazilPaymentConsentFvp", existingConsentJson)
			.build();

		Environment environment = new Environment();
		environment.putObject("config", config);

		AddPaymentConsentRequestBodyToConfig condition = new AddPaymentConsentRequestBodyToConfig();
		condition.setProperties("test", mock(TestInstanceEventLog.class), Condition.ConditionResult.FAILURE);


		condition.evaluate(environment);
		JsonObject generatedConsentJson = environment.getElementFromObject("config", "resource.brazilPaymentConsent").getAsJsonObject();

		JsonObject expectedConsentJson = new JsonObject();
		JsonObject expectedData = new JsonObject();

		JsonObject expectedLoggedUser = new JsonObject();
		JsonObject expectedDocument = new JsonObject();
		expectedDocument.addProperty("identification", "76109277673");
		expectedDocument.addProperty("rel", "CPF");
		expectedLoggedUser.add("document", expectedDocument);
		expectedData.add("loggedUser", expectedLoggedUser);

		JsonObject expectedCreditor = new JsonObject();
		expectedCreditor.addProperty("personType", "PESSOA_NATURAL");
		expectedCreditor.addProperty("cpfCnpj", "99991111140");
		expectedCreditor.addProperty("name", "Joao Silva");
		expectedData.add("creditor", expectedCreditor);

		JsonObject expectedPayment = new JsonObject();
		expectedPayment.addProperty("type", "PIX");
		expectedPayment.addProperty("currency", "BRL");
		expectedPayment.addProperty("amount", "100.00");

		JsonObject expectedDetails = new JsonObject();
		expectedDetails.addProperty("proxy", "cliente-a00001@pix.bcb.gov.br");
		expectedDetails.addProperty("localInstrument", "DICT");

		JsonObject expectedCreditorAccount = new JsonObject();
		expectedCreditorAccount.addProperty("ispb", "99999004");
		expectedCreditorAccount.addProperty("issuer", "0001");
		expectedCreditorAccount.addProperty("number", "12345678");
		expectedCreditorAccount.addProperty("accountType", "CACC");
		expectedDetails.add("creditorAccount", expectedCreditorAccount);
		expectedPayment.add("details", expectedDetails);
		expectedData.add("payment", expectedPayment);

		JsonObject expectedDebtorAccount = new JsonObject();
		expectedDebtorAccount.addProperty("ispb", "12345678");
		expectedDebtorAccount.addProperty("issuer", "6272");
		expectedDebtorAccount.addProperty("number", "94088392");
		expectedDebtorAccount.addProperty("accountType", "CACC");
		expectedData.add("debtorAccount", expectedDebtorAccount);

		expectedConsentJson.add("data", expectedData);

		JSONAssert.assertEquals(expectedConsentJson.toString(), generatedConsentJson.toString(), new CustomComparator(
			JSONCompareMode.LENIENT,
			new Customization("data.payment.date", (o1, o2) -> true)
		));
	}
}

