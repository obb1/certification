package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetCreditFixedIncomesTransactionsCurrentV1n3OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactions.json")
	public void happyPath() {
		run(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());

	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactionsNoFinancialTransactionTax.json")
	public void unhappyPathNoFinancialTransactionTax() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());
		assertThat(e.getMessage(), containsString("financialTransactionTax is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactionsNoIncomeTax.json")
	public void unhappyPathNoIncomeTax() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());
		assertThat(e.getMessage(), containsString("incomeTax is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactionsNoIndexerPercentage.json")
	public void unhappyPathNoIndexerPercentage() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());
		assertThat(e.getMessage(), containsString("indexerPercentage is required when type is ENTRADA"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactionsNoTransactionQuantity.json")
	public void unhappyPathNoTransactionQuantity() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());
		assertThat(e.getMessage(), containsString("transactionQuantity is required when transactionType is [COMPRA, VENDA, VENCIMENTO, TRANSFERENCIA_TITULARIDADE, TRANSFERENCIA_CUSTODIA]"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactionsNoTransactionRate.json")
	public void unhappyPathNoTransactionRate() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());
		assertThat(e.getMessage(), containsString("remunerationTransactionRate is required when type is ENTRADA"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactionsNoTransactionTypeAdditionalInfo.json")
	public void unhappyPathNoTransactionTypeAdditionalInfo() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());
		assertThat(e.getMessage(), containsString("transactionTypeAdditionalInfo is required when transactionType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/creditFixedIncomes/v2.1.0/transactionsNoTransactionUnitPrice.json")
	public void unhappyPathNoUnitPrice() {
		ConditionError e = runAndFail(new GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator());
		assertThat(e.getMessage(), containsString("transactionUnitPrice is required when transactionType is [COMPRA, VENDA, VENCIMENTO]"));
	}
}
