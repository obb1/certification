package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Supplier;


import static org.junit.Assert.assertTrue;


public class CheckLocalTimeTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void testLocalTimeNotBetween1130And1159() {
		ZonedDateTime mockNow = ZonedDateTime.of(LocalDate.now(), LocalTime.of(11, 9,0), ZoneId.of("America/Sao_Paulo"));
		Supplier<ZonedDateTime> mockNowSupplier = () -> mockNow;
		CheckLocalTime condition = new CheckLocalTime(mockNowSupplier);
		ConditionError error = runAndFail(condition);
		String expectedMessage = "localTime is not between 11:30 PM and 11:59 PM";

		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void testLocalTimeBetween1130And1159() {
		ZonedDateTime mockNow = ZonedDateTime.of(LocalDate.now(), LocalTime.of(23, 45,0), ZoneId.of("America/Sao_Paulo"));
		Supplier<ZonedDateTime> mockNowSupplier = () -> mockNow;
		CheckLocalTime condition = new CheckLocalTime(mockNowSupplier);
		run(condition);
	}

	@Test
	public void testLocalTimeEqualsMidnight() {
		ZonedDateTime mockNow = ZonedDateTime.of(LocalDate.now(), LocalTime.of(0, 0,0), ZoneId.of("America/Sao_Paulo"));
		Supplier<ZonedDateTime> mockNowSupplier = () -> mockNow;
		CheckLocalTime condition = new CheckLocalTime(mockNowSupplier);
		ConditionError error = runAndFail(condition);
		String expectedMessage = "localTime is not between 11:30 PM and 11:59 PM";

		assertTrue(error.getMessage().contains(expectedMessage));
	}

	@Test
	public void testLocalTimeEquals1130() {
		ZonedDateTime mockNow = ZonedDateTime.of(LocalDate.now(), LocalTime.of(23, 30,0), ZoneId.of("America/Sao_Paulo"));
		Supplier<ZonedDateTime> mockNowSupplier = () -> mockNow;
		CheckLocalTime condition = new CheckLocalTime(mockNowSupplier);
		run(condition);
	}

	@Test
	public void testLocalTimeEquals1159() {
		ZonedDateTime mockNow = ZonedDateTime.of(LocalDate.now(), LocalTime.of(23, 59,0), ZoneId.of("America/Sao_Paulo"));
		Supplier<ZonedDateTime> mockNowSupplier = () -> mockNow;
		CheckLocalTime condition = new CheckLocalTime(mockNowSupplier);
		run(condition);
	}

	@Test
	public void testDefaultConstructor() {
		CheckLocalTime condition = new CheckLocalTime();
		ConditionError error = runAndFail(condition);
		String expectedMessage = "localTime is not between 11:30 PM and 11:59 PM";

		assertTrue(error.getMessage().contains(expectedMessage));
	}

}
