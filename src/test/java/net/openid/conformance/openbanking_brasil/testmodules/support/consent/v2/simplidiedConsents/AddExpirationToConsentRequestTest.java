package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplidiedConsents;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.addExpirationToConsentRequest.AddExpirationPlus30DaysToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.addExpirationToConsentRequest.AddExpirationPlus5MinutesToConsentRequest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AddExpirationToConsentRequestTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value = "jsonRequests/consents/v3/consentRequestNoExpirationDateTime.json", key = "consent_endpoint_request")
	public void happyPathTestPlus30Days() {
		run(new AddExpirationPlus30DaysToConsentRequest());
		String expirationDateTimeStr = OIDFJSON.getString(
			environment.getObject("consent_endpoint_request")
				.getAsJsonObject("data")
				.get("expirationDateTime")
		);
		LocalDateTime expirationDateTime = LocalDateTime.parse(expirationDateTimeStr, DateTimeFormatter.ISO_DATE_TIME);
		LocalDate expirationDate = expirationDateTime.toLocalDate();

		LocalDate currentDate = LocalDate.now(ZoneOffset.UTC);
		LocalDate futureDate = currentDate.plusDays(30);

		assertEquals(expirationDate, futureDate);
	}

	@Test
	@UseResurce(value = "jsonRequests/consents/v3/consentRequestNoExpirationDateTime.json", key = "consent_endpoint_request")
	public void happyPathTestPlus5Minutes() {
		run(new AddExpirationPlus5MinutesToConsentRequest());
		String expirationDateTimeStr = OIDFJSON.getString(
			environment.getObject("consent_endpoint_request")
				.getAsJsonObject("data")
				.get("expirationDateTime")
		);
		LocalDateTime expirationDateTime = LocalDateTime.parse(expirationDateTimeStr, DateTimeFormatter.ISO_DATE_TIME);

		LocalDateTime currentDateTime = LocalDateTime.now(ZoneOffset.UTC);
		LocalDateTime futureDateTime = currentDateTime.plusMinutes(5);
		Duration tolerance = Duration.ofSeconds(30);

		assertTrue(!expirationDateTime.isAfter(futureDateTime) && expirationDateTime.isAfter(futureDateTime.minus(tolerance)));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingConsentRequest() {
		run(new AddExpirationPlus5MinutesToConsentRequest());
	}
}
