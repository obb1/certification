package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment.AbstractAddCnpjToPaymentRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment.AddFirstCnpjToPaymentRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment.AddSecondCnpjToPaymentRequestBody;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddCnpjToPaymentRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	private static final String RESOURCE_PATH_HAPPY = "jsonRequests/automaticPayments/payments/brazilPaymentWithCnpj.json";
	private static final String RESOURCE_PATH_UNHAPPY = "jsonRequests/automaticPayments/payments/brazilPaymentMissingDocument.json";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";
	private static final String FIRST_CNPJ = "123";
	private static final String SECOND_CNPJ = "456";

	@Before
	public void init() {
		environment.putObject("first_creditor",
		 	new JsonObjectBuilder()
				.addField("personType", "PESSOA_JURIDICA")
				.addField("cpfCnpj", FIRST_CNPJ)
				.addField("name", "example")
				.build()
		);
		environment.putObject("second_creditor",
			new JsonObjectBuilder()
				.addField("personType", "PESSOA_JURIDICA")
				.addField("cpfCnpj", SECOND_CNPJ)
				.addField("name", "example")
				.build()
		);
	}

	@Test
	@UseResurce(value = RESOURCE_PATH_HAPPY, key = KEY, path = PATH)
	public void happyPathTestFirstCreditor() {
		happyPathTest(new AddFirstCnpjToPaymentRequestBody(), FIRST_CNPJ);
	}

	@Test
	@UseResurce(value = RESOURCE_PATH_HAPPY, key = KEY, path = PATH)
	public void happyPathTestSecondCreditor() {
		happyPathTest(new AddSecondCnpjToPaymentRequestBody(), SECOND_CNPJ);
	}

	@Test
	public void unhappyPathTestMissingPayment() {
		environment.putObject("resource", new JsonObject());
		unhappyPathTest("Unable to find data.document in payments payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject("resource", "brazilPixPayment", new JsonObject());
		unhappyPathTest("Unable to find data.document in payments payload");
	}

	@Test
	@UseResurce(value = RESOURCE_PATH_UNHAPPY, key = KEY, path = PATH)
	public void unhappyPathTestMissingDocument() {
		unhappyPathTest("Unable to find data.document in payments payload");
	}

	@Test
	@UseResurce(value = RESOURCE_PATH_HAPPY, key = KEY, path = PATH)
	public void unhappyPathTestMissingCreditor() {
		environment.removeObject("first_creditor");
		unhappyPathTest("Unable to find creditor in the environment");
	}

	@Test
	@UseResurce(value = RESOURCE_PATH_HAPPY, key = KEY, path = PATH)
	public void unhappyPathTestMissingIdentification() {
		environment.getObject("first_creditor").remove("cpfCnpj");
		unhappyPathTest("Unable to find 'cpfCnpj' field inside the creditor");
	}

	protected void happyPathTest(AbstractAddCnpjToPaymentRequestBody condition, String expectedIdentification) {
		run(condition);
		String identification = OIDFJSON.getString(
			environment.getElementFromObject("resource", "brazilPixPayment.data.document.identification"));
		assertEquals(expectedIdentification, identification);
	}

	protected void unhappyPathTest(String expectedErrorMessage) {
		AddFirstCnpjToPaymentRequestBody condition = new AddFirstCnpjToPaymentRequestBody();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(expectedErrorMessage));
	}
}
