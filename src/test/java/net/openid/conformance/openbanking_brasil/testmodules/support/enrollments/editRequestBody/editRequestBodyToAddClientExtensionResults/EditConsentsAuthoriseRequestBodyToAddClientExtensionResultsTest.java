package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EditConsentsAuthoriseRequestBodyToAddClientExtensionResultsTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String RESOURCE_VALUE = "jsonRequests/payments/enrollments/postConsentsAuthorise.json";
	protected static final String RESOURCE_KEY = "resource_request_entity_claims";

	@Test
	@UseResurce(value = RESOURCE_VALUE, key = RESOURCE_KEY)
	public void happyPathTestNullClientExtensionResults() {
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = RESOURCE_VALUE, key = RESOURCE_KEY)
	public void happyPathTestEmptyClientExtensionResults() {
		environment.putObject("client_extension_results", new JsonObject());
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = RESOURCE_VALUE, key = RESOURCE_KEY)
	public void happyPathTestPopulatedClientExtensionResults() {
		addSimpleClientExtensionResultsToEnv();
		happyPathTest(true);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingRequest() {
		run(new EditConsentsAuthoriseRequestBodyToAddClientExtensionResults());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(RESOURCE_KEY, new JsonObject());
		addSimpleClientExtensionResultsToEnv();
		unhappyPathTest("Could not find data.fidoAssertion inside the request body");
	}

	@Test
	public void unhappyPathTestMissingFidoAssertion() {
		environment.putObject(RESOURCE_KEY, new JsonObjectBuilder().addField("data", new JsonObject()).build());
		addSimpleClientExtensionResultsToEnv();
		unhappyPathTest("Could not find data.fidoAssertion inside the request body");
	}

	protected void happyPathTest(boolean hasClientExtensionResults) {
		run(new EditConsentsAuthoriseRequestBodyToAddClientExtensionResults());
		JsonObject fidoAssertion = environment.getObject(RESOURCE_KEY).getAsJsonObject("data")
			.getAsJsonObject("fidoAssertion");
		if (hasClientExtensionResults) {
			assertTrue(fidoAssertion.has("clientExtensionResults"));
		} else {
			assertFalse(fidoAssertion.has("clientExtensionResults"));
		}
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditConsentsAuthoriseRequestBodyToAddClientExtensionResults());
		assertTrue(error.getMessage().contains(message));
	}

	protected void addSimpleClientExtensionResultsToEnv() {
		environment.putObject("client_extension_results",
			new JsonObjectBuilder().addField("appId", true).build()
		);
	}
}
