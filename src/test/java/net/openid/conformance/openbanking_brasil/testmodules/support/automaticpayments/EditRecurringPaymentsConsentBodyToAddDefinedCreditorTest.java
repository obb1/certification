package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToAddDefinedCreditor;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EditRecurringPaymentsConsentBodyToAddDefinedCreditorTest extends AbstractJsonResponseConditionUnitTest {

	private static final String NAME = "Joao Silva";
	private static final String CPF = "12345678901";
	private static final String CNPJ = "12345678901234";
	private static final String INVALID_CPF_CNPJ = "123456789012";
	private static final String JSON_PATH = "jsonRequests/automaticPayments/consents/brazilPaymentConsentWithSweepingField.json";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void happyPathTestCpf() {
		happyPathTest(true);
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void happyPathTestCnpj() {
		happyPathTest(false);
	}

	@Test
	public void unhappyPathTestMissingData() {
		prepareResourceForTest(true);
		JsonObject resource = environment.getObject("resource");
		resource.add("brazilPaymentConsent", new JsonObject());
		unhappyPathTest("Could not find data inside payment consent request body");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void unhappyPathTestInvalidCpfCnpj() {
		prepareResourceForTest(true);
		JsonObject resource = environment.getObject("resource");
		resource.addProperty("creditorCpfCnpj", INVALID_CPF_CNPJ);
		unhappyPathTest("The value in creditorCpfCnpj should be either a CPF or a CNPJ");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void unhappyPathTestMissingCpfCnpj() {
		prepareResourceForTest(true);
		JsonObject resource = environment.getObject("resource");
		resource.remove("creditorCpfCnpj");
		unhappyPathTest("Could not find creditorCpfCnpj in the resource");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void unhappyPathTestMissingName() {
		prepareResourceForTest(true);
		JsonObject resource = environment.getObject("resource");
		resource.remove("creditorName");
		unhappyPathTest("Could not find creditorName in the resource");
	}


	protected void happyPathTest(boolean isCpf) {
		prepareResourceForTest(isCpf);
		run(new EditRecurringPaymentsConsentBodyToAddDefinedCreditor());
		JsonArray creditors = environment.getElementFromObject("resource", "brazilPaymentConsent.data.creditors")
			.getAsJsonArray();
		JsonObject creditor = creditors.get(0).getAsJsonObject();

		String expectedCpfCnpj = isCpf ? CPF : CNPJ;
		String expectedPersonType = isCpf ? "PESSOA_NATURAL" : "PESSOA_JURIDICA";
		assertEquals(NAME, OIDFJSON.getString(creditor.get("name")));
		assertEquals(expectedCpfCnpj, OIDFJSON.getString(creditor.get("cpfCnpj")));
		assertEquals(expectedPersonType, OIDFJSON.getString(creditor.get("personType")));
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentsConsentBodyToAddDefinedCreditor());
		assertTrue(error.getMessage().contains(message));
	}

	protected void prepareResourceForTest(boolean isCpf) {
		String cpfCnpj = isCpf ? CPF : CNPJ;
		JsonObject resource = new JsonObjectBuilder()
			.addField("creditorName", NAME)
			.addField("creditorCpfCnpj", cpfCnpj)
			.build();
		environment.putObject("resource", resource);
	}
}
