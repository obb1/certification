package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetUserDefinedExpirationDateTime;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditRecurringPaymentsConsentBodyToSetUserDefinedExpirationDateTimeTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_PATH = "jsonRequests/automaticPayments/consents/v2/brazilPaymentConsentWithAutomaticFieldV2.json";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";
	private static final String ENV_EXPIRATION_DATE_TIME = "2024-10-03T10:40:35Z";

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void happyPathTest() {
		environment.putString(KEY, "expirationDateTime", ENV_EXPIRATION_DATE_TIME);

		run(new EditRecurringPaymentsConsentBodyToSetUserDefinedExpirationDateTime());

		String expirationDateTime = OIDFJSON.getString(environment
			.getElementFromObject(KEY, PATH + ".data.expirationDateTime"));

		assertEquals(ENV_EXPIRATION_DATE_TIME, expirationDateTime);
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putString(KEY, "expirationDateTime", ENV_EXPIRATION_DATE_TIME);
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest("Unable to find data field in consents payload");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putString(KEY, "expirationDateTime", ENV_EXPIRATION_DATE_TIME);
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest("Unable to find data field in consents payload");
	}

	@Test
	@UseResurce(value = JSON_PATH, key = KEY, path = PATH)
	public void unhappyPathTestInvalidExpirationDateTime() {
		environment.putString(KEY, "expirationDateTime", "2024-10-03T10:35:45.00Z");
		unhappyPathTest("The value defined for resource.expirationDateTime is not valid");

		environment.putString(KEY, "expirationDateTime", "2024-09-32T10:35:45Z");
		unhappyPathTest("The value defined for resource.expirationDateTime is not valid");

		environment.putString(KEY, "expirationDateTime", "2024-13-03T10:35:45Z");
		unhappyPathTest("The value defined for resource.expirationDateTime is not valid");

		environment.putString(KEY, "expirationDateTime", "2024-10-03");
		unhappyPathTest("The value defined for resource.expirationDateTime is not valid");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EditRecurringPaymentsConsentBodyToSetUserDefinedExpirationDateTime());
		assertTrue(error.getMessage().contains(message));
	}
}
