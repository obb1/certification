package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.loans.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetPersonalLoansOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/loans/v1/GetPersonalLoans200Response.json")
	public void happyPath() {
		run(new GetPersonalLoansOASValidatorV1());
	}
}
