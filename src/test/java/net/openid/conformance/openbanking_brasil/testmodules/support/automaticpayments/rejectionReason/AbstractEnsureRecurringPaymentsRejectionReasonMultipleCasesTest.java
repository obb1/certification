package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.rejectionReason;

import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public abstract class AbstractEnsureRecurringPaymentsRejectionReasonMultipleCasesTest extends AbstractEnsureRecurringPaymentsRejectionReasonTest {

	@Test
	@UseResurce(BASE_JSON_PATH + "InvalidRejectionReason.json")
	public void unhappyPathTestInvalidRejectionReasonCode() {
		assertThrowsExactly(IllegalArgumentException.class, () -> {
			run(condition());
		});
	}
}
