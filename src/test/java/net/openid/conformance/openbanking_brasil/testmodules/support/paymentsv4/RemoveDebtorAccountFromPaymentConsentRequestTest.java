package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RemoveDebtorAccountFromPaymentConsentRequestTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_PATH_BASE = "jsonRequests/payments/consents/v4/postPaymentConsentRequest";
	private static final String KEY = "resource";
	private static final String PATH = "brazilPaymentConsent";

	@Test
	@UseResurce(value = JSON_PATH_BASE + "V4.json", key = KEY, path = PATH)
	public void happyPathTestWithDebtorAccount() {
		happyPathTest();
	}

	@Test
	@UseResurce(value = JSON_PATH_BASE + "WithoutDebtorAccountV4.json", key = KEY, path = PATH)
	public void happyPathTestWithoutDebtorAccount() {
		happyPathTest();
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResource() {
		run(new RemoveDebtorAccountFromPaymentConsentRequest());
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, PATH, new JsonObject());
		unhappyPathTest();
	}

	private void happyPathTest() {
		run(new RemoveDebtorAccountFromPaymentConsentRequest());
		JsonObject data = environment.getElementFromObject(KEY, PATH + ".data").getAsJsonObject();
		assertFalse(data.has("debtorAccount"));
	}

	private void unhappyPathTest() {
		ConditionError error = runAndFail(new RemoveDebtorAccountFromPaymentConsentRequest());
		assertTrue(error.getMessage().contains("Could not find payment consent data in the environment"));
	}
}
