package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedFrom.EnsureRecurringPaymentsConsentsRejectedFromIniciadora;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureRecurringPaymentsConsentsRejectedFromTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_FILE_PATH = "jsonResponses/automaticpayments/PostRecurringConsents";
	private static final String MISSING_FIELD_MESSAGE = "Unable to find element data.rejection.rejectedFrom in the response payload";

	@Test
	@UseResurce(BASE_FILE_PATH + "RejectedFromIniciadoraRejectedByUsuario.json")
	public void happyPathTest() {
		EnsureRecurringPaymentsConsentsRejectedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRejectedFromIniciadora();
		run(condition);
	}

	@Test
	public void unhappyPathMissingResponse() {
		EnsureRecurringPaymentsConsentsRejectedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRejectedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	public void unhappyPathMissingData() {
		environment.putObject(EnsureRecurringPaymentsConsentsRejectedFromIniciadora.RESPONSE_ENV_KEY, new JsonObject());
		EnsureRecurringPaymentsConsentsRejectedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRejectedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRejectionField.json")
	public void unhappyPathMissingRejection() {
		EnsureRecurringPaymentsConsentsRejectedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRejectedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "MissingRejectedFromField.json")
	public void unhappyPathMissingRejectedFrom() {
		EnsureRecurringPaymentsConsentsRejectedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRejectedFromIniciadora();
		ConditionError error = runAndFail(condition);

		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
	}

	@Test
	@UseResurce(BASE_FILE_PATH + "RejectedFromDetentoraRejectedByDetentora.json")
	public void unhappyPathRejectedFromNotMatchingExpected() {
		EnsureRecurringPaymentsConsentsRejectedFromIniciadora condition = new EnsureRecurringPaymentsConsentsRejectedFromIniciadora();
		ConditionError error = runAndFail(condition);

		String expectedMessage = "rejectedFrom returned in the response does not match the expected rejectedFrom";
		assertTrue(error.getMessage().contains(expectedMessage));
	}
}
