package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.personalInsurance.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetPersonalInsuranceOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/insurance/PersonalInsuranceListResponseV2.json")
	public void testHappyPath() {
		run(new GetPersonalInsuranceOASValidatorV2());
	}

	@Test
	@UseResurce("jsonResponses/opendata/insurance/PersonalInsuranceListResponseV2InvalidPaymentMethodAdittionalInfo.json")
	public void testInvalidPaymentMethodAdditionalInfo() {
		ConditionError error = runAndFail(new GetPersonalInsuranceOASValidatorV2());
		assertThat(error.getMessage(), containsString("data.society.products.premiumPayment.paymentMethodAdittionalInfo is required when data.society.products.premiumPayment.paymentMethods contains OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/opendata/insurance/PersonalInsuranceListResponseV2InvalidTypeAdditionalInfos.json")
	public void testInvalidTypeAdditionalInfos() {
		ConditionError error = runAndFail(new GetPersonalInsuranceOASValidatorV2());
		assertThat(error.getMessage(), containsString("typeAdditionalInfos is required when type is OUTROS"));
	}

}
