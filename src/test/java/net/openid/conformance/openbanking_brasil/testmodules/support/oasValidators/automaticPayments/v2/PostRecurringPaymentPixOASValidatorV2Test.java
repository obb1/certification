package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

public class PostRecurringPaymentPixOASValidatorV2Test extends AbstractRecurringPaymentPixOASValidatorV2Test {

	@Override
	protected int status() {
		return 201;
	}

	@Override
	protected AbstractRecurringPaymentPixOASValidatorV2 validator() {
		return new PostRecurringPaymentPixOASValidatorV2();
	}
}
