package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.util.JsonUtils;
import net.openid.conformance.util.UseResurce;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

public class AbstractCheckExpectedDateResponseTest extends AbstractJsonResponseConditionUnitTest {

	private CopyResourceEndpointResponse copyResourceEndpointResponse = new CopyResourceEndpointResponse();

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseOK.json")
	public void checkAccountsTransactionV1Response() {
		CheckExpectedBookingDateResponse condition = new CheckExpectedBookingDateResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseOK.json");
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionsV2/accountTransactionsResponseOK.json")
	public void checkAccountsTransactionV2Response() {
		CheckExpectedBookingDateResponse condition = new CheckExpectedBookingDateResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/accountV2/transactionsV2/accountTransactionsFullRangeResponseOK.json");
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseOK.json")
	public void checkTransactionDateAtBookingDateResponse() {
		Gson gson = JsonUtils.createBigDecimalAwareGson();
		CheckExpectedBookingDateResponse condition = new CheckExpectedBookingDateResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseOK.json");
		run(condition);

		JsonObject actualResponse = gson.fromJson(environment.getString("resource_endpoint_response_full", "body"), JsonObject.class);
		JsonArray actualData = actualResponse.getAsJsonArray("data");

		assertFalse(actualData.isEmpty(), "Should have data in the response");

		for (JsonElement element : actualData) {
			JsonObject resourceObject = element.getAsJsonObject();
			JsonElement resourceDate = resourceObject.get("transactionDate");
			assertNotNull(resourceDate, "Should have transactionDate");
		}
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithDateTime.json")
	public void checkTransactionDateTimeAtBookingDateTimeResponse() {
		Gson gson = JsonUtils.createBigDecimalAwareGson();
		CheckExpectedBookingDateTimeResponse condition = new CheckExpectedBookingDateTimeResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithDateTime.json");
		run(condition);

		JsonObject actualResponse = gson.fromJson(environment.getString("resource_endpoint_response_full", "body"), JsonObject.class);
		JsonArray actualData = actualResponse.getAsJsonArray("data");

		assertFalse(actualData.isEmpty(), "Should have data in the response");

		for (JsonElement element : actualData) {
			JsonObject resourceObject = element.getAsJsonObject();
			JsonElement resourceDate = resourceObject.get("transactionDateTime");
			assertNotNull(resourceDate, "Should have transactionDateTime");
		}
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionResponseWithoutTransactionDate.json")
	public void testMissingDateFieldAtBookingDateTimeResponse() {
		CheckExpectedBookingDateTimeResponse condition = new CheckExpectedBookingDateTimeResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithoutTransactionDate.json");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedBookingDateTimeResponse: Could not find transactionDateTime JSON element in the resource Object", error.getMessage());
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionResponseWithoutTransactionDate.json")
	public void testMissingDateFieldAtBookingDateResponse() {
		CheckExpectedBookingDateResponse condition = new CheckExpectedBookingDateResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithoutTransactionDate.json");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedBookingDateResponse: Could not find transactionDate JSON element in the resource Object", error.getMessage());
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithWrongTransactionDateField.json")
	public void testWrongDateFormatAtBookingDateTimeResponse() {
		CheckExpectedBookingDateTimeResponse condition = new CheckExpectedBookingDateTimeResponse();
		environment.putString("fromBookingDate", "2021-01-01");
		environment.putString("toBookingDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithWrongTransactionDateField.json");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedBookingDateTimeResponse: Error parsing date", error.getMessage());
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseOK.json")
	public void checkTransactionDateAtTransactionDateResponse() {
		Gson gson = JsonUtils.createBigDecimalAwareGson();
		CheckExpectedTransactionDateResponse condition = new CheckExpectedTransactionDateResponse();
		environment.putString("fromTransactionDate", "2021-01-01");
		environment.putString("toTransactionDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseOK.json");
		run(condition);

		JsonObject actualResponse = gson.fromJson(environment.getString("resource_endpoint_response_full", "body"), JsonObject.class);
		JsonArray actualData = actualResponse.getAsJsonArray("data");

		assertFalse(actualData.isEmpty(), "Should have data in the response");

		for (JsonElement element : actualData) {
			JsonObject resourceObject = element.getAsJsonObject();
			JsonElement resourceDate = resourceObject.get("transactionDate");
			assertNotNull(resourceDate, "Should have transactionDate");
		}
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithDateTime.json")
	public void checkTransactionDateTimeAtTransactionDateTimeResponse() {
		Gson gson = JsonUtils.createBigDecimalAwareGson();
		CheckExpectedTransactionDateTimeResponse condition = new CheckExpectedTransactionDateTimeResponse();
		environment.putString("fromTransactionDate", "2021-01-01");
		environment.putString("toTransactionDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithDateTime.json");
		run(condition);

		JsonObject actualResponse = gson.fromJson(environment.getString("resource_endpoint_response_full", "body"), JsonObject.class);
		JsonArray actualData = actualResponse.getAsJsonArray("data");

		assertFalse(actualData.isEmpty(), "Should have data in the response");

		for (JsonElement element : actualData) {
			JsonObject resourceObject = element.getAsJsonObject();
			JsonElement resourceDate = resourceObject.get("transactionDateTime");
			assertNotNull(resourceDate, "Should have transactionDateTime");
		}
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionResponseWithoutTransactionDate.json")
	public void testMissingDateFieldAtTransactionDateTimeResponse() {
		Gson gson = JsonUtils.createBigDecimalAwareGson();
		CheckExpectedTransactionDateTimeResponse condition = new CheckExpectedTransactionDateTimeResponse();
		environment.putString("fromTransactionDate", "2021-01-01");
		environment.putString("toTransactionDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithoutTransactionDate.json");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedTransactionDateTimeResponse: Could not find transactionDateTime JSON element in the resource Object", error.getMessage());
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithWrongTransactionDateField.json")
	public void testWrongDateFormatAtTransactionDateTimeResponse() {
		CheckExpectedTransactionDateTimeResponse condition = new CheckExpectedTransactionDateTimeResponse();
		environment.putString("fromTransactionDate", "2021-01-01");
		environment.putString("toTransactionDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithWrongTransactionDateField.json");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedTransactionDateTimeResponse: Error parsing date", error.getMessage());
	}

	@Test
	public void testWithNullFullRangeData() {
		CheckExpectedTransactionDateResponse condition = new CheckExpectedTransactionDateResponse();
		environment.putString("fromTransactionDate", "2021-01-01");
		environment.putString("toTransactionDate", "2021-02-01");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedTransactionDateResponse: [pre] Something unexpected happened (this could be caused by something you did wrong, or it may be an issue in the test suite - please review the instructions and your configuration, if you still see a problem please contact certification@oidf.org with the full details) - couldn't find object in environment: full_range_response", error.getMessage());
	}

	@Test
	@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithoutData.json")
		public void testWithNoDataInResponse() {
		CheckExpectedTransactionDateTimeResponse condition = new CheckExpectedTransactionDateTimeResponse();
		environment.putString("fromTransactionDate", "2021-01-01");
		environment.putString("toTransactionDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithoutData.json");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedTransactionDateTimeResponse: Full Range data response cannot be empty", error.getMessage());
		}

		@Test
		@UseResurce("jsonResponses/account/transactions/accountTransactionsResponseWithoutData.json")
		public void testWithActualDataNull() {
		CheckExpectedTransactionDateResponse condition = new CheckExpectedTransactionDateResponse();
		environment.putString("fromTransactionDate", "2021-01-01");
		environment.putString("toTransactionDate", "2021-02-01");
		copyAndAddFullRangeResponseToEnvironment("jsonResponses/account/transactions/accountTransactionsFullRangeResponseWithActualDataNull.json");
		ConditionError error = runAndFail(condition);
		assertEquals("CheckExpectedTransactionDateResponse: Could not find data JSON array in the actual response", error.getMessage());
		}

	private void copyAndAddFullRangeResponseToEnvironment(String path) {
		try {
			String fullResponseJson = IOUtils.resourceToString(path, StandardCharsets.UTF_8, getClass().getClassLoader());
			environment.putString("resource_endpoint_response_full", "body", fullResponseJson);
			TestInstanceEventLog eventLog = mock(TestInstanceEventLog.class);
			copyResourceEndpointResponse.setProperties("UNIT-TEST", eventLog, Condition.ConditionResult.INFO);
			copyResourceEndpointResponse.evaluate(environment);
			environment.mapKey("full_range_response", "resource_endpoint_response_full_copy");
		} catch (IOException e) {
			throw new AssertionError("Could not load resource");
		}
	}




}
