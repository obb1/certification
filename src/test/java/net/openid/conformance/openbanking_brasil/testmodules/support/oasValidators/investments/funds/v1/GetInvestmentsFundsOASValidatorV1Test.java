package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.investments.funds.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetInvestmentsFundsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/opendata/investments/GetFundsResponseV1.json")
	public void happyPath() {
		run(new GetInvestmentsFundsOASValidatorV1());
	}

}
