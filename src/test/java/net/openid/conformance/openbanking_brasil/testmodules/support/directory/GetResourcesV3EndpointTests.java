package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;

public class GetResourcesV3EndpointTests extends AbstractGetXFromAuthServerTest {

	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/resources/v2/resources";
	}

	@Override
	protected String getApiFamilyType() {
		return "resources";
	}

	@Override
	protected String getApiVersion() {
		return "3.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetResourcesV3Endpoint();
	}
}
