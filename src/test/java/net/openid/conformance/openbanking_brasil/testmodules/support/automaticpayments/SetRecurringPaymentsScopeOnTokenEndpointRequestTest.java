package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SetRecurringPaymentsScopeOnTokenEndpointRequestTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		environment.putObject("token_endpoint_request_form_parameters", new JsonObject());
		SetRecurringPaymentsScopeOnTokenEndpointRequest condition = new SetRecurringPaymentsScopeOnTokenEndpointRequest();
		run(condition);

		JsonObject request = environment.getObject("token_endpoint_request_form_parameters");
		assertTrue(
			request.has("scope") &&
			request.get("scope").isJsonPrimitive() &&
			request.get("scope").getAsJsonPrimitive().isString()
		);
		assertEquals("recurring-payments", OIDFJSON.getString(request.get("scope")));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathMissingRequestFromEnvTest() {
		SetRecurringPaymentsScopeOnTokenEndpointRequest condition = new SetRecurringPaymentsScopeOnTokenEndpointRequest();
		run(condition);
	}
}
