package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.*;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.JsonUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.util.Base64;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateFidoAuthorizeRequestBodyToRequestEntityClaimsTest extends AbstractJsonResponseConditionUnitTest {


	private final static String RP_ID = "rp_id";
	private final static String EC_KID = "ec_id";
	private final static String RSA_KID = "rsa_id";

	private static KeyPair EC_KEY_PAIR;
	private static KeyPair RSA_KEY_PAIR;

	private final static JsonObject CLIENT_DATA = new JsonObjectBuilder().addField("test", "test").build();
	//	private final static String AUTHENTICATOR_DATA = "authenticator_data";
	private final static String USER_ID = "user_id";
	private final static String ENROLLMENT_ID = "enrollment_id";


	@Before
	public void init() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
		Security.addProvider(new BouncyCastleProvider());

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC", "BC");
		keyPairGenerator.initialize(new ECGenParameterSpec("prime256v1"));
		EC_KEY_PAIR = keyPairGenerator.generateKeyPair();
		JWK ecJwk = new ECKey.Builder(Curve.P_256, (ECPublicKey) EC_KEY_PAIR.getPublic()).privateKey(EC_KEY_PAIR.getPrivate()).keyID(EC_KID).build();


		keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		RSA_KEY_PAIR = keyPairGenerator.generateKeyPair();
		JWK rsaKwk = new RSAKey.Builder((RSAPublicKey) RSA_KEY_PAIR.getPublic()).privateKey(RSA_KEY_PAIR.getPrivate()).keyID(RSA_KID).build();

		environment.putObject("rsa_fido_keys_jwk", JsonUtils.createBigDecimalAwareGson().fromJson(rsaKwk.toJSONString(), JsonObject.class));
		environment.putObject("ec_fido_keys_jwk", JsonUtils.createBigDecimalAwareGson().fromJson(ecJwk.toJSONString(), JsonObject.class));
	}

	@Test
	public void happyPathWithRsaKey() {
		happyPathTest("rsa_fido_keys_jwk", RSA_KID, RSA_KEY_PAIR, KeyType.RSA);
	}

	@Test
	public void happyPathWithEcKey() {
		happyPathTest("ec_fido_keys_jwk", EC_KID, EC_KEY_PAIR, KeyType.EC);
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingEnrollmentId() {
		prepareUnhappyTest();
		environment.removeNativeValue("enrollment_id");
		environment.mapKey("fido_keys_jwk", "rsa_fido_keys_jwk");

		CreateFidoAuthorizeRequestBodyToRequestEntityClaims condition = new CreateFidoAuthorizeRequestBodyToRequestEntityClaims();
		run(condition);
	}

	@Test
	public void unhappyPathTestMissingFidoKeysJwk() {
		prepareUnhappyTest();
		unhappyTest("Could not find fido_keys_jwk in the environment");
	}

	@Test
	public void unhappyPathTestMissingKid() {
		prepareUnhappyTest();

		JsonObject fidoKeysJwk = environment.getObject("rsa_fido_keys_jwk").deepCopy();
		fidoKeysJwk.remove("kid");
		environment.putObject("fido_keys_jwk", fidoKeysJwk);

		unhappyTest("Could not find kid in the fido_keys_jwk");
	}

	@Test
	public void unhappyPathTestMissingClientData() {
		prepareUnhappyTest();
		environment.removeObject("client_data");
		environment.mapKey("fido_keys_jwk", "rsa_fido_keys_jwk");
		unhappyTest("Could not find client_data in the environment");
	}

	@Test
	public void unhappyPathTestMissingUserId() {
		prepareUnhappyTest();
		environment.removeNativeValue("user_id");
		environment.mapKey("fido_keys_jwk", "rsa_fido_keys_jwk");
		unhappyTest("Could not find user_id in the environment");
	}

	@Test
	public void unhappyPathTestInvalidPrivateKey() {
		prepareUnhappyTest();

		JsonObject fidoKeysJwk = environment.getObject("rsa_fido_keys_jwk").deepCopy();
		String n = OIDFJSON.getString(fidoKeysJwk.get("n"));
		fidoKeysJwk.addProperty("n", n.substring(1));
		environment.putObject("fido_keys_jwk", fidoKeysJwk);

		unhappyTest("Could not sign authorisation payload");
	}

	@Test
	public void unhappyPathTestMissingRpId() {
		prepareUnhappyTest();
		environment.removeNativeValue("rp_id");
		environment.mapKey("fido_keys_jwk", "rsa_fido_keys_jwk");
		unhappyTest("Could not find rp_id in the environment");
	}

	protected void happyPathTest(String envVar, String kid, KeyPair keyPair, KeyType keyType) {
		try {
			environment.putString("rp_id", RP_ID);
			environment.putObject("client_data", CLIENT_DATA);
			environment.putString("user_id", USER_ID);
			environment.putString("enrollment_id", ENROLLMENT_ID);
			environment.mapKey("fido_keys_jwk", envVar);

			CreateFidoAuthorizeRequestBodyToRequestEntityClaims condition = new CreateFidoAuthorizeRequestBodyToRequestEntityClaims();
			run(condition);

			JsonObject request = environment.getObject("resource_request_entity_claims");
			byte[] authenticatorData = getAuthenticatorData();
			JsonObject expectedObject = getExpectedObject(kid, authenticatorData);

			JsonObject responseObject = request.getAsJsonObject("data").getAsJsonObject("fidoAssertion").getAsJsonObject("response");
			String signature = OIDFJSON.getString(responseObject.get("signature"));
			responseObject.remove("signature");
			assertEquals(expectedObject, request);
			validateSignature(signature, keyPair, authenticatorData, keyType);
		} catch (NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
			throw new AssertionError("Test Failed", e);
		}
	}

	protected void prepareUnhappyTest() {
		environment.putString("rp_id", RP_ID);
		environment.putObject("client_data", CLIENT_DATA);
		environment.putString("user_id", USER_ID);
		environment.putString("enrollment_id", ENROLLMENT_ID);
	}

	protected void unhappyTest(String errorMessage) {
		CreateFidoAuthorizeRequestBodyToRequestEntityClaims condition = new CreateFidoAuthorizeRequestBodyToRequestEntityClaims();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(errorMessage));
	}


	private byte[] getAuthenticatorData() throws NoSuchAlgorithmException {

		byte[] rpIdHash = MessageDigest.getInstance("SHA-256").digest(RP_ID.getBytes(StandardCharsets.UTF_8));
		byte flags = Byte.parseByte("00000101", 2);
		byte[] signCount = new byte[4];
		byte[] authenticatorDataPayload = ArrayUtils.add(rpIdHash, flags);
		return ArrayUtils.addAll(authenticatorDataPayload, signCount);
	}

	private void validateSignature(String signature, KeyPair keyPair, byte[] authenticatorData, KeyType keyType) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		byte[] providedDecodedSignature = Base64.getUrlDecoder().decode(signature);

		byte[] clientDataHash = MessageDigest.getInstance("SHA-256").digest(CLIENT_DATA.toString().getBytes(StandardCharsets.UTF_8));
		final byte[] expectedPayload = ArrayUtils.addAll(authenticatorData, clientDataHash);
		Signature instance;

		if (KeyType.RSA.equals(keyType)) {
			instance = Signature.getInstance("SHA256withRSA");
		} else if (KeyType.EC.equals(keyType)) {
			instance = Signature.getInstance("SHA256withECDSA");
		} else {
			throw new AssertionError("Test Failed - unsupported signature algorithm");
		}

		instance.initVerify(keyPair.getPublic());
		instance.update(expectedPayload);
		boolean verify = instance.verify(providedDecodedSignature);
		assertTrue(verify);
	}


	private JsonObject getExpectedObject(String kid, byte[] authenticatorData) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {


		JsonObject response = new JsonObjectBuilder()
			.addField("clientDataJSON", Base64.getUrlEncoder().withoutPadding().encodeToString(CLIENT_DATA.toString().getBytes(StandardCharsets.UTF_8)))
			.addField("authenticatorData", Base64.getUrlEncoder().withoutPadding().encodeToString(authenticatorData))
			.addField("userHandle", USER_ID)
			.build();

		JsonObject fidoAssertion = new JsonObjectBuilder()
			.addField("id", Base64.getUrlEncoder().withoutPadding().encodeToString(kid.getBytes(StandardCharsets.UTF_8)))
			.addField("rawId", Base64.getUrlEncoder().withoutPadding().encodeToString(kid.getBytes(StandardCharsets.UTF_8)))
			.addField("type", "public-key")
			.addField("response", response)
			.build();

		JsonObject riskSignals = new JsonObjectBuilder()
			.addField("deviceId", "5ad82a8f-37e5-4369-a1a3-be4b1fb9c034")
			.addField("isRootedDevice", false)
			.addField("screenBrightness", 90)
			.addField("elapsedTimeSinceBoot", 28800000)
			.addField("osVersion", "16.6")
			.addField("userTimeZoneOffset", "-03")
			.addField("language", "pt")
			.addField("accountTenure", "2023-01-01")
			.addFields("screenDimensions", Map.of(
				"height", 1080,
				"width", 1920
			))
			.build();

		return new JsonObjectBuilder()
			.addFields("data", Map.of(
				"enrollmentId", ENROLLMENT_ID,
				"riskSignals", riskSignals,
				"fidoAssertion", fidoAssertion
			))
			.build();
	}


}
