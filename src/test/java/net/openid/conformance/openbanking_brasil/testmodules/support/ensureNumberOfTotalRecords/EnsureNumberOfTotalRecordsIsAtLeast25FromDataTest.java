package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public class EnsureNumberOfTotalRecordsIsAtLeast25FromDataTest extends AbstractEnsureNumberOfTotalRecordsFromDataTest{
	@Override
	protected int getTotalRecordsComparisonAmount() {
		return 25;
	}

	@Override
	protected TotalRecordsComparisonOperator getComparisonMethod() {
		return TotalRecordsComparisonOperator.AT_LEAST;
	}

	@Override
	protected AbstractEnsureNumberOfTotalRecordsFromData getCondition() {
		return new EnsureNumberOfTotalRecordsIsAtLeast25FromData();
	}
}
