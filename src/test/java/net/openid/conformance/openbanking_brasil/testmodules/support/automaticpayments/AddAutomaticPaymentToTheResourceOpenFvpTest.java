package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddAutomaticPaymentToTheResourceOpenFvp;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AddAutomaticPaymentToTheResourceOpenFvpTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CPF = "12345678901";
	private static final String CNPJ = "12345678000101";
	private static final String ISPB = "12345678";
	private static final String ISSUER = "1234";
	private static final String NUMBER = "123456789";
	private static final String ACCOUNT_TYPE = "CACC";
	private static final String PROXY = "email@email.com";

	@Test
	public void happyPathTestCpf() {
		happyPathTest(true, true);
	}

	@Test
	public void happyPathTestCnpj() {
		happyPathTest(false, true);
	}

	@Test
	public void happyPathTestConfigResource() {
		happyPathTest(true, false);
	}

	@Test
	public void unhappyPathTestMissingResource() {
		unhappyPathTest("Could not find resource object");
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsent() {
		JsonObject resource = createResourceField("loggedUser", CPF);
		resource.remove("brazilPaymentConsent");
		environment.putObject("resource", resource);
		unhappyPathTest("Could not extract the consent data from the resource");
	}

	@Test
	public void unhappyPathTestMissingBrazilPaymentConsentData() {
		JsonObject resource = createResourceField("loggedUser", CPF);
		JsonObject consent = resource.getAsJsonObject("brazilPaymentConsent");
		consent.remove("data");
		environment.putObject("resource", resource);
		unhappyPathTest("Could not extract the consent data from the resource");
	}

	@Test
	public void unhappyPathTestCnpjMissingDocument() {
		JsonObject resource = createResourceField("businessEntity", CNPJ);
		JsonObject businessEntity = resource.getAsJsonObject("brazilPaymentConsent")
			.getAsJsonObject("data")
			.getAsJsonObject("businessEntity");
		businessEntity.remove("document");
		environment.putObject("resource", resource);
		unhappyPathTest("Could not find identification field inside businessEntity");
	}

	@Test
	public void unhappyPathTestCnpjMissingIdentification() {
		JsonObject resource = createResourceField("businessEntity", CNPJ);
		JsonObject document = resource.getAsJsonObject("brazilPaymentConsent")
			.getAsJsonObject("data")
			.getAsJsonObject("businessEntity")
			.getAsJsonObject("document");
		document.remove("identification");
		environment.putObject("resource", resource);
		unhappyPathTest("Could not find identification field inside businessEntity");
	}

	@Test
	public void unhappyPathTestCpfMissingDocument() {
		JsonObject resource = createResourceField("loggedUser", CPF);
		JsonObject loggedUser = resource.getAsJsonObject("brazilPaymentConsent")
			.getAsJsonObject("data")
			.getAsJsonObject("loggedUser");
		loggedUser.remove("document");
		environment.putObject("resource", resource);
		unhappyPathTest("Could not find identification field inside loggedUser");
	}

	@Test
	public void unhappyPathTestCpfMissingIdentification() {
		JsonObject resource = createResourceField("loggedUser", CPF);
		JsonObject document = resource.getAsJsonObject("brazilPaymentConsent")
			.getAsJsonObject("data")
			.getAsJsonObject("loggedUser")
			.getAsJsonObject("document");
		document.remove("identification");
		environment.putObject("resource", resource);
		unhappyPathTest("Could not find identification field inside loggedUser");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountIspb() {
		unhappyPathTestCreditorAccount("Ispb");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountIssuer() {
		unhappyPathTestCreditorAccount("Issuer");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountNumber() {
		unhappyPathTestCreditorAccount("Number");
	}

	@Test
	public void unhappyPathTestMissingCreditorAccountAccountType() {
		unhappyPathTestCreditorAccount("AccountType");
	}

	@Test
	public void unhappyPathTestMissingCreditorProxy() {
		JsonObject resource = createResourceField("loggedUser", CPF);
		resource.remove("creditorProxy");
		environment.putObject("resource", resource);
		unhappyPathTest("Could not find creditorProxy in the resource");
	}

	private void happyPathTest(boolean isCpf, boolean hasResource) {
		String consentDataField = isCpf ? "loggedUser" : "businessEntity";
		String identification = isCpf ? CPF : CNPJ;
		String rel = isCpf ? "CPF" : "CNPJ";

		JsonObject resource = createResourceField(consentDataField, identification);

		if (hasResource) {
			environment.putObject("resource", resource);
		} else {
			environment.putObject("config", "resource", resource);
		}

		run(new AddAutomaticPaymentToTheResourceOpenFvp());

		assertResponse(identification, rel);
	}

	private JsonObject createResourceField(String consentDataField, String identification) {
		return new JsonObjectBuilder()
			.addField(String.format("brazilPaymentConsent.data.%s.document.identification", consentDataField), identification)
			.addField("creditorAccountIspb", ISPB)
			.addField("creditorAccountIssuer", ISSUER)
			.addField("creditorAccountNumber", NUMBER)
			.addField("creditorAccountAccountType", ACCOUNT_TYPE)
			.addField("creditorProxy", PROXY)
			.build();
	}

	private void assertResponse(String identification, String rel) {
		String ispb = environment.getString("ispb");
		assertEquals(DictHomologKeys.PROXY_CNPJ_INITIATOR.substring(0, 8), ispb);

		String expectedEndToEndId = environment.getString("endToEndId");
		String expectedDate = LocalDate.now(ZoneId.of("America/Sao_Paulo")).toString();

		JsonObject payment = environment.getElementFromObject("resource", "brazilPixPayment").getAsJsonObject();

		JsonObject data = assertObjectField(payment, "data");
		assertStringField(data, "endToEndId", expectedEndToEndId);
		assertStringField(data, "date", expectedDate);
		assertStringField(data, "remittanceInformation", DictHomologKeys.PROXY_EMAIL_STANDARD_REMITTANCEINFORMATION);
		assertStringField(data, "cnpjInitiator", DictHomologKeys.PROXY_CNPJ_INITIATOR);
		assertStringField(data, "ibgeTownCode", DictHomologKeys.PROXY_EMAIL_STANDARD_IBGETOWNCODE);
		assertStringField(data, "localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT);
		assertStringField(data, "proxy", PROXY);

		JsonObject dataPayment = assertObjectField(data, "payment");
		assertStringField(dataPayment, "amount", "100.00");
		assertStringField(dataPayment, "currency", "BRL");

		JsonObject document = assertObjectField(data, "document");
		assertStringField(document, "identification", identification);
		assertStringField(document, "rel", rel);

		JsonObject creditorAccount = assertObjectField(data, "creditorAccount");
		assertStringField(creditorAccount, "ispb", ISPB);
		assertStringField(creditorAccount, "issuer", ISSUER);
		assertStringField(creditorAccount, "number", NUMBER);
		assertStringField(creditorAccount, "accountType", ACCOUNT_TYPE);
	}

	private JsonObject assertObjectField(JsonObject obj, String fieldName) {
		assertTrue(obj.has(fieldName) && obj.get(fieldName).isJsonObject());
		return obj.getAsJsonObject(fieldName);
	}

	private void assertStringField(JsonObject obj, String fieldName, String expectedValue) {
		assertTrue(obj.has(fieldName) && obj.get(fieldName).isJsonPrimitive() && obj.get(fieldName).getAsJsonPrimitive().isString());
		assertEquals(expectedValue, OIDFJSON.getString(obj.get(fieldName)));
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new AddAutomaticPaymentToTheResourceOpenFvp());
		assertTrue(error.getMessage().contains(message));
	}

	private void unhappyPathTestCreditorAccount(String field) {
		JsonObject resource = createResourceField("loggedUser", CPF);
		resource.remove("creditorAccount" + field);
		environment.putObject("resource", resource);
		unhappyPathTest(String.format("Could not find creditorAccount %s in the resource", field));
	}
}
