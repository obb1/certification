package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.AbstractCreateAutomaticRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalVariableAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;

public class CreateAutomaticRecurringConfigurationObjectSemanalVariableAmountTest extends AbstractCreateAutomaticRecurringConfigurationObjectTest {

	@Override
	protected AbstractCreateAutomaticRecurringConfigurationObject condition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalVariableAmount();
	}

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getExpectedInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected boolean hasFixedAmount() {
		return false;
	}

	@Override
	protected boolean hasMinimumVariableAmount() {
		return true;
	}

	@Override
	protected boolean hasMaximumVariableAmount() {
		return true;
	}
}
