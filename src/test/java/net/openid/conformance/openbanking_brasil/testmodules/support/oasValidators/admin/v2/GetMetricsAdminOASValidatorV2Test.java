package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.admin.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetMetricsAdminOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/admin/GetMetricsResponse.json")
	public void testHappyPath() {
		GetMetricsAdminOASValidatorV2 cond = new GetMetricsAdminOASValidatorV2();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/admin/GetMetricsResponse_missing_field.json")
	public void testUnhappyPath() {
		GetMetricsAdminOASValidatorV2 cond = new GetMetricsAdminOASValidatorV2();
		runAndFail(cond);
	}
}
