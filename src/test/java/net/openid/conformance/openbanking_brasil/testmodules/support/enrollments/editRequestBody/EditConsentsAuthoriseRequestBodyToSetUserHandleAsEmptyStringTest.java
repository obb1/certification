package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

public class EditConsentsAuthoriseRequestBodyToSetUserHandleAsEmptyStringTest extends AbstractJsonResponseConditionUnitTest {

	private static final String JSON_BASE_PATH = "jsonRequests/payments/enrollments/v2/postConsentsAuthoriseRequest";
	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Test
	@UseResurce(value = JSON_BASE_PATH + "V2.json", key = REQUEST_CLAIMS_KEY)
	public void happyPathTest() {
		run(new EditConsentsAuthoriseRequestBodyToSetUserHandleAsEmptyString());
		String userHandle = OIDFJSON.getString(environment.getElementFromObject(REQUEST_CLAIMS_KEY, "data.fidoAssertion.response.userHandle"));
		assertEquals("", userHandle);
	}

	@Test
	public void unhappyPathTestMissingRequest() {
		assertThrowsExactly(AssertionError.class, () -> {
			run(new EditConsentsAuthoriseRequestBodyToSetUserHandleAsEmptyString());
		});
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(REQUEST_CLAIMS_KEY, new JsonObject());
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutFidoAssertionV2.json", key = REQUEST_CLAIMS_KEY)
	public void unhappyPathTestMissingFidoAssertion() {
		unhappyPathTest();
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithoutFidoAssertionResponseV2.json", key = REQUEST_CLAIMS_KEY)
	public void unhappyPathTestMissingFidoAssertionResponse() {
		unhappyPathTest();
	}

	protected void unhappyPathTest() {
		ConditionError error = runAndFail(new EditConsentsAuthoriseRequestBodyToSetUserHandleAsEmptyString());
		assertTrue(error.getMessage().contains("Could not find fidoAssertion response inside consents authorise request body"));
	}
}
