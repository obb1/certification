package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

public class EnsureConsentResponseCodeWas201Test extends AbstractEnsureResponseCodeWasTest {

	@Override
	protected AbstractEnsureResponseCodeWas condition() {
		return new EnsureConsentResponseCodeWas201();
	}

	@Override
	protected int happyPathResponseCode() {
		return HttpStatus.CREATED.value();
	}

	@Override
	protected int unhappyPathResponseCode() {
		return HttpStatus.BAD_REQUEST.value();
	}
}
