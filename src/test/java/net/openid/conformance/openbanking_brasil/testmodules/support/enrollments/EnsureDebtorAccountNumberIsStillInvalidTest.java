package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureDebtorAccountNumberIsStillInvalidTest extends AbstractJsonResponseConditionUnitTest {

	private static final String DEBTOR_ACCOUNT_NUMBER = "1234";
	private static final String BASE_JSON_PATH = "jsonResponses/enrollments/v2/PostEnrollments201Response";

	@Before
	public void init() {
		environment.putString("debtor_account_number", DEBTOR_ACCOUNT_NUMBER);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "InvalidDebtorAccountNumber.json")
	public void happyPathTest() {
		run(new EnsureDebtorAccountNumberIsStillInvalid());
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureDebtorAccountNumberIsStillInvalid.RESPONSE_ENV_KEY, new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	@Test
	public void unhappyPathTestUnparseableBody() {
		environment.putObject(EnsureDebtorAccountNumberIsStillInvalid.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(EnsureDebtorAccountNumberIsStillInvalid.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build());
		unhappyPathTest("Could not find data.debtorAccount.number inside enrollment response body");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "NoDebtorAccount.json")
	public void unhappyPathTestMissingDebtorAccount() {
		unhappyPathTest("Could not find data.debtorAccount.number inside enrollment response body");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "NoDebtorAccountNumber.json")
	public void unhappyPathTestMissingDebtorAccountNumber() {
		unhappyPathTest("Could not find data.debtorAccount.number inside enrollment response body");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void unhappyPathTestNumbersNotMatching() {
		unhappyPathTest("The debtor account number in the enrollments response is not equals to the original invalid number sent at the request");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureDebtorAccountNumberIsStillInvalid());
		assertTrue(error.getMessage().contains(message));
	}
}
