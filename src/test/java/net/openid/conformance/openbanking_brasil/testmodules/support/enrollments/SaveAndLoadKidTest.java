package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SaveAndLoadKidTest extends AbstractJsonResponseConditionUnitTest {

	private JsonObject FIDO_KEY_JWK;

	@Before
	public void init() {
		FIDO_KEY_JWK = new JsonObjectBuilder()
			.addField("p", "p")
			.addField("kty", "RSA")
			.addField("q", "q")
			.addField("e", "e")
			.addField("d", "d")
			.addField("kid", "kid")
			.addField("qi", "qi")
			.addField("dp", "dp")
			.addField("dq", "dq")
			.addField("n", "n")
			.build();
	}

	@Test
	public void happyPathTest() {
		environment.putObject("fido_keys_jwk", FIDO_KEY_JWK);

		// save kid and check if it worked
		run(new SaveKid());
		String savedKid = environment.getString("saved_kid");
		assertEquals(OIDFJSON.getString(FIDO_KEY_JWK.get("kid")), savedKid);

		// update kid
		FIDO_KEY_JWK.addProperty("kid", "different kid");

		// load kid and check if it overwrites the update
		run(new LoadOldKidToKeyPair());
		assertEquals(savedKid, OIDFJSON.getString(FIDO_KEY_JWK.get("kid")));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestSaveWithoutKeys() {
		run(new SaveKid());
	}

	@Test
	public void unhappyPathTestSaveMissingKid() {
		JsonObject fidoKeyJwk = FIDO_KEY_JWK.deepCopy();
		fidoKeyJwk.remove("kid");
		environment.putObject("fido_keys_jwk", fidoKeyJwk);
		ConditionError error = runAndFail(new SaveKid());
		assertTrue(error.getMessage().contains("Could not find kid in the fido_keys_jwk"));
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestLoadBeforeSave() {
		environment.putObject("fido_keys_jwk", FIDO_KEY_JWK);
		run(new LoadOldKidToKeyPair());
	}
}
