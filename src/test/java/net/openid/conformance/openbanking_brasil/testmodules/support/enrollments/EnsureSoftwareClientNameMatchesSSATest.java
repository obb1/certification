package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

public class EnsureSoftwareClientNameMatchesSSATest extends AbstractJsonResponseConditionUnitTest {
	protected static final String BASE_JSON_PATH = "jsonResponses/enrollments/v2/";
	protected static final String CLIENT_NAME = "Raidiam Mockbank - Pipeline NRJ";

	@Test
	@UseResurce(BASE_JSON_PATH + "PostFidoRegistrationOptions201ResponseRPName.json")
	public void happyPathTest() {
		environment.putString("software_client_name", CLIENT_NAME);

		EnsureSoftwareClientNameMatchesSSA condition = new EnsureSoftwareClientNameMatchesSSA();
		run(condition);
		assertEquals(CLIENT_NAME, environment.getString("software_client_name"));
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(EnsureSoftwareClientNameMatchesSSA.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(EnsureSoftwareClientNameMatchesSSA.RESPONSE_ENV_KEY,
			new JsonObject());
		unhappyPathTest("Could not extract body from jwt response");
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(EnsureSoftwareClientNameMatchesSSA.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build());
		unhappyPathTest("Could not extract the name from the RP in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostFidoRegistrationOptions201ResponseRPNameMissing.json")
	public void unhappyPathTestMissingRPName() {
		unhappyPathTest("Could not extract the name from the RP in the response");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "PostFidoRegistrationOptions201ResponseRPNameMismatch.json")
	public void unhappyPathTestRPNameMismatch() {
		unhappyPathTest("Mismatch between software client name and RP name");
	}

	protected void unhappyPathTest(String message) {
		environment.putString("software_client_name", CLIENT_NAME);

		ConditionError error = runAndFail(new EnsureSoftwareClientNameMatchesSSA());
		assertTrue(error.getMessage().contains(message));
	}
}
