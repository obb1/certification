package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.AbstractValidateField;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.ValidateConsentsFieldV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.ValidateConsentsOperationalFieldV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.ValidateCustomerFieldV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.ValidateEnrollmentsFieldV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.ValidateEnrollmentsFieldV2;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidateFieldTest extends AbstractJsonResponseConditionUnitTest {

	protected static final String CONSENT_V2 = "https://www.example.com/consents/v2/consents";
	protected static final String CONSENT_V3 = "https://www.example.com/consents/v3/consents";
	protected static final String ENROLLMENT_V1 = "https://www.example.com/enrollments/v1/enrollments";
	protected static final String ENROLLMENT_V2 = "https://www.example.com/enrollments/v2/enrollments";
	protected static final String PAYMENT_CONSENT_V3 = "https://www.example.com/payments/v3/consents";
	protected static final String PAYMENT_CONSENT_V4 = "https://www.example.com/payments/v4/consents";
	protected static final String CPF = "12345678901";
	protected static final String CNPJ = "12345678901234";


	@Test
	public void happyPathConsentsV3PersonalTest() {
		buildConfigForTest(CONSENT_V3, null, "personal", CPF, null, false);
		happyPathTest(new ValidateConsentsFieldV3());
	}

	@Test
	public void happyPathConsentsV3BusinessTest() {
		buildConfigForTest(CONSENT_V3, null, "business", CPF, CNPJ, false);
		happyPathTest(new ValidateConsentsFieldV3());
	}

	@Test
	public void happyPathConsentsOperationalV2PersonalTest() {
		buildConfigForTest(CONSENT_V2, null, "personal", CPF, null, true);
		happyPathTest(new ValidateConsentsOperationalFieldV2());
	}

	@Test
	public void happyPathConsentsOperationalV2BusinessTest() {
		buildConfigForTest(CONSENT_V2, null, "business", CPF, CNPJ, true);
		happyPathTest(new ValidateConsentsOperationalFieldV2());
	}

	@Test
	public void happyPathCustomerV2Test() {
		buildConfigForTest(CONSENT_V3, null, null, CPF, null, false);
		happyPathTest(new ValidateCustomerFieldV2());
	}

	@Test
	public void happyPathEnrollmentsV1PersonalTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, ENROLLMENT_V1, "personal", CPF, null, false);
		happyPathTest(new ValidateEnrollmentsFieldV1());
	}

	@Test
	public void happyPathEnrollmentsV1BusinessTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, ENROLLMENT_V1, "business", CPF, CNPJ, false);
		happyPathTest(new ValidateEnrollmentsFieldV1());
	}

	@Test
	public void happyPathEnrollmentsV2PersonalTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, ENROLLMENT_V2, "personal", CPF, null, false);
		happyPathTest(new ValidateEnrollmentsFieldV2());
	}

	@Test
	public void happyPathEnrollmentsV2BusinessTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, ENROLLMENT_V2, "business", CPF, CNPJ, false);
		happyPathTest(new ValidateEnrollmentsFieldV2());
	}


	@Test
	public void unhappyPathConsentsV3WrongVersionTest() {
		buildConfigForTest(CONSENT_V2, null, "personal", CPF, null, false);
		unhappyPathTest(new ValidateConsentsFieldV3(), "consentUrl does not match the regex");
	}

	@Test
	public void unhappyPathCustomersV2WrongVersionTest() {
		buildConfigForTest(CONSENT_V2, null, "personal", CPF, null, false);
		unhappyPathTest(new ValidateCustomerFieldV2(), "consentUrl does not match the regex");
	}

	@Test
	public void unhappyPathEnrollmentsV1WrongConsentVersionTest() {
		buildConfigForTest(PAYMENT_CONSENT_V3, ENROLLMENT_V1, "personal", CPF, null, false);
		unhappyPathTest(new ValidateEnrollmentsFieldV1(), "consentUrl does not match the regex");
	}

	@Test
	public void unhappyPathEnrollmentsV1WrongEnrollmentVersionTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, ENROLLMENT_V2, "personal", CPF, null, false);
		unhappyPathTest(new ValidateEnrollmentsFieldV1(), "enrollmentsUrl does not match the regex");
	}

	@Test
	public void unhappyPathEnrollmentsV2WrongConsentVersionTest() {
		buildConfigForTest(PAYMENT_CONSENT_V3, ENROLLMENT_V2, "personal", CPF, null, false);
		unhappyPathTest(new ValidateEnrollmentsFieldV2(), "consentUrl does not match the regex");
	}

	@Test
	public void unhappyPathEnrollmentsV2WrongEnrollmentVersionTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, ENROLLMENT_V1, "personal", CPF, null, false);
		unhappyPathTest(new ValidateEnrollmentsFieldV2(), "enrollmentsUrl does not match the regex");
	}


	@Test
	public void unhappyPathMissingBrazilCpfOperationalTest() {
		buildConfigForTest(CONSENT_V2, null, "personal", null, null, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "Element with path $.resource.brazilCpfOperational was not found");
	}

	@Test
	public void unhappyPathEmptyBrazilCpfOperationalTest() {
		buildConfigForTest(CONSENT_V2, null, "personal", "", null, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "brazilCpfOperational is not valid");
	}

	@Test
	public void unhappyPathInvalidBrazilCpfOperationalTest() {
		buildConfigForTest(CONSENT_V2, null, "personal", CNPJ, null, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "brazilCpfOperational is not valid");
	}

	@Test
	public void unhappyPathMissingProductTypeOperationalTest() {
		buildConfigForTest(CONSENT_V2, null, null, CPF, null, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "Element with path $.consent.productType was not found");
	}

	@Test
	public void unhappyPathEmptyProductTypeOperationalTest() {
		buildConfigForTest(CONSENT_V2, null, "", CPF, null, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "Product type (Business or Personal) must be specified in the test configuration");
	}

	@Test
	public void unhappyPathInvalidProductTypeOperationalTest() {
		buildConfigForTest(CONSENT_V2, null, "invalid", CPF, null, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "Product type (Business or Personal) must be specified in the test configuration");
	}

	@Test
	public void unhappyPathMissingbrazilCnpjOperationalBusinessTest() {
		buildConfigForTest(CONSENT_V2, null, "business", CPF, null, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "Element with path $.resource.brazilCnpjOperationalBusiness was not found");
	}

	@Test
	public void unhappyPathEmptybrazilCnpjOperationalBusinessTest() {
		buildConfigForTest(CONSENT_V2, null, "business", CPF, "", true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "brazilCnpjOperationalBusiness is not valid");
	}

	@Test
	public void unhappyPathInvalidbrazilCnpjOperationalBusinessTest() {
		buildConfigForTest(CONSENT_V2, null, "business", CPF, CPF, true);
		unhappyPathTest(new ValidateConsentsOperationalFieldV2(), "brazilCnpjOperationalBusiness is not valid");
	}

	@Test
	public void unhappyPathMissingEnrollmentsUrlTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, null, "personal", CPF, null, false);
		unhappyPathTest(new ValidateEnrollmentsFieldV1(), "Element with path $.resource.enrollmentsUrl was not found");
	}

	@Test
	public void unhappyPathInvalidEnrollmentsUrlTest() {
		buildConfigForTest(PAYMENT_CONSENT_V4, CONSENT_V2, "personal", CPF, null, false);
		unhappyPathTest(new ValidateEnrollmentsFieldV1(), "enrollmentsUrl does not match the regex");
	}

	protected void happyPathTest(AbstractValidateField validateField) {
		run(validateField);
	}

	protected void unhappyPathTest(AbstractValidateField validateField, String errorMessage) {
		ConditionError error = runAndFail(validateField);
		assertTrue(error.getMessage().contains(errorMessage));
	}

	protected void buildConfigForTest(String consentUrl, String enrollmentUrl, String productType, String brazilCpf, String brazilCnpj, boolean isOperational) {
		JsonObjectBuilder configBuilder = new JsonObjectBuilder();

		addFieldIfNotNull(configBuilder, "resource.consentUrl", consentUrl);
		addFieldIfNotNull(configBuilder, "resource.enrollmentsUrl", enrollmentUrl);
		addFieldIfNotNull(configBuilder, "consent.productType", productType);

		if (isOperational) {
			addFieldIfNotNull(configBuilder, "resource.brazilCpfOperational", brazilCpf);
			addFieldIfNotNull(configBuilder, "resource.brazilCnpjOperationalBusiness", brazilCnpj);
		} else {
			addFieldIfNotNull(configBuilder, "resource.brazilCpf", brazilCpf);
			addFieldIfNotNull(configBuilder, "resource.brazilCnpj", brazilCnpj);
		}

		environment.putObject("config", configBuilder.build());
	}

	private void addFieldIfNotNull(JsonObjectBuilder builder, String key, String value) {
		if (value != null) {
			builder.addField(key, value);
		}
	}
}
