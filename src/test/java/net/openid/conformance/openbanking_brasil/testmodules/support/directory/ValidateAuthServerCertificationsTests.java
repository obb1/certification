package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class ValidateAuthServerCertificationsTests extends AbstractJsonResponseConditionUnitTest {
	JsonObject authServer;
	private JsonObject load(String name) throws IOException {
		String jsonString = IOUtils.resourceToString("jsonEnvironmentObjects/".concat(name), Charset.defaultCharset(), getClass().getClassLoader());
		return new JsonParser().parse(jsonString).getAsJsonObject();
	}
	@Before
	public void init() {
		try {
			authServer = load("directory/authServer/good_auth_server.json");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		JsonArray certifications = new JsonArray();
		LocalDate startDate = LocalDate.now();
		LocalDate expirationDate = startDate.plusMonths(13);
		String formattedStartDate = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String formattedExpirationDate = expirationDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		authServer.add("AuthorisationServerCertifications", certifications);
	}
	@Test
	public void validateAuthServerCertificationsHappyPath() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		environment.putObject("authorisation_server", authServer);
		run(condition);
	}

	@Test
	public void validateAuthServerCertificationsMissingCertifications() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		JsonObject adjustedAuthServer = authServer.deepCopy();
		JsonArray certifications = adjustedAuthServer.getAsJsonArray("AuthorisationServerCertifications");
		certifications.remove(0);
		certifications.remove(0);
		adjustedAuthServer.add("AuthorisationServerCertifications", certifications);
		environment.putObject("authorisation_server", adjustedAuthServer);
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("ValidateAuthServerCertifications: Security certification not found: certification: BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)"));
	}

	@Test
	public void validateAuthServerCertificationsNoCertifications() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		JsonArray certifications = new JsonArray();
		JsonObject adjustedAuthServer = authServer.deepCopy();
		adjustedAuthServer.add("AuthorisationServerCertifications", certifications);
		environment.putObject("authorisation_server", adjustedAuthServer);
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("ValidateAuthServerCertifications: AuthorisationServerCertifications not found"));
	}

	@Test
	public void validateAuthServerCertificationsExpiredCerts() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		JsonArray certifications = new JsonArray();
		JsonObject adjustedAuthServer = authServer.deepCopy();
		LocalDate startDate = LocalDate.now().minusMonths(24);
		LocalDate expirationDate = startDate.plusMonths(13);
		String formattedStartDate = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String formattedExpirationDate = expirationDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://openid.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key, PAR","something",1,"https://openid.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key, PAR","something",1,"https://openid.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key, PAR","something",1,"https://openid.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key, PAR","something",1,"https://openid.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		adjustedAuthServer.add("AuthorisationServerCertifications", certifications);
		environment.putObject("authorisation_server", adjustedAuthServer);
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("No active certifications found"));
	}

	@Test
	public void validateAuthServerCertificationsExpiredCertsThatAreStillValid() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		JsonArray certifications = new JsonArray();
		JsonObject adjustedAuthServer = authServer.deepCopy();
		LocalDate startDate = LocalDate.now().minusMonths(14);
		LocalDate expirationDate = startDate.plusMonths(13);
		String formattedStartDate = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String formattedExpirationDate = expirationDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		adjustedAuthServer.add("AuthorisationServerCertifications", certifications);
		environment.putObject("authorisation_server", adjustedAuthServer);
		run(condition);
	}

	@Test
	public void validateAuthServerCertificationsAuthServerCertUriBadRegex() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		JsonArray certifications = new JsonArray();
		JsonObject adjustedAuthServer = authServer.deepCopy();
		LocalDate startDate = LocalDate.now();
		LocalDate expirationDate = startDate.plusMonths(13);
		String formattedStartDate = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String formattedExpirationDate = expirationDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://oof.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		adjustedAuthServer.add("AuthorisationServerCertifications", certifications);
		environment.putObject("authorisation_server", adjustedAuthServer);
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("ValidateAuthServerCertifications: ApiCertificationUri Regex does not match: regex: (https:\\/\\/openid\\.net\\/(.*)\\/uploads\\/)((\\d{4}))(\\/)(\\d{2})(.*)(.zip), actual: https://oof.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip;ApiCertificationUri Regex does not match: regex: (https:\\/\\/openid\\.net\\/(.*)\\/uploads\\/)((\\d{4}))(\\/)(\\d{2})(.*)(.zip), actual: https://oof.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
	}

	@Test
	public void validateAuthServerCertificationsAuthServerDoesNotFailCertsAfterThreshold() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		JsonArray certifications = new JsonArray();
		JsonObject adjustedAuthServer = authServer.deepCopy();
		LocalDate startDate = LocalDate.now();
		LocalDate expirationDate = startDate.plusMonths(13);
		LocalDate badStartDate = LocalDate.now().minusMonths(12);
		LocalDate badExpirationDate = badStartDate.plusMonths(11);
		String formattedStartDate = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String formattedExpirationDate = expirationDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String badFormattedStartDate = badStartDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String badFormattedExpirationDate = badExpirationDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		certifications.add(buildCertification(badFormattedStartDate,badFormattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://openid.net/wordpress-content/uploads/"+badStartDate.getYear()+"/"+String.format("%02d",badStartDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		adjustedAuthServer.add("AuthorisationServerCertifications", certifications);
		environment.putObject("authorisation_server", adjustedAuthServer);
		run(condition);
	}
	@Test
	public void validateAuthServerCertificationsAuthServerCertUriBadDate() {
		ValidateAuthServerCertifications condition = new ValidateAuthServerCertifications();
		JsonArray certifications = new JsonArray();
		JsonObject adjustedAuthServer = authServer.deepCopy();
		LocalDate startDate = LocalDate.now();
		LocalDate expirationDate = startDate.plusMonths(13);
		String formattedStartDate = startDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String formattedExpirationDate = expirationDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		String formattedCertificationStartDate = startDate.format(DateTimeFormatter.ofPattern("MM-yyyy"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://openid.net/wordpress-content/uploads/2021/06/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ Private Key","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","FAPI Adv. OP w/ MTLS, PAR","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		certifications.add(buildCertification(formattedStartDate,formattedExpirationDate,"something","something","Self-Certified","BR-OF Adv. OP w/ Private Key, PAR (FAPI-BR v2)","something",1,"https://openid.net/wordpress-content/uploads/"+startDate.getYear()+"/"+String.format("%02d",startDate.getMonthValue())+"/Raidiam-Raidiam_Connect_OpenBanking_Brazil-mtls-by_value_plain_fapi_plain_response-28-Jun-2021.zip"));
		adjustedAuthServer.add("AuthorisationServerCertifications", certifications);
		environment.putObject("authorisation_server", adjustedAuthServer);
		ConditionError error = runAndFail(condition);
		String expectedError = String.format("ValidateAuthServerCertifications: ApiCertificationUri registration date is invalid, CertificationStartDate is equal to dateFromUri month or dateFromUri month +1 or year is incorrect: date_from_certification_start_date: %s CertificationId: something",formattedCertificationStartDate);
		assertThat(error.getMessage(), containsString(expectedError));
	}
	private JsonObject buildCertification(String startDate, String expirationDate, String certificationId, String authorizationServerId, String status, String profileVariant, String profileType, int profileVersion, String certificationURI) {
		JsonObject certification = new JsonObject();
		certification.addProperty("CertificationStartDate", startDate);
		certification.addProperty("CertificationExpirationDate", expirationDate);
		certification.addProperty("CertificationId", certificationId);
		certification.addProperty("AuthorisationServerId", authorizationServerId);
		certification.addProperty("Status", status);
		certification.addProperty("ProfileVariant", profileVariant);
		certification.addProperty("ProfileType", profileType);
		certification.addProperty("ProfileVersion", profileVersion);
		certification.addProperty("CertificationURI", certificationURI);
		return certification;
	}
}
