package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class StopIfWarningTest extends AbstractJsonResponseConditionUnitTest {
	@Test
	public void testHappyPath() {
		StopIfWarning cond = new StopIfWarning();
		run(cond);
	}

	@Test
	public void testUnhappyPath() {
		String warning = "this is a warning";
		environment.putString("warning_message",warning);
		StopIfWarning cond = new StopIfWarning();

		ConditionError error = runAndFail(cond);
		assertTrue(error.getMessage().contains(warning));
	}
}
