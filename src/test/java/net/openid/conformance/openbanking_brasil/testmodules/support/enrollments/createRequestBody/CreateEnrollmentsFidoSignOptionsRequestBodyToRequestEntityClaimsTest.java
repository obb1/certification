package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaimsTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void  happyPathTest(){
		environment.putString("rp_id", "rp_id");
		environment.putString("consent_id", "consent_id");

		CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims condition = new CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims();
		run(condition);

		JsonObject requestBody = environment.getObject("resource_request_entity_claims");
		JsonObject expectedObject = new JsonObjectBuilder().addFields("data", Map.of(
			"rp", "rp_id",
			"platform", "ANDROID",
			"consentId", "consent_id"
		)).build();

		assertEquals(expectedObject, requestBody);
	}
}
