package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;

public class CopyOriginalRecurringPaymentFieldsFromResponseToRequestBodyTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/GetRecurringPayments200Response";

	@Before
	public void init() {
		environment.putObject("resource", new JsonObject());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + ".json")
	public void happyPathTestComplete() {
		happyPathTest(true);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "OnlyMandatoryFields.json")
	public void happyPathTestOnlyMandatoryFields() {
		happyPathTest(false);
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya.eyb.c").build());
		unhappyPathTest("Could not parse the body");
	}

	@Test
	public void unhappyPathTestMissingBody() {
		environment.putObject(CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody.RESPONSE_ENV_KEY,
			new JsonObject());
		unhappyPathTest("Could not extract body from response");
	}

	private void happyPathTest(boolean isComplete) {
		run(new CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody());

		Set<String> mandatoryFields = Set.of("endToEndId", "date", "payment", "creditorAccount",
			"cnpjInitiator", "localInstrument", "document");
		Set<String> optionalFields = Set.of("recurringConsentId", "remittanceInformation", "ibgeTownCode", "authorisationFlow",
			"riskSignals", "proxy", "transactionIdentification", "originalRecurringPaymentId", "paymentReference");

		JsonObject paymentRequestData = environment.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonObject();
		Set<String> keySet = paymentRequestData.keySet();
		assertTrue(keySet.containsAll(mandatoryFields));

		if (isComplete) {
			Set<String> allFields = new HashSet<>(mandatoryFields);
			allFields.addAll(optionalFields);
			assertTrue(allFields.containsAll(keySet));
		}
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody());
		assertTrue(error.getMessage().contains(message));
	}
}
