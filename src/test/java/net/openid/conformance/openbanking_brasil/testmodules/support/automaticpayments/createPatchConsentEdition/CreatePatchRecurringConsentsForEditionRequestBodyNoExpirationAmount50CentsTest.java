package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createPatchConsentEdition;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount50Cents;

public class CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount50CentsTest extends AbstractCreatePatchRecurringConsentsForEditionRequestBodyTest {

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody condition() {
		return new CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount50Cents();
	}

	@Override
	protected String getExpectedExpirationDateTime() {
		return null;
	}

	@Override
	protected String getExpectedMaxVariableAmount() {
		return "0.50";
	}
}
