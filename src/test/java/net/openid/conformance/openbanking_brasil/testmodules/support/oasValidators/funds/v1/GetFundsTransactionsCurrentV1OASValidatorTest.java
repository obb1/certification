package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetFundsTransactionsCurrentV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/funds/v1.0.0/transactions.json")
	public void happyPath() {
		run(new GetFundsTransactionsCurrentV1OASValidator());

	}

	@Test
	@UseResurce("jsonResponses/funds/v1.0.0/transactionsNoIncomeTax.json")
	public void unhappyPathNoIncomeTax() {
		ConditionError e = runAndFail(new GetFundsTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("incomeTax is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/funds/v1.0.0/transactionsNoTransactionTypeAdditionalInfo.json")
	public void unhappyPathNoTransactionTypeAdditionalInfo() {
		ConditionError e = runAndFail(new GetFundsTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("transactionTypeAdditionalInfo is required when transactionType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/funds/v1.0.0/transactionsNoExitFee.json")
	public void unhappyPathNoExitFee() {
		ConditionError e = runAndFail(new GetFundsTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("transactionExitFee is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/funds/v1.0.0/transactionsNoNetValue.json")
	public void unhappyPathNoNetValue() {
		ConditionError e = runAndFail(new GetFundsTransactionsCurrentV1OASValidator());
		assertThat(e.getMessage(), containsString("transactionNetValue is required when type is SAIDA"));
	}

}

