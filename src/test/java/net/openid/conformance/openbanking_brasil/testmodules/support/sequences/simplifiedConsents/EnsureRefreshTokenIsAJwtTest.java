package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnsureRefreshTokenIsAJwtTest extends AbstractJsonResponseConditionUnitTest {


	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentGood.json")
	public void happyPath(){
		environment.putString("token_endpoint_response", "refresh_token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");
		EnsureRefreshTokenIsAJwt cond = new EnsureRefreshTokenIsAJwt();
		run(cond);
	}


	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentGood.json")
	public void unhappyPathTokenIsNotJwt(){
		environment.putString("token_endpoint_response", "refresh_token", "value");
		EnsureRefreshTokenIsAJwt cond = new EnsureRefreshTokenIsAJwt();
		ConditionError e = runAndFail(cond);
		assertEquals(e.getMessage(), "EnsureRefreshTokenIsAJwt: Refresh token is not a JWT");
	}

	@Test
	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentGood.json")
	public void unhappyPathTokenIsMissing(){
		environment.putString("token_endpoint_response", "refresh_token", "");
		EnsureRefreshTokenIsAJwt cond = new EnsureRefreshTokenIsAJwt();
		ConditionError e = runAndFail(cond);
		assertEquals(e.getMessage(), "EnsureRefreshTokenIsAJwt: Couldn't find refresh token");
	}
}
