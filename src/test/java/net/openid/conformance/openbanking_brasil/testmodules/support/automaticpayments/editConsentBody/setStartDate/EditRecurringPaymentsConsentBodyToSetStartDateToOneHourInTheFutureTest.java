package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.setStartDate;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate.AbstractEditRecurringPaymentsConsentBodyToSetStartDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate.EditRecurringPaymentsConsentBodyToSetStartDateToOneHourInTheFuture;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class EditRecurringPaymentsConsentBodyToSetStartDateToOneHourInTheFutureTest extends AbstractEditRecurringPaymentsConsentBodyToSetStartDateTest {

	@Override
	protected AbstractEditRecurringPaymentsConsentBodyToSetStartDate condition() {
		return new EditRecurringPaymentsConsentBodyToSetStartDateToOneHourInTheFuture();
	}

	@Override
	protected LocalDateTime expectedDateTime() {
		return ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime().plusHours(1).truncatedTo(ChronoUnit.SECONDS);
	}
}
