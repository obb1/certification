package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureErrorResponseCodeFieldWasContaDebitoDivergenteConsentimentoVinculoTest extends AbstractJsonResponseConditionUnitTest {

	private static final String BASE_JSON_PATH = "jsonResponses/enrollments/v2/PostConsentsAuthorise422Response";

	@Before
	public void init() {
		setJwt(true);
		environment.mapKey("endpoint_response", "resource_endpoint_response_full");
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "ContaDebitoDivergenteConsentimentoVinculo.json")
	public void validateGood422ValorAcimaLimite() {
		run(new EnsureErrorResponseCodeFieldWasContaDebitoDivergenteConsentimentoVinculo());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "StatusVinculoInvalido.json")
	public void validateNotFound422ValorAcimaLimite() {
		ConditionError conditionError = runAndFail(new EnsureErrorResponseCodeFieldWasContaDebitoDivergenteConsentimentoVinculo());
		assertTrue(conditionError.getMessage().contains("Could not find error with expected code in the errors JSON array"));
	}
}
