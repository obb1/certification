package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class PatchPaymentsConsentOASValidatorV4Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/paymentInitiation/pixByPayments/v4/PatchPixConsentValidatorV4Ok.json")
	public void happyPath() {
		run(new PatchPaymentsConsentOASValidatorV4());
	}
}
