package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresentTest extends AbstractJsonResponseConditionUnitTest {

	private static final String KEY = "config";
	private static final String CONSENT_PATH = "resource.brazilPaymentConsent";
	private static final String CPF_PATH = "conditionalResources.brazilCpfJointAccount";
	private static final String CNPJ_PATH = "conditionalResources.brazilCnpjJointAccount";
	private static final String BASE_FILE_PATH = "jsonRequests/automaticPayments/consents/brazilPaymentConsent";
	private static final String CPF = "01234567890";
	private static final String CNPJ = "01234567890123";
	private static final String NAME = "Joao Silva";
	private static final String MISSING_FIELD_MESSAGE = "Could not find brazilPaymentConsent";

	@Before
	public void init() {
		environment.putString(KEY, "resource.creditorName", NAME);
		environment.putBoolean("continue_test", true);
	}

	@Test
	@UseResurce(value = BASE_FILE_PATH + "WithCreditorWithCpf.json", key = KEY, path = CONSENT_PATH)
	public void happyPathTestCpf() {
		happyPathTest(false);
	}

	@Test
	@UseResurce(value = BASE_FILE_PATH + "WithCreditorWithCpf.json", key = KEY, path = CONSENT_PATH)
	public void happyPathTestCnpj() {
		happyPathTest(true);
	}

	@Test
	public void unhappyPathTestMissingResource() {
		environment.putObject(KEY, new JsonObject());
		unhappyPathTestMissingField();
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		environment.putObject(KEY, "resource", new JsonObject());
		unhappyPathTestMissingField();
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(KEY, CONSENT_PATH, new JsonObject());
		unhappyPathTestMissingField();
	}

	@Test
	public void unhappyPathTestMissingBothCpfAndCnpj() {
		EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent condition = new EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Brazil CPF and CNPJ for Joint Account field is empty. Institution is assumed to not have this functionality"));
		assertFalse(environment.getBoolean("continue_test"));
	}

	private void happyPathTest(boolean withCnpj) {
		environment.putString(KEY, CPF_PATH, CPF);
		if (withCnpj) {
			environment.putString(KEY, CNPJ_PATH, CNPJ);
		}
		EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent condition = new EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent();
		run(condition);

		JsonObject data = environment.getElementFromObject(KEY, CONSENT_PATH+".data").getAsJsonObject();

		JsonObject loggedUser = assertObjectFieldIsPresent(data, "loggedUser");
		JsonObject document = assertObjectFieldIsPresent(loggedUser, "document");
		String identification = assertStringFieldIsPresent(document, "identification");
		assertEquals(CPF, identification);
		String rel = assertStringFieldIsPresent(document, "rel");
		assertEquals("CPF", rel);

		assertTrue(data.has("creditors") &&
			data.get("creditors").isJsonArray() &&
			data.getAsJsonArray("creditors").size() == 1 &&
			data.getAsJsonArray("creditors").get(0).isJsonObject());
		JsonObject creditor = data.getAsJsonArray("creditors").get(0).getAsJsonObject();
		String personType = assertStringFieldIsPresent(creditor, "personType");
		String cpfCnpj = assertStringFieldIsPresent(creditor, "cpfCnpj");
		String name = assertStringFieldIsPresent(creditor, "name");
		assertEquals(NAME, name);

		if (withCnpj) {
			JsonObject businessEntity = assertObjectFieldIsPresent(data, "businessEntity");
			JsonObject businessDocument = assertObjectFieldIsPresent(businessEntity, "document");
			String businessIdentification = assertStringFieldIsPresent(businessDocument, "identification");
			assertEquals(CNPJ, businessIdentification);
			String businessRel = assertStringFieldIsPresent(businessDocument, "rel");
			assertEquals("CNPJ", businessRel);

			assertEquals("PESSOA_JURIDICA", personType);
			assertEquals(CNPJ, cpfCnpj);
		} else {
			assertEquals("PESSOA_NATURAL", personType);
			assertEquals(CPF, cpfCnpj);
		}

		assertTrue(environment.getBoolean("continue_test"));
	}

	private void unhappyPathTestMissingField() {
		environment.putString(KEY, CPF_PATH, CPF);
		EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent condition = new EnsureRecurringPaymentsJointAccountCpfOrCnpjIsPresent();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains(MISSING_FIELD_MESSAGE));
		assertTrue(environment.getBoolean("continue_test"));
	}

	private String assertStringFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonPrimitive() && obj.get(field).getAsJsonPrimitive().isString());
		return OIDFJSON.getString(obj.get(field));
	}

	private JsonObject assertObjectFieldIsPresent(JsonObject obj, String field) {
		assertTrue(obj.has(field) && obj.get(field).isJsonObject());
		return obj.getAsJsonObject(field);
	}
}
