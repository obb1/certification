package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.validateVersionHeader;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.validateVersionHeader.AbstractValidateVersionHeader;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public abstract class AbstractValidateVersionHeaderTest extends AbstractJsonResponseConditionUnitTest {

	protected abstract AbstractValidateVersionHeader condition();
	protected abstract String expectedVersion();

	@Test
	public void happyPathTest() {
		setHeaders("x-v", expectedVersion());
		run(condition());
	}

	@Test
	public void unhappyPathTestMissingVersionHeader() {
		unhappyPathTest("Could not find \"x-v\" header in the response");
	}

	@Test
	public void unhappyPathTestUnmatchingVersion() {
		setHeaders("x-v", "1.0");
		unhappyPathTest("The value of the \"x-v\" header does not match the expected version");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(condition());
		assertTrue(error.getMessage().contains(message));
	}
}
