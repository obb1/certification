package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilAutomaticPaymentRecurringConsentProdValues;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class InsertBrazilAutomaticPaymentRecurringConsentProdValuesTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void testWithoutBusinessEntityIdentification() {
		InsertBrazilAutomaticPaymentRecurringConsentProdValues condition = new InsertBrazilAutomaticPaymentRecurringConsentProdValues();
		run(condition);
		assertNull(environment.getString("config", "resource.brazilPaymentConsent.data.businessEntity.document.rel"));
	}

	@Test
	public void testWithBusinessEntityIdentification() {
		environment.putString("config", "resource.brazilCnpj", "12345678901234");
		InsertBrazilAutomaticPaymentRecurringConsentProdValues condition = new InsertBrazilAutomaticPaymentRecurringConsentProdValues();
		run(condition);
		Assert.assertEquals("CNPJ", environment.getString("config", "resource.brazilPaymentConsent.data.businessEntity.document.rel"));
		Assert.assertEquals("12345678901234", environment.getString("config", "resource.brazilPaymentConsent.data.businessEntity.document.identification"));
	}

}
