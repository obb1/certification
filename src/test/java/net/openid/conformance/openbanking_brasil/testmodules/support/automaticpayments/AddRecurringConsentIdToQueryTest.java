package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringConsentIdToQuery;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddRecurringConsentIdToQueryTest extends AbstractJsonResponseConditionUnitTest {

	private static final String CONSENT_ID = "urn:bancoex:C1DD33123";

	@Before
	public void init() {
		environment.putString("protected_resource_url", "https://api.openbanking.uatbi.com.br/open-banking/automatic-payments/v1/pix/recurring-payments");
		environment.putString("consent_id", CONSENT_ID);
	}

	@Test
	public void happyPathTest() {
		AddRecurringConsentIdToQuery condition = new AddRecurringConsentIdToQuery();
		run(condition);

		String url = environment.getString("protected_resource_url");
		assertTrue(url.contains("recurringConsentId="+CONSENT_ID));
	}

	@Test
	public void unhappyPathTest() {
		environment.putString("protected_resource_url", "\\");
		AddRecurringConsentIdToQuery condition = new AddRecurringConsentIdToQuery();
		ConditionError error = runAndFail(condition);
		assertTrue(error.getMessage().contains("Could not"));
	}
}
