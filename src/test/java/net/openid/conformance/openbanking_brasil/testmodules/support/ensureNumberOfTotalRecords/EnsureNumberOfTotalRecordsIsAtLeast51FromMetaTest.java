package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import static org.junit.jupiter.api.Assertions.*;

public class EnsureNumberOfTotalRecordsIsAtLeast51FromMetaTest extends AbstractEnsureNumberOfTotalRecordsFromMetaTest {
	@Override
	protected int getTotalRecordsComparisonAmount() {
		return 51;
	}

	@Override
	protected TotalRecordsComparisonOperator getComparisonMethod() {
		return TotalRecordsComparisonOperator.AT_LEAST;
	}

	@Override
	protected AbstractEnsureNumberOfTotalRecordsFromMeta getCondition() {
		return new EnsureNumberOfTotalRecordsIsAtLeast51FromMeta();
	}
}
