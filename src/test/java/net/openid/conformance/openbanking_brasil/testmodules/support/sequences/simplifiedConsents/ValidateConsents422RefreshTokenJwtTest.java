package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidateConsents422RefreshTokenJwtTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/errors/422/422RefreshTokenJwt.json")
	public void happyPath() {
		ValidateConsents422RefreshTokenJwt cond = new ValidateConsents422RefreshTokenJwt();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/errors/422/good422ConsentErrorResponseV2.json")
	public void unhappyPath() {
		ValidateConsents422RefreshTokenJwt cond = new ValidateConsents422RefreshTokenJwt();
		ConditionError e = runAndFail(cond);
		assertEquals("ValidateConsents422RefreshTokenJwt: Unexpected error code", e.getMessage());
	}

}
