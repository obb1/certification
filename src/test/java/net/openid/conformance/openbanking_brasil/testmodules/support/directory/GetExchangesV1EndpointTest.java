package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetExchangesV1EndpointTest extends AbstractGetXFromAuthServerTest {
	@Override
	protected String getEndpoint() {
		return "https://test.com/open-banking/exchanges/v1/operations";
	}

	@Override
	protected String getApiFamilyType() {
		return "exchanges";
	}

	@Override
	protected String getApiVersion() {
		return "1.0.0";
	}

	@Override
	protected boolean isResource() {
		return true;
	}

	@Override
	protected AbstractGetXFromAuthServer getCondition() {
		return new GetExchangesV1Endpoint();
	}
}

