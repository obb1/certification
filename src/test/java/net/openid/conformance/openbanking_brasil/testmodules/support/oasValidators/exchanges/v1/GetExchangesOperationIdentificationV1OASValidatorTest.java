package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetExchangesOperationIdentificationV1OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/exchanges/identification.json")
	public void happyPath() {
		run(new GetExchangesOperationIdentificationV1OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/exchanges/identificationNoIntermediaryInstitutionName.json")
	public void noIntermediaryInstitutionName() {
		run(new GetExchangesOperationIdentificationV1OASValidator());
	}

}
