package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import static org.junit.jupiter.api.Assertions.*;

public class EnsureNumberOfTotalRecordsIsAtLeast126FromMetaTest extends AbstractEnsureNumberOfTotalRecordsFromMetaTest{
	@Override
	protected int getTotalRecordsComparisonAmount() {
		return 126;
	}

	@Override
	protected TotalRecordsComparisonOperator getComparisonMethod() {
		return TotalRecordsComparisonOperator.AT_LEAST;
	}

	@Override
	protected AbstractEnsureNumberOfTotalRecordsFromMeta getCondition() {
		return new EnsureNumberOfTotalRecordsIsAtLeast126FromMeta();
	}
}
