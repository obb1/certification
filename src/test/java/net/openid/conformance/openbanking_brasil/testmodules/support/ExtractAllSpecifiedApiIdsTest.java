package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class ExtractAllSpecifiedApiIdsTest extends AbstractJsonResponseConditionUnitTest {


	@Before
	public void setupApi(){
		environment.putString("apiIdName", "accountId");
	}

	@Test
	@UseResurce("jsonResponses/account/v2n/accountListResponseV2n.json")
	public void happyPath() {
		ExtractAllSpecifiedApiIds condition = new ExtractAllSpecifiedApiIds();
		run(condition);
		assertThat(
			OIDFJSON.getString(
				environment.getElementFromObject("extracted_api_ids","extractedApiIds")
				.getAsJsonArray()
				.get(0)),
			Matchers.is("92792126019929279212650822221989319252576"));
	}

	@Test
	@UseResurce("jsonResponses/account/v2n/accountListEmptyDataResponseV2n.json")
	public void unhappyPathNoData() {
		ExtractAllSpecifiedApiIds condition = new ExtractAllSpecifiedApiIds();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("No API IDs were extracted"));
	}

	@Test
	public void unhappyPathNoBody() {
		environment.putObject("resource_endpoint_response_full", new JsonObject());
		ExtractAllSpecifiedApiIds condition = new ExtractAllSpecifiedApiIds();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not extract body from response"));
	}

	@Test
	public void unhappyPathUnparseableBody() {
		JsonObject resource = new JsonObject();
		resource.addProperty("body", "ey1.b.c");
		environment.putObject("resource_endpoint_response_full", resource);
		ExtractAllSpecifiedApiIds condition = new ExtractAllSpecifiedApiIds();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString("Could not parse body"));
	}
}
