package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetLoansScheduledInstalmentsV2N4OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/instalments.json")
	public void happyPath() {
		run(new GetLoansScheduledInstalmentsV2n4OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/instalmentsNoTotalNumberOfInstalments.json")
	public void unhappyPathNoTotalNumberOfInstalments() {
		ConditionError e = runAndFail(new GetLoansScheduledInstalmentsV2n4OASValidator());
		assertThat(e.getMessage(), containsString("totalNumberOfInstalments is required when typeNumberOfInstalments is [DIA, SEMANA, MES, ANO]"));
	}

	@Test
	@UseResurce("jsonResponses/loans/v2.3.0/instalmentsNoTotalNumberOfInstalments2.json")
	public void unhappyPathNoTotalNumberOfInstalments2() {
		ConditionError e = runAndFail(new GetLoansScheduledInstalmentsV2n4OASValidator());
		assertThat(e.getMessage(), containsString("totalNumberOfInstalments is required when typeContractRemaining is not SEM_PRAZO_REMANESCENTE"));

	}

}

