package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EnsureOperationalUserIsPresentTest extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		environment.putObject("config", new JsonObject());
	}

	@Test
	public void testShouldContinue() {
		environment.putString("config", "resource.brazilCpfOperational", "random_cpf");
		EnsureOperationalUserIsPresent cond = new EnsureOperationalUserIsPresent();

		run(cond);
	}

	@Test
	public void testShouldNotContinue() {
		EnsureOperationalUserIsPresent cond = new EnsureOperationalUserIsPresent();

		ConditionError error = runAndFail(cond);
		System.out.println(error.getMessage());

		assertTrue(error.getMessage().contains("brazilCpfOperational field is empty"));
	}
}
