package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.createPatchConsentEdition;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.AbstractCreatePatchRecurringConsentsForEditionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition.CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount30Cents;

public class CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount30CentsTest extends AbstractCreatePatchRecurringConsentsForEditionRequestBodyTest {

	@Override
	protected AbstractCreatePatchRecurringConsentsForEditionRequestBody condition() {
		return new CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount30Cents();
	}

	@Override
	protected String getExpectedExpirationDateTime() {
		return null;
	}

	@Override
	protected String getExpectedMaxVariableAmount() {
		return "0.30";
	}
}
