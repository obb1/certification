package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EnsureNonRedirectPaymentsJointAccountCpfOrCnpjIsPresentTest extends AbstractJsonResponseConditionUnitTest {

	private static final String LOGGED_USER_CPF = "loggedUserCpf";
	private static final String BUSINESS_ENTITY_CNPJ = "businessEntityCnpj";
	private static final String JOINT_ACCOUNT_CPF = "jointAccountCpf";
	private static final String JOINT_ACCOUNT_CNPJ = "jointAccountCnpj";

	@Before
	public void init() {
		JsonObject config = new JsonObjectBuilder()
			.addFields("resource", Map.of(
				"loggedUserIdentification", LOGGED_USER_CPF,
				"businessEntityIdentification", BUSINESS_ENTITY_CNPJ
			))
			.addField("resource.brazilPaymentConsent.data", new JsonObject())
			.addFields("conditionalResources", Map.of(
				"brazilCpfJointAccount", JOINT_ACCOUNT_CPF,
				"brazilCnpjJointAccount", JOINT_ACCOUNT_CNPJ
			))
			.build();
		environment.putObject("config", config);
		environment.putBoolean("continue_test", true);
	}

	@Test
	public void happyPathTestOnlyCpf() {
		removeConditionalResource("brazilCnpjJointAccount");

		JsonObject data = happyPathTest();

		verifyDocument(data, "loggedUser", "CPF", JOINT_ACCOUNT_CPF);
		verifyIdentification("loggedUserIdentification", JOINT_ACCOUNT_CPF);
		assertFalse(data.has("business_entity"));
		verifyIdentification("businessEntityIdentification", BUSINESS_ENTITY_CNPJ);
	}

	@Test
	public void happyPathTestOnlyCnpj() {
		removeConditionalResource("brazilCpfJointAccount");

		JsonObject data = happyPathTest();

		assertFalse(data.has("loggedUser"));
		verifyIdentification("loggedUserIdentification", LOGGED_USER_CPF);
		verifyDocument(data, "businessEntity", "CNPJ", JOINT_ACCOUNT_CNPJ);
		verifyIdentification("businessEntityIdentification", JOINT_ACCOUNT_CNPJ);
	}

	@Test
	public void happyPathTestBothCpfAndCnpj() {
		JsonObject data = happyPathTest();

		verifyDocument(data, "loggedUser", "CPF", JOINT_ACCOUNT_CPF);
		verifyIdentification("loggedUserIdentification", JOINT_ACCOUNT_CPF);
		verifyDocument(data, "businessEntity", "CNPJ", JOINT_ACCOUNT_CNPJ);
		verifyIdentification("businessEntityIdentification", JOINT_ACCOUNT_CNPJ);
	}

	@Test
	public void unhappyPathTestMissingBothCpfAndCnpj() {
		removeConditionalResource("brazilCpfJointAccount");
		removeConditionalResource("brazilCnpjJointAccount");

		unhappyPathTest("Brazil CPF and CNPJ for Joint Account field is empty. Institution is assumed to not have this functionality");
		assertFalse(environment.getBoolean("continue_test"));
	}

	@Test
	public void unhappyPathTestMissingConsent() {
		JsonObject resource = environment.getElementFromObject("config", "resource").getAsJsonObject();
		resource.remove("brazilPaymentConsent");

		unhappyPathTest("Could not find brazilPaymentConsent");
	}

	private JsonObject happyPathTest() {
		run(new EnsureNonRedirectPaymentsJointAccountCpfOrCnpjIsPresent());
		assertTrue(environment.getBoolean("continue_test"));
		return environment
			.getElementFromObject("config", "resource.brazilPaymentConsent.data")
			.getAsJsonObject();
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new EnsureNonRedirectPaymentsJointAccountCpfOrCnpjIsPresent());
		assertTrue(error.getMessage().contains(message));
	}

	private void removeConditionalResource(String resourceKey) {
		JsonObject conditionalResources = environment.getElementFromObject("config", "conditionalResources").getAsJsonObject();
		conditionalResources.remove(resourceKey);
	}

	private void verifyDocument(JsonObject data, String documentType, String expectedRel, String expectedIdentification) {
		JsonObject document = data.getAsJsonObject(documentType).getAsJsonObject("document");
		assertEquals(expectedRel, OIDFJSON.getString(document.get("rel")));
		assertEquals(expectedIdentification, OIDFJSON.getString(document.get("identification")));
	}

	private void verifyIdentification(String identificationType, String expectedIdentificationValue) {
		String identificationValue = OIDFJSON.getString(environment.getElementFromObject("config", "resource." + identificationType));
		assertEquals(expectedIdentificationValue, identificationValue);
	}
}
