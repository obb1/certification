package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountOverdraft.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

public class GetUnarrangedAccountOverdraftOASValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	@Before
	public void setUp() {
		JsonObject response = environment.getObject(OpenAPIJsonSchemaValidator.ResponseEnvKey.FullResponseEnvKey.getKey());
		response.addProperty("status", 200);
	}

	@Test
	@UseResurce("jsonResponses/productsNServices/v1/unarrangedAccountOverdraft/businessUnarrangedAccountOverdraftResponseOK.json")
	public void validateBusinessUnarranged() {
		GetUnarrangedAccountBusinessOverdraftOASValidatorV1 condition = new GetUnarrangedAccountBusinessOverdraftOASValidatorV1();
		run(condition);
	}

	@Test
	@UseResurce("jsonResponses/productsNServices/v1/unarrangedAccountOverdraft/personalUnarrangedAccountOverdraftResponseOK.json")
	public void validatePersonalUnarranged() {
		GetUnarrangedAccountPersonalOverdraftOASValidatorV1 condition = new GetUnarrangedAccountPersonalOverdraftOASValidatorV1();
		run(condition);
	}
}
