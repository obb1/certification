package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreTwoRjctPayments;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class EnsureThereAreTwoRjctPaymentsTest extends AbstractEnsureThereArePaymentsTest {

	@Override
	protected AbstractEnsureThereArePayments condition() {
		return new EnsureThereAreTwoRjctPayments();
	}

	@Override
	@Test
	@UseResurce(BASE_JSON_PATH + "TwoRjct.json")
	public void happyPathTest() {
		super.happyPathTest();
	}
}
