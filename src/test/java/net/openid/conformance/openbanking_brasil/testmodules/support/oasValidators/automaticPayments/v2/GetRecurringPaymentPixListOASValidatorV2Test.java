package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GetRecurringPaymentPixListOASValidatorV2Test extends AbstractJsonResponseConditionUnitTest {

	protected static final String BASE_JSON_PATH = "jsonResponses/automaticpayments/v2/payments/";

	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringPaymentsList200Response.json")
	public void happyPathTest() {
		run(new GetRecurringPaymentPixListOASValidatorV2());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringPaymentsList200ResponseMissingOptionalFields.json")
	public void happyPathTestMissingOptionalFields() {
		run(new GetRecurringPaymentPixListOASValidatorV2());
	}

	@Test
	@UseResurce(BASE_JSON_PATH + "GetRecurringPaymentsList200ResponseRJCTMissingRejectionReason.json")
	public void unhappyPathTestRJCTMissingRejectionReason() {
		unhappyPathTest("rejectionReason is required when status is RJCT");
	}

	protected void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new GetRecurringPaymentPixListOASValidatorV2());
		assertTrue(error.getMessage().contains(message));
	}
}
