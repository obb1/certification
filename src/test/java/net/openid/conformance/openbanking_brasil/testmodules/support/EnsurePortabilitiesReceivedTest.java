package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EnsurePortabilitiesReceivedTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/naturalPersonalRelationshipV2/naturalPersonRelationshipResponse-V2.1.0.json")
	public void testHappyPath() {
		EnsurePortabilitiesReceived cond = new EnsurePortabilitiesReceived();
		run(cond);
	}

	@Test
	@UseResurce("jsonResponses/registrationData/registrationDataV2/naturalPersonalRelationshipV2/naturalPersonRelationshipResponseWithoutPortabilities-V2.1.0.json")
	public void testUnhappyPath() {
		EnsurePortabilitiesReceived cond = new EnsurePortabilitiesReceived();

		ConditionError error = runAndFail(cond);
		assertThat(error.getMessage(), containsString("portabilitiesReceived is empty"));
	}
}
