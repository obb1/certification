package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostFidoRegistrationOptionsOASValidatorV2Test extends AbstractEnrollmentsOASV2ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2 validator() {
		return new PostFidoRegistrationOptionsOASValidatorV2();
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoRegistrationOptions201Response.json")
	public void happyPathTest201() {
		happyPathTest(201);
	}

	@Test
	@UseResurce(JSON_PATH + "PostFidoRegistrationOptions422Response.json")
	public void happyPathTest422() {
		happyPathTest(422);
	}
}
