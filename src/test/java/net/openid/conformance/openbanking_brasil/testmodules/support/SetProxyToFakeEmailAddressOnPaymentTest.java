package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SetProxyToFakeEmailAddressOnPaymentTest extends AbstractJsonResponseConditionUnitTest {
	private static final String KEY = "resource";
	private static final String PATH = "brazilPixPayment";
	private static final String expectedProxy = "fakeperson@example.com";

	@Test
	@UseResurce(value="jsonRequests/automaticPayments/payments/brazilPaymentWithCpf.json", key=KEY, path=PATH)
	public void happyPathSingleTest() {
		SetProxyToFakeEmailAddressOnPayment condition = new SetProxyToFakeEmailAddressOnPayment();
		run(condition);

		JsonElement proxy = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonObject("data").get("proxy");
		assertNotNull(proxy);
		assertEquals(expectedProxy, OIDFJSON.getString(proxy));
	}

	@Test
	@UseResurce(value="jsonRequests/payments/payments/paymentWithDataArrayWith5Payments.json", key=KEY, path=PATH)
	public void happyPathArrayTest() {
		SetProxyToFakeEmailAddressOnPayment condition = new SetProxyToFakeEmailAddressOnPayment();
		run(condition);

		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {

			JsonElement proxy = data.get(i).getAsJsonObject().get("proxy");
			assertNotNull(proxy);
			assertEquals(expectedProxy, OIDFJSON.getString(proxy));
		}
	}
}
