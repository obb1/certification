package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GetBankFixedIncomesTransactionsOASValidatorV1Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/transactions.json")
	public void happyPath() {
		run(new GetBankFixedIncomesTransactionsCurrentOASValidatorV1());
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/transactionsNoAdditionalInfo.json")
	public void noAdditionalInfo() {
		ConditionError e = runAndFail(new GetBankFixedIncomesTransactionsCurrentOASValidatorV1());
		assertTrue(e.getMessage().contains("transactionTypeAdditionalInfo is required when transactionType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/transactionsNoFinancialTax.json")
	public void noFinancialTax() {
		ConditionError e = runAndFail(new GetBankFixedIncomesTransactionsCurrentOASValidatorV1());
		assertTrue(e.getMessage().contains("financialTransactionTax is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/transactionsNoIncomeTax.json")
	public void noIncomeTax() {
		ConditionError e = runAndFail(new GetBankFixedIncomesTransactionsCurrentOASValidatorV1());
		assertTrue(e.getMessage().contains("incomeTax is required when type is SAIDA"));
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/transactionsNoIndexerPercentage.json")
	public void noIndexerPercentage() {
		ConditionError e = runAndFail(new GetBankFixedIncomesTransactionsCurrentOASValidatorV1());
		assertTrue(e.getMessage().contains("indexerPercentage is required when type is ENTRADA"));
	}

	@Test
	@UseResurce("jsonResponses/bankFixedIncomes/transactionsNoRemunerationTransactionRate.json")
	public void noTransactionRate() {
		ConditionError e = runAndFail(new GetBankFixedIncomesTransactionsCurrentOASValidatorV1());
		assertTrue(e.getMessage().contains("remunerationTransactionRate is required when type is ENTRADA"));
	}
}
