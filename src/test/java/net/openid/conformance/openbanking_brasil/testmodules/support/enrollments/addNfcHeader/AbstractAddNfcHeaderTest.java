package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.addNfcHeader;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AbstractAddNfcHeaderTest extends AbstractJsonResponseConditionUnitTest {

	private static final String HEADERS_KEY = "resource_endpoint_request_headers";
	private static final String NFC_HEADER_KEY = "x-bcb-nfc";

	@Test
	public void happyPathTestTrue() {
		happyPathTest(true, true);
	}

	@Test
	public void happyPathTestFalse() {
		happyPathTest(false, true);
	}

	@Test
	public void happyPathTestTrueMissingHeaders() {
		happyPathTest(true, false);
	}

	@Test
	public void happyPathTestFalseMissingHeaders() {
		happyPathTest(false, false);
	}

	private void happyPathTest(boolean isTrue, boolean addHeaders) {
		if (addHeaders) {
			environment.putObject(HEADERS_KEY, new JsonObjectBuilder().addField("x-fapi-interaction-id", "123").build());
		}

		AbstractAddNfcHeader condition = isTrue ? new AddNfcHeaderAsTrue() : new AddNfcHeaderAsFalse();

		run(condition);
		JsonObject headers = environment.getObject(HEADERS_KEY);
		assertEquals(OIDFJSON.getString(headers.get(NFC_HEADER_KEY)), String.valueOf(isTrue));

		if (!addHeaders) {
			assertEquals(1, headers.size());
		}
	}

}
