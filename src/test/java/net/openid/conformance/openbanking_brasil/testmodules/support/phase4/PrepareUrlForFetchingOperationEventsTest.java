package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PrepareUrlForFetchingOperationEventsTest extends AbstractJsonResponseConditionUnitTest {
	@UseResurce("jsonResponses/exchanges/OperationsListResponse.json")
	@Test
	public void happyPath() {
		environment.putString("config", "resource.resourceUrl", "https://matls-api.mockbank.poc.raidiam.io/open-banking/exchanges/v1/operations");
		environment.putString("operationId", "1234567890");
		PrepareUrlForFetchingOperationEvents condition = new PrepareUrlForFetchingOperationEvents();
		run(condition);

		assertTrue(environment.getString("protected_resource_url").matches("^(https://)(.*?)(exchanges/v[0-9])/operations/1234567890/events"));
	}
}
