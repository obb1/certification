package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editPaymentBody.paymentReference;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForAnualPayment;
import org.junit.Test;

public class SetPaymentReferenceForAnualPaymentTest extends AbstractSetPaymentReferenceTest {

	@Override
	protected AbstractSetPaymentReference condition() {
		return new SetPaymentReferenceForAnualPayment();
	}

	@Test
	public void happyPathTest() {
		happyPathTest("2024-01-01", "Y2024");
	}
}
