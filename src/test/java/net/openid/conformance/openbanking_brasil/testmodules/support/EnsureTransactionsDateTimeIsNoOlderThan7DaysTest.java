
package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class EnsureTransactionsDateTimeIsNoOlderThan7DaysTest extends AbstractJsonResponseConditionUnitTest {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsDateTimeCurrentGood.json")
	@Test
	public void happyPath() {
		JsonObject transaction = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject();
		LocalDateTime currentDate = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		transaction.addProperty("transactionDateTime", currentDate.format(FORMATTER));

		EnsureTransactionsDateTimeIsNoOlderThan7Days cond = new EnsureTransactionsDateTimeIsNoOlderThan7Days();
		run(cond);
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsDateTimeCurrentGood.json")
	@Test
	public void unhappyPath() {
		EnsureTransactionsDateTimeIsNoOlderThan7Days cond = new EnsureTransactionsDateTimeIsNoOlderThan7Days();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("Transaction is older than 7 days or in the future"));
	}
	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsDateTimeCurrentGood.json")
	@Test
	public void unhappyOneWeekInThePast() {

		JsonObject transaction = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject();
		LocalDateTime currentDate = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		transaction.addProperty("transactionDateTime", currentDate.minusDays(8).format(FORMATTER));

		EnsureTransactionsDateTimeIsNoOlderThan7Days cond = new EnsureTransactionsDateTimeIsNoOlderThan7Days();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("Transaction is older than 7 days or in the future"));
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsDateTimeCurrentGood.json")
	@Test
	public void unhappyFutureDate() {

		JsonObject transaction = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject();
		LocalDateTime currentDate = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		transaction.addProperty("transactionDateTime", currentDate.plusHours(25).format(FORMATTER));

		EnsureTransactionsDateTimeIsNoOlderThan7Days cond = new EnsureTransactionsDateTimeIsNoOlderThan7Days();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("Transaction is older than 7 days or in the future"));
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentEmptyDataGood.json")
	@Test
	public void unhappyPathEmptyData() {
		EnsureTransactionsDateTimeIsNoOlderThan7Days cond = new EnsureTransactionsDateTimeIsNoOlderThan7Days();
		run(cond);
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentInvalidData.json")
	@Test
	public void unhappyPathInvalidData() {
		EnsureTransactionsDateTimeIsNoOlderThan7Days cond = new EnsureTransactionsDateTimeIsNoOlderThan7Days();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("Could not parse the value of the transaction date field"));
	}
}
