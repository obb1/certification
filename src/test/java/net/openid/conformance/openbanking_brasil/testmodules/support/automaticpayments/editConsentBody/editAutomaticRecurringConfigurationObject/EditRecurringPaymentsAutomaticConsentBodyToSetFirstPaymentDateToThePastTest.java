package net.openid.conformance.openbanking_brasil.testmodules.support.automaticpayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.AbstractEditRecurringPaymentsAutomaticConsentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject.EditRecurringPaymentsAutomaticConsentBodyToSetFirstPaymentDateToThePast;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class EditRecurringPaymentsAutomaticConsentBodyToSetFirstPaymentDateToThePastTest extends AbstractEditRecurringPaymentsAutomaticConsentBodyTest {

	@Override
	protected AbstractEditRecurringPaymentsAutomaticConsentBody condition() {
		return new EditRecurringPaymentsAutomaticConsentBodyToSetFirstPaymentDateToThePast();
	}

	@Override
	protected void assertAutomaticObject(JsonObject automatic) {
		String date = OIDFJSON.getString(automatic.getAsJsonObject("firstPayment").get("date"));
		LocalDate yesterday = LocalDate.now(ZoneId.of("UTC-3")).minusDays(1);
		String expectedDate = yesterday.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		assertEquals(expectedDate, date);
	}

	@Test
	@UseResurce(value = JSON_BASE_PATH + "WithAutomaticRecurringConfigurationMissingFirstPayment.json", key = KEY, path = PATH)
	public void unhappyPathTestMissingFirstPayment() {
		unhappyPathTest("Unable to find firstPayment inside automatic object");
	}
}
