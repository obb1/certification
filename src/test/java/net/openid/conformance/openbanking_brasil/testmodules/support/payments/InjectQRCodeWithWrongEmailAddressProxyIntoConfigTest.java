package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InjectQRCodeWithWrongEmailAddressProxyIntoConfigTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce(value="test_resource_payments_v4_single.json", key="resource")
	public void happyPathSingleTest() {
		InjectQRCodeWithWrongEmailAddressProxyIntoConfig condition = new InjectQRCodeWithWrongEmailAddressProxyIntoConfig();
		run(condition);

		JsonElement consentPayment = environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment");
		String consentAmount = OIDFJSON.getString(consentPayment.getAsJsonObject().get("amount"));
		String consentQrCode = OIDFJSON.getString(consentPayment.getAsJsonObject().getAsJsonObject("details").get("qrCode"));
		String paymentAmount = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.payment.amount"));
		String paymentQrCode = OIDFJSON.getString(environment.getElementFromObject("resource", "brazilPixPayment.data.payment.qrCode"));

		assertEquals(consentAmount, paymentAmount);
		assertEquals(consentQrCode, paymentQrCode);
	}

	@Test
	@UseResurce(value="test_resource_payments_v4.json", key="resource")
	public void happyPathArrayTest(){
		InjectQRCodeWithWrongEmailAddressProxyIntoConfig condition = new InjectQRCodeWithWrongEmailAddressProxyIntoConfig();
		run(condition);

		JsonElement consentPayment = environment.getElementFromObject("resource", "brazilPaymentConsent.data.payment");
		String consentAmount = OIDFJSON.getString(consentPayment.getAsJsonObject().get("amount"));
		String consentQrCode = OIDFJSON.getString(consentPayment.getAsJsonObject().getAsJsonObject("details").get("qrCode"));


		JsonArray data = environment.getObject("resource").getAsJsonObject("brazilPixPayment").getAsJsonArray("data");

		for (int i = 0 ; i < data.size(); i++) {
			String paymentAmount = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("payment").get("amount"));
			String paymentQrCode = OIDFJSON.getString(data.get(i).getAsJsonObject().getAsJsonObject("payment").get("qrCode"));
			assertEquals(consentAmount, paymentAmount);
			assertEquals(consentQrCode, paymentQrCode);
		}
	}
}
