package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SaveCurrentInstantTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void happyPathTest() {
		run(new SaveCurrentInstant());
		String savedInstantStr = environment.getString("saved_instant");
		Instant savedInstant = Instant.parse(savedInstantStr);
		assertFalse(Instant.now().isBefore(savedInstant));
		assertTrue(savedInstantStr.matches("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z"));
	}
}
