package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;

public class GetEnrollmentsOASValidatorV2Test extends AbstractEnrollmentsOASV2ValidatorsTest {

	@Override
	protected AbstractEnrollmentsOASValidatorV2 validator() {
		return new GetEnrollmentsOASValidatorV2();
	}

	@Before
	public void init() {
		generateGetEnrollments200Response();
	}

	@Test
	public void happyPathTest200() {
		happyPathTest(200);
	}

	@Test
	public void happyPathTest200MissingSituationalFields() {
		removeSituationalFields();
		happyPathTest(200);
	}

	@Test
	@UseResurce(JSON_PATH + "GenericErrorResponse.json")
	public void happyPathTest401() {
		happyPathTest(401);
	}

	@Test
	public void unhappyPathTest200NoIssuerCacc() {
		removeIssuerAndSetAccountType("CACC");
		unhappyPathTest(200, "issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	public void unhappyPathTest200NoIssuerSvgs() {
		removeIssuerAndSetAccountType("SVGS");
		unhappyPathTest(200, "issuer is required when accountType is [CACC, SVGS]");
	}

	@Test
	public void unhappyPathTest200CreationDateTimeTooFarInThePast() {
		editTimeToBeTooFar("creationDateTime", true);
		unhappyPathTest(200, "The creationDateTime field value is too far from the current instant in UTC time");
	}

	@Test
	public void unhappyPathTest200CreationDateTimeTooFarInTheFuture() {
		editTimeToBeTooFar("creationDateTime", false);
		unhappyPathTest(200, "The creationDateTime field value is too far from the current instant in UTC time");
	}

	@Test
	public void unhappyPathTest200StatusUpdateDateTimeTooFarInThePast() {
		editTimeToBeTooFar("statusUpdateDateTime", true);
		unhappyPathTest(200, "The statusUpdateDateTime field value is too far from the current instant in UTC time");
	}

	@Test
	public void unhappyPathTest200StatusUpdateDateTimeTooFarInTheFuture() {
		editTimeToBeTooFar("statusUpdateDateTime", false);
		unhappyPathTest(200, "The statusUpdateDateTime field value is too far from the current instant in UTC time");
	}

	@Test
	public void unhappyPathTest200NoDebtorAccountAwaitingEnrollment() {
		removeDebtorAccountAndSetEnrollmentStatus("AWAITING_ENROLLMENT");
		unhappyPathTest(200, "debtorAccount is required when status is [AWAITING_ENROLLMENT, AUTHORISED, REVOKED]");
	}

	@Test
	public void unhappyPathTest200NoDebtorAccountAuthorised() {
		removeDebtorAccountAndSetEnrollmentStatus("AUTHORISED");
		unhappyPathTest(200, "debtorAccount is required when status is [AWAITING_ENROLLMENT, AUTHORISED, REVOKED]");
	}

	@Test
	public void unhappyPathTest200NoDebtorAccountRevoked() {
		removeDebtorAccountAndSetEnrollmentStatus("REVOKED");
		unhappyPathTest(200, "debtorAccount is required when status is [AWAITING_ENROLLMENT, AUTHORISED, REVOKED]");
	}

	@Test
	public void unhappyPathTest200NoTransactionLimitAwaitingEnrollment() {
		removeLimitAndSetEnrollmentStatus("transactionLimit", "AWAITING_ENROLLMENT");
		unhappyPathTest(200, "transactionLimit is required when status is [AWAITING_ENROLLMENT, AUTHORISED]");
	}

	@Test
	public void unhappyPathTest200NoTransactionLimitAuthorised() {
		removeLimitAndSetEnrollmentStatus("transactionLimit", "AUTHORISED");
		unhappyPathTest(200, "transactionLimit is required when status is [AWAITING_ENROLLMENT, AUTHORISED]");
	}

	@Test
	public void unhappyPathTest200NoDailyLimitAwaitingEnrollment() {
		removeLimitAndSetEnrollmentStatus("dailyLimit", "AWAITING_ENROLLMENT");
		unhappyPathTest(200, "dailyLimit is required when status is [AWAITING_ENROLLMENT, AUTHORISED]");
	}

	@Test
	public void unhappyPathTest200NoDailyLimitAuthorised() {
		removeLimitAndSetEnrollmentStatus("dailyLimit", "AUTHORISED");
		unhappyPathTest(200, "dailyLimit is required when status is [AWAITING_ENROLLMENT, AUTHORISED]");
	}

	@Test
	public void happyPathTest200NoBusinessEntityAndLowTransactionLimit() {
		removeBusinessEntityAndSetTransactionLimit("500.00");
		happyPathTest(200);
	}

	@Test
	public void unhappyPathTest200NoBusinessEntityAndHighTransactionLimit() {
		removeBusinessEntityAndSetTransactionLimit("500.01");
		unhappyPathTest(200, "If businessEntity is not in the payload, transactionLimit is limited to 500 BRL");
	}

	protected void generateGetEnrollments200Response() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneOffset.UTC);
		Instant currentTime = Instant.now();
		String currentTimeStr = formatter.format(currentTime);
		String expirationDateTimeStr = formatter.format(currentTime.plusSeconds(300));
		JsonArray permissions = new JsonArray();
		permissions.add("PAYMENTS_INITIATE");

		JsonObject response = new JsonObjectBuilder()
			.addFields("data", Map.of(
				"enrollmentId", "urn:bancoex:C1DD33123",
				"creationDateTime", currentTimeStr,
				"status", "AWAITING_RISK_SIGNALS",
				"statusUpdateDateTime", currentTimeStr,
				"permissions", permissions,
				"expirationDateTime", expirationDateTimeStr,
				"transactionLimit", "100000.12",
				"dailyLimit", "100000.12",
				"enrollmentName", "Nome Dispositivo"
			))
			.addFields("data.loggedUser.document", Map.of(
				"identification", "11111111111",
				"rel", "CPF"
			))
			.addFields("data.businessEntity.document", Map.of(
				"identification", "11111111111111",
				"rel", "CNPJ"
			))
			.addFields("data.debtorAccount", Map.of(
				"ispb", "12345678",
				"issuer", "1774",
				"number", "1234567890",
				"accountType", "CACC"
			))
			.addFields("data.cancellation.cancelledBy.document", Map.of(
				"identification", "11111111111",
				"rel", "CPF"
			))
			.addFields("data.cancellation", Map.of(
				"additionalInformation", "Contrato entre iniciadora e detentora interrompido",
				"cancelledFrom", "INICIADORA",
				"rejectedAt", currentTimeStr
			))
			.addField("data.cancellation.reason.rejectionReason", "REJEITADO_TEMPO_EXPIRADO_RISK_SIGNALS")
			.addField("links.self", "https://api.banco.com.br/open-banking/api/v2/resource")
			.addField("meta.requestDateTime", currentTimeStr)
			.build();

		addResponseToEnv(response);
	}

	private void addResponseToEnv(JsonObject response) {
		environment.putString("resource_endpoint_response_full", "body", response.toString());
	}

	private JsonObject extractResponseFromEnv() {
		return JsonParser.parseString(environment.getString("resource_endpoint_response_full", "body")).getAsJsonObject();
	}

	private void removeSituationalFields() {
		JsonObject response = extractResponseFromEnv();
		JsonObject data = response.getAsJsonObject("data");
		data.remove("expirationDateTime");
		data.remove("businessEntity");
		data.remove("debtorAccount");
		data.remove("cancellation");
		data.remove("transactionLimit");
		data.remove("dailyLimit");
		data.remove("enrollmentName");
		addResponseToEnv(response);
	}

	private void removeIssuerAndSetAccountType(String accountType) {
		JsonObject response = extractResponseFromEnv();
		JsonObject debtorAccount = response.getAsJsonObject("data").getAsJsonObject("debtorAccount");
		debtorAccount.remove("issuer");
		debtorAccount.addProperty("accountType", accountType);
		addResponseToEnv(response);
	}

	private void editTimeToBeTooFar(String dateTimeField, boolean isPast) {
		JsonObject response = extractResponseFromEnv();
		JsonObject data = response.getAsJsonObject("data");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneOffset.UTC);
		Instant currentTime = Instant.now();
		Instant dateTime = isPast ? currentTime.minus(2, ChronoUnit.HOURS) : currentTime.plus(2, ChronoUnit.HOURS);
		data.addProperty(dateTimeField, formatter.format(dateTime));
		addResponseToEnv(response);
	}

	private void removeDebtorAccountAndSetEnrollmentStatus(String status) {
		JsonObject response = extractResponseFromEnv();
		JsonObject data = response.getAsJsonObject("data");
		data.remove("debtorAccount");
		data.addProperty("status", status);
		addResponseToEnv(response);
	}

	private void removeLimitAndSetEnrollmentStatus(String limit, String status) {
		JsonObject response = extractResponseFromEnv();
		JsonObject data = response.getAsJsonObject("data");
		data.remove(limit);
		data.addProperty("status", status);
		addResponseToEnv(response);
	}

	private void removeBusinessEntityAndSetTransactionLimit(String transactionLimit) {
		JsonObject response = extractResponseFromEnv();
		JsonObject data = response.getAsJsonObject("data");
		data.remove("businessEntity");
		data.addProperty("transactionLimit", transactionLimit);
		addResponseToEnv(response);
	}
}
