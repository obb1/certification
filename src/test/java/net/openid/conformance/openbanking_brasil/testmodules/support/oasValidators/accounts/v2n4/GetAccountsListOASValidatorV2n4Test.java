package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class GetAccountsListOASValidatorV2n4Test extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/account/v2n/accountListResponseV2n.json")
	public void happyPath() {
		run(new GetAccountsListOASValidatorV2n4());
	}

	@Test
	@UseResurce("jsonResponses/account/v2n/accountListResponseV2nNoBranchCode.json")
	public void noBranchCode() {
		ConditionError e = runAndFail(new GetAccountsListOASValidatorV2n4());
		assertTrue(e.getMessage().contains("branchCode is required when type is not CONTA_PAGAMENTO_PRE_PAGA"));
	}
}
