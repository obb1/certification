package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class EnsureTransactionsDateTimeIsSetToTodayTest extends AbstractJsonResponseConditionUnitTest {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsDateTimeCurrentGood.json")
	@Test
	public void happyPath() {
		JsonObject transaction = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject();
		LocalDateTime currentDate = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		transaction.addProperty("transactionDateTime", currentDate.format(FORMATTER));

		EnsureTransactionsDateTimeIsSetToToday cond = new EnsureTransactionsDateTimeIsSetToToday();
		run(cond);
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsDateTimeCurrentGood.json")
	@Test
	public void unhappyPath() {
		EnsureTransactionsDateTimeIsSetToToday cond = new EnsureTransactionsDateTimeIsSetToToday();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("The dates of the transactions are not today's date"));
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentEmptyDataGood.json")
	@Test
	public void unhappyPathEmptyData() {
		EnsureTransactionsDateTimeIsSetToToday cond = new EnsureTransactionsDateTimeIsSetToToday();
		run(cond);
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentInvalidData.json")
	@Test
	public void unhappyPathInvalidData() {
		EnsureTransactionsDateTimeIsSetToToday cond = new EnsureTransactionsDateTimeIsSetToToday();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("Could not parse the value of the transaction date field"));
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentInvalidData.json")
	@Test
	public void unhappyDifferentDayData() {
		JsonObject transaction = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject();
		LocalDateTime currentDate = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		transaction.remove("transactionDateTime");
		transaction.addProperty("transactionDateTime", currentDate.plusDays(1).format(FORMATTER));
		EnsureTransactionsDateTimeIsSetToToday cond = new EnsureTransactionsDateTimeIsSetToToday();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("The dates of the transactions are not today's date"));
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentInvalidData.json")
	@Test
	public void unhappyDifferentMonthData() {
		JsonObject transaction = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject();
		LocalDateTime currentDate = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		transaction.remove("transactionDateTime");
		transaction.addProperty("transactionDateTime", currentDate.plusDays(32).format(FORMATTER));
		EnsureTransactionsDateTimeIsSetToToday cond = new EnsureTransactionsDateTimeIsSetToToday();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("The dates of the transactions are not today's date"));
	}

	@UseResurce("jsonResponses/account/accountV2/transactionscurrentV2/accountsTransactionsCurrentInvalidData.json")
	@Test
	public void unhappyDifferentYearData() {
		JsonObject transaction = jsonObject.getAsJsonArray("data").get(0).getAsJsonObject();
		LocalDateTime currentDate = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		transaction.remove("transactionDateTime");
		transaction.addProperty("transactionDateTime", currentDate.plusDays(366).format(FORMATTER));
		EnsureTransactionsDateTimeIsSetToToday cond = new EnsureTransactionsDateTimeIsSetToToday();
		ConditionError conditionError = runAndFail(cond);
		assertThat(conditionError.getMessage(), containsString("The dates of the transactions are not today's date"));
	}

}
