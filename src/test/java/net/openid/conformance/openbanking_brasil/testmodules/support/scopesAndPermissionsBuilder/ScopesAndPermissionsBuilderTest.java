package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder;
import com.google.gson.JsonElement;

import com.google.gson.JsonObject;
import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFGroupingEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.OIDFJSON;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;


public class ScopesAndPermissionsBuilderTest  extends AbstractJsonResponseConditionUnitTest {


	@Before
	public void setEnv(){
		JsonObject client = new JsonObject();
		environment.putObject("client", client);
	}

	@Test
	public void addPermissionsBasedOnGroupingTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissionsBasedOnGrouping(OPFGroupingEnum.ACCOUNTS_BALANCES);
		permissionsBuilder.addPermissionsBasedOnGrouping(OPFGroupingEnum.PERSONAL_REGISTRATION_DATA);
		permissionsBuilder.build();
		List<String> permissions = Arrays.asList(environment.getString("consent_permissions").split(" "));
		assertTrue(
			permissions.contains("ACCOUNTS_READ") &&
			permissions.contains("RESOURCES_READ") &&
			permissions.contains("ACCOUNTS_BALANCES_READ") &&
			permissions.contains("CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ")
		);
	}

	@Test
	public void addPermissionsBasedOnCategoryTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA);
		permissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.EXCHANGES);
		permissionsBuilder.build();
		List<String> permissions = Arrays.asList(environment.getString("consent_permissions").split(" "));
		assertTrue(
			permissions.contains("RESOURCES_READ") &&
			permissions.contains("CUSTOMERS_PERSONAL_ADITTIONALINFO_READ") &&
			permissions.contains("CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ") &&
				permissions.contains("EXCHANGES_READ")
		);
	}

	@Test
	public void addScopesBasedOnGroupingHappyPathTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addScopesBasedOnGrouping(OPFGroupingEnum.PERSONAL_REGISTRATION_DATA);
		permissionsBuilder.addScopesBasedOnGrouping(OPFGroupingEnum.EXCHANGES);
		permissionsBuilder.build();
		JsonObject client = environment.getObject("client");
		List<String> scopes = Arrays.asList(OIDFJSON.getString(client.get("scope")).split(" "));
		assertTrue(
			scopes.contains("customers") &&
				scopes.contains("resources") &&
				scopes.contains("exchanges")
		);
	}

	@Test
	public void addScopesBasedOnCategoryTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addScopesBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA);
		permissionsBuilder.addScopesBasedOnCategory(OPFCategoryEnum.ACCOUNTS);
		permissionsBuilder.build();
		JsonObject client = environment.getObject("client");
		List<String> scopes = Arrays.asList(OIDFJSON.getString(client.get("scope")).split(" "));
		assertTrue(
			scopes.contains("customers") &&
				scopes.contains("resources") && scopes.contains("accounts")
		);
	}

	@Test
	public void addPermissionsTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissions(OPFPermissionsEnum.RESOURCES_READ);
		permissionsBuilder.addPermissions(OPFPermissionsEnum.ACCOUNTS_READ);
		permissionsBuilder.build();
		List<String> permissions = Arrays.asList(environment.getString("consent_permissions").split(" "));
		assertTrue(
			permissions.contains("ACCOUNTS_READ") &&
				permissions.contains("RESOURCES_READ")
		);
	}

	@Test
	public void addScopesTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addScopes(OPFScopesEnum.CUSTOMERS);
		permissionsBuilder.addScopes(OPFScopesEnum.RESOURCES);
		permissionsBuilder.build();
		JsonObject client = environment.getObject("client");
		List<String> scopes = Arrays.asList(OIDFJSON.getString(client.get("scope")).split(" "));
		assertTrue(
			scopes.contains("customers") &&
				scopes.contains("resources")
		);
	}

	@Test
	public void removePermissionsTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissions(OPFPermissionsEnum.RESOURCES_READ, OPFPermissionsEnum.LOANS_READ);
		permissionsBuilder.build();
		permissionsBuilder.removePermissions(OPFPermissionsEnum.RESOURCES_READ);
		permissionsBuilder.build();
		String permissions = environment.getString("consent_permissions");
		Assertions.assertEquals("LOANS_READ", permissions);
	}

	@Test
	public void removeScopesTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addScopes(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.RESOURCES);
		permissionsBuilder.build();
		permissionsBuilder.removeScopes(OPFScopesEnum.RESOURCES);
		permissionsBuilder.build();
		JsonObject client = environment.getObject("client");
		String scopes = OIDFJSON.getString(client.get("scope"));
		Assertions.assertEquals("customers", scopes);
	}

	@Test
	public void removePermissionsNotFoundTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissions(OPFPermissionsEnum.RESOURCES_READ, OPFPermissionsEnum.LOANS_READ);
		permissionsBuilder.build();
		permissionsBuilder.removePermissions(OPFPermissionsEnum.ACCOUNTS_READ);
		permissionsBuilder.build();
		String permissions = environment.getString("consent_permissions");
		Assertions.assertEquals("LOANS_READ RESOURCES_READ", permissions);
	}

	@Test
	public void removeScopesNotFoundTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addScopes(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.RESOURCES);
		permissionsBuilder.build();
		permissionsBuilder.removeScopes(OPFScopesEnum.ACCOUNTS);
		permissionsBuilder.build();
		JsonObject client = environment.getObject("client");
		String scopes = OIDFJSON.getString(client.get("scope"));
		Assertions.assertEquals("resources customers", scopes);
	}

	@Test
	public void getLogDataTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissionsBasedOnGrouping(OPFGroupingEnum.PERSONAL_REGISTRATION_DATA);
		permissionsBuilder.addScopesBasedOnGrouping(OPFGroupingEnum.PERSONAL_REGISTRATION_DATA);
		permissionsBuilder.build();
		Map<String, Object> expectedData = new HashMap<>(){
			{
				put("msg", "Providing Permissions/Scopes:");
				put("result", Condition.ConditionResult.SUCCESS);
				put("scopes", Set.of("resources", "customers"));
				put("permissions", Set.of("RESOURCES_READ", "CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ"));
			}
		};
		Map<String,Object> actualLogMessage = permissionsBuilder.getLogData();
		assertEquals(expectedData, actualLogMessage);
	}

	@Test
	public void emptyScopeSetTest() {
		ScopesAndPermissionsBuilder permissionsBuilder = new ScopesAndPermissionsBuilder(environment, mock(TestInstanceEventLog.class));
		permissionsBuilder.addPermissions(OPFPermissionsEnum.RESOURCES_READ, OPFPermissionsEnum.ACCOUNTS_READ);
		permissionsBuilder.build();
		JsonElement client = environment.getObject("client").get("scope");
		assertNull(client);
	}
}
