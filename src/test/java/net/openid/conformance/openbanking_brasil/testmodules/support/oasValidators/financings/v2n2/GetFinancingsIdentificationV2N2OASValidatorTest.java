package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class GetFinancingsIdentificationV2N2OASValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Before
	public void setUp() {
		setStatus(200);
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/identification.json")
	public void happyPath() {
		run(new GetFinancingsIdentificationV2n2OASValidator());
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/identificationNoInstlmentPeriodicityInfo.json")
	public void unhappyPathNoPeriodicityAdditionalInfo() {
		ConditionError e = runAndFail(new GetFinancingsIdentificationV2n2OASValidator());
		assertThat(e.getMessage(), containsString("instalmentPeriodicityAdditionalInfo is required when instalmentPeriodicity is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/identificationNoScheduledAdditionalInfo.json")
	public void unhappyPathNoScheduledAdditionalInfo() {
		ConditionError e = runAndFail(new GetFinancingsIdentificationV2n2OASValidator());
		assertThat(e.getMessage(), containsString("amortizationScheduledAdditionalInfo is required when amortizationScheduled is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/identificationNoChargeAdditionalInfo.json")
	public void unhappyPathNoChargeAdditionalInfo() {
		ConditionError e = runAndFail(new GetFinancingsIdentificationV2n2OASValidator());
		assertThat(e.getMessage(), containsString("chargeAdditionalInfo is required when chargeType is OUTROS"));
	}

	@Test
	@UseResurce("jsonResponses/financings/v2.1.0/identificationNoFeeRate.json")
	public void unhappyPathNoFeeRate() {
		ConditionError e = runAndFail(new GetFinancingsIdentificationV2n2OASValidator());
		assertThat(e.getMessage(), containsString("feeRate is required when feeCharge is PERCENTUAL"));
	}

}

