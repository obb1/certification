package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.util.UseResurce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CheckEnrollmentNameConsistencyTest extends AbstractJsonResponseConditionUnitTest {

	private static final String ENROLLMENT_NAME_KEY = "enrollment_name";
	private static final String ENROLLMENT_NAME = "Nome Dispositivo";

	@Before
	public void init() {
		environment.putString(ENROLLMENT_NAME_KEY, ENROLLMENT_NAME);
	}

	@Test
	@UseResurce("jsonResponses/enrollments/v2/PostEnrollments201Response.json")
	public void happyPathTest() {
		run(new CheckEnrollmentNameConsistency());
	}

	@Test
	public void unhappyPathTestUnparseableJwt() {
		environment.putObject(
			CheckEnrollmentNameConsistency.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "eya1.b2.c3").build()
		);
		unhappyPathTest("Could not parse body");
	}

	@Test(expected = AssertionError.class)
	public void unhappyPathTestMissingResponseBody() {
		run(new CheckEnrollmentNameConsistency());
	}

	@Test
	public void unhappyPathTestMissingData() {
		environment.putObject(
			CheckEnrollmentNameConsistency.RESPONSE_ENV_KEY,
			new JsonObjectBuilder().addField("body", "{}").build()
		);
		unhappyPathTest("Could not extract data.enrollmentName from response body");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/v2/PostEnrollments201ResponseWithoutEnrollmentName.json")
	public void unhappyPathTestMissingEnrollmentName() {
		unhappyPathTest("Could not extract data.enrollmentName from response body");
	}

	@Test
	@UseResurce("jsonResponses/enrollments/v2/PostEnrollments201Response.json")
	public void unhappyPathTestEnrollmentNameDoesNotMatch() {
		environment.putString(ENROLLMENT_NAME_KEY, "wrong name");
		unhappyPathTest("\"enrollmentName\" does not match the expected value");
	}

	private void unhappyPathTest(String message) {
		ConditionError error = runAndFail(new CheckEnrollmentNameConsistency());
		assertTrue(error.getMessage().contains(message));
	}
}
