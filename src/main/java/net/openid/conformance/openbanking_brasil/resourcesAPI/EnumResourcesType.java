package net.openid.conformance.openbanking_brasil.resourcesAPI;

public enum EnumResourcesType {
	ACCOUNT("accounts"), CREDIT_CARD_ACCOUNT("credit-cards"), LOAN("loans"), FINANCING("financings"),
	UNARRANGED_ACCOUNT_OVERDRAFT("unarranged-accounts-overdraft"), INVOICE_FINANCING("invoice-financings"),
	BANK_FIXED_INCOME("bank-fixed-incomes"), CREDIT_FIXED_INCOME("credit-fixed-incomes"), VARIABLE_INCOME("variable-incomes"),
	TREASURE_TITLE("treasure-titles"), FUND("funds"), EXCHANGE("EXCHANGE");

	private final String type;

	EnumResourcesType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
}
