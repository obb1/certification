package net.openid.conformance.openbanking_brasil.resourcesAPI.v3;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api: https://openbanking-brasil.github.io/openapi/swagger-apis/resources/3.0.0-beta.1.yml
 * Api endpoint: /resources
 * Api version: 3.0.0-beta.1
 **/
@ApiName("Resources V3")
public class ResourcesResponseValidatorV3 extends AbstractJsonAssertingCondition {

	public static final Set<String> ENUM_STATUS = SetUtils.createSet("AVAILABLE, UNAVAILABLE, TEMPORARILY_UNAVAILABLE, PENDING_AUTHORISATION");
	public static final Set<String> ACCOUNT = SetUtils.createSet("ACCOUNT, CREDIT_CARD_ACCOUNT, LOAN, FINANCING, UNARRANGED_ACCOUNT_OVERDRAFT, INVOICE_FINANCING, BANK_FIXED_INCOME, CREDIT_FIXED_INCOME, VARIABLE_INCOME, TREASURE_TITLE, FUND, EXCHANGE");

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment,"resource_endpoint_response_full");
		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(this::assertInnerFields)
				.setMinItems(0)
				.build());

		logFinalStatus();
		return environment;
	}

	private void assertInnerFields(JsonObject body) {
		assertField(body,
			new StringField
				.Builder("resourceId")
				.setPattern("^[a-zA-Z0-9][a-zA-Z0-9-]{0,99}$")
				.setMaxLength(100)
				.setMinLength(1)
				.build());

		assertField(body,
			new StringField
				.Builder("type")
				.setEnums(ACCOUNT)
				.build());

		assertField(body,
			new StringField
				.Builder("status")
				.setEnums(ENUM_STATUS)
				.build());
	}
}
