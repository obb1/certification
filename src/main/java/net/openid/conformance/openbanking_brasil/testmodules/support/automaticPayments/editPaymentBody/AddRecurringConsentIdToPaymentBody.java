package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class AddRecurringConsentIdToPaymentBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource", strings = "consent_id")
	public Environment evaluate(Environment env) {
		String recurringConsentId = env.getString("consent_id");
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));

		data.addProperty("recurringConsentId", recurringConsentId);
		logSuccess("The recurringConsentId has been added to the payments request payload", args("data", data));

		return env;
	}
}
