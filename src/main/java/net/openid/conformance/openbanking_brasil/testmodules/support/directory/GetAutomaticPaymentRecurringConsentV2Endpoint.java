package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAutomaticPaymentRecurringConsentV2Endpoint extends AbstractGetXFromAuthServer {

		@Override
		protected String getEndpointRegex() {
			return "^(https://)(.*?)(/open-banking/automatic-payments/v\\d+/recurring-consents)$";
		}

		@Override
		protected String getApiFamilyType() {
			return "payments-recurring-consents";
		}

		@Override
		protected String getApiVersionRegex() {
			return "^(2.[0-9].[0-9])$";
		}

		@Override
		protected boolean isResource() {
			return false;
		}
	}
