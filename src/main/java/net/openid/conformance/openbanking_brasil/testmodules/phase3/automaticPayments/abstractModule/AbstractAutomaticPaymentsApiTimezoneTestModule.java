package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithAllFields;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate.EditRecurringPaymentsConsentBodyToSetStartDateToOneHourInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToTodayUtcMinus3;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckCurrentTime;

public abstract class AbstractAutomaticPaymentsApiTimezoneTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithAllFields();
	}

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		setStartDateTime();
	}

	protected void setStartDateTime() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetStartDateToOneHourInTheFuture.class);
	}

	@Override
	protected void requestProtectedResource() {
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
		super.requestProtectedResource();
		callAndContinueOnFailure(CheckCurrentTime.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void startPixPaymentsBlock() {
		super.startPixPaymentsBlock();
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToTodayUtcMinus3.class);
	}
}
