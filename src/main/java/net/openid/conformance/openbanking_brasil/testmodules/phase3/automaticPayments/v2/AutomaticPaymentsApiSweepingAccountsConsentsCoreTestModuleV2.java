package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckForAccessTokenValue;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractExpiresInFromTokenEndpointResponse;
import net.openid.conformance.condition.client.ValidateExpiresIn;
import net.openid.conformance.condition.client.WaitForConfiguredSeconds;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsConsentsCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-consents-core_test-module_v2",
	displayName = "automatic-payments_api_sweeping-accounts-consents-core_test-module_v2",
	summary = "Ensure a consent for sweeping accounts can be successfully created and authorized. This test will also guarantee that a consent does not expire after 5 minutes under the AWAITING_AUTHORISATION status.\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts data, filling all fields for periodic limits\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Set the Conformance Suite to sleep for 7 minutes\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsConsentsCoreTestModuleV2 extends AbstractAutomaticPaymentsApiSweepingAccountsConsentsCoreTestModule {

	protected void createNewClientCredentialsToken() {
		runInBlock("Create new client_credentials token to continue test", () -> {
			callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
			callAndStopOnFailure(SetRecurringPaymentsScopeOnTokenEndpointRequest.class);
			call(sequence(addTokenEndpointClientAuthentication));
			callAndStopOnFailure(CallTokenEndpoint.class);
			callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
			callAndStopOnFailure(CheckForAccessTokenValue.class);
			callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);
			callAndContinueOnFailure(ExtractExpiresInFromTokenEndpointResponse.class, Condition.ConditionResult.FAILURE, "RFC6749-4.4.3", "RFC6749-5.1");
			call(condition(ValidateExpiresIn.class)
				.skipIfObjectMissing("expires_in")
				.onSkip(Condition.ConditionResult.INFO)
				.requirements("RFC6749-5.1")
				.onFail(Condition.ConditionResult.FAILURE)
				.dontStopOnFailure());
			callAndStopOnFailure(SaveAccessToken.class);
		});
	}

	@Override
	protected void performPreAuthorizationSteps() {
		call(createOBBPreauthSteps());
		validateVersionHeader();
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock(currentClientString() + "Validate consents response", this::validatePostConsentResponse);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
		env.putInteger("loopSequencePauseTime", 420);
		callAndStopOnFailure(WaitForConfiguredSeconds.class);
		createNewClientCredentialsToken();
		fetchConsentToCheckStatus("AWAITING_AUTHORISATION", new EnsurePaymentConsentStatusWasAwaitingAuthorisation());
		validateGetConsentResponse();
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class));
	}
}
