package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadPaymentIds extends AbstractCondition {
	@Override
	@PreEnvironment(required = "saved_payment_ids")
	@PostEnvironment(required = "payment_ids")
	public Environment evaluate(Environment env) {
		env.putObject("payment_ids", env.getObject("saved_payment_ids").deepCopy());
		logSuccess("The payment ids have been successfully loaded", env.getObject("payment_ids"));
		return env;
	}
}
