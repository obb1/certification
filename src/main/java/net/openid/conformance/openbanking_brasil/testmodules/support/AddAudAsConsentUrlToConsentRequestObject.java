package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddAudAsConsentUrlToConsentRequestObject extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_request", strings = "consent_url")
	public Environment evaluate(Environment env) {
		JsonObject consentRequestObject = env.getObject("consent_endpoint_request");
		String consentEndpoint = env.getString("consent_url");
		consentRequestObject.addProperty("aud", consentEndpoint);
		logSuccess("Added aud to consent endpoint request", args("aud", consentEndpoint));

		return env;
	}
}
