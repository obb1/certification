package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

import java.util.Map;

public class SanitiseQrCodeConfig extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		String localInstrument = env.getString("config", "resource.brazilPaymentConsent.data.payment.details.localInstrument");
		if("QRDN".equals(localInstrument) || "QRES".equals(localInstrument)) {
			logSuccess("Local Instrument is QRES or QRDN - leaving alone");
			return env;
		}
		JsonObject consentConfig = (JsonObject) env.getElementFromObject("config", "resource.brazilPaymentConsent.data.payment.details");
		JsonElement paymentConfig = env.getElementFromObject("config", "resource.brazilPixPayment.data");
		sanitise(null, consentConfig, "consent");
		sanitise(env.getString("payment_is_array"), paymentConfig, "payment");
		return env;
	}

	private void sanitise(String isArray, JsonElement config, String name) {
		if(config != null) {
			if (isArray != null) {
				for (JsonElement payment: config.getAsJsonArray()) {
					payment.getAsJsonObject().remove("qrCode");
				}
			} else {
				config.getAsJsonObject().remove("qrCode");
			}
			logSuccess("Removed qrCode from element", Map.of("element", name));
		} else {
			logSuccess("Element not present - leaving alone", Map.of("element", name));
		}
	}
}
