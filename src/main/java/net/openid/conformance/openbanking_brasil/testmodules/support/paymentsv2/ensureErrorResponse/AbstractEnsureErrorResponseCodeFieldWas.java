package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.List;

public abstract class AbstractEnsureErrorResponseCodeFieldWas extends AbstractJsonAssertingCondition {


	public static final String RESPONSE_ENV_KEY = "endpoint_response";

	/**
	 *
	 * Checks the desired error code. The endpoint_response env var has to be mapped to the actual full response
	 */
	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment environment) {
		List<String> expectedCodes = getExpectedCodes();

		JsonElement body = bodyFrom(environment,RESPONSE_ENV_KEY);

		JsonElement errorsEl = findByPath(body, "errors");
		if (!errorsEl.isJsonArray()) {
			throw error("Errors is not JSON array", args("Errors", errorsEl));
		}

		JsonArray errors = errorsEl.getAsJsonArray();

		for (JsonElement error : errors) {
			String code = OIDFJSON.getString(findByPath(error, "code"));
			if (expectedCodes.contains(code)) {
				logSuccess("Found error with the expected code", args("Expected", expectedCodes, "Found error", error));
				return environment;
			}
		}

		throw error("Could not find error with expected code in the errors JSON array", args("Expected", expectedCodes, "Errors", errors));

	}

	protected abstract List<String> getExpectedCodes();
}
