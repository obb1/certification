package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.webauthn4j.data.attestation.statement.COSEAlgorithmIdentifier;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonUtils;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class GenerateKeyPairFromFidoRegistrationOptionsResponse extends AbstractCondition {

	// Registration Options Response
	private static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	private static final List<COSEAlgorithmIdentifier> SUPPORTED_ALGORITHMS = List.of(
		COSEAlgorithmIdentifier.ES256,
		COSEAlgorithmIdentifier.RS256
	);

	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Error parsing JWT response");
		}
	}


	/**
	 * RESPONSE_ENV_KEY - resource_endpoint_response_full - expects to contain Registration Options Response
	 **/

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	@PostEnvironment(required = "fido_keys_jwk")
	public Environment evaluate(Environment env) {
		JsonObject body = bodyFrom(env).getAsJsonObject();
		JsonArray pubKeyCredParams = Optional.ofNullable(body.getAsJsonObject("data"))
			.map(data -> data.getAsJsonArray("pubKeyCredParams"))
			.orElseThrow(() -> error("Could not find data.pubKeyCredParams JSON array in the fido-registration-options response"));

		if (pubKeyCredParams.isEmpty()) {
			throw error("pubKeyCredParams JSON array is empty");
		}

		final COSEAlgorithmIdentifier algorithm = getSupportedPublicKeyAlgorithmFrom(pubKeyCredParams);
		KeyPair keyPair = generateKeyPair(algorithm);
		saveKeyPairToEnvironment(env, keyPair, algorithm);
		return env;
	}

	private void saveKeyPairToEnvironment(Environment environment, KeyPair keyPair, COSEAlgorithmIdentifier algorithm) {
		final PublicKey publicKey = keyPair.getPublic();
		final PrivateKey privateKey = keyPair.getPrivate();
		final long algorithmValue = algorithm.getValue();
		final String kid = UUID.randomUUID().toString();
		JWK jwk;
		if (algorithmValue == COSEAlgorithmIdentifier.ES256.getValue()) {
			jwk = new ECKey.Builder(Curve.P_256, (ECPublicKey) publicKey).privateKey(privateKey).keyID(kid).build();
		} else if (algorithmValue == COSEAlgorithmIdentifier.RS256.getValue()) {
			jwk = new RSAKey.Builder((RSAPublicKey) publicKey).privateKey(privateKey).keyID(kid).build();
		} else {
			throw error("Unsupported Algorithm is provided", Map.of("Algorithm", algorithm));
		}

		JsonObject jsonObject = JsonUtils.createBigDecimalAwareGson().fromJson(jwk.toJSONString(), JsonObject.class);
		environment.putObject("fido_keys_jwk", jsonObject);
		logSuccess("Generated FIDO keys");
	}

	private COSEAlgorithmIdentifier getSupportedPublicKeyAlgorithmFrom(JsonArray pubKeyCredParams) {
		for (JsonElement element : pubKeyCredParams) {
			JsonObject pubKeyCredParam = element.getAsJsonObject();
			COSEAlgorithmIdentifier algorithm = Optional.ofNullable(pubKeyCredParam.get("alg"))
				.map(OIDFJSON::getInt)
				.map(COSEAlgorithmIdentifier::create)
				.orElseThrow(() -> error("Could not find alg in the pubKeyCredParam", Map.of("array", pubKeyCredParams, "object", pubKeyCredParam)));

			String type = Optional.ofNullable(pubKeyCredParam.get("type"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("Could not find type in the pubKeyCredParam", Map.of("array", pubKeyCredParams, "object", pubKeyCredParam)));

			if (type.equals("public-key") && SUPPORTED_ALGORITHMS.contains(algorithm)) {
				return algorithm;
			}
		}

		throw error("Could not find supported public key algorithm in the pubKeyCredParams array", Map.of("array", pubKeyCredParams));
	}

	private KeyPair generateKeyPair(COSEAlgorithmIdentifier algorithm) {
		long algorithmValue = algorithm.getValue();
		try {
			KeyPairGenerator keyPairGenerator;
			if (algorithmValue == COSEAlgorithmIdentifier.ES256.getValue()) {
				keyPairGenerator = KeyPairGenerator.getInstance("EC", "BC");
				keyPairGenerator.initialize(new ECGenParameterSpec("prime256v1"));
			} else if (algorithmValue == COSEAlgorithmIdentifier.RS256.getValue()) {
				keyPairGenerator = KeyPairGenerator.getInstance("RSA");
				keyPairGenerator.initialize(2048);
			} else {
				throw error("Unsupported Algorithm is provided", Map.of("Algorithm", algorithm));
			}

			return keyPairGenerator.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			throw error("The requested algorithm is not available: " + e.getMessage());
		} catch (NoSuchProviderException e) {
			throw error("The requested provider is not available: " + e.getMessage());
		} catch (InvalidAlgorithmParameterException e) {
			throw error("The requested algorithm parameter is not available: " + e.getMessage());
		}
	}
}
