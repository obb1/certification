package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import com.google.gson.JsonObject;
import org.springframework.http.HttpMethod;


public class PostEnrollmentsOASValidatorV2 extends AbstractEnrollmentsOASValidatorV2 {

	@Override
	protected String getEndpointPathSuffix() {
		return "";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertDebtorAccountIssuerConstraint(data);
		assertCreationAndStatusUpdateDateTime(data);
	}
}
