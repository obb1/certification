package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Objects;


public class FAPIBrazilOpenBankingCreateConsentExtensionRequest extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config", strings = "consent_extension_expiration_time")
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {
		String cpf = env.getString("config", "resource.brazilCpf");
		String cnpj = env.getString("config", "resource.brazilCnpj");
		if (Strings.isNullOrEmpty(cpf)) {
			throw error("CPF must be specified in the test configuration");
		}

		JsonObject data = new JsonObject();

		String expirationDateTime = env.getString("consent_extension_expiration_time");
		if (!Objects.equals(expirationDateTime, "")) {
			data.addProperty("expirationDateTime", expirationDateTime);
			logSuccess("Expiration time added to consent extension request", args("expirationDateTime", env.getString("consent_extension_expiration_time")));
		}

		JsonObject loggedUser = new JsonObject();
		JsonObject document = new JsonObject();
		document.addProperty("identification", cpf);
		document.addProperty("rel", "CPF");
		loggedUser.add("document", document);
		data.add("loggedUser", loggedUser);
		logSuccess("CPF found in config creating loggedUser", args("loggedUser", loggedUser));

		if (!Strings.isNullOrEmpty(cnpj)) {
			JsonObject businessEntity = new JsonObject();
			JsonObject document2 = new JsonObject();
			document2.addProperty("identification", cnpj);
			document2.addProperty("rel", "CNPJ");
			businessEntity.add("document", document2);
			data.add("businessEntity", businessEntity);
			logSuccess("CPF found in config creating businessEntity", args("businessEntity", businessEntity));
		}

		JsonObject requestObject = new JsonObject();
		requestObject.add("data", data);
		env.putObject("consent_endpoint_request", requestObject);
		logSuccess("Created consent extension endpoint request", args("consent_endpoint_request", requestObject));
		return env;
	}
}
