package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class SetProtectedResourceLastUrlToPollFirstLink extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		if (Boolean.TRUE.equals(env.getBoolean("is_last_page"))) {
			JsonObject body = parseResponseBody(env);

			JsonObject links = body.getAsJsonObject("links");

			if (links == null) {
				throw error("No links found in the response");
			}

			JsonElement firstUrl = links.get("first");
			JsonElement selfUrl = links.get("self");

			String protectedResourceUrl;
			if (firstUrl != null) {
				protectedResourceUrl = OIDFJSON.getString(firstUrl);
			} else if (selfUrl != null) {
				protectedResourceUrl = OIDFJSON.getString(selfUrl);
			} else {
				throw error("No valid URL found in the response links");
			}
			env.putString("protected_resource_url", protectedResourceUrl);
			logSuccess("Successfully prepared url for next GET request.", args("url", protectedResourceUrl));
			env.putBoolean("is_last_page", false);
		}

		return env;
	}

	private JsonObject parseResponseBody(Environment env) {
		try {
			return OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}
	}
}
