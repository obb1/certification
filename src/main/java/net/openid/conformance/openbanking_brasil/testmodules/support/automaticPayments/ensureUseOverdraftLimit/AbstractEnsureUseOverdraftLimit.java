package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractEnsureUseOverdraftLimit extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		JsonObject data = response.getAsJsonObject("data");

		boolean useOverdraftLimit = Optional.ofNullable(data.getAsJsonObject("recurringConfiguration"))
			.map(recurringConfiguration -> recurringConfiguration.getAsJsonObject("automatic"))
			.map(automatic -> automatic.get("useOverdraftLimit"))
			.map(OIDFJSON::getBoolean)
			.orElseThrow(() -> error("Could not extract useOverdraftLimit field from response data"));

		if (expectedUseOverdraftLimitValue() != useOverdraftLimit) {
			throw error("The useOverdraftLimit value is different from expected",
				args("useOverdraftLimit", useOverdraftLimit, "expected", expectedUseOverdraftLimitValue()));
		}

		logSuccess("The useOverdraftLimit value is as expected");

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	protected abstract boolean expectedUseOverdraftLimitValue();
}
