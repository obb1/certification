package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21;

public class GetVariableIncomesTransactionsV1n21OASValidator extends AbstractVariableIncomesTransactionsOASValidatorV1n21 {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions";
	}


}
