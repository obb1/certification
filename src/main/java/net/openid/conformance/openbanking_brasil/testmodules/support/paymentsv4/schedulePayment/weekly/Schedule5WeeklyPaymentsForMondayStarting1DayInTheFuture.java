package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly;

public class Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture extends AbstractScheduleWeeklyPayment {

	@Override
	protected DayOfWeekEnum getDayOfWeek() {
		return DayOfWeekEnum.SEGUNDA_FEIRA;
	}

	@Override
	protected int getNumberOfDaysToAddToCurrentDate() {
		return 1;
	}

	@Override
	protected int getQuantity() {
		return 5;
	}
}
