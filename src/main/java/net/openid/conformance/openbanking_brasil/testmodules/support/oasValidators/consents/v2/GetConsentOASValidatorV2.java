package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v2;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.AbstractGetConsentOASValidator;

@ApiName("Get Consent V2")
public class GetConsentOASValidatorV2 extends AbstractGetConsentOASValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/consents/swagger-consents-2.1.0.yml";
	}
}
