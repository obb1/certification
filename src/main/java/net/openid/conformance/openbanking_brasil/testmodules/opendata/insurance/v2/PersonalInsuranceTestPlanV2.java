package net.openid.conformance.openbanking_brasil.testmodules.opendata.insurance.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.opendata.utils.PrepareToGetOpenDataApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.personalInsurance.v2.GetPersonalInsuranceOASValidatorV2;
import net.openid.conformance.openinsurance.testplan.utils.CallNoCacheResource;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "opendata-insurance-personal_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.INSURANCE_PERSONAL_API_PHASE_4A_TEST_PLAN_V2,
	summary = PlanNames.INSURANCE_PERSONAL_API_PHASE_4A_TEST_PLAN_V2
)
public class PersonalInsuranceTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PersonalInsuranceApiTestModuleV2.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}

	@PublishTestModule(
		testName = "opendata-insurance-personal_api_structural_test-module_v2",
		displayName = "Validate structure of Opendata Insurance - PersonalInsurance API Api resources",
		summary = "Validate structure of Opendata Insurance - PersonalInsurance Api resources",
		profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4)
	public static class PersonalInsuranceApiTestModuleV2 extends AbstractNoAuthFunctionalTestModule {

		@Override
		protected void runTests() {
			runInBlock("Validate Opendata Insurance - PersonalInsurance response", () -> {
				callAndStopOnFailure(PrepareToGetOpenDataApi.class);
				callAndStopOnFailure(CallNoCacheResource.class);
				callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(GetPersonalInsuranceOASValidatorV2.class, Condition.ConditionResult.FAILURE);
			});
		}
	}
}
