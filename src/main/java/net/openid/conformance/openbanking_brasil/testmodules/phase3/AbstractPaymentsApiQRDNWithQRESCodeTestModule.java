package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRDNCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithRealEmailIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRDNCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasQRCodeInvalidoV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;

public abstract class AbstractPaymentsApiQRDNWithQRESCodeTestModule extends AbstractPaymentConsentUnhappyPathTestModule {

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectQRDNCodeLocalInstrument.class);
        callAndStopOnFailure(SelectQRDNCodePixLocalInstrument.class);
        callAndStopOnFailure(InjectQRCodeWithRealEmailIntoConfig.class);
    }

    @Override
    protected void validatePaymentRejectionReasonCode() {
        // not used here
    }

    @Override
    protected void validate422ErrorResponseCode() {
        // not used here
    }

    @Override
    protected Class<? extends Condition> getExpectedPaymentResponseCode() {
        // not used here
        return null;
    }

    @Override
    protected Class<? extends Condition> postPaymentValidator() {
        // not used here
        return null;
    }

    @Override
    protected Class<? extends Condition> getPaymentValidator() {
        // not used here
        return null;
    }

    @Override
    protected Class<? extends Condition> paymentInitiationErrorValidator() {
        // not used here
        return null;
    }

	@Override
	protected void onAuthorizationCallbackResponse() {
		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);

		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OAuth2-iss-2");

		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());

		callAndStopOnFailure(EnsureConsentsRejectionReasonCodeWasQRCodeInvalidoV3.class);

		fireTestFinished();
	}
}
