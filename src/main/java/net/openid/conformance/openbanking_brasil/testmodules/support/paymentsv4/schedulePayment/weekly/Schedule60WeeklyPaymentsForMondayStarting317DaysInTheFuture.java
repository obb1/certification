package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly;

public class Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture extends AbstractScheduleWeeklyPayment {

	@Override
	protected DayOfWeekEnum getDayOfWeek() {
		return DayOfWeekEnum.SEGUNDA_FEIRA;
	}

	@Override
	protected int getNumberOfDaysToAddToCurrentDate() {
		return 317;
	}

	@Override
	protected int getQuantity() {
		return 60;
	}
}
