package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreatePatchEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas.EnsureEnrollmentCancelledFromWasIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRevocationReasonWas.EnsureEnrollmentRevocationReasonWasRevogadoManualmente;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRevoked;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasStatusVinculoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareRejectionOrRevocationReasonForPatchRequest.PrepareRevocationReasonRevogadoManualmenteForPatchRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPatchEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoSignOptions;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;

public abstract class AbstractEnrollmentsApiRevokedEnrollmentTestModule extends AbstractCompleteEnrollmentsApiEnrollmentTestModule {

	@Override
	protected void executeTestSteps() {
		runInBlock("Patch enrollments - Expects 204", () -> {
			useClientCredentialsAccessToken();
			callAndStopOnFailure(PrepareRevocationReasonRevogadoManualmenteForPatchRequest.class);
			PostEnrollmentsResourceSteps patchEnrollmentsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPatchEnrollments(),
				CreatePatchEnrollmentsRequestBodyToRequestEntityClaims.class,
				true
			);
			call(patchEnrollmentsSteps);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class,Condition.ConditionResult.FAILURE);
			userAuthorisationCodeAccessToken();
		});

		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRevoked(), "REVOKED");
		runInBlock("Validate enrollments fields related to cancellation", () -> {
			callAndContinueOnFailure(EnsureEnrollmentCancelledFromWasIniciadora.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureEnrollmentRevocationReasonWasRevogadoManualmente.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Post consents - Expects 201", this::postAndValidateConsent);

		runInBlock("Post fido-sign-options - Expects 422 STATUS_VINCULO_INVALIDO", () -> {
			call(new PostEnrollmentsResourceSteps(
				new PrepareToPostFidoSignOptions(),
				CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims.class,
				true
			));
		});
		runInBlock("Validate fido-sign-options error response", () -> {
			validateResourceResponse(postFidoSignOptionsValidator());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY);
		});
	}

	protected void postAndValidateConsent() {
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		call(createOBBPreauthSteps()
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Consent id is not added to the scope in non-redirect tests"));
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock(currentClientString() + "Validate consents response", this::validatePostConsentResponse);
	}

	protected void validatePostConsentResponse() {
		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected abstract Class<? extends Condition> postPaymentConsentValidator();

	protected abstract Class<? extends Condition> postFidoSignOptionsValidator();
}
