package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractEnsureScheduledDateIsX extends AbstractCondition {

	public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject consentRequestPayment = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.flatMap(consent -> Optional.ofNullable(consent.getAsJsonObject()))
			.flatMap(consent -> Optional.ofNullable(consent.getAsJsonObject("data")))
			.flatMap(data -> Optional.ofNullable(data.getAsJsonObject("payment")))
			.orElseThrow(() -> error("Could not find brazilPaymentConsent.data.payment in the resource"));


		String date = LocalDate.now(ZoneId.of("America/Sao_Paulo")).plusDays(getNumberOfDaysToAddToCurrentDate()).format(FORMATTER);

		JsonObject schedule = new JsonObjectBuilder()
			.addFields("single", Map.of("date", date))
			.build();

		consentRequestPayment.add("schedule", schedule);
		logSuccess("Added schedule.single.date to the consent request payment", args("consentRequestPayment", consentRequestPayment));
		return env;
	}

	protected abstract long getNumberOfDaysToAddToCurrentDate();


}
