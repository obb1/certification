package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors;

import com.google.gson.JsonArray;

public class EditRecurringPaymentsConsentBodyToSetTwoPessoaJuridicaCreditors extends AbstractEditRecurringPaymentsConsentBodyToSetCreditor {

	@Override
	protected void addCreditorsToArray(JsonArray creditors) {
		creditors.add(buildCreditor("PESSOA_JURIDICA", "98380199000125", "Lucio Pelucio Ltda."));
		creditors.add(buildCreditor("PESSOA_JURIDICA", "98380199000206", "Melo Caramelo Eireli"));
	}

	@Override
	protected String logMessage() {
		return "2 PESSOA_JURIDICA creditors";
	}
}
