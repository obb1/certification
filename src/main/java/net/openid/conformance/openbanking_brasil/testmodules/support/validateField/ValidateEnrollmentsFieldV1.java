package net.openid.conformance.openbanking_brasil.testmodules.support.validateField;

public class ValidateEnrollmentsFieldV1 extends AbstractValidateEnrollmentField {

	@Override
	protected int getEnrollmentVersion() {
		return 1;
	}

	@Override
	protected int getConsentVersion() {
		return 4;
	}
}
