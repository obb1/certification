package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadOldResponse extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = "saved_response")
	@PostEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		env.putObject(RESPONSE_ENV_KEY, env.getObject("saved_response").deepCopy());
		logSuccess("Saved endpoint response has loaded to its environment variable");
		return env;
	}
}
