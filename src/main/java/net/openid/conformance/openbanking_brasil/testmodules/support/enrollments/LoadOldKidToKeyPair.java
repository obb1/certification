package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadOldKidToKeyPair extends AbstractCondition {

	@Override
	@PreEnvironment(strings = "saved_kid", required = "fido_keys_jwk")
	public Environment evaluate(Environment env) {
		JsonObject fidoKeysJwk = env.getObject("fido_keys_jwk");
		String kid = env.getString("saved_kid");
		fidoKeysJwk.addProperty("kid", kid);
		logSuccess("Loaded old kid to the key pair", args("fido_keys_jwk", fidoKeysJwk));
		return env;
	}
}
