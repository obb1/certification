package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ExtractSoftwareOriginUriFromDirectorySSASteps;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Optional;

public class GenerateFidoClientData extends AbstractCondition {
	/**
	 * {@link ExtractEnrollmentIdFromEnrollmentsEndpointResponse} condition adds fido_challenge string to the env
	 * {@link ExtractSoftwareOriginUriFromDirectorySSASteps} condition sequence adds software_origin_uri to the env
	 **/
	@Override
	@PreEnvironment(required = {"config"}, strings = "fido_challenge")
	@PostEnvironment(required = "client_data")
	public Environment evaluate(Environment env) {

		try {
			String softwareOriginUrl = env.getString("software_origin_uri");

			String enrollmentUrl = OIDFJSON.getString(
				Optional.ofNullable(
					env.getElementFromObject("config", "resource.enrollmentsUrl")
				).orElseThrow(() -> error("enrollments url missing from configuration"))
			);
			String enrollmentsUrlHost = new URL(enrollmentUrl).getHost();

			String origin = Strings.isNullOrEmpty(softwareOriginUrl) ? String.format("https://%s", enrollmentsUrlHost) : softwareOriginUrl;

			Boolean isClientDataRegistration = Optional.ofNullable(env.getBoolean("is_client_data_registration"))
				.orElse(true);

			String type = Boolean.TRUE.equals(isClientDataRegistration) ? "webauthn.create" : "webauthn.get";

			String challenge = env.getString("fido_challenge");

			JsonObject clientData = new JsonObject();
			clientData.addProperty("type", type);
			clientData.addProperty("challenge", challenge);
			clientData.addProperty("origin", origin);
			clientData.addProperty("crossOrigin", false);

			env.putObject("client_data", clientData);

			logSuccess("client data was added to the environment", Map.of("client_data", clientData));
			return env;
		} catch (MalformedURLException e) {
			throw error("Could not parse the enrollment URL");
		}
	}
}
