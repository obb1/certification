package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules.AbstractFinancingsApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2.GetFinancingsListV2n2OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "financings_api_resources_test-module_v2-2",
	displayName = "Validate structure of financing API and Resources API resources V2",
	summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
		"• Create a consent with all the permissions needed to access the tested API\n" +
		"• Expects server to create the consent with 201\n" +
		"• Redirect the user to authorize at the financial institution\n" +
		"• Call the tested resource API\n" +
		"• Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the id provided by this API\n" +
		"• Call the resources API\n" +
		"• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class FinancingsApiResourcesTestModuleV2n extends AbstractFinancingsApiResourcesTestModule {

	@Override
	protected Class<? extends Condition> apiValidator() {
		return GetFinancingsListV2n2OASValidator.class;
	}

}

