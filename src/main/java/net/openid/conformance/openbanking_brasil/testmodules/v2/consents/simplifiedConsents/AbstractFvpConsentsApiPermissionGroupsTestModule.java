package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ArrayUtils;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasSemPermissoesFuncionaisRestantes;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

public abstract class AbstractFvpConsentsApiPermissionGroupsTestModule extends AbstractFvpDcrXConsentsTestModule {

	private boolean passed;

	protected abstract Class<? extends Condition> postConsentValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetConsentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void runTests() {
		passed = false;
		String[] personalRegistrationData = {"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ","RESOURCES_READ"};
		String[] personalAdditionalInfo = {"CUSTOMERS_PERSONAL_ADITTIONALINFO_READ", "RESOURCES_READ"};
		String[] balances = {"ACCOUNTS_READ", "ACCOUNTS_BALANCES_READ", "RESOURCES_READ"};
		String[] limits = {"ACCOUNTS_READ", "ACCOUNTS_OVERDRAFT_LIMITS_READ", "RESOURCES_READ"};
		String[] extras = {"ACCOUNTS_READ", "ACCOUNTS_TRANSACTIONS_READ", "RESOURCES_READ"};
		String[] creditCardLimits = {"CREDIT_CARDS_ACCOUNTS_READ", "CREDIT_CARDS_ACCOUNTS_LIMITS_READ", "RESOURCES_READ"};
		String[] creditCardTransactions = {"CREDIT_CARDS_ACCOUNTS_READ", "CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ", "RESOURCES_READ"};
		String[] creditCardInvoices = {"CREDIT_CARDS_ACCOUNTS_READ", "CREDIT_CARDS_ACCOUNTS_BILLS_READ", "CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ", "RESOURCES_READ"};
		String[] creditOperationsContractData = {"LOANS_READ", "LOANS_WARRANTIES_READ", "LOANS_SCHEDULED_INSTALMENTS_READ", "LOANS_PAYMENTS_READ", "FINANCINGS_READ", "FINANCINGS_WARRANTIES_READ", "FINANCINGS_SCHEDULED_INSTALMENTS_READ", "FINANCINGS_PAYMENTS_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ", "UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ", "INVOICE_FINANCINGS_READ", "INVOICE_FINANCINGS_WARRANTIES_READ", "INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ", "INVOICE_FINANCINGS_PAYMENTS_READ", "RESOURCES_READ"};
		String[] investments = {"BANK_FIXED_INCOMES_READ", "CREDIT_FIXED_INCOMES_READ", "FUNDS_READ", "VARIABLE_INCOMES_READ", "TREASURE_TITLES_READ", "RESOURCES_READ"};
		String[] exchanges = {"EXCHANGES_READ", "RESOURCES_READ"};
		String[] combo = ArrayUtils.concatArrays(balances, creditCardLimits);

		validatePermissions(personalRegistrationData, "Personal Registration Data");
		validatePermissions(personalAdditionalInfo, "Personal Additional Information");
		validatePermissions(balances, "Balances");
		validatePermissions(limits, "Limits");
		validatePermissions(extras, "Extras");
		validatePermissions(creditCardLimits, "Credit Card Limits");
		validatePermissions(creditCardTransactions, "Credit Card Transactions");
		validatePermissions(creditCardInvoices, "Credit Card Invoices");
		validatePermissions(creditOperationsContractData, "Credit Operations");
		validatePermissions(investments, "Investments");
		validatePermissions(exchanges, "Credit Operations");
		validatePermissions(combo, "Balances & Credit Card Limits");

		//If all validates returned a 422
		if (!passed) {
			throw new TestFailureException(getId(), "All resources returned a 422 when at least one set of permissions should have passed");
		}
	}

	private void validatePermissions(String[] permissions, String name) {
		String logMessage = String.format("Validate consent api request for '%s' permission group(s)", name);
		eventLog.startBlock(logMessage);

		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(SetContentTypeApplicationJson.class);
		env.putString("consent_permissions", String.join(" ", permissions));
		callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.SUCCESS);

		if (OIDFJSON.getInt(env.getObject("consent_endpoint_response_full").get("status")) == HttpStatus.CREATED.value()) {
			passed = true;
			callAndStopOnFailure(ConsentIdExtractor.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		} else {
			eventLog.log("422 error response", "Server does not support the sent group - expected 422");
			callAndContinueOnFailure(postConsentValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeWasSemPermissoesFuncionaisRestantes.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeWasSemPermissoesFuncionaisRestantes.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeWasSemPermissoesFuncionaisRestantes.RESPONSE_ENV_KEY);
		}

		eventLog.endBlock();
	}

	@Override
	protected void insertConsentProdValues() {}
}
