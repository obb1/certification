package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class ValidateIndefiniteExpirationTimeReturned extends AbstractValidateConsentExpirationTimeReturned {
	@Override
	protected String getExpectedConsentExpirationTimeEnvKey() {
		return "consent_extension_expiration_time";
	}
}

