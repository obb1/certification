package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiMultipleConsentsCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_multiple-consents-core_test-module_v1",
	displayName = "automatic-payments_api_multiple-consents-core_test-module_v1",
	summary = "Ensure a multiple consents account requires full authorization before payment is executed. Afterwards, it will check if the consent will be fully authorized. This test will use the CPF/CNPJ on the config for \"Payment consent -  XXX - Multiple Consents Test\", or Skipped if the field is not filled. The CPF/CNPJ informed will also be sent at the creditor account.\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, with the Multiple Accounts CPF/CNPJ\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is PARTIALLY_ACCEPTED\n" +
		"• Call the POST recurring-payments endpoint with the selected creditor\n" +
		"• Expect 422 CONSENTIMENTO_PENDENTE_AUTORIZACAO - Validate Response\n" +
		"• Poll the GET recurring-consents endpoint while status is PARTIALLY_ACCEPTED\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the POST recurring-payments endpoint with the selected creditor\n" +
		"• Expect 201 - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount"
	}
)
public class AutomaticPaymentsApiMultipleConsentsCoreTestModuleV1 extends AbstractAutomaticPaymentsApiMultipleConsentsCoreTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetRecurringPixOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class), condition(GetAutomaticPaymentPixRecurringV1Endpoint.class));
	}
}
