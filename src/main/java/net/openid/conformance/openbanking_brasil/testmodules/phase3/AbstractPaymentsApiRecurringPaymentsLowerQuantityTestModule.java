package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithStandardPayload;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.RemoveLastElementFromCustomSchedule;

public abstract class AbstractPaymentsApiRecurringPaymentsLowerQuantityTestModule extends AbstractPaymentApiSchedulingUnhappyTestModule {

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new CustomSchedulePaymentsWithStandardPayload();
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(RemoveLastElementFromCustomSchedule.class);
	}
}
