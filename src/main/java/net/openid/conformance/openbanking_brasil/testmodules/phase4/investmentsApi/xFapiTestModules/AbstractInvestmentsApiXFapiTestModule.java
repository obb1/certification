package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForDateHeaderInResourceResponse;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestment;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentBalances;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactionsCurrent;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractInvestmentsApiXFapiTestModule extends AbstractInvestmentsApiTestModule {

	@Override
	protected void requestProtectedResource() {
		testStep("List", () -> {}, investmentsRootValidator());
		super.requestProtectedResource();
	}

	@Override
	protected void executeParticularTestSteps() {
		testStep("Identification", () -> callAndStopOnFailure(PrepareUrlForFetchingInvestment.class), investmentsIdentificationValidator());

		testStep("Balances", () -> callAndStopOnFailure(PrepareUrlForFetchingInvestmentBalances.class), investmentsBalancesValidator());

		testStep("Transactions", this::prepareUrlForTransactions, investmentsTransactionsValidator());

		testStep("Transactions-Current", this::prepareUrlForTransactionsCurrent, investmentsTransactionsCurrentValidator());
	}

	protected void testStep(String endpoint, Runnable prepareUrl, Class<? extends Condition> validator) {
		ConditionSequence withoutInteractionIdSequence = getInvestmentsEndpointSequence();
		getAndValidateApi(endpoint, prepareUrl, validator, withoutInteractionIdSequence, "without the x-fapi-interaction-id");

		ConditionSequence withInvalidInteractionIdSequence = getInvestmentsEndpointSequence()
			.insertBefore(CallProtectedResource.class,
				sequenceOf(
					condition(CreateInvalidFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
				)
			);
		getAndValidateApi(endpoint, prepareUrl, validator, withInvalidInteractionIdSequence, "with an invalid x-fapi-interaction-id");
	}

	protected void getAndValidateApi(String endpoint, Runnable prepareUrl, Class<? extends Condition> validator, ConditionSequence sequence, String blockHeaderSuffix) {
		runInBlock(String.format("GET Investments %s endpoint %s", endpoint, blockHeaderSuffix), () -> {
			prepareUrl.run();
			call(sequence);
		});
		runInBlock(String.format("Validating GET Investments %s response", endpoint), () -> {
			callAndContinueOnFailure(EnsureResourceResponseCodeWas400.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
		});
	}

	protected ConditionSequence getInvestmentsEndpointSequence() {
		return sequenceOf(
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
			condition(CallProtectedResource.class),
			condition(CheckForDateHeaderInResourceResponse.class),
			condition(CheckForFAPIInteractionIdInResourceResponse.class),
			condition(EnsureResourceResponseReturnedJsonContentType.class)
		);
	}

	protected void prepareUrlForTransactions() {
		callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
		callAndStopOnFailure(setToTransactionToToday());
		callAndStopOnFailure(setFromTransactionTo360DaysAgo());
		callAndContinueOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl(), Condition.ConditionResult.FAILURE);

	}
	protected void prepareUrlForTransactionsCurrent() {
		callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class);
		callAndStopOnFailure(setToTransactionToToday());
		callAndStopOnFailure(setFromTransactionTo6DaysAgo());
		callAndContinueOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl(), Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence deleteConsent() {
		return super.deleteConsent().insertBefore(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, sequenceOf(
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class)
		));
	}

	protected abstract Class<? extends Condition> investmentsIdentificationValidator();

	protected abstract Class<? extends Condition> investmentsBalancesValidator();

	protected abstract Class<? extends Condition> investmentsTransactionsValidator();

	protected abstract Class<? extends Condition> investmentsTransactionsCurrentValidator();

	protected abstract Class<? extends Condition> setToTransactionToToday();

	protected abstract Class<? extends Condition> setFromTransactionTo360DaysAgo();

	protected abstract Class<? extends Condition> setFromTransactionTo6DaysAgo();

	protected abstract Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl();
}
