package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.pixqrcode.PixQRCode;
import net.openid.conformance.testmodule.Environment;

public class InjectQRCodeWithRealEmailIntoConfig extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject consentPaymentDetails = (JsonObject) env.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details");
		JsonElement paymentInitiation = env.getElementFromObject("resource", "brazilPixPayment.data");

		PixQRCode qrCode = new PixQRCode();
		qrCode.useStandardConfig();

		String amount = env.getString("resource", "brazilPaymentConsent.data.payment.amount");
		if(Strings.isNullOrEmpty(amount)){
			throw error("Could not find amount in the payments consent resource");
		}

		qrCode.setTransactionAmount(amount);
		consentPaymentDetails.addProperty("qrCode", qrCode.toString());

		if(env.getString("payment_is_array")!=null){
			for (JsonElement payment: paymentInitiation.getAsJsonArray()) {
				payment.getAsJsonObject().addProperty("qrCode", qrCode.toString());
			}
		} else {
			paymentInitiation.getAsJsonObject().addProperty("qrCode", qrCode.toString());
		}

		logSuccess("Added qr code to payment consent and payment initiation", args("qrCode", qrCode.toString()));

		return env;
	}
}
