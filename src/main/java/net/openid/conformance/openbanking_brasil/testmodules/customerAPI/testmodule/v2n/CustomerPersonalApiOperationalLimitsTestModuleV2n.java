package net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n;

import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractCustomerPersonalApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalIdentificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalQualificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalRelationsResponseOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "customer-personal_api_operational-limits_test-module_v2-2",
	displayName = "Make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Personal API are considered correctly",
	summary =	"""
		This test will make sure that the server is not blocking access to the APIs as long as the operational limits for the Customer Personal API are considered correctly
		• Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL) and at least the CPF for Operational Limits (CPF for OL) test have been provided
		• Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Customer Personal permission group - Expect Server to return a 201 - Save ConsentID (1)
		• Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test
		• With the authorized consent id (1), call the GET Customer Personal Identifications API 4 times - Expect a 200 response
		• With the authorized consent id (1), call the GET Customer Personal Qualifications 4 times - Expect a 200 response
		• With the authorized consent id (1), call the GET Customer Personal Financial Relations 4 times - Expect a 200 response
	""",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational"
	}
)
public class CustomerPersonalApiOperationalLimitsTestModuleV2n extends AbstractCustomerPersonalApiOperationalLimitsTestModule {
	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPersonalQualificationValidator() {
		return PersonalQualificationResponseOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPersonalRelationsValidator() {
		return PersonalRelationsResponseOASValidatorV2.class;
	}

	@Override
	protected Class<? extends AbstractJsonAssertingCondition> getPersonalIdentificationValidator() {
		return PersonalIdentificationResponseOASValidatorV2.class;
	}
}
