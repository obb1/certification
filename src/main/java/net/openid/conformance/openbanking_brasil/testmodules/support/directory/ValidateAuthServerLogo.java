package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

public class ValidateAuthServerLogo extends AbstractCondition {
	@Override
	@PreEnvironment(required = "authorisation_server")
	public Environment evaluate(Environment env) {
		JsonObject authorisationServer = env.getObject("authorisation_server");
		String logoUri;
		if (authorisationServer.has("CustomerFriendlyLogoUri") && !authorisationServer.get("CustomerFriendlyLogoUri").isJsonNull()) {
			logoUri = OIDFJSON.getString(authorisationServer.get("CustomerFriendlyLogoUri"));
		} else {
			logFailure("CustomerFriendlyLogoUri is not present in the authorisation server metadata");
			return env;
		}
		try {
			RestTemplate restTemplate = createRestTemplate(env);

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public boolean hasError(ClientHttpResponse response) {
					// Treat all http status codes as 'not an error', so spring never throws an exception due to the http
					// status code meaning the rest of our code can handle http status codes how it likes
					return false;
				}
			});

			HttpHeaders headers = new HttpHeaders();
			headers.set("User-Agent", "OpenFinanceFVP/1.0");
			HttpMethod httpMethod = HttpMethod.GET;
			HttpEntity<?> request = new HttpEntity<>(headers);

			try {
				ResponseEntity<String> response = restTemplate.exchange(logoUri, httpMethod, request, String.class);
				if (response.getStatusCode() != HttpStatus.OK) {
					throw error("Unexpected HTTP status code", args("expected", HttpStatus.OK, "actual", response.getStatusCode()));
				}
				validateSvgHeaderReturned(response);
				boolean answer = isAuthServerLogoCorrectSize(response);
				if (!answer) {
					logFailure("The logo is not the correct size");
				} else {
					logSuccess("The logo is the correct size");
				}
				validateLogoDimensions(response);

			} catch (RestClientResponseException e) {
				throw error("Error from CustomerFriendlyLogoUri",
					args("CustomerFriendlyLogoUri", logoUri, "code", e.getRawStatusCode(),
						"status", e.getStatusText(), "body", e.getResponseBodyAsString()));

			} catch (RestClientException e) {

				String reason = "Unknown";
				if (e.getCause() != null) {
					reason = e.getCause().getMessage();
				}

				throw error("Call to CustomerFriendlyLogoUri failed", e,
					args("CustomerFriendlyLogoUri", logoUri, "reason", reason));
			}

		} catch (NoSuchAlgorithmException | KeyManagementException | CertificateException | InvalidKeySpecException |
				 KeyStoreException | IOException | UnrecoverableKeyException e) {
			throw error("Error creating HTTP Client", e);
		}
		return env;
	}

	protected boolean isAuthServerLogoCorrectSize(ResponseEntity<String> response) {
		long contentLength = response.getHeaders().getContentLength();
		if (contentLength == -1 || contentLength == 0) {
				byte[] responseBody = Objects.requireNonNull(response.getBody()).getBytes();
				contentLength = responseBody.length;
		}
		logSuccess("Extracted logo size", args("size", contentLength));
		return contentLength <= 1024 * 1024;
	}

	protected void validateLogoDimensions(ResponseEntity<String> response){
		try {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(Objects.requireNonNull(response.getBody()).getBytes());
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(inputStream);
			Element svgElement = document.getDocumentElement();
			String widthString = svgElement.getAttribute("width");
			String heightString = svgElement.getAttribute("height");
			int width;
			int height;
			if (widthString.isEmpty() || heightString.isEmpty()) {
				String viewBoxString = svgElement.getAttribute("viewBox");
				String[] viewBoxParts = viewBoxString.split(" ");
				width = (int) Math.round(Double.parseDouble(viewBoxParts[2].replaceAll("[^\\d.]", "")) * getConversionFactor(viewBoxParts[2]));
				height = (int) Math.round(Double.parseDouble(viewBoxParts[3].replaceAll("[^\\d.]", "")) * getConversionFactor(viewBoxParts[3]));
			} else {
				width = (int) Math.round(Double.parseDouble(widthString.replaceAll("[^\\d.]", "")) * getConversionFactor(widthString));
				height = (int) Math.round(Double.parseDouble(heightString.replaceAll("[^\\d.]", "")) * getConversionFactor(heightString));
			}
			if (height < 512 || width < 512) {
				throw error("Logo does not meet the minimum requirements of 512x512", args("width", width, "height", height));
			}
			logSuccess("Width: " + width + ", Height: " + height);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw error("Error parsing SVG", e);
		}
	}

	private double getConversionFactor(String dimensionString) {
		String unit = dimensionString.replaceAll("[^a-zA-Z]", "");
		switch (unit) {
			case "pt":
				return 96 / 72.0; // points to pixels
			case "in":
				return 96; // inches to pixels
			case "mm":
				return 96 / 25.4; // millimeters to pixels
			case "cm":
				return 96 / 2.54; // centimeters to pixels
			default:
				return 1; // pixels
		}
	}

	protected void validateSvgHeaderReturned(ResponseEntity<String> response) {
		String contentType = response.getHeaders().getFirst("Content-Type");
		if (contentType == null) {
			throw error("Content-Type header not returned, no further processing possible");
		}
		if (!contentType.contains("image/svg+xml")) {
			throw error("Content-Type header returned is/contains not image/svg+xml, no further processing possible");
		}
		logSuccess("Content-Type header returned is/contains image/svg+xml");
	}
}
