package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.RejectAuthCodeInAuthorizationEndpointResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalVariableAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentsConsentsRejectionReasonWasAutenticacaoDivergente;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasRejected;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixUnmatchingLoggedUserTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalVariableAmount();
	}

	@Override
	protected void onAuthorizationCallbackResponse() {
		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");
		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");
		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");
		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);
		callAndStopOnFailure(RejectAuthCodeInAuthorizationEndpointResponse.class);
		performPostAuthorizationFlow();
	}

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckStatus("REJECTED", new EnsurePaymentConsentStatusWasRejected());
		callAndStopOnFailure(EnsureRecurringPaymentsConsentsRejectionReasonWasAutenticacaoDivergente.class);
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}
}
