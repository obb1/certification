package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRejectionReasonEnum;

public class EnsureEnrollmentRejectionReasonWasRejeitadoHybridFlow extends AbstractEnsureEnrollmentRejectionReasonWas {

    @Override
    protected String getExpectedRejectionReason() {
        return EnrollmentRejectionReasonEnum.REJEITADO_FALHA_HYBRID_FLOW.toString();
    }
}
