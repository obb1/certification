package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public class DeleteSavedAccessToken extends AbstractCondition {

    @Override
    public Environment evaluate(Environment env) {
        JsonObject accessToken = env.getObject("old_access_token");
        if (accessToken != null && !accessToken.isJsonNull()) {
            env.removeObject("old_access_token");
            logSuccess("Removed old_access_token from the environment", args("old_access_token", accessToken));
        } else {
            logSuccess("No old_access_token to delete");
        }
        return env;
    }
}
