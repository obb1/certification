package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.overridePaymentsInResources.OverridePaymentInResourcesWithWrongMonthlySchedule;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly.Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture;

public abstract class AbstractPaymentsApiRecurringPaymentsUnhappyDateAdjustmentTestModule extends AbstractPaymentApiSchedulingUnhappyTestModule {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(OverridePaymentInResourcesWithWrongMonthlySchedule.class);
	}

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture();
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento();
	}
}
