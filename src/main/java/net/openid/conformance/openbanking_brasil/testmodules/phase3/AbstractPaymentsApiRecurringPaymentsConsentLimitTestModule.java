package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithDateTooFarInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithRepeatedDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule60DailyPaymentsStarting672DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule61DailyPaymentsStarting10DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly.Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule61WeeklyPaymentsForMondayStarting10DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentsApiRecurringPaymentsConsentLimitTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		//Not needed for this test
		return null;	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		//Not needed for this test
		return null;	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		//Not needed for this test
		return null;	}

	@Override
	protected void configureDictInfo() {
		//Not needed for this test
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		callAndStopOnFailure(Schedule60DailyPaymentsStarting672DaysInTheFuture.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.insertAfter(OptionallyAllow201Or422.class,
				condition(paymentConsentErrorValidator())
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
					.skipIfStringMissing("validate_errors")
			)
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Will not call token endpoint")
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			.insertBefore(AddFAPIAuthDateToResourceEndpointRequest.class,condition(CreateRandomFAPIInteractionId.class));
	}

	@Override
	protected void performAuthorizationFlow() {

		runInBlock("Daily Scheduling", () -> {
			call(createOBBPreauthSteps());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			paymentConsentErrorValidator();
			env.mapKey(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Weekly Scheduling", () -> {
			callAndStopOnFailure(Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture.class);
			call(createOBBPreauthSteps());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			paymentConsentErrorValidator();
			env.mapKey(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Monthly Scheduling", () -> {
			callAndStopOnFailure(Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture.class);
			call(createOBBPreauthSteps());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			paymentConsentErrorValidator();
			env.mapKey(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Custom Scheduling - Invalid Dates", () -> {
			callAndContinueOnFailure(CustomSchedulePaymentsWithDateTooFarInTheFuture.class, Condition.ConditionResult.FAILURE);
			call(createOBBPreauthSteps());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			paymentConsentErrorValidator();
			env.mapKey(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Custom Scheduling - Repeated Dates", () -> {
			callAndStopOnFailure(CustomSchedulePaymentsWithRepeatedDates.class);
			call(createOBBPreauthSteps());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			paymentConsentErrorValidator();
			env.mapKey(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Daily Scheduling - Limit Exceeded", () -> {
			callAndStopOnFailure(Schedule61DailyPaymentsStarting10DaysInTheFuture.class);
			call(createOBBPreauthSteps());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			paymentConsentErrorValidator();
			env.mapKey(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Weekly Scheduling - Limit Exceeded", () -> {
			callAndStopOnFailure(Schedule61WeeklyPaymentsForMondayStarting10DaysInTheFuture.class);
			call(createOBBPreauthSteps());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			paymentConsentErrorValidator();
			env.mapKey(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
		});

		fireTestFinished();
	}

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

}
