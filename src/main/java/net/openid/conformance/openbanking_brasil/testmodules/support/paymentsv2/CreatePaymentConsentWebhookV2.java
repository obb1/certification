package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

public class CreatePaymentConsentWebhookV2 extends AbstractCreateWebhookEndpoint{
	@Override
	protected String getValueFromEnv() {
		return "consent_id";
	}

	@Override
	protected String getExpectedUrlPath() {
		return "/open-banking/webhook/v1/payments/v2/consents/";
	}
}

