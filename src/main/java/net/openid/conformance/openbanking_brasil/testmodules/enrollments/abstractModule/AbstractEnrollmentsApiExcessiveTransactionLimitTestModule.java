package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.ExtractTransactionLimitFromEnrollmentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasValorAcimaLimite;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOverTransactionLimitOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureErrorResponse.EnsureErrorResponseCodeFieldWasValorAcimaLimite;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractEnrollmentsApiExcessiveTransactionLimitTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		super.getEnrollmentsAfterFidoRegistration();
		callAndStopOnFailure(ExtractTransactionLimitFromEnrollmentResponse.class);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return super.getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class));
	}

	@Override
	protected void validatePaymentsResponse() {
		int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
		if (status == HttpStatus.CREATED.value()) {
			super.validatePaymentsResponse();
		} else if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			env.mapKey(EnsureErrorResponseCodeFieldWasValorAcimaLimite.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndStopOnFailure(EnsureErrorResponseCodeFieldWasValorAcimaLimite.class);
			env.unmapKey(EnsureErrorResponseCodeFieldWasValorAcimaLimite.RESPONSE_ENV_KEY);
		}
	}

	@Override
	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasRjct.class);
		callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasValorAcimaLimite.class);
	}

	@Override
	protected void executeFurtherSteps() {}

	@Override
	protected void configPaymentsFlow() {
		callAndStopOnFailure(SetPaymentAmountToOverTransactionLimitOnConsent.class);
		super.configPaymentsFlow();
	}

	@Override
	protected void configureDictInfo() {}
}
