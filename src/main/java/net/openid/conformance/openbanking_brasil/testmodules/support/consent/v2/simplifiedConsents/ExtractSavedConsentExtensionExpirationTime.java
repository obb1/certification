package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ExtractSavedConsentExtensionExpirationTime extends AbstractCondition {
	@Override
	@PreEnvironment(strings = "saved_consent_extension_expiration_time")
	@PostEnvironment(strings = "consent_extension_expiration_time")
	public Environment evaluate(Environment env) {
		String savedConsentExtensionExpirationTime = env.getString("saved_consent_extension_expiration_time");
		if (savedConsentExtensionExpirationTime != null) {
			env.putString("consent_extension_expiration_time", savedConsentExtensionExpirationTime);
			logSuccess("Extracted saved consent extension expiry time", args("consent_extension_expiration_time", savedConsentExtensionExpirationTime));
			return env;
		}
		throw error("No saved consent extension expiry time found");
	}
}
