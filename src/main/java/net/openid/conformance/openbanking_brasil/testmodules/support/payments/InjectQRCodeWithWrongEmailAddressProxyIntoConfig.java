package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.pixqrcode.PixQRCode;
import net.openid.conformance.testmodule.Environment;

import java.util.Locale;
import java.util.Random;

public class InjectQRCodeWithWrongEmailAddressProxyIntoConfig extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		//Build the QRes
		PixQRCode qrCode = new PixQRCode();
		qrCode.useStandardConfig();

		//Generate random amount
		Random r = new Random();
		float random = r.nextFloat() * 100;
		String amount = String.format(Locale.UK, "%.02f", random);
		qrCode.setTransactionAmount(amount);

		//Mismatched proxy
		qrCode.setProxy(DictHomologKeys.PROXY_PHONE_NUMBER);

		JsonObject consentPayment = env.getElementFromObject("resource", "brazilPaymentConsent.data.payment").getAsJsonObject();
		consentPayment.addProperty("amount", amount);
		JsonObject consentPaymentDetails = env.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details").getAsJsonObject();
		consentPaymentDetails.addProperty("qrCode", qrCode.toString());

		JsonElement paymentInitiation = env.getElementFromObject("resource", "brazilPixPayment.data");

		if (paymentInitiation.isJsonArray()) {
			for (int i = 0; i < paymentInitiation.getAsJsonArray().size(); i++) {
				JsonObject payment = paymentInitiation.getAsJsonArray().get(i).getAsJsonObject().getAsJsonObject("payment");
				payment.addProperty("amount", amount);
				payment.addProperty("qrCode", qrCode.toString());
			}
			logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
			return env;
		}

		JsonObject payment = paymentInitiation.getAsJsonObject().getAsJsonObject("payment");
		payment.getAsJsonObject().addProperty("amount", amount);
		payment.getAsJsonObject().addProperty("qrCode", qrCode.toString());

		logSuccess(String.format("Added new QRes to payment consent and payment initiation with amount %s BRL", amount), args("QRes", qrCode.toString()));
		return env;
	}
}
