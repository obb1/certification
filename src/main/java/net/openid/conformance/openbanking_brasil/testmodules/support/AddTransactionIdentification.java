package net.openid.conformance.openbanking_brasil.testmodules.support;


public class AddTransactionIdentification extends AbstractPaymentTransactionIdentificationCondition {

	@Override
	protected String getTransactionIdentification() {
		return "E00038166201907261559y6j6";
	}

}
