package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class ExtractFirstUriFromSSASoftwareOriginUris extends AbstractCondition {

	@Override
	@PreEnvironment(required = "software_statement_assertion")
	@PostEnvironment(strings = "software_origin_uri")
	public Environment evaluate(Environment env) {
		JsonArray uris = Optional.ofNullable(env.getObject("software_statement_assertion").get("claims"))
			.map(JsonElement::getAsJsonObject)
			.map(claims -> claims.get("software_origin_uris"))
			.map(JsonElement::getAsJsonArray)
			.orElseThrow(() -> error("There is no software_origin_uris field inside the SSA"));

		if (uris.isEmpty()) {
			throw error("software_origin_uris is an empty array");
		}

		String uri = OIDFJSON.getString(uris.get(0));
		env.putString("software_origin_uri", uri);
		logSuccess("Successfully extracted the first URI from the SSA software_origin_uris field");

		return env;
	}
}
