package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureMinimumAuthorizationCodeEntropy;
import net.openid.conformance.condition.client.EnsureMinimumAuthorizationCodeLength;
import net.openid.conformance.condition.client.ExtractAuthorizationCodeFromAuthorizationResponse;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.RejectAuthCodeInAuthorizationEndpointResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckIfAuthorizationEndpointResponseHasErrorOrNot;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.FAPIBrazilAddRecurringConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticPaymentsApiInvalidScopeTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps(
			addTokenEndpointClientAuthentication,
			false
		).replace(FAPIBrazilAddRecurringConsentIdToClientScope.class, condition(FAPIBrazilAddConsentIdToClientScope.class));
	}

	@Override
	protected void onAuthorizationCallbackResponse() {
		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");
		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");
		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");
		callAndStopOnFailure(CheckIfAuthorizationEndpointResponseHasErrorOrNot.class);

		if (env.getBoolean("token_endpoint_response_was_error")) {
			callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);
			callAndStopOnFailure(RejectAuthCodeInAuthorizationEndpointResponse.class);
			onPostAuthorizationFlowComplete();
		} else {
			callAndStopOnFailure(ExtractAuthorizationCodeFromAuthorizationResponse.class);
			callAndContinueOnFailure(EnsureMinimumAuthorizationCodeLength.class, Condition.ConditionResult.FAILURE, "RFC6749-10.10", "RFC6819-5.1.4.2-2");
			callAndContinueOnFailure(EnsureMinimumAuthorizationCodeEntropy.class, Condition.ConditionResult.FAILURE, "RFC6749-10.10", "RFC6819-5.1.4.2-2");
			handleSuccessfulAuthorizationEndpointResponse();
		}
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		String skipMessage = "Not expecting a jwt response";
		return new CallPixPaymentsEndpointSequence()
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class))
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas403.class))
			.replace(EnsureContentTypeApplicationJwt.class, condition(EnsureContentTypeJson.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE))
			.skip(ExtractSignedJwtFromResourceResponse.class, skipMessage)
			.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, skipMessage)
			.skip(FAPIBrazilValidateResourceResponseTyp.class, skipMessage)
			.skip(FetchServerKeys.class, skipMessage)
			.skip(ValidateResourceResponseSignature.class, skipMessage)
			.skip(ValidateResourceResponseJwtClaims.class, skipMessage);
	}

	@Override
	protected void validateResponse() {}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}
}
