package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentApiSchedulingUnhappyTestModule extends AbstractPaymentApiSchedulingTestModule {

	protected abstract AbstractEnsureErrorResponseCodeFieldWas errorFieldCondition();

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return super.getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas422.class));
	}

	@Override
	protected void validateResponse() {
		env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
		callAndContinueOnFailure(errorFieldCondition().getClass(), Condition.ConditionResult.FAILURE);
		env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
	}
}
