package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddAudAsPaymentInitiationUriToRequestObject;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIatToRequestObject;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIssAsCertificateOuToRequestObject;
import net.openid.conformance.condition.client.AddJtiAsUuidToRequestObject;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.CreatePaymentRequestEntityClaims;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentInitiationRequest;
import net.openid.conformance.condition.client.SetApplicationJwtAcceptHeaderForResourceEndpointRequest;
import net.openid.conformance.condition.client.SetApplicationJwtContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.condition.client.SetResourceMethodToPost;
import net.openid.conformance.openbanking_brasil.testmodules.support.InjectWrongISSToJWT;
import net.openid.conformance.openbanking_brasil.testmodules.support.ModifyPixPaymentValue;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsureErrorResponseCodeFieldWasConsentimentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasErroIdempotencia;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOldValueOnPaymentInitiation;

public abstract class AbstractPaymentsIdempotencyTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

    protected abstract Class<? extends Condition> paymentInitiationErrorValidator();

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
        callAndStopOnFailure(SanitiseQrCodeConfig.class);
    }

    @Override
    protected void requestProtectedResource() {
        //Make request using an invalid ISS and expect a 403 response
        eventLog.startBlock("Making PIX request with incorrect ISS");
        callPixWrongIss();
        //Make request with the same idempotency key used at POST consent
        eventLog.startBlock("Making correct PIX request");
        callPix();
        //Make request using same idempotency key but using a different payload
        eventLog.startBlock("Making PIX request with with same idempotency key but different payload");
        callPixChangedPayload();
        //Make request with different idempotency key but with same payload
        eventLog.startBlock("Making PIX request with the same payload but different idempotency key");
        callPixDifferentIdempotencyKey();
    }

    @Override
    protected void validateResponse() {
        // Not needed for this test
    }


    public void callPixChangedPayload() {
	userAuthorisationCodeAccessToken();

        callAndStopOnFailure(ModifyPixPaymentValue.class);

        callPixFirstBlock();

        callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

        callPixSecondBlock();

        callAndStopOnFailure(SetPaymentAmountToOldValueOnPaymentInitiation.class);

        callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);

        call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));

        callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasErroIdempotencia.class, Condition.ConditionResult.FAILURE);

        callAndStopOnFailure(paymentInitiationErrorValidator());

	call(exec().unmapKey("endpoint_response"));
    }

    public void callPix() {

        callPixFirstBlock();

        callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

        callPixSecondBlock();

        callAndStopOnFailure(EnsureResourceResponseCodeWas201.class);

        callAndStopOnFailure(postPaymentValidator());
    }

    private void callPixDifferentIdempotencyKey() {
	userAuthorisationCodeAccessToken();

        env.mapKey("idempotency_key", "different_idempotency_key");

        callAndStopOnFailure(CreateIdempotencyKey.class);

        callPixFirstBlock();

        env.unmapKey("idempotency_key");

        callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

        callPixSecondBlock();

	call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));

        callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);

        callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasConsentimentoInvalido.class, Condition.ConditionResult.FAILURE);

        callAndContinueOnFailure(paymentInitiationErrorValidator(), Condition.ConditionResult.FAILURE);

	call(exec().unmapKey("endpoint_response"));
    }

    public void callPixWrongIss() {
        callPixFirstBlock();

        callAndStopOnFailure(InjectWrongISSToJWT.class);

        callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");

        callPixSecondBlock();

        callAndStopOnFailure(EnsureResourceResponseReturnedJsonContentType.class);

        callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);

        validateError();
    }

    private void validateError() {
        callAndStopOnFailure(paymentInitiationErrorValidator());
    }

    private void callPixFirstBlock() {
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);

        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");

        callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");

        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);

        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");

        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
        callAndStopOnFailure(SetApplicationJwtContentTypeHeaderForResourceEndpointRequest.class);
        callAndStopOnFailure(SetApplicationJwtAcceptHeaderForResourceEndpointRequest.class);
        callAndStopOnFailure(SetResourceMethodToPost.class);
        callAndStopOnFailure(CreatePaymentRequestEntityClaims.class);

        call(exec().mapKey("request_object_claims", "resource_request_entity_claims"));

        callAndStopOnFailure(AddAudAsPaymentInitiationUriToRequestObject.class, "BrazilOB-6.1");

        callAndStopOnFailure(AddIssAsCertificateOuToRequestObject.class, "BrazilOB-6.1");

        callAndStopOnFailure(AddJtiAsUuidToRequestObject.class, "BrazilOB-6.1");
    }

    private void callPixSecondBlock() {
        call(exec().unmapKey("request_object_claims"));

        callAndStopOnFailure(FAPIBrazilSignPaymentInitiationRequest.class);

        callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
    }
}
