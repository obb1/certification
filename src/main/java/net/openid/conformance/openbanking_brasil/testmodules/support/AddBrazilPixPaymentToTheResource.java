package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.DayOfWeekEnum;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.DateTimeException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class AddBrazilPixPaymentToTheResource extends AbstractCondition {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String SCHEDULED_TIME = "1500";
	private JsonObject creditorAccount;
	private String localInstrument;
	private String amount;
	private String currency;
	private JsonElement ibgeTownCode;
	private JsonElement qrCode;
	private JsonElement proxy;
	private JsonObject schedule;

	private enum Periodicity {
		DAILY,
		WEEKLY,
		MONTHLY
	}

	@Override
	@PostEnvironment(strings = {"endToEndId", "ispb"})
	public Environment evaluate(Environment env) {
		extractPaymentDataFromConsent(env);
		JsonObject pixPayment = env.getString("payment_is_array") == null ? createPaymentDataAsJsonObject(env, getFormattedDateTimes(env).get(0)) : createPaymentDataAsJsonArray(env);
		env.putObject("resource", "brazilPixPayment", pixPayment);
		logSuccess("Hardcoded brazilPixPayment object was added to the resource", pixPayment);
		return env;
	}

	protected void extractPaymentDataFromConsent(Environment env) {
		JsonObject resource = env.getObject("resource");
		if (resource == null) {
			resource = Optional.ofNullable(env.getElementFromObject("config", "resource"))
				.orElseThrow(() -> error("Could not find resource object"))
				.getAsJsonObject();
		}

		JsonObject consentPayment = Optional.ofNullable(resource.getAsJsonObject("brazilPaymentConsent"))
			.map(brazilPaymentConsent -> brazilPaymentConsent.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("payment"))
			.orElseThrow(() -> error("Could not extract the payment resource"))
			.getAsJsonObject();

		amount = OIDFJSON.getString(
			Optional.ofNullable(consentPayment.get("amount"))
				.orElseThrow(() -> error("amount field is missing in the payment object", consentPayment))
		);

		currency = OIDFJSON.getString(
			Optional.ofNullable(consentPayment.get("currency"))
				.orElseThrow(() -> error("currency field is missing in the payment object", consentPayment))
		);

		ibgeTownCode = consentPayment.get("ibgeTownCode");
		schedule = consentPayment.getAsJsonObject("schedule");

		JsonObject consentDetails = Optional.ofNullable(consentPayment.get("details"))
			.orElseThrow(() -> error("details object is missing in the payment", consentPayment))
			.getAsJsonObject();

		localInstrument = OIDFJSON.getString(
			Optional.ofNullable(consentDetails.get("localInstrument"))
				.orElseThrow(() -> error("localInstrument field is missing in the details", consentDetails))
		);

		creditorAccount = Optional.ofNullable(consentDetails.get("creditorAccount"))
			.orElseThrow(() -> error("creditorAccount object is missing in the details", consentDetails))
			.getAsJsonObject();

		qrCode = consentDetails.get("qrCode");
		proxy = consentDetails.get("proxy");
	}

	protected JsonObject createPaymentDataAsJsonObject(Environment env, String formattedDateTime) {
		String endToEndId = generateEndToEndId(env, formattedDateTime);

		JsonObjectBuilder pixPaymentBuilder = new JsonObjectBuilder().addFields("data", Map.of(
			"endToEndId", endToEndId,
			"creditorAccount", creditorAccount,
			"localInstrument", localInstrument,
			"remittanceInformation", DictHomologKeys.PROXY_EMAIL_STANDARD_REMITTANCEINFORMATION,
			"cnpjInitiator", DictHomologKeys.PROXY_CNPJ_INITIATOR
		));
		if (ibgeTownCode != null) {
			pixPaymentBuilder.addField("data.ibgeTownCode", OIDFJSON.getString(ibgeTownCode));
		}
		if (qrCode != null) {
			pixPaymentBuilder.addField("data.qrCode", OIDFJSON.getString(qrCode));
		}
		if (proxy != null) {
			pixPaymentBuilder.addField("data.proxy", OIDFJSON.getString(proxy));
		}
		pixPaymentBuilder.addFields("data.payment", Map.of(
			"amount", amount,
			"currency", currency
		));
		return pixPaymentBuilder.build();
	}

	protected JsonObject createPaymentDataAsJsonArray(Environment env) {
		List<String> formattedDateTimes = getFormattedDateTimes(env);
		JsonArray data = new JsonArray();

		for (String formattedDateTime : formattedDateTimes) {
			JsonObject singlePaymentData = createPaymentDataAsJsonObject(env, formattedDateTime)
				.getAsJsonObject("data");
			data.add(singlePaymentData);
		}

		JsonObject pixPayment = new JsonObject();
		pixPayment.add("data", data);

		return pixPayment;
	}

	protected List<String> getFormattedDateTimes(Environment env) {
		OffsetDateTime currentDateTime;
		List<String> formattedDateTimes = new ArrayList<>();
		if (schedule == null) {
			currentDateTime = OffsetDateTime.now(ZoneOffset.UTC);
			formattedDateTimes = List.of(currentDateTime.format(FORMATTER));
		} else {
			JsonObject single = schedule.getAsJsonObject("single");
			JsonObject daily = schedule.getAsJsonObject("daily");
			JsonObject weekly = schedule.getAsJsonObject("weekly");
			JsonObject monthly = schedule.getAsJsonObject("monthly");
			JsonObject custom = schedule.getAsJsonObject("custom");

			List<String> dates;
			int quantity = 1;
			if (single != null && single.has("date")) {
				String startDate = OIDFJSON.getString(single.get("date"));
				dates = generateDateList(startDate, quantity, Periodicity.DAILY, null);
			} else if (daily != null && daily.has("startDate") && daily.has("quantity")) {
				String startDate = OIDFJSON.getString(daily.get("startDate"));
				quantity = OIDFJSON.getInt(daily.get("quantity"));
				dates = generateDateList(startDate, quantity, Periodicity.DAILY, null);
			} else if (weekly != null && weekly.has("dayOfWeek") && weekly.has("startDate") && weekly.has("quantity")) {
				String startDate = OIDFJSON.getString(weekly.get("startDate"));
				String dayOfWeek = OIDFJSON.getString(weekly.get("dayOfWeek"));
				startDate = getFirstDateMatchingDayOfWeek(startDate, dayOfWeek);
				quantity = OIDFJSON.getInt(weekly.get("quantity"));
				dates = generateDateList(startDate, quantity, Periodicity.WEEKLY, null);
			} else if (monthly != null && monthly.has("dayOfMonth") && monthly.has("startDate") && monthly.has("quantity")) {
				String startDate = OIDFJSON.getString(monthly.get("startDate"));
				int dayOfMonth = OIDFJSON.getInt(monthly.get("dayOfMonth"));
				startDate = getFirstDateMatchingDayOfMonth(startDate, dayOfMonth);
				quantity = OIDFJSON.getInt(monthly.get("quantity"));
				dates = generateDateList(startDate, quantity, Periodicity.MONTHLY, dayOfMonth);
			} else if (custom != null && custom.has("dates") && custom.has("additionalInformation")) {
				dates = new ArrayList<>();
				JsonArray datesArray = custom.getAsJsonArray("dates");
				for (JsonElement dateElement : datesArray) {
					dates.add(OIDFJSON.getString(dateElement));
				}
			} else {
				throw error("data.payment.schedule is not in a valid format",
					args("schedule", schedule));
			}

			env.putInteger("quantity_of_payments", quantity);

			for (String date : dates) {
				String formattedDateTime = date
					.replaceAll("-", "")
					.concat(SCHEDULED_TIME);
				formattedDateTimes.add(formattedDateTime);
			}
		}

		return formattedDateTimes;
	}

	protected List<String> generateDateList(String startDate, int quantity, Periodicity periodicity, Integer dayOfMonth) {
		List<String> dateList = new ArrayList<>();

		LocalDate currentDate = LocalDate.parse(startDate);

		for (int i = 0; i < quantity; i++) {
			dateList.add(currentDate.format(DATE_FORMATTER));
			switch (periodicity) {
				case DAILY:
					currentDate = currentDate.plusDays(1);
					break;
				case WEEKLY:
					currentDate = currentDate.plusWeeks(1);
					break;
				case MONTHLY:
					if (currentDate.getDayOfMonth() < dayOfMonth) {
						// currentDate = "2023-05-01", dayOfMonth = 31 -> currentDate = "2023-05-31"
						currentDate = currentDate.withDayOfMonth(dayOfMonth);
					} else {
						// currentDate = "2023-04-05", dayOfMonth = 5 -> currentDate = "2023-05-05"
						currentDate = currentDate.plusMonths(1);
						if (currentDate.getDayOfMonth() < dayOfMonth) {
							// currentDate = "2023-03-31", dayOfMonth = 31 -> currentDate = "2023-05-01"
							currentDate = currentDate.plusDays(1);
						}
					}
					break;
			}
		}

		return dateList;
	}

	protected String getFirstDateMatchingDayOfWeek(String dateString, String dayOfWeek) {
		LocalDate startDate = LocalDate.parse(dateString);
		DayOfWeek targetDayOfWeek = DayOfWeek.of(DayOfWeekEnum.valueOf(dayOfWeek).getValue());

		int daysToAdd = (targetDayOfWeek.getValue() - startDate.getDayOfWeek().getValue() + 7) % 7;
		LocalDate firstDateMatching = startDate.plusDays(daysToAdd);

		return firstDateMatching.format(DATE_FORMATTER);
	}

	protected String getFirstDateMatchingDayOfMonth(String dateString, int dayOfMonth) {
		LocalDate startDate = LocalDate.parse(dateString);
		LocalDate firstDateMatching;

		if (startDate.getDayOfMonth() <= dayOfMonth) {
			try {
				// startDate = "2023-05-23", dayOfMonth = 31 -> firstDateMatching = "2023-05-31"
				firstDateMatching = startDate.withDayOfMonth(dayOfMonth);
			} catch (DateTimeException err) {
				// startDate = "2023-06-23", dayOfMonth = 31 -> firstDateMatching = "2023-07-01"
				firstDateMatching = startDate.plusMonths(1).withDayOfMonth(1);
			}
		} else {
			try {
				// startDate = "2023-04-30", dayOfMonth = 29 -> firstDateMatching = "2023-05-29"
				firstDateMatching = startDate.plusMonths(1).withDayOfMonth(dayOfMonth);
			} catch (DateTimeException err) {
				// startDate = "2023-01-30", dayOfMonth = 29 -> firstDateMatching = "2023-03-01"
				firstDateMatching = startDate.plusMonths(2).withDayOfMonth(1);
			}
		}

		return firstDateMatching.format(DATE_FORMATTER);
	}

	protected String generateEndToEndId(Environment env, String formattedDateTime) {
		String ispb = DictHomologKeys.PROXY_CNPJ_INITIATOR.substring(0, 8);
		String randomString = RandomStringUtils.randomAlphanumeric(11);
		String endToEndId = String.format("E%s%s%s", ispb, formattedDateTime, randomString);
		env.putString("endToEndId", endToEndId);
		env.putString("ispb", ispb);
		return endToEndId;
	}
}
