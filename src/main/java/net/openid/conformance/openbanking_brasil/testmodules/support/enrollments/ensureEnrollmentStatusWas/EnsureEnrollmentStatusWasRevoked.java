package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentStatusEnum;

public class EnsureEnrollmentStatusWasRevoked extends AbstractEnsureEnrollmentStatusWas {

    @Override
    protected String getExpectedStatus() {
        return EnrollmentStatusEnum.REVOKED.toString();
    }
}
