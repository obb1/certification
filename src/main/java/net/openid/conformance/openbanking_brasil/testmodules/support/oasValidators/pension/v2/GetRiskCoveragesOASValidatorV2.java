package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.pension.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("Pension Risk Coverages V2")
public class GetRiskCoveragesOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/pension/swagger-pension-2.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/risk-coverages";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		for(JsonElement dataElement : body.getAsJsonArray("data")) {
			JsonObject data = dataElement.getAsJsonObject();
			assertCoveragesTypeAdditionalInfosConstraints(data);
			assertContributionPaymentConstraints(data);
		}
	}

	protected void assertCoveragesTypeAdditionalInfosConstraints(JsonObject data) {
		JsonArray coverages = JsonPath.read(data, "$.society.products[*].coverages[*]");
		for(JsonElement coverageElement : coverages) {
			this.assertField1IsRequiredWhenField2HasValue2(
				coverageElement.getAsJsonObject(),
				"typeAdditionalInfos",
				"type",
				"OUTROS"
			);
		}
	}

	protected void assertContributionPaymentConstraints(JsonObject data) {
		JsonArray contributionPayments = JsonPath.read(data, "$.society.products[*].contributionPayment");
		for(JsonElement contributionPaymentElement : contributionPayments) {
			JsonObject contributionPayment = contributionPaymentElement.getAsJsonObject();

			this.assertField1IsRequiredWhenField2HasValue2(
				contributionPayment,
				"contributionPaymentMethodAdditionalInfo",
				"contributionPaymentMethod",
				"OUTROS"
			);

			this.assertField1IsRequiredWhenField2HasValue2(
				contributionPayment,
				"contributionPeriodicityAdditionalInfo",
				"contributionPeriodicity",
				"OUTROS"
			);
		}
	}
}
