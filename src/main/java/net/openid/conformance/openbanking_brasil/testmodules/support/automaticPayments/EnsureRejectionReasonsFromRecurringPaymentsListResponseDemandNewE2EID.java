package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.v2.RecurringPaymentsRejectionReasonEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;
import java.util.Set;

public class EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject body = extractBodyFromEnv(env);
		JsonArray data = body.getAsJsonArray("data");

		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject();
			RecurringPaymentsRejectionReasonEnumV2 code = getRejectionReasonCodeFromPayment(payment);
			validateRejectionReasonCode(code);
		}
		logSuccess("All recurring payments from the list have rejectionReason codes that demand a new E2EID");

		return env;
	}

	private JsonObject extractBodyFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	private RecurringPaymentsRejectionReasonEnumV2 getRejectionReasonCodeFromPayment(JsonObject payment) {
		String codeStr = Optional.ofNullable(payment.getAsJsonObject("rejectionReason"))
			.map(rejectionReason -> rejectionReason.get("code"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find rejectionReason code inside of payment", args("payment", payment)));
		try {
			return RecurringPaymentsRejectionReasonEnumV2.valueOf(codeStr);
		} catch(IllegalArgumentException e) {
			throw error("Invalid rejectionReason code", args("code", codeStr));
		}
	}

	private void validateRejectionReasonCode(RecurringPaymentsRejectionReasonEnumV2 code) {
		if (!getRejectionReasonCodesThatDemandNewE2EID().contains(code)) {
			throw error("The rejectionReason code inside of payment does not demand a new E2EID",
				args("code", code, "expected codes", getRejectionReasonCodesThatDemandNewE2EID()));
		}
		log("The current payment has a rejectionReason code that demands a new E2EID");
	}

	private Set<RecurringPaymentsRejectionReasonEnumV2> getRejectionReasonCodesThatDemandNewE2EID() {
		return Set.of(
			RecurringPaymentsRejectionReasonEnumV2.NAO_INFORMADO,
			RecurringPaymentsRejectionReasonEnumV2.PAGAMENTO_RECUSADO_SPI,
			RecurringPaymentsRejectionReasonEnumV2.FALHA_INFRAESTRUTURA_SPI,
			RecurringPaymentsRejectionReasonEnumV2.FALHA_INFRAESTRUTURA_ICP,
			RecurringPaymentsRejectionReasonEnumV2.FALHA_INFRAESTRUTURA_PSP_RECEBEDOR
		);
	}
}
