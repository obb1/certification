package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import org.springframework.http.HttpMethod;

public class PostFidoSignOptionsOASValidatorV2 extends AbstractEnrollmentsOASValidatorV2 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}/fido-sign-options";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
