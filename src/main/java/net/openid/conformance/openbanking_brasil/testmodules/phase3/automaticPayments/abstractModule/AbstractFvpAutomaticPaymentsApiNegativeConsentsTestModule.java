package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SaveConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveSweepingField;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.AbstractEnsureConsentResponseCode;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilAutomaticPaymentRecurringConsentProdValues;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpStatus;

public abstract class AbstractFvpAutomaticPaymentsApiNegativeConsentsTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> postPaymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetRecurringPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilAutomaticPaymentRecurringConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		runTestBlock(
			"POST consents without the recurringConfiguration.sweeping field - Expects 400 or 422 PARAMETRO_NAO_INFORMADO",
			EditRecurringPaymentsConsentBodyToRemoveSweepingField.class,
			EnsureConsentResponseCodeWas400Or422.class,
			EnsureErrorResponseCodeFieldWasParametroNaoInformado.class
		);

		runTestBlock(
			"POST consents with the startDateTime as D+10 and expirationDateTime as D+5 - Expects 422 PARAMETRO_INVALIDO",
			EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate.class,
			EnsureConsentResponseCodeWas422.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);

		eventLog.startBlock("POST consents without the x-fapi-interaction-id - Expects 400");
		callAndStopOnFailure(LoadConsentsBody.class);
		call(getPaymentsConsentSequence()
			.skip(CreateRandomFAPIInteractionId.class, "Will not create x-fapi-interaction-id header")
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Will not create x-fapi-interaction-id header")
			.replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas400.class)));
		callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		eventLog.startBlock("POST consents with the x-fapi-interaction-id as '123456' - Expects 400 or 422 PARAMETRO_INVALIDO");
		callAndStopOnFailure(LoadConsentsBody.class);
		call(getPaymentsConsentSequence()
			.replace(CreateRandomFAPIInteractionId.class, condition(CreateInvalidFAPIInteractionId.class))
			.replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas400Or422.class)));
		callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
		if(OIDFJSON.getInt(env.getObject("consent_endpoint_response_full").get("status")) == HttpStatus.UNPROCESSABLE_ENTITY.value()){
			env.mapKey("endpoint_response", "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
			env.unmapKey("endpoint_response");
		}
		eventLog.endBlock();
	}

	protected void runTestBlock(String blockHeader,
								Class<? extends AbstractCondition> editBodyCondition,
								Class<? extends AbstractEnsureConsentResponseCode> ensureResponseCodeCondition,
								Class<? extends AbstractEnsureErrorResponseCodeFieldWas> ensureErrorResponseCondition) {
		eventLog.startBlock(blockHeader);
		callAndStopOnFailure(LoadConsentsBody.class);
		callAndStopOnFailure(editBodyCondition);
		call(getPaymentsConsentSequence().replace(EnsureConsentResponseCodeWas201.class, condition(ensureResponseCodeCondition)));
		callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
		if (OIDFJSON.getInt(env.getObject("consent_endpoint_response_full").get("status")) == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(ensureErrorResponseCondition, Condition.ConditionResult.FAILURE);
			env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		}
		eventLog.endBlock();
	}

	@Override
	protected void configureDummyData() {
		super.configureDummyData();
		callAndStopOnFailure(SaveConsentsBody.class);
	}
}
