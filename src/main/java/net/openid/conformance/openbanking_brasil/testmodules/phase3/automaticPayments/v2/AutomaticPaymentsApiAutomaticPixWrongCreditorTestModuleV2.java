package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiAutomaticPixWrongCreditorTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_automatic-pix-wrong-creditor_test-module_v2",
	displayName = "automatic-payments_api_automatic-pix-wrong-creditor_test-module_v2",
	summary = "Ensure the first payment is not accepted if the creditorAccount is not for the informed creditor\n" +
		"• Call the POST recurring-consents endpoint with automatic pix fields, with referenceStartDate as D+1, interval as SEMANAL, a fixed Amount 1 BRL, and sending firstPayment information with amount as 1 BRL, date as D+0, and creditorAccount account number and ispb as random numbers\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the POST recurring-payments endpoint with the amount as 1 BRL, and creditorAccount number and ispb as a defined at the consent\n" +
		"• Expect 201 - Validate Response\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD or ACCP\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is TITULARIDADE_INCONSISTENTE\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.contractDebtorName",
		"resource.contractDebtorIdentification",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName",
		"resource.creditorCpfCnpj"
	}
)
public class AutomaticPaymentsApiAutomaticPixWrongCreditorTestModuleV2 extends AbstractAutomaticPaymentsApiAutomaticPixWrongCreditorTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}
}
