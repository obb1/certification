package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetInvalidCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-invalid-creditor_test-module_v2",
	displayName = "automatic-payments_api_sweeping-accounts-invalid-creditor_test-module_v2",
	summary = "Ensure a consent for sweeping accounts cannot be created with invalid creditor account fields\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending the logged user as provided on the config, and the creditor as a different one\n" +
		"• Expect 422 DETALHE_PAGAMENTO_INVALIDO - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV2 extends AbstractAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentErrorValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected void performTestSteps() {
		performTestStep(
			"POST consents sending the logged user not matching the creditor - Expects 422 DETALHE_PAGAMENTO_INVALIDO",
			EditRecurringPaymentsConsentBodyToSetInvalidCreditor.class,
			EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class
		);
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class));
	}
}
