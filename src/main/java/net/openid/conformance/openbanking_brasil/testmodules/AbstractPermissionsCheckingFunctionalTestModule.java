package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;

import java.util.Optional;

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public abstract class AbstractPermissionsCheckingFunctionalTestModule extends AbstractOBBrasilFunctionalTestModule {

	private boolean isFirstConsent = true;


	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(RememberOriginalScopes.class);
		prepareCorrectConsents();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps();
	}

	protected boolean preFetchResources = false;

	@Override
	protected void requestProtectedResource() {
		if(!preFetchResources) {
			preFetchResources = true;
			super.requestProtectedResource();
			eventLog.startBlock(currentClientString() + "Validate response");
			preFetchResources();
			callAndStopOnFailure(ResetScopesToConfigured.class);
			prepareIncorrectPermissions();
			performAuthorizationFlow();
			eventLog.endBlock();
		}
	}

	boolean preFetched = false;

	@Override
	protected void onPostAuthorizationFlowComplete() {

		if(!preFetched) {
			preFetched = true;
			return;
		}

		requestResourcesWithIncorrectPermissions();

		fireTestFinished();

	}

	@Override
	protected void performPreAuthorizationSteps() {
		if (!isFirstConsent) {
			deleteConsent();
		} else {
			isFirstConsent = false;
		}
		super.performPreAuthorizationSteps();
		callAndContinueOnFailure(SaveConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
	}

	private void deleteConsent() {
		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(LoadConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToFetchConsentRequest.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	public void cleanup() {
		deleteConsent();
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		if(!getName().startsWith("opin")) {

			String resourceResponse = "resource_endpoint_response_full";
			String consentResponse = "consent_endpoint_response_full";
			String methodString = env.getString("http_method");
			HttpMethod method;
			if (methodString != null) {
				method = HttpMethod.valueOf(methodString);
				if (method.equals(HttpMethod.DELETE)) {
					return;
				}
			}

			if (CallProtectedResource.class.equals(builder.getConditionClass())) {
				if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
					validateLinksAndMeta(resourceResponse);
				} else {
					validateLinksAndMeta(consentResponse);
				}
			}
			if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
				CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass())) {
				validateLinksAndMeta("consent_endpoint_response_full");
			}
		}
	}

	private void validateLinksAndMeta(String responseFull) {

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
			.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
		if (!isEndpointCallSuccessful(status)) {
			return;
		}

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
		if (recursion.equals("false")) {
			callAndStopOnFailure(SaveOldValues.class);
			env.putString("recursion", "true");
			env.mapKey("resource_endpoint_response_full", responseFull);

			if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
				validateLinks(responseFull);
			}

			env.unmapKey("resource_endpoint_response_full");
			callAndStopOnFailure(LoadOldValues.class);
			env.putString("recursion", "false");
		}
	}

	private void validateLinks(String responseFull) {
		call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
		env.mapKey("resource_endpoint_response_full", responseFull);

		ConditionSequence sequence = new ValidateSelfEndpoint();

		if (responseFull.contains("consent")) {
			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
		}

		call(sequence);
	}

	private boolean isEndpointCallSuccessful(int status) {
		if (status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}

	protected abstract void preFetchResources();
	protected abstract void prepareCorrectConsents();
	protected abstract void prepareIncorrectPermissions();

	protected abstract void requestResourcesWithIncorrectPermissions();

	@Override
	protected final void validateResponse() {

	}



}
