package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.ExtractSpecifiedResourceApiAccountIdsFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountBalances;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactionLimit;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactionsCurrent;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddSpecifiedPageSizeParameterToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromBookingDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureOnlyOneRecordWasReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractAllSpecifiedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.FetchSpecifiedNumberOfExtractedApiIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetSpecifiedValueToSpecifiedUrlParameter;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateNumberOfRecordsPage1;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateNumberOfRecordsPage2;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIsAtLeast20FromData;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public abstract class AbstractAccountsApiOperationalLimitsTestModulePhase2 extends AbstractOperationalLimitsTestModule {


	protected abstract Class<? extends Condition> getAccountListValidator();

	protected abstract Class<? extends Condition> getAccountIdentificationResponseValidator();

	protected abstract Class<? extends Condition> getAccountTransactionsValidator();

	protected abstract Class<? extends Condition> getAccountTransactionsCurrentValidator();

	protected abstract Class<? extends Condition> getAccountBalancesResponseValidator();

	protected abstract Class<? extends Condition> getAccountLimitsValidator();

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final int REQUIRED_NUMBER_OF_RECORDS = 20;
	private static final String API_RESOURCE_ID = "accountId";
	private static final int NUMBER_OF_IDS_TO_FETCH = 2;

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetAccountsV2Endpoint.class),
			condition(GetConsentV3Endpoint.class)
		)));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putString("api_type", EnumResourcesType.ACCOUNT.name());
		callAndContinueOnFailure(ExtractSpecifiedResourceApiAccountIdsFromConfig.class, Condition.ConditionResult.INFO);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ACCOUNTS).addScopes(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.OPEN_ID).build();
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}


	@Override
	protected void requestProtectedResource() {
		JsonArray fetchedApiIds = prepareApiIds();

		for (int i = 0; i < NUMBER_OF_IDS_TO_FETCH; i++) {

			// If during the second resource test part, fetchedApiIds size is less than 2 it means that the second account ID was not provided by the client.
			// In that case we assume that the multiple accounts for the same users feature is not supported, therefore we skip the part of the test with the second resource ID.
			if (i == 1 && fetchedApiIds.size() < NUMBER_OF_IDS_TO_FETCH) {
				eventLog.log(getName(), "Could not find second account ID. Skipping part of the test with the second resource ID");
				continue;
			}

			String accountId = OIDFJSON.getString(fetchedApiIds.get(i));
			env.putString(API_RESOURCE_ID, accountId);
			accountsOperationalLimitCalls();
		}
	}

	private JsonArray prepareApiIds() {
		JsonArray fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");

		// IF no resource IDs were provided in the config, we will fetch instead
		if (fetchedApiIds.isEmpty()) {

			preCallProtectedResource("Fetching Accounts");
			// Validate accounts  response
			runInBlock("Validating Accounts response", () -> {
				callAndStopOnFailure(getAccountListValidator());

				env.putString("apiIdName", API_RESOURCE_ID);
				callAndStopOnFailure(ExtractAllSpecifiedApiIds.class);

				JsonArray extractedApiIds = env.getObject("extracted_api_ids").getAsJsonArray("extractedApiIds");

				if (extractedApiIds.size() >= NUMBER_OF_IDS_TO_FETCH) {
					env.putInteger("number_of_ids_to_fetch", NUMBER_OF_IDS_TO_FETCH);
				} else {
					env.putInteger("number_of_ids_to_fetch", 1);
				}

				callAndStopOnFailure(FetchSpecifiedNumberOfExtractedApiIds.class);
			});

			fetchedApiIds = env.getObject("fetched_api_ids").getAsJsonArray("fetchedApiIds");
		}
		return fetchedApiIds;
	}

	@Override
	protected void validateResponse() {
		// Not needed for this test
	}

	private void accountsOperationalLimitCalls() {
		//Call Account resource
		callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);

		for (int i = 0; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Fetching First Account", i + 1));
			if (i == 0) {
				runInLoggingBlock(() -> runInBlock("Validate Account Response",
					() -> callAndContinueOnFailure(getAccountIdentificationResponseValidator(), Condition.ConditionResult.FAILURE)));

			}
		}

		makeOverOlCall("Account");

		enableLogging();
		env.putString("metaOnlyRequestDateTime", "true");
		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactions.class);

		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromBookingDate", currentDate.minusDays(6).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));

		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);

		for (int i = 0; i < 4; i++) {
			preCallProtectedResource(String.format("[%d] Fetching transactions with booking date parameters", i + 1));

			if (i == 0) {
				runInLoggingBlock(() -> runInBlock("Validate Account Transactions Response", () -> {
					callAndContinueOnFailure(getAccountTransactionsValidator(), Condition.ConditionResult.FAILURE);
					callAndStopOnFailure(EnsureNumberOfTotalRecordsIsAtLeast20FromData.class);
				}));
			}
		}

		makeOverOlCall("Transactions");

		enableLogging();
		env.putString("metaOnlyRequestDateTime", "false");
		callAndStopOnFailure(PrepareUrlForFetchingAccountBalances.class);

		for (int i = 0; i < 420; i++) {
			preCallProtectedResource(String.format("[%d] Fetching balances", i + 1));
			validateFields(i, "Validate Account Transactions Balances", getAccountBalancesResponseValidator());
		}

		makeOverOlCall("Balances");

		enableLogging();
		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactionLimit.class);

		for (int i = 0; i < 420; i++) {
			preCallProtectedResource(String.format("[%d] Fetching accounts limits", i + 1));
			validateFields(i, "Validate accounts limits Balances", getAccountLimitsValidator());
		}

		makeOverOlCall("Trans. Limits");

		enableLogging();
		env.putString("metaOnlyRequestDateTime", "true");
		callAndStopOnFailure(PrepareUrlForFetchingAccountTransactionsCurrent.class);

		for (int i = 0; i < 239; i++) {
			preCallProtectedResource(String.format("[%d] Fetching Account Transactions current", i + 1));
			validateFields(i, "Validate Account Transactions current", getAccountTransactionsCurrentValidator());
		}

		enableLogging();

		env.putString("fromBookingDate", currentDate.minusDays(6).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));

		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		env.putInteger("required_page_size", 1);
		callAndContinueOnFailure(AddSpecifiedPageSizeParameterToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);

		enableLogging();
		for (int i = 0; i < REQUIRED_NUMBER_OF_RECORDS; i++) {
			eventLog.startBlock(String.format("[%d] Fetching Accounts Transactions Current next link", i + 1));
			preCallProtectedResource();

			if (i == 0) {
				callAndContinueOnFailure(getAccountTransactionsCurrentValidator(), Condition.ConditionResult.FAILURE);
				callAndStopOnFailure(ValidateNumberOfRecordsPage1.class);
			}

			if (i == 1) {
				callAndStopOnFailure(ValidateNumberOfRecordsPage2.class);
			}

			callAndStopOnFailure(EnsureOnlyOneRecordWasReturned.class);
			callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);

			env.putString("value", "1");
			env.putString("parameter", "page-size");
			callAndStopOnFailure(SetSpecifiedValueToSpecifiedUrlParameter.class);

			eventLog.endBlock();
			disableLogging();
		}

		makeOverOlCall("Trans. Current");
		enableLogging();
	}

	private void validateFields(int i, String message, Class<? extends Condition> conditionClass) {
		if (i == 0) {
			runInLoggingBlock(() -> runInBlock(message, () -> callAndStopOnFailure(conditionClass, Condition.ConditionResult.FAILURE)));
		}
	}

}
