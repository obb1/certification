package net.openid.conformance.openbanking_brasil.testmodules.support.validateField;

public class ValidateEnrollmentsFieldV2 extends AbstractValidateEnrollmentField {

	@Override
	protected int getEnrollmentVersion() {
		return 2;
	}

	@Override
	protected int getConsentVersion() {
		return 4;
	}
}
