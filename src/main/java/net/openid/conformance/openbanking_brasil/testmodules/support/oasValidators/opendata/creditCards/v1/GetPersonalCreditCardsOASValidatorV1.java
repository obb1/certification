package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.creditCards.v1;

public class GetPersonalCreditCardsOASValidatorV1 extends AbstractGetOpenDataCreditCardsOASValidatorV1 {

	@Override
	protected String apiPrefix() {
		return "personal";
	}
}
