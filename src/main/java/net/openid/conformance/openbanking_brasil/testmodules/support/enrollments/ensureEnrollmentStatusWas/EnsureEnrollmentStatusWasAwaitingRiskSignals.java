package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentStatusEnum;

public class EnsureEnrollmentStatusWasAwaitingRiskSignals extends AbstractEnsureEnrollmentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return EnrollmentStatusEnum.AWAITING_RISK_SIGNALS.toString();
	}
}
