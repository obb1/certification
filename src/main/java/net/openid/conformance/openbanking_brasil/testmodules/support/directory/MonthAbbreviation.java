package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public enum MonthAbbreviation {
	JAN("JANUARY"),
	FEB("FEBRUARY"),
	MAR("MARCH"),
	APR("APRIL"),
	MAY("MAY"),
	JUN("JUNE"),
	JUL("JULY"),
	AUG("AUGUST"),
	SEP("SEPTEMBER"),
	OCT("OCTOBER"),
	NOV("NOVEMBER"),
	DEC("DECEMBER");

	private final String fullName;

	MonthAbbreviation(String fullName) {
		this.fullName = fullName.toUpperCase();
	}

	public String getFullName() {
		return fullName;
	}
}
