package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractChangeDebtorAccountType extends AbstractCondition {

	protected abstract String getAccountType(Environment env);

	@Override
	public Environment evaluate(Environment env) {

		JsonElement debtorElem = env.getElementFromObject("config", "resource.brazilPaymentConsent.data.debtorAccount");
		if (debtorElem == null) {
			throw error("Debtor object not found in config");
		}

		JsonObject debtor = debtorElem.getAsJsonObject();
		debtor.addProperty("accountType", getAccountType(env));
		env.putObject("config", "resource.brazilPaymentConsent.data.debtorAccount", debtor);

		logSuccess(String.format("debtor accountType changed to %s", getAccountType(env)), debtor);
		return env;
	}
}
