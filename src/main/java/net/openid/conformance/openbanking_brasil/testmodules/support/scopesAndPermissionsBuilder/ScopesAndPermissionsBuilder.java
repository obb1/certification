package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.CategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.GroupingEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.PermissionsEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.ScopesEnum;
import net.openid.conformance.testmodule.Environment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The `ScopesAndPermissionsBuilder` class provides methods to construct and manage scopes and permissions for requests.
 * This builder follows the builder pattern, allowing you to chain method calls to set scopes and permissions
 * before finally building the request.
 * <p>
 * Example usage:
 * <pre>{@code
 * ScopesAndPermissionsBuilder builder = new ScopesAndPermissionsBuilder(environment, eventLog);
 * builder.addScopes(ScopesEnum.CUSTOMERS, ScopesEnum.RESOURCES)
 *        .addPermissions(PermissionsEnum.READ)
 *        .build();
 * }</pre>
 */
public class ScopesAndPermissionsBuilder {
	private Set<String> permissionsSet;
	private Set<String> scopesSet;
	protected Environment env;
	protected TestInstanceEventLog eventLog;

	private boolean setsSet = false;

	/**
	 * Constructs a new `ScopesAndPermissionsBuilder` instance with environment and eventLog.
	 */
	public ScopesAndPermissionsBuilder(Environment env, TestInstanceEventLog eventLog) {

		this.eventLog = eventLog;
		this.env = env;

		eventLog.log("Scopes and Permissions Builder","ScopesAndPermissionsBuilder was correctly instantiated");
	}

	/**
	 * Resets the builder, clearing all scopes and permissions.
	 */
	public ScopesAndPermissionsBuilder reset() {
		this.permissionsSet = new HashSet<>();
		this.scopesSet = new HashSet<>();
		setsSet = true;
		return this;
	}

	private boolean isSetsSet() {
		return setsSet;
	}

	/**
	 * Adds all permissions of the specified permission group(s) to the builder's permissionsSet.
	 * @param groups Permissions Groups defined in GroupingEnum.
	 */
	public ScopesAndPermissionsBuilder addPermissionsBasedOnGrouping(GroupingEnum... groups) {
		if(!isSetsSet()) {
			reset();
		}
		for (GroupingEnum group : groups) {
			Set<String> permissions = group.getPermissions().stream()
				.map(PermissionsEnum::getPermission)
				.collect(Collectors.toSet());
			permissionsSet.addAll(permissions);
		}
		return  this;
	}

	/**
	 * Adds all scopes of the specified permission group(s) to the builder's permissionsSet.
	 * @param groups Permissions Groups defined in GroupingEnum.
	 */
	public ScopesAndPermissionsBuilder addScopesBasedOnGrouping(GroupingEnum... groups) {
		if(!isSetsSet()) {
			reset();
		}
		for (GroupingEnum group : groups) {
			Set<ScopesEnum> groupScopes = group.getScopes();
			groupScopes.forEach(scope -> scopesSet.add(scope.getScope()));
		}
		return  this;
	}

	/**
	 * Adds all permissions of the specified permission category to the builder's permissionsSet.
	 * @param productCategory Permissions Groups defined in CategoryEnum.
	 */
	public ScopesAndPermissionsBuilder addPermissionsBasedOnCategory(CategoryEnum... productCategory) {
		if(!isSetsSet()) {
			reset();
		}
		for (CategoryEnum category : productCategory) {
			permissionsSet.addAll(category.getPermissions().stream().map(PermissionsEnum::getPermission).collect(Collectors.toSet()));
		}
		return  this;
	}

	/**
	 * Adds all scopes of the specified permission category to the builder's permissionsSet.
	 * @param categories Permissions Groups defined in CategoryEnum.
	 */
	public ScopesAndPermissionsBuilder addScopesBasedOnCategory(CategoryEnum... categories) {
		if(!isSetsSet()) {
			reset();
		}

		for (CategoryEnum category : categories) {
			Set<ScopesEnum> categoryScopes = category.getScopes();
			categoryScopes.forEach(scope -> scopesSet.add(scope.getScope()));
		}

		return  this;
	}

	/**
	 * Add singles permissions to the builder's permissionsSet. Be aware that doing a POST Consent without a complete permission group may return an error.
	 * @param permissions Permissions defined in PermissionsEnum.
	 */
	public ScopesAndPermissionsBuilder addPermissions(PermissionsEnum... permissions) {
		if(!isSetsSet()) {
			reset();
		}
		for (PermissionsEnum permission : permissions) {
			permissionsSet.add(permission.getPermission());
		}
		return this;
	}

	/**
	 * Add singles scopes to the builder's scopesSet.
	 * @param scopes Scopes defined in ScopesEnum.
	 */
	public ScopesAndPermissionsBuilder addScopes(ScopesEnum... scopes) {
		if(!isSetsSet()) {
			reset();
		}
		for (ScopesEnum scope : scopes) {
			scopesSet.add(scope.getScope());
		}

		return  this;
	}

	/**
	 * Remove singles permissions from the builder's permissionsSet. Be aware that doing a POST Consent without a complete permission group may return an error.
	 * @param permissions Permissions defined in PermissionsEnum.
	 */
	public ScopesAndPermissionsBuilder removePermissions(PermissionsEnum... permissions) {

		List<String> permissionsAsStrings = Arrays.stream(permissions)
			.map(PermissionsEnum::getPermission)
			.toList();

		if (!permissionsSet.removeAll(permissionsAsStrings)) {
			eventLog.log("Scopes and Permissions Builder","removePermissions couldn't find specified permissions to remove");
		}

		return this;
	}

	/**
	 * Remove singles permissions from the builder's permissionsSet. Be aware that doing a POST Consent without a complete permission group may return an error.
	 * @param scopes Scopes defined in ScopesEnum.
	 */
	public ScopesAndPermissionsBuilder removeScopes(ScopesEnum... scopes) {

		List<String> scopeNames = Arrays.stream(scopes)
			.map(ScopesEnum::getScope)
			.toList();

		if (!scopesSet.removeAll(scopeNames)) {
			eventLog.log("Scopes and Permissions Builder", "removeScopes couldn't find specified scopes to remove");
		}

		return this;
	}

	/**
	 * Gets the scopes and permissions set in the builder as a map.
	 * The map contains the keys "scopes" and "permissions".
	 *
	 * @return A map containing the scopes and permissions set in the builder.
	 */
	public Map<String, Object> getLogData() {
		Map<String, Object> map = Map.of(
			"scopes", scopesSet,
			"permissions", permissionsSet
		);
		Map<String, Object> copy = new HashMap<>(map);
		copy.put("result", Condition.ConditionResult.SUCCESS);
		copy.put("msg","Providing Permissions/Scopes:");
		return copy;

	}

	/**
	 * Builds the scopes and permissions configuration, setting them in the environment.
	 */
	public void build() {
		env.putString("consent_permissions", String.join(" ", permissionsSet));
		if(!scopesSet.isEmpty()) {
			JsonObject client = env.getObject("client");
			client.addProperty("scope", String.join(" ", scopesSet));
		}
		eventLog.log("Scopes and Permissions Builder:", getLogData());
	}



}
