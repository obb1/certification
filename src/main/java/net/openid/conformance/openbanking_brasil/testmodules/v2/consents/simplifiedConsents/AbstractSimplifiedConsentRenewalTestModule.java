package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ExtractConsentStatusUpdateDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.GetConsentExtensionValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToGetConsentExtension;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateConsent422EstadoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInGetSizeOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateIndefiniteExpirationTimeReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentExtendsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallGetConsentExtensionsSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPostConsentExtensionsExpectingErrorSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPostConsentExtensionsSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractSimplifiedConsentRenewalTestModule extends AbstractOBBrasilFunctionalTestModule {
	@Override
	protected void configureClient(){
		callGetConsentAndOrResourceUrlSequence();
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		super.configureClient();
	}
	protected abstract void callGetConsentAndOrResourceUrlSequence();
	@Override
	protected void validateClientConfiguration() {
		super.validateClientConfiguration();
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS, OPFScopesEnum.OPEN_ID).build();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		env.mapKey("access_token", "client_credentials_token");
		OpenBankingBrazilPreAuthorizationSteps steps = (OpenBankingBrazilPreAuthorizationSteps) new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.replace(FAPIBrazilOpenBankingCreateConsentRequest.class, condition(buildConsentRequestBody()).onFail(Condition.ConditionResult.FAILURE))
			.replace(FAPIBrazilAddExpirationToConsentRequest.class, condition(setConsentExpirationTime()).onFail(Condition.ConditionResult.FAILURE))
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().mapKey("resource_endpoint_response", "consent_endpoint_response"))
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, condition(setConsentCreationValidation()).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure())
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().unmapKey("resource_endpoint_response")).
			insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class)));

		return steps;
	}

	@Override
	protected void performPostAuthorizationFlow() {
		env.unmapKey("access_token");
		createAuthorizationCodeRequest();
		requestAuthorizationCode();
		runInBlock("Validating get consent response", this::callGetConsentEndpoint);
		callExtensionEndpoints();
		fireTestFinished();
	}
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response", this::callPostConsentExtensionEndpoint);

		runInBlock("Validating get consent extension response", this::callGetConsentExtensionEndpoint);
	}
	public ConditionSequence getPostConsentExtensionSequence() {
		ConditionSequence postExtension = new CallPostConsentExtensionsSequence()
			.replace(CreateIndefiniteConsentExpiryTime.class, condition(setExtensionExpirationTime()).onFail(Condition.ConditionResult.FAILURE))
			.replace(ValidateIndefiniteExpirationTimeReturned.class, condition(validateExpirationTimeReturned()).onFail(Condition.ConditionResult.FAILURE))
			.insertAfter(EnsureConsentResponseCodeWas201.class, condition(setPostConsentExtensionValidator()).onFail(Condition.ConditionResult.FAILURE));

		return postExtension;
	}

	protected abstract Class<? extends Condition> setPostConsentExtensionValidator() ;

	public void callPostConsentExtensionEndpoint(){
		call(getPostConsentExtensionSequence());
	}

	//Call GET consent endpoint
	public void callGetConsentEndpoint() {
		env.mapKey("access_token", "client_credentials_token");
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ExtractConsentStatusUpdateDateTime.class);
		call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
		validateGetConsentEndpointResponse();
		call(exec().unmapKey("resource_endpoint_response_full"));
		env.unmapKey("access_token");
	}

	public ConditionSequence getConsentExtensionSequence() {
		ConditionSequence getExtensionSequence =  new CallGetConsentExtensionsSequence()
			.replace(PrepareToGetConsentExtension.class, condition(setGetConsentExtensionEndpoint()).onFail(Condition.ConditionResult.FAILURE))
			.replace(ValidateExtensionExpiryTimeInGetSizeOne.class, condition(validateGetConsentExtensionEndpointResponse()).onFail(Condition.ConditionResult.FAILURE))
			.replace(GetConsentExtensionValidatorV2.class, condition(setGetConsentExtensionValidator()).onFail(Condition.ConditionResult.FAILURE));

		return getExtensionSequence;
	}
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtension.class;
	}
	protected Class<? extends Condition> setGetConsentExtensionValidator() {
		return GetConsentExtensionValidatorV2.class;
	}
	public void callGetConsentExtensionEndpoint(){
		env.mapKey("access_token", "client_credentials_token");
		call(getConsentExtensionSequence());
		env.unmapKey("access_token");
	}

	protected abstract Class<? extends Condition> validateExpirationTimeReturned();
	public void validateGetConsentEndpointResponse() {
		callAndContinueOnFailure(setGetConsentValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentWasAuthorised.class, Condition.ConditionResult.FAILURE);
	}
	protected abstract Class<? extends Condition> setGetConsentValidator();
	protected abstract Class<? extends Condition> validateGetConsentExtensionEndpointResponse();

	protected abstract Class<? extends Condition> buildConsentRequestBody();

	protected abstract Class<? extends Condition> setConsentExpirationTime();

	protected abstract Class<? extends Condition> setConsentCreationValidation();

	protected abstract Class<? extends Condition> setExtensionExpirationTime();
	@Override
	protected void validateResponse() {

	}
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void requestAuthorizationCode(){
		super.requestAuthorizationCode();
		checkTokenValue();
	}

	protected void checkTokenValue() {
		callAndStopOnFailure(EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration.class);
	}

	public ConditionSequence getPostConsentExtensionExpectingErrorSequence(Class<? extends Condition> errorCondition, Class<? extends Condition> errorCodeMessageCondition, Class<? extends Condition> expiryTimeCondition) {
		ConditionSequence callPostConsentExtensionsExpectingErrorSequence = new CallPostConsentExtensionsExpectingErrorSequence();
		callPostConsentExtensionsExpectingErrorSequence.replace(CreateIndefiniteConsentExpiryTime.class, condition(expiryTimeCondition).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE));
		callPostConsentExtensionsExpectingErrorSequence.replace(EnsureConsentResponseCodeWas422.class, condition(errorCondition).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE));
		callPostConsentExtensionsExpectingErrorSequence.replace(PostConsentExtendsOASValidatorV3.class, condition(setPostConsentExtensionValidator()).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE));
		if (errorCodeMessageCondition == null) {
			callPostConsentExtensionsExpectingErrorSequence.skip(ValidateConsent422EstadoInvalido.class, "No error code to validate");
		} else {
			callPostConsentExtensionsExpectingErrorSequence.replace(ValidateConsent422EstadoInvalido.class, condition(errorCodeMessageCondition).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE));
		}
		return callPostConsentExtensionsExpectingErrorSequence;
	}
}
