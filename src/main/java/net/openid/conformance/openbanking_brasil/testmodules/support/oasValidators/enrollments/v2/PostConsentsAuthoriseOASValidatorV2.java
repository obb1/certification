package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import org.springframework.http.HttpMethod;

public class PostConsentsAuthoriseOASValidatorV2 extends AbstractEnrollmentsOASValidatorV2 {

	@Override
	protected String getEndpointPathApi() {
		return "/consents";
	}

	@Override
	protected String getEndpointPathSuffix() {
		return "/{consentId}/authorise";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
