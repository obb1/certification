package net.openid.conformance.openbanking_brasil.testmodules.support.enums.consentsV3;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum DeleteConsent422ErrorEnumV3 {

	CONSENTIMENTO_EM_STATUS_REJEITADO;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
