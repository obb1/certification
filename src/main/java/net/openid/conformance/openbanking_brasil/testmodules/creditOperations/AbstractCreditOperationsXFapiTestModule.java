package net.openid.conformance.openbanking_brasil.testmodules.creditOperations;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.CreditOperationsContractSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractCreditOperationsXFapiTestModule  extends AbstractPhase2TestModule {

	protected abstract OPFCategoryEnum getCategory();
	protected abstract OPFScopesEnum getScope();
	protected abstract EnumResourcesType getApiType();
	protected abstract Class<? extends Condition> getGetConsentValidator();
	protected abstract Class<? extends Condition> getContractsListValidator();
	protected abstract Class<? extends Condition> getContractIdentificationValidator();
	protected abstract Class<? extends Condition> getContractWarrantiesValidator();
	protected abstract Class<? extends Condition> getContractScheduledInstalmentsValidator();
	protected abstract Class<? extends Condition> getContractPaymentsValidator();

	@Override
	protected void configureClient() {
		super.configureClient();
		env.putString("api_type", getApiType().name());
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(getScope(), OPFScopesEnum.OPEN_ID).addPermissionsBasedOnCategory(getCategory()).build();
		env.putString("metaOnlyRequestDateTime", "true");
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, sequenceOf(
				condition(getPostConsentValidator()).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure(),
				condition(EnsureConsentWasAwaitingAuthorisation.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure()
			));
	}

	@Override
	protected void performPostAuthorizationFlow() {
		runInBlock("Validating get consent response - Expects 200 with AUTHORISED status", () -> {
			call(getGetConsentSequence());
			callAndStopOnFailure(EnsureConsentWasAuthorised.class);
		});
		super.performPostAuthorizationFlow();
	}

	@Override
	protected void requestProtectedResource() {
		testInteractionIdScenariosForResourceEndpoint("/contracts", getContractsListValidator());
		super.requestProtectedResource();
	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(getContractsListValidator(), Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(CreditOperationsContractSelector.class);

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContract.class);
		testInteractionIdScenariosForResourceEndpoint("/contracts/{contractId}", getContractIdentificationValidator());

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContractGuarantees.class);
		testInteractionIdScenariosForResourceEndpoint("/contracts/{contractId}/warranties", getContractWarrantiesValidator());

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContractInstallments.class);
		testInteractionIdScenariosForResourceEndpoint("/contracts/{contractId}/scheduled-instalments", getContractScheduledInstalmentsValidator());

		callAndStopOnFailure(PrepareUrlForFetchingCreditOperationsContractPayments.class);
		testInteractionIdScenariosForResourceEndpoint("/contracts/{contractId}/payments", getContractPaymentsValidator());
	}

	protected void testInteractionIdScenariosForResourceEndpoint(String endpoint, Class<? extends Condition> resourceValidator) {
		callResourceEndpointWithoutInteractionId(endpoint, resourceValidator);
		callResourceEndpointWithInvalidInteractionId(endpoint, resourceValidator);
	}

	protected void callResourceEndpointWithoutInteractionId(String endpoint, Class<? extends Condition> resourceValidator) {
		runInBlock(String.format("Calling %s endpoint without x-fapi-interaction-id", endpoint), () -> {
			call(getGetResourceSequence(resourceValidator)
				.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Will not add x-fapi-interaction-id"));
		});
	}

	protected void callResourceEndpointWithInvalidInteractionId(String endpoint, Class<? extends Condition> resourceValidator) {
		runInBlock(String.format("Calling %s endpoint with an invalid x-fapi-interaction-id", endpoint), () -> {
			env.putString("fapi_interaction_id", "a-bad-fapi-interaction-id");
			call(getGetResourceSequence(resourceValidator));
		});
	}

	protected ConditionSequence getGetResourceSequence(Class<? extends Condition> resourceValidator) {
		return sequenceOf(
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
			condition(CallProtectedResource.class),
			condition(EnsureResourceResponseCodeWas400.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure(),
			condition(CheckForFAPIInteractionIdInResourceResponse.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure(),
			condition(resourceValidator).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure()
		);
	}

	protected ConditionSequence getGetConsentSequence() {
		return sequenceOf(
			condition(PrepareToFetchConsentRequest.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class),
			condition(EnsureConsentResponseCodeWas200.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure(),
			condition(getGetConsentValidator()).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure()
		);
	}
}
