package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseDoesNotHaveLinks;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractFidoRegistrationOptionsChallenge;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ExtractSoftwareOriginUriFromDirectorySSASteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractUserIdFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoAttestationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateKeyPairFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateFidoRegistrationRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistration;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistrationOptions;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractCompleteEnrollmentsApiEnrollmentTestModule extends AbstractEnrollmentsApiTestModule {

	/**
	 * This class makes the steps:
	 * - GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array
	 * - Call the POST enrollments endpoint
	 * - Expect a 201 response - Validate the response and check if the status is "AWAITING_RISK_SIGNALS"
	 * - Call the POST Risk Signals endpoint sending the appropriate signals
	 * - Expect a 204 response
	 * - Call the GET enrollments endpoint
	 * - Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION
	 * - Redirect the user to authorize the enrollment
	 * - Call the GET enrollments endpoint
	 * - Expect a 201 response - Validate the response and check if the status is "AWAITING_ENROLLMENT"
	 * - Call the POST fido-registration-options endpoint
	 * - Expect a 201 response - Validate the response and extract the challenge
	 * - Create the FIDO Credentials and create the attestationObject
	 * - Call the POST fido-registration endpoint, sending the compliant attestationObject, with the origin extracted on the SSA at the ClientDataJson.origin
	 * - Expect a 204 response
	 * - Call the GET enrollments endpoint
	 * - Expect a 201 response - Validate the response and check if the status is "AUTHORISED"
	 * If you only need the flow until GET enrollments expecting "AWAITING_ENROLLMENT", consider using {@link AbstractEnrollmentsApiTestModule}
	 */

	protected ConditionSequence makeSoftwareOriginUriExtractionSteps() {
		return new ExtractSoftwareOriginUriFromDirectorySSASteps();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		call(makeSoftwareOriginUriExtractionSteps());
		super.performPreAuthorizationSteps();
	}

	@Override
	protected void requestProtectedResource() {
		postAndValidateFidoRegistrationOptions();
		postAndValidateFidoRegistration();
		getEnrollmentsAfterFidoRegistration();
		super.requestProtectedResource();
	}

	protected void postAndValidateFidoRegistrationOptions() {
		userAuthorisationCodeAccessToken();
		runInBlock("Post fido-registration-options - Expects 201", () -> {
			call(createPostFidoRegistrationOptionsSteps());
		});
		runInBlock("Validate fido-registration-options response", () -> {
			validateResourceResponse(postFidoRegistrationOptionsValidator());
			callAndContinueOnFailure(EnsureResponseDoesNotHaveLinks.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(ExtractFidoRegistrationOptionsChallenge.class);
			callAndStopOnFailure(ExtractUserIdFromFidoRegistrationOptionsResponse.class);
		});
	}

	protected PostEnrollmentsResourceSteps createPostFidoRegistrationOptionsSteps() {
		return new PostEnrollmentsResourceSteps(
			new PrepareToPostFidoRegistrationOptions(),
			CreateEnrollmentsFidoRegistrationOptionsRequestBody.class
		);
	}

	protected void postAndValidateFidoRegistration() {
		userAuthorisationCodeAccessToken();
		runInBlock("Post fido-registration - Expects 204", () -> {
			prepareEnvironmentForPostFidoRegistration();
			call(createPostFidoRegistrationSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void prepareEnvironmentForPostFidoRegistration() {
		callAndStopOnFailure(GenerateFidoClientData.class);
		callAndStopOnFailure(GenerateKeyPairFromFidoRegistrationOptionsResponse.class);
		callAndStopOnFailure(GenerateFidoAttestationObject.class);
	}

	protected PostEnrollmentsResourceSteps createPostFidoRegistrationSteps() {
		return new PostEnrollmentsResourceSteps(
			new PrepareToPostFidoRegistration(),
			CreateFidoRegistrationRequestBodyToRequestEntityClaims.class,
			true
		);
	}

	protected void getEnrollmentsAfterFidoRegistration() {
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasAuthorised(), "AUTHORISED");
	}

	protected abstract Class<? extends Condition> postFidoRegistrationOptionsValidator();
}
