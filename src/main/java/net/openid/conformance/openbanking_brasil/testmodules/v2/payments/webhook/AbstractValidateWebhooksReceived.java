package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractValidateWebhooksReceived extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		String type = webhookType();
		int expectedAmount = expectedWebhooksReceivedAmount();
		String id = "";
		switch (type) {
			case "consent":
			case "recurring_consent":
				id =  "consent_id";
				break;
			case "payment":
			case "recurring_payment":
				id = "payment_id";
				break;
			case "enrollment":
				id = "enrollment_id";
				break;
			}
		extractAndValidate(env, id, type, expectedAmount);
		resetWebhooks(env,type);
		return env;
	}

	private void extractAndValidate(Environment env, String id, String type, int expectedAmount) {
		String webhookTimeStamp = env.getString(String.format("time_of_%s_request", type));
		String idString = env.getString(id);
		if (idString == null) {
			throw error(id + " not found");
		}
		JsonObject webhooksObject = getWebhooks(env, type);
		validateWebhooksReceived(webhooksObject, expectedAmount, type, webhookTimeStamp, idString);
	}

	protected void resetWebhooks(Environment env, String type){
		JsonObject webhooksReceived = new JsonObject();
		JsonArray webhooks = new JsonArray();
		webhooksReceived.add("webhooks", webhooks);
		env.putObject("webhooks_received_" + type, webhooksReceived);
		logSuccess("Reset webhooks received", webhooksReceived);
	}
	protected JsonObject getWebhooks(Environment env,String type){
		JsonObject webhooksObject = env.getObject("webhooks_received_" + type);
		if (webhooksObject == null) {
			throw error("Webhooks not found");
		}
		return webhooksObject;
	}
	protected void validateWebhooksReceived(JsonObject webhooksReceived, int expectedWebhooksReceivedAmount, String webhookType, String webhookTimeStamp, String resourceId){
		JsonArray webhooks = webhooksReceived.getAsJsonArray("webhooks");
		if (webhooks.size() != expectedWebhooksReceivedAmount) {
			throw error("Expected " + expectedWebhooksReceivedAmount + " webhooks, but received " + webhooks.size());
		}
		for (JsonElement webhook : webhooks) {
			logSuccess("Validating webhook", webhook.getAsJsonObject());
			if (OIDFJSON.getString(webhook.getAsJsonObject().get("type")).equals(webhookType)) {
				validateWebhook(webhook.getAsJsonObject(), webhookTimeStamp, resourceId, webhookType);
			}
		}
	}
	protected abstract int expectedWebhooksReceivedAmount();
	protected abstract String webhookType();

	protected void validateWebhook(JsonObject webhook, String webhookTimeStamp, String resourceId, String type){
		ensureWebhookInteractionId(webhook);
		ensureContentTypeJson(webhook);
		validateWebhookTimeStamp(webhook, webhookTimeStamp);
		validateResourceId(webhook, resourceId, type);
	}

	protected void ensureWebhookInteractionId(JsonObject webhook){
		JsonObject webhookHeader = webhook.getAsJsonObject("headers");
		if (!webhookHeader.keySet().contains("x-webhook-interaction-id")){
			throw error ("Headers returned in webhook response do not contain x-webhook-interaction-id", args("expected_header", "x-webhook-interaction-id", "headers_returned", webhookHeader));
		}
		String interactionId = OIDFJSON.getString(webhookHeader.get("x-webhook-interaction-id"));
		logSuccess("Webhook interaction id found", args("interaction_id", interactionId));
	}

	protected void ensureContentTypeJson(JsonObject webhook){
		JsonObject webhookHeader = webhook.getAsJsonObject("headers");
		if (!webhookHeader.keySet().contains("content-type")){
			throw error ("Headers returned in webhook response do not contain content-type", args("expected_header", "content-type", "headers_returned", webhookHeader));
		}
		String contentType = OIDFJSON.getString(webhookHeader.get("content-type"));
		if (contentType.equals("application/json")) {
			logSuccess("content-type found", args("content-type", contentType));
		} else {
			throw error("content-type not found or not equal to application/json");
		}
	}

	protected void validateWebhookTimeStamp(JsonObject webhook, String webhookTimeStamp){
		if (!webhook.keySet().contains("body")){
			throw error("No webhook body found in the webhook request");
		}
		String webhookBody = OIDFJSON.getString(webhook.get("body"));
		JsonObject webhookBodyJson = JsonParser.parseString(webhookBody).getAsJsonObject();
		findKeyOrError(webhookBodyJson, "data");
		JsonObject dataObject = webhookBodyJson.getAsJsonObject("data");
		findKeyOrError(dataObject, "timestamp");
		String webhookBodyTimeStamp = OIDFJSON.getString(dataObject.get("timestamp"));
		Instant currentInstant = Instant.now().truncatedTo(ChronoUnit.SECONDS);
		Instant responseTimeStamp = Instant.parse(webhookBodyTimeStamp).truncatedTo(ChronoUnit.SECONDS);
		Instant timeStampAtStartOfPostRequest = Instant.parse(webhookTimeStamp).truncatedTo(ChronoUnit.SECONDS);
		if ((responseTimeStamp.isBefore(timeStampAtStartOfPostRequest) || responseTimeStamp.isAfter(currentInstant)) && (!responseTimeStamp.equals(timeStampAtStartOfPostRequest))) {
			throw error("Webhook timestamp is not within the time of the POST request and current time", args("timestamp", webhookBodyTimeStamp, "time_of_request_initiated", timeStampAtStartOfPostRequest, "current_timestamp", currentInstant.toString()));
		}
		logSuccess("Webhook timestamp is within the test start and current time", args("timestamp", webhookBodyTimeStamp));
	}

	private void validateResourceId(JsonObject webhook, String resourceId, String type){
		String path = OIDFJSON.getString(webhook.get("path"));
		switch (type) {
			case "consent":
				String consentId = getValueFromRegexGroup(path, "^(open-banking\\/webhook\\/v\\d+\\/payments\\/v\\d+\\/consents\\/)(urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+)$", 2);
				if (consentId == null || !consentId.equals(resourceId)) {
					throw error("Consent ID in webhook path does not match the consent ID in the request", args("consent_id_in_webhook_path", consentId, "consent_id_in_request", resourceId));
				}
				break;
			case "payment":
				String paymentId = getValueFromRegexGroup(path, "^(open-banking\\/webhook\\/v\\d+\\/payments\\/v\\d+\\/pix\\/payments\\/)([a-zA-Z0-9][a-zA-Z0-9\\-]{0,99})$", 2);
				if (paymentId == null || !paymentId.equals(resourceId)) {
					throw error("Payment ID in webhook path does not match the payment ID in the request", args("payment_id_in_webhook_path", paymentId, "payment_id_in_request", resourceId));
				}
				break;
			case "enrollment":
				String enrollmentId = getValueFromRegexGroup(path, "^(open-banking\\/webhook\\/v\\d+\\/enrollments\\/v\\d+\\/enrollments\\/)(urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+)$", 2);
				if (enrollmentId == null || !enrollmentId.equals(resourceId)) {
					throw error("Enrollment ID in webhook path does not match the enrollment ID in the request", args("enrollment_id_in_webhook_path", enrollmentId, "enrollment_id_in_request", resourceId));
				}
				break;
			case "recurring_consent":
				String recurringConsentId = getValueFromRegexGroup(path, "^(open-banking\\/webhook\\/v\\d+\\/automatic-payments\\/v\\d+\\/recurring-consents\\/)(urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+)$", 2);
				if (recurringConsentId == null || !recurringConsentId.equals(resourceId)) {
					throw error("Consent ID in webhook path does not match the consent ID in the request", args("consent_id_in_webhook_path", recurringConsentId, "consent_id_in_request", resourceId));
				}
				break;
			case "recurring_payment":
				String recurringPaymentId = getValueFromRegexGroup(path, "^(open-banking\\/webhook\\/v\\d+\\/automatic-payments\\/v\\d+\\/pix\\/recurring-payments\\/)([a-zA-Z0-9][a-zA-Z0-9\\-]{0,99})$", 2);
				if (recurringPaymentId == null || !recurringPaymentId.equals(resourceId)) {
					throw error("Payment ID in webhook path does not match the payment ID in the request", args("payment_id_in_webhook_path", recurringPaymentId, "payment_id_in_request", resourceId));
				}
				break;
		}
	}
	protected String getValueFromRegexGroup(String path, String regex, int groupNumber) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(path);
		if (matcher.matches()){
			logSuccess("Regex matches", args("regex", regex, "actual", path));
			return matcher.group(groupNumber);
		}
		logFailure("Regex does not match", args("regex", regex, "actual", path));
		return null;
	}
	private void findKeyOrError(JsonObject object, String key) {
		if (!object.keySet().contains(key)) {
			throw error("Webhook response body does not contain " + key + " field");
		}
	}
}
