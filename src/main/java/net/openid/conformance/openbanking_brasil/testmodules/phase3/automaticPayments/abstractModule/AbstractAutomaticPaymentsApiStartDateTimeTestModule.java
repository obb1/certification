package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractUnhappyAutomaticPaymentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setStartDate.EditRecurringPaymentsConsentBodyToSetStartDateToTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import org.springframework.http.HttpStatus;

public abstract class AbstractAutomaticPaymentsApiStartDateTimeTestModule extends AbstractUnhappyAutomaticPaymentsFunctionalTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}

	@Override
	protected HttpStatus expectedResponseCode() {
		return HttpStatus.UNPROCESSABLE_ENTITY;
	}

	@Override
	protected void requestProtectedResource(){
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
		super.requestProtectedResource();
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();
		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		env.unmapKey("access_token");
		validateGetConsentResponse();
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas ensureErrorResponseCodeFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetStartDateToTheFuture.class);
	}
}
