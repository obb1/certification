package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;


import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetEndpointFromAuthServer;
import net.openid.conformance.sequence.AbstractConditionSequence;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Optional;

public class ValidateRegisteredEndpoints extends AbstractConditionSequence {

	private Class<? extends AbstractGetEndpointFromAuthServer> endpointExtractionCondition;

	private ConditionSequence endpointExtractionSequence;

	public ValidateRegisteredEndpoints(Class<? extends AbstractGetEndpointFromAuthServer> endpointExtractionCondition) {
		this.endpointExtractionCondition = endpointExtractionCondition;
	}

	public ValidateRegisteredEndpoints(ConditionSequence endpointExtractionSequence) {
		this.endpointExtractionSequence = endpointExtractionSequence;
	}

	@Override
	public void evaluate() {
		call(exec().startBlock("Validating Authorisation Server has supported Endpoint"));
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		Optional.ofNullable(endpointExtractionCondition)
			.ifPresentOrElse(this::callAndStopOnFailure, () -> call(endpointExtractionSequence));
		call(exec().endBlock());
	}
}
