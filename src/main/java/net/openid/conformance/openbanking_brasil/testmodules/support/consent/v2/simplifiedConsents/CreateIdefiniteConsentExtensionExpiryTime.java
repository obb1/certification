package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreateIdefiniteConsentExtensionExpiryTime extends AbstractCondition {
	@Override
	@PostEnvironment(strings = "consent_extension_expiration_time")
	public Environment evaluate(Environment env) {
		String indefiniteExpiryTime = "2300-01-01T00:00:00Z";
		JsonObject expiryTimes = env.getObject("extension_expiry_times");
		if (expiryTimes == null) {
			expiryTimes = new JsonObject();
			JsonArray expiryTimesArray = new JsonArray();
			expiryTimesArray.add(indefiniteExpiryTime);
			expiryTimes.add("extension_times", expiryTimesArray);
		} else {
			JsonArray expiryTimesArray = expiryTimes.getAsJsonArray("extension_times");
			expiryTimesArray.add(indefiniteExpiryTime);
		}
		env.putObject("extension_expiry_times", expiryTimes);
		env.putString("consent_extension_expiration_time", indefiniteExpiryTime);
		logSuccess("Created consent extension expiry time", args("consent_extension_expiration_time", indefiniteExpiryTime));

		return env;	}
}
