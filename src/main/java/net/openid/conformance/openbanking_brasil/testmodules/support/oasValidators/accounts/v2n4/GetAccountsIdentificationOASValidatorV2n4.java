package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.field.ExtraField;
import org.springframework.http.HttpMethod;

public class GetAccountsIdentificationOASValidatorV2n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/accounts/swagger-accounts-2.4.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/accounts/{accountId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected ExtraField getExtraField() {
		return new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build();
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	private void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasNotValue2(
			data,
			"branchCode",
			"type",
			"CONTA_PAGAMENTO_PRE_PAGA"
		);
	}
}
