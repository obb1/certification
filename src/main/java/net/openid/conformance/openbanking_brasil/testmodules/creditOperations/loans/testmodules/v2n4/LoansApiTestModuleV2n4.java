package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.AbstractLoansApiTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansIdentificationV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansListV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansPaymentsV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansScheduledInstalmentsV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansWarrantiesV2n4OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "loans_api_core_test-module_v2-4",
	displayName = "Validate structure of all loans API resources - V2",
	summary = "Validates the structure of all loans API resources - V2\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Credit Operations API  - V2 (\"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Loans Contracts API - V2\n" +
		"\u2022 Expects 201 - Fetches one of the IDs returned\n" +
		"\u2022 Calls GET Loans Contracts API - V2 with ID\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Warranties API - V2\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Payments API V2\n" +
		"\u2022 Expects 200\n" +
		"\u2022 Calls GET Loans Contracts Instalments API - V2 \n" +
		"\u2022 Expects 200\n" +
		"10",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class LoansApiTestModuleV2n4 extends AbstractLoansApiTest {

	@Override
	protected Class<? extends Condition> apiResourceContractListResponseValidator() {
		return GetLoansListV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractResponseValidator() {
		return GetLoansIdentificationV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractGuaranteesResponseValidator() {
		return GetLoansWarrantiesV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractPaymentsResponseValidator() {
		return GetLoansPaymentsV2n4OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> apiResourceContractInstallmentsResponseValidator() {
		return GetLoansScheduledInstalmentsV2n4OASValidator.class;
	}
}
