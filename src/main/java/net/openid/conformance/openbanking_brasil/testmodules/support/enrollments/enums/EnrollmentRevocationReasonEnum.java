package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum EnrollmentRevocationReasonEnum {
	REVOGADO_MANUALMENTE,
	REVOGADO_VALIDADE_EXPIRADA,
	REVOGADO_FALHA_INFRAESTRUTURA,
	REVOGADO_SEGURANCA_INTERNA,
	REVOGADO_OUTRO;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
