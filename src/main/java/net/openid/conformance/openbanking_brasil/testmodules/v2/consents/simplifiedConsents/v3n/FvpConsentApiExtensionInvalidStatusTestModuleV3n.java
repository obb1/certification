package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractFvpConsentApiExtensionInvalidStatusTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-consents_api_extension-invalid-status_test-module_v3-2",
	displayName = "fvp-consents_api_extension-invalid-status_test-module_v3-2",
	summary = "Ensure an extension cannot be done if consent is at AWAITING_AUTHORISATION status\n" +
		"• Call the POST Consents Endpoint with expirationDateTime as current time + 5 minutes\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the GET Consents\n" +
		"• Expects 200 - Validate Response and check if the status is AWAITING_AUTHORISATION\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as  D+365, and all required headers\n" +
		"• Expects 401 or 403 - Validate Error Message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl"
	}
)
public class FvpConsentApiExtensionInvalidStatusTestModuleV3n extends AbstractFvpConsentApiExtensionInvalidStatusTestModule {

	@Override
	protected Class<? extends Condition> postConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> getConsentValidator() {
		return GetConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}
}
