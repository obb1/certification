package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateAuthServerApiResources extends AbstractCondition {

	List<String> failureMessages = new ArrayList<>();
	boolean isFailure = false;
	@Override
	@PreEnvironment(required = "authorisation_server")
	public Environment evaluate(Environment env) {
		JsonObject authorisationServer = env.getObject("authorisation_server");
		JsonArray apiResources = authorisationServer.getAsJsonArray("ApiResources");
		if (apiResources.isEmpty()) {
			logSuccess("No ApiResources found, stopping validation");
		}else {
			validateApiFamilies(apiResources, AuthServerApiResourcePhaseEnum.PHASE_3.getStrings());
			validateApiFamilies(apiResources, AuthServerApiResourcePhaseEnum.PHASE_2.getStrings());
		}
		if (isFailure){
			Set<String> uniqueFailureMessages = new HashSet<>(failureMessages);
			List<String> errorMessage = new ArrayList<>(uniqueFailureMessages);
			throw error(String.join(";", errorMessage));
		}
		return env;
	}

	protected void validateApiFamilies(JsonArray apiResources, String... apiFamilies){
		for (JsonElement apiResource : apiResources) {
			String apiFamily = OIDFJSON.getString((apiResource.getAsJsonObject().get("ApiFamilyType")));
			JsonObject apiResourceJson = getApiResourceByFamily(apiFamilies, apiResource);
			if (apiResourceJson != null && !apiResourceJson.get("ApiCertificationUri").isJsonNull()){
				validateApiCertificationUri(apiResourceJson, apiFamily);
				boolean familyComplete = OIDFJSON.getBoolean(apiResourceJson.get("FamilyComplete"));
				if (familyComplete){
					logSuccess("ApiResource family is complete", args("ApiFamilyType", apiFamily));
				} else {
					isFailure = true;
					String failureMessage = String.format("ApiResource family is not complete");
					failureMessages.add(failureMessage);
					logFailure("ApiResource family is not complete", args("ApiFamilyType", apiFamily));
				}

			}
		}
	}

	protected JsonObject getApiResourceByFamily(String[] apiFamilies, JsonElement apiResource){
		String versionValidatorRegex= "^([0-9].[0-9].[0-9])$";
		for (String apiFamily : apiFamilies) {
			if (OIDFJSON.getString((apiResource.getAsJsonObject().get("ApiFamilyType"))).equals(apiFamily) && OIDFJSON.getString(apiResource.getAsJsonObject().get("ApiVersion")).matches(versionValidatorRegex)){
				return apiResource.getAsJsonObject();
			}
		}
		return null;
	}

	protected void validateApiCertificationUri(JsonObject apiResource, String familyType){
		String apiCertUri = OIDFJSON.getString(apiResource.get("ApiCertificationUri"));
		if (apiResource.get("CertificationStartDate").isJsonNull()){
			logFailure("CertificationStartDate is null", args("ApiResource", apiResource));
			isFailure = true;
			String failureMessage = String.format("CertificationStartDate is null");
			failureMessages.add(failureMessage);
			return;
		}
		String adjustedFamilyType = ApiResourcePathMapEnum.getValue(familyType);
		String apiVersion = OIDFJSON.getString(apiResource.get("ApiVersion"));
		int majorVersion = Character.getNumericValue(apiVersion.charAt(0));
		String regex = String.format("(https:\\/\\/github\\.com\\/OpenBanking-Brasil\\/conformance\\/)(blob|raw)(\\/main\\/submissions\\/functional\\/)(%s\\/)(%d\\.\\d\\.\\d)(.*)(([0-2][1-9]|[1-3]0|31)-(0[1-9]|1[0-2])-(20)\\d{2})((\\.zip)|(\\.json))", adjustedFamilyType,majorVersion);
		validateRegex(apiCertUri, regex,apiResource);
	}


	protected void validateRegex(String path, String regex,JsonObject apiResource) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(path);
		if (matcher.matches()){
			logSuccess("ApiCertificationUri Regex matches", args("regex", regex, "actual", path,"api_resource",apiResource));
		}
		else {
			logFailure("ApiCertificationUri Regex does not match", args("regex", regex, "actual", path, "api_resource", apiResource));
			isFailure = true;
			String failureMessage = String.format("ApiCertificationUri Regex does not match");
			failureMessages.add(failureMessage);
		}
	}
}
