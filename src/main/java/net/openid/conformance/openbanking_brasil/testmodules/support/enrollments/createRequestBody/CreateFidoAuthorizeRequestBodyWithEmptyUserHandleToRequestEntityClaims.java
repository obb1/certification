package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.KeyType;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.security.PrivateKey;
import java.util.Optional;

public class CreateFidoAuthorizeRequestBodyWithEmptyUserHandleToRequestEntityClaims
	extends CreateFidoAuthorizeRequestBodyToRequestEntityClaims {

	@Override
	protected JsonObject getResponseObject(Environment env, JsonObject fidoKeysJwk) {

		final PrivateKey privateKey = getKeyPairFromJwkJson(fidoKeysJwk).getPrivate();
		final KeyType keyType = getKeyTypeFromJwkJson(fidoKeysJwk);

		final byte[] clientData = Optional.ofNullable(env.getObject("client_data"))
			.map(JsonElement::toString)
			.map(String::getBytes)
			.orElseThrow(() -> error("Could not find client_data in the environment"));

		final byte[] authenticatorData = getAuthenticatorData(env);

		final String signature = getSignature(clientData, authenticatorData, privateKey, keyType);

		return new JsonObjectBuilder()
			.addField("clientDataJSON", encodeData(clientData))
			.addField("authenticatorData", encodeData(authenticatorData))
			.addField("signature", signature)
			.addField("userHandle", "")
			.build();
	}
}
