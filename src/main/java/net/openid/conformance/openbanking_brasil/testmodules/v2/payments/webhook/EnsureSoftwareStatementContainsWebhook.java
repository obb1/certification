package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.runner.TestDispatcher;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureSoftwareStatementContainsWebhook extends AbstractCondition {
	@Override
	@PreEnvironment(required = "software_statement_assertion")
	public Environment evaluate(Environment env) {
		String baseUrl = env.getString("base_url");
		baseUrl = baseUrl.replaceFirst(TestDispatcher.TEST_PATH, TestDispatcher.TEST_MTLS_PATH);
		JsonObject softwareStatementAssertionClaime = env.getObject("software_statement_assertion").getAsJsonObject("claims");
		if (!softwareStatementAssertionClaime.keySet().contains("software_api_webhook_uris")) {
			throw error("Unable to find software_api_webhook_uris in software statement, using defined webhook endpoint", args("endpoint", baseUrl));
		}
		JsonArray webhookEndpoint = softwareStatementAssertionClaime.get("software_api_webhook_uris").getAsJsonArray();
		String expectedEndpoint = null;
		for (JsonElement endpoint: webhookEndpoint) {
			if (OIDFJSON.getString(endpoint).equals(baseUrl)){
				expectedEndpoint = OIDFJSON.getString(endpoint);
				env.putString("software_api_webhook_uri", OIDFJSON.getString(endpoint));
				logSuccess("Webhook endpoint found that matches the expected pattern", args("actual_endpoint", OIDFJSON.getString(endpoint), "expected_endpoint", baseUrl));
				break;
			}
		}
		if (expectedEndpoint == null){
			throw error("Unable to find a Webhook endpoint that matches the defined pattern", args("expected_endpoint", baseUrl,"actual_endpoint", webhookEndpoint));
		}
		return env;
	}
}
