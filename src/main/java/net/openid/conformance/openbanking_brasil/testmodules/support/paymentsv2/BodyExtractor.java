package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JWTUtil;
import net.openid.conformance.util.JsonUtils;

import java.text.ParseException;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

public class BodyExtractor {
	private static final Pattern JWT_PATTERN = Pattern.compile("^(e[yw][a-zA-Z0-9_-]+)\\.([a-zA-Z0-9_-]+)\\.([a-zA-Z0-9_-]+)(\\.([a-zA-Z0-9_-]+)\\.([a-zA-Z0-9_-]+))?$");


	public static Optional<JsonElement> bodyFrom(Environment environment, String responseObjectKey) throws ParseException {

		JsonObject apiResponse = environment.getObject(responseObjectKey);
		if (apiResponse == null) {
			return Optional.empty();
		}

		JsonElement bodyEl = apiResponse.get("body");
		if (bodyEl == null) {
			return Optional.empty();
		}
		String bodyStr = OIDFJSON.getString(bodyEl);

		if (isJwt(bodyStr)) {
			JsonObject decodedJwt;
			decodedJwt = JWTUtil.jwtStringToJsonObjectForEnvironment(bodyStr);
			JsonElement claims = decodedJwt.get("claims");
			forceIntsInJsonObject(claims.getAsJsonObject());
			return Optional.ofNullable(decodedJwt.get("claims"));
		} else {
			JsonElement parsedJson = JsonUtils.createBigDecimalAwareGson().fromJson(bodyStr, JsonElement.class);
			forceIntsInJsonObject(parsedJson.getAsJsonObject());
			return Optional.ofNullable(parsedJson);

		}
	}

	public static boolean isJwt(String response) {
		return JWT_PATTERN.matcher(response).matches();
	}

	private static void forceIntsInJsonObject(JsonObject object) {
		for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
			JsonElement valueElement = entry.getValue();

			if (valueElement.isJsonObject()) {
				forceIntsInJsonObject(valueElement.getAsJsonObject());
			} else if (valueElement.isJsonArray()) {
				forceIntsInJsonArray(valueElement.getAsJsonArray());
			} else if (valueElement.isJsonPrimitive() && valueElement.getAsJsonPrimitive().isNumber()) {
				double value = OIDFJSON.getDouble(valueElement);
				if (value == (int) value) {
					entry.setValue(new JsonPrimitive((int) value));
				}
			}
		}
	}

	private static void forceIntsInJsonArray(JsonArray array) {
		for (int i = 0; i < array.size(); i++) {
			JsonElement element = array.get(i);
			if (element.isJsonObject()) {
				forceIntsInJsonObject(element.getAsJsonObject());
			} else if (element.isJsonArray()) {
				forceIntsInJsonArray(element.getAsJsonArray());
			} else if (element.isJsonPrimitive() && element.getAsJsonPrimitive().isNumber()) {
				double value = OIDFJSON.getDouble(element);
				if (value == (int) value) {
					array.set(i, new JsonPrimitive((int) value));
				}
			}
		}
	}

}
