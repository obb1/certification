package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractPaymentTransactionIdentificationCondition extends AbstractCondition {

	@Override
	public final Environment evaluate(Environment env) {
		log("Setting transaction identification to a new value");
		JsonElement data = env.getElementFromObject("resource", "brazilPixPayment.data");

		if (data.isJsonArray()) {
			for (int i = 0; i < data.getAsJsonArray().size(); i++) {
				JsonObject payment = data.getAsJsonArray().get(i).getAsJsonObject();
				payment.addProperty("transactionIdentification", getTransactionIdentification());
				log("Added transactionIdentification to payment", payment);
			}
			return env;

		}
		data.getAsJsonObject().addProperty("transactionIdentification", getTransactionIdentification());
		log("Added transactionIdentification to payment", data.getAsJsonObject());
		return env;
	}

	protected abstract String getTransactionIdentification();

}
