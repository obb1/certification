package net.openid.conformance.openbanking_brasil.testmodules.v3.resources;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractOBBrasilPhase2V2TestModuleOptionalErrors;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureDataArrayIsEmpty;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ResourceApiV2PollingSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "resources_api_200-customer-data_test-module_v3",
	displayName = "Validate correct response when only request customer data permissions",
	summary = "Validates correct response when only requesting customer data permissions\n" +
		"\u2022 Creates a Consent with either the customer personal or business permissions (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\", \"CUSTOMERS_PERSONAL_ADITTIONALINFO_READ\", \"RESOURCES_READ\") or (\"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\", \"CUSTOMERS_BUSINESS_ADITTIONALINFO_READ\", \"RESOURCES_READ\")\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET Resources API V3\n" +
		"\u2022 Expects a 200 response with empty data object",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ResourcesApiTestModuleCorrect200V3 extends AbstractOBBrasilPhase2V2TestModuleOptionalErrors {

	@Override
	protected void configureClient(){
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetConsentV3Endpoint.class),
				condition(GetResourcesV3Endpoint.class)
			)
			)
		);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA).addScopes(OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA).addScopes(OPFScopesEnum.RESOURCES, OPFScopesEnum.OPEN_ID).build();
		}
	}

	@Override
	protected void validateResponse() {

		ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
			eventLog,testInfo, getTestExecutionManager());
		runInBlock("Polling Resources API", () -> {
			call(pollingSteps);
		});

		String logMessage = "Validate correct 200 api response with empty data object";
		runInBlock(logMessage, () -> {
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureDataArrayIsEmpty.class, Condition.ConditionResult.FAILURE);
		});
	}
}
