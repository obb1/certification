package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreateInvalidFAPIInteractionId extends AbstractCondition {
	@Override
	@PostEnvironment(strings = "fapi_interaction_id")
	public Environment evaluate(Environment env) {

		String interactionId = "123456";
		env.putString("fapi_interaction_id", interactionId);

		log("Created invalid ID", args("fapi_interaction_id", interactionId));

		return env;
	}
}
