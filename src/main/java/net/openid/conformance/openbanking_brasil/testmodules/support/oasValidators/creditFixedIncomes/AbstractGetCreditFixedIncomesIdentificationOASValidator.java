package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes;

import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetCreditFixedIncomesIdentificationOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertData(data);
		assertRenumeration(data);
	}

	private void assertData(JsonObject data) {
		assertField1isRequiredWhenField2IsNotPresent(
			data,
			"isinCode",
			"clearingCode"
		);

		assertField1isRequiredWhenField2IsNotPresent(
			data,
			"clearingCode",
			"isinCode"
		);

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"voucherPaymentPeriodicity",
			"voucherPaymentIndicator",
			"SIM"
		);

		assertVoucherPaymentPeriodicityAdditionalInfo(data);
	}

	private void assertVoucherPaymentPeriodicityAdditionalInfo(JsonObject data) {
		if (isPresent(data, "voucherPaymentPeriodicity")) {
			assertField1IsRequiredWhenField2HasValue2(
					data,
					"voucherPaymentPeriodicityAdditionalInfo",
					"voucherPaymentPeriodicity",
					"OUTROS"
			);
		}
	}




	private void assertRenumeration(JsonObject data) {
		JsonObject remuneration = JsonPath.read(data, "$.remuneration");

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"preFixedRate",
			"indexer",
			"PRE_FIXADO"
		);

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"postFixedIndexerPercentage",
			"indexer",
			"PRE_FIXADO"
		);

		assertField1IsRequiredWhenField2HasValue2(
			remuneration,
			"indexerAdditionalInfo",
			"indexer",
			"OUTROS"
		);

	}

}
