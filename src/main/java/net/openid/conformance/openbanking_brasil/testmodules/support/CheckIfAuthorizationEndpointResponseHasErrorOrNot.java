package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CheckIfAuthorizationEndpointResponseHasErrorOrNot extends AbstractCondition {

	@Override
	@PreEnvironment(required = "authorization_endpoint_response")
	public Environment evaluate(Environment env) {
		String error = env.getString("authorization_endpoint_response", "error");
		env.putBoolean("token_endpoint_response_was_error", !Strings.isNullOrEmpty(error));
		logSuccess("Environment has been updated based on authorization endpoint response");
		return env;
	}
}
