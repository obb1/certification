package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ConsentExtensionRequestInsertInvalidUser extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"consent_endpoint_request"})
	public Environment evaluate(Environment env) {
		JsonObject consentEndpointRequest = env.getObject("consent_endpoint_request");
		JsonObject data = consentEndpointRequest.getAsJsonObject("data");

		if(data.has("businessEntity")) {
			addInvalidBusinessEntity(data);
		} else {
			addInvalidLoggedUser(data);
		}

		logSuccess("Inserted Invalid user to request", args("consentRequestData", data));
		return null;
	}

	private void addInvalidBusinessEntity(JsonObject consentData) {
		JsonObject businessEntity = new JsonObject();
		JsonObject document = new JsonObject();
		document.addProperty("identification", "38404035000169");
		document.addProperty("rel", "CNPJ");
		businessEntity.add("document", document);
		consentData.add("businessEntity", businessEntity);
	}

	private void addInvalidLoggedUser(JsonObject consentData) {
		JsonObject loggedUser = new JsonObject();
		JsonObject document = new JsonObject();
		document.addProperty("identification", "82139497066");
		document.addProperty("rel", "CPF");
		loggedUser.add("document", document);
		consentData.add("loggedUser", loggedUser);
	}
}
