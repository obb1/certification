package net.openid.conformance.openbanking_brasil.testmodules.support.phase2.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CreateResourceErrorEnumV2 {
	STATUS_RESOURCE_PENDING_AUTHORISATION;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
