package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetCreditorAccountToConfigValue extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {

		log("Setting creditorAccount from config value");

		JsonElement creditorElem = env.getObject("originalCreditorAccount");
		if (creditorElem == null) {
			throw error("Original debtor object not found in config");
		}

		JsonObject creditorObject = creditorElem.getAsJsonObject();
		env.putObject("config", "resource.brazilPaymentConsent.data.payment.details.creditorAccount", creditorObject);

		logSuccess("creditorAccount was successfully copied from config");
		return env;
	}

}
