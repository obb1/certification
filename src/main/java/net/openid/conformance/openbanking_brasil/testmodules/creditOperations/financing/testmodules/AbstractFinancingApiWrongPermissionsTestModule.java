package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiWrongPermissionsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractInstallmentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractPaymentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFetchingFinancingContractWarrantiesResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.financing.PrepareUrlForFinancingRoot;
import net.openid.conformance.openbanking_brasil.testmodules.support.FinancingContractSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractFinancingApiWrongPermissionsTestModule extends AbstractApiWrongPermissionsTestModulePhase2 {

	@Override
	protected abstract Class<? extends Condition> apiResourceContractListResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractGuaranteesResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractPaymentsResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractInstallmentsResponseValidator();

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.FINANCINGS;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetFinancingsV2Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> contractSelector() {
		return FinancingContractSelector.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingRootEndpoint() {
		return PrepareUrlForFinancingRoot.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractEndpoint() {
		return PrepareUrlForFetchingFinancingContractResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractWarrantiesEndpoint() {
		return PrepareUrlForFetchingFinancingContractWarrantiesResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractPaymentsEndpoint() {
		return PrepareUrlForFetchingFinancingContractPaymentsResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractScheduledInstalmentsEndpoint() {
		return PrepareUrlForFetchingFinancingContractInstallmentsResource.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
