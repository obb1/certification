package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetCreditCardAccountsTransactionsOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/creditCard/swagger-credit-cards-2.3.1.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray dataArray = JsonPath.read(body, "$.data[*]");
		for(JsonElement dataElement : dataArray) {
			assertDataAdditionalConstraints(dataElement.getAsJsonObject());
		}
	}

	protected void assertDataAdditionalConstraints(JsonObject data) {
		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"transactionalAdditionalInfo",
			"transactionType",
			"OUTROS"
		);

		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"paymentType",
			"transactionType",
			"PAGAMENTO"
		);

		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"feeType",
			"transactionType",
			"TARIFA"
		);

		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"feeTypeAdditionalInfo",
			"feeType",
			"OUTRA"
		);

		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"otherCreditsType",
			"transactionType",
			"OPERACOES_CREDITO_CONTRATADAS_CARTAO"
		);

		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"otherCreditsAdditionalInfo",
			"otherCreditsType",
			"OUTROS"
		);

		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"chargeIdentificator",
			"paymentType",
			"A_PRAZO"
		);

		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"chargeNumber",
			"paymentType",
			"A_PRAZO"
		);
	}
}
