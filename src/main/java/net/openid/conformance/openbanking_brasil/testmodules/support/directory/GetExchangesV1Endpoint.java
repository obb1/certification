package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetExchangesV1Endpoint extends AbstractGetXFromAuthServer {
	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/exchanges\\/v\\d+\\/operations)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "exchanges";
	}

	@Override
	protected String getApiVersionRegex() {
		return "^(1.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
