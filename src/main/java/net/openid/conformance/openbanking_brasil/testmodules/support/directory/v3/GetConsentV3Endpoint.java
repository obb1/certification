package net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;

public class GetConsentV3Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/consents\\/v\\d+\\/consents)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "consents";
	}

	@Override
	protected String getApiVersionRegex() {
		return "^(3.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return false;
	}
}
