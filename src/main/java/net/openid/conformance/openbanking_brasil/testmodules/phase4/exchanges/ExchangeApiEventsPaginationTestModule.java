package net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaginationUserToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize25;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIs25FromData;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIsAtLeast51FromMeta;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesEventsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.EnsureInvestmentsPaginationListCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperationEvents;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "exchanges_api_pagination_events-test-module",
	displayName = "exchanges_api_pagination-events-test-module",
	summary = "Test will make sure that the pagination rules are being respected for the exchange events endpoint, by changing the page-size and the page parameter of the URI and confirming that pagination is being correctly done. Test will also require at least 51 exchange operations events to be returned\n" +
		"Call the POST Consents endpoint, using the brazilCpf/Cnpj for Pagination List, the Exchange API Permission Group - Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
		"Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"Call the GET Consents endpoint\n" +
		"Expects 200 - Validate response and confirm that the Consent is set to \"\"Authorised\"\"\n" +
		"Call the GET exchange List endpoint\n" +
		"Expects a 200 response - Validate response and extract the first operationId\n" +
		"Call the GET exchange Events endpoint with page-size equal to 1000, using the extracted operationId\n" +
		"Expects a 200 response - Ensure that the totalRecords is equal os superior to 51 - Validate the Response_body\n" +
		"Call the GET Exchange Events endpoint  with page-size equal to 25\n" +
		"Expect a 200 response - Ensure that the number of returned transactions is equal to 25. Perform all required links validation, including, but not limited to, ensuring the next uri is different to the self uri, ensure the \"\"prev\"\" hasn't been sent - Validate the Response_body\n" +
		"Call the GET Exchange Events endpoint with the next link returned on the previous call\n" +
		"Expects a 200 response - Ensure that the number of returned transactions is equal to 25.  Perform all required links validation, including, but not limited to, ensuring the next uri is different to the self uri, ensure the \"\"prev\"\" has been sent, next and self uris are different and are pointing to pages 1, 2 and 3 respectively - Validate the Response_body\n" +
		"Call the Delete Consents Endpoints\n" +
		"Expect a 204 without a body",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class ExchangeApiEventsPaginationTestModule extends AbstractExchangesApiTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(AddPaginationUserToConfig.class);
	}

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsureInvestmentsPaginationListCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no Pagination List CPF/CNPJ was informed.");
		}
	}

	@Override
	protected void executeParticularTestSteps() {


		runInBlock("Call GET Events endpoint with page-size equal to 1000 - Expects 200 status code and totalRecords >= 51", ()->
		{
			callAndStopOnFailure(PrepareUrlForFetchingOperationEvents.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
			preCallProtectedResource();
			callAndContinueOnFailure(GetExchangesEventsV1OASValidator.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast51FromMeta.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Call GET Events endpoint with page-size equal to 25 - Expects 200 status code and totalRecords = 25", ()->
		{
			callAndStopOnFailure(PrepareUrlForFetchingOperationEvents.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize25.class);
			preCallProtectedResource();
			callAndContinueOnFailure(GetExchangesEventsV1OASValidator.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIs25FromData.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Call the GET Exchange Events endpoint with the next link returned on the previous call", ()->
		{
			callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
			preCallProtectedResource();
			callAndContinueOnFailure(GetExchangesEventsV1OASValidator.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIs25FromData.class, Condition.ConditionResult.FAILURE);
		});
	}
}
