package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class CreatePatchRecurringPaymentsForCancellationRequestBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject document;
		Optional<JsonObject> optionalData = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent.data"))
			.map(JsonElement::getAsJsonObject);

		//If brazilPaymentConsent is missing we create document from loggedUserIdentification
		if(optionalData.isPresent()){
			JsonObject data = optionalData.get();
			document = Optional.ofNullable(data.getAsJsonObject("businessEntity"))
				.map(businessEntity -> businessEntity.getAsJsonObject("document"))
				.orElse(data.getAsJsonObject("loggedUser").getAsJsonObject("document"));
		} else {
			String loggedUserIdentification = Optional.ofNullable(env.getElementFromObject("resource", "loggedUserIdentification"))
				.map(JsonElement::getAsString)
				.orElseThrow(() -> error("Unable to find POST recurring consents request data or loggedUserIdentification"));

			String rel = switch (loggedUserIdentification.length()) {
				case 14 -> "CNPJ";
				case 11 -> "CPF";
				default -> null;
			};

			if (rel == null) {
				throw error("POST recurring consents request data missing and invalid loggedUserIdentification: must be either 11 or 14 characters");
			}

			document = new JsonObject();
			document.addProperty("identification", loggedUserIdentification);
			document.addProperty("rel", rel);
		}

		JsonObject requestBody = new JsonObjectBuilder()
			.addField("data.status", "CANC")
			.addField("data.cancellation.cancelledBy.document", document.deepCopy())
			.build();

		env.putObject("resource_request_entity_claims", requestBody);
		logSuccess("Patch request body for recurring payments created", args("body", requestBody));

		return env;
	}
}
