package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

public abstract class AbstractRecurringConsentOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected ResponseEnvKey getResponseEnvKey() {
		return ResponseEnvKey.FullConsentResponseEnvKey;
	}

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/automatic-payments/swagger-automatic-payments-2.0.0-rc.1.yml";
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	protected void assertData(JsonObject data) {
		assertCreditorsConstraints(data);

		assertDebtorAccountConstraints(data);

		assertAdditionalStatusRelatedConstraints(data);

		assertApprovalDueDateConstraints(data);

		JsonObject recurringConfiguration = data.getAsJsonObject("recurringConfiguration");

		if (recurringConfiguration.has("automatic")) {
			assertAutomaticRecurringConfigurationConstraints(recurringConfiguration.getAsJsonObject("automatic"));
		}

		if (recurringConfiguration.has("sweeping")) {
			JsonObject sweepingConfig = recurringConfiguration.getAsJsonObject("sweeping");
			if (sweepingConfig.has("periodicLimits")) {
				assertSweepingOrVrpPeriodicLimitsConstraints(sweepingConfig.getAsJsonObject("periodicLimits"));
			}
		}

		if (recurringConfiguration.has("vrp")) {
			JsonObject vrpConfig = recurringConfiguration.getAsJsonObject("vrp");
			if (vrpConfig.has("periodicLimits")) {
				assertSweepingOrVrpPeriodicLimitsConstraints(vrpConfig.getAsJsonObject("periodicLimits"));
			}
		}
	}

	protected void assertCreditorsConstraints(JsonObject data) {
		JsonObject recurringConfiguration = data.getAsJsonObject("recurringConfiguration");
		if (recurringConfiguration.has("automatic")) {
			JsonArray creditors = data.getAsJsonArray("creditors");
			if (creditors.size() > 1) {
				throw error("There should only be one creditor when recurringConfiguration is set to automatic",
					args("number of creditors", creditors.size()));
			}
			JsonObject creditor = creditors.get(0).getAsJsonObject();
			String personType = OIDFJSON.getString(creditor.get("personType"));
			if (!personType.equals("PESSOA_JURIDICA")) {
				throw error("The only creditor should be of type PESSOA_JURIDICA when recurringConfiguration is set to automatic",
					args("personType", personType));
			}
		}
	}

	protected void assertDebtorAccountConstraints(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(data, "debtorAccount", "status", List.of("AUTHORISED", "REVOKED", "CONSUMED"));

		if (data.has("debtorAccount")) {
			JsonObject debtorAccount = data.getAsJsonObject("debtorAccount");
			assertField1IsRequiredWhenField2HasValue2(debtorAccount, "issuer", "accountType", List.of("CACC", "SVGS"));
			assertIbgeTownCodeConstraints(data, debtorAccount);
		}
	}

	protected void assertIbgeTownCodeConstraints(JsonObject data, JsonObject debtorAccount) {
		JsonObject recurringConfiguration = data.getAsJsonObject("recurringConfiguration");
		String status = OIDFJSON.getString(data.get("status"));
		List<String> mandatoryStatuses = List.of("AUTHORISED", "REVOKED", "CONSUMED");
		if (recurringConfiguration.has("automatic") && mandatoryStatuses.contains(status) && !debtorAccount.has("ibgeTownCode")) {
			throw error("debtorAccount.ibgeTownCode is required when recurringConfiguration is automatic and status is AUTHORISED, REVOKED or CONSUMED");
		}
	}

	protected void assertAdditionalStatusRelatedConstraints(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(data, "rejection", "status", "REJECTED");
		assertField1IsRequiredWhenField2HasValue2(data, "revocation", "status", "REVOKED");
		assertField1IsRequiredWhenField2HasValue2(data, "authorisedAtDateTime", "status", "AUTHORISED");
	}

	protected void assertApprovalDueDateConstraints(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(data, "approvalDueDate", "status", "PARTIALLY_ACCEPTED");
		if (data.has("approvalDueDate")) {
			String approvalDueDateStr = OIDFJSON.getString(data.get("approvalDueDate"));
			LocalDate approvalDueDate = LocalDate.parse(approvalDueDateStr);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

			String creationDateTimeStr = OIDFJSON.getString(data.get("creationDateTime"));
			LocalDate creationDate = LocalDate.parse(creationDateTimeStr, formatter);

			if (data.has("expirationDateTime")) {
				String expirationDateTime = OIDFJSON.getString(data.get("expirationDateTime"));
				LocalDate expirationDate = LocalDate.parse(expirationDateTime, formatter);

				long maxPlusDays = Math.min(30, ChronoUnit.DAYS.between(creationDate, expirationDate));
				LocalDate maxExpectedDate = creationDate.plusDays(maxPlusDays);
				if (approvalDueDate.isAfter(maxExpectedDate)) {
					throw error("The approvalDueDate should not exceed neither the expirationDateTime nor the creationDateTime + 30 days",
						args("approvalDueDate", approvalDueDate, "expirationDateTime", expirationDateTime, "creationDateTime", creationDateTimeStr));
				}
			} else {
				LocalDate expectedDate = creationDate.plusDays(30);
				if (!approvalDueDate.equals(expectedDate)) {
					throw error("When expirationDateTime is not specified, approvalDueDate should default to 30 days after creationDateTime",
						args("approvalDueDate", approvalDueDate, "expected", expectedDate));
				}
			}
		}
	}

	protected void assertAutomaticRecurringConfigurationConstraints(JsonObject automaticConfig) {
		boolean hasFixedAmount = isPresent(automaticConfig, "fixedAmount");
		boolean hasVariableAmount = isPresent(automaticConfig, "minimumVariableAmount") || isPresent(automaticConfig, "maximumVariableAmount");
		if (hasFixedAmount && hasVariableAmount) {
			throw error("The recurring payment can either have a fixed amount or a variable amount, not both");
		}

		if (automaticConfig.has("firstPayment")) {
			JsonObject firstPayment = automaticConfig.getAsJsonObject("firstPayment");
			JsonObject creditorAccount = firstPayment.getAsJsonObject("creditorAccount");
			assertField1IsRequiredWhenField2HasValue2(creditorAccount, "issuer", "accountType", List.of("CACC", "SVGS"));
		}
	}

	protected void assertSweepingOrVrpPeriodicLimitsConstraints(JsonObject periodicLimits) {
		String[] periods = {"day", "week", "month", "year"};
		for (String period : periods) {
			if (periodicLimits != null && periodicLimits.has(period) && periodicLimits.get(period).isJsonObject()) {
				JsonObject periodObject = periodicLimits.getAsJsonObject(period);
				boolean hasQuantityLimit = isPresent(periodObject, "quantityLimit");
				boolean hasTransactionLimit = isPresent(periodObject, "transactionLimit");

				if (!hasQuantityLimit && !hasTransactionLimit) {
					throw error(String.format("%s must have at least one of quantityLimit or transactionLimit.", period));
				}
			}
		}
	}
}

