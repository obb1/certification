package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiWrongPermissionsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractInstallmentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractPaymentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractWarrantiesResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForLoansRoot;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoansContractSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetLoansV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractLoansApiWrongPermissionsTest extends AbstractApiWrongPermissionsTestModulePhase2 {

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.LOANS;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetLoansV2Endpoint.class),
			condition(GetConsentV3Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> contractSelector() {
		return LoansContractSelector.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingRootEndpoint() {
		return PrepareUrlForLoansRoot.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractEndpoint() {
		return PrepareUrlForFetchingLoanContractResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractWarrantiesEndpoint() {
		return PrepareUrlForFetchingLoanContractWarrantiesResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractPaymentsEndpoint() {
		return PrepareUrlForFetchingLoanContractPaymentsResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractScheduledInstalmentsEndpoint() {
		return PrepareUrlForFetchingLoanContractInstallmentsResource.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
