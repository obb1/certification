package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsConsentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedBy.EnsureRecurringPaymentsConsentsRejectedByUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedFrom.EnsureRecurringPaymentsConsentsRejectedFromIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;

public abstract class AbstractAutomaticPaymentsApiRejectedConsentTestModule extends AbstractAutomaticPaymentsConsentsFunctionalTestModule {

	protected abstract Class<? extends Condition> patchPaymentConsentValidator();

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		runInBlock("Call PATCH consents - Expects 200", this::makePatchConsentsRequest);
		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		validateGetConsentResponse();
		runInBlock("Validate rejection fields", this::validateRejectionFields);
		fireTestFinished();
	}

	protected void makePatchConsentsRequest() {
		call(new CallPatchAutomaticPaymentsConsentsEndpointSequence());
		callAndContinueOnFailure(patchPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
	}

	protected void validateRejectionFields() {
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRejectedByUsuario.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRejectedFromIniciadora.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}
}
