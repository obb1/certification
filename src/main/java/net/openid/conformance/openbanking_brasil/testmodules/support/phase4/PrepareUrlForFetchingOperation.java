package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingOperation extends ResourceBuilder {

    @Override
    @PreEnvironment(strings = {"operationId"})
    public Environment evaluate(Environment env) {
        String api = "exchanges";
        String operationId = env.getString("operationId");

        setApi(api);
        setEndpoint("/operations/" + operationId);

        return super.evaluate(env);
    }
}
