package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.channels.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


/**
 * Api url: https://raw.githubusercontent.com/OpenBanking-Brasil/draft-openapi/main/swagger-apis/channels/2.0.1.yml
 * Api endpoint: /electronic-channels
 * Api version: v2.0.1
 */
@ApiName("Get Electronic Channels V2")
public class GetChannelsElectronicChannelsOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-channels-2.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/electronic-channels";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray dataArray = JsonPath.read(body, "$.data[*].identification");
		for(JsonElement dataElement : dataArray) {
			assertDataAdditionalConstraints(dataElement.getAsJsonObject());
		}
	}

	protected void assertDataAdditionalConstraints(JsonObject data) {
		this.assertField1IsRequiredWhenField2HasValue2(
			data,
			"additionalInfo",
			"type",
			"OUTROS"
		);
	}

}
