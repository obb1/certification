package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.webauthn4j.data.attestation.authenticator.COSEKey;
import net.openid.conformance.testmodule.Environment;

import java.util.UUID;

public class GenerateFidoAttestationObjectWithInvalidPublicKey extends GenerateFidoAttestationObject {

	@Override
	protected COSEKey getKey(Environment env) {
		return new InvalidPublicCOSEKey();
	}

	@Override
	protected String getKid(Environment env) {
		return UUID.randomUUID().toString();
	}
}
