package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class CreateEnrollmentsFidoSignOptionsRequestBodyToRequestEntityClaims extends AbstractCondition {

	/**
	 * {@link CreateEnrollmentsFidoRegistrationOptionsRequestBody} condition adds rp_id string to the env
	 **/
	@Override
	@PreEnvironment(strings = {"rp_id", "consent_id"})
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {

		JsonObject requestBody = new JsonObjectBuilder().addFields("data", Map.of(
			"rp", env.getString("rp_id"),
			"platform", "ANDROID",
			"consentId", env.getString("consent_id")
		)).build();

		env.putObject("resource_request_entity_claims", requestBody);
		logSuccess("Fido sign options request body was created and added to environment",
			args("fido registration options request body", requestBody));
		return env;
	}
}
