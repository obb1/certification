package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class ExtractConsentStatusUpdateDateTime extends AbstractCondition {
	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	@PostEnvironment(strings = "consent_status_update_date_time")
	public Environment evaluate(Environment env) {
		String path = "body_json.data.statusUpdateDateTime";
		String consentStatusUpdateDateTime = env.getString("consent_endpoint_response_full", path);
		if (consentStatusUpdateDateTime == null) {
			throw error("Couldn't find " + path + " in the consent response",
				args("consent_endpoint_response", env.getObject("consent_endpoint_response_full")));
		}
		env.putString("consent_status_update_date_time", consentStatusUpdateDateTime);
		logSuccess("Extracted consent status update date time", args("consent_status_update_date_time", consentStatusUpdateDateTime));
		return env;
	}
}
