package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;

public class EditRecurringPaymentsAutomaticConsentBodyToAddMinHigherThanMax extends AbstractEditRecurringPaymentsAutomaticConsentBody {

	@Override
	protected void updateAutomaticObject(JsonObject automatic) {
		automatic.addProperty("minimumVariableAmount", "1.00");
		automatic.addProperty("maximumVariableAmount", "0.50");
		automatic.remove("fixedAmount");
	}

	@Override
	protected String logMessage() {
		return "have \"minimumVariableAmount\" value higher than \"maximumVariableAmount\"";
	}
}
