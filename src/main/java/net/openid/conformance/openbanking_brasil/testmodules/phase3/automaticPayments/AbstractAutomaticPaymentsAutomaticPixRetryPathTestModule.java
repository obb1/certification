package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddClientAssertionToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddClientIdToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CompareIdTokenClaims;
import net.openid.conformance.condition.client.CreateClientAuthenticationAssertionClaims;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.EnsureAccessTokenValuesAreDifferent;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromOBResourceConfiguration;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromResourceConfiguration;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.condition.client.FAPIBrazilGetKeystoreJwksUri;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentConsentRequest;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.condition.client.SignClientAuthenticationAssertion;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringConsentIdToQuery;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringPaymentResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateDummyPaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureNoFirstPaymentInformationIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractIspbFromCurrentE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractOriginalRecurringPaymentIdFromListResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.FAPIBrazilAddRecurringConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.AbstractEnsureIsRetryAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.AbstractEnsurePaymentListDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.RefreshTokenRequestSteps;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractAutomaticPaymentsAutomaticPixRetryPathTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {
	/**
	 * This class should be extended by automatic pix tests that follow the steps bellow:
	 *     - Call the GET recurring-consents {recurringConsentId} endpoint, using the {value} specified in the Config and a client_credentials token.
	 *     - Expect a 200 response - Validate the response, ensure the status is "Authorised", isRetryAccepted is {value}, no firstPayment information is present.
	 *     - Call the GET recurring-payment endpoint with the recurringConsent ID in the header.
	 *     - Expect a 200 response - Validate the response, ensure there is {value} payment(s), the status is "RJCT", and the payment date is set to {value}.
	 *     - Call the GET recurring-payments {recurringPaymentId} endpoint to fetch the original payment data.
	 *     - Expect a 200 response - Validate the response and extract its fields.
	 *     - Call the POST recurring-payments endpoint, using an access token derived from the refresh token in the configuration. Send a new E2EID, date as {value}, and the other fields with the same information as the previous payment, and include the first paymentId as originalRecurringPaymentId.
	 *     - Expect 422 {value} or 201 - Validate Response
	 *     If a 201 is returned:
	 *     - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP
	 *     - Call the GET recurring-payments {recurringPaymentId}
	 *     - Expect 200 - Validate Response and ensure status is {value}, ensure that rejectionReason.code is {value}
	 * In order to properly fetch the previously created recurring-consent and create the new recurring-payment, the user will need to provide the following fields:
	 *     - resource.recurringConsentId
	 *     - resource.refreshToken
	 */

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return null;
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return null;
	}

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddRecurringPaymentResourceUrlToConfig.class);
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
		callAndStopOnFailure(ExtractTLSTestValuesFromResourceConfiguration.class);
		callAndContinueOnFailure(ExtractTLSTestValuesFromOBResourceConfiguration.class, Condition.ConditionResult.INFO);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class);
		callAndStopOnFailure(getSetConfigurationFieldsForRetryTestCondition().getClass());
	}

	@Override
	protected Class<? extends Condition> addAutomaticPaymentToTheResource() {
		return null;
	}

	@Override
	protected void configurePaymentDate() {
	}

	@Override
	protected void configureCreditorFields() {}

	@Override
	protected void performAuthorizationFlow(){
		createClientCredentialsToken();
		getAndValidateRecurringConsent();
		getAndValidateRecurringPaymentsList();
		getAndValidateOriginalRecurringPayment();
		createRefreshToken();
		postAndValidateRecurringPayment();
		onPostAuthorizationFlowComplete();
	}

	protected void createClientCredentialsToken() {
		runInBlock("Create a client_credentials token", () -> {
			call(callTokenEndpointShortVersion());
		});
	}

	protected ConditionSequence callTokenEndpointShortVersion() {
		return sequenceOf(
			condition(CreateTokenEndpointRequestForClientCredentialsGrant.class),
			condition(SetRecurringPaymentsScopeOnTokenEndpointRequest.class),
			condition(AddClientIdToTokenEndpointRequest.class),
			condition(CreateClientAuthenticationAssertionClaims.class).dontStopOnFailure(),
			condition(SignClientAuthenticationAssertion.class).dontStopOnFailure(),
			condition(AddClientAssertionToTokenEndpointRequest.class).dontStopOnFailure(),
			condition(CallTokenEndpoint.class),
			condition(CheckIfTokenEndpointResponseError.class),
			condition(ExtractAccessTokenFromTokenResponse.class),
			condition(SaveAccessToken.class)
		);
	}

	protected void getAndValidateRecurringConsent() {
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(CreateDummyPaymentConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilSignPaymentConsentRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		runInBlock("Validate recurring-consent", () -> {
			callAndContinueOnFailure(EnsureConsentResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getEnsureIsRetryAcceptedCondition().getClass(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNoFirstPaymentInformationIsPresent.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void getAndValidateRecurringPaymentsList() {
		runInBlock("Call GET recurring-payments endpoint to check all payments", () -> {
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
			callAndStopOnFailure(AddRecurringConsentIdToQuery.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
			callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
			callAndContinueOnFailure(getPaymentsByConsentIdValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getEnsureThereArePaymentsCondition().getClass(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getEnsurePaymentListDatesCondition().getClass(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(ExtractOriginalRecurringPaymentIdFromListResponse.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureRejectionReasonsFromRecurringPaymentsListResponseDemandNewE2EID.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void getAndValidateOriginalRecurringPayment() {
		runInBlock("Call GET recurring-payments/{recurringPaymentId} endpoint to fetch original payment data", () -> {
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentEndpoint.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
			callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE, "FAPI1-BASE-6.2.1-11");
			callAndContinueOnFailure(getPaymentValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void createRefreshToken() {
		userAuthorisationCodeAccessToken();
		callAndStopOnFailure(FAPIBrazilAddRecurringConsentIdToClientScope.class);
		call(new RefreshTokenRequestSteps(false, addTokenEndpointClientAuthentication)
			.skip(EnsureAccessTokenValuesAreDifferent.class, "Will not compare tokens")
			.skip(CompareIdTokenClaims.class, "Will not compare tokens"));
	}

	protected void postAndValidateRecurringPayment() {
		userAuthorisationCodeAccessToken();
		configurePayment();
		requestProtectedResource();
	}

	protected void configurePayment() {
		callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
		callAndStopOnFailure(FAPIBrazilGetKeystoreJwksUri.class);
		callAndStopOnFailure(setPaymentDateCondition().getClass());
		callAndStopOnFailure(ExtractIspbFromCurrentE2EID.class);
		callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return super.getPixPaymentSequence().replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class));
	}

	@Override
	protected void validateResponse() {
		int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
		if (status == HttpStatus.CREATED.value()) {
			super.validateResponse();
		} else if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			call(postPaymentValidationSequence());
			env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndStopOnFailure(errorCodeCondition().getClass());
			env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		}
	}

	@Override
	protected void validateFinalState() {
		callAndContinueOnFailure(EnsurePaymentStatusWasRjct.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(rejectionReasonCondition().getClass(), Condition.ConditionResult.FAILURE);
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpV2.class;
	}

	protected abstract AbstractSetConfigurationFieldsForRetryTest getSetConfigurationFieldsForRetryTestCondition();
	protected abstract AbstractEnsureIsRetryAccepted getEnsureIsRetryAcceptedCondition();
	protected abstract AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition();
	protected abstract AbstractEnsurePaymentListDates getEnsurePaymentListDatesCondition();
	protected abstract AbstractEditRecurringPaymentsBodyToSetDate setPaymentDateCondition();
	protected abstract AbstractEnsureRecurringPaymentsRejectionReason rejectionReasonCondition();
	protected abstract AbstractEnsureErrorResponseCodeFieldWas errorCodeCondition();
	protected abstract Class<? extends Condition> getPaymentsByConsentIdValidator();
}
