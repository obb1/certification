package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

public class CreateSweepingRecurringConfigurationObjectWithAllLimitsSetToOne extends AbstractCreateRecurringConfigurationObject {

	private static final String AMOUNT = "1.00";
	private static final String TRANSACTION_LIMIT = "1.00";
	private static final int QUANTITY_LIMIT = 1;

	@Override
	protected JsonObject buildRecurringConfiguration(Environment env) {
		return addSweepingFields(new JsonObjectBuilder(), AMOUNT, TRANSACTION_LIMIT, QUANTITY_LIMIT)
			.build();
	}
}
