package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums;

public interface PermissionsEnum {

	String getPermission();
}
