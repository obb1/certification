package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditClientDataToSetInvalidOrigin;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas.EnsureEnrollmentRejectionReasonWasRejeitadoFalhaFido;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasOrigemFidoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;

public abstract class AbstractEnrollmentsApiInvalidOriginTestModule extends AbstractCompleteEnrollmentsApiEnrollmentTestModule {

	@Override
	protected void postAndValidateFidoRegistration() {
		runInBlock("Post fido-registration with invalid origin - Expects 422 ORIGEM_FIDO_INVALIDA", () -> {
			prepareEnvironmentForPostFidoRegistration();
			call(createPostFidoRegistrationSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postFidoRegistrationValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasOrigemFidoInvalida.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasOrigemFidoInvalida.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasOrigemFidoInvalida.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void prepareEnvironmentForPostFidoRegistration() {
		super.prepareEnvironmentForPostFidoRegistration();
		callAndContinueOnFailure(EditClientDataToSetInvalidOrigin.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
		callAndContinueOnFailure(EnsureEnrollmentRejectionReasonWasRejeitadoFalhaFido.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void executeTestSteps() {}

	protected abstract Class<? extends Condition> postFidoRegistrationValidator();

}
