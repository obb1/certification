package net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.v3.AbstractCustomerDataXFapiTestModuleV3;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "customer-business_api_x-fapi_test-module_v2",
	displayName = "customer-business_api_x-fapi_test-module_v2",
	summary = "Test will ensure that the x-fapi-interaction-id is required at the request\n" +
		"\u2022 Call the POST Consents endpoint with the Customer Business API Permission Group\n" +
		"\u2022 Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"\u2022 Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"\u2022 Call the GET Consents endpoint\n" +
		"\u2022 Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"\u2022 Call the GET Identifications Endpoint without the x-fapi-interaction-id\n" +
		"\u2022 Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"\u2022 Call the GET Identifications Endpoint with an invalid x-fapi-interaction-id\n" +
		"\u2022 Expects 400 - Validate error message\n" +
		"\u2022 Call the GET Identifications Endpoint with the x-fapi-interaction-id\n" +
		"\u2022 Expects 200 - Validate response\n",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class CustomerBusinessApiXFapiTestModuleV2 extends AbstractCustomerDataXFapiTestModuleV3 {
	@Override
	protected boolean isConsents() {
		return false;
	}

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return BusinessIdentificationOASValidatorV2.class;
	}
	@Override
	protected OPFCategoryEnum getCategory() { return OPFCategoryEnum.BUSINESS_REGISTRATION_DATA; }
	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.CUSTOMERS;
	}
	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetBusinessIdentificationsV2Endpoint.class)
		);
	}
}
