package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalRandomAccountNumberAndIspb;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentRejectionReasonCodeWasTitularidadeInconsistente;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixWrongCreditorTestModule extends AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule {

	@Override
	protected boolean isHappyPath() {
		return true;
	}

	@Override
	protected AbstractEnsurePaymentStatusWasX finalPaymentStatusCondition() {
		return new EnsurePaymentStatusWasRjct();
	}

	@Override
	protected AbstractEnsurePaymentConsentStatusWas finalConsentStatusCondition() {
		return new EnsurePaymentConsentStatusWasAuthorised();
	}

	@Override
	protected String finalConsentStatus() {
		return "AUTHORISED";
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorResponseCodeFieldCondition() {
		return null;
	}

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason ensureRejectionReasonCondition() {
		return new EnsureRecurringPaymentRejectionReasonCodeWasTitularidadeInconsistente();
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalRandomAccountNumberAndIspb();
	}

	@Override
	protected void validateFinalState() {
		super.validateFinalState();
		callAndContinueOnFailure(ensureRejectionReasonCondition().getClass(), Condition.ConditionResult.FAILURE);
	}
}
