package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment;

public class AddFirstCnpjToPaymentRequestBody extends AbstractAddCnpjToPaymentRequestBody {

	@Override
	protected String getKey() {
		return "first_creditor";
	}
}
