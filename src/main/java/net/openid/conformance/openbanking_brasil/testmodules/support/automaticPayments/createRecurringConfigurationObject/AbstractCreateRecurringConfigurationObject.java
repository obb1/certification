package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public abstract class AbstractCreateRecurringConfigurationObject extends AbstractCondition {

	protected static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
	protected static final String AMOUNT = "600.00";

	@Override
	@PostEnvironment(required = "recurring_configuration_object")
	public Environment evaluate(Environment env) {
		JsonObject recurringConfiguration = buildRecurringConfiguration(env);
		env.putObject("recurring_configuration_object", recurringConfiguration);
		logSuccess("The recurringConfiguration object has been created",
			env.getObject("recurring_configuration_object"));
		return env;
	}

	protected abstract JsonObject buildRecurringConfiguration(Environment env);

	protected JsonObjectBuilder addSweepingFields(JsonObjectBuilder builder, String amount, String transactionLimit, int quantityLimit) {
		return builder
			.addFields("sweeping", Map.of(
				"totalAllowedAmount", amount,
				"transactionLimit", transactionLimit,
				"startDateTime", getStartDateTime()
			))
			.addFields("sweeping.periodicLimits.day", Map.of(
				"quantityLimit", quantityLimit,
				"transactionLimit", transactionLimit
			))
			.addFields("sweeping.periodicLimits.week", Map.of(
				"quantityLimit", quantityLimit,
				"transactionLimit", transactionLimit
			))
			.addFields("sweeping.periodicLimits.month", Map.of(
				"quantityLimit", quantityLimit,
				"transactionLimit", transactionLimit
			))
			.addFields("sweeping.periodicLimits.year", Map.of(
				"quantityLimit", quantityLimit,
				"transactionLimit", transactionLimit
			));
	}

	protected String getStartDateTime() {
		LocalDateTime now = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		return now.format(DATE_TIME_FORMATTER);
	}
}
