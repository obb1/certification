package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

public class AddRecurringPaymentConsentRequestBodyToConfig extends AbstractCondition {

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private static final int EXPIRATION_TIME_DAYS = 180;

	@Override
	@PreEnvironment(required = "config")

	public Environment evaluate(Environment env) {
		JsonElement brasilPaymentConsent = env.getElementFromObject("config", "resource.brazilAutomaticPaymentConsentFvp");
		if (brasilPaymentConsent == null) {

			String debtorAccountIspb = extractFromConfigOrDie(env, "resource.debtorAccountIspb");
			String debtorAccountIssuer = extractFromConfigOrDie(env, "resource.debtorAccountIssuer");
			String debtorAccountNumber = extractFromConfigOrDie(env, "resource.debtorAccountNumber");
			String debtorAccountType = extractFromConfigOrDie(env, "resource.debtorAccountType");
			String loggedUserIdentification = Optional.ofNullable(env.getString("config", "resource.loggedUserIdentification"))
				.orElse(DictHomologKeys.PROXY_EMAIL_CPF);
			var recurringConfiguration = Optional.ofNullable(env.getObject("recurring_configuration_object"));
			var businessEntityIdentification = Optional.ofNullable(env.getString("config", "resource.businessEntityIdentification"));

			String creditorName = extractFromConfigOrDie(env, "resource.creditorName");
			String creditorIdentification = businessEntityIdentification.orElse(loggedUserIdentification);
			String creditorPersonType = businessEntityIdentification.isPresent() ? "PESSOA_JURIDICA" : "PESSOA_NATURAL";
			JsonArray creditors = new JsonArray();
			creditors.add(
				new JsonObjectBuilder()
					.addField("personType", creditorPersonType)
					.addField("cpfCnpj", creditorIdentification)
					.addField("name", creditorName)
					.build()
			);

			LocalDateTime now = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
			String expirationDateTime = now.plusDays(EXPIRATION_TIME_DAYS).format(DATE_TIME_FORMATTER);

			JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
				.addFields("data.loggedUser.document", Map.of(
					"identification", loggedUserIdentification,
					"rel", "CPF")
				)
				.addFields("data.debtorAccount", Map.of(
					"ispb", debtorAccountIspb,
					"issuer", debtorAccountIssuer,
					"number", debtorAccountNumber,
					"accountType", debtorAccountType)
				)
				.addFields("data", Map.of(
					"creditors", creditors,
					"expirationDateTime", expirationDateTime
				));

			recurringConfiguration.ifPresent(recConfigObj -> jsonObjectBuilder
				.addField("data.recurringConfiguration", recConfigObj));

			businessEntityIdentification.ifPresent(identification -> jsonObjectBuilder
				.addFields("data.businessEntity.document", Map.of(
					"identification", identification,
					"rel", "CNPJ"
				)));

			JsonObject consentRequest = jsonObjectBuilder.build();
			env.putObject("config", "resource.brazilPaymentConsent", consentRequest);
			logSuccess("Consent request body was added to the config", args("consentRequest", consentRequest));
		}else
		{
			var recurringConfiguration = Optional.ofNullable(env.getObject("recurring_configuration_object"));
			recurringConfiguration.ifPresent(recConfigObj -> {
				JsonObject data = brasilPaymentConsent.getAsJsonObject().getAsJsonObject("data");
				data.remove("payment");
				data.add("recurringConfiguration", recConfigObj);
			});
			env.putObject("config", "resource.brazilPaymentConsent", brasilPaymentConsent.getAsJsonObject());
			logSuccess("Consent request body was added to the config", args("consentRequest", brasilPaymentConsent));
		}
			return env;
	}

	private String extractFromConfigOrDie(Environment env, final String path) {
		Optional<String> string = Optional.ofNullable(env.getString("config", path));
		return string.orElseThrow(() -> error(String.format("Unable to find element %s in config", path)));
	}
}
