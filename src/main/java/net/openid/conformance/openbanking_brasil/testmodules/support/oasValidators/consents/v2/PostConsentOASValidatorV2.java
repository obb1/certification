package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v2;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.AbstractPostConsentOASValidator;

@ApiName("Post Consent V2")
public class PostConsentOASValidatorV2 extends AbstractPostConsentOASValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/consents/swagger-consents-2.1.0.yml";
	}
}
