package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

public class ValidatePaymentConsentWebhooksOptionalAmount extends AbstractValidateWebhooksReceived {


	@Override
	protected int expectedWebhooksReceivedAmount() {
		return 2;
	}

	@Override
	protected String webhookType() {
		return "consent";
	}
}
