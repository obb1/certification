package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.consent.v2.LinksAndMetaConsentValidatorV2;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

public class GetConsentExtensionValidatorV2 extends AbstractJsonAssertingCondition {
	private final LinksAndMetaConsentValidatorV2 linksAndMetaValidator = new LinksAndMetaConsentValidatorV2(this);

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment,"resource_endpoint_response_full");
		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(this::assertInnerFields)
				.build());
		linksAndMetaValidator.assertMetaAndLinks(body);
		logFinalStatus();
		return environment;
	}

	private void assertInnerFields(JsonObject body) {
		assertField(body,
			new DatetimeField
				.Builder("requestDateTime")
				.setMaxLength(20)
				.build());


		assertField(body,
			new DatetimeField
				.Builder("expirationDateTime")
				.setMaxLength(20)
				.build());

		assertField(body,
			new ObjectField
				.Builder("loggedUser")
				.setValidator(this::assertLoggedUser)
				.build());

	}

	private void assertLoggedUser(JsonObject rejection) {
		assertField(rejection,
			new ObjectField
				.Builder("document")
				.setValidator(this::assertDocument)
				.setOptional()
				.build());
	}

	private void assertDocument(JsonObject rejection) {
		assertField(rejection,
			new StringField
				.Builder("identification")
				.setPattern("^\\d{11}$")
				.setMaxLength(11)
				.build());

		assertField(rejection,
			new StringField
				.Builder("rel")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}
}
