package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiOperationalLimitsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractInstallmentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractPaymentsResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.PrepareUrlForFetchingLoanContractWarrantiesResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetLoansV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

import static net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSpecificCreditOperationsPermissionsWereReturned.CreditOperationsPermissionsType;

public abstract class AbstractLoansApiOperationalLimitsTest extends AbstractApiOperationalLimitsTestModulePhase2 {

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getResourceEndpoint() {
		return GetLoansV2Endpoint.class;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.LOANS;
	}

	@Override
	protected CreditOperationsPermissionsType getPermissionType() {
		return CreditOperationsPermissionsType.LOAN;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractId() {
		return PrepareUrlForFetchingLoanContractResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingWarranties() {
		return PrepareUrlForFetchingLoanContractWarrantiesResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingScheduledInstalments() {
		return PrepareUrlForFetchingLoanContractInstallmentsResource.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingPayments() {
		return PrepareUrlForFetchingLoanContractPaymentsResource.class;
	}
}
