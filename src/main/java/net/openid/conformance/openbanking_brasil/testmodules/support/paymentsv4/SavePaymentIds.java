package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SavePaymentIds extends AbstractCondition {
	@Override
	@PreEnvironment(required = "payment_ids")
	@PostEnvironment(required = "saved_payment_ids")
	public Environment evaluate(Environment env) {
		env.putObject("saved_payment_ids", env.getObject("payment_ids").deepCopy());
		logSuccess("The payment ids have been successfully copied", env.getObject("saved_payment_ids"));
		return env;
	}
}
