package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiNegativeConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_negative-consents_test-module_v1",
	displayName = "automatic-payments_api_negative-consents_test-module_v1",
	summary = "Ensure validations are being executed at the POST recurring-consent endpoint, and a consent cannot be created in unhappy requests\n" +
		"• Call the POST recurring-consents endpoint without the recurringConfiguration.sweeping field \n" +
		"• Expect 422 PARAMETRO_NAO_INFORMADO or 400 - Validate Error Message\n" +
		"• Call the POST recurring-consents endpoint with the start date time as D+10 and expirationDateTime as D+5\n" +
		"• Expect 422 PARAMETRO_INVALIDO - Validate Error Message\n" +
		"• Call the POST recurring-consents endpoint without the x-fapi-interaction-id\n" +
		"• Expect 400 - Validate Error Message and check if a x-fapi-interaction-id has been sent back \n" +
		"• Call the POST recurring-consents endpoint with the x-fapi-interaction-id as '123456'\n" +
		"• Expect 400 or 422 PARAMETRO_INVALIDO - Validate Error Message and check if a x-fapi-interaction-id has been sent back",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiNegativeConsentsTestModuleV1 extends AbstractAutomaticPaymentsApiNegativeConsentsTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class));
	}
}
