package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class SetProtectedResourceUrlToRecurringPaymentEndpoint extends ResourceBuilder {

	@Override
	@PreEnvironment(strings = "payment_id")
	public Environment evaluate(Environment env) {
		String paymentId = env.getString("payment_id");
		setApi("automatic-payments");
		setEndpoint("/pix/recurring-payments/" + paymentId);

		return super.evaluate(env);
	}
}
