package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.AbstractGetUnarrangedAccountsOverdraftScheduledInstalmentsOASValidator;


public class GetUnarrangedAccountsOverdraftScheduledInstalmentsV2n3OASValidator extends AbstractGetUnarrangedAccountsOverdraftScheduledInstalmentsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/unarrangedAccountsOverdraft/unarranged-accounts-overdraft-v2.3.0.yml";
	}
}
