package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiNegativeConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetStartDateTimeAfterExpiration;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_negative-consents_test-module_v2",
	displayName = "automatic-payments_api_negative-consents_test-module_v2",
	summary = "Ensure validations are being executed at the POST recurring-consent endpoint, and a consent cannot be created in unhappy requests\n" +
		"• Call the POST recurring-consents endpoint without the recurringConfiguration.sweeping field \n" +
		"• Expect 422 PARAMETRO_NAO_INFORMADO or 400 - Validate Error Message\n" +
		"• Call the POST recurring-consents endpoint with the start date time as D+10 and expirationDateTime as D+5\n" +
		"• Expect 422 PARAMETRO_INVALIDO - Validate Error Message\n" +
		"• Call the POST recurring-consents endpoint without the x-fapi-interaction-id\n" +
		"• Expect 400 - Validate Error Message and check if a x-fapi-interaction-id has been sent back \n" +
		"• Call the POST recurring-consents endpoint with the x-fapi-interaction-id as '123456'\n" +
		"• Expect 400 - Validate Error Message and check if a x-fapi-interaction-id has been sent back",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiNegativeConsentsTestModuleV2 extends AbstractAutomaticPaymentsApiNegativeConsentsTestModule {

	@Override
	protected void postAndValidateConsentWithInvalidFapiInteractionId() {
		runInBlock("POST consents with the x-fapi-interaction-id as '123456' - Expects 400", () -> {
			callAndStopOnFailure(LoadConsentsBody.class);
			call(postConsentSequence()
				.replace(CreateRandomFAPIInteractionId.class, condition(CreateInvalidFAPIInteractionId.class)));
			callAndContinueOnFailure(EnsureConsentResponseCodeWas400.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void postAndValidateConsentWithStartDateAfterExpirationDate() {
		performTestStep(
			"POST consents with the startDateTime as D+10 and expirationDateTime as D+5 - Expects 422 PARAMETRO_INVALIDO",
			EditRecurringPaymentsConsentBodyToSetStartDateTimeAfterExpiration.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentErrorValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class));
	}
}
