package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EnsurePortabilitiesReceived extends AbstractJsonAssertingCondition {
	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject body = bodyFrom(env, "resource_endpoint_response_full").getAsJsonObject();
		JsonObject data = Optional.ofNullable(body.getAsJsonObject("data"))
			.orElseThrow(() -> error("the field data is empty"));

		JsonArray portabilitiesReceived = Optional.ofNullable(data.getAsJsonArray("portabilitiesReceived"))
			.orElse(new JsonArray());
		if(portabilitiesReceived.isEmpty()) {
			throw error("data.portabilitiesReceived is empty", args(
				"data", data
			));
		}

		logSuccess("data.portabilitiesReceived is present in the response");
		return env;
	}
}
