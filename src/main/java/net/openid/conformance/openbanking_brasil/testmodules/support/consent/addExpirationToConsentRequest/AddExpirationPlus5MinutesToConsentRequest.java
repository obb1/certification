package net.openid.conformance.openbanking_brasil.testmodules.support.consent.addExpirationToConsentRequest;

import java.time.temporal.ChronoUnit;

public class AddExpirationPlus5MinutesToConsentRequest extends AbstractAddExpirationToConsentRequest {

	@Override
	protected int amountToAdd() {
		return 5;
	}

	@Override
	protected ChronoUnit timeUnit() {
		return ChronoUnit.MINUTES;
	}
}
