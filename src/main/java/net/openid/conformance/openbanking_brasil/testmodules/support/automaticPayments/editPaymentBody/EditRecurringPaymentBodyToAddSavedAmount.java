package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditRecurringPaymentBodyToAddSavedAmount extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource", strings = "payment_amount")
	public Environment evaluate(Environment env) {
		JsonObject payment = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(paymentElement -> paymentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("payment"))
			.orElseThrow(() -> error("Unable to find data.payment in payments payload"));

		String amount = env.getString("payment_amount");

		payment.addProperty("amount", amount);
		logSuccess("Payment payload has been updated with saved amount", args("payment", payment));

		return env;
	}
}
