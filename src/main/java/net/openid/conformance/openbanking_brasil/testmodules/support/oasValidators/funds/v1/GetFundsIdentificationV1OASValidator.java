package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetFundsIdentificationV1OASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/funds/funds-v1.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
