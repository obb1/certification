package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetConsentOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/consents/{consentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertRejectionConstraint(data);
	}

	protected void assertRejectionConstraint(JsonObject data) {
		String status = OIDFJSON.getString(data.get("status"));
		if("REJECTED".equals(status) && data.get("rejection") == null) {
			throw error("data.rejection is required when data.status is REJECTED");
		}
	}

	@Override
	protected ResponseEnvKey getResponseEnvKey() {
		return ResponseEnvKey.FullConsentResponseEnvKey;
	}
}
