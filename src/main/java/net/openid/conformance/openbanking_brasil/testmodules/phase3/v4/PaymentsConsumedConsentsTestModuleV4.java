package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsumedConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_consumed-consent_test-module_v4",
	displayName = "Payments API basic consumed consent test module",
	summary = "1. Ensure an error is sent when consent is reused at a failed payment flow\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AWAITING_AUTHORISATION\"\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments Endpoint without the EndtoEndId field\n" +
		"\u2022 Expects 422 PARAMETRO_NAO_INFORMADO \n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"CONSUMED\"\n" +
		"\u2022 Calls the POST Payments with the EndtoEndId field \n" +
		"\u2022 Expects 422 CONSENTIMENTO_INVALIDO - Validate error message\n" +
		"2. Ensure an error is sent when consent is reused at a success payment flow\n" +
		"\u2022 Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"\u2022 Expects 201 - Validate if status is \"AWAITING_AUTHORISATION\"\n" +
		"\u2022 Redirects the user to authorize the created consent    \n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022 Calls the POST Payments with valid payload\n" +
		"\u2022 Expects 201 - Validate Response    \n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"CONSUMED\"\n" +
		"\u2022 Calls the POST Payments with the same consent and payload, but different idempotency key\n" +
		"\u2022 Expects 422 CONSENTIMENTO_INVALIDO - Validate error message",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)

public class PaymentsConsumedConsentsTestModuleV4 extends AbstractPaymentsConsumedConsentsTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
