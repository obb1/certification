package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums.CreatePaymentErrorEnumV4;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasConsentimentoPendeteAutorizacao extends AbstractEnsureErrorResponseCodeFieldWas {
	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreatePaymentErrorEnumV4.CONSENTIMENTO_PENDENTE_AUTORIZACAO.toString());
	}
}
