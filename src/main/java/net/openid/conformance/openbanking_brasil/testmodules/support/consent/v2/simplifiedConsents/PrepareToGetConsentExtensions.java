package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class PrepareToGetConsentExtensions extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config", strings = "consent_id")
	@PostEnvironment(strings = {"consent_extension_url", "http_method"})
	public Environment evaluate(Environment env) {
		String consentUrl = env.getString("config", "resource.consentUrl");
		if (consentUrl == null) {
			throw error("Couldn't find consentUrl in configuration");
		}
		String consentId = env.getString("consent_id");
		consentUrl = String.format("%s/%s/extensions", consentUrl, consentId);
		env.putString("consent_extension_url", consentUrl);
		env.putString("http_method", "GET");
		log("Fetching consent extension from consent API", args("consentId", consentId, "url", consentUrl));
		return env;
	}
}
