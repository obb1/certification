package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

import java.util.List;

public class EnsureResourceResponseCodeWas401 extends AbstractEnsureResourceResponseCode {

	@Override
	protected List<HttpStatus> getExpectedStatus() {
		return List.of(HttpStatus.UNAUTHORIZED);
	}

}
