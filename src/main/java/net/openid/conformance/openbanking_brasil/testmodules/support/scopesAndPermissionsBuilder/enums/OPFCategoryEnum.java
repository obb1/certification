package net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public enum OPFCategoryEnum implements CategoryEnum {

	ALL_PHASE4(OPFGroupingEnum.INVESTMENTS, OPFGroupingEnum.EXCHANGES),

	ALL_PERSONAL_PHASE2(OPFGroupingEnum.PERSONAL_REGISTRATION_DATA, OPFGroupingEnum.PERSONAL_ADDITIONAL_INFO,
		OPFGroupingEnum.ACCOUNTS_BALANCES, OPFGroupingEnum.ACCOUNTS_LIMITS, OPFGroupingEnum.ACCOUNTS_TRANSACTIONS,
		OPFGroupingEnum.CREDIT_CARDS_BILLS, OPFGroupingEnum.CREDIT_CARDS_LIMITS, OPFGroupingEnum.CREDIT_CARDS_TRANSACTIONS,
		OPFGroupingEnum.CREDIT_OPERATIONS
	),
	ALL_BUSINESS_PHASE2(OPFGroupingEnum.BUSINESS_REGISTRATION_DATA, OPFGroupingEnum.BUSINESS_ADDITIONAL_INFO,
		OPFGroupingEnum.ACCOUNTS_BALANCES, OPFGroupingEnum.ACCOUNTS_LIMITS, OPFGroupingEnum.ACCOUNTS_TRANSACTIONS,
		OPFGroupingEnum.CREDIT_CARDS_BILLS, OPFGroupingEnum.CREDIT_CARDS_LIMITS, OPFGroupingEnum.CREDIT_CARDS_TRANSACTIONS,
		OPFGroupingEnum.CREDIT_OPERATIONS
	),
	PERSONAL_REGISTRATION_DATA(OPFGroupingEnum.PERSONAL_REGISTRATION_DATA, OPFGroupingEnum.PERSONAL_ADDITIONAL_INFO),
	BUSINESS_REGISTRATION_DATA(OPFGroupingEnum.BUSINESS_REGISTRATION_DATA, OPFGroupingEnum.BUSINESS_ADDITIONAL_INFO),
	ACCOUNTS(OPFGroupingEnum.ACCOUNTS_BALANCES, OPFGroupingEnum.ACCOUNTS_LIMITS, OPFGroupingEnum.ACCOUNTS_TRANSACTIONS),
	CREDIT_CARDS_ACCOUNTS(OPFGroupingEnum.CREDIT_CARDS_BILLS, OPFGroupingEnum.CREDIT_CARDS_LIMITS, OPFGroupingEnum.CREDIT_CARDS_TRANSACTIONS),
	CREDIT_OPERATIONS(OPFGroupingEnum.CREDIT_OPERATIONS),
	INVESTMENTS(OPFGroupingEnum.INVESTMENTS),
	EXCHANGES(OPFGroupingEnum.EXCHANGES),
	;

	private final Set<GroupingEnum> relatedGroupings;

	OPFCategoryEnum(GroupingEnum... relatedGroupings) {
		this.relatedGroupings = Arrays.stream(relatedGroupings).collect(Collectors.toSet());
	}

	@Override
	public Set<PermissionsEnum> getPermissions() {
		Set<PermissionsEnum> permissions = new HashSet<>();
		for (GroupingEnum grouping : this.relatedGroupings) {
			Set<PermissionsEnum> individualPermissions = grouping.getPermissions();
			permissions.addAll(individualPermissions);
		}
		return permissions;
	}

	@Override
	public Set<ScopesEnum> getScopes() {
		Set<ScopesEnum> scopes = new HashSet<>();
		for (GroupingEnum grouping : this.relatedGroupings) {
			scopes.addAll(grouping.getScopes());
		}
		return scopes;
	}


}
