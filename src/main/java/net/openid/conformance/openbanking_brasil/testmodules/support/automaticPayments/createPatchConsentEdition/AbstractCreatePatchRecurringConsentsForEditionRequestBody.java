package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractCreatePatchRecurringConsentsForEditionRequestBody extends AbstractCondition {

	protected static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");

		String loggedUserIdentification = Optional.ofNullable(resource.get("loggedUserIdentification"))
			.map(OIDFJSON::getString)
			.orElse(DictHomologKeys.PROXY_EMAIL_CPF);
		var businessEntityIdentification = Optional.ofNullable(resource.get("businessEntityIdentification"));

		JsonObjectBuilder consentRequestObjBuilder = new JsonObjectBuilder()
			.addField("data.creditors", getCreditors())
			.addFields("data.loggedUser.document", Map.of(
				"identification", loggedUserIdentification,
				"rel", "CPF"
			));

		if (getRiskSignals() != null) {
			consentRequestObjBuilder.addField("data.riskSignals", getRiskSignals());
		}

		if (getExpirationDateTime() != null) {
			consentRequestObjBuilder.addField("data.expirationDateTime", getExpirationDateTime());
		}

		if (getMaxVariableAmount() != null) {
			consentRequestObjBuilder.addField("data.recurringConfiguration.automatic.maximumVariableAmount", getMaxVariableAmount());
		}

		businessEntityIdentification.ifPresent(identification -> consentRequestObjBuilder.addFields("data.businessEntity.document", Map.of(
			"identification", identification,
			"rel", "CNPJ"
		)));

		JsonObject consentRequestBody = consentRequestObjBuilder.build();

		addEditedFieldsToTheEnv(env);

		env.putObject("consent_endpoint_request", consentRequestBody);
		logSuccess("Patch request body for recurring payments consents created", args("body", consentRequestBody));

		return env;
	}

	protected JsonObject getRiskSignals() {
		return new JsonObjectBuilder()
			.addField("deviceId", "00000000-54b3-e7c7-0000-000046bffd97")
			.addField("isRootedDevice", false)
			.addField("screenBrightness", 0)
			.addField("elapsedTimeSinceBoot", 0)
			.addField("osVersion", "string")
			.addField("userTimeZoneOffset", "string")
			.addField("language", "string")
			.addField("accountTenure", "2024-12-17")
			.addFields("screenDimensions", Map.of(
				"height", 0,
				"width", 0
			)).build();
	}

	protected JsonArray getCreditors() {
		JsonArray creditors = new JsonArray();
		JsonObject creditor = new JsonObjectBuilder().addField("name", getName()).build();
		creditors.add(creditor);
		return creditors;
	}

	protected String getName() {
		return "Openflix";
	}

	protected void addEditedFieldsToTheEnv(Environment env) {
		if (getMaxVariableAmount() != null) {
			env.putString("consent_edition_amount", getMaxVariableAmount());
		}
		if (getExpirationDateTime() != null) {
			env.putString("consent_edition_expiration", getExpirationDateTime());
		}
	}

	protected abstract String getExpirationDateTime();
	protected abstract String getMaxVariableAmount();
}
