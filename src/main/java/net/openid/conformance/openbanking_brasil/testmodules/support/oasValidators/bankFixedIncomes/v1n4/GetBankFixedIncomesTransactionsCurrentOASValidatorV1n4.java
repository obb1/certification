package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4;

public class GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4 extends AbstractBankFixedIncomesTransactionsOASValidatorV1n4 {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions";
	}

}
