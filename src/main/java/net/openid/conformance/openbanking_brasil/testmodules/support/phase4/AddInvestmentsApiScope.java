package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;
import net.openid.conformance.testmodule.Environment;

public class AddInvestmentsApiScope extends AbstractScopeAddingCondition {

    private String api;

    @Override
    protected String newScope() {
        return api;
    }

    @Override
    @PreEnvironment(strings = "investment-api")
    public Environment evaluate(Environment env) {
        api = env.getString("investment-api");
        return super.evaluate(env);
    }
}
