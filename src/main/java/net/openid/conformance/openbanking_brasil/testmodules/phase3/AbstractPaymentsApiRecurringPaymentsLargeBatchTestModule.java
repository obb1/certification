package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule60DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;

public abstract class AbstractPaymentsApiRecurringPaymentsLargeBatchTestModule extends AbstractPaymentApiSchedulingTestModule{

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule60DailyPaymentsStarting1DayInTheFuture();
	}

	@Override
	protected void configureDictInfo() {
		// Not needed in this test
	}
	@Override
	protected void runRepeatSequence() {
		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(60)
			.times(30)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();
	}
}
