package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum RecurringPaymentsConsentsRevokedByEnum {

	INICIADORA, USUARIO, DETENTORA;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
