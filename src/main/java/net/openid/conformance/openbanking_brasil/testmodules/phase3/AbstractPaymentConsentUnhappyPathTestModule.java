package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractPaymentConsentUnhappyPathTestModule extends AbstractPaymentUnhappyPathTestModule {

    protected abstract Class<? extends Condition> paymentConsentErrorValidator();

    @Override
    protected void performPreAuthorizationSteps() {
        OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
            .insertAfter(OptionallyAllow201Or422.class,
                condition(paymentConsentErrorValidator())
                    .dontStopOnFailure()
                    .onFail(Condition.ConditionResult.FAILURE)
                    .skipIfStringMissing("validate_errors")
            )
            .insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
                sequenceOf(condition(CreateRandomFAPIInteractionId.class),
                    condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
            );
        call(steps);
        callAndStopOnFailure(SaveAccessToken.class);
        callAndStopOnFailure(CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida.class,Condition.ConditionResult.FAILURE);
        if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
            fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
        }
        int status = Optional.ofNullable(env.getInteger("consent_endpoint_response_full", "status"))
            .orElseThrow(() -> new TestFailureException(getId(), "Could not find consent_endpoint_response_full"));
        if (status == HttpStatus.SC_CREATED) {
            callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
        } else {
            callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
            env.mapKey(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
            callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class, Condition.ConditionResult.FAILURE);
            fireTestFinished();
        }
    }
}
