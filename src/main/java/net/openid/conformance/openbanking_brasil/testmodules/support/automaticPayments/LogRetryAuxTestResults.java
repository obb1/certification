package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonArray;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LogRetryAuxTestResults extends AbstractCondition {

	@Override
	@PreEnvironment(strings = { "consent_id", "refresh_token" }, required = "recurringPaymentIds")
	public Environment evaluate(Environment env) {
		JsonArray recurringPaymentIdsArray = env
			.getElementFromObject("recurringPaymentIds", "recurringPaymentIdsArray")
			.getAsJsonArray();
		String recurringConsentId = env.getString("consent_id");
		String refreshToken = env.getString("refresh_token");

		logSuccess("After running the test, the following data has been generated", args(
			"recurringConsentId", recurringConsentId,
			"recurringPaymentIds", recurringPaymentIdsArray,
			"refresh token", refreshToken
		));

		return env;
	}
}
