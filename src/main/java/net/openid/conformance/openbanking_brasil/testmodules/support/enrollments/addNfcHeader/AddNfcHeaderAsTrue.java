package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.addNfcHeader;

public class AddNfcHeaderAsTrue extends AbstractAddNfcHeader {

	@Override
	protected boolean nfcHeaderValue() {
		return true;
	}
}
