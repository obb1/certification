package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckAllTransactionsDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckTransactionDateRange;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToCreditFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromAndToTransactionDateToSavedTransactionDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo180DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo360DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateTo180DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractCreditFixedIncomesApiTransactionDateTest extends AbstractInvestmentsApiTransactionDateTestModule {

    @Override
    protected AbstractSetInvestmentApi setInvestmentsApi() {
        return new SetInvestmentApiToCreditFixedIncomes();
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo180DaysAgo() {
        return SetFromTransactionDateTo180DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionToToday() {
        return SetToTransactionDateToToday.class;
    }

    @Override
    protected Class<? extends Condition> setFromTransactionTo360DaysAgo() {
        return SetFromTransactionDateTo360DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> setToTransactionTo180DaysAgo() {
        return SetToTransactionDateTo180DaysAgo.class;
    }

    @Override
    protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
        return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
    }

    @Override
    protected Class<? extends Condition> setFromAndToTransactionToSavedTransaction() {
        return SetFromAndToTransactionDateToSavedTransactionDate.class;
    }

    @Override
    protected Class<? extends Condition> checkTransactionRange() {
        return CheckTransactionDateRange.class;
    }

    @Override
    protected Class<? extends Condition> checkAllDates() {
        return CheckAllTransactionsDates.class;
    }

    @Override
    protected String transactionParameterName() {
        return "TransactionDate";
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.CREDIT_FIXED_INCOMES;
	}
}
