package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePatchPaymentsRequestFromConsentRequestV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancellationReason.EnsureCancellationReasonWasCanceladoAgendamento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureCancelledFrom.EnsureCancelledFromWasIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasCanc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.CreatePatchConsentsRequestPayload;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.LoadPaymentIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.PrepareToFetchNextPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SavePaymentIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureTimeIsAfterRequestDateTime.EnsureStatusUpdateDateTimeIsAfterRequestTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule5DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchPixPaymentsEndpointSequenceV2;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentsApiRecurringPaymentsPatchTestModule extends AbstractPaymentApiSchedulingTestModule {

	private boolean isFirstValidation = true;
	private Class<? extends Condition> ensurePaymentStatusCondition = EnsurePaymentStatusWasSchd.class;

	protected abstract Class<? extends Condition> patchPaymentConsentValidator();
	protected abstract Class<? extends Condition> patchPaymentValidator();

	@Override
	protected void configureDictInfo() {}

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule5DailyPaymentsStarting1DayInTheFuture();
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();
		executePostPollingSteps();
	}

	@Override
	protected void validateAllPayments() {
		if (isFirstValidation) {
			callAndStopOnFailure(SavePaymentIds.class);
		}
		super.validateAllPayments();
	}

	@Override
	protected ConditionSequence getPaymentsValidationRepeatSequence() {
		return super.getPaymentsValidationRepeatSequence().replace(EnsurePaymentStatusWasSchd.class, condition(ensurePaymentStatusCondition));
	}

	protected void executePostPollingSteps() {
		runInBlock(currentClientString() + "Call PATCH payments endpoint", () -> {
			isFirstValidation = false;
			callAndStopOnFailure(LoadPaymentIds.class);
			call(getCreatePatchRequestSequence());
			call(getCallPatchConditionSequence());
		});

		runInBlock(currentClientString() + "Validate patched payment", () -> {
			call(getPatchValidationSequence());
		});

		runInBlock(currentClientString() + "Call GET payments endpoint for the 4 payments left expecting SCHD", super::validateAllPayments);

		runInBlock(currentClientString() + "Call PATCH consents endpoint", () -> {
			callAndStopOnFailure(LoadPaymentIds.class);
			call(getCreateConsentsPatchRequestSequence());
			call(getCallPatchConsentsConditionSequence());
		});

		runInBlock(currentClientString() + "Validate patched consent", () -> {
			call(getPatchConsentsValidationSequence());
		});

		ensurePaymentStatusCondition = EnsurePaymentStatusWasCanc.class;
		runInBlock(currentClientString() + "Call GET payments endpoint for the 5 payments expecting CANC", super::validateAllPayments);
	}

	protected ConditionSequence getCreatePatchRequestSequence(){
		return sequenceOf(
			condition(CreatePatchPaymentsRequestFromConsentRequestV2.class)
		);
	}

	protected ConditionSequence getCallPatchConditionSequence() {
		return new CallPatchPixPaymentsEndpointSequenceV2()
			.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(PrepareToFetchNextPayment.class));
	}

	protected ConditionSequence getPatchValidationSequence() {
		return sequenceOf(
			condition(patchPaymentValidator()).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(EnsurePaymentStatusWasCanc.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(EnsureCancellationReasonWasCanceladoAgendamento.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(EnsureCancelledFromWasIniciadora.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)
		);
	}

	protected ConditionSequence getCreateConsentsPatchRequestSequence() {
		return sequenceOf(
			condition(CreatePatchConsentsRequestPayload.class)
		);
	}

	protected ConditionSequence getCallPatchConsentsConditionSequence() {
		return new CallPatchAutomaticPaymentsConsentsEndpointSequence()
			.skip(
				CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class,
				"The patch payload was already created"
			);
	}

	protected ConditionSequence getPatchConsentsValidationSequence() {
		return sequenceOf(
			condition(EnsureConsentResponseCodeWas200.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(patchPaymentConsentValidator()).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"),
			condition(EnsureStatusUpdateDateTimeIsAfterRequestTime.class).dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			exec().unmapKey("resource_endpoint_response_full")
		);
	}

	@Override
	protected void validateSelfLink(String responseFull) {
		String httpMethod = env.getString("http_method");

		if (httpMethod.equals("PATCH") && responseFull.contains("consent")) {
			eventLog.log("Validate Consent Self link", "Validate self link skipped for link.self returned in the response");
			return;
		}
		super.validateSelfLink(responseFull);
	}
}
