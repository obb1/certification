package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsWrongAmountTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_recurring-payments-wrong-amount_test-module_v4",
	displayName = "payments_api_recurring-payments-wrong-amount_test-module_v4",
	summary = "Ensure that consent for monthly recurring payments can be carried out and that the payment is not successfully scheduled when the amount from one of the payments does not match the consent.\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+1, and schedule.daily.quantity as 5\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"• Calls the POST Payments Endpoint with the 5 Payments, using the appropriate wth the e2eid but onf of the payments with a different amount\n" +
		"• Expects 201 or 422 - PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"If 201 is returned:\n" +
		"\u3000• Poll the Get Payments endpoint with one of the PaymentID Created while payment status is RCVD\n" +
		"\u3000• Expects 200 with a RJCT status, and rejectionReason as PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"CONSUMED\" and validate response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiRecurringPaymentsWrongAmountTestModuleV4 extends AbstractPaymentsApiRecurringPaymentsWrongAmountTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

		@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

}
