package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.time.Instant;
import java.util.Optional;

public class EnsureUpdatedAtDateTimeFieldWasUpdated extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY, strings = "saved_instant")
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		JsonObject data = response.getAsJsonObject("data");
		String updatedAtDateTimeString = Optional.ofNullable(data.get("updatedAtDateTime"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not extract updatedAtDateTime field from response data", args("data", data)));
		String savedInstantString = env.getString("saved_instant");

		Instant updatedAtDateTime = Instant.parse(updatedAtDateTimeString);
		Instant savedInstant = Instant.parse(savedInstantString);

		if (updatedAtDateTime.isBefore(savedInstant)) {
			throw error("The current value for updatedAtDateTime should be equal or in the future in relation to the instant before the call to the API",
				args("previous", savedInstantString, "current", updatedAtDateTimeString));
		}

		logSuccess("The field updatedAtDateTime has been updated, as expected");

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}
}
