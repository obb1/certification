package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;

public class CheckIfLinksIsPresent extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			JsonObject body = extractResponseBody(env);
			boolean isLinksPresent = checkLinksPresence(body);
			env.putBoolean("is_links_present", isLinksPresent);
			logSuccess("\"links\" presence has been checked", args("body", body));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	private JsonObject extractResponseBody(Environment env) throws ParseException {
		return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Could not extract body from response"));
	}

	private boolean checkLinksPresence(JsonObject body) {
		JsonElement links = body.get("links");
		return links != null;
	}
}
