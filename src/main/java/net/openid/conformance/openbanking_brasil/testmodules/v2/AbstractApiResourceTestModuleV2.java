package net.openid.conformance.openbanking_brasil.testmodules.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.CompareResourceIdWithAPIResourceId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractAllSpecifiedApiIdsMultiplePages;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatusMultiplePages;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PollApiRootToExtractEveryIdFromEveryPage;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ResourceApiV2PollingSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
    "client.org_jwks", "resource.consentUrl"
})
public abstract class AbstractApiResourceTestModuleV2 extends AbstractPhase2V2TestModule {

    private static final String RESOURCE_STATUS = EnumResourcesStatus.AVAILABLE.name();

    protected abstract Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint();
    protected abstract Class<? extends AbstractGetXFromAuthServer> getApiEndpoint();
    protected abstract OPFScopesEnum getScope();
    protected abstract OPFCategoryEnum getProductCategoryEnum();
    protected abstract Class<? extends Condition> apiValidator();
    protected abstract String apiName();
    protected abstract String apiResourceId();
    protected abstract String resourceType();

	protected String getVersion(){
		return "2";
	}

	protected abstract Class<? extends Condition> getResourceValidator();

    @Override
    protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(getConsentEndpoint()),
			condition(getApiEndpoint())
		)));
        super.configureClient();
    }

	@Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env, eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.OPEN_ID, OPFScopesEnum.RESOURCES,getScope());
        scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(getProductCategoryEnum()).build();
    }

    @Override
    protected void validateResponse() {
        runInBlock(String.format("Validate %s root response v%s", apiName(), getVersion()),
			() -> callAndStopOnFailure(apiValidator()));

        call(exec().startBlock("Setting page-size=1000 and calling the " + apiName() + " endpoint again"));
        env.putString("apiIdName", apiResourceId());
        PollApiRootToExtractEveryIdFromEveryPage apiPollingSteps = new PollApiRootToExtractEveryIdFromEveryPage(
            env, getId(), eventLog, testInfo, getTestExecutionManager(), ExtractAllSpecifiedApiIdsMultiplePages.class
        );
        call(apiPollingSteps);

        runInBlock(String.format("Validate %s root response v%s", apiName(), getVersion()),
			() -> callAndStopOnFailure(apiValidator()));

		callAndStopOnFailure(GetResourcesV3Endpoint.class);
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);

        ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
            eventLog,testInfo, getTestExecutionManager());
        runInBlock("Polling Resources API", () -> {
            call(pollingSteps);
        });

        call(exec().startBlock("Poll has ended, setting page-size=1000 and calling the resource again"));
        env.putString("resource_type", resourceType());
        env.putString("resource_status", RESOURCE_STATUS);
        PollApiRootToExtractEveryIdFromEveryPage resourcesPollingSteps = new PollApiRootToExtractEveryIdFromEveryPage(
            env, getId(), eventLog, testInfo, getTestExecutionManager(), ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatusMultiplePages.class
        );
        call(resourcesPollingSteps);

        runInBlock("Validate Resources response V3", () -> call(getResourceValidationSequence()));

        eventLog.startBlock("Compare active resourceId's with API resources");
        callAndStopOnFailure(CompareResourceIdWithAPIResourceId.class);
        eventLog.endBlock();
    }

	protected ConditionSequence getResourceValidationSequence(){
		return sequenceOf(
			condition(getResourceValidator())
		);
	}
}
