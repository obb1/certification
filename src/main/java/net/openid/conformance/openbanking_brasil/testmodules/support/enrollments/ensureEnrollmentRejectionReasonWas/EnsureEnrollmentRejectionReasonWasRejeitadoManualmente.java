package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRejectionReasonEnum;

public class EnsureEnrollmentRejectionReasonWasRejeitadoManualmente extends AbstractEnsureEnrollmentRejectionReasonWas {

    @Override
    protected String getExpectedRejectionReason() {
        return EnrollmentRejectionReasonEnum.REJEITADO_MANUALMENTE.toString();
    }
}
