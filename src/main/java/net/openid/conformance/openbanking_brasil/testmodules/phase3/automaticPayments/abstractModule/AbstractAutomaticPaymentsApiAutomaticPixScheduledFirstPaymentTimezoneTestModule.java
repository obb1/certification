package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanal;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckCurrentTime;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixScheduledFirstPaymentTimezoneTestModule extends AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule {

	@Override
	protected boolean isHappyPath() {
		return true;
	}

	@Override
	protected AbstractEnsurePaymentStatusWasX finalPaymentStatusCondition() {
		return new EnsurePaymentStatusWasAcsc();
	}

	@Override
	protected AbstractEnsurePaymentConsentStatusWas finalConsentStatusCondition() {
		return new EnsurePaymentConsentStatusWasAuthorised();
	}

	@Override
	protected String finalConsentStatus() {
		return "AUTHORISED";
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorResponseCodeFieldCondition() {
		return null;
	}

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason ensureRejectionReasonCondition() {
		return null;
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanal();
	}

	@Override
	protected void validateResponse() {
		super.validateResponse();
		callAndStopOnFailure(CheckCurrentTime.class);
	}
}
