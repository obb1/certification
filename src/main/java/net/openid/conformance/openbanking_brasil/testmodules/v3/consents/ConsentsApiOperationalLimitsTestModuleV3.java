package net.openid.conformance.openbanking_brasil.testmodules.v3.consents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.AbstractConsentsApiOperationalLimitsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "consents_api_operational-limits_test-module_v3",
	displayName = "Consents Api operational limits test module V3",
	summary = "This test will make sure that the server will not limit access to Consents API V3 regardless of the number of calls done against it.\n\n" +
		"\u2022 Create a consent using the CPF and CNPJ provided for the Operational Limits tests. Send the permissions for either customer business or customer personal data, based on what has been provided on the test configuration\n" +
		"\u2022 Redirect the user to authorise the Consent with the customer and the created consent scopes- Expect a success on the redirect\n" +
		"\u2022 Call the GET Consents API 600 Times using the Authorized ConsentID\n" +
		"\u2022 Expect every single call to return a 600 times - Expect a 200 - response_body should only be validated on first API Call\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness"
	}
)
public class ConsentsApiOperationalLimitsTestModuleV3 extends AbstractConsentsApiOperationalLimitsTestModule {

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetConsentOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostConsentOASValidatorV3.class;
	}
}
