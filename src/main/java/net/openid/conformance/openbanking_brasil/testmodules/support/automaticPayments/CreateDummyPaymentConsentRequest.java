package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreateDummyPaymentConsentRequest extends AbstractCondition {

	@Override
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {
		env.putObject("consent_endpoint_request", new JsonObject());
		return env;
	}
}
