package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Map;

public class AddRecurringPaymentResourceUrlToConfig extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String consentUrl = OIDFJSON.getString(env.getElementFromObject("config", "resource.consentUrl"));
		String resourceUrl = consentUrl.replace("/recurring-consents", "/pix/recurring-payments");
		env.putString("config", "resource.resourceUrl", resourceUrl);
		logSuccess("resourceUrl was added to the config", Map.of("resourceUrl", resourceUrl));
		return env;
	}
}
