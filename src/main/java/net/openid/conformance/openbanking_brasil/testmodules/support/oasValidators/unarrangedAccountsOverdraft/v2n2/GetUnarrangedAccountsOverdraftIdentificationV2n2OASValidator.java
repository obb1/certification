package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.AbstractGetUnarrangedAccountsOverdraftIdentificationOASValidator;


public class GetUnarrangedAccountsOverdraftIdentificationV2n2OASValidator extends AbstractGetUnarrangedAccountsOverdraftIdentificationOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/unarrangedAccountsOverdraft/unarranged-accounts-overdraft-v2.2.0.yml";
	}
}
