package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public abstract class AbstractPaymentLocalInstrumentCondtion extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public final Environment evaluate(Environment env) {
		log("Setting local instrument to a new value");
		JsonObject resourceObj = env.getObject("resource");
		JsonObject paymentConsent = getPaymentConsentObject(resourceObj);

		Optional<JsonObject> optionalPaymentConsent = Optional.ofNullable(paymentConsent);

		JsonObject details = optionalPaymentConsent
			.map(obj -> obj.getAsJsonObject("data"))
			.map(obj -> obj.getAsJsonObject("payment"))
			.map(obj -> obj.getAsJsonObject("details"))
			.orElseThrow(() -> error("Configuration is malformed, missing data.payment.details"));

		details.addProperty("localInstrument", getLocalInstrument());
		log(details);
		return env;
	}

	protected JsonObject getPaymentConsentObject(JsonObject resourceConfig) {
		return resourceConfig.getAsJsonObject("brazilPaymentConsent");
	}

	protected abstract String getLocalInstrument();

}
