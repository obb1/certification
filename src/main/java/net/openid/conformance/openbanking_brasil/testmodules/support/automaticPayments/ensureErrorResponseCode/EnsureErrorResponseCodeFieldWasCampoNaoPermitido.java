package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.CreateAutomaticPaymentsErrorEnumV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasCampoNaoPermitido extends AbstractEnsureErrorResponseCodeFieldWas {

	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreateAutomaticPaymentsErrorEnumV1.CAMPO_NAO_PERMITIDO.toString());
	}
}
