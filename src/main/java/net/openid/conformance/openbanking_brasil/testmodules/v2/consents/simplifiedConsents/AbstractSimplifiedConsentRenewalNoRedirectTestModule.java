package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateConsent422EstadoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPostConsentExtensionsExpectingErrorSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractSimplifiedConsentRenewalNoRedirectTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {
	@Override
	protected void runTests() {
		call(new ValidateWellKnownUriSteps());
		callGetConsentAndOrResourceUrlSequence();
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS).build();
		call(createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence));
		postConsentRequest();
		callGetConsentEndpoint();
	}
	protected abstract void callGetConsentAndOrResourceUrlSequence();
	public void postConsentRequest(){
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(buildConsentRequestBody());
		callAndStopOnFailure(setConsentExpirationTime());
		callAndStopOnFailure(CallConsentEndpointWithBearerToken.class);
		call(exec().mapKey("endpoint_response", "consent_endpoint_response_full"));
		callAndStopOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
		call(exec().unmapKey("endpoint_response"));
		call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
		callAndStopOnFailure(validatePostConsentResponse());
		call(exec().unmapKey("resource_endpoint_response_full"));
		callAndStopOnFailure(ExtractConsentIdFromConsentEndpointResponse.class);
		callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
		callAndStopOnFailure(FAPIBrazilAddConsentIdToClientScope.class);
	}
	protected abstract Class<? extends Condition> validatePostConsentResponse();


	//Call GET consent endpoint
	public void callGetConsentEndpoint() {
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
		callAndStopOnFailure(validateGetConsentEndpointResponse());
		callAndContinueOnFailure(EnsureConsentWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
		call(exec().unmapKey("resource_endpoint_response_full"));
	}

	protected abstract Class<? extends Condition> validateExpirationTimeReturned();
	//Validate GET consent endpoint response and validate extension time in response
	protected abstract Class<? extends Condition> validateGetConsentEndpointResponse();
	//Build consent request body
	protected abstract Class<? extends Condition> buildConsentRequestBody();

	//Set consent expiration time
	protected abstract Class<? extends Condition> setConsentExpirationTime();

	protected abstract Class<? extends Condition> setExtensionExpirationTime();

	public ConditionSequence getPostConsentExtensionExpectingErrorSequence(Class<? extends Condition> errorCondition, Class<? extends Condition> errorCodeMessageCondition, Class<? extends Condition> expiryTimeCondition) {
		ConditionSequence callPostConsentExtensionsExpectingErrorSequence = new CallPostConsentExtensionsExpectingErrorSequence();
		callPostConsentExtensionsExpectingErrorSequence.replace(CreateIndefiniteConsentExpiryTime.class, condition(expiryTimeCondition).onFail(Condition.ConditionResult.FAILURE));
		callPostConsentExtensionsExpectingErrorSequence.replace(EnsureConsentResponseCodeWas422.class, condition(errorCondition).onFail(Condition.ConditionResult.FAILURE));
		if (errorCodeMessageCondition == null) {
			callPostConsentExtensionsExpectingErrorSequence.skip(ValidateConsent422EstadoInvalido.class, "No error code to validate");
		} else {
			callPostConsentExtensionsExpectingErrorSequence.replace(ValidateConsent422EstadoInvalido.class, condition(errorCodeMessageCondition).onFail(Condition.ConditionResult.FAILURE));
		}
		return callPostConsentExtensionsExpectingErrorSequence;
	}
}
