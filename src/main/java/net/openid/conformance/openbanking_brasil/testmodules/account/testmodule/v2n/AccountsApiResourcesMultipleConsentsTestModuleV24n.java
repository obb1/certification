package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule.AbstractAccountsApiResourcesMultipleConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsBalancesOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "accounts_api_multiple-consents_test-module_v2-4",
	displayName = "Validates that the server has correctly implemented the rules set for joint accounts that require multiple consents for data to be shared.",
	summary =
		"This test module should be executed only by instituions that currently support consents with multiple accounts, in case the instituion does not support this feature the 'Joint Account brazilCpf' and 'Joint Account brazilCnpj' fields should be left empty, which in turn will make the test return a Warning\n" +
			"This test module alidates that the server has correctly implemented the rules set for joint accounts that require multiple consents for data to be shared\n" +
			"\u2022 Create a CONSENT with only ACCOUNTS_READ, ACCOUNTS_BALANCES_READ and RESOURCES_READ Permissions using the CPF and CNPJ provided for joint accounts\n" +
			"\u2022 Expect a Success 201\n" +
			"\u2022 Redirect the user to authorize the Consent - Redirect URI must contain accounts, resources and consents scopes\n" +
			"\u2022 Expect a Successful authorization with an authorization code created\n" +
			"\u2022 Call the RESOURCES API with the authorized consent\n" +
			"\u2022 Expect a 200 - Validate that one Account Resource has been returned and it is on the state AWAITING_AUTHORIZATION\n" +
			"\u2022 Call the ACCOUNTS API\n" +
			"\u2022 Expect a 200 - Make sure the Server returns a 200 with an empty list on the object\n" +
			"\u2022 Call the ACCOUNTS BALANCES API with the Account ID of the Account on AWAITING_AUTHORIZATION\n" +
			"\u2022 Expect a 403 - Validate that the field response.errors.code is STATUS_RESOURCE_AWAITING_AUTHORIZATION\n" +
			"\u2022 POLL the GET RESOURCES API for 5 minutes, one call every 30 seconds.\n" +
			"\u2022 Continue Polling until the Account Resource returned is on the status AVAILABLE\n" +
			"\u2022 Call the ACCOUNTS API\n" +
			"\u2022 Expect a 200 - Make sure the Account Resource is now returned on the API response\n",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class AccountsApiResourcesMultipleConsentsTestModuleV24n extends AbstractAccountsApiResourcesMultipleConsentsTestModule {

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountListValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends OpenAPIJsonSchemaValidator> getAccountBalancesValidator() {
		return GetAccountsBalancesOASValidatorV2n4.class;
	}
}
