package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1.GetVariableIncomesListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToVariableIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "variable-incomes_api_resources_test-module",
    displayName = "variable-incomes_api_resources_test-module",
    summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
        "• Call the POST Consents endpoint with the Variable Incomes API Permission Group\n" +
        "• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Resources API\n" +
        "• Expect a 200 success - Extract all of the AVAILABLE resources that have been returned with the tested investment API type - VARIABLE_INCOMES - Validate Response Body\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expect a 200 - Validate Response - Make sure that all the fields that were set as AVAILABLE on the Resources API are returned on the Investments List Endpoint\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf"
    }
)
public class VariableIncomesApiResourcesTestModule extends AbstractInvestmentsApiResourcesTestModule {

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return GetResourcesOASValidatorV3.class;
	}

    @Override
    protected AbstractSetInvestmentApi investmentApi() {
        return new SetInvestmentApiToVariableIncomes();
    }

    @Override
    protected Class<? extends Condition> apiValidator() {
        return GetVariableIncomesListV1OASValidator.class;
    }

    @Override
    protected String resourceType() {
        return EnumResourcesType.VARIABLE_INCOME.name();
    }
	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.VARIABLE_INCOMES;
	}
	@Override
	protected OPFCategoryEnum getProductCategoryEnum() {
		return OPFCategoryEnum.INVESTMENTS;
	}
}
