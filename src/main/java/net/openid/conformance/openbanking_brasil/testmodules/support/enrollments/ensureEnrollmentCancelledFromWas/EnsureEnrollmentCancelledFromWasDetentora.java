package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentCanceledFromEnum;

public class EnsureEnrollmentCancelledFromWasDetentora extends AbstractEnsureEnrollmentCancelledFromWas {

    @Override
    protected String getExpectedCancelledFromType() {
        return EnrollmentCanceledFromEnum.DETENTORA.toString();
    }
}
