package net.openid.conformance.openbanking_brasil.testmodules.logging;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.IndexOptions;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.logging.GsonArrayToBsonArrayConverter;
import net.openid.conformance.logging.GsonObjectToBsonDocumentConverter;
import net.openid.conformance.logging.JsonObjectSanitiser;
import net.openid.conformance.logging.MapSanitiser;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static net.openid.conformance.logging.MapCopy.deepCopy;

public class YACSDBExpireLogs implements EventLog {
	public static final String COLLECTION = "EVENT_LOG";

	@Autowired
	private MongoTemplate mongoTemplate;

	private final JsonObjectSanitiser jsonObjectSanitiser;
	private final MapSanitiser mapSanitiser;

	public YACSDBExpireLogs(JsonObjectSanitiser jsonObjectSanitiser, MapSanitiser mapSanitiser) {
		this.jsonObjectSanitiser = jsonObjectSanitiser;
		this.mapSanitiser = mapSanitiser;
	}

	@Override
	public void log(String testId, String source, Map<String, String> owner, String msg) {
		Document document = new Document()
			.append("_id", testId + "-" + RandomStringUtils.randomAlphanumeric(32))
			.append("testId", testId)
			.append("src", source)
			.append("testOwner", owner)
			.append("time", new Date().getTime())
			.append("msg", msg)
			.append("createdOn",new Date());


		mongoTemplate.insert(document, COLLECTION);
	}

	@Override
	public void log(String testId, String source, Map<String, String> owner, JsonObject obj) {
		JsonObject ret = new JsonParser().parse(obj.toString()).getAsJsonObject();
		jsonObjectSanitiser.sanitise(source, ret);
		Document dbObject = Document.parse(GsonObjectToBsonDocumentConverter.convertFieldsToStructure(ret).toString());
		dbObject.append("_id", testId + "-" + RandomStringUtils.randomAlphanumeric(32));
		dbObject.append("testId", testId);
		dbObject.append("src", source);
		dbObject.append("testOwner", owner);
		dbObject.append("time", new Date().getTime());
		dbObject.append("createdOn",new Date());

		mongoTemplate.insert(dbObject, COLLECTION);
	}

	@Override
	public void log(String testId, String source, Map<String, String> owner, Map<String, Object> map) {
		map = deepCopy(map);
		mapSanitiser.sanitise(source, map);
		BasicDBObjectBuilder documentBuilder = BasicDBObjectBuilder.start(GsonArrayToBsonArrayConverter.convertUnloggableValuesInMap(map))
			.add("_id", testId + "-" + RandomStringUtils.randomAlphanumeric(32))
			.add("testId", testId)
			.add("src", source)
			.add("testOwner", owner)
			.add("time", new Date().getTime())
			.add("createdOn",new Date());

		mongoTemplate.insert(documentBuilder.get(), COLLECTION);
	}

	@Override
	public void createIndexes(){
		MongoCollection<Document> eventLogCollection = mongoTemplate.getCollection(COLLECTION);
		eventLogCollection.createIndex(new BasicDBObject("createdOn", 1), new IndexOptions().expireAfter(15L, TimeUnit.MINUTES));
		eventLogCollection.createIndex(new Document("testId", 1));
		eventLogCollection.createIndex(new Document("testOwner", 1));
	}
}
