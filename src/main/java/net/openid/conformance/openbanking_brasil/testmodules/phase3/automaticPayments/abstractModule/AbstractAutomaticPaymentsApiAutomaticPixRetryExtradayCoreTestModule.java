package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs200;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixRetryPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.AbstractEditRecurringPaymentsBodyToSetDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToOneDayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.AbstractEnsureIsRetryAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureIsRetryAccepted.EnsureIsRetryAcceptedTrue;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.AbstractEnsurePaymentListDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates.EnsureTwoPaymentsForTodayOrBefore;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreTwoRjctPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.AbstractSetConfigurationFieldsForRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest.SetConfigurationFieldsForExtradayRetryAcceptedRetryTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPatchRecurringPaymentEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixRetryExtradayCoreTestModule extends AbstractAutomaticPaymentsAutomaticPixRetryPathTestModule {

	protected abstract Class<? extends Condition> patchPaymentValidator();

	@Override
	protected void postAndValidateRecurringPayment() {
		super.postAndValidateRecurringPayment();
		patchAndValidateRecurringPayment();
	}

	protected void patchAndValidateRecurringPayment() {
		useClientCredentialsAccessToken();
		runInBlock("Call PATCH recurring-payment endpoint - Expect 422 PAGAMENTO_NAO_PERMITE_CANCELAMENTO", () -> {
			call(new CallPatchRecurringPaymentEndpointSequence()
				.replace(EnsureHttpStatusCodeIs200.class, condition(EnsureResourceResponseCodeWas422.class)));
			callAndContinueOnFailure(patchPaymentValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndStopOnFailure(EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento.class);
			env.unmapKey(EnsureErrorResponseCodeFieldWasPagamentoNaoPermiteCancelamento.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void configurePaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToOneDayInTheFuture.class);
		callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class));
	}

	@Override
	protected void validateResponse() {
		call(postPaymentValidationSequence());
		runRepeatSequence();
		validateFinalState();
	}

	@Override
	protected void validateFinalState() {
		callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected AbstractSetConfigurationFieldsForRetryTest getSetConfigurationFieldsForRetryTestCondition() {
		return new SetConfigurationFieldsForExtradayRetryAcceptedRetryTest();
	}

	@Override
	protected AbstractEnsureIsRetryAccepted getEnsureIsRetryAcceptedCondition() {
		return new EnsureIsRetryAcceptedTrue();
	}

	@Override
	protected AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition() {
		return new EnsureThereAreTwoRjctPayments();
	}

	@Override
	protected AbstractEnsurePaymentListDates getEnsurePaymentListDatesCondition() {
		return new EnsureTwoPaymentsForTodayOrBefore();
	}

	@Override
	protected AbstractEnsureRecurringPaymentsRejectionReason rejectionReasonCondition() {
		return null;
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorCodeCondition() {
		return null;
	}

	@Override
	protected AbstractEditRecurringPaymentsBodyToSetDate setPaymentDateCondition() {
		return new EditRecurringPaymentsBodyToSetDateToOneDayInTheFuture();
	}
}
