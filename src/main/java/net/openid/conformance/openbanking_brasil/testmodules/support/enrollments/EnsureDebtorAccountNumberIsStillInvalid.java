package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class EnsureDebtorAccountNumberIsStillInvalid extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";
	private static final String DEBTOR_ACCOUNT_NUMBER_KEY = "debtor_account_number";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY, strings = DEBTOR_ACCOUNT_NUMBER_KEY)
	public Environment evaluate(Environment env) {
		JsonObject body = extractResponseFromEnv(env);
		String debtorAccountNumber = Optional.ofNullable(body.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("debtorAccount"))
			.map(debtorAccount -> debtorAccount.get("number"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find data.debtorAccount.number inside enrollment response body",
				args("body", body)));

		String expectedNumber = env.getString(DEBTOR_ACCOUNT_NUMBER_KEY);

		if (!debtorAccountNumber.equals(expectedNumber)) {
			throw error("The debtor account number in the enrollments response is not equals to the original invalid number sent at the request",
				args("expected number", expectedNumber, "number received", debtorAccountNumber));
		}
		logSuccess("The debtor account number in the enrollments response is equals to the original invalid number sent at the request");

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}
}
