package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.JsonHelper;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractEnsureNumberOfTotalRecordsFromMeta extends AbstractEnsureNumberOfTotalRecords {

	@Override
	protected int getTotalRecords(JsonObject response)
	{
		int totalRecords;
		if (!JsonHelper.ifExists(response, "$.meta.totalRecords")) {
			throw error("The response does not contain a totalRecords element in meta.");
		}

		JsonElement metaElement = findByPath(response, "$.meta");
		totalRecords = OIDFJSON.getInt(metaElement.getAsJsonObject().get("totalRecords"));
		return totalRecords;
	}
}
