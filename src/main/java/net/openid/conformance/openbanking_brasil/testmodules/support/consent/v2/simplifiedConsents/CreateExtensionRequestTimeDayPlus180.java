package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class CreateExtensionRequestTimeDayPlus180 extends AbstractCreateConsentExtensionExpirationTime{
	@Override
	protected int getExtensionTimeInDays() {
		return 180;
	}

	@Override
	protected boolean saveConsentExtensionExpirationTime() {
		return false;
	}
}

