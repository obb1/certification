package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum AccountTypeEnumV3 {

	SLRY, CACC, SVGS, TRAN;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}


}
