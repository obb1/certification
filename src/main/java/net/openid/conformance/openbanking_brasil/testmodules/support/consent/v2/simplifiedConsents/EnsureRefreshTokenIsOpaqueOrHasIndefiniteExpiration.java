package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.common.base.Strings;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JWTUtil;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;

public class EnsureRefreshTokenIsOpaqueOrHasIndefiniteExpiration extends AbstractCondition {

	@Override
	@PreEnvironment(required = "token_endpoint_response")
	public Environment evaluate(Environment env) {
		String refreshToken = env.getString("token_endpoint_response", "refresh_token");
		if (!Strings.isNullOrEmpty(refreshToken)) {
			try {
				JWT parsedRefreshToken = JWTUtil.parseJWT(refreshToken);
				JWTClaimsSet claims = parsedRefreshToken.getJWTClaimsSet();

				Date expirationTime = claims.getExpirationTime();
				if (expirationTime == null) {
					logSuccess("Refresh token is a JWT with no 'exp' field");
					return env;
				}

				Instant thresholdDate = Instant.parse("2300-01-01T00:00:00Z");
				if (expirationTime.toInstant().isAfter(thresholdDate) || expirationTime.toInstant().equals(thresholdDate)) {
					logSuccess("Refresh token is a JWT with 'exp' field equal or superior to 01-01-2300");
					return env;
				}

				throw error("Refresh token is a JWT with 'exp' field earlier than 01-01-2300");
			} catch (ParseException e) {
				logSuccess("Refresh token is not a JWT");
				return env;
			}
		} else {
			throw error("Couldn't find refresh token");
		}
	}
}
