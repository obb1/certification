package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class EditRecurringPaymentBodyToSetUserDefinedCreditorAccount extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));

		JsonObject creditorAccount = new JsonObjectBuilder()
			.addField("ispb", extractCreditorAccountFieldFromEnvOrFail(env, "Ispb"))
			.addField("issuer", extractCreditorAccountFieldFromEnvOrFail(env, "Issuer"))
			.addField("accountType", extractCreditorAccountFieldFromEnvOrFail(env, "AccountType"))
			.addField("number", extractCreditorAccountFieldFromEnvOrFail(env, "Number"))
			.build();

		data.add("creditorAccount", creditorAccount);
		logSuccess("The creditorAccount field has been added to the recurring payment request body",
			args("request data", data));

		return env;
	}

	protected String extractCreditorAccountFieldFromEnvOrFail(Environment env, String field) {
		return Optional.ofNullable(env.getElementFromObject("resource", "creditorAccount" + field))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error(String.format("Unable to find creditorAccount%s in the resource", field)));
	}
}
