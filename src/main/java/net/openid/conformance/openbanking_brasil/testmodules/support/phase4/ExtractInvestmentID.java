package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class ExtractInvestmentID extends AbstractCondition {

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    @PostEnvironment(strings = "investmentId")
    public Environment evaluate(Environment env) {
        String bodyString = OIDFJSON.getString(Optional.ofNullable(env.getObject(RESPONSE_ENV_KEY).get("body"))
            .orElseThrow(() -> error("No body in the response")));
		JsonObject body = JsonParser.parseString(bodyString).getAsJsonObject();
		JsonArray data = Optional.ofNullable(body.get("data")).orElse(new JsonArray()).getAsJsonArray();
        if (data.isJsonNull() || data.isEmpty()) {
            throw error("data array does not have any investment");
        }
        String investmentId = OIDFJSON.getString(Optional.ofNullable(data.get(0).getAsJsonObject().get("investmentId"))
            .orElseThrow(() -> error("No investmentId")));
        env.putString("investmentId", investmentId);
        return env;
    }
}
