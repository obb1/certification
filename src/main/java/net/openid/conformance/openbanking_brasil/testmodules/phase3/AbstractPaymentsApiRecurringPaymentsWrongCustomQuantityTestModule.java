package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithTooLittleDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;

public abstract class AbstractPaymentsApiRecurringPaymentsWrongCustomQuantityTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl){
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		callAndStopOnFailure(CustomSchedulePaymentsWithTooLittleDates.class);
		super.onConfigure(config, baseUrl);
	}


	@Override
	protected void performAuthorizationFlow(){
		OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.insertAfter(OptionallyAllow201Or422.class,
				condition(paymentConsentErrorValidator())
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
					.skipIfStringMissing("validate_errors")
			)
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			);

		call(steps);
		callAndStopOnFailure(EnsureConsentResponseCodeWas422.class);
		env.mapKey(EnsureErrorResponseCodeFieldWasParametroInvalido.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class);
		fireTestFinished();
	}

	@Override
	protected void configureDictInfo() {
		// Won't be needed in this test
	}
	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		// Won't be needed in this test
		return null;
	}
	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		// Won't be needed in this test
		return null;
	}
	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		// Won't be needed in this test
		return null;
	}
	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		// Won't be needed in this test
		return null;
	}

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();


}
