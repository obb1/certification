package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_json-accept-header-jwt-returned_test-module_v4",
	displayName = "Payments Consents API test module which sends an accept header of JSON and expects a JWT",
	summary = "Ensure a JWT is returned when a JSON accept header is sent" +
		"\u2022 Calls POST Consents Endpoint \n" +
		"\u2022 Expects 201 - Validate Response \n" +
		"\u2022 Calls GET Consents Endpoint  with a JSON accept header \n" +
		"\u2022 Expects 200 or 406 \n" +
		"\u2022 If a 200 is returned, ensure it is a JWT",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV4 extends AbstractPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
