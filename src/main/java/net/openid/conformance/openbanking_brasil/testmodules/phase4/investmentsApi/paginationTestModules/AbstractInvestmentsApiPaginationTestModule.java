package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize1001;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize25;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize51;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic.AssertLinksLogicFirstPageWithMorePages;
import net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic.AssertLinksLogicSecondPageWithMorePages;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIs25FromData;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIsAtLeast25FromData;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;

public abstract class AbstractInvestmentsApiPaginationTestModule extends AbstractInvestmentsApiTestModule {

	@Override
	protected void executeParticularTestSteps() {

		runInBlock(String.format("Call GET %s endpoint with page-size equal to 1000 - Expects 200 status code and totalRecords >= 51", endpointName()), ()->
		{
			callAndStopOnFailure(urlPreparingCondition().getClass());
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
			callAndStopOnFailure(setFromTransaction());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
			preCallProtectedResource();
			callAndContinueOnFailure(validator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast25FromData.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock(String.format("Call GET %s endpoint with page-size equal to 51 - Expects 200 status code and totalRecords >= 25", endpointName()), ()->
		{
			callAndStopOnFailure(urlPreparingCondition().getClass());
			callAndStopOnFailure(SetProtectedResourceUrlPageSize51.class);
			callAndStopOnFailure(setFromTransaction());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
			preCallProtectedResource();
			callAndContinueOnFailure(validator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIsAtLeast25FromData.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock(String.format("Call GET %s endpoint with page-size equal to 1001 - Expects 422 status code", endpointName()), ()->
		{
			callAndStopOnFailure(urlPreparingCondition().getClass());
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1001.class);
			callAndStopOnFailure(setFromTransaction());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(validator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock(String.format("Call GET %s endpoint with page-size equal to 25 - Expects 200 status code, 25 transactions and valid links logic", endpointName()), ()->
		{
			callAndStopOnFailure(urlPreparingCondition().getClass());
			callAndStopOnFailure(SetProtectedResourceUrlPageSize25.class);
			callAndStopOnFailure(setFromTransaction());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
			preCallProtectedResource();
			callAndContinueOnFailure(validator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIs25FromData.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(AssertLinksLogicFirstPageWithMorePages.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock(String.format("Call the GET %s endpoint with the next link returned on the previous call - Expects 200 status code, 25 transactions and valid links logic", endpointName()), ()->
		{
			callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
			preCallProtectedResource();
			callAndContinueOnFailure(validator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureNumberOfTotalRecordsIs25FromData.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(AssertLinksLogicSecondPageWithMorePages.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected abstract String endpointName();
	protected abstract ResourceBuilder urlPreparingCondition();
	protected abstract Class<? extends Condition> setFromTransaction();
	protected abstract Class<? extends Condition> setToTransactionToToday();
	protected abstract Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl();
	protected abstract Class<? extends Condition> validator();
}
