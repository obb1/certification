package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.List;


public class PostPaymentsPixOASValidatorV4 extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/payments/payments-4.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/pix/payments";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray dataArray = body.getAsJsonArray("data");

		for (JsonElement element : dataArray) {
			if (element.isJsonObject()) {
				assertData(element.getAsJsonObject());
			}
		}
	}

	private void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"consentId",
			"authorisationFlow",
			List.of("CIBA_FLOW","FIDO_FLOW")
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"proxy",
			"localInstrument",
			List.of("INIC","DICT","QRDN","QRES")
		);
		assertField2IsNotPresentWhenField1HasValue1(
			data,
			"localInstrument",
			"MANU",
			"proxy"
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"creditorAccount.issuer",
			"creditorAccount.accountType",
			List.of("CACC","SVGS")
		);
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"debtorAccount.issuer",
			"debtorAccount.accountType",
			List.of("CACC","SVGS")
		);


	}

}
