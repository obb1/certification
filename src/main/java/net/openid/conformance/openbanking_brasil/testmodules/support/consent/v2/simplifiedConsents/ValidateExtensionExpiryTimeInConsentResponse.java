package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;


public class ValidateExtensionExpiryTimeInConsentResponse extends AbstractJsonAssertingCondition {
	@Override
	@PreEnvironment(strings = "consent_extension_expiration_time", required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject data = bodyFrom(env,"consent_endpoint_response_full").getAsJsonObject().getAsJsonObject("data");

		String expectedExpirationDateTime = env.getString("consent_extension_expiration_time");
		if (expectedExpirationDateTime.equals("")) {
			if (data.has("expirationDateTime")) {
				throw error("expirationDateTime field was not sent, so it should not be present in the response",
					args("data", data));
			}
		} else {
			String expirationDateTime = OIDFJSON.getString(data.get("expirationDateTime"));
			if (!expirationDateTime.equals(expectedExpirationDateTime)) {
				throw error("expirationDateTime returned does not match the one sent in the request",
					args("expirationDateTime sent", expectedExpirationDateTime,
						"expirationDateTime returned", expirationDateTime));
			}
		}

		logSuccess("Updated consent expirationDateTime matches expected", args("expirationDateTime", expectedExpirationDateTime));
		return env;
	}
}
