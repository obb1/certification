package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule.AbstractAutomaticPaymentsApiWebhookRejectedTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PatchRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_webhook-rejected_test-module_v2",
	displayName = "automatic-payments_api_webhook-rejected_test-module_v2",
	summary = """
		Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a consent reaches the REJECTED state
		For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;
		• Obtain a SSA from the Directory
		• Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;, where the alias is to be obtained from the field alias on the test configuration
		• Call the Registration Endpoint, also sending the field "webhook_uris":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;”]
		• Expect a 201 - Validate Response
		• Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds
		• Set the recurring consents webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v2/automatic-payments/v2/recurring-consents/{recurringConsentId}, where the alias is to be obtained from the field alias on the test configuration
		• Call the POST recurring-consents endpoint with sweeping accounts fields
		• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION
		• Call the GET recurring-consents endpoint
		• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION
		• Call the PATCH {recurringConsentId} endpoint  with status as REJECTED, rejectedBy is USUARIO, rejectedFrom is INICIADORA and reason.code is REJEITADO_USUARIO
		• Expect 200 - Validate Message
		• Call the GET recurring-consents endpoint
		• Expect 201 - Validate Response and ensure status as REJECTED, rejectedBy is USUARIO, rejectedFrom is INICIADORA and reason.code is REJEITADO_USUARIO
		• Expect an incoming message, the defined consents endpoint, which  must be mtls protected - Wait 60 seconds for the messages to be returned
		• For the webhook calls - Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within now and the start of the test
		• Call the Delete Registration Endpoint
		• Expect a 204 - No Content
		""",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName",
		"resource.webhookWaitTime",
		"directory.client_id",
		"directory.discoveryUrl"
	}
)
public class AutomaticPaymentsApiWebhookRejectedTestModuleV2 extends AbstractAutomaticPaymentsApiWebhookRejectedTestModule {

	@Override
	protected void keepOldStartDateTime() {}

	@Override
	protected ConditionSequence getUrlsFromAuthServerCondition() {
		return sequenceOf(
			condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class),
			condition(GetAutomaticPaymentPixRecurringV2Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> setPaymentConsentWebhookCreator() {
		return CreateRecurringConsentWebhookV2.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentConsentValidator() {
		return PatchRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}
}
