package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;

import java.util.List;

public abstract class AbstractRecurringPaymentPixOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/automatic-payments/swagger-automatic-payments-2.0.0-rc.1.yml";
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	protected void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(data, "recurringConsentId", "authorisationFlow", "FIDO_FLOW");
		assertField1IsRequiredWhenField2HasValue2(data, "rejectionReason", "status", "RJCT");
		assertField1IsRequiredWhenField2HasValue2(data, "cancellation", "status", "CANC");

		assertProxyRestrictions(data);
		assertTransactionIdentificationRestrictions(data);

		JsonObject creditorAccount = data.getAsJsonObject("creditorAccount");
		assertField1IsRequiredWhenField2HasValue2(creditorAccount, "issuer", "accountType", List.of("CACC", "SVGS"));

		// TODO: uncomment the lines below when swagger is fixed
//		if (data.has("debtorAccount")) {
//			JsonObject debtorAccount = data.getAsJsonObject("debtorAccount");
//			assertField1IsRequiredWhenField2HasValue2(debtorAccount, "issuer", "accountType", List.of("CACC", "SVGS"));
//		}
	}

	protected void assertProxyRestrictions(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(data, "proxy", "localInstrument", List.of("DICT", "INIC"));
		assertField2IsNotPresentWhenField1HasValue1(data, "localInstrument", "MANU", "proxy");
	}

	protected void assertTransactionIdentificationRestrictions(JsonObject data) {
		assertField2IsNotPresentWhenField1HasValue1(data, "localInstrument", "MANU", "transactionIdentification");
		assertField2IsNotPresentWhenField1HasValue1(data, "localInstrument", "DICT", "transactionIdentification");
		assertField1IsRequiredWhenField2HasValue2(data, "transactionIdentification", "localInstrument", "INIC");
	}
}
