package net.openid.conformance.openbanking_brasil.testmodules.channels.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.LogOnlyFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToGetProductsNChannelsApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.channels.v2.GetChannelsBranchesOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "channels-branches_api_structural_test-module_v2",
	displayName = "Validate structure of Channels Branches Api resources",
	summary =
		"\u2022 Make a unauthenticated request at the required Endpoint\n" +
		"\u2022 Expect 200 - Validate Response",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4
)
public class ChannelsBranchesApiTestModuleV2 extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate Channels Branches response", () -> {
			callAndStopOnFailure(PrepareToGetProductsNChannelsApi.class);
			preCallResource();
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(LogOnlyFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetChannelsBranchesOASValidatorV2.class, Condition.ConditionResult.FAILURE);
		});
	}
}
