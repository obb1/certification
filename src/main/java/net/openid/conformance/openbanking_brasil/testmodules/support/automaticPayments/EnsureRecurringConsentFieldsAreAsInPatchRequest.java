package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class EnsureRecurringConsentFieldsAreAsInPatchRequest extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		JsonObject data = response.getAsJsonObject("data");

		validateField(env, "consent_edition_amount", data, "$.recurringConfiguration.automatic.maximumVariableAmount", "maximumVariableAmount");
		validateField(env, "consent_edition_expiration", data, "$.expirationDateTime", "expirationDateTime");

		logSuccess("All fields have been updated as expected after consent edition");

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	private void validateField(Environment env, String envKey, JsonObject data, String jsonPath, String fieldName) {
		if (env.getString(envKey) != null) {
			String expectedValue = env.getString(envKey);
			String actualValue = Optional.ofNullable(data)
				.map(d -> extractJsonValue(d, jsonPath))
				.orElseThrow(() -> error(String.format("Could not extract %s from response data", fieldName), args("data", data)));

			if (!expectedValue.equals(actualValue)) {
				throw error(String.format("The field %s has not been updated after consent edition", fieldName),
					args("expected", expectedValue, "actual", actualValue));
			}
		} else {
			if (extractJsonValue(data, jsonPath) != null) {
				throw error(String.format("The field %s has been indefinitely extended, so it should not exist in the response data", fieldName),
					args("data", data));
			}
		}
	}

	private String extractJsonValue(JsonObject data, String jsonPath) {
		try {
			JsonElement value = JsonPath.read(data, jsonPath);
			return OIDFJSON.getString(value);
		} catch (PathNotFoundException exception) {
			return null;
		}
	}
}
