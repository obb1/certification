package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalFinancialRelationships;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalIdentifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.RemoveBrazilCnpjFromResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractPhase2V2TestModule;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractCustomerPersonalDataApiTestModule extends AbstractPhase2V2TestModule {

	protected abstract ConditionSequence getEndpoints();

	@Override
	protected void configureClient(){
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(getEndpoints()));
		callAndStopOnFailure(RemoveBrazilCnpjFromResource.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA).addScopes(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.OPEN_ID).build();
		callAndContinueOnFailure(PrepareToGetPersonalQualifications.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
	}

	@Override
	protected void validateResponse() {
		validatePersonalQualificationResponse();
		validatePersonalRelationsResponse();
		validatePersonalIdentificationResponse();
	}

	protected abstract Class<? extends AbstractJsonAssertingCondition> getPersonalQualificationValidator();

	protected void validatePersonalQualificationResponse() {
		runInBlock("Validating personal qualifications response", () -> {
			callAndStopOnFailure(PrepareToGetPersonalQualifications.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getPersonalQualificationValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	protected abstract Class<? extends AbstractJsonAssertingCondition> getPersonalRelationsValidator();

	protected void validatePersonalRelationsResponse() {
		runInBlock("Validating personal financial relationship response", () -> {
			callAndContinueOnFailure(PrepareToGetPersonalFinancialRelationships.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getPersonalRelationsValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	protected abstract Class<? extends AbstractJsonAssertingCondition> getPersonalIdentificationValidator();

	protected void validatePersonalIdentificationResponse() {
		runInBlock("Validating personal identifications response", () -> {
			callAndStopOnFailure(PrepareToGetPersonalIdentifications.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getPersonalIdentificationValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void requestProtectedResource() {
		validateResponse();
	}
}
