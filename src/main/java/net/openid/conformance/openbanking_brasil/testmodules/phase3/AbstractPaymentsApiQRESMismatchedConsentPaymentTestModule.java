package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectMismatchingQRCodeIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Optional;

public abstract class AbstractPaymentsApiQRESMismatchedConsentPaymentTestModule extends AbstractPaymentUnhappyPathTestModule {

    protected abstract Class<? extends Condition> paymentConsentErrorValidator();

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectQRESCodeLocalInstrument.class);
        callAndStopOnFailure(SelectQRESCodePixLocalInstrument.class);
        callAndStopOnFailure(InjectMismatchingQRCodeIntoConfig.class);
    }

    @Override
    protected void validatePaymentRejectionReasonCode() {
		callAndContinueOnFailure(EnsurePaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void validate422ErrorResponseCode() {
		env.mapKey(EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento.RESPONSE_ENV_KEY,"resource_endpoint_response_full");
		callAndStopOnFailure(EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento.class);
		env.unmapKey(EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento.RESPONSE_ENV_KEY);

	}

    @Override
    protected Class<? extends Condition> getExpectedPaymentResponseCode() {
        return EnsureResourceResponseCodeWas201Or422.class;
    }

    @Override
    protected ConditionSequence createOBBPreauthSteps() {
        return new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.insertAfter(OptionallyAllow201Or422.class,
                condition(paymentConsentErrorValidator())
                    .dontStopOnFailure()
                    .onFail(Condition.ConditionResult.FAILURE)
                    .skipIfStringMissing("validate_errors")
            )
            .insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
                sequenceOf(condition(CreateRandomFAPIInteractionId.class),
                    condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
            ).insertAfter(CheckForFAPIInteractionIdInResourceResponse.class,
                condition(EnsureMatchingFAPIInteractionId.class));
    }

	@Override
	protected void validateSelfLink(String responseFull) {
		if (env.containsObject("saved_client_credentials")) {
			eventLog.log("Loading client credentials token for GET validation", env.getObject("saved_client_credentials"));
			env.mapKey("access_token", "saved_client_credentials");
		}
		super.validateSelfLink(responseFull);
	}

    @Override
    protected void performPreAuthorizationSteps() {
        env.mapKey("access_token", "saved_client_credentials");
        call(createOBBPreauthSteps());
        callAndStopOnFailure(CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
        if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
            fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
        }
        eventLog.startBlock(currentClientString() + "Validate consents response");
        callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
        eventLog.endBlock();
    }
}
