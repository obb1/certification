package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules;

import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToBankFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;

public abstract class AbstractBankFixedIncomesApiWrongPermissionsTest extends AbstractInvestmentsApiWrongPermissionsTestModule {

    @Override
    protected AbstractSetInvestmentApi investmentApi() {
        return new SetInvestmentApiToBankFixedIncomes();
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.BANK_FIXED_INCOMES;
	}
}
