package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditConsentsAuthoriseRequestBodyToSetUserHandleAsEmptyString extends AbstractCondition {

	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Override
	@PreEnvironment(required = REQUEST_CLAIMS_KEY)
	public Environment evaluate(Environment env) {
		JsonObject response = Optional.ofNullable(env.getElementFromObject(REQUEST_CLAIMS_KEY, "data.fidoAssertion.response"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Could not find fidoAssertion response inside consents authorise request body"));

		response.addProperty("userHandle", "");
		logSuccess("The data.fidoAssertion.response.userHandle field has been set as an empty string in the consents authorise request body",
			args("fidoAssertion", response));

		return env;
	}
}
