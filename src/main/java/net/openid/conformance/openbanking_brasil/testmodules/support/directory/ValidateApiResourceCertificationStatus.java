package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.ArrayList;
import java.util.List;

public class ValidateApiResourceCertificationStatus extends AbstractCondition {

	boolean isWarning = false;
	List<String> warningMessages = new ArrayList<>();
	List<String> failureMessages = new ArrayList<>();
	boolean isFailure = false;

	@Override
	@PreEnvironment(required = "authorisation_server")
	public Environment evaluate(Environment env) {
		JsonObject authorisationServer = env.getObject("authorisation_server");
		JsonArray apiResources = authorisationServer.getAsJsonArray("ApiResources");
		if (apiResources == null || apiResources.size() == 0) {
			logSuccess("No ApiResources found, stopping validation");
			return env;
		}

		apiResources.forEach(apiResource -> {
			validateApiResourceStatus(apiResource.getAsJsonObject());
		});
		if (isWarning) {
			env.putString("warning_message", String.join(";", warningMessages));
		}
		if (isFailure) {
			throw error(String.join(";", failureMessages));
		}
		return env;
	}

	protected void validateApiResourceStatus(JsonObject apiResource){
		if (apiResource.get("CertificationStatus").isJsonNull()){
			logSuccess("CertificationStatus is null", args("ApiResource", apiResource));
			return;
		}
		String apiResourceStatus = OIDFJSON.getString(apiResource.get("CertificationStatus"));
		String apiFamilyType = OIDFJSON.getString(apiResource.get("ApiFamilyType"));
		String apiResourceVersion = OIDFJSON.getString(apiResource.get("ApiVersion"));
		String apiResourcrId = OIDFJSON.getString(apiResource.get("ApiResourceId"));
		switch (apiResourceStatus) {
			case "Warning":
				logSuccess("ApiResource status is Warning", args("CertificationStatus", apiResourceStatus, "ApiResource", apiResource));
				String warningMessage = String.format("ApiResource has Warning status: ApiResourceName: %s, ApiResourceVersion: %s, ApiResourceId: %s, CertificationStatus: %s", apiFamilyType, apiResourceVersion, apiResourcrId,apiResourceStatus);
				warningMessages.add(warningMessage);
				isWarning = true;
				break;
			case "Rejected":
				logSuccess("ApiResource status is Rejected", args("CertificationStatus", apiResourceStatus, "ApiResource", apiResource));
				String failureMessage = String.format("ApiResource has Rejected status: ApiResourceName: %s, ApiResourceVersion: %s, ApiResourceId: %s, CertificationStatus: %s", apiFamilyType, apiResourceVersion, apiResourcrId,apiResourceStatus);
				failureMessages.add(failureMessage);
				isFailure = true;
				break;
			default:
				logSuccess("ApiResource status is Valid", args("CertificationStatus", apiResourceStatus, "ApiResource", apiResource));
				break;
		}
	}
}
