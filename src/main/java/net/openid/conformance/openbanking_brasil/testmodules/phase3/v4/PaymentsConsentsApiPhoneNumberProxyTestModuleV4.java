package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsApiPhoneNumberProxyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_phone-number-proxy_test-module_v4",
	displayName = "Ensure paymets flow is sucessful when a valid phone number proxy is sent",
	summary = "Ensure paymets flow is sucessful when a valid phone number proxy is sent\n" +
		"\u2022 Calls POST Consents Endpoint with valid phone number as proxy, using DICT as the localInstrument\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" \n" +
		"\u2022 Calls the POST Payments with valid payload\n" +
		"\u2022 Expects 201 - Validate response\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Expectes Definitive  state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsApiPhoneNumberProxyTestModuleV4 extends AbstractPaymentsConsentsApiPhoneNumberProxyTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
