package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class RemoveEnrollmentsUrlFromResource extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("config").getAsJsonObject("resource");
		if (resource == null) {
			throw error("No resource in config file");
		}
		resource.remove("enrollmentsUrl");
		logSuccess("enrollmentsUrl field removed from resource", args("resource", resource));
		return env;
	}
}
