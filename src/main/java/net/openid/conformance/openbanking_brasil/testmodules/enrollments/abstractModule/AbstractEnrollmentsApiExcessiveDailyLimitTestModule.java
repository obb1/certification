package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.ExtractDailyLimitFromEnrollmentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.ExtractTransactionLimitFromEnrollmentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj.SetConsentIdInPaymentsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasValorAcimaLimite;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToTransactionLimitOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureErrorResponse.EnsureErrorResponseCodeFieldWasValorAcimaLimite;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractEnrollmentsApiExcessiveDailyLimitTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	protected int amountOfPayments;
	protected int currentPayment = 0;

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		super.getEnrollmentsAfterFidoRegistration();
		callAndStopOnFailure(ExtractTransactionLimitFromEnrollmentResponse.class);
		callAndStopOnFailure(ExtractDailyLimitFromEnrollmentResponse.class);
	}

	@Override
	protected void configPaymentsFlow() {
		setAmountOfPayments();
		callAndStopOnFailure(SetPaymentAmountToTransactionLimitOnConsent.class);
		super.configPaymentsFlow();
	}

	@Override
	protected void configureDictInfo() {}

	@Override
	protected void postAndValidatePayment() {
		if (currentPayment < amountOfPayments) {
			super.postAndValidatePayment();
		} else {
			userAuthorisationCodeAccessToken();
			runInBlock(currentClientString() + "Call pix/payments endpoint", () -> {
				callAndStopOnFailure(SetConsentIdInPaymentsRequestBody.class);
				call(getPixPaymentSequence().replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class)));
			});

			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
			if (status == HttpStatus.CREATED.value()) {
				runInBlock(currentClientString() + "Validate response", this::validatePaymentsResponse);
				fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
			} else if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				env.mapKey(EnsureErrorResponseCodeFieldWasValorAcimaLimite.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndStopOnFailure(EnsureErrorResponseCodeFieldWasValorAcimaLimite.class);
				env.unmapKey(EnsureErrorResponseCodeFieldWasValorAcimaLimite.RESPONSE_ENV_KEY);
			}
		}
	}

	@Override
	protected void validateFinalState() {
		if (currentPayment < amountOfPayments) {
			super.validateFinalState();
		} else {
			callAndStopOnFailure(EnsurePaymentStatusWasRjct.class);
			callAndStopOnFailure(EnsurePaymentRejectionReasonCodeWasValorAcimaLimite.class);
		}
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		configPaymentsFlow();
		if (currentPayment < amountOfPayments) {
			isEnrollmentsFlowComplete = true;
			currentPayment++;
			eventLog.log(getName(), String.format("Starting execution of payment flow #%d", currentPayment));
			performPreAuthorizationSteps();
			makeRefreshTokenCall();
			executeTestSteps();
			onPostAuthorizationFlowComplete();
		} else {
			fireTestFinished();
		}
	}

	protected void setAmountOfPayments() {
		double transactionLimit = Double.parseDouble(env.getString("transaction_limit"));
		double dailyLimit = Double.parseDouble(env.getString("daily_limit"));
		amountOfPayments = (int) (dailyLimit / transactionLimit) + 1;
		eventLog.log(getName(), String.format("We are going to execute the entire payments flow %d times", amountOfPayments));
	}

	protected abstract Class<? extends Condition> getPaymentConsentValidator();
}
