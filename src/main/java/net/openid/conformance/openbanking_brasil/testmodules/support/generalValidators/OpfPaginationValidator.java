package net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.JsonHelper;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class OpfPaginationValidator extends AbstractJsonAssertingCondition {

	private static final List<String> LINKS_SANITARY_QUERY_PARAMS = List.of("pagination-key");

	protected String selfLink;
	protected String firstLink;
	protected String prevLink;
	protected String nextLink;
	protected String lastLink;
	protected int realNumberOfRecords;
	protected int expectedNumberOfRecords;

	protected int pageSize;
	protected int totalRecords;
	protected int totalPages;
	protected String requestDateTime;
	private JsonObject linksObject;
	private JsonObject metaObject;
	private JsonElement body;
	protected int currentPageNumber;

	protected String requestUri;

	protected boolean lastLinkDisabled;

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {

		prepareRecordData(env);
		lastLinkDisabled = isLastLinkDisabled();
		validateRegex(requestUri, 1);

		//Verify if self link is the same as the request
		if (!isUrlEqual(requestUri, selfLink)) {
			String requestMethod = getRequestMethod(env);
			if (!Strings.isNullOrEmpty(requestMethod) &&
				requestMethod.equals("POST")) {
				if (!selfLink.startsWith(requestUri)) {
					throw error("self link in the response is not the same as the request url. (#1)", args("requestUrl", requestUri, "selfLink", selfLink));
				}
			} else{
				throw error("self link in the response is not the same as the request url. (#2)", args("requestUrl", requestUri, "selfLink", selfLink));
			}
		}

		if (pageSize > 1000) {
			throw error("page-size cannot be greater than 1000", args("page-size", pageSize));
		}
		if(!requestUri.contains("transactions")) {
			if (isSinglePage()) {
				validateSinglePage();
			} else {
				//General validations for multiple pages
				if ((!Strings.isNullOrEmpty(lastLink) && getPageNumber(lastLink) != totalPages) || totalPages < 1) {
					throw error("Last link parameters are not according to the swagger specification.", args("links", linksObject));
				}

				if (currentPageNumber < 0 || currentPageNumber > totalPages) {
					throw error("Current page number is outside of expected page range",
						args("Current page: ", currentPageNumber, "Total pages: ", totalPages));
				} else if (currentPageNumber == 1) {
					validateFirstPage();
				} else if (currentPageNumber == totalPages) {
					validateLastPage();
				} else {
					validateMiddlePage();
				}
			}
		}

		return env;
	}

	protected void validateSinglePage() {

		if (realNumberOfRecords != expectedNumberOfRecords) {
			throw error("The actual number of records present in the response differs from the expected value provided on meta", args("meta", metaObject));
		}

		if (!Strings.isNullOrEmpty(prevLink) || !Strings.isNullOrEmpty(nextLink)) {
			throw error("There should not be prev/next links", args("links", linksObject));
		}

		if ((!Strings.isNullOrEmpty(firstLink) && !selfLink.equals(firstLink)) ||
			(!Strings.isNullOrEmpty(lastLink) && !selfLink.equals(lastLink))) {
			throw error ("Self link has to be the same as first/last links if they are present.", args("links", linksObject));
		}
	}

	protected  void validateFirstPage() {
		if (!Strings.isNullOrEmpty(prevLink)) {
			throw error("Self link is the first page but prev link was found", args("links", linksObject));
		}

		if ((expectedNumberOfRecords != realNumberOfRecords)) {
			throw error("Self link is the first page but does not have the right amount of records", args("meta", metaObject));
		}

		if ((!Strings.isNullOrEmpty(firstLink) && !selfLink.equals(firstLink))) {
			throw error ("Self link has to be the same as first link if it is present.", args("links", linksObject));
		}

		if (getPageNumber(nextLink) != currentPageNumber + 1) {
			throw error("Next link page does not point to the next page.", args("links", linksObject));
		}
	}

	protected void validateMiddlePage() {
		if (expectedNumberOfRecords != realNumberOfRecords) {
			throw error("Self link does not have the right amount of records", args("meta", metaObject));
		}

		if (getPageNumber(prevLink) != currentPageNumber - 1) {
			throw error("Prev link page does not point to the previous page.", args("links", linksObject));
		}

		if (getPageNumber(nextLink) != currentPageNumber + 1) {
			throw error("Next link page does not point to the next page.", args("links", linksObject));
		}

		if (getPageNumber(firstLink) != 1) {
			throw error("First link page is not 1.", args("links", linksObject));
		}
	}

	protected void validateLastPage() {
		if (!Strings.isNullOrEmpty(nextLink)) {
			throw error("Self link is the last page but next link was found", args("links", linksObject));
		}


		if (realNumberOfRecords < pageSize && realNumberOfRecords != (totalRecords % pageSize)) {
			throw error("Number of records does not match expected amount for final page",
				args("totalRecords", totalRecords, "number of records on page", realNumberOfRecords, "pageSize", pageSize));
		}

		if (expectedNumberOfRecords != realNumberOfRecords) {
			throw error("Self link does not have the right amount of records", args("meta", metaObject));
		}

		if (getPageNumber(prevLink) != currentPageNumber - 1) {
			throw error("Prev link page does not point to the previous page.", args("links", linksObject));
		}

		if (getPageNumber(firstLink) != 1) {
			throw error("First link page is not 1.", args("links", linksObject));
		}
	}
	private boolean isSinglePage() {
		return totalPages <= 1 && currentPageNumber == 1;
	}

	private boolean isUrlPathEqual(String urlA, String urlB) {
		try {
			URI uriA = new URI(urlA);
			URI uriB = new URI(urlB);

			return uriA.getPath().equals(uriB.getPath());
		} catch (URISyntaxException e) {
			return false;
		}
	}
	private boolean isUrlEqual(String urlA, String urlB) {
		try {
			LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String formattedDate = currentDate.format(dateFormatter);

			urlA = urlA.replaceAll("page-size=25&?\\b","")
				.replaceAll("page=1&?\\b","")
				.replaceAll("fromTransactionDate=" + formattedDate + "&?", "")
				.replaceAll("toTransactionDate=" + formattedDate + "&?", "")
				.replaceAll("fromTransactionConversionDate=" + formattedDate + "&?", "")
				.replaceAll("toTransactionConversionDate=" + formattedDate + "&?", "");
			urlB = urlB.replaceAll("page-size=25&?\\b","")
				.replaceAll("page=1&?\\b","")
				.replaceAll("fromTransactionDate=" + formattedDate + "&?", "")
				.replaceAll("toTransactionDate=" + formattedDate + "&?", "")
				.replaceAll("fromTransactionConversionDate=" + formattedDate + "&?", "")
				.replaceAll("toTransactionConversionDate=" + formattedDate + "&?", "");

			if(urlA.endsWith("?")) {
				urlA=urlA.substring(0,urlA.length()-1);
			}
			if(urlB.endsWith("?")) {
				urlB=urlB.substring(0,urlB.length()-1);
			}

			int urlAPageSize = getPageSize(urlA);
			int urlBPageSize = getPageSize(urlB);
			int urlAPage = getPageNumber(urlA);
			int urlBPage = getPageNumber(urlB);

			if(urlAPageSize >= urlBPageSize)
			{
				urlA = urlA.replaceAll("page-size=[0-9]+&?","");
				urlB = urlB.replaceAll("page-size=[0-9]+&?","");
			}
			else{
				return false;
			}

			URI uriA = new URI(urlA);
			URI uriB = new URI(urlB);
			String queryA = uriA.getQuery();
			String queryB = uriB.getQuery();
			Map<String, List<String>> paramsA = getQueryParams(queryA);
			Map<String, List<String>> paramsB = getQueryParams(queryB);

			String noQuerryUrlA = urlA.split("\\?")[0];
			String noQuerryUrlB = urlB.split("\\?")[0];

			return noQuerryUrlA.equals(noQuerryUrlB) && paramsA.equals(paramsB) && isUrlPathEqual(urlA, urlB) && urlAPage == urlBPage;
		} catch (URISyntaxException e) {
			return false;
		}
	}

	public static Map<String, List<String>> getQueryParams(String query){
		Map<String, List<String>> params = new HashMap<>();

		if (query == null || query.isEmpty()) {
			return params;
		}

		String[] pairs = query.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			String key = idx > 0 ? pair.substring(0, idx) : pair;
			if (!params.containsKey(key)) {
				params.put(key, new LinkedList<String>());
			}
			String value = idx > 0 && pair.length() > idx + 1 ? pair.substring(idx + 1) : null;
			params.get(key).add(value);
		}

		return params;
	}


	private String sanitiseLink(String link, String linkName){
		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromUri(new URI(link));

			LINKS_SANITARY_QUERY_PARAMS.forEach(param -> builder.replaceQueryParam(param, List.of()));

			log(String.format("Cleaned up %s", linkName),
				Map.of("sanity_query_params", LINKS_SANITARY_QUERY_PARAMS,
					linkName, link));

			return builder.toUriString();
		} catch (URISyntaxException e) {
			throw error(String.format("Could not %s", linkName),
				Map.of("linkName", link));
		}
	}


	public void prepareRecordData(Environment env) {

		body = bodyFrom(env,RESPONSE_ENV_KEY);
		setRequestUri(env);

		realNumberOfRecords = countRecords();
		linksObject = findByPath(body, "$.links").getAsJsonObject();
		if (!Strings.isNullOrEmpty(OIDFJSON.getString(findByPath(linksObject, "$.self")))) {
			selfLink = sanitiseLink(OIDFJSON.getString(findByPath(linksObject, "$.self")), "self_link");
		}

		if (JsonHelper.ifExists(linksObject, "$.first")) {
			firstLink = sanitiseLink(OIDFJSON.getString(findByPath(linksObject, "$.first")), "first_link");
		}
		if (JsonHelper.ifExists(linksObject, "$.prev")) {
			prevLink = sanitiseLink(OIDFJSON.getString(findByPath(linksObject, "$.prev")), "last_link");
		}
		if (JsonHelper.ifExists(linksObject, "$.next")) {
			nextLink = sanitiseLink(OIDFJSON.getString(findByPath(linksObject, "$.next")), "next_link");
		}
		if (JsonHelper.ifExists(linksObject, "$.last")) {
			lastLink = sanitiseLink(OIDFJSON.getString(findByPath(linksObject, "$.last")), "last_link");
		}

		currentPageNumber = getPageNumber(selfLink);
		pageSize = getPageSize(selfLink);

		metaObject = findByPath(body, "$.meta").getAsJsonObject();

		requestDateTime = OIDFJSON.getString(findByPath(metaObject, "$.requestDateTime"));

		if(!requestUri.contains("transactions")){
			totalRecords = OIDFJSON.getInt(findByPath(metaObject, "$.totalRecords"));
			totalPages = OIDFJSON.getInt(findByPath(metaObject, "$.totalPages"));
			expectedNumberOfRecords = calculateExpectedNumberOfRecords();
		}
	}

	protected int calculateExpectedNumberOfRecords() {
		if (currentPageNumber == totalPages) {
			return totalRecords - (totalPages - 1)*pageSize;
		}
		if(totalPages==0) {
			return totalRecords;
		}
		return pageSize;
	}

	protected int countRecords() {
		JsonElement dataElement = findByPath(body, "$.data");
		if (!dataElement.isJsonArray()) {
			if (requestUri.contains("payments") && findByPath(dataElement, "$.releases")!= null) {
				JsonArray dataArray = findByPath(dataElement, "$.releases").getAsJsonArray();
				return dataArray.size();
			} else if (requestUri.contains("scheduled-instalments") && JsonHelper.ifExists(dataElement, "$.balloonPayments")) {
				JsonArray dataArray = findByPath(dataElement, "$.balloonPayments").getAsJsonArray();
				return dataArray.size();
			} else {
				log("Data element on response is not an array, no pagination is assumed.");
				return 1;
			}
		}
		JsonArray dataArray = dataElement.getAsJsonArray();
		return dataArray.size();
	}

	protected void setRequestUri(Environment env) {

		if (env.getEffectiveKey(RESPONSE_ENV_KEY).contains("consent")) {
			if(getRequestMethod(env).equals("POST")){
				requestUri = env.getString("config", "resource.consentUrl");
			}else if(getRequestMethod(env).equals("GET")){
				requestUri = env.getString("consent_url");
			}
			if (Strings.isNullOrEmpty(requestUri)) {
				throw error("consent url missing from configuration");
			}
		} else {
			requestUri = env.getString("protected_resource_url");
			if (Strings.isNullOrEmpty(requestUri)) {
				throw error("resource url missing from configuration");
			}
		}
		requestUri = sanitiseLink(requestUri, "request_uri");
		log(String.format("Proceeding validation with %s", requestUri));
	}

	protected void validateRegex(String requestUri, int expectedVersion) {
		LinksValidator regexLinksValidator = new LinksValidator(this);
		if (lastLinkDisabled) {
			regexLinksValidator.setNoLastLink(true);
		}
		MetaValidator regexMetaValidator = new MetaValidator(this);

		if(requestUri.contains("transactions")){
			regexMetaValidator = new MetaValidator(this,false,false,true);
		}

		regexMetaValidator.assertMetaObject(body);
		regexLinksValidator.assertLinksObject(body, requestUri, expectedVersion);
		String errorMessage = regexMetaValidator.getErrorMessage();
		if (!Objects.equals(errorMessage, "")) {
			throw error(errorMessage, regexMetaValidator.getArgs());
		}
	}

	protected int getPageNumber(String uri) {
		try {
			Optional<NameValuePair> params = URLEncodedUtils.parse(new URI(uri), StandardCharsets.UTF_8).stream()
				.filter(p -> p.getName().equals("page"))
				.findFirst();
			if (params.isEmpty()) {
				return Integer.parseInt(new BasicNameValuePair("page", "1").getValue());
			}
			return Integer.parseInt(params.get().getValue());

		} catch (URISyntaxException e) {
			throw error("Link is not a valid URI", Map.of("Link", uri));
		}
	}

	protected int getPageSize(String uri) {
		try {
			List<NameValuePair> params = URLEncodedUtils.parse(new URI(uri), StandardCharsets.UTF_8);
			return Integer.parseInt(
				params.stream()
					.filter(p -> p.getName().equals("page-size"))
					.findFirst()
					.orElse(new BasicNameValuePair("page-size", "25"))
					.getValue());

		} catch (URISyntaxException e) {
			throw error("Link is not a valid URI", Map.of("Link", uri));
		}
	}

	private boolean isLastLinkDisabled() {
		String[] apisToCheck = {"/accounts/", "/credit-cards-accounts/", "/bank-fixed-incomes/", "/credit-fixed-incomes/", "/variable-incomes/", "/treasure-titles/", "/funds/"};

		for (String api : apisToCheck) {
			if(selfLink.contains(api)){
				if (selfLink.matches("^https://(.*)/v\\d/([a-zA-Z][a-zA-Z-]{0,99})/([a-zA-Z0-9][a-zA-Z0-9-]{0,99})/(transactions|transaction-current)(/)?(.)*")) {
					log("Validating transactions endpoint - No last link is expected");
					return true;
				}
			}
		}
		return false;
	}

	private String getRequestMethod(Environment env) {
		String requestMethod = env.getString("http_method");
		if (Strings.isNullOrEmpty(requestMethod)) {
			//Post is the standard http method used
			return "POST";
		}

		return requestMethod;
	}
}
