package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors;

import com.google.gson.JsonArray;

public class EditRecurringPaymentsConsentBodyToSetOnePessoaNaturalCreditor extends AbstractEditRecurringPaymentsConsentBodyToSetCreditor {

	@Override
	protected void addCreditorsToArray(JsonArray creditors) {
		creditors.add(buildCreditor("PESSOA_NATURAL", "45083807785", "Jose Silva"));
	}

	@Override
	protected String logMessage() {
		return "1 PESSOA_NATURAL creditor";
	}
}
