package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment;

public class AddSecondCnpjToPaymentRequestBody extends AbstractAddCnpjToPaymentRequestBody {

	@Override
	protected String getKey() {
		return "second_creditor";
	}
}
