package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.AbstractCreditFixedIncomesTransactionsOASValidator;

public abstract class AbstractCreditFixedIncomesTransactionsOASValidatorV1n3 extends AbstractCreditFixedIncomesTransactionsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/creditFixedIncomes/credit-fixed-incomes-v1.0.3.yml";
	}
}
