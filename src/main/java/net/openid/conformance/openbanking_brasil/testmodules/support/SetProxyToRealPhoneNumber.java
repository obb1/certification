package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SetProxyToRealPhoneNumber extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		Optional.ofNullable(
			env.getElementFromObject("resource", "brazilPaymentConsent.data.payment.details")
		).map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Unable to find resource.brazilPaymentConsent.data.payment.details"))
			.addProperty("proxy", DictHomologKeys.PROXY_PHONE_NUMBER);

		JsonElement paymentRequestDataElement = Optional.ofNullable(
			env.getElementFromObject("resource", "brazilPixPayment.data")
		).orElseThrow(() -> error("Unable to find resource.brazilPixPayment.data"));

		if (paymentRequestDataElement.isJsonObject()) {
			paymentRequestDataElement.getAsJsonObject().addProperty("proxy", DictHomologKeys.PROXY_PHONE_NUMBER);
		} else if (paymentRequestDataElement.isJsonArray()) {
			for (JsonElement paymentElement : paymentRequestDataElement.getAsJsonArray()) {
				paymentElement.getAsJsonObject().addProperty("proxy", DictHomologKeys.PROXY_PHONE_NUMBER);
			}
		} else {
			throw error("Invalid data type for payment request data", args("data", paymentRequestDataElement));
		}

		logSuccess("Added phone number as proxy to both payment and consent request payloads");

		return env;
	}

}
