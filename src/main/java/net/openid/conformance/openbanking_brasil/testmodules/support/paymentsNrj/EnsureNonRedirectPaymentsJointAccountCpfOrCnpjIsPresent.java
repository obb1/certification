package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class EnsureNonRedirectPaymentsJointAccountCpfOrCnpjIsPresent extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getElementFromObject("config", "resource").getAsJsonObject();
		JsonElement cpfElement = env.getElementFromObject("config", "conditionalResources.brazilCpfJointAccount");
		JsonElement cnpjElement = env.getElementFromObject("config", "conditionalResources.brazilCnpjJointAccount");

		if (cpfElement == null && cnpjElement ==  null) {
			env.putBoolean("continue_test", false);
			throw error("Brazil CPF and CNPJ for Joint Account field is empty. Institution is assumed to not have this functionality");
		}

		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("config", "resource.brazilPaymentConsent.data"))
			.orElseThrow(() -> error("Could not find brazilPaymentConsent")).getAsJsonObject();

		if (cpfElement != null) {
			String cpf = OIDFJSON.getString(cpfElement);
			JsonObject loggedUser = new JsonObjectBuilder()
				.addField( "document.identification", cpf)
				.addField( "document.rel", "CPF")
				.build();

			brazilPaymentConsent.add("loggedUser", loggedUser);
			resource.addProperty("loggedUserIdentification", cpf);
			logSuccess("Brazil CPF for Joint Account field is present and added", args("loggedUser", loggedUser));
		}

		if (cnpjElement != null) {
			String cnpj = OIDFJSON.getString(cnpjElement);
			JsonObject businessEntity = new JsonObjectBuilder()
				.addField( "document.identification", cnpj)
				.addField( "document.rel", "CNPJ")
				.build();

			brazilPaymentConsent.add("businessEntity", businessEntity);
			resource.addProperty("businessEntityIdentification", cnpj);
			logSuccess("Brazil CNPJ for Joint Account field is present and added", args("businessEntity", businessEntity));
		}
		return env;
	}
}
