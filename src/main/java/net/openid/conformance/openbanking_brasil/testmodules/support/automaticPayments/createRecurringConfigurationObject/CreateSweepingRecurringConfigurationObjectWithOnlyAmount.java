package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.lang3.StringUtils;

public class CreateSweepingRecurringConfigurationObjectWithOnlyAmount extends AbstractCreateRecurringConfigurationObject {

	@Override
	protected JsonObject buildRecurringConfiguration(Environment env) {
		String totalAmount = env.getString("recurring_total_amount");
		return new JsonObjectBuilder()
			.addField("sweeping.totalAllowedAmount", !StringUtils.isBlank(totalAmount) ? totalAmount : AMOUNT)
			.addField("sweeping.startDateTime", getStartDateTime())
			.build();
	}
}
