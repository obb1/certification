package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsRootCnpjTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument.AddManuLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveProxy;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-root-cnpj_test-module_v2",
	displayName = "automatic-payments_api_sweeping-accounts-root-cnpj_test-module_v2",
	summary = "Ensure a payment using sweeping account can be successfully executed when the creditors are all from the same company. " +
		"This test will always use the following CNPJ 98380199000125 as proxy and businessEntityIdentification when creating the payment, and not considering what is informed on this field on the config, although if this field is not informed the test will be skipped. " +
		"Therefore the 98380199000125 should be registered in the ASPSP for a successful test run.\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending the creditor account with 4 cnpjs, all of them containing the same root\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the POST recurring-payments endpoint , and the creditor as the first cnpj\n" +
		"• Expect 201 - Validate Response\n" +
		"• Repeat the payments process and validation, but now sending the creditor with a different cnpj sent at the consent",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsRootCnpjTestModuleV2 extends AbstractAutomaticPaymentsApiSweepingAccountsRootCnpjTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddManuLocalInstrument.class);
		super.onConfigure(config, baseUrl);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentsBodyToRemoveProxy.class);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}
}
