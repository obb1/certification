package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiAutomaticPixRetryIntradayCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_automatic-pix-intraday-core_test-module_v2",
	displayName = "automatic-payments_api_automatic-pix-intraday-core_test-module_v2",
	summary = "This test will simulate an intraday retry after a failed payment. To achieve this, the test requires a pre-created and approved Consent ID, which should be configured in the \"Intraday ConsentId - One Payment\" field, with a Failed Payment ID, which will be retrieved during the test. The test will also need a refresh token linked to the client and user specified in the configuration. An access token derived from this refresh token will be used to make the payment, configured in the \"Intraday Refresh Token - One Payment\" field. The pre-set consent must have isRetryAccepted set to false, and no firstPayment should be included. Given the expected time to resend the new E2EID, this test can be executed until 12pm BRL time.\n" +
		"• Call the GET recurring-consents {recurringConsentId} endpoint, using the Intraday ConsentId One Payment specified in the Config and a client_credentials token\n" +
		"• Expect a 200 response - Validate the response, ensure the status is \"Authorised\", isRetryAccepted is false, no firstPayment information is present\n" +
		"• Call the GET recurring-payment endpoint with the recurringConsent ID in the header.\n" +
		"• Expect a 200 response - Validate the response, ensure there is only one payment, the status is RJCT, the payment date is set to D+0, and that the rejectionReason expects a new E2EID\n" +
		"• Call the GET recurring-payments {recurringPaymentId} endpoint to fetch the original payment data.\n" +
		"• Expect a 200 response - Validate the response and extract its fields.\n" +
		"• Call the POST recurring-payments endpoint, using an access token derived from the refresh token in the configuration. Send a new E2EID, date as D+0, and setting the amount as double of the original value,  and include the paymentId in originalRecurringPaymentId\n" +
		"• Expect 422 DETALHE_TENTATIVA_INVALIDO or 201 - Validate Response\n" +
		"If a 201 is returned:\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD or ACCP\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is DETALHE_TENTATIVA_INVALIDO\n" +
		"• Call the POST recurring-payments endpoint, using an access token derived from the refresh token in the configuration. Send a new E2EID, date as D+0, and the other fields with the same information as the previous payment, and include the paymentId in originalRecurringPaymentId\n" +
		"• Expect a 201 response - Validate the response\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD or ACCP\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is SCHD\n" +
		"• Validate if the current time is set between 00h00 am UTC-3 and 11:59am UTC-3 - Return failure if not as defined on the test summary",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.intradayOnePaymentConsentId",
		"resource.intradayOnePaymentRefreshToken"
	}
)
public class AutomaticPaymentsApiAutomaticPixRetryIntradayCoreTestModuleV2 extends AbstractAutomaticPaymentsApiAutomaticPixRetryIntradayCoreTestModule {

	@Override
	protected Class<? extends Condition> getPaymentsByConsentIdValidator() {
		return GetRecurringPaymentPixListOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}
}
