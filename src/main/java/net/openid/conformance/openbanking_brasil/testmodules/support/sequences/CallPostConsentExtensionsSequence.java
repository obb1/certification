package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.AddCustomerUserAgentHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.AddDummyCustomerIpAddress;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CallConsentExtensionEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureConsentStatusUpdateTimeHasNotChanged;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.FAPIBrazilOpenBankingCreateConsentExtensionRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToPostConsentExtension;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateIndefiniteExpirationTimeReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class CallPostConsentExtensionsSequence extends AbstractConditionSequence {
	@Override
	public void evaluate() {
		callAndStopOnFailure(PrepareToPostConsentExtension.class);
		callAndStopOnFailure(CreateIndefiniteConsentExpiryTime.class);
		callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentExtensionRequest.class);
		callAndStopOnFailure(PrepareToPostConsentExtension.class);
		callAndStopOnFailure(AddDummyCustomerIpAddress.class);
		callAndStopOnFailure(AddCustomerUserAgentHeader.class);
		callAndContinueOnFailure(CallConsentExtensionEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
		callAndStopOnFailure(EnsureConsentResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ValidateIndefiniteExpirationTimeReturned.class);
		callAndContinueOnFailure(EnsureConsentWasAuthorised.class, Condition.ConditionResult.FAILURE);
		call(exec().unmapKey("resource_endpoint_response_full"));
		callAndStopOnFailure(EnsureConsentStatusUpdateTimeHasNotChanged.class, Condition.ConditionResult.FAILURE);
	}
}
