package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class CreateEmptyConsentExpiryTime extends AbstractCondition {

	@Override
	@PostEnvironment(required = "extension_expiry_times")
	public Environment evaluate(Environment env) {
		String expiryTime = "";
		JsonObject expiryTimes = env.getObject("extension_expiry_times");
		if (expiryTimes == null) {
			expiryTimes = new JsonObject();
			JsonArray expiryTimesArray = new JsonArray();
			expiryTimesArray.add(expiryTime);
			expiryTimes.add("extension_times", expiryTimesArray);
		} else {
			JsonArray expiryTimesArray = expiryTimes.getAsJsonArray("extension_times");
			expiryTimesArray.add(expiryTime);
		}
		env.putObject("extension_expiry_times", expiryTimes);
		env.putString("consent_extension_expiration_time", expiryTime);
		logSuccess("Created empty consent extension expiry time");
		return env;
	}
}
