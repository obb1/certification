package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class CreateRiskSignalsRequestBodyToRequestEntityClaims extends AbstractCondition {
	@Override
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		env.putObject("resource_request_entity_claims", getRiskSignalsObject());
		logSuccess("Successfully created risk-signals request body", args(
			"body", env.getObject("resource_request_entity_claims")
		));
		return env;
	}


	protected static JsonObject getRiskSignalsObject(){
		return new JsonObjectBuilder().addFields("data", Map.of(
			"deviceId", "5ad82a8f-37e5-4369-a1a3-be4b1fb9c034",
			"isRootedDevice", false,
			"screenBrightness", 90,
			"elapsedTimeSinceBoot", 28800000,
			"osVersion", "16.6",
			"userTimeZoneOffset", "-03",
			"language", "pt",
			"accountTenure", "2023-01-01"
		)).addFields("data.screenDimensions", Map.of(
			"height", 1080,
			"width", 1920
		)).build();
	}
}
