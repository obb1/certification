package net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesEventsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperation;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperationEvents;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "exchange_api_core_test-module",
	displayName = "exchange_api_core_test-module",
	summary = "Test will call all of the endpoints of the Exchange API, confirming that they return the expected response_body, as defined on the swagger. This test will: \n" +
	"• Call the POST Consents endpoint with the Exchange API Permission Group\n" +
	"• Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
	"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
	"• Call the GET Consents endpoint\n" +
	"• Expects 200 - Validate response and ensure status is AUTHORISED\n" +
	"• Call the GET Operations Endpoint\n" +
	"• Expects 200  - Validate response - Extract one of the exchange operations\n" +
	"• Call the GET Operation/{Operationid} endpoint with the extracted operationID\n" +
	"• Expects 200  - Validate response\n" +
	"• Call the GET  Call the GET Operation/{Operationid}/events endpoint with the extracted operationID\n" +
	"• Expects 200  - Validate response\n" +
	"• Call the Delete Consents Endpoints\n" +
	"• Expect a 204 without a body\n",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class ExchangeApiCoreTestModule extends AbstractExchangesApiTestModule {

	@Override
	protected void executeParticularTestSteps() {
		getAndValidateApi("Details", () -> callAndStopOnFailure(PrepareUrlForFetchingOperation.class), GetExchangesOperationIdentificationV1OASValidator.class);
		getAndValidateApi("Events", () -> callAndStopOnFailure(PrepareUrlForFetchingOperationEvents.class), GetExchangesEventsV1OASValidator.class);
	}

	protected void getAndValidateApi(String apiName, Runnable prepareUrl, Class<? extends Condition> validator) {
		runInBlock(String.format("GET Operations %s endpoint with extracted operationId", apiName), () -> {
			prepareUrl.run();
			callAndStopOnFailure(CallProtectedResource.class);
		});
		runInBlock(String.format("Validating GET Operations %s response", apiName), () -> {
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(validator,Condition.ConditionResult.FAILURE);
		});
	}
}
