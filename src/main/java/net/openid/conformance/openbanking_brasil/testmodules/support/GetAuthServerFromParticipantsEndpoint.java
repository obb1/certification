package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.extensions.SpringContext;
import net.openid.conformance.extensions.yacs.ParticipantsService;
import net.openid.conformance.testmodule.Environment;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class GetAuthServerFromParticipantsEndpoint extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		ParticipantsService participantsService = getParticipantsService();
		String organisationId = getStringFromEnvironment(env, "config", "resource.brazilOrganizationId", "Config brazilOrganizationId Field");
		String configWellKnownUri = getStringFromEnvironment(env, "config", "server.discoveryUrl", "Config Discovery URL Field");
		organisationId = organisationId.replaceAll("-","_");
		Set<Map> orgs = participantsService.orgsFor(Set.of(organisationId));
		logSuccess("Found organisation", args("org", orgs));
		Map authServer = null;
		for(Map org: orgs) {
			List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
			for (Map map : authServersForOrg) {
				String wellKnown = (String) map.get("OpenIDDiscoveryDocument");
				if (wellKnown != null && wellKnown.trim().equals(configWellKnownUri.trim())) {
					authServer = map;
				}
			}
		}
		if (authServer == null) {
			throw error("Could not find Authorisation Server with provided Well-Known URL in the Directory Participants List",
				args("Well-Known", configWellKnownUri, "OrganisationID", organisationId));
		}
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writeValueAsString(authServer);
		} catch (JsonProcessingException e) {
			throw error("Could not parse Authorisation Server JSON", e, args("authServer", authServer));
		}
		JsonObject authServerObject;
		if (json != null){
			authServerObject = (JsonObject) JsonParser.parseString(json);
		} else {
			throw error("Could not parse Authorisation Server JSON", args("authServer", authServer));
		}
		env.putObject("authorisation_server", authServerObject);
		logSuccess("Found auth server from participants endpoint", args("authServer", authServerObject));
		return env;
	}
	private ParticipantsService getParticipantsService() {
		return SpringContext.getBean(ParticipantsService.class);
	}

}
