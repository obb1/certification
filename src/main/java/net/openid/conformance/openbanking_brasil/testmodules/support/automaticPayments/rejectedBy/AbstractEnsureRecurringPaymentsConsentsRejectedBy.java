package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedBy;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRejectedByEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureRecurringPaymentsConsentsRejectedBy extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String rejectedBy = OIDFJSON.getString(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
					.map(data -> data.getAsJsonObject("rejection"))
					.map(rejection -> rejection.get("rejectedBy"))
					.orElseThrow(() -> error("Unable to find element data.rejection.rejectedBy in the response payload"))
			);
			if (rejectedBy.equals(expectedRejectedBy().toString())) {
				logSuccess("rejectedBy returned in the response matches the expected rejectedBy");
			} else {
				throw error("rejectedBy returned in the response does not match the expected rejectedBy",
					args("rejectedBy", rejectedBy,
						"expected rejectedBy", expectedRejectedBy().toString())
				);
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract RecurringPaymentsConsentsRejectedByEnum expectedRejectedBy();
}
