package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetVariableIncomesListV1OASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/variableIncome/variable-income-v1.2.0.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected String getEndpointPath() {
		return "/investments";
	}


}
