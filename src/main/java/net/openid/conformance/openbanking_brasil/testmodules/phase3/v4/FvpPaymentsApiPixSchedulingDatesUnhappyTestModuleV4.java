package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsApiPixSchedulingDatesUnhappyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_pixscheduling-dates-unhappy_test-module_v4",
	displayName = "5 Negative Test Scenarios that change the payment date and schedule.single.date fields and expect errors on the POST Consents request",
	summary = "5 Negative Test Scenarios that change the payment date and schedule.single.date fields and expect errors on the POST Consents request\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"• Attempts to create a payment consent scheduled with the date element present together with schedule.single.date, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA\n" +
		"• Create consent with request payload with both the date and the schedule.single.date fields\n" +
		"• Call the POST Consents endpoints\n" +
		"• Expects 422 with error DATA_PAGAMENTO_INVALIDA - Validate Error response \n" +
		"• Attempts to create a payment consent scheduled for today, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA\n" +
		"• Create consent with request payload set with schedule.single.date field defined as today\n" +
		"• Call the POST Consents endpoints\n" +
		"• Expects 422 with error  DATA_PAGAMENTO_INVALIDA - Validate Error response \n" +
		"• Attempts to create a payment consent scheduled for a day in the past, with a payment date of yesterday, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA \n" +
		"• Create consent with request payload set with schedule.single.date field defined as yesterday D-1\n" +
		"• Call the POST Consents endpoints\n" +
		"• Expects 422 with error DATA_PAGAMENTO_INVALIDA  - Validate Error response \n" +
		"• Attempts to create a payment consent scheduled for a day too far in the future, with a payment date of yesterday + 740 days, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA \n" +
		"• Create consent with request payload set with schedule.single.date field defined as yesterday D+370\n" +
		"• Call the POST Consents endpoints\n" +
		"• Expects 422 with error DATA_PAGAMENTO_INVALIDA  - Validate Error response \n" +
		"• Attempts to create a payment consent wihtout the date or date scheduled fields, both which are optional, expecting error PARAMETRO_NAO_INFORMADO\n" +
		"• Create consent with request payload set without the date field provided\n" +
		"• Call the POST Consents endpoints\n" +
		"• Expects 422 with error PARAMETRO_NAO_INFORMADO  - Validate Error response\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpPaymentsApiPixSchedulingDatesUnhappyTestModuleV4 extends AbstractFvpPaymentsApiPixSchedulingDatesUnhappyTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
