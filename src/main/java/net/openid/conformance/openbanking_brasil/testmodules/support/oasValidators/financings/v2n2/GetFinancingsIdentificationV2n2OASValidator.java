package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public class GetFinancingsIdentificationV2n2OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/financings/financings-v2.2.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/contracts/{contractId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		assertData(data);
		assertFinanceCharges(data);
		assertContractedFees(data);
	}


	private void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"instalmentPeriodicityAdditionalInfo",
			"instalmentPeriodicity",
			"OUTROS"
		);


		assertField1IsRequiredWhenField2HasValue2(
			data,
			"amortizationScheduledAdditionalInfo",
			"amortizationScheduled",
			"OUTROS"
		);

	}

	private void assertFinanceCharges(JsonObject data) {
		data.getAsJsonArray("contractedFinanceCharges").forEach(el ->
			assertField1IsRequiredWhenField2HasValue2(
				el.getAsJsonObject(),
				"chargeAdditionalInfo",
				"chargeType",
				"OUTROS"
			));


	}

	private void assertContractedFees(JsonObject data) {
		data.getAsJsonArray("contractedFees").forEach(el ->
			assertField1IsRequiredWhenField2HasValue2(
				el.getAsJsonObject(),
				"feeRate",
				"feeCharge",
				"PERCENTUAL"
			)
		);
	}
}
