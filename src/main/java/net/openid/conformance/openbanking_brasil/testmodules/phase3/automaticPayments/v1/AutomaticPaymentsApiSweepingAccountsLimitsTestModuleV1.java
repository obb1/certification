package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiSweepingAccountsLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_sweeping-accounts-limits_test-module_v1",
	displayName = "automatic-payments_api_sweeping-accounts-limits_test-module_v1",
	summary = "Ensure a Payment for sweeping accounts cannot be executed if its above the established limits in different scenarios. If a brazilCpf or brazilCnpj is informed at the config, these fields will be used at both the loggedUser/businessEntity and Creditor. If not, the institution will be required to have an account registered with the CPF 99991111140.\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields, sending the sweeping.transactionLimit value as 400.00, sweeping.periodicLimits.day.transactionLimit as 500.00 and sweeping.periodicLimits.day.quantityLimit as 2\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"1. Ensure an error is returned when the sweeping.transactionLimit value is exceeded\n" +
		"• Call the POST recurring-payments endpoint with the amount as 450.00 to the selected creditor\n" +
		"• Expect 201 or 422 LIMITE_PERIODO_VALOR_EXCEDIDO - Validate Error Message\n" +
		"If a 201 is returned:\n" +
		"\u3000∘ Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"\u3000∘ Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is LIMITE_PERIODO_VALOR_EXCEDIDO\n" +
		"2. Ensure a valid payment is successful\n" +
		"• Call the POST recurring-payments endpoint with the amount as 300.00 to the selected creditor\n" +
		"• Expect 201 - Validate Response\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is ACSC\n" +
		"3. Ensure an error is returned when the sweeping.periodicLimits.day.transactionLimit value is exceeded\n" +
		"• Call the POST recurring-payments endpoint with the amount as 300.00 to the selected creditor\n" +
		"• Expect 201 or 422 LIMITE_PERIODO_VALOR_EXCEDIDO - Validate Error Message\n" +
		"If a 201 is returned:\n" +
		"\u3000∘ Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"\u3000∘ Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is LIMITE_PERIODO_VALOR_EXCEDIDO\n" +
		"4. Ensure another valid payment is successful\n" +
		"• Call the POST recurring-payments endpoint with the amount as 100.00 to the selected creditor\n" +
		"• Expect 201 - Validate Response\n" +
		"• Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"• Call the GET recurring-payments {recurringPaymentId}\n" +
		"• Expect 200 - Validate Response and ensure status is ACSC\n" +
		"5. Ensure an error is returned when the sweeping.periodicLimits.day.quantityLimit value is exceeded\n" +
		"• Call the POST recurring-payments endpoint with the amount as 50.00 to the selected creditor\n" +
		"• Expect 201 or 422 LIMITE_PERIODO_QUANTIDADE_EXCEDIDO - Validate Error Message\n" +
		"If a 201 is returned:\n" +
		"\u3000∘ Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP or ACPD\n" +
		"\u3000∘ Expect 200 - Validate Response and ensure status is RJCT, ensure that rejectionReason.code is LIMITE_PERIODO_QUANTIDADE_EXCEDIDO\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName"
	}
)
public class AutomaticPaymentsApiSweepingAccountsLimitsTestModuleV1 extends AbstractAutomaticPaymentsApiSweepingAccountsLimitsTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetRecurringPixOASValidatorV1.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class), condition(GetAutomaticPaymentPixRecurringV1Endpoint.class));
	}

}
