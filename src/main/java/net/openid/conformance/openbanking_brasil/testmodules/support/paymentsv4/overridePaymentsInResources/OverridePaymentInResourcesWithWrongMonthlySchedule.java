package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.overridePaymentsInResources;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Optional;

public class OverridePaymentInResourcesWithWrongMonthlySchedule extends AbstractOverridePaymentInResources {

	@Override
	protected void overridePaymentsData(JsonArray data, JsonObject schedule) {
		JsonObject monthly = Optional.ofNullable(schedule.getAsJsonObject("monthly"))
			.orElseThrow(() -> error("Could not extract the monthly from the payment in resource"));

		int quantity = OIDFJSON.getInt(monthly.get("quantity"));
		String startDateString = OIDFJSON.getString(monthly.get("startDate"));
		int dayOfMonth = OIDFJSON.getInt(monthly.get("dayOfMonth"));
		if (data.size() != quantity) {
			throw error("Payments data array does not have the correct amount of payments in it");
		}

		String currentDateString = getStartDate(startDateString, dayOfMonth);
		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject();

			String oldEndToEndId = OIDFJSON.getString(payment.get("endToEndId"));
			String newEndToEndId = generateEndToEndId(oldEndToEndId, currentDateString);
			payment.addProperty("endToEndId", newEndToEndId);

			currentDateString = nextDateString(currentDateString, dayOfMonth);
		}
	}

	protected String getStartDate(String startDateString, int dayOfMonth) {
		LocalDate startDate = LocalDate.parse(startDateString);
		if (startDate.getDayOfMonth() <= dayOfMonth) {
			try {
				startDate = startDate.withDayOfMonth(dayOfMonth);
			} catch (DateTimeException err) {
				startDate = startDate.with(TemporalAdjusters.lastDayOfMonth());
			}
		} else {
			try {
				startDate = startDate.plusMonths(1).withDayOfMonth(dayOfMonth);
			} catch (DateTimeException err) {
				startDate = startDate.plusMonths(1).with(TemporalAdjusters.lastDayOfMonth());
			}
		}
		return startDate.format(DATE_FORMATTER);
	}

	protected String nextDateString(String currentString, int dayOfMonth) {
		LocalDate currentDate = LocalDate.parse(currentString);
		if (currentDate.getDayOfMonth() < dayOfMonth) {
			currentDate = currentDate.plusMonths(1).withDayOfMonth(dayOfMonth);
		} else {
			currentDate = currentDate.plusMonths(1);
		}
		return currentDate.format(DATE_FORMATTER);
	}
}
