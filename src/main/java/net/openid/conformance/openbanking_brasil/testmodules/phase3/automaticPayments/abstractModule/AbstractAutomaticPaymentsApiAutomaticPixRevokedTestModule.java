package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixHappyCorePathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRejectionRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanal;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemanalPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreOneAcscAndOneRjctPayments;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixRevokedTestModule extends AbstractAutomaticPaymentsAutomaticPixHappyCorePathTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanal();
	}

	@Override
	protected AbstractSetPaymentReference setPaymentReferenceForSecondPayment() {
		return new SetPaymentReferenceForSemanalPayment();
	}

	@Override
	protected void patchSecondPayment() {
		useClientCredentialsAccessToken();
		runInBlock("Call PATCH recurring-consent endpoint", () -> {
			call(new CallPatchAutomaticPaymentsConsentsEndpointSequence()
				.replace(CreatePatchRecurringPaymentsConsentsForRejectionRequestBody.class,
					condition(CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody.class)));
			callAndContinueOnFailure(patchConsentValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition() {
		return new EnsureThereAreOneAcscAndOneRjctPayments();
	}

	@Override
	protected void getAndValidateRecurringPaymentsEndpoint() {
		super.getAndValidateRecurringPaymentsEndpoint();
		callAndContinueOnFailure(EnsureRejectedPaymentsInPaymentsListAreForConsentimentoRevogado.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected Class<? extends Condition> patchPaymentValidator() {
		return null;
	}

	protected abstract Class<? extends Condition> patchConsentValidator();
}
