package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class SetPaymentReferenceForSemestralPayment extends AbstractSetPaymentReference {

	@Override
	protected String getPaymentReference(String dateStr) {
		LocalDate date = getPaymentDate(dateStr);
		int month = date.getMonthValue();
		int semester = (month - 1) / 6 + 1;
		int year = date.get(ChronoField.YEAR);

		return String.format("S%d-%d", semester, year);
	}
}
