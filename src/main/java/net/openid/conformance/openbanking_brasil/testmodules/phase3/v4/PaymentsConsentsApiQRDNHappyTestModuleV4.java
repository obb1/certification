package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsApiQRDNHappyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_qrdn-good-proxy_test-module_v4",
	displayName = "Payments Consents API v2 test module for QRDN local instrument with user provided details",
	summary = "Ensures payment reaches an accepted state when a valid QRDN is sent. Additionally, checks if POST consent is Rejected when QRDN is reused.\n" +
		"The Dynamic QRCode must be created by the organisation by using the PIX Tester environment and all the creditor details must be aligned with what is supplied on the config fields.\n" +
		"• Call POST Consents with user provided QRDN \n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST Payments Endpoint - Send the CNPJ Initiator provided by the user and Create the E2EID using the CNPJ Initiator\n" +
		"• Expects 201 - Validate Response\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP, or ACPD (Validate only the first call) \n" +
		"• Expects Accepted state (ACSC) - Validate Response\n" +
		"• Call POST Consent with the same QRDN\n" +
		"• Expects 422 - DETALHE_PAGAMENTO_INVALIDO\n" +
		"• Validate Error message\n" +
		"If no errors are identified:\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user\n" +
		"• Ensure an error is returned and that the authorization code was not sent back\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"REJECTED\" and rejectionReason is  QRCODE_INVALIDO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount",
		"resource.brazilQrdnPaymentConsent",
		"resource.brazilQrdnCnpj",
		"resource.transactionIdentifier"
	}
)
public class PaymentsConsentsApiQRDNHappyTestModuleV4 extends AbstractPaymentsConsentsApiQRDNHappyTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}

