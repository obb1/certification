package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractPermissionsCheckingFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForCreditCardRoot;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingBillTransactionResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardBills;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardLimits;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromDueDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.CardAccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.CardBillSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetCreditCardsAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public abstract class AbstractCreditCardApiWrongPermissionsTestModule extends AbstractPermissionsCheckingFunctionalTestModule {
	protected abstract Class<? extends Condition> getCardAccountsListValidator();
	protected abstract Class<? extends Condition> getCardAccountsTransactionsValidator();
	protected abstract Class<? extends Condition> getCardAccountsLimitsValidator();
	protected abstract Class<? extends Condition> getCardIdentificationValidator();
	protected abstract Class<? extends Condition> getCardBillsValidator();
	protected abstract Class<? extends Condition> getCardBillsTransactionsValidator();

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    protected void configureClient(){
        call(new ValidateWellKnownUriSteps());
        call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetCreditCardsAccountsV2Endpoint.class),
			condition(GetConsentV3Endpoint.class)
		)));
        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
        LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
        env.putString("fromTransactionDate", currentDate.minusDays(360).format(FORMATTER));
        env.putString("toTransactionDate", currentDate.format(FORMATTER));
        env.putString("fromDueDate", currentDate.minusDays(360).format(FORMATTER));
        env.putString("toDueDate", currentDate.format(FORMATTER));
        super.configureClient();
    }

    @Override
    protected void prepareCorrectConsents() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_CARDS_ACCOUNTS).addScopes(OPFScopesEnum.CREDIT_CARDS_ACCOUNTS, OPFScopesEnum.OPEN_ID).build();
    }

    @Override
    protected void preFetchResources() {
        callAndStopOnFailure(CardAccountSelector.class);
        callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);
        preCallProtectedResource("Fetch card bill Account V2");
        callAndStopOnFailure(PrepareUrlForFetchingCardLimits.class);
        preCallProtectedResource("Fetch CreditCard Limits V2");
        callAndStopOnFailure(PrepareUrlForFetchingCardTransactions.class);
        callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
        preCallProtectedResource("Fetch CreditCard Transactions V2");
        callAndStopOnFailure(PrepareUrlForFetchingCardBills.class);
        callAndStopOnFailure(AddToAndFromDueDateParametersToProtectedResourceUrl.class);
        preCallProtectedResource("Fetch CreditCard Bills V2");
        callAndStopOnFailure(CardBillSelector.class);
        callAndStopOnFailure(PrepareUrlForFetchingBillTransactionResource.class);
        callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
        preCallProtectedResource("Fetch CreditCard Bills Transaction V2");
    }

    @Override
    protected void prepareIncorrectPermissions() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA).build();
		}
    }

    @Override
    protected void requestResourcesWithIncorrectPermissions() {
        runInBlock("Ensure we cannot call the CreditCard Root API V2", () -> {
            callAndStopOnFailure(PrepareUrlForCreditCardRoot.class);
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(getCardAccountsListValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the CreditCard Account API V2", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(getCardIdentificationValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the CreditCard Bill API V2", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingCardBills.class);
            callAndStopOnFailure(AddToAndFromDueDateParametersToProtectedResourceUrl.class);
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(getCardBillsValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the CreditCard Bill Transaction API V2", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingBillTransactionResource.class);
            callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(getCardBillsTransactionsValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the CreditCard Limits API V2", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingCardLimits.class);
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(getCardAccountsLimitsValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });

        runInBlock("Ensure we cannot call the  CreditCard Transactions API V2", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingCardTransactions.class);
            callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndContinueOnFailure(getCardAccountsTransactionsValidator(), Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });
    }
}
