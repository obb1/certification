package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectBADPaymentType;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectInvalidPersonType;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetConsentDateInPast;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.SetInvalidPaymentCurrency;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPostConsentEndpoint422Sequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;

import java.util.Optional;

public abstract class AbstractPaymentsConsentsApiNegativeTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();


	@Override
	protected abstract Class<? extends Condition> postPaymentConsentValidator();

	@Override
	protected abstract Class<? extends Condition> postPaymentValidator();

	@Override
	protected abstract Class<? extends Condition> getPaymentConsentValidator();

	@Override
	protected abstract Class<? extends Condition> getPaymentValidator();

	@Override
	protected void configureDictInfo() {
		// Not needed in this test
	}

	@Override
	protected void createAuthorizationCodeRequest() {
		// Not needed in this test
	}

	@Override
	protected void requestAuthorizationCode() {
		// Not needed in this test
	}

	@Override
	protected void requestProtectedResource() {
		// Not needed in this test
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);

		saveBrazilPaymentConsent();

		call(exec().startBlock("Ensure error when Payment Type is \"BAD\""));
		executeUnhappyPath(SelectBADPaymentType.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when an invalid person type is sent"));
		executeUnhappyPath(SelectInvalidPersonType.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when invalid currency is sent"));
		executeUnhappyPath(SetInvalidPaymentCurrency.class, EnsureErrorResponseCodeFieldWasParametroInvalido.class);

		call(exec().startBlock("Ensure error when invalid date is sent"));
		executeUnhappyPath(SetConsentDateInPast.class, EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);

		call(exec().startBlock("Ensure x-fapi-interaction-id is validated"));
		executeUnhappyPathReplacingStep(
			CreateRandomFAPIInteractionId.class,
			CreateInvalidFAPIInteractionId.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);

		call(exec().startBlock("Ensure POST Consents is successful when valid payload is sent"));
		restoreBrazilPaymentConsent();
		performAuthorizationFlow();
	}

	protected void executeUnhappyPath(Class<? extends Condition> selectWrongType, Class<? extends Condition> ensureError) {
		restoreBrazilPaymentConsent();
		callAndStopOnFailure(selectWrongType);
		call(postConsentSequence().replace(EnsureHttpStatusCodeIs201.class, condition(EnsureConsentResponseCodeWas422.class)));
		validateErrorMessage();
		env.mapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
		callAndContinueOnFailure(ensureError, Condition.ConditionResult.FAILURE);
		env.unmapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY);
	}

	protected void executeUnhappyPathReplacingStep(
		Class<? extends Condition> originalStepConsentSequence,
		Class<? extends Condition> substituteStepConsentSequence,
		Class<? extends Condition> ensureError
	) {
		restoreBrazilPaymentConsent();
		call(postConsentUnhappySequence()
			.replace(originalStepConsentSequence, condition(substituteStepConsentSequence)));
		if(OIDFJSON.getInt(env.getObject("consent_endpoint_response_full").get("status"))==422){
			call(new CallPostConsentEndpoint422Sequence());
		}
		else {
			call(new CallPostConsentEndpoint422Sequence()
				.skip(EnsureContentTypeApplicationJwt.class, "Expecting a JSON")
				.skip(ExtractSignedJwtFromResourceResponse.class, "Expecting a JSON")
				.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, "Expecting a JSON")
				.skip(FAPIBrazilValidateResourceResponseTyp.class, "Expecting a JSON")
				.skip(ValidateResourceResponseSignature.class, "Expecting a JSON")
				.skip(ValidateResourceResponseJwtClaims.class, "Expecting a JSON"));
		}
		validateErrorMessage();
		if(OIDFJSON.getInt(env.getObject("consent_endpoint_response_full").get("status"))==422){
			env.mapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY,"consent_endpoint_response_full");
			callAndContinueOnFailure(ensureError, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY);
		}
	}

	protected void execute400UnhappyPath() {
		restoreBrazilPaymentConsent();
		call(new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
				.skip(ExtractConsentIdFromConsentEndpointResponse.class, "Not needed in this test")
				.skip(FAPIBrazilAddConsentIdToClientScope.class, "Not needed in this test")
				.replace(EnsureHttpStatusCodeIs201.class, condition(EnsureConsentResponseCodeWas400.class))
				.skip(EnsureContentTypeApplicationJwt.class, "Expecting a JSON")
				.skip(ExtractSignedJwtFromResourceResponse.class, "Expecting a JSON")
				.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, "Expecting a JSON")
				.skip(FAPIBrazilValidateResourceResponseTyp.class, "Expecting a JSON")
				.skip(ValidateResourceResponseSignature.class, "Expecting a JSON")
				.skip(ValidateResourceResponseJwtClaims.class, "Expecting a JSON")
				.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Not adding x-fapi-i-id, expecting error")
				.skip(EnsureMatchingFAPIInteractionId.class, "Not adding x-fapi-i-id, expecting error"));
		validateErrorMessage();
	}

	protected ConditionSequence postConsentSequence() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, false)
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
			sequenceOf(condition(CreateRandomFAPIInteractionId.class),
				condition(AddFAPIInteractionIdToResourceEndpointRequest.class)))
			.skip(ExtractConsentIdFromConsentEndpointResponse.class, "Not needed in this test")
			.skip(FAPIBrazilAddConsentIdToClientScope.class, "Not needed in this test")
			.insertAfter(CheckForFAPIInteractionIdInResourceResponse.class,
				condition(EnsureMatchingFAPIInteractionId.class));
	}

	protected ConditionSequence postConsentUnhappySequence() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, true, false, true);
	}

	protected void saveBrazilPaymentConsent() {
		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract brazilPaymentConsent")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsentSave", brazilPaymentConsent.deepCopy());
	}

	protected void restoreBrazilPaymentConsent() {
		JsonObject brazilPaymentConsentSave = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsentSave"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract brazilPaymentConsentSave")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsent", brazilPaymentConsentSave.deepCopy());
	}

	protected void validateErrorMessage() {
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
	}

}
