package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class SaveCurrentInstant extends AbstractCondition {

	@Override
	@PostEnvironment(strings = "saved_instant")
	public Environment evaluate(Environment env) {
		Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);
		env.putString("saved_instant", now.toString());
		logSuccess("Current instant has been saved to the environment",
			args("current instant", env.getString("saved_instant")));
		return env;
	}
}
