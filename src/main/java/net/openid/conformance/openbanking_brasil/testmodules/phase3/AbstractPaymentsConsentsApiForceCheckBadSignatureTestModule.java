package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddAudAsPaymentConsentUriToRequestObject;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIatToRequestObject;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.AddIssAsCertificateOuToRequestObject;
import net.openid.conformance.condition.client.AddJtiAsUuidToRequestObject;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentConsentRequest;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.InvalidateConsentEndpointRequestSignature;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.SignedPaymentConsentSequence;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Random;

public abstract class AbstractPaymentsConsentsApiForceCheckBadSignatureTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

    protected abstract Class<? extends Condition> paymentConsentValidator();
    protected abstract Class<? extends Condition> paymentConsentErrorValidator();

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
    }

    @Override
    protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
        return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
    }

    @Override
    protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
        callAndContinueOnFailure(SanitiseQrCodeConfig.class, Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void runTests() {
        int numOfGoodTests = new Random().nextInt(20) + 1;

        eventLog.startBlock("Making a random number of good payment consent requests");
        for(int i = 0; i < numOfGoodTests; i++){
            callAndStopOnFailure(PrepareToPostConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);
            call(sequence(SignedPaymentConsentSequence.class));
            if(i==0){
                callAndContinueOnFailure(paymentConsentValidator(), Condition.ConditionResult.FAILURE);
            }
        }

        eventLog.startBlock("Trying a badly signed payment consent request");
        makeBadlySignedPaymentConsentRequest();
    }

    protected void makeBadlySignedPaymentConsentRequest(){
        callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
        callAndStopOnFailure(CreateIdempotencyKey.class);
        callAndStopOnFailure(AddIdempotencyKeyHeader.class);
        callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
        callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
        callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
        callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
        call(exec().mapKey("request_object_claims", "consent_endpoint_request"));
        callAndStopOnFailure(AddAudAsPaymentConsentUriToRequestObject.class, "BrazilOB-6.1");
        callAndStopOnFailure(AddIssAsCertificateOuToRequestObject.class, "BrazilOB-6.1");
        callAndStopOnFailure(AddJtiAsUuidToRequestObject.class, "BrazilOB-6.1");
        callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");
        call(exec().unmapKey("request_object_claims"));
        callAndStopOnFailure(FAPIBrazilSignPaymentConsentRequest.class);
        callAndStopOnFailure(InvalidateConsentEndpointRequestSignature.class);
        callAndStopOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(EnsureConsentResponseCodeWas400.class, Condition.ConditionResult.FAILURE);
        call(exec().mapKey("endpoint_response", "consent_endpoint_response_full"));
        callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE, "BrazilOB-6.1");
        call(exec().unmapKey("endpoint_response"));
        callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
        call(exec().mapKey("server", "org_server"));
        call(exec().mapKey("server_jwks", "org_server_jwks"));
        callAndStopOnFailure(FetchServerKeys.class);
        call(exec().unmapKey("server"));
        call(exec().unmapKey("server_jwks"));
    }
}
