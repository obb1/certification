package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class PrepareToFetchNextPayment extends ResourceBuilder {

	@Override
	@PreEnvironment(required = "payment_ids")
	public Environment evaluate(Environment env) {
		JsonObject paymentIdsObj = env.getObject("payment_ids");
		JsonArray paymentIds = paymentIdsObj.getAsJsonArray("paymentIds");
		if(paymentIds.isEmpty()){
			throw error("Payments array must not be empty", args("payment_ids",paymentIdsObj));
		}

		String paymentId = OIDFJSON.getString(paymentIds.get(0));
		paymentIds.remove(0);
		env.putBoolean("no_more_payments", paymentIds.isEmpty());

		setApi("payments");
		setEndpoint("/pix/payments/" + paymentId);
		log("Prepared to fetch payment", args("paymentId", paymentId));

		return super.evaluate(env);
	}
}
