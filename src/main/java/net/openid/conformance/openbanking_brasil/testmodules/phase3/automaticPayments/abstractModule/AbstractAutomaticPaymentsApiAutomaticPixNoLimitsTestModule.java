package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixHappyCorePathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanalWithoutFirstPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToAddDefinedCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetUserDefinedCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemanalPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToTenDaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToThreeDaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereAreTwoSchdPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo1;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasSchd;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixNoLimitsTestModule extends AbstractAutomaticPaymentsAutomaticPixHappyCorePathTestModule {

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanalWithoutFirstPayment();
	}

	@Override
	protected AbstractSetPaymentReference setPaymentReferenceForSecondPayment() {
		return new SetPaymentReferenceForSemanalPayment();
	}

	@Override
	protected void configurePaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToSetExpirationTo180DaysInTheFuture.class);
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToThreeDaysInTheFuture.class);
		callAndStopOnFailure(SetPaymentReferenceForSemanalPayment.class);
		callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
	}

	@Override
	protected void configureCreditorFields() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToAddDefinedCreditor.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetUserDefinedCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj.class);
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountTo1();
	}

	@Override
	protected void validateFinalState() {
		callAndContinueOnFailure(EnsurePaymentStatusWasSchd.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void configureSecondPaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToTenDaysInTheFuture.class);
	}

	@Override
	protected AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition() {
		return new EnsureThereAreTwoSchdPayments();
	}

	@Override
	protected void patchSecondPayment() {}

	@Override
	protected Class<? extends Condition> patchPaymentValidator() {
		return null;
	}
}
