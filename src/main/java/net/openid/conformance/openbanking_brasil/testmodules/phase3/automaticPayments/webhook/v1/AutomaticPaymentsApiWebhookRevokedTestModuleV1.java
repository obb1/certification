package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.abstractModule.AbstractAutomaticPaymentsApiWebhookRevokedTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.createWebhook.CreateRecurringConsentWebhookV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PatchRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_webhook-revoked_test-module_v1",
	displayName = "automatic-payments_api_webhook-revoked_test-module_v1",
	summary = "Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a consent reaches the REVOKED state\n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;\n" +
		"• Obtain a SSA from the Directory\n" +
		"• Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;”]\n" +
		"• Expect a 201 - Validate Response\n" +
		"• Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"• Set the recurring consents webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v1/automatic-payments/v1/recurring-consents/{recurringConsentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields and a infinite consent, not sending the exspirationDateTime field\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the PATCH {recurringConsentId} endpoint with  status as REVOKED, rejectedBy is USUARIO, rejectedFrom is INICIADORA and reason.code is REVOGADO_USUARIO\n" +
		"• Expect 200 - Validate Message\n" +
		"• all the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure  status as REVOKED, rejectedBy is USUARIO, rejectedFrom is INICIADORA and reason.code is REVOGADO_USUARIO\n" +
		"• Expect an incoming message, the defined consents endpoint, which  must be mtls protected - Wait 60 seconds for the messages to be returned\n" +
		"• For the webhook calls - Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within now and the start of the test\n" +
		"• Call the Delete Registration Endpoint\n" +
		"• Expect a 204 - No Content\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.creditorName",
		"resource.webhookWaitTime",
		"directory.client_id",
		"directory.discoveryUrl"
	}
)
public class AutomaticPaymentsApiWebhookRevokedTestModuleV1 extends AbstractAutomaticPaymentsApiWebhookRevokedTestModule {

	@Override
	protected ConditionSequence getUrlsFromAuthServerCondition() {
		return sequenceOf(
			condition(GetAutomaticPaymentRecurringConsentV1Endpoint.class),
			condition(GetAutomaticPaymentPixRecurringV1Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> setPaymentConsentWebhookCreator() {
		return CreateRecurringConsentWebhookV1.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PostRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentConsentValidator() {
		return PatchRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return false;
	}
}
