package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetFromAndToTransactionConversionDateToSavedTransactionConversionDate extends AbstractCondition {

    @Override
    @PreEnvironment(strings = "transactionConversionDate")
    @PostEnvironment(strings = {"fromTransactionConversionDate", "toTransactionConversionDate"})
    public Environment evaluate(Environment env) {
        String transactionConversionDate = env.getString("transactionConversionDate");
        env.putString("fromTransactionConversionDate", transactionConversionDate);
        env.putString("toTransactionConversionDate", transactionConversionDate);
        logSuccess("Transaction conversion date query parameters have been set to the saved value", args(
            "fromTransactionConversionDate", env.getString("fromTransactionConversionDate"),
            "toTransactionConversionDate", env.getString("toTransactionConversionDate")
        ));
        return env;
    }
}
