package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"resource", "recurring_configuration_object"})
	public Environment evaluate(Environment env) {
		JsonObject recurringConfigObj = env.getObject("recurring_configuration_object");
		JsonObject creditorAccount = Optional.ofNullable(recurringConfigObj.getAsJsonObject("automatic"))
			.map(automatic -> automatic.getAsJsonObject("firstPayment"))
			.map(firstPayment -> firstPayment.getAsJsonObject("creditorAccount"))
			.orElseThrow(() -> error("Unable to find firstPayment creditorAccount inside automatic recurring configuration object",
				args("recurring configuration object", recurringConfigObj)));

		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));
		data.add("creditorAccount", creditorAccount.deepCopy());

		logSuccess("The creditorAccount field has been set in the recurring payments request body according to its values in firstPayment from consent request",
			args("data", data));

		return env;
	}
}
