package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditEnrollmentsRequestBodyToChangeDebtorAccountNumber extends AbstractCondition {

	private static final int MAX_LENGTH = 20;
	private static final String DUMMY_NUMBER = "1234";

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	@PostEnvironment(strings = "debtor_account_number")
	public Environment evaluate(Environment env) {
		JsonObject debtorAccount = Optional.ofNullable(
			env.getElementFromObject("resource_request_entity_claims", "data.debtorAccount")
		).orElseThrow(() -> error("Could not find debtorAccount inside the body")).getAsJsonObject();

		String oldNumber = Optional.ofNullable(debtorAccount.get("number"))
			.map(OIDFJSON::getString)
			.orElse(DUMMY_NUMBER);

		String newNumber = generateNewNumber(oldNumber);
		debtorAccount.addProperty("number", newNumber);

		env.putString("debtor_account_number", newNumber);

		logSuccess("Successfully edited request body to change debtorAccount.number",
			args("request body", env.getObject("resource_request_entity_claims")));

		return env;
	}

	private String generateNewNumber(String oldNumber) {
		if (oldNumber.length() < MAX_LENGTH) {
			return oldNumber + "0";
		}
		return DUMMY_NUMBER;
	}
}
