package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public abstract class AbstractGetEndpointFromAuthServer extends AbstractCondition {

	protected enum PaymentType {
		PAYMENTS,
		AUTOMATIC_PAYMENTS
	}

	protected String getApiEndpoint(Environment env, String apiFamilyType, String apiVersionRegex, String endpointRegex) {

		JsonArray apiResources = Optional.ofNullable(env.getObject("authorisation_server"))
			.map(server -> server.getAsJsonArray("ApiResources"))
			.orElseThrow(() -> error("Authorisation Servers does not have any endpoints available"));

		for (JsonElement entry : apiResources) {
			JsonObject apiResource = entry.getAsJsonObject();
			if (OIDFJSON.getString(apiResource.get("ApiFamilyType")).equals(apiFamilyType) && OIDFJSON.getString(apiResource.get("ApiVersion")).matches(apiVersionRegex)) {
				JsonArray apiDiscoveryEndpoints = apiResource.getAsJsonArray("ApiDiscoveryEndpoints");
				for (JsonElement apiEndpoint : apiDiscoveryEndpoints) {
					JsonObject apiEndpointObject = apiEndpoint.getAsJsonObject();
					String endpointString = OIDFJSON.getString(apiEndpointObject.get("ApiEndpoint"));
					if (endpointString.matches(endpointRegex)) {
						return endpointString;
					}
				}
			}
		}
		return null;
	}

	protected void buildResourcesConfigResourceUrlFromConsentUrl(Environment env, String apiReplacement, String endpointReplacement, String resourceVersion) {
		String consentUrl = env.getString("config", "resource.consentUrl");

		if (Strings.isNullOrEmpty(consentUrl)) {
			throw error("Consent URL not found in configuration");
		}

		validateConsentAgainstRegex(consentUrl, "^(https:\\/\\/)(.*?)(\\/open-banking\\/consents\\/v\\d+\\/consents)$");
		String resourceUrl = consentUrl.replaceFirst("consents", apiReplacement).replaceFirst("consents", endpointReplacement);


		if (resourceVersion != null) {
			resourceUrl = resourceUrl.replaceFirst("/v\\d+/", "/v" + resourceVersion + "/");
		}

		env.putString("config", "resource.resourceUrl", resourceUrl);
		logSuccess(String.format("resourceUrl for %s set up", apiReplacement), args("resourceUrl", resourceUrl));


	}


	protected void buildResourcesConfigPaymentUrlFromConsentUrl(Environment env, PaymentType paymentType) {
		String consentUrl = env.getString("config", "resource.consentUrl");

		if (Strings.isNullOrEmpty(consentUrl)) {
			throw error("Consent URL not found in configuration");
		}

		String resourceUrl = "";

		switch (paymentType) {
			case PAYMENTS:
				validateConsentAgainstRegex(consentUrl, "^(https:\\/\\/)(.*?)(\\/open-banking\\/payments\\/v\\d+\\/consents)$");
				resourceUrl = consentUrl.replaceFirst("consents", "pix/payments");
				break;
			case AUTOMATIC_PAYMENTS:
				validateConsentAgainstRegex(consentUrl, "^(https:\\/\\/)(.*?)(\\/open-banking\\/automatic-payments\\/v\\d+\\/recurring-consents)$");
				resourceUrl = consentUrl.replaceFirst("recurring-consents", "pix/recurring-payments");
				break;
		}

		env.putString("config", "resource.resourceUrl", resourceUrl);
		logSuccess(String.format("resourceUrl for %s set up", paymentType), args("resourceUrl", resourceUrl));


	}

	private void validateConsentAgainstRegex(String consentUrl, String validatorRegex) {
		if (!consentUrl.matches(validatorRegex)) {
			throw error("consentUrl is not valid, please ensure that url matches regex", args("consentUrl", consentUrl, "regex", validatorRegex));
		}
	}
}
