package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentRejectionReasonEnum;

public class EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoAccountHolderValidation extends AbstractEnsureEnrollmentRejectionReasonWas {

	@Override
	protected String getExpectedRejectionReason() {
		return EnrollmentRejectionReasonEnum.REJEITADO_TEMPO_EXPIRADO_ACCOUNT_HOLDER_VALIDATION.toString();
	}
}
