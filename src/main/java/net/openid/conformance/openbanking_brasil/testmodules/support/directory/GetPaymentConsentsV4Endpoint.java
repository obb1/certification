package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetPaymentConsentsV4Endpoint extends GetPaymentConsentsVXEndpoint {
	@Override
	protected String getApiVersionRegex() {
		return "^(4.[0-9].[0-9])$";
	}
}
