package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiPaymentsKeysSwapTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults.AbstractEditRequestBodyToAddClientExtensionResults;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.GetEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoRegistrationOptionsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoSignOptionsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "enrollments_api_payments-keys-swap_test-module_v1",
	displayName = "enrollments_api_payments-keys-swap_test-module_v1",
	summary = "Ensure a consent is not authorized if the message is signed with a different private key\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Execute a full enrollment journey sending the origin extracted above, extract the enrollment ID, and store the refresh_token generated ensuring it has the nrp-consents scope\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AUTHORISED\"\n" +
		"• Call POST consent with valid payload\n" +
		"• Expects 201 - Validate response\n" +
		"• Call POST consent authorise with valid payload, signing the challenge with a private key not related to the public key registered during the enrollment process\n" +
		"• Expects 422 RISCO - Validate error response\n" +
		"• Call GET Consent Endpoint\n" +
		"• Expects 200 - Validate if status is REJECTED, and rejectionReason is NAO_INFORMADO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class EnrollmentsApiPaymentsKeysSwapTestModuleV1 extends AbstractEnrollmentsApiPaymentsKeysSwapTestModule {

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postFidoSignOptionsValidator() {
		return PostFidoSignOptionsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class), condition(GetEnrollmentsV1Endpoint.class));
	}

	@Override
	protected void generateClientExtensionResults() {}

	@Override
	protected Class<? extends AbstractEditRequestBodyToAddClientExtensionResults> editFidoRegistrationRequestBody() {
		return null;
	}

	@Override
	protected Class<? extends AbstractEditRequestBodyToAddClientExtensionResults> editConsentsAuthoriseRequestBody() {
		return null;
	}

	@Override
	public void cleanup() {
		//not required
	}
}
