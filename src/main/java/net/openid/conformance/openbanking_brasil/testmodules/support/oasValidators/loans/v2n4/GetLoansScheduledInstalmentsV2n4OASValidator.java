package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.AbstractGetLoansScheduledInstalmentsOASValidator;


public class GetLoansScheduledInstalmentsV2n4OASValidator extends AbstractGetLoansScheduledInstalmentsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/loans/loans-v2.4.0.yml";
	}
}
