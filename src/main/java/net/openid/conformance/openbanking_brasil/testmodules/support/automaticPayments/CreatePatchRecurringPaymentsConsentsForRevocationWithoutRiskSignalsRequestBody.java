package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevocationReasonEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedByEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedFromEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsStatusEnum;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class CreatePatchRecurringPaymentsConsentsForRevocationWithoutRiskSignalsRequestBody extends AbstractCondition {

	@Override
	@PostEnvironment(required = "consent_endpoint_request")
	public Environment evaluate(Environment env) {
		JsonObject consentRequestBody = new JsonObjectBuilder()
			.addField("data.status", RecurringPaymentsConsentsStatusEnum.REVOKED.toString())
			.addFields("data.revocation", Map.of(
				"revokedBy", RecurringPaymentsConsentsRevokedByEnum.USUARIO.toString(),
				"revokedFrom", RecurringPaymentsConsentsRevokedFromEnum.INICIADORA.toString()
			))
			.addFields("data.revocation.reason", Map.of(
				"code", RecurringPaymentsConsentsRevocationReasonEnum.REVOGADO_USUARIO.toString(),
				"detail", RecurringPaymentsConsentsRevocationReasonEnum.REVOGADO_USUARIO.toString()
			))
			.build();
		env.putObject("consent_endpoint_request", consentRequestBody);
		logSuccess("Patch request body for recurring payments consents created", args("body", consentRequestBody));
		return env;
	}
}
