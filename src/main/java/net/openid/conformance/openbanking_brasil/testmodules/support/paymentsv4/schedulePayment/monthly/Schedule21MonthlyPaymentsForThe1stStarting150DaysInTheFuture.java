package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly;

public class Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture extends AbstractScheduleMonthlyPayment {

	@Override
	protected int getDayOfMonth() {
		return 1;
	}

	@Override
	protected int getNumberOfDaysToAddToCurrentDate() {
		return 150;
	}

	@Override
	protected int getQuantity() {
		return 21;
	}
}
