package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureQRCodePresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PaymentConsentErrorTestingSequence;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentsConsentsApiEnforceDICTTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

    protected abstract Class<? extends Condition> paymentConsentErrorValidator();

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
    }

    @Override
    protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        eventLog.startBlock("Setting date to today");
        callAndContinueOnFailure(SelectDICTCodeLocalInstrument.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(EnsureQRCodePresentInConfig.class, Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(EnsureProxyPresentInConfig.class);
    }

    @Override
    protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
        return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
    }

    @Override
    protected void runTests() {
        runInBlock("Validate payment initiation consent", () -> {
            callAndStopOnFailure(PrepareToPostConsentRequest.class);
            callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

            call(sequence(PaymentConsentErrorTestingSequence.class));
            callAndStopOnFailure(EnsureConsentResponseCodeWas422.class);

            callAndStopOnFailure(paymentConsentErrorValidator());
            callAndStopOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class);
        });
    }
}
