package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums.PaymentConsentStatusEnumV4;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class EnsureExpirationDateTime extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		try {
			JsonObject body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);

			String status = OIDFJSON.getString(
				Optional.ofNullable(body.getAsJsonObject("data").get("status"))
					.orElseThrow(() -> error("Body does not have data.status field", args("body", body)))
			);
			if (!status.equals(PaymentConsentStatusEnumV4.PARTIALLY_ACCEPTED.toString())) {
				log("Payment status not PARTIALLY_ACCEPTED");
				return env;
			}

			String expirationDateTime = OIDFJSON.getString(body.getAsJsonObject("data").get("expirationDateTime"));
			Instant instantExpirationDateTime = Instant.parse(expirationDateTime).truncatedTo(ChronoUnit.DAYS);

			JsonObject paymentObject = body.getAsJsonObject("data").get("payment").getAsJsonObject();

			if (!paymentObject.has("schedule")){
				log("Payment has no schedule object");
				return env;
			}
			JsonObject schedule = paymentObject.getAsJsonObject("schedule");
			String scheduleType = getScheduleType(schedule);
			String date;
			if(schedule.has("single")) {
				date = OIDFJSON.getString(paymentObject.getAsJsonObject("schedule").get(scheduleType).getAsJsonObject().get("date"));
			} else if (schedule.has("custom")) {
				date = OIDFJSON.getString(paymentObject.getAsJsonObject("schedule").get(scheduleType).getAsJsonObject().get("dates").getAsJsonArray().get(0));
			} else {
				date = OIDFJSON.getString(paymentObject.getAsJsonObject("schedule").get(scheduleType).getAsJsonObject().get("startDate"));
			}
			date = date + "T00:00:00Z";
			Instant instantDate = Instant.parse(date);
			if(instantExpirationDateTime.isAfter(instantDate)){
				if(schedule.has("single")){
					throw error("expirationDateTime is after date");
				} else {
					throw error("expirationDateTime is after startDate");
				}
			}
			return env;

		} catch (ParseException e) {
			throw error("Could not parse body");
		}
	}

	private String getScheduleType(JsonObject scheduleObject){
		if(scheduleObject.has("single")){ return "single";}
		if(scheduleObject.has("daily")){ return "daily";}
		if(scheduleObject.has("weekly")){ return "weekly";}
		if(scheduleObject.has("monthly")){ return "monthly";}
		if(scheduleObject.has("custom")){ return "custom";}
		throw error("Payment schedule is empty or incorrect");
	}
}
