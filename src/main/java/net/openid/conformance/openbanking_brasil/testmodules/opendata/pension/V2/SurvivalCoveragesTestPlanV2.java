package net.openid.conformance.openbanking_brasil.testmodules.opendata.pension.V2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.opendata.utils.PrepareToGetOpenDataApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.pension.v2.GetSurvivalCoveragesOASValidatorV2;
import net.openid.conformance.openinsurance.testplan.utils.CallNoCacheResource;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "opendata-pension-survival-coverages_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.PENSION_SURVIVAL_COVERAGES_API_PHASE_4A_TEST_PLAN_V2,
	summary = PlanNames.PENSION_SURVIVAL_COVERAGES_API_PHASE_4A_TEST_PLAN_V2
)
public class SurvivalCoveragesTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(net.openid.conformance.openbanking_brasil.testmodules.opendata.pension.V2.SurvivalCoveragesTestPlanV2.SurvivalCoveragesTestModuleV2.class),
				List.of(new Variant(ClientAuthType.class, "none"))
			)
		);
	}

	@PublishTestModule(
		testName = "opendata-pension-survival-coverages_api_structural_test-module_v2",
		displayName = "Validate structure of Pension Survival Coverages response",
		summary = "Validate structure of Pension Survival Coverages response",
		profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4
	)
	public static class SurvivalCoveragesTestModuleV2 extends AbstractNoAuthFunctionalTestModule {

		@Override
		protected void runTests() {
			runInBlock("Validate Pension Survival Coverages response", () -> {
				callAndStopOnFailure(PrepareToGetOpenDataApi.class);
				callAndStopOnFailure(CallNoCacheResource.class);
				callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(GetSurvivalCoveragesOASValidatorV2.class, Condition.ConditionResult.FAILURE);
			});
		}
	}
}
