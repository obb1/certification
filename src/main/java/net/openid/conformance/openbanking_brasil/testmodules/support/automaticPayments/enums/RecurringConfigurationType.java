package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums;

public enum RecurringConfigurationType {
	AUTOMATIC, SWEEPING, VRP
}
