package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreatePatchEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas.EnsureEnrollmentCancelledFromWasIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas.EnsureEnrollmentRejectionReasonWasRejeitadoManualmente;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasStatusVinculoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareRejectionOrRevocationReasonForPatchRequest.PrepareRejectionReasonRejeitadoManualmenteForPatchRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPatchEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistrationOptions;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractEnrollmentsApiRejectedPostAuthorisationEnrollmentTestModule extends AbstractEnrollmentsApiTestModule {

    @Override
    protected void executeTestSteps() {
        runInBlock("Call PATCH request to enrollments endpoint - Expects 204", () -> {
			useClientCredentialsAccessToken();
			callAndStopOnFailure(PrepareRejectionReasonRejeitadoManualmenteForPatchRequest.class);
			PostEnrollmentsResourceSteps patchEnrollmentsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPatchEnrollments(),
				CreatePatchEnrollmentsRequestBodyToRequestEntityClaims.class,
				true
			);
			call(patchEnrollmentsSteps);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
			userAuthorisationCodeAccessToken();
        });

		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
        runInBlock("Validate enrollments fields related to cancellation", () -> {
            callAndContinueOnFailure(EnsureEnrollmentCancelledFromWasIniciadora.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(EnsureEnrollmentRejectionReasonWasRejeitadoManualmente.class, Condition.ConditionResult.FAILURE);
        });

        runInBlock("Call POST fido-registration-options - Expects 401 or 422 STATUS_VINCULO_INVALIDO", () -> {
			userAuthorisationCodeAccessToken();
			PostEnrollmentsResourceSteps fidoRegistrationOptionsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPostFidoRegistrationOptions(),
				CreateEnrollmentsFidoRegistrationOptionsRequestBody.class,
				true
			);
			call(fidoRegistrationOptionsSteps);
            callAndContinueOnFailure(EnsureResourceResponseCodeWas401or422.class, Condition.ConditionResult.FAILURE);

			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
			if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				env.mapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.class, Condition.ConditionResult.FAILURE);
				env.unmapKey(EnsureErrorResponseCodeFieldWasStatusVinculoInvalido.RESPONSE_ENV_KEY);
			}

            callAndContinueOnFailure(postFidoRegistrationOptionsValidator(), Condition.ConditionResult.FAILURE);
        });
    }

    protected abstract Class<? extends Condition> postFidoRegistrationOptionsValidator();
}
