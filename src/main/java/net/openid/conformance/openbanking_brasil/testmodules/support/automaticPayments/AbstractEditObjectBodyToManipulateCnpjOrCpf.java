package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.condition.AbstractCondition;

import java.util.Random;

public abstract class AbstractEditObjectBodyToManipulateCnpjOrCpf extends AbstractCondition {

	protected static final int BASE_DIGITS = 8;
	protected static final int CPF_DIGITS = 11;
	protected static final int CNPJ_DIGITS = 14;

	protected String generateNewValidCpfFromDifferentRegion(String oldCpf) {
		char regionalDigit = oldCpf.charAt(8);
		char newRegionalDigit = addOneToNumericChar(regionalDigit);
		String cpfBase = generateRandomBase() + newRegionalDigit;

		return generateCpfWithVerifierDigits(cpfBase);
	}

	protected char addOneToNumericChar(char digitChar) {
		int digit = Character.getNumericValue(digitChar);
		digit = (digit + 1) % 10;
		return (char) (digit + '0');
	}

	protected String generateRandomBase() {
		Random random = new Random();
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < BASE_DIGITS; i++) {
			sb.append(random.nextInt(10));
		}

		return sb.toString();
	}

	/**
	 * Generates a new CPF with verifier digits according to the algorithm described in the
	 * <a href="https://pt.wikipedia.org/wiki/Cadastro_de_Pessoas_F%C3%ADsicas#Algoritmo">CPF Wikipedia page</a>.
	 *
	 * @param cpfBase The base CPF without verifier digits.
	 * @return A complete CPF with verifier digits.
	 */
	protected String generateCpfWithVerifierDigits(String cpfBase) {
		int v1 = 0;
		int v2 = 0;

		for (int i = 0; i < cpfBase.length(); i++) {
			int currentDigit = Character.getNumericValue(cpfBase.charAt(cpfBase.length() - 1 - i));
			v1 += currentDigit * (9 - (i % 10));
			v2 += currentDigit * (9 - ((i + 1) % 10));
		}

		v1 = (v1 % 11) % 10;
		v2 += v1 * 9;
		v2 = (v2 % 11) % 10;

		return cpfBase + v1 + v2;
	}

	/**
	 * Generates a new random CNPJ with verifier digits.
	 *
	 * @return A complete CNPJ with verifier digits.
	 */
	protected String generateNewRandomCnpj() {
		String cnpjBase = generateRandomBase() + "0001";
		return generateFullCnpjWithVerifierDigits(cnpjBase);
	}

	/**
	 * Generates a full CNPJ with verifier digits based on the given base according to the algorithm described in the
	 * <a href="https://pt.wikipedia.org/wiki/Cadastro_Nacional_da_Pessoa_Jur%C3%ADdica#Pseudoc%C3%B3digo">CNPJ Wikipedia page</a>.
	 *
	 * @param cnpjBase The base CNPJ without verifier digits.
	 * @return A complete CNPJ with verifier digits.
	 */
	protected String generateFullCnpjWithVerifierDigits(String cnpjBase) {
		int[] cnpjDigits = new int[cnpjBase.length()];
		for (int i = 0; i < cnpjDigits.length; i++) {
			cnpjDigits[i] = Character.getNumericValue(cnpjBase.charAt(i));
		}

		int v1 = 5*cnpjDigits[0] + 4*cnpjDigits[1]  + 3*cnpjDigits[2]  + 2*cnpjDigits[3];
		v1 += 9*cnpjDigits[4] + 8*cnpjDigits[5] + 7*cnpjDigits[6] + 6*cnpjDigits[7];
		v1 += 5*cnpjDigits[8] + 4*cnpjDigits[9] + 3*cnpjDigits[10] + 2*cnpjDigits[11];
		v1 = 11 - (v1 % 11);
		if (v1 >= 10) {
			v1 = 0;
		}

		int v2 = 6*cnpjDigits[0] + 5*cnpjDigits[1]  + 4*cnpjDigits[2]  + 3*cnpjDigits[3];
		v2 += 2*cnpjDigits[4] + 9*cnpjDigits[5]  + 8*cnpjDigits[6]  + 7*cnpjDigits[7];
		v2 += 6*cnpjDigits[8] + 5*cnpjDigits[9] + 4*cnpjDigits[10] + 3*cnpjDigits[11];
		v2 += 2*v1;
		v2 = 11 - (v2 % 11);
		if (v2 >= 10) {
			v2 = 0;
		}

		return cnpjBase + v1 + v2;
	}
}
