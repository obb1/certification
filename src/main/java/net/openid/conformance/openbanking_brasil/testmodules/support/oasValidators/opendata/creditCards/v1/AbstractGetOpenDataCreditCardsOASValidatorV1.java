package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.creditCards.v1;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractGetOpenDataCreditCardsOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	protected abstract String apiPrefix();

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-opendata-credit-cards-v1.0.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return String.format("/%s-credit-cards", apiPrefix());
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray data = body.getAsJsonArray("data");
		for (JsonElement element : data) {
			JsonObject obj = element.getAsJsonObject();
			assertAdditionalInfoConstraint(obj, "identification.product", "type", "OUTROS");
			assertAdditionalInfoConstraint(obj, "identification.creditCard", "network", "OUTRAS");
			assertCodeAdditionalInfoConstraint(obj);
		}
	}

	protected void assertAdditionalInfoConstraint(JsonObject obj, String path, String field, String fieldValue) {
		assertField1IsRequiredWhenField2HasValue2(getObjectFromPath(obj, path), field + "AdditionalInfo", field, fieldValue);
	}

	protected void assertCodeAdditionalInfoConstraint(JsonObject obj) {
		JsonArray otherCredits = obj
			.getAsJsonObject("interest")
			.getAsJsonArray("otherCredits");
		for (JsonElement otherCreditEl : otherCredits) {
			JsonObject otherCredit = otherCreditEl.getAsJsonObject();
			assertField1IsRequiredWhenField2HasValue2(otherCredit, "codeAdditionalInfo", "code", "OUTROS");
		}
	}

	protected JsonObject getObjectFromPath(JsonObject obj, String path) {
		String[] keys = path.split("\\.");
		for (String key : keys) {
			obj = obj.getAsJsonObject(key);
		}
		return obj;
	}
}
