package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddClientIdToTokenEndpointRequest;
import net.openid.conformance.condition.client.AddMTLSEndpointAliasesToEnvironment;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckForAccessTokenValue;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractDirectoryConfiguration;
import net.openid.conformance.condition.client.FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken;
import net.openid.conformance.condition.client.FAPIBrazilCheckDirectoryApiBase;
import net.openid.conformance.condition.client.FAPIBrazilCheckDirectoryDiscoveryUrl;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.condition.client.GetDynamicServerConfiguration;
import net.openid.conformance.condition.client.SetDirectorySoftwareScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractFirstUriFromSSASoftwareOriginUris;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class ExtractSoftwareOriginUriFromDirectorySSASteps extends AbstractConditionSequence {

	@Override
	public void evaluate() {
		callAndStopOnFailure(ExtractDirectoryConfiguration.class);
		callAndContinueOnFailure(FAPIBrazilCheckDirectoryDiscoveryUrl.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");
		callAndContinueOnFailure(FAPIBrazilCheckDirectoryApiBase.class, Condition.ConditionResult.FAILURE, "BrazilOBDCR-7.1-1");
		call(exec().mapKey("config", "directory_config"));
		call(exec().mapKey("server", "directory_server"));
		call(exec().mapKey("client", "directory_client"));
		call(exec().mapKey("access_token", "directory_access_token"));
		call(exec().mapKey("discovery_endpoint_response", "directory_discovery_endpoint_response"));
		callAndStopOnFailure(GetDynamicServerConfiguration.class);
		callAndContinueOnFailure(AddMTLSEndpointAliasesToEnvironment.class, Condition.ConditionResult.FAILURE, "RFC8705-5");
		callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
		callAndStopOnFailure(SetDirectorySoftwareScopeOnTokenEndpointRequest.class);
		callAndStopOnFailure(AddClientIdToTokenEndpointRequest.class);
		callAndStopOnFailure(CallTokenEndpoint.class);
		callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
		callAndStopOnFailure(CheckForAccessTokenValue.class);
		callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);
		call(exec().unmapKey("config"));
		call(exec().unmapKey("server"));
		call(exec().unmapKey("client"));
		call(exec().unmapKey("access_token"));
		call(exec().unmapKey("discovery_endpoint_response"));
		callAndContinueOnFailure(AddMTLSEndpointAliasesToEnvironment.class, Condition.ConditionResult.FAILURE, "RFC8705-5");
		callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
		callAndStopOnFailure(FAPIBrazilCallDirectorySoftwareStatementEndpointWithBearerToken.class);
		callAndStopOnFailure(ExtractFirstUriFromSSASoftwareOriginUris.class);
	}
}
