package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditFidoRegistrationOptionsRequestBodyToSetInvalidRp extends AbstractCondition {

	private final static String INVALID_RP = "www.fakedomain.com";

	@Override
	@PreEnvironment(required = "resource_request_entity_claims", strings = "rp_id")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(
			env.getElementFromObject("resource_request_entity_claims", "data")
		).orElseThrow(() -> error("Could not find data inside the body")).getAsJsonObject();

		data.addProperty("rp", INVALID_RP);
		env.putString("rp_id", INVALID_RP);

		logSuccess("Successfully edited request body to include invalid rp",
			args("request body", env.getObject("resource_request_entity_claims")));

		return env;
	}
}
