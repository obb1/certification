package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.webauthn4j.data.attestation.authenticator.COSEKey;
import com.webauthn4j.data.attestation.statement.COSEAlgorithmIdentifier;
import com.webauthn4j.data.attestation.statement.COSEKeyOperation;
import com.webauthn4j.data.attestation.statement.COSEKeyType;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.List;


public class InvalidPublicCOSEKey implements COSEKey {

	private static final long serialVersionUID = 1234567L;


	@Override
	public boolean hasPublicKey() {
		return true;
	}

	@Override
	public boolean hasPrivateKey() {
		return false;
	}

	@Override
	public @Nullable PublicKey getPublicKey() {
		return new RSAPublicKey() {
			@Override
			public BigInteger getPublicExponent() {
				return new BigInteger("INVALID_PUBLIC_KEY".getBytes());
			}

			@Override
			public String getAlgorithm() {
				return "RSA";
			}

			@Override
			public String getFormat() {
				return "PKCS#8";
			}

			@Override
			public byte[] getEncoded() {
				return "INVALID_PUBLIC_KEY".getBytes();
			}

			@Override
			public BigInteger getModulus() {
				return new BigInteger("INVALID_PUBLIC_KEY".getBytes());
			}
		};
	}

	@Override
	public @Nullable PrivateKey getPrivateKey() {
		return null;
	}

	@Override
	public @Nullable COSEKeyType getKeyType() {
		return COSEKeyType.RSA;
	}

	@Override
	public @Nullable byte[] getKeyId() {
		return null;
	}

	@Override
	public @Nullable COSEAlgorithmIdentifier getAlgorithm() {
		return COSEAlgorithmIdentifier.RS256;
	}

	@Override
	public @Nullable List<COSEKeyOperation> getKeyOps() {
		return null;
	}

	@Override
	public @Nullable byte[] getBaseIV() {
		return null;
	}

	@Override
	public void validate() {

	}

}
