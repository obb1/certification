package net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic;

import com.google.gson.JsonObject;

public class AssertLinksLogicSecondPageWithMorePages extends AbstractAssertLinksLogic {

	@Override
	protected void validateLinksLogic(JsonObject links) {
		String prev = extractMandatoryLink(links, "prev", "if it is not the first page");
		String self = extractMandatoryLink(links, "self", "for any response");
		String next = extractMandatoryLink(links, "next", "if it is not the last page");

		validateSuccessiveLinks(prev, "prev", self, "self");
		validateSuccessiveLinks(self, "self", next, "next");
	}
}
