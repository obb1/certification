package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensurePaymentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3.enums.PaymentRejectionReasonEnumV3;

import java.util.List;

public class EnsurePaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento extends AbstractEnsurePaymentRejectionReasonCodeWasXV3 {

    @Override
    protected List<String> getExpectedRejectionCodes() {
        return List.of(PaymentRejectionReasonEnumV3.PAGAMENTO_DIVERGENTE_CONSENTIMENTO.toString());
    }
}
