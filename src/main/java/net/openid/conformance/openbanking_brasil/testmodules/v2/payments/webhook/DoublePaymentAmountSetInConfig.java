package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;


public class DoublePaymentAmountSetInConfig extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonElement data = env.getElementFromObject("resource", "brazilPaymentConsent.data");
		String paymentAmount;
		JsonObject payment;
		if(data.isJsonArray()){
			paymentAmount= OIDFJSON.getString(data.getAsJsonArray().get(0).getAsJsonObject().getAsJsonObject("payment").get("amount"));
			payment = Optional.ofNullable(data.getAsJsonArray().get(0).getAsJsonObject().getAsJsonObject("payment"))
				.orElseThrow(() ->error("Could not extract brazilPaymentConsent.data.payment"));

		} else {
			paymentAmount = getStringFromEnvironment(env, "resource", "brazilPaymentConsent.data.payment.amount", "Payment amount from config");
			payment = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent.data.payment"))
				.orElseThrow(() -> error("Could not extract brazilPaymentConsent.data.payment")).getAsJsonObject();
		}
		double amount = Double.parseDouble(paymentAmount);
		double newAmount = amount * 2;
		String formattedAmount = String.format("%.2f", newAmount);
		payment.addProperty("amount", formattedAmount);
		if(data.isJsonArray()){
			data.getAsJsonArray().get(0).getAsJsonObject().add("payment",payment);
		} else {
			env.putObject("resource", "brazilPaymentConsent.data.payment", payment);
		}
		JsonElement pixData = env.getElementFromObject("resource", "brazilPixPayment.data");
		if(pixData.isJsonArray()){
			env.getElementFromObject("resource", "brazilPixPayment.data").getAsJsonArray().get(0).getAsJsonObject().add("payment",payment);
		}
		else
		{
			env.putObject("resource", "brazilPixPayment.data.payment", payment);
		}
		logSuccess(String.format("new amount of %s added to %s", formattedAmount, "brazilPaymentConsent"));
		return env;
	}
}
