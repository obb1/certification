package net.openid.conformance.openbanking_brasil.testmodules.v3.consents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.AbstractConsentsApiConsentExpiredTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_expired-consent_test-module_v3",
	displayName = "Validate that consents can expire",
	summary = "Consent will be created with a 3-minute expiry. The user will be sent to the authorization endpoint before the consent has expired. Conformance Suite will call the Consents API after the consent has expired to make sure the consent is marked as CONSENT_MAX_DATE_REACHED.\n" +
		"\u2022 Creates a Consent with all of the existing permissions and set expiration to be of 3 minutes\n" +
		"\u2022 Checks all of the fields sent on the consent API are specification compliant\n" +
		"\u2022 Expects a valid consent creation 201\n" +
		"\u2022 Redirects the User - He should accept the Consent \n" +
		"\u2022 Conformance Suite Will be set to sleep for 3 minutes \n" +
		"\u2022 Call the Consents API for the authorized ConsentID \n" +
		"\u2022 Expect a success 200 - Make sure Status is set to REJECTED. Make Sure RejectedBy is set to ASPSP. Make sure Reason is set to \"CONSENT_MAX_DATE_REACHED\"\n" +
		"\u2022 Calls the DELETE Consents Endpoints \n" +
		"\u2022 Expects 422 CONSENTIMENTO_EM_STATUS_REJEITADO - Validate Error Message",

	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ConsentsApiConsentExpiredTestModuleV3 extends AbstractConsentsApiConsentExpiredTestModule {

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3.class;
	}

}
