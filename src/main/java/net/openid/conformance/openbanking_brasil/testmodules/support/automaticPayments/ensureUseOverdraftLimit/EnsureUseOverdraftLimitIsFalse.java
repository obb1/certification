package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureUseOverdraftLimit;

public class EnsureUseOverdraftLimitIsFalse extends AbstractEnsureUseOverdraftLimit {

	@Override
	protected boolean expectedUseOverdraftLimitValue() {
		return false;
	}
}
