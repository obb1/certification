package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily;

public class Schedule61DailyPaymentsStarting10DaysInTheFuture extends AbstractScheduleDailyPayment {

	@Override
	protected int getNumberOfDaysToAddToCurrentDate() {
		return 10;
	}

	@Override
	protected int getQuantity() {
		return 61;
	}
}
