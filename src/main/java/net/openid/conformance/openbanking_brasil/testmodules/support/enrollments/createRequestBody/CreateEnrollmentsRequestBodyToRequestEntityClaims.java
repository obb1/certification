package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;
import java.util.Optional;

public class CreateEnrollmentsRequestBodyToRequestEntityClaims extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config", strings = "enrollment_permissions")
    @PostEnvironment(required = "resource_request_entity_claims")
    public Environment evaluate(Environment env) {
        String loggedUserIdentification = extractFromConfigOrDie(env, "resource.loggedUserIdentification");
        String debtorAccountIspb = extractFromConfigOrDie(env, "resource.debtorAccountIspb");
        String debtorAccountIssuer = extractFromConfigOrDie(env, "resource.debtorAccountIssuer");
        String debtorAccountNumber = extractFromConfigOrDie(env, "resource.debtorAccountNumber");
        String debtorAccountType = extractFromConfigOrDie(env, "resource.debtorAccountType");
        var businessEntityIdentification = Optional.ofNullable(env.getString("config", "resource.businessEntityIdentification"));

        String[] permissions = env.getString("enrollment_permissions").split("\\W");

        JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
            .addFields("data.loggedUser.document", Map.of(
                "identification", loggedUserIdentification,
                "rel", "CPF")
            )
            .addFields("data.debtorAccount", Map.of(
                "ispb", debtorAccountIspb,
                "issuer", debtorAccountIssuer,
                "number", debtorAccountNumber,
                "accountType", debtorAccountType)
            );

        businessEntityIdentification.ifPresent(identification -> jsonObjectBuilder
            .addFields("data.businessEntity.document", Map.of(
                "identification", identification,
                "rel", "CNPJ"
            )));

        JsonObject enrollmentsRequest = jsonObjectBuilder.build();
        JsonObject data = enrollmentsRequest.getAsJsonObject("data");
        data.add("permissions", new Gson().toJsonTree(permissions));

        env.putObject("resource_request_entity_claims", enrollmentsRequest);

        logSuccess("Enrollments request body was created and added to environment",
            args("enrollments request body", enrollmentsRequest));

        return env;
    }

    private String extractFromConfigOrDie(Environment env, final String path) {
        Optional<String> string = Optional.ofNullable(env.getString("config", path));
        return string.orElseThrow(() -> error(String.format("Unable to find element %s in config", path)));
    }
}
