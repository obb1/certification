package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EnsureConsentStatusUpdateTimeHasNotChanged extends AbstractCondition {
	@Override
	@PreEnvironment(required = "consent_endpoint_response_full", strings = "consent_status_update_date_time")
	public Environment evaluate(Environment env) {
		String consentStatusUpdateDateTime = env.getString("consent_status_update_date_time");
		String path = "body_json.data.statusUpdateDateTime";
		String consentExtensionStatusUpdateDateTime = env.getString("consent_endpoint_response_full", path);
		if (consentStatusUpdateDateTime == null) {
			throw error("Couldn't find " + path + " in the consent extension response",
				args("consent_endpoint_response", env.getObject("consent_endpoint_response_full")));
		}
		if (!consentStatusUpdateDateTime.equals(consentExtensionStatusUpdateDateTime)) {
			throw error("Consent status update date time has changed",
				args("consent_status_update_date_time", consentStatusUpdateDateTime,
					"consent_extension_status_update_date_time", consentExtensionStatusUpdateDateTime));
		}
		logSuccess("Consent status update date time has not changed",
			args("consent_status_update_date_time", consentStatusUpdateDateTime,
				"consent_extension_status_update_date_time", consentExtensionStatusUpdateDateTime));
		return env;
	}
}
