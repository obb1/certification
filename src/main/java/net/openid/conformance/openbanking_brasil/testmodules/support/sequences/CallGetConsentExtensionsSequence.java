package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CallConsentExtensionEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.GetConsentExtensionValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToGetConsentExtension;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInGetSizeOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class CallGetConsentExtensionsSequence extends AbstractConditionSequence {
	@Override
	public void evaluate() {
		callAndStopOnFailure(PrepareToGetConsentExtension.class);
		callAndContinueOnFailure(CallConsentExtensionEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
		callAndStopOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(GetConsentExtensionValidatorV2.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ValidateExtensionExpiryTimeInGetSizeOne.class, Condition.ConditionResult.FAILURE);
		call(exec().unmapKey("resource_endpoint_response_full"));
	}
}
