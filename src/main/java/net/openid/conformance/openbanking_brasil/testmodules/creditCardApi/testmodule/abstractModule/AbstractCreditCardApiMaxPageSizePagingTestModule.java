package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.CardAccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearContentTypeHeaderForResourceEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ClearRequestObjectFromEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlTransactionsPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetResourceMethodToGet;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateNumberOfRecordsPage1;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateNumberOfRecordsPage2;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetCreditCardsAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public abstract class AbstractCreditCardApiMaxPageSizePagingTestModule extends AbstractPhase2TestModule {


	protected abstract Class<? extends Condition> getCardAccountsTransactionsValidator();

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetCreditCardsAccountsV2Endpoint.class),
			condition(GetConsentV3Endpoint.class)
		);
	}

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected void configureClient(){
		super.configureClient();
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromTransactionDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toTransactionDate", currentDate.format(FORMATTER));
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.CREDIT_CARDS_ACCOUNTS).addScopes(OPFScopesEnum.CREDIT_CARDS_ACCOUNTS, OPFScopesEnum.OPEN_ID).build();

	}

	@Override
	protected void validateResponse() {
		runInBlock("Fetch credit card transactions endpoint with page-size=1000 - Expects 200", () -> {
			callAndStopOnFailure(CardAccountSelector.class);
			callAndStopOnFailure(SetProtectedResourceUrlTransactionsPageSize1000.class);
			callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResourceResponseCodeWas200.class);
			callAndContinueOnFailure(getCardAccountsTransactionsValidator(), Condition.ConditionResult.FAILURE);
			env.putString("metaOnlyRequestDateTime", "true");
			callAndContinueOnFailure(ValidateNumberOfRecordsPage1.class, Condition.ConditionResult.FAILURE);
		});


		runInBlock("Fetch second page - Expects 200", () -> {
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndContinueOnFailure(SetProtectedResourceUrlToNextEndpoint.class, Condition.ConditionResult.WARNING);
			callAndContinueOnFailure(SetResourceMethodToGet.class, Condition.ConditionResult.WARNING);
			callAndContinueOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class, Condition.ConditionResult.WARNING);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.WARNING);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.WARNING);
			callAndContinueOnFailure(getCardAccountsTransactionsValidator(), Condition.ConditionResult.WARNING);
			callAndContinueOnFailure(ValidateNumberOfRecordsPage2.class, Condition.ConditionResult.FAILURE);
		});
	}

}
