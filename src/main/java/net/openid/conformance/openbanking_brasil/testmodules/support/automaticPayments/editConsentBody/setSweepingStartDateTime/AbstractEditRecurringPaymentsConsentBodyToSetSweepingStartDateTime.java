package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public abstract class AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime extends AbstractCondition {

	protected abstract LocalDateTime calculateStartDateTime(LocalDateTime currentDateTime);

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data field in consents payload"));

		JsonObject sweeping = Optional.ofNullable(data.getAsJsonObject("recurringConfiguration"))
			.map(recurringConfiguration -> recurringConfiguration.getAsJsonObject("sweeping"))
			.orElseThrow(() -> error("The recurringConfiguration is not set to sweeping"));

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = calculateStartDateTime(currentDateTime);
		String formattedDateTime = dateTime.format(formatter);
		sweeping.addProperty("startDateTime", formattedDateTime);

		logSuccess("The startDateTime field has been set",
			args("data", data));

		return env;
	}
}
