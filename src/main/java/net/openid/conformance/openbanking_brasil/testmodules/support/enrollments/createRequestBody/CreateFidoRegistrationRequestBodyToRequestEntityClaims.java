package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoAttestationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateKeyPairFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.text.ParseException;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;

public class CreateFidoRegistrationRequestBodyToRequestEntityClaims extends AbstractCondition {

	// Registration Options Response
	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Error parsing JWT response");
		}
	}


	/**
	 * {@link GenerateFidoClientData} condition adds client_data object to the env
	 * {@link GenerateKeyPairFromFidoRegistrationOptionsResponse} condition adds fido_keys_jwk object to the env
	 * {@link GenerateFidoAttestationObject} condition adds encoded_attestation_object string to the env
	 * RESPONSE_ENV_KEY - resource_endpoint_response_full - expects to contain Registration Options Response
	 **/

	@Override
	@PreEnvironment(required = {RESPONSE_ENV_KEY, "fido_keys_jwk", "client_data"}, strings = {"encoded_attestation_object"})
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {

		JsonObject body = bodyFrom(env).getAsJsonObject();
		JsonObject fidoKeysJwk = env.getObject("fido_keys_jwk");

		String encodedAttestationObject = env.getString("encoded_attestation_object");
		String clientDataJson = env.getObject("client_data").toString();
		String encodedClientDataJson =  Base64.getEncoder().withoutPadding().encodeToString(clientDataJson.getBytes());

		String kid = Optional.ofNullable(fidoKeysJwk.get("kid"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find kid in the fido_keys_jwk", Map.of("key", fidoKeysJwk)));

		String id = Base64.getEncoder().withoutPadding().encodeToString(kid.getBytes());

		JsonObjectBuilder fidoRegistrationRequestBuilder = new JsonObjectBuilder()
			.addField("data.id", id)
			.addField("data.rawId", id)
			.addField("data.type", "public-key")
			.addField("data.response.clientDataJSON", encodedClientDataJson)
			.addField("data.response.attestationObject", encodedAttestationObject);


		Optional.ofNullable(body.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("authenticatorSelection"))
			.map(authenticatorSelection -> authenticatorSelection.get("authenticatorAttachment"))
			.map(OIDFJSON::getString)
			.ifPresent(authenticatorAttachment -> fidoRegistrationRequestBuilder.addField("data.authenticatorAttachment", authenticatorAttachment));

		JsonObject fidoRegistrationRequestBody = fidoRegistrationRequestBuilder.build();

		logSuccess("Added FIDO Registration Request Body to the environment", Map.of("body", fidoRegistrationRequestBody));
		env.putObject("resource_request_entity_claims", fidoRegistrationRequestBody);

		return env;
	}
}
