package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.AbstractPostConsentExtendsOASValidator;

@ApiName("Post Consent Extends V3")
public class PostConsentExtendsOASValidatorV3 extends AbstractPostConsentExtendsOASValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/consents/swagger-consents-3.0.1.yml";
	}


}
