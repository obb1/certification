package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractFvpAutomaticPaymentsApiRejectedConsentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.GetRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PatchRecurringConsentOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringConsentOASValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-automatic-payments_api_rejected-consent_test-module_v1",
	displayName = "fvp-automatic-payments_api_rejected-consent_test-module_v1",
	summary = "Ensure a consent is REJECTED when the PATCH recurring-consentId endpoint is called before approval.\n" +
		"• Call the POST recurring-consents endpoint with sweeping accounts fields\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 200 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Call the PATCH {recurringConsentId} endpoint  with status as REJECTED, rejectedBy as USUARIO, rejectedFrom as INICIADORA and reason.code as REJEITADO_USUARIO\n" +
		"• Expect 200 - Validate Message\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is REJECTED, rejectedBy is USUARIO, rejectedFrom is INICIADORA and reason.code is REJEITADO_USUARIO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpAutomaticPaymentsApiRejectedConsentTestModuleV1 extends AbstractFvpAutomaticPaymentsApiRejectedConsentTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentConsentValidator() {
		return PatchRecurringConsentOASValidatorV1.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetAutomaticPaymentRecurringConsentV1Endpoint.class;
	}
}
