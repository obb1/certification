package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SetPaymentConsentDebtorAccountNumber extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource", strings = "debtor_account_number")
	public Environment evaluate(Environment env) {
		JsonObject debtorAccount = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent.data.debtorAccount"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Could not find debtorAccount in payment consent request body"));
		String number = env.getString("debtor_account_number");

		debtorAccount.addProperty("number", number);

		logSuccess("The debtorAccount.number field inside the payment consent request has been updated",
			args("debtorAccount", debtorAccount));
		return env;
	}
}
