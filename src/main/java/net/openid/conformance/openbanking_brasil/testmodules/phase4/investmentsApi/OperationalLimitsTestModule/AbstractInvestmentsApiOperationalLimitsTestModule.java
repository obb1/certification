package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvestmentsV1FromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1.GetVariableIncomeBrokerNotesV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckScopeInConfigIsValid;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractBrokerNoteId;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractInvestmentID;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestment;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentBalances;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentBrokerNote;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactionsCurrent;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentsRoot;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractInvestmentsApiOperationalLimitsTestModule extends AbstractOperationalLimitsTestModule {

	/**
	 * Test will make sure that the defined operational limits for all endpoints are respected.
	 * It will also confirm that the transactions endpoint can be used beyond the operational limit,
	 * as long as the next page is called.
	 * <p>
	 * Test will require the user to have set at least one ACTIVE resources each with at least 10
	 * Transactions to be returned on the transactions endpoint. The tested resource must be set linked
	 * to the brazilCpf and brazilCnpj for Operational Limits Test will execute a POST token with a
	 * refresh_token grant at least 60 seconds before the token is about to be expired, by working with
	 * the "expires_in" set on the token endpoint Test will not log all successful the validations done
	 * on the endpoints due to the high number of requests, however all failures will be provided.
	 */

	protected abstract OPFScopesEnum setScope();

	@Override
	protected void configureClient() {
		callAndStopOnFailure(setInvestmentsApi().getClass());
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetInvestmentsV1FromAuthServer.class),
			condition(GetConsentV3Endpoint.class)
		)));
		callAndStopOnFailure(CheckScopeInConfigIsValid.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder
			.addScopes(setScope(), OPFScopesEnum.CONSENTS, OPFScopesEnum.OPEN_ID)
			.addPermissionsBasedOnCategory(OPFCategoryEnum.INVESTMENTS)
			.build();
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void performPreAuthorizationSteps() {
		call(createOBBPreauthSteps());
		fetchConsent();
		validateGetConsentResponse();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(isSecondClient(), false, addTokenEndpointClientAuthentication, false, false, false)
			.insertAfter(FAPIBrazilConsentEndpointResponseValidatePermissions.class, condition(SaveConsentsAccessToken.class));
	}

	@Override
	protected void requestProtectedResource() {
		performRootInvestmentsOperationalLimitsSteps();

		performInvestmentsTransactionsOperationalLimitsSteps();

		if (isVariableIncome()) {
			performInvestmentsVariableIncomeOperationalLimitsSteps();
		}

		performInvestmentsIdentificationOperationalLimitsSteps();

		performInvestmentsBalancesOperationalLimitsSteps();

		performInvestmentsTransactionsCurrentOperationalLimitsSteps();
	}

	protected void performInvestmentsTransactionsCurrentOperationalLimitsSteps() {
		runInBlock("Prepare investments transactions current URL", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class);
			callAndStopOnFailure(setFromTransactionTo6DaysAgo());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
		});

		// Call transactions current with 6 day range & log/validate only the first call
		for (int i = 0; i < getInvestmentsTransactionsCurrentNumberOfExecutions(); i++) {
			eventLog.startBlock(String.format("[%d] Calling investment transactions endpoint.", i + 1));
			preCallProtectedResource();

			if (i == 0) {
				investmentTransactionsCurrentValidator();
				disableLogging();
			}
			eventLog.endBlock();
		}
		enableLogging();
	}

	protected void performInvestmentsBalancesOperationalLimitsSteps() {
		callAndStopOnFailure(PrepareUrlForFetchingInvestmentBalances.class);
		// Call balances & log/validate only the first call
		for (int i = 0; i < getInvestmentsBalancesNumberOfExecutions(); i++) {
			eventLog.startBlock(String.format("[%d] Calling investment balances endpoint.", i + 1));
			preCallProtectedResource();
			if (i == 0) {
				investmentBalancesValidator();
				disableLogging();
			}
			eventLog.endBlock();
		}
		enableLogging();
	}

	protected void performInvestmentsIdentificationOperationalLimitsSteps() {
		callAndStopOnFailure(PrepareUrlForFetchingInvestment.class);
		// Call identifications & log/validate only the first call
		for (int i = 0; i < getInvestmentsIdentificationNumberOfExecutions(); i++) {
			eventLog.startBlock(String.format("[%d] Calling investment endpoint.", i + 1));
			preCallProtectedResource();
			if (i == 0) {
				investmentIdentificationValidator();
				disableLogging();
			}
			eventLog.endBlock();
		}
		enableLogging();
	}

	protected void performInvestmentsVariableIncomeOperationalLimitsSteps() {
		callAndStopOnFailure(PrepareUrlForFetchingInvestmentBrokerNote.class);
		for (int i = 0; i < getInvestmentsBrokerNoteNumberOfExecutions(); i++) {
			eventLog.startBlock(String.format("[%d] Calling investment broker note endpoint with stored brokerNoteId.", i + 1));
			preCallProtectedResource();
			if (i == 0) {
				callAndContinueOnFailure(getInvestmentsBrokerNoteValidator(), Condition.ConditionResult.FAILURE);
				disableLogging();
			}
			eventLog.endBlock();
		}
		enableLogging();
	}

	protected void performInvestmentsTransactionsOperationalLimitsSteps() {
		runInBlock("Prepare Investments Transactions URL", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
			callAndStopOnFailure(setFromTransactionTo365DaysAgo());
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
		});

		for (int i = 0; i < getInvestmentsTransactionsNumberOfExecutions(); i++) {

			eventLog.startBlock(String.format("[%d] Calling investment transactions endpoint.", i + 1));

			preCallProtectedResource();

			if (i == 0) {
				runInBlock("Validating GET Investment transactions response", this::investmentTransactionsValidator);
				callAndStopOnFailure(SetProtectedResourceUrlToSelfEndpoint.class);
				disableLogging();
			} else if (i == 3) {
				callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
			}
			eventLog.endBlock();
		}
		if (isVariableIncome()) {
			// If the tested API is for VI Extract one brokerNoteId value returned
			callAndStopOnFailure(ExtractBrokerNoteId.class);
		}
		enableLogging();
	}

	protected void performRootInvestmentsOperationalLimitsSteps() {
		callAndStopOnFailure(PrepareUrlForFetchingInvestmentsRoot.class);
		for (int i = 0; i < getInvestmentsNumberOfExecutions(); i++) {
			eventLog.startBlock(String.format("[%d] Calling root investments endpoint.", i + 1));
			preCallProtectedResource();
			if (i == 0) {
				investmentsRootValidator();
				callAndStopOnFailure(ExtractInvestmentID.class);
				disableLogging();
			}
			eventLog.endBlock();
		}
		enableLogging();
	}

	@Override
	protected void validateResponse() {
		// Not needed
	}

	protected void fetchConsent() {
		runInBlock(String.format("Checking the created consent - Expecting %s status", "AWAITING_AUTHORISATION"), () -> {
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void validateGetConsentResponse() {
		callAndContinueOnFailure(GetConsentOASValidatorV3n2.class, Condition.ConditionResult.FAILURE);
	}

	protected ConditionSequence deleteConsent() {
		return sequenceOf(
			condition(LoadConsentsAccessToken.class),
			condition(PrepareToFetchConsentRequest.class),
			condition(PrepareToDeleteConsent.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
		);
	}

	@Override
	public void cleanup() {
		runInBlock("Deleting consent", () -> call(deleteConsent()));
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");
	}

	/** Abstract Class Config **/

	protected Class<? extends Condition> getInvestmentsBrokerNoteValidator() {
		return GetVariableIncomeBrokerNotesV1OASValidator.class;
	}

	protected int getInvestmentsNumberOfExecutions() {
		return 30;
	}

	protected int getInvestmentsIdentificationNumberOfExecutions() {
		return 4;
	}

	protected int getInvestmentsBalancesNumberOfExecutions() {
		return 120;
	}

	protected int getInvestmentsTransactionsNumberOfExecutions() {
		return 14;
	}

	protected int getInvestmentsTransactionsCurrentNumberOfExecutions() {
		return 120;
	}

	protected int getInvestmentsBrokerNoteNumberOfExecutions() {
		return 30;
	}

	protected boolean isVariableIncome() {
		return false;
	}

	protected abstract AbstractSetInvestmentApi setInvestmentsApi();

	protected abstract void investmentsRootValidator();

	protected abstract void investmentTransactionsValidator();

	protected abstract void investmentTransactionsCurrentValidator();

	protected abstract void investmentIdentificationValidator();

	protected abstract void investmentBalancesValidator();

	protected abstract Class<? extends Condition> setToTransactionToToday();

	protected abstract Class<? extends Condition> setFromTransactionTo365DaysAgo();

	protected abstract Class<? extends Condition> setFromTransactionTo6DaysAgo();

	protected abstract Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl();
}
