package net.openid.conformance.openbanking_brasil.testmodules.directory;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.GetDynamicServerConfiguration;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractBlockLoggingTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.ValidateApiResourceCertificationStatus;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.VariantParameters;

@PublishTestModule(
	testName = "directory_api_certification-status_test-module",
	displayName = "Directory Registration Server Check - FVP",
	summary = "Certify that the status of Api Resource Certification is correctly registered\n" +
		"\u2022 Obtain the all Api Resource Certifications published for the tested Authorisation Server\n" +
		"\u2022 Validate the Certification Status of each Api Resource\n",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	configurationFields = {
		"server.authorisationServerId",
		"resource.brazilOrganizationId"
	}
)
@VariantParameters({
	FAPI1FinalOPProfile.class,
	ClientAuthType.class,
	FAPIAuthRequestMethod.class

})
public class ApiResourceCertificationStatusTestModule extends AbstractBlockLoggingTestModule {
	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
		env.putObject("config", config);
		callAndStopOnFailure(GetDynamicServerConfiguration.class);
		setStatus(Status.CONFIGURED);
	}

	@Override
	public void start() {
		setStatus(Status.RUNNING);
		eventLog.startBlock("Extracted Authorisation Server");
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		eventLog.endBlock();
		eventLog.startBlock("Validate Api Resource Certification Status");
		callAndContinueOnFailure(ValidateApiResourceCertificationStatus.class, Condition.ConditionResult.FAILURE);
		if (env.getString("warning_message") != null){
			callAndContinueOnFailure(ChuckWarning.class, Condition.ConditionResult.WARNING);
		}
		fireTestFinished();
	}



	@Override
	protected void logFinalEnv() {
		// nothing to add
	}
}
