package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsPatchTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PatchPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PatchPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_recurring-payments-patch_test-module_v4",
	displayName = "payments_api_recurring-payments-patch_test-module_v4",
	summary = "Ensure that the PATCH endpoints can successfully cancel payments, depending on the type of cancellation being executed\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+1, and schedule.daily.quantity as 5\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is “AUTHORISED” and validate response\n" +
		"• Calls the POST Payments Endpoint with the 5 Payments, using the appropriate endToEndId for each of them\n" +
		"• Expects 201 - Validate Response\n" +
		"• Poll the Get Payments endpoint with the last PaymentID Created while payment status is RCVD or ACCP\n" +
		"• Call the GET Payments for the 5 Payments\n" +
		"• Expect Payment Scheduled in all of them (SCHD) - Validate Response\n" +
		"• Call the PATCH PaymentId Endpoint with the first PaymentID\n" +
		"• Expect 200 - Check if status is CANC, and cancellation reason is CANCELADO_AGENDAMENTO. Check if cancelledFrom is INICIADORA\n" +
		"• Call the GET Payments for the 4 Payments left\n" +
		"• Expect Payment Scheduled in all of them (SCHD) - Validate Response\n" +
		"• Call the PATCH ConsentId Endpoint\n" +
		"• Expect 200 - Check if the list has the 4 paymentIds left, and the statusUpdateDateTime is after the time of the request\n" +
		"• Call the GET Payments for the 5 Payments\n" +
		"• Expect Payment Cancelled in all of them (CANC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiRecurringPaymentsPatchTestModuleV4 extends AbstractPaymentsApiRecurringPaymentsPatchTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentConsentValidator() {
		return PatchPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> patchPaymentValidator() {
		return PatchPaymentsPixOASValidatorV4.class;
	}
}
