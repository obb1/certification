package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj.EnsureNonRedirectPaymentsJointAccountCpfOrCnpjIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus.CheckPaymentConsentPollStatusWasPartiallyAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasPartiallyAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractEnrollmentsApiMultipleConsentsCoreTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	@Override
	protected void setupResourceEndpoint() {
		super.setupResourceEndpoint();
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsureNonRedirectPaymentsJointAccountCpfOrCnpjIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no 'Payment consent - Logged User CPF - Multiple Consents Test' was informed.");
		}
	}

	@Override
	protected void getConsentAfterConsentAuthorise() {
		fetchConsentToCheckStatus("PARTIALLY_ACCEPTED", new EnsurePaymentConsentStatusWasPartiallyAccepted());

		repeatSequence(this::getRepeatConsentSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(19)
			.validationSequence(this::getPaymentConsentValidationSequence)
			.run();

		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
	}

	protected ConditionSequence getRepeatConsentSequence() {
		callAndStopOnFailure(SetProtectedResourceUrlToConsentSelfEndpoint.class);
		return new ValidateSelfEndpoint()
			.skip(SetProtectedResourceUrlToSelfEndpoint.class, "The protected resource url was already set")
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(EnsureResourceResponseCodeWas200.class, condition(getConsentPollStatusCondition()));
	}

	protected Class<? extends Condition> getConsentPollStatusCondition() {
		return CheckPaymentConsentPollStatusWasPartiallyAccepted.class;
	}

	protected ConditionSequence getPaymentConsentValidationSequence() {
		return sequenceOf(
			condition(getPaymentConsentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
	}

	@Override
	protected void executeFurtherSteps() {}

	@Override
	protected void configureDictInfo() {}

	protected abstract Class<? extends Condition> getPaymentConsentValidator();
}
