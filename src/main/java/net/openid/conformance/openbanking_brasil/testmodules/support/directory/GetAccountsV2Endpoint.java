package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAccountsV2Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/accounts\\/v\\d+\\/accounts)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "accounts";
	}

	@Override
	protected String getApiVersionRegex() {
		return  "^(2.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
