package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.BuildRequestObjectPostToPAREndpoint;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.CopyDebtorAccountToCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.SetCreditorAccountToConfigValue;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.WaitFor5Minutes;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasContasOrigemDestinoIguaisV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasRejeitadoUsuarioV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasTempoExpiradoAutorizacaoV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;

import java.util.Optional;


public abstract class AbstractPaymentsApiConsentsRejectionReasonTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	private int run = 0;

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		//Not needed for this test
		return null;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		//Not needed for this test
		return null;
	}

	@Override
	protected void configureDictInfo() {
		if (run == 0) {
			callAndStopOnFailure(CopyDebtorAccountToCreditorAccount.class);
		}
	}

	@Override
	protected void performPreAuthorizationSteps() {
		if (run == 0) {
			OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
				.insertAfter(OptionallyAllow201Or422.class,
					condition(paymentConsentErrorValidator())
						.dontStopOnFailure()
						.onFail(Condition.ConditionResult.FAILURE)
						.skipIfStringMissing("validate_errors")
				)
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(RememberOriginalScopes.class))
				.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
					sequenceOf(condition(CreateRandomFAPIInteractionId.class),
						condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
				);
			call(steps);
			callAndStopOnFailure(SaveAccessToken.class);
		} else {
			super.performPreAuthorizationSteps();
		}
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		ConditionSequence preAuthSteps = super.createOBBPreauthSteps();
		if (run > 0) {
			preAuthSteps = preAuthSteps
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class,
					condition(ResetScopesToConfigured.class))
				.insertAfter(ExtractAccessTokenFromTokenResponse.class,
					condition(SaveAccessToken.class));
		}
		return preAuthSteps;
	}

	@Override
	protected void performAuthorizationFlow() {
		eventLog.startBlock("Executing Test case " + (run + 1));
		if (run == 1) {
			callAndStopOnFailure(SetCreditorAccountToConfigValue.class);
		}

		if (run == 0) {
			performPreAuthorizationSteps();
			int status = Optional.ofNullable(env.getInteger("consent_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find consent_endpoint_response_full"));
			if (status == HttpStatus.SC_CREATED) {
				callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);

				eventLog.startBlock(currentClientString() + "Make request to authorization endpoint");

				createAuthorizationRequest();

				createAuthorizationRequestObject();

				if (isPar.isTrue()) {
					callAndStopOnFailure(BuildRequestObjectPostToPAREndpoint.class);
					addClientAuthenticationToPAREndpointRequest();
					performParAuthorizationRequestFlow();
				} else {
					buildRedirect();
					performRedirect();
				}
			} else {
				callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
				env.mapKey(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class, Condition.ConditionResult.FAILURE);
				env.unmapKey(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.RESPONSE_ENV_KEY);
				performPostAuthorizationFlow();
			}
		} else if (run >= 2) {
			performPreAuthorizationSteps();
			callAndStopOnFailure(WaitFor5Minutes.class);
			call(new ObtainPaymentsAccessTokenWithClientCredentials(addTokenEndpointClientAuthentication));
			callAndStopOnFailure(SaveAccessToken.class);
			fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
			callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasTempoExpiradoAutorizacaoV3.class, Condition.ConditionResult.FAILURE);
			fireTestFinished();
		} else {
			super.performAuthorizationFlow();
		}
	}

	@Override
	protected void onAuthorizationCallbackResponse() {

		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

		// as https://tools.ietf.org/html/draft-ietf-oauth-iss-auth-resp is still a draft we only warn if the value is wrong,
		// and do not require it to be present.
		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");

		if (run <= 1) {
			callAndContinueOnFailure(CheckAuthorizationEndpointHasError.class, Condition.ConditionResult.FAILURE);
		}

		performPostAuthorizationFlow();
	}


	@Override
	protected void performPostAuthorizationFlow() {

		int status = Optional.ofNullable(env.getInteger("consent_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find consent_endpoint_response_full"));

		if (run == 0 && status != HttpStatus.SC_UNPROCESSABLE_ENTITY) {
			fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
			callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasContasOrigemDestinoIguaisV3.class, Condition.ConditionResult.FAILURE);
		} else if (run == 1) {
			fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
			callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasRejeitadoUsuarioV3.class, Condition.ConditionResult.FAILURE);
		}

		run++;
		performAuthorizationFlow();
	}

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();
}
