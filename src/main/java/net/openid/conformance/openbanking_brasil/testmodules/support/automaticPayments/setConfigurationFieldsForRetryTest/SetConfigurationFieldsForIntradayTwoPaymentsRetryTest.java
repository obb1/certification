package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest;

import net.openid.conformance.testmodule.Environment;

public class SetConfigurationFieldsForIntradayTwoPaymentsRetryTest extends AbstractSetConfigurationFieldsForRetryTest {

	@Override
	protected String getBasePath(Environment env) {
		return "intradayTwoPayments";
	}
}
