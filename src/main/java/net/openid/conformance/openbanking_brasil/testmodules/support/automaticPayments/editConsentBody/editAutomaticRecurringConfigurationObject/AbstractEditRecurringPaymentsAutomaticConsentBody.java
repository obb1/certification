package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public abstract class AbstractEditRecurringPaymentsAutomaticConsentBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject automatic = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(JsonElement::getAsJsonObject)
			.map(consent -> consent.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("recurringConfiguration"))
			.map(recurringConfig -> recurringConfig.getAsJsonObject("automatic"))
			.orElseThrow(() -> error("Unable to find data.recurringConfiguration.automatic in consents payload"));

		updateAutomaticObject(automatic);

		logSuccess("Updated consent request body automatic recurringConfiguration object to" + logMessage(), args("automatic", automatic));

		return env;
	}

	protected abstract void updateAutomaticObject(JsonObject automatic);
	protected abstract String logMessage();
}
