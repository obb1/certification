package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class RemoveLastElementFromCustomSchedule extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");
		JsonObject paymentRequest = resource.getAsJsonObject("brazilPixPayment");
		JsonArray data = paymentRequest.getAsJsonArray("data");

		data.remove(data.size()-1);

		logSuccess("Removed final payment from brazilPixPayment.data", args("data", data));
		return env;
	}
}
