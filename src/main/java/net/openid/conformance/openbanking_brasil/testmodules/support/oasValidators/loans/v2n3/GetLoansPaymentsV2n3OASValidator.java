package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.AbstractGetLoansPaymentsOASValidator;


public class GetLoansPaymentsV2n3OASValidator extends AbstractGetLoansPaymentsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/loans/loans-v2.3.0.yml";
	}
}
