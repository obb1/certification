package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.AbstractPostConsentExtendsOASValidator;

@ApiName("Post Consent Extends V3-2")
public class PostConsentExtendsOASValidatorV3n2 extends AbstractPostConsentExtendsOASValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/consents/swagger-consents-3.2.0.yml";
	}


}
