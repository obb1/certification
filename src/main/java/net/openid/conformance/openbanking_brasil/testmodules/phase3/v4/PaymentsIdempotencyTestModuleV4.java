package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsIdempotencyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_idempotency_test-module_v4",
	displayName = "Payments API test module for re-using idempotency keys",
	summary = "Ensure Error when payment request is sent with reused invalid iss and idempotency key\n" +
		"• Calls POST Consents Endpoint\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirects the user to authorize the created consent \n" +
		"• Call GET Consent, with the same idempotency id used at POST Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\" \n" +
		"• Call the POST Payments with an invalid iss\n" +
		"• Expect 403 - Validate Error Message\n" +
		"• Calls the POST Payments, with the same idempotency key used at POST Consent\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the POST Payments with the same idempotency key but a different payload\n" +
		"• Expect 422 ERRO_IDEMPOTENCIA - Validate Error Message\n" +
		"• Call the POST Payments, with the same payload as the previous request but a different idempotency id\n" +
		"• Expects 422 - CONSENTIMENTO_INVALIDO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsIdempotencyTestModuleV4 extends AbstractPaymentsIdempotencyTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

		@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

}
