package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithDateTooFarInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithRepeatedDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule60DailyPaymentsStarting672DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule61DailyPaymentsStarting10DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly.Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule61WeeklyPaymentsForMondayStarting10DaysInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;

public abstract class AbstractFvpPaymentsApiRecurringPaymentsConsentLimitTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		postPaymentExpectingFailure(
			"Daily Scheduling",
			Schedule60DailyPaymentsStarting672DaysInTheFuture.class,
			EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class
		);

		postPaymentExpectingFailure(
			"Weekly Scheduling",
			Schedule60WeeklyPaymentsForMondayStarting317DaysInTheFuture.class,
			EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class
		);

		postPaymentExpectingFailure(
			"Monthly Scheduling",
			Schedule21MonthlyPaymentsForThe1stStarting150DaysInTheFuture.class,
			EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class
		);

		postPaymentExpectingFailure(
			"Custom Scheduling - Invalid Dates",
			CustomSchedulePaymentsWithDateTooFarInTheFuture.class,
			EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class
		);

		postPaymentExpectingFailure(
			"Custom Scheduling - Repeated Dates",
			CustomSchedulePaymentsWithRepeatedDates.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);

		postPaymentExpectingFailure(
			"Daily Scheduling - Limit Exceeded",
			Schedule61DailyPaymentsStarting10DaysInTheFuture.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);

		postPaymentExpectingFailure(
			"Weekly Scheduling - Limit Exceeded",
			Schedule61WeeklyPaymentsForMondayStarting10DaysInTheFuture.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);
	}

	@Override
	protected void configureDummyData() {
		super.configureDummyData();
		callAndStopOnFailure(RemovePaymentConsentDate.class);
	}

	protected void postPaymentExpectingFailure(
		String headerText,
		Class<? extends AbstractSchedulePayment> scheduleCondition,
		Class<? extends AbstractEnsureErrorResponseCodeFieldWas> errorCondition) {

		eventLog.startBlock(headerText);
		callAndStopOnFailure(scheduleCondition);
		call(getPaymentsConsentSequence().replace(EnsureConsentResponseCodeWas201.class, condition(EnsureConsentResponseCodeWas422.class)));
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
		env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndContinueOnFailure(errorCondition, Condition.ConditionResult.FAILURE);
		env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		eventLog.endBlock();
	}
}
