package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public abstract class AbstractSetPaymentReference extends AbstractCondition {

	protected abstract String getPaymentReference(String dateStr);

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));

		String dateStr = Optional.ofNullable(data.get("date"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find date inside payments payload"));
		data.addProperty("paymentReference", getPaymentReference(dateStr));

		logSuccess("The paymentReference field has been set in the recurring payments request body",
			args("data", data));

		return env;
	}

	protected LocalDate getPaymentDate(String dateStr) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(dateStr, formatter);
	}
}
