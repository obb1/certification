package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.KeyType;
import com.upokecenter.cbor.CBOREncodeOptions;
import com.upokecenter.cbor.CBORObject;
import com.webauthn4j.converter.AttestationObjectConverter;
import com.webauthn4j.converter.util.ObjectConverter;
import com.webauthn4j.data.attestation.AttestationObject;
import com.webauthn4j.data.attestation.authenticator.AAGUID;
import com.webauthn4j.data.attestation.authenticator.AttestedCredentialData;
import com.webauthn4j.data.attestation.authenticator.AuthenticatorData;
import com.webauthn4j.data.attestation.authenticator.COSEKey;
import com.webauthn4j.data.attestation.statement.AttestationStatement;
import com.webauthn4j.data.attestation.statement.NoneAttestationStatement;
import com.webauthn4j.data.extension.authenticator.RegistrationExtensionAuthenticatorOutput;
import com.webauthn4j.util.Base64UrlUtil;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.util.Map;
import java.util.Optional;

public class GenerateFidoAttestationObject extends AbstractFidoKeysCondition {


	/**
	 * {@link GenerateKeyPairFromFidoRegistrationOptionsResponse} condition adds fido_keys_jwk object to the env
	 * {@link CreateEnrollmentsFidoRegistrationOptionsRequestBody} condition adds rp_id string to the env
	 **/

	@Override
	@PreEnvironment(strings = "rp_id")
	@PostEnvironment(strings = "encoded_attestation_object")
	public Environment evaluate(Environment env) {

		String rpId = env.getString("rp_id");

		String kid = getKid(env);

		byte[] rpIdHash = getSha256Digest(rpId.getBytes(StandardCharsets.UTF_8));

		byte flags = Byte.parseByte("01000101", 2); // Bits set indicate the UP, UV, and AT flags as per the webauthn specification
		byte[] credentialId = kid.getBytes(StandardCharsets.UTF_8);

		AttestedCredentialData attestedCredentialData = new AttestedCredentialData(AAGUID.ZERO, credentialId, getKey(env));
		AuthenticatorData<RegistrationExtensionAuthenticatorOutput> authenticatorData = new AuthenticatorData<>(rpIdHash, flags, 0, attestedCredentialData);
		AttestationStatement attestationStatement = new NoneAttestationStatement();
		AttestationObject attestationObject = new AttestationObject(authenticatorData, attestationStatement);

		var converter = new AttestationObjectConverter(new ObjectConverter());
		// webauthn4j doesn't support finite-length encoding, so the indefinite-length result will be
		// converted with another library.
		byte[] attestationObjectBytes = converter.convertToBytes(attestationObject);
		byte[] canonicalAttestationObjectBytes = CBORObject.DecodeFromBytes(attestationObjectBytes).EncodeToBytes(CBOREncodeOptions.DefaultCtap2Canonical);

		String encodedAttestationObject = Base64UrlUtil.encodeToString(canonicalAttestationObjectBytes);
		env.putString("encoded_attestation_object", encodedAttestationObject);
		logSuccess("Generated encoded attestation object", Map.of("object", attestationObject, "encoded_object", encodedAttestationObject));

		return env;
	}

	protected COSEKey getKey(Environment env) {
		JsonObject fidoKeysJwk = Optional.ofNullable(env.getObject("fido_keys_jwk"))
			.orElseThrow(() -> error("Could not find fido_keys_jwk in the environment"));

		KeyPair keyPair = getKeyPairFromJwkJson(fidoKeysJwk);
		KeyType keyType = getKeyTypeFromJwkJson(fidoKeysJwk);

		return super.getCOSEKey(keyPair.getPublic(), keyType);
	}

	protected String getKid(Environment env){
		return Optional.ofNullable(env.getObject("fido_keys_jwk"))
			.map(jwk -> jwk.get("kid"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find kid in the fido_keys_jwk.kid"));
	}


}
