package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

public class CreatePatchRecurringConsentsForEditionRequestBodyNoExpirationAmount50Cents extends AbstractCreatePatchRecurringConsentsForEditionRequestBody {

	@Override
	protected String getExpirationDateTime() {
		return null;
	}

	@Override
	protected String getMaxVariableAmount() {
		return "0.50";
	}
}
