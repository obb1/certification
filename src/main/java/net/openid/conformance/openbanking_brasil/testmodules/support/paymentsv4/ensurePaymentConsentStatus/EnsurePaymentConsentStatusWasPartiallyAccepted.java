package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus;


import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums.PaymentConsentStatusEnumV4;

public class EnsurePaymentConsentStatusWasPartiallyAccepted extends AbstractEnsurePaymentConsentStatusWas {

	@Override
	protected String getExpectedStatus() {
		return PaymentConsentStatusEnumV4.PARTIALLY_ACCEPTED.toString();
	}
}
