package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticUnhappyPaymentsConsentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToRemoveSweepingField;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpStatus;

public abstract class AbstractAutomaticPaymentsApiNegativeConsentsTestModule extends AbstractAutomaticUnhappyPaymentsConsentsFunctionalTestModule {

	@Override
	protected void performTestSteps() {
		postAndValidateConsentWithoutSweepingField();

		postAndValidateConsentWithStartDateAfterExpirationDate();

		postAndValidateConsentWithoutFapiInteractionId();

		postAndValidateConsentWithInvalidFapiInteractionId();
	}

	protected void postAndValidateConsentWithoutSweepingField() {
		runInBlock("POST consents without the recurringConfiguration.sweeping field - Expects 422 PARAMETRO_NAO_INFORMADO or 400", () -> {
			callAndStopOnFailure(LoadConsentsBody.class);
			callAndStopOnFailure(EditRecurringPaymentsConsentBodyToRemoveSweepingField.class);
			call(postConsentSequence());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas400Or422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
			if (env.getInteger("consent_endpoint_response_full", "status") == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				env.mapKey("endpoint_response", "consent_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class, Condition.ConditionResult.FAILURE);
				env.unmapKey("endpoint_response");
			}
		});
	}

	protected void postAndValidateConsentWithStartDateAfterExpirationDate() {
		performTestStep(
			"POST consents with the startDateTime as D+10 and expirationDateTime as D+5 - Expects 422 PARAMETRO_INVALIDO",
			EditRecurringPaymentsConsentBodyToSetInvalidExpirationDate.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);
	}

	protected void postAndValidateConsentWithoutFapiInteractionId() {
		runInBlock("POST consents without the x-fapi-interaction-id - Expects 400", () -> {
			callAndStopOnFailure(LoadConsentsBody.class);
			call(postConsentSequence()
				.skip(CreateRandomFAPIInteractionId.class, "Will not create x-fapi-interaction-id header")
				.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "Will not create x-fapi-interaction-id header"));
			callAndContinueOnFailure(EnsureConsentResponseCodeWas400.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void postAndValidateConsentWithInvalidFapiInteractionId() {
		runInBlock("POST consents with the x-fapi-interaction-id as '123456' - Expects 400 or 422 PARAMETRO_INVALIDO", () -> {
			callAndStopOnFailure(LoadConsentsBody.class);
			call(postConsentSequence()
				.replace(CreateRandomFAPIInteractionId.class, condition(CreateInvalidFAPIInteractionId.class)));
			callAndContinueOnFailure(EnsureConsentResponseCodeWas400Or422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE);
			if(OIDFJSON.getInt(env.getObject("consent_endpoint_response_full").get("status"))==422){
				env.mapKey("endpoint_response", "consent_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class, Condition.ConditionResult.FAILURE);
				env.unmapKey("endpoint_response");
			}
		});
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}
}
