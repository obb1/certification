package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class GetInvestmentsV1FromAuthServer extends AbstractGetXFromAuthServer {


	private String investmentApi;

	@Override
	@PreEnvironment(strings = "investment-api")
	public Environment evaluate(Environment env) {
		investmentApi = env.getString("investment-api");
		return super.evaluate(env);
	}

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/" + investmentApi + "\\/v\\d+\\/investments)$";
	}

	@Override
	protected String getApiFamilyType() {
		return investmentApi;
	}

	@Override
	protected String getApiVersionRegex() {
		return "^(1.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
