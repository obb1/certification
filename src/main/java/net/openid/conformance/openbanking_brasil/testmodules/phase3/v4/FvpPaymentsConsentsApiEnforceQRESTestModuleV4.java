package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsConsentsApiEnforceQRESTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_qres-code-enforcement_test-module_v4",
	displayName = "Payments Consents API test module to enforce qres local instrument",
	summary = "Ensure a QR code is required when the local instrument is QRES\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"• Create consent with \"localInstrument\" as QRES, but no “qrcode” in the payload\n" +
		"• Expects 422 PARAMETRO_NAO_INFORMADO\n" +
		"• Validate Error response\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpPaymentsConsentsApiEnforceQRESTestModuleV4 extends AbstractFvpPaymentsConsentsApiEnforceQRESTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
