package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiMultipleConsentsConditionalTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "payments_api_multiple-consents-conditional_test-module_v4",
	displayName = "Payments API multiple consents conditional test",
	summary = "This test module should be executed only by institutions that currently support consents with multiple accounts, in case the institution does not support this feature the ‘Payment consent - Logged User CPF - Multiple Consents Test' and 'Payment consent - Business Entity CNPJ - Multiple Consents Test’  should be left empty, which in turn will make the test return a SKIPPED\n" +
		"\n" +
		"This test module validates that the payment will reach an ACSC state after multiple consents are granted\n" +
		"\u2022 Validates if the user has provided the field \"Payment consent - Logged User CPF - Multiple Consents Test\". If field is not provided the whole test scenario must be SKIPPED\n" +
		"\u2022 Set the payload to be customer business if Payment consent - Business Entity CNPJ - Multiple Consents Test was provided. If left blank, it should be customer personal\n" +
		"\u2022 Creates consent request_body with valid email proxy (cliente-a00001@pix.bcb.gov.br) and its standardized payload, set the 'Payment consent - Logged User CPF - Multiple Consents Test' on the loggedUser identification field, Debtor account should not be sent\n" +
		"\u2022 Call the POST Consents API\n" +
		"\u2022 Expects 201 - Validate that the response_body is in line with the specifications\n" +
		"\u2022 Redirects the user to authorize the created consent - Expect Successful Authorization\n" +
		"\u2022 Calls the GET Consents Endpoint\n" +
		"\u2022 Expects 200 - Check the status is “PARTIALLY_ACCEPTED” and validate the response\n" +
		"\u2022 Calls the POST Payments Endpoint\n" +
		"\u2022 Expects 422 - CONSENTIMENTO_PENDENTE_AUTORIZACAO\n" +
		"\u2022 Poll the GET Consents endpoint for 10 minutes with the ConsentID created while consent is \"PARTIALLY_ACCEPTED\"\n" +
		"\u2022 Call the POST Payment Endpoint\n" +
		"\u2022 Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"\u2022 Call the GET Payments Endpoint\n" +
		"\u2022 Expected Accepted state (ACSC) - Validate the Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount",
	}
)
public class PaymentsApiMultipleConsentsConditionalTestModuleV4 extends AbstractPaymentsApiMultipleConsentsConditionalTestModuleV4 {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

		@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

}
