package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJwt;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200or406;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilPaymentConsentProdValues;
import net.openid.conformance.sequence.ConditionSequence;
import org.springframework.http.HttpStatus;

public abstract class AbstractFvpPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> postPaymentConsentValidator();
	protected abstract Class<? extends Condition> getPaymentConsentValidator();
	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilPaymentConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("Create a payment consent");
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

		call(getPaymentsConsentSequence());

		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		eventLog.startBlock("Try to fetch consent with json accept header");
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		ConditionSequence validationSequence = new ValidateSelfEndpoint()
			.replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas200or406.class))
			.skip(SaveOldValues.class, "Skipped in the sequence for further check")
			.skip(LoadOldValues.class, "Skipped in the sequence for further check");
		call(validationSequence);
		if(env.getInteger("consent_endpoint_response_full", "status") == HttpStatus.OK.value()) {
			callAndStopOnFailure(getPaymentConsentValidator());
			callAndStopOnFailure(EnsureResponseWasJwt.class);
		} else {
			callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
		}
		env.unmapKey("resource_endpoint_response_full");
		eventLog.endBlock();
	}

	@Override
	protected void configureDummyData() {
		super.configureDummyData();
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndContinueOnFailure(SanitiseQrCodeConfig.class, Condition.ConditionResult.FAILURE);
	}
}
