package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class PaymentConsentIdExtractor extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	@PostEnvironment(strings = "consent_id")
	public Environment evaluate(Environment env) {
		try {
			JsonObject consent = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
			JsonObject data = consent.getAsJsonObject("data");
			String consentId = OIDFJSON.getString(data.get("consentId"));
			env.putString("consent_id", consentId);
			logSuccess("Found consent id", args("consentId", consentId));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}

		return env;
	}

}
