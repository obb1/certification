package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiInvalidParametersTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToSetAccountTypeToSLRY;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.GetEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_invalid-parameters_test-module_v2-1",
	displayName = "enrollments_api_invalid-parameters_test-module_v2-1",
	summary = "Ensure that enrollment can't be requested if request parameters are sent wrongly\n" +
		"• Call the POST enrollments endpoint, don't send an x-fapi-interaction-id on the header\n" +
		"• Expect a 400 response\n" +
		"• Call the POST enrollments endpoint, sign the message with an incorrect private key\n" +
		"• Expect a 400 response\n" +
		"• Call the POST enrollments endpoint without sending the issuer field, but with accountType as CACC\n" +
		"• Expect a 422 response - Validate Code PARAMETRO_NAO_INFORMADO\n" +
		"• Call the POST enrollments endpoint without sending the debtorAccount.accountType field\n" +
		"• Expect a 422 response - With Code PARAMETRO_NAO_INFORMADO\n" +
		"• Call the POST enrollments endpoint and send an invalid permission value\n" +
		"• Expect a 422 response - With Code PERMISSOES_INVALIDAS\n" +
		"• Call the POST enrollments endpoint sending the debtorAccount.accountType field as SLRY\n" +
		"• Expect a 422 response - With Code PARAMETRO_INVALIDO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class EnrollmentsApiInvalidParametersTestModuleV2n1 extends AbstractEnrollmentsApiInvalidParametersTestModule {

	@Override
	protected void executeTestSteps() {
		super.executeTestSteps();

		runInBlock("Call POST enrollments - Send the debtorAccount.accountType field as SLRY",
			this::postAndValidateEnrollmentsWithAccountTypeAsSlry);
	}

	protected void postAndValidateEnrollmentsWithAccountTypeAsSlry() {
		callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
		call(createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToSetAccountTypeToSLRY.class))
		);
		validate422Response(new EnsureErrorResponseCodeFieldWasParametroInvalido());
	}

	@Override
	protected void postAndValidateEnrollmentsWithoutIssuerAndAccountTypeAsCacc() {
		callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
		call(createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToRemoveIssuerFieldAndSetAccountToCACC.class))
		);
		validate422Response(new EnsureErrorResponseCodeFieldWasParametroNaoInformado());
	}

	@Override
	protected void postAndValidateEnrollmentsWithoutAccountType() {
		callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
		call(createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToRemoveDebtorAccountAccountTypeField.class))
		);
		validate422Response(new EnsureErrorResponseCodeFieldWasParametroNaoInformado());
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV2Endpoint.class));
	}

	@Override
	public void cleanup() {
		//not required
	}
}
