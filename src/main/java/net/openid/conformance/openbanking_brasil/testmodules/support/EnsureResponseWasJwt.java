package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Base64;

public class EnsureResponseWasJwt extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"resource_endpoint_response_headers", "resource_endpoint_response_full"})
	public Environment evaluate(Environment env) {
		String contentTypeStr = env.getString("resource_endpoint_response_headers", "content-type");
		if(!contentTypeStr.contains("application/jwt")) {
			throw error("Was expecting a JWT response. Returned: " + contentTypeStr);
		} else {
			String body = env.getString("resource_endpoint_response_full", "body");
			String[] splitJwt = body.split("\\.");
			if (splitJwt.length != 3) {
				throw error(
					"Even though the content-type header of the response is application/jwt, the response isn't a JWT",
					args("body", body)
				);
			}
			try {
				String firstPartString = new String(Base64.getDecoder().decode(splitJwt[0]));
				JsonObject firstPart = new Gson().fromJson(firstPartString, JsonObject.class);
				if (!firstPart.has("alg")) {
					throw error(
						"The first part of the JWT does not have alg field, thus it is an invalid JWT",
						args("first part", firstPart)
					);
				}
			} catch (Exception err) {
				throw error("Response is an invalid JWT");
			}
			logSuccess("Response content type was JWT");
		}
		return env;
	}

}

