package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AddProductTypeToPhase2Config extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config" )
	public Environment evaluate(Environment env) {
		final String key = "config";
		final String path = "consent.productType";
		String cnpj = env.getString(key, "resource.brazilCnpj");

		if (Strings.isNullOrEmpty(cnpj)) {
			log("brazilCnpj is empty, setting product type to Personal");
			env.putString(key, path, "personal");
			logSuccess("Personal Product Type added successfully", args("productType", env.getString(key, path)));
		} else {
			log("brazilCnpj is not empty, setting product type to Business", args("brazilCnpj", cnpj));
			env.putString(key, path, "business");
			logSuccess("Business Product Type added successfully", args("productType", env.getString(key, path)));
		}

		return env;
	}
}
