package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3;

public class GetCreditFixedIncomesTransactionsV1n3OASValidator extends AbstractCreditFixedIncomesTransactionsOASValidatorV1n3 {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions";
	}

}
