package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class CheckAmountOfFailures extends AbstractCondition {

	public static final String LIST_OF_API_FAILURES_ENV_KEY = "api_failures";

	@Override
	@PreEnvironment(required = LIST_OF_API_FAILURES_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject apiFailures = env.getObject(LIST_OF_API_FAILURES_ENV_KEY);
		log("Amount of failures per API tested", args("API failures", apiFailures));
		for (String key : apiFailures.keySet()) {
			if (OIDFJSON.getInt(apiFailures.get(key)) > 0) {
				throw error("At least one of the APIs tested has presented errors");
			}
		}
		logSuccess("No API tested has presented any errors");
		return env;
	}
}
