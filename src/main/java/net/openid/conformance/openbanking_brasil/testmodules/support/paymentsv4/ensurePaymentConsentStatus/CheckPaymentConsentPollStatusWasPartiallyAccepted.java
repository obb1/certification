package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus;


import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractCheckPaymentPollStatus;
import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums.PaymentConsentStatusEnumV4;

import java.util.List;

public class CheckPaymentConsentPollStatusWasPartiallyAccepted extends AbstractCheckPaymentPollStatus {

	@Override
	protected List<String> getExpectedStatuses() {
		return List.of(PaymentConsentStatusEnumV4.PARTIALLY_ACCEPTED.toString());
	}
}
