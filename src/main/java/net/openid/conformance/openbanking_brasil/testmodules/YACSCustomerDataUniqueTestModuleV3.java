package net.openid.conformance.openbanking_brasil.testmodules;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForDateHeaderInResourceResponse;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountBalances;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountLimits;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingBillTransactionResource;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardBills;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardLimits;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.PrepareUrlForFetchingCardTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContract;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContractGuarantees;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContractInstallments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.PrepareUrlForFetchingCreditOperationsContractPayments;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessFinancialRelations;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessIdentifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalFinancialRelationships;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalIdentifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetPersonalQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromBookingDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromDueDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.CardAccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.CardBillSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.CreditOperationsContractSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureExpirationWasNotReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureSpecificPermissionsWereReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadProtectedResourceAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetCreditCardsAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetExchangesV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvestmentsV1FromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvoiceFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetLoansV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPersonalIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountTransactionsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsBalancesOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsIdentificationOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsLimitsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesBalancesOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesIdentificationOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesListOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsTransactionsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsLimitsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsTransactionsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesBalancesV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesIdentificationV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesListV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditFixedIncomes.v1n3.GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessQualificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.BusinessRelationsResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalIdentificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalQualificationResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2.PersonalRelationsResponseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesEventsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n3.GetFinancingsWarrantiesV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsBalancesV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsTransactionsCurrentV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsIdentificationV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsListV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsPaymentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsScheduledInstalmentsV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3.GetInvoiceFinancingsWarrantiesV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansIdentificationV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansListV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansPaymentsV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansScheduledInstalmentsV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4.GetLoansWarrantiesV2n4OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitleBalancesV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitleIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitlesListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitlesTransactionsCurrentV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21.GetVariableIncomeIdentificationV1n21OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21.GetVariableIncomesBalancesV1n21OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21.GetVariableIncomesListV1n21OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21.GetVariableIncomesTransactionsCurrentV1n21OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractInvestmentID;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractOperationID;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperation;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperationEvents;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestment;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentBalances;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactionsCurrent;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToBankFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToCreditFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToFunds;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToTreasureTitles;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToVariableIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFGroupingEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.CheckAmountOfFailures;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@PublishTestModule(
	testName = "fvp-customer_data_unique_happy_path_test-module",
	displayName = "Ensure all endpoints for APIs consented and registered at the directory can be reached.",
	summary = """
		Ensure all endpoints for APIs consented and registered at the directory can be reached.
		• Perform pre checks: Ensure brazilCpf is provided, make sure that a client_id has been generated in the DCR, make sure the provided well known is registered within the Directory, and ensure that the server has Consent and Resources APIs registered for versions 3.0.0 or higher, and fetch the information of all product APIs registered at the directory
		• Call the POST Consents V3  Endpoint with all existing permissions choosing either personal or business, depending if a CPF or CNPJ was provided, and do not send the expirationDateTime
		• Expect 201 - Validate Response
		• Call the GET Consent V3 Endpoint
		• Expect 200 - Validate Response and if check status is AWAITING_AUTHORISATION, and that expirationDateTime is not present at the response
		• Redirect the user to Authorize consent
		• Call the GET Consent V3 Endpoint
		• Expect 200 - Validate Response and if check status is AUTHORISED. Ensure that the permissions for Credit Operations, Investments and Exchanges are kept on the Consent.
		• Based on the products registered for the server on the directory and the permissions returned after the redirect, execute the following calls and validation for all Endpoints in the mentioned API:
			• Call the GET Resources API Endpoint
			• Expect 200 - Validate Response
			• Call the GET Accounts API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Credit Card API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Customer API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Financings API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Invoice Financings API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Loans API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Bank Fixed Incomes API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Credit Fixed Incomes API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Variable Incomes API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Funds API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Treasure Titles API Endpoints
			• Expect 200 - Validate Response
			• Call the GET Exchange API Endpoints
			• Expect 200 - Validate Response
		• Call the DELETE Consents V3 Endpoint
		• Expect 204
		• Log list of endpoints that are tested
	""",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class YACSCustomerDataUniqueTestModuleV3 extends AbstractPhase2TestModule {

	private static final String SKIP_VALIDATION_ENV_KEY = "skip_validation";
	private static final String LIST_OF_ENDPOINTS_ENV_KEY = "endpoints_requested";
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private static String currentApi = "resources";

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog)
			.addPermissionsBasedOnCategory(OPFCategoryEnum.INVESTMENTS, OPFCategoryEnum.EXCHANGES)
			.addScopesBasedOnCategory(OPFCategoryEnum.INVESTMENTS, OPFCategoryEnum.EXCHANGES)
			.addScopes(OPFScopesEnum.OPEN_ID);
		if (isBusiness()) {
			scopesAndPermissionsBuilder
				.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_BUSINESS_PHASE2)
				.addScopesBasedOnCategory(OPFCategoryEnum.ALL_BUSINESS_PHASE2);
		} else {
			scopesAndPermissionsBuilder
				.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_PERSONAL_PHASE2)
				.addScopesBasedOnCategory(OPFCategoryEnum.ALL_PERSONAL_PHASE2);
		}
		scopesAndPermissionsBuilder.build();

		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromBookingDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));
		env.putString("fromTransactionDate", currentDate.minusDays(6).format(FORMATTER));
		env.putString("toTransactionDate", currentDate.format(FORMATTER));
		env.putString("fromDueDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toDueDate", currentDate.format(FORMATTER));
		env.putBoolean(SKIP_VALIDATION_ENV_KEY, false);
		var endpointsRequested = new JsonObject();
		endpointsRequested.add(LIST_OF_ENDPOINTS_ENV_KEY, new JsonArray());
		env.putObject(LIST_OF_ENDPOINTS_ENV_KEY, endpointsRequested);
		var apiFailures = new JsonObject();
		env.putObject(CheckAmountOfFailures.LIST_OF_API_FAILURES_ENV_KEY, apiFailures);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps()
			.skip(FAPIBrazilAddExpirationToConsentRequest.class,"testing consent without expiration");
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		fetchConsentToCheckStatus("AWAITING_AUTHORISATION", new EnsureConsentWasAwaitingAuthorisation());
		callAndStopOnFailure(EnsureExpirationWasNotReturned.class);
	}

	@Override
	protected void requestProtectedResource() {
		if(!validationStarted) {
			env.mapKey("consent_endpoint_response_full", "resource_endpoint_response_full");
			fetchConsentToCheckStatus("AUTHORISED", new EnsureConsentWasAuthorised());
			callAndStopOnFailure(EnsureExpirationWasNotReturned.class);
			env.unmapKey("resource_endpoint_response_full");

			ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
			scopesAndPermissionsBuilder.addPermissionsBasedOnGrouping(
				OPFGroupingEnum.CREDIT_OPERATIONS,
				OPFGroupingEnum.INVESTMENTS,
				OPFGroupingEnum.EXCHANGES
			).build();
			callAndStopOnFailure(EnsureSpecificPermissionsWereReturned.class);

			callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
			callAndStopOnFailure(LoadProtectedResourceAccessToken.class);
			addCurrentApiToFailuresList();
			super.requestProtectedResource();
			eventLog.endBlock();
		}
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetResourcesV3Endpoint.class)
		);
	}

	@Override
	protected void validateResponse() {
		addProtectedResourceUrl();
		callAndTrackFailure(GetResourcesOASValidatorV3.class);

		validateAccountsApi();

		validateCreditCardsApi();

		if (isBusiness()) {
			validateCustomerBusinessApi();
		} else {
			validateCustomerPersonalApi();
		}

		validateFinancingsApi();

		validateInvoiceFinancingsApi();

		validateLoansApi();

		validateBankFixedIncomesApi();

		validateCreditFixedIncomesApi();

		validateVariableIncomesApi();

		validateFundsApi();

		validateTreasureTitlesApi();

		validateExchangesApi();

		callAndContinueOnFailure(CheckAmountOfFailures.class, Condition.ConditionResult.FAILURE);
		eventLog.log("Endpoints requested during execution", env.getObject(LIST_OF_ENDPOINTS_ENV_KEY));
	}

	private void validateAccountsApi() {
		runInBlock("Validate accounts API", () -> {
			callAndContinueOnFailure(GetOptionalAccountsV2Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "accounts";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch accounts V2");
			callAndTrackFailure(GetAccountsListOASValidatorV2n4.class);

			callAndContinueOnFailure(AccountSelector.class, Condition.ConditionResult.INFO);
			if(env.getString("accountId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingAccountResource.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch account V2");
				callAndTrackFailure(GetAccountsIdentificationOASValidatorV2n4.class);

				callAndContinueOnFailure(PrepareUrlForFetchingAccountBalances.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch account balance V2");
				callAndTrackFailure(GetAccountsBalancesOASValidatorV2n4.class);

				callAndContinueOnFailure(PrepareUrlForFetchingAccountTransactions.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch account transactions V2");
				callAndTrackFailure(GetAccountTransactionsOASValidatorV2n4.class);

				callAndContinueOnFailure(PrepareUrlForFetchingAccountLimits.class, Condition.ConditionResult.FAILURE);
				preCallProtectedResource("Fetch account limits V2");
				callAndTrackFailure(GetAccountsLimitsOASValidatorV2n4.class);
			}
			env.removeNativeValue("accountId");
		});
	}

	public static class GetOptionalAccountsV2Endpoint extends GetAccountsV2Endpoint {

		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateCreditCardsApi() {
		runInBlock("Validate credit card API", () -> {
			callAndContinueOnFailure(GetOptionalCreditCardsAccountsV2Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "credit-cards";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch credit card accounts V2");
			callAndTrackFailure(GetCreditCardAccountsListOASValidatorV2.class);

			callAndContinueOnFailure(CardAccountSelector.class, Condition.ConditionResult.INFO);
			if(env.getString("accountId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingAccountResource.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch credit card details V2");
				callAndTrackFailure(GetCreditCardAccountsIdentificationOASValidatorV2.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCardLimits.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch card limits V2");
				callAndTrackFailure(GetCreditCardAccountsLimitsOASValidatorV2.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCardTransactions.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				preCallProtectedResource("Fetch card transactions V2");
				callAndTrackFailure(GetCreditCardAccountsTransactionsOASValidatorV2.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCardBills.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromDueDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch card bills V2");
				callAndTrackFailure(GetCreditCardAccountsBillsOASValidatorV2.class);

				callAndContinueOnFailure(CardBillSelector.class, Condition.ConditionResult.INFO);
				if(env.getString("billId")!=null) {
					callAndContinueOnFailure(PrepareUrlForFetchingBillTransactionResource.class, Condition.ConditionResult.FAILURE);
					callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
					addProtectedResourceUrl();
					preCallProtectedResource("Fetch credit card bill transaction V2");
					callAndTrackFailure(GetCreditCardAccountsBillsTransactionsOASValidatorV2.class);
					env.removeNativeValue("billId");

				}
			}
			env.removeNativeValue("accountId");
		});
	}

	public static class GetOptionalCreditCardsAccountsV2Endpoint extends GetCreditCardsAccountsV2Endpoint {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateCustomerBusinessApi() {
		runInBlock("Validate customer business API", () -> {
			callAndContinueOnFailure(GetOptionalBusinessIdentificationsV2Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "customer-business";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(PrepareToGetBusinessQualifications.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch business qualification V2");
			callAndTrackFailure(BusinessQualificationResponseOASValidatorV2.class);

			callAndContinueOnFailure(PrepareToGetBusinessIdentifications.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch business identifications V2");
			callAndTrackFailure(BusinessIdentificationOASValidatorV2.class);

			callAndContinueOnFailure(PrepareToGetBusinessFinancialRelations.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch business financial relations V2");
			callAndTrackFailure(BusinessRelationsResponseOASValidatorV2.class);
		});
	}

	public static class GetOptionalBusinessIdentificationsV2Endpoint extends GetBusinessIdentificationsV2Endpoint {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateCustomerPersonalApi() {
		runInBlock("Validate customer personal API", () -> {
			callAndContinueOnFailure(GetOptionalPersonalIdentificationsV2Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "customer-personal";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(PrepareToGetPersonalQualifications.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch personal qualifications V2");
			callAndTrackFailure(PersonalQualificationResponseOASValidatorV2.class);

			callAndContinueOnFailure(PrepareToGetPersonalIdentifications.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch personal identifications V2");
			callAndTrackFailure(PersonalIdentificationResponseOASValidatorV2.class);

			callAndContinueOnFailure(PrepareToGetPersonalFinancialRelationships.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch personal financial relations V2");
			callAndTrackFailure(PersonalRelationsResponseOASValidatorV2.class);
		});
	}

	public static class GetOptionalPersonalIdentificationsV2Endpoint extends GetPersonalIdentificationsV2Endpoint {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateFinancingsApi() {
		runInBlock("Validate financings API", () -> {
			env.putString("api_type", EnumResourcesType.FINANCING.name());
			callAndContinueOnFailure(GetOptionalFinancingsV2Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "financings";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch contracts V2");
			callAndTrackFailure(GetFinancingsListV2n3OASValidator.class);

			callAndContinueOnFailure(CreditOperationsContractSelector.class, Condition.ConditionResult.INFO);
			if(env.getString("contractId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContract.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract V2");
				callAndTrackFailure(GetFinancingsIdentificationV2n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractGuarantees.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract guarantees V2");
				callAndTrackFailure(GetFinancingsWarrantiesV2n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractPayments.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract payments V2");
				callAndTrackFailure(GetFinancingsPaymentsV2n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractInstallments.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract instalments V2");
				callAndTrackFailure(GetFinancingsScheduledInstalmentsV2n3OASValidator.class);
			}
			env.removeNativeValue("contractId");
		});
	}

	public static class GetOptionalFinancingsV2Endpoint extends GetFinancingsV2Endpoint {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateInvoiceFinancingsApi() {
		runInBlock("Validate invoice financings API", () -> {
			env.putString("api_type", EnumResourcesType.INVOICE_FINANCING.name());
			callAndContinueOnFailure(GetOptionalInvoiceFinancingsV2Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "invoice-financings";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch contracts V2");
			callAndTrackFailure(GetInvoiceFinancingsListV2n3OASValidator.class);

			callAndContinueOnFailure(CreditOperationsContractSelector.class, Condition.ConditionResult.INFO);
			if(env.getString("contractId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContract.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract V2");
				callAndTrackFailure(GetInvoiceFinancingsIdentificationV2n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractGuarantees.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract guarantees V2");
				callAndTrackFailure(GetInvoiceFinancingsWarrantiesV2n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractPayments.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract payments V2");
				callAndTrackFailure(GetInvoiceFinancingsPaymentsV2n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractInstallments.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract instalments V2");
				callAndTrackFailure(GetInvoiceFinancingsScheduledInstalmentsV2n3OASValidator.class);
			}
			env.removeNativeValue("contractId");

		});
	}

	public static class GetOptionalInvoiceFinancingsV2Endpoint extends GetInvoiceFinancingsV2Endpoint {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateLoansApi() {
		runInBlock("Validate loans API", () -> {
			env.putString("api_type", EnumResourcesType.LOAN.name());
			callAndContinueOnFailure(GetOptionalLoansV2Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "loans";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch contracts V2");
			callAndTrackFailure(GetLoansListV2n4OASValidator.class);

			callAndContinueOnFailure(CreditOperationsContractSelector.class, Condition.ConditionResult.INFO);
			if(env.getString("contractId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContract.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract V2");
				callAndTrackFailure(GetLoansIdentificationV2n4OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractGuarantees.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract guarantees V2");
				callAndTrackFailure(GetLoansWarrantiesV2n4OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractPayments.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract payments V2");
				callAndTrackFailure(GetLoansPaymentsV2n4OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingCreditOperationsContractInstallments.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch contract instalments V2");
				callAndTrackFailure(GetLoansScheduledInstalmentsV2n4OASValidator.class);
			}
			env.removeNativeValue("contractId");

		});
	}

	public static class GetOptionalLoansV2Endpoint extends GetLoansV2Endpoint {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateBankFixedIncomesApi() {
		runInBlock("Validate bank fixed incomes API", () -> {
			callAndContinueOnFailure(SetInvestmentApiToBankFixedIncomes.class, Condition.ConditionResult.FAILURE);
			env.putString("api_type", EnumResourcesType.BANK_FIXED_INCOME.name());
			callAndContinueOnFailure(GetOptionalInvestmentsV1FromAuthServer.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "bank-fixed-incomes";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch investments V1");
			callAndTrackFailure(GetBankFixedIncomesListOASValidatorV1n4.class);

			callAndContinueOnFailure(ExtractInvestmentID.class, Condition.ConditionResult.INFO);
			if(env.getString("investmentId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingInvestment.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment V1");
				callAndTrackFailure(GetBankFixedIncomesIdentificationOASValidatorV1n4.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentBalances.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment balances V1");
				callAndTrackFailure(GetBankFixedIncomesBalancesOASValidatorV1n4.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactions.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment transactions V1");
				callAndTrackFailure(GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investments transactions current V1");
				callAndTrackFailure(GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4.class);
			}
			env.removeNativeValue("investmentId");

		});
	}

	private void validateCreditFixedIncomesApi() {
		runInBlock("Validate credit fixed incomes API", () -> {
			callAndContinueOnFailure(SetInvestmentApiToCreditFixedIncomes.class, Condition.ConditionResult.FAILURE);
			env.putString("api_type", EnumResourcesType.CREDIT_FIXED_INCOME.name());
			callAndContinueOnFailure(GetOptionalInvestmentsV1FromAuthServer.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "credit-fixed-incomes";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch investments V1");
			callAndTrackFailure(GetCreditFixedIncomesListV1n3OASValidator.class);

			callAndContinueOnFailure(ExtractInvestmentID.class, Condition.ConditionResult.INFO);
			if(env.getString("investmentId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingInvestment.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment V1");
				callAndTrackFailure(GetCreditFixedIncomesIdentificationV1n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentBalances.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment balances V1");
				callAndTrackFailure(GetCreditFixedIncomesBalancesV1n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactions.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment transactions V1");
				callAndTrackFailure(GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investments transactions current V1");
				callAndTrackFailure(GetCreditFixedIncomesTransactionsCurrentV1n3OASValidator.class);
			}
			env.removeNativeValue("investmentId");

		});
	}

	private void validateVariableIncomesApi() {
		runInBlock("Validate variable incomes API", () -> {
			callAndContinueOnFailure(SetInvestmentApiToVariableIncomes.class, Condition.ConditionResult.FAILURE);
			env.putString("api_type", EnumResourcesType.VARIABLE_INCOME.name());
			callAndContinueOnFailure(GetOptionalInvestmentsV1FromAuthServer.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "variable-incomes";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch investments V1");
			callAndTrackFailure(GetVariableIncomesListV1n21OASValidator.class);

			callAndContinueOnFailure(ExtractInvestmentID.class, Condition.ConditionResult.INFO);
			if(env.getString("investmentId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingInvestment.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment V1");
				callAndTrackFailure(GetVariableIncomeIdentificationV1n21OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentBalances.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment balances V1");
				callAndTrackFailure(GetVariableIncomesBalancesV1n21OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactions.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment transactions V1");
				callAndTrackFailure(GetVariableIncomesTransactionsCurrentV1n21OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investments transactions current V1");
				callAndTrackFailure(GetVariableIncomesTransactionsCurrentV1n21OASValidator.class);
			}
			env.removeNativeValue("investmentId");

		});
	}

	private void validateFundsApi() {
		runInBlock("Validate funds API", () -> {
			callAndContinueOnFailure(SetInvestmentApiToFunds.class, Condition.ConditionResult.FAILURE);
			env.putString("api_type", EnumResourcesType.FUND.name());
			callAndContinueOnFailure(GetOptionalInvestmentsV1FromAuthServer.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "funds";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch investments V1");
			callAndTrackFailure(GetFundsListV1OASValidator.class);

			callAndContinueOnFailure(ExtractInvestmentID.class, Condition.ConditionResult.INFO);
			if(env.getString("investmentId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingInvestment.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment V1");
				callAndTrackFailure(GetFundsIdentificationV1OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentBalances.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment balances V1");
				callAndTrackFailure(GetFundsBalancesV1OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactions.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment transactions V1");
				callAndTrackFailure(GetFundsTransactionsCurrentV1OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investments transactions current V1");
				callAndTrackFailure(GetFundsTransactionsCurrentV1OASValidator.class);
			}
			env.removeNativeValue("investmentId");

		});
	}

	private void validateTreasureTitlesApi() {
		runInBlock("Validate treasure titles API", () -> {
			callAndContinueOnFailure(SetInvestmentApiToTreasureTitles.class, Condition.ConditionResult.FAILURE);
			env.putString("api_type", EnumResourcesType.FUND.name());
			callAndContinueOnFailure(GetOptionalInvestmentsV1FromAuthServer.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "treasure-titles";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch investments V1");
			callAndTrackFailure(GetTreasureTitlesListV1OASValidator.class);

			callAndContinueOnFailure(ExtractInvestmentID.class, Condition.ConditionResult.INFO);
			if(env.getString("investmentId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingInvestment.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment V1");
				callAndTrackFailure(GetTreasureTitleIdentificationV1OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentBalances.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment balances V1");
				callAndTrackFailure(GetTreasureTitleBalancesV1OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactions.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investment transactions V1");
				callAndTrackFailure(GetTreasureTitlesTransactionsCurrentV1OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch investments transactions current V1");
				callAndTrackFailure(GetTreasureTitlesTransactionsCurrentV1OASValidator.class);
			}
			env.removeNativeValue("investmentId");

		});
	}

	public static class GetOptionalInvestmentsV1FromAuthServer extends GetInvestmentsV1FromAuthServer {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private void validateExchangesApi() {
		runInBlock("Validate exchanges API", () -> {
			env.putString("api_type", EnumResourcesType.EXCHANGE.name());
			callAndContinueOnFailure(GetOptionalExchangesV1Endpoint.class, Condition.ConditionResult.FAILURE);
			if(shouldSkipValidation()) {return;}

			currentApi = "exchanges";
			addCurrentApiToFailuresList();

			callAndContinueOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class, Condition.ConditionResult.FAILURE);
			addProtectedResourceUrl();
			preCallProtectedResource("Fetch exchanges V1");
			callAndTrackFailure(GetExchangesOperationsListV1OASValidator.class);

			callAndContinueOnFailure(ExtractOperationID.class, Condition.ConditionResult.INFO);
			if(env.getString("operationId") != null) {

				callAndContinueOnFailure(PrepareUrlForFetchingOperation.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch operation V1");
				callAndTrackFailure(GetExchangesOperationIdentificationV1OASValidator.class);

				callAndContinueOnFailure(PrepareUrlForFetchingOperationEvents.class, Condition.ConditionResult.FAILURE);
				addProtectedResourceUrl();
				preCallProtectedResource("Fetch operation events V1");
				callAndTrackFailure(GetExchangesEventsV1OASValidator.class);
			}
			env.removeNativeValue("investmentId");

		});
	}

	public static class GetOptionalExchangesV1Endpoint extends GetExchangesV1Endpoint {
		@Override
		protected void performUrlNotFoundAction(Environment env) {
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, true);
		}
	}

	private boolean isBusiness() {
		return "business".equals(env.getString("config", "consent.productType"));
	}

	private boolean shouldSkipValidation() {
		boolean shouldSkip = env.getBoolean(SKIP_VALIDATION_ENV_KEY);
		if(shouldSkip) {
			eventLog.log("Skipping validation", "Skipping validation since API is not registered");
			// Make sure the variable must be set again to skip a validation.
			env.putBoolean(SKIP_VALIDATION_ENV_KEY, false);
		}
		return shouldSkip;
	}

	private void addProtectedResourceUrl() {
		String url = env.getString("protected_resource_url");
		env.getObject(LIST_OF_ENDPOINTS_ENV_KEY).getAsJsonArray(LIST_OF_ENDPOINTS_ENV_KEY).add(url);
	}

	@Override
	protected void preCallProtectedResource() {
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
		callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-5");
		callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");

		callAndTrackFailure(EnsureResourceResponseCodeWas200.class);
		callAndTrackFailure(CheckForDateHeaderInResourceResponse.class);
		callAndTrackFailure(CheckForFAPIInteractionIdInResourceResponse.class);
		callAndTrackFailure(EnsureResourceResponseReturnedJsonContentType.class);
	}

	private void addCurrentApiToFailuresList() {
		JsonObject apiFailures = env.getObject(CheckAmountOfFailures.LIST_OF_API_FAILURES_ENV_KEY);
		apiFailures.addProperty(currentApi, 0);
	}

	private void incrementCurrentApiFailures() {
		JsonObject apiFailures = env.getObject(CheckAmountOfFailures.LIST_OF_API_FAILURES_ENV_KEY);
		int currentFailures = OIDFJSON.getInt(apiFailures.get(currentApi));
		apiFailures.addProperty(currentApi, currentFailures + 1);
	}

	private void callAndTrackFailure(Class<? extends Condition> conditionClass) {
		try {
			call(condition(conditionClass));
		} catch (TestFailureException ex) {
			incrementCurrentApiFailures();
		}
	}

}
