package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2n1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiMultipleConsentsCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.GetEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostEnrollmentsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostFidoRegistrationOptionsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1.PostFidoSignOptionsOASValidatorV2n1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_multiple-consents-core_test-module_v2-1",
	displayName = "enrollments_api_multiple-consents-core_test-module_v2-1",
	summary = "Ensure multiple consents payment reaches an accepted state when a payment without redirect is executed. This test will use the CPF/CNPJ on the config for \"Payment consent - XXX - Multiple Consents Test\", or Skipped if the field is not filled\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Execute a full enrollment journey sending the origin extracted above, extract the enrollment ID, and store the refresh_token generated ensuring it has the nrp-consents scope\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AUTHORISED\"\n" +
		"• Call POST consent with valid payload\n" +
		"• Expects 201 - Validate response\n" +
		"• Call the POST sign-options endpoint with the consentID created\n" +
		"• Expect 201 - Validate response and extract challenge\n" +
		"• Calls the POST Token endpoint with the refresh_token grant, ensuring a token with the nrp-consent scope was issued\n" +
		"• Call POST consent authorize with valid payload, signing the challenge with the compliant private key\n" +
		"• Expects 204\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"PARTIALLY_ACCEPTED\"\n" +
		"• Poll the GET consents endpoint while status is \"PARTIALLY_ACCEPTED\"\n" +
		"• Call the GET consents endpoint\n" +
		"• Expect 200 - Validate Response and ensure status is \"AUTHORISED\"\n" +
		"• Calls the POST Payments Endpoint, using the access token with the nrp-consents, authorisationFlow as FIDO_FLOW and consentId with the corresponding ConsentID\n" +
		"• Expects 201 - Validate Response\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"• Expects Accepted state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"conditionalResources.brazilCpfJointAccount",
		"conditionalResources.brazilCnpjJointAccount"
	}
)
public class EnrollmentsApiMultipleConsentsCoreTestModuleV2n1 extends AbstractEnrollmentsApiMultipleConsentsCoreTestModule {

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentConsentsV4Endpoint.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postFidoSignOptionsValidator() {
		return PostFidoSignOptionsOASValidatorV2n1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2n1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2n1.class;
	}
}
