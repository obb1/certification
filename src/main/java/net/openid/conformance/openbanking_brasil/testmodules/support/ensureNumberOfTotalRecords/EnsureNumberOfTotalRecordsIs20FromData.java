package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public class EnsureNumberOfTotalRecordsIs20FromData extends AbstractEnsureNumberOfTotalRecordsFromData {

	@Override
	protected  TotalRecordsComparisonOperator getComparisonMethod(){
		return TotalRecordsComparisonOperator.EQUAL;
	}

	@Override
	protected int getTotalRecordsComparisonAmount(){
		return 20;
	}

}



