package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetBusinessIdentificationsV2Endpoint extends AbstractGetXFromAuthServer {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/customers\\/v\\d+\\/business\\/identifications)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "customers-business";
	}

	@Override
	protected String getApiVersionRegex() {
		return  "^(2.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
