package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.text.ParseException;

public class CopyOriginalRecurringPaymentFieldsFromResponseToRequestBody extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = { RESPONSE_ENV_KEY, "resource" })
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		JsonObject data = response.getAsJsonObject("data");

		JsonObject requestData = data.deepCopy();
		updateRequestData(requestData);
		JsonObject request = new JsonObjectBuilder().addField("data", requestData).build();

		env.putObject("resource", "brazilPixPayment", request);
		logSuccess("Created recurring-payment request body based on GET recurring-payments/{recurringPaymentdId} response",
			args("request body", request));

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	private void updateRequestData(JsonObject requestData) {
		String recurringPaymentId = OIDFJSON.getString(requestData.get("recurringPaymentId"));
		requestData.addProperty("originalRecurringPaymentId", recurringPaymentId);
		requestData.remove("recurringPaymentId");
		requestData.remove("creationDateTime");
		requestData.remove("statusUpdateDateTime");
		requestData.remove("status");
		requestData.remove("rejectionReason");
		requestData.remove("debtorAccount");
		requestData.remove("cancellation");
	}
}
