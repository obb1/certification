package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SetConsentIdInPaymentsRequestBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource", strings = "consent_id")
	public Environment evaluate(Environment env) {
		JsonElement dataElement = Optional.ofNullable(
			env.getElementFromObject("resource", "brazilPixPayment.data")
		).orElseThrow(() -> error("Could not find payment data in the environment"));
		String consentId = env.getString("consent_id");

		if (dataElement.isJsonArray()) {
			JsonArray data = dataElement.getAsJsonArray();
			for (JsonElement paymentElement : data) {
				JsonObject payment = paymentElement.getAsJsonObject();
				payment.addProperty("consentId", consentId);
			}
		} else if (dataElement.isJsonObject()) {
			JsonObject data = dataElement.getAsJsonObject();
			data.addProperty("consentId", consentId);
		} else {
			throw error("Payment is not of a valid type", args("payment data", dataElement));
		}

		logSuccess("Successfully added consentId field to payments request body", args("payments data", dataElement));

		return env;
	}
}
