package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.http.HttpMethod;

public class GetRecurringPaymentPixListOASValidatorV2 extends AbstractRecurringPaymentPixOASValidatorV2 {

	@Override
	protected String getEndpointPath() {
		return "/pix/recurring-payments";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonArray("data"));
	}

	protected void assertData(JsonArray data) {
		for (JsonElement paymentDataElement : data) {
			JsonObject paymentData = paymentDataElement.getAsJsonObject();
			assertField1IsRequiredWhenField2HasValue2(paymentData, "rejectionReason", "status", "RJCT");
		}
	}
}
