package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PostEnrollmentsFidoRegistrationErrorEnum {
    STATUS_VINCULO_INVALIDO, ORIGEM_FIDO_INVALIDA, RP_INVALIDA, CHALLENGE_INVALIDO, PUBLIC_KEY_INVALIDA, EXTENSION_INVALIDA;

    public static Set<String> toSet(){
        return Stream.of(values())
            .map(Enum::name)
            .collect(Collectors.toSet());
    }
}
