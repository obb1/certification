package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class EditPaymentConsentRequestBodyToIncludeDefinedCreditor extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");

		JsonObject data = Optional.ofNullable(resource.getAsJsonObject("brazilPaymentConsent"))
			.map(consent -> consent.getAsJsonObject("data"))
			.orElseThrow(() -> error("Could not find data inside payment consent request body"));

		String name = getCreditorFieldFromResource(resource, "Name");
		String cpfCnpj = getCreditorFieldFromResource(resource, "CpfCnpj");
		String personType = switch (cpfCnpj.length()) {
			case 11 -> "PESSOA_NATURAL";
			case 14 -> "PESSOA_JURIDICA";
			default -> throw error("The value in creditorCpfCnpj should be either a CPF or a CNPJ",
				args("creditorCpfCnpj", cpfCnpj));
		};

		JsonObject creditor = new JsonObjectBuilder()
			.addField("name", name)
			.addField("cpfCnpj", cpfCnpj)
			.addField("personType", personType)
			.build();

		data.add("creditor", creditor);
		logSuccess("User defined creditor has been added to the payment consent request body",
			args("payment data", data));

		return env;
	}

	protected String getCreditorFieldFromResource(JsonObject resource, String fieldName) {
		return Optional.ofNullable(resource.get("creditor" + fieldName))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error(String.format("Could not find creditor%s in the resource", fieldName), args("resource", resource)));
	}
}
