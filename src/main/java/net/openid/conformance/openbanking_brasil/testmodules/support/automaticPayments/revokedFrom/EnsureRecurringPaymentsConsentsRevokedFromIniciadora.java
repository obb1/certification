package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revokedFrom;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevokedFromEnum;

public class EnsureRecurringPaymentsConsentsRevokedFromIniciadora extends AbstractEnsureRecurringPaymentsConsentsRevokedFrom {

	@Override
	protected RecurringPaymentsConsentsRevokedFromEnum expectedRevokedFrom() {
		return RecurringPaymentsConsentsRevokedFromEnum.INICIADORA;
	}
}
