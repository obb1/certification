package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;
import java.util.Optional;

public class EnsureEnrollmentDoesNotContainExpirationDateTime extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	public Environment evaluate(Environment env) {
		JsonObject body = extractResponseFromEnv(env);
		JsonObject data = Optional.ofNullable(body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Could not find data inside enrollment response body"));

		if (data.has("expirationDateTime")) {
			throw error("No expirationDateTime was expected in the enrollment response", args("data", data));
		}

		logSuccess("No expirationDateTime was returned, as expected");

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}
}
