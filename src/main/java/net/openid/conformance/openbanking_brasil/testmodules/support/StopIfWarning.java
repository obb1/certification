package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.StringUtils;

public class StopIfWarning extends AbstractCondition {
	@Override
	public Environment evaluate(Environment env) {
		String warning = env.getString("warning_message");
		if(!StringUtils.isBlank(warning)) {
			throw error(warning);
		}

		return env;
	}
}
