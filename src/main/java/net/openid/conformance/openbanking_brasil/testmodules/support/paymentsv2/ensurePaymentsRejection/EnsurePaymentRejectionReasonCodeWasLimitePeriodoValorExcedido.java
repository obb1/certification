package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsRejectionReasonEnum;

import java.util.List;

public class EnsurePaymentRejectionReasonCodeWasLimitePeriodoValorExcedido extends AbstractEnsurePaymentRejectionReasonCodeWasX {

	@Override
	protected List<String> getExpectedRejectionCodes() {
		return List.of(RecurringPaymentsRejectionReasonEnum.LIMITE_PERIODO_VALOR_EXCEDIDO.toString());
	}
}
