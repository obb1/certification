package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.AbstractGetLoansIdentificationOASValidator;


public class GetLoansIdentificationV2n4OASValidator extends AbstractGetLoansIdentificationOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/loans/loans-v2.4.0.yml";
	}
}
