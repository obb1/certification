package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditPaymentConsentRequestBodyToIncludeDefinedProxyIfPresent extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");

		JsonObject paymentDetails = Optional.ofNullable(resource.getAsJsonObject("brazilPaymentConsent"))
			.map(consent -> consent.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("payment"))
			.map(payment -> payment.getAsJsonObject("details"))
			.orElseThrow(() -> error("Could not find data.payment.details inside payment consent request body"));

		Optional<String> proxyOptional = Optional.ofNullable(resource.get("creditorProxy")).map(OIDFJSON::getString);

		if (proxyOptional.isPresent()) {
			String proxy = proxyOptional.get();
			paymentDetails.addProperty("proxy", proxy);
			logSuccess("User defined proxy has been added to the payment consent request body",
				args("payment details", paymentDetails));
		} else {
			log("No proxy defined, proceeding without adding proxy to the payment consent request body",
				args("payment details", paymentDetails));
		}

		return env;
	}
}
