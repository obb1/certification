package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class EditPaymentConsentRequestBodyToIncludeDefinedCreditorAccount extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");

		JsonObject paymentDetails = Optional.ofNullable(resource.getAsJsonObject("brazilPaymentConsent"))
			.map(consent -> consent.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("payment"))
			.map(payment -> payment.getAsJsonObject("details"))
			.orElseThrow(() -> error("Could not find data.payment.details inside payment consent request body"));

		JsonObject creditorAccount = new JsonObjectBuilder()
			.addField("ispb", getCreditorAccountFieldFromResource(resource, "Ispb"))
			.addField("issuer", getCreditorAccountFieldFromResource(resource, "Issuer"))
			.addField("number", getCreditorAccountFieldFromResource(resource, "Number"))
			.addField("accountType", getCreditorAccountFieldFromResource(resource, "AccountType"))
			.build();

		paymentDetails.add("creditorAccount", creditorAccount);
		logSuccess("User defined creditorAccount has been added to the payment consent request body",
			args("payment details", paymentDetails));

		return env;
	}

	protected String getCreditorAccountFieldFromResource(JsonObject resource, String fieldName) {
		return Optional.ofNullable(resource.get("creditorAccount" + fieldName))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error(String.format("Could not find creditorAccount %s in the resource", fieldName), args("resource", resource)));
	}
}
