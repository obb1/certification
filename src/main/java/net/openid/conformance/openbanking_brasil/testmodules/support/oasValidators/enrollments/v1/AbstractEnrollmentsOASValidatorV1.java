package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;

import java.util.List;

public abstract class AbstractEnrollmentsOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	protected abstract String getEndpointPathSuffix();

	protected String getEndpointPathApi() {
		return "/enrollments";
	}

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/enrollments/swagger-enrollments-1.3.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return getEndpointPathApi() + getEndpointPathSuffix();
	}

	protected void assertDebtorAccountIssuerConstraint(JsonObject data) {
		JsonObject debtorAccount = data.getAsJsonObject( "debtorAccount");
		if (debtorAccount != null) {
			assertField1IsRequiredWhenField2HasValue2(
				debtorAccount,
				"issuer",
				"accountType",
				List.of("CACC","SVGS")
			);
		}
	}
}
