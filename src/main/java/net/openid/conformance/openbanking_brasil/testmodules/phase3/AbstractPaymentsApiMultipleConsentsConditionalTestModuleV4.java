package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.EnsureExpirationDateTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureErrorResponse.EnsureErrorResponseCodeFieldWasConsentimentoPendeteAutorizacao;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus.CheckPaymentConsentPollStatusWasPartiallyAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasPartiallyAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPaymentsApiMultipleConsentsConditionalTestModuleV4 extends AbstractPaymentsApiMultipleConsentsConditionalTestModule{

	protected boolean firstPost = true;

	protected abstract Class<? extends Condition> paymentInitiationErrorValidator();

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckStatus("PARTIALLY_ACCEPTED", new EnsurePaymentConsentStatusWasPartiallyAccepted());
		env.unmapKey("access_token");
		validateGetConsentResponse();

		eventLog.startBlock(currentClientString() + "Call token endpoint");
		createAuthorizationCodeRequest();
		requestAuthorizationCode();
		requestProtectedResource();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void validateGetConsentResponse() {
		super.validateGetConsentResponse();
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureExpirationDateTime.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");
	}

	@Override
	protected ConditionSequence postPaymentValidationSequence() {
		if (firstPost) {
			firstPost = false;
			return sequenceOf(
				condition(paymentInitiationErrorValidator())
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE),
				condition(EnsureErrorResponseCodeFieldWasConsentimentoPendeteAutorizacao.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
			);
		}
		return super.postPaymentValidationSequence();
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas422.class));
	}

	@Override
	protected void validateResponse() {
		env.mapKey("endpoint_response","resource_endpoint_response_full");
		call(postPaymentValidationSequence());
		env.unmapKey("endpoint_response");
		useClientCredentialsAccessToken();

		repeatSequence(this::getRepeatConsentSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(19)
			.validationSequence(this::getPaymentConsentValidationSequence)
			.run();

		userAuthorisationCodeAccessToken();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		call(new CallPixPaymentsEndpointSequence());
		eventLog.endBlock();

		eventLog.startBlock(currentClientString() + "Validate response");
		validateSecondPostResponse();
	}

	protected void validateSecondPostResponse() {
		call(postPaymentValidationSequence());
		useClientCredentialsAccessToken();

		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		validateFinalState();
	}

	protected ConditionSequence getRepeatConsentSequence() {
		return new ValidateSelfEndpoint()
			.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class))
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(EnsureResourceResponseCodeWas200.class, condition(getConsentPollStatusCondition()));
	}

	protected Class<? extends Condition> getConsentPollStatusCondition() {
		return CheckPaymentConsentPollStatusWasPartiallyAccepted.class;
	}

	protected ConditionSequence getPaymentConsentValidationSequence() {
		return sequenceOf(
			exec().mapKey("consent_endpoint_response_full", "resource_endpoint_response_full"),
			condition(getPaymentConsentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE),
			exec().unmapKey("consent_endpoint_response_full")
		);
	}
}
