package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsRejectionReasonEnum;

import java.util.List;

public class EnsurePaymentRejectionReasonCodeWasLimitePeriodoQuantidadeExcedido extends AbstractEnsurePaymentRejectionReasonCodeWasX {

	@Override
	protected List<String> getExpectedRejectionCodes() {
		return List.of(RecurringPaymentsRejectionReasonEnum.LIMITE_PERIODO_QUANTIDADE_EXCEDIDO.toString());
	}
}
