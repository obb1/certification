package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditFidoRegistrationOptionsRequestBodyToSetInvalidRp;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas.EnsureEnrollmentRejectionReasonWasRejeitadoFalhaFido;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasRpInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistrationOptions;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractEnrollmentsApiInvalidRpIdTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void executeTestSteps() {
		runInBlock("Post fido-registration-options - Expects 422 RP_INVALIDA", () -> {
			ConditionSequence postFidoRegistrationOptionsSteps = new PostEnrollmentsResourceSteps(
				new PrepareToPostFidoRegistrationOptions(),
				CreateEnrollmentsFidoRegistrationOptionsRequestBody.class,
				true
			).insertBefore(SignEnrollmentsRequest.class, condition(EditFidoRegistrationOptionsRequestBodyToSetInvalidRp.class));
			call(postFidoRegistrationOptionsSteps);
		});

		runInBlock("Validate fido-registration-options error response", () -> {
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postFidoRegistrationOptionsValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasRpInvalida.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasRpInvalida.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasRpInvalida.RESPONSE_ENV_KEY);
		});

		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
		runInBlock("Validate enrollments rejection reason", () -> {
			callAndStopOnFailure(EnsureEnrollmentRejectionReasonWasRejeitadoFalhaFido.class);
		});
	}

	protected abstract Class<? extends Condition> postFidoRegistrationOptionsValidator();

}
