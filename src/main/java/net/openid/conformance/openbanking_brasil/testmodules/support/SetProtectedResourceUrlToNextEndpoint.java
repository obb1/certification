package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;


public class SetProtectedResourceUrlToNextEndpoint extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	@PostEnvironment(strings = "protected_resource_url")
	public Environment evaluate(Environment env) {

		JsonObject body;

		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		JsonObject links = body.getAsJsonObject("links");

		log("Ensure that there is a link to next.");
		if (!JsonHelper.ifExists(body, "$.links.next")) {
			log("'Next' link not found in the response.");
			throw error("'Next' link not found in the response.");
		}

		env.putString("protected_resource_url", OIDFJSON.getString(links.get("next")));
		logSuccess("'Next' link found and set as Resource Url");
		return env;
	}
}

