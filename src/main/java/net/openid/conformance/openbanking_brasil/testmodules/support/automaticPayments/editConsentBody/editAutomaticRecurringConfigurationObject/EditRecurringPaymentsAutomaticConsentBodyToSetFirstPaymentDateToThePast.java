package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class EditRecurringPaymentsAutomaticConsentBodyToSetFirstPaymentDateToThePast extends AbstractEditRecurringPaymentsAutomaticConsentBody {

	@Override
	protected void updateAutomaticObject(JsonObject automatic) {
		JsonObject firstPayment = Optional.ofNullable(automatic.getAsJsonObject("firstPayment"))
			.orElseThrow(() -> error("Unable to find firstPayment inside automatic object", args("automatic", automatic)));

		LocalDate yesterday = LocalDate.now(ZoneId.of("UTC-3")).minusDays(1);
		String yesterdayStr = yesterday.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

		firstPayment.addProperty("date", yesterdayStr);
	}

	@Override
	protected String logMessage() {
		return "set \"firstPayment.date\" field as a date in the past";
	}
}
