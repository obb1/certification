package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Map;
import java.util.Optional;

public class AddPaymentConsentRequestBodyToConfig extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		JsonElement brasilPaymentConsent = env.getElementFromObject("config", "resource.brazilPaymentConsentFvp");
		if (brasilPaymentConsent == null) {
			String loggedUserIdentification = extractFromConfigOrDie(env, "resource.loggedUserIdentification");
			String debtorAccountIspb = extractFromConfigOrDie(env, "resource.debtorAccountIspb");
			String debtorAccountIssuer = extractFromConfigOrDie(env, "resource.debtorAccountIssuer");
			String debtorAccountNumber = extractFromConfigOrDie(env, "resource.debtorAccountNumber");
			String debtorAccountType = extractFromConfigOrDie(env, "resource.debtorAccountType");
			String paymentAmount = extractFromConfigOrDie(env, "resource.paymentAmount");
			var businessEntityIdentification = Optional.ofNullable(env.getString("config", "resource.businessEntityIdentification"));

			LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

			JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
				.addFields("data.loggedUser.document", Map.of(
					"identification", loggedUserIdentification,
					"rel", "CPF")
				)
				.addFields("data.creditor",
					Map.of(
						"personType", DictHomologKeys.PROXY_EMAIL_PERSON_TYPE,
						"cpfCnpj", DictHomologKeys.PROXY_EMAIL_CPF,
						"name", DictHomologKeys.PROXY_EMAIL_OWNER_NAME)
				)
				.addFields("data.payment", Map.of(
					"type", "PIX",
					"currency", "BRL",
					"ibgeTownCode", DictHomologKeys.PROXY_EMAIL_STANDARD_IBGETOWNCODE,
					"amount", paymentAmount,
					"date", currentDate.toString())
				)
				.addFields("data.payment.details", Map.of(
					"localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT,
					"proxy", DictHomologKeys.PROXY_EMAIL)
				)
				.addFields("data.payment.details.creditorAccount", Map.of(
					"ispb", DictHomologKeys.PROXY_EMAIL_ISPB,
					"issuer", DictHomologKeys.PROXY_EMAIL_BRANCH_NUMBER,
					"number", DictHomologKeys.PROXY_EMAIL_ACCOUNT_NUMBER,
					"accountType", DictHomologKeys.PROXY_EMAIL_ACCOUNT_TYPE)
				)
				.addFields("data.debtorAccount", Map.of(
					"ispb", debtorAccountIspb,
					"issuer", debtorAccountIssuer,
					"number", debtorAccountNumber,
					"accountType", debtorAccountType)
				);

			businessEntityIdentification.ifPresent(identification -> jsonObjectBuilder
				.addFields("data.businessEntity.document", Map.of(
					"identification", identification,
					"rel", "CNPJ"
				)));


			JsonObject consentRequest = jsonObjectBuilder.build();
			env.putObject("config", "resource.brazilPaymentConsent", consentRequest);
			logSuccess("Consent request body was added to the config", args("consentRequest", consentRequest));
		} else {
			JsonObject brazilPaymentConsentJson = brasilPaymentConsent.getAsJsonObject();

			String loggedUserIdentification = OIDFJSON.getString(brazilPaymentConsentJson.getAsJsonObject("data").getAsJsonObject("loggedUser").getAsJsonObject("document").get("identification"));

			String debtorAccountIspb = OIDFJSON.getString(brazilPaymentConsentJson.getAsJsonObject("data").getAsJsonObject("payment").getAsJsonObject("details").getAsJsonObject("creditorAccount").get("ispb"));
			String debtorAccountIssuer = OIDFJSON.getString(brazilPaymentConsentJson.getAsJsonObject("data").getAsJsonObject("payment").getAsJsonObject("details").getAsJsonObject("creditorAccount").get("issuer"));
			String debtorAccountNumber = OIDFJSON.getString(brazilPaymentConsentJson.getAsJsonObject("data").getAsJsonObject("payment").getAsJsonObject("details").getAsJsonObject("creditorAccount").get("number"));
			String debtorAccountType = OIDFJSON.getString(brazilPaymentConsentJson.getAsJsonObject("data").getAsJsonObject("payment").getAsJsonObject("details").getAsJsonObject("creditorAccount").get("accountType"));
			String paymentAmount = OIDFJSON.getString(brazilPaymentConsentJson.getAsJsonObject("data").getAsJsonObject("payment").get("amount"));

			env.putString("config", "resource.loggedUserIdentification", loggedUserIdentification);
			env.putString("config", "resource.debtorAccountIspb", debtorAccountIspb);
			env.putString("config", "resource.debtorAccountIssuer", debtorAccountIssuer);
			env.putString("config", "resource.debtorAccountNumber", debtorAccountNumber);
			env.putString("config", "resource.debtorAccountType", debtorAccountType);
			env.putString("config", "resource.paymentAmount", paymentAmount);

			env.putObject("config", "resource.brazilPaymentConsent", brasilPaymentConsent.getAsJsonObject());
			logSuccess("Consent request body was added to the config", args("consentRequest", brasilPaymentConsent));
		}
		return env;
	}


	private String extractFromConfigOrDie(Environment env, final String path) {
		Optional<String> string = Optional.ofNullable(env.getString("config", path));
		return string.orElseThrow(() -> error(String.format("Unable to find element %s in config", path)));
	}
}
