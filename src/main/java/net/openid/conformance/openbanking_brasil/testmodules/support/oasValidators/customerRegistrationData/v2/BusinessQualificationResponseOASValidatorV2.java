package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

import java.util.Optional;


@ApiName("Business Qualification V2.2.0")
public class BusinessQualificationResponseOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/customers/swagger-customers-2.2.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/business/qualifications";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		this.assertEconomicActivitiesConstraint(data);
		this.assertInformedRevenueConstraint(data);
	}

	protected void assertEconomicActivitiesConstraint(JsonObject data) {
		JsonArray economicActivities = Optional.ofNullable(data.getAsJsonArray("economicActivities")).orElse(new JsonArray());
		int numberOfMainEconomicActivities = 0;
		for(JsonElement economicActivityElement : economicActivities) {
			JsonObject economicActivity = economicActivityElement.getAsJsonObject();
			if(OIDFJSON.getBoolean(economicActivity.get("isMain"))) {
				numberOfMainEconomicActivities++;
			}
		}

		if(numberOfMainEconomicActivities > 1) {
			throw error("only one element of data.economicActivities.isMain can be true", args(
				"numberOfMainEconomicActivities", numberOfMainEconomicActivities
			));
		}
	}

	protected void assertInformedRevenueConstraint(JsonObject data) {
		JsonObject informedRevenue = data.getAsJsonObject("informedRevenue");
		if(informedRevenue == null) {
			return;
		}

		if(!informedRevenue.has("frequency")) {
			return;
		}
		String frequency = OIDFJSON.getString(informedRevenue.get("frequency"));
		if("OUTROS".equals(frequency) && informedRevenue.get("frequencyAdditionalInfo") == null) {
			throw error("data.informedRevenue.frequencyAdditionalInfo is required when data.informedRevenue.frequency is \"OUTROS\"");
		}
	}

}
