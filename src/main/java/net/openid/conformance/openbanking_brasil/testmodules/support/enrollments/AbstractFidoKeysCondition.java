package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.KeyType;
import com.nimbusds.jose.jwk.RSAKey;
import com.webauthn4j.data.attestation.authenticator.COSEKey;
import com.webauthn4j.data.attestation.authenticator.EC2COSEKey;
import com.webauthn4j.data.attestation.authenticator.RSACOSEKey;
import com.webauthn4j.data.attestation.statement.COSEAlgorithmIdentifier;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.OIDFJSON;

import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractFidoKeysCondition extends AbstractCondition {


	protected KeyType getKeyTypeFromJwkJson(JsonObject jwkJson) {
		return Optional.ofNullable(jwkJson.get("kty"))
			.map(OIDFJSON::getString)
			.map(KeyType::parse)
			.orElseThrow(() -> error("Could not find kty in the fido_keys_jwk", Map.of("key", jwkJson)));
	}

	protected KeyPair getKeyPairFromJwkJson(JsonObject jwkJson) {
		try {
			KeyType kty = getKeyTypeFromJwkJson(jwkJson);
			if (kty.equals(KeyType.RSA)) {
				return RSAKey.parse(jwkJson.toString()).toKeyPair();
			} else if (kty.equals(KeyType.EC)) {
				return ECKey.parse(jwkJson.toString()).toKeyPair();
			} else {
				throw error("Unsupported FIDO key type", Map.of("key", kty));
			}
		} catch (ParseException | JOSEException e) {
			throw error("Could not parse FIDO JWK", Map.of("fido_jwk", jwkJson));
		}
	}

	protected Signature getSupportedSignatureInstance(KeyType keyType) {
		try {
			if (KeyType.EC.equals(keyType)) {
				return Signature.getInstance("SHA256withECDSA");
			}
			if (KeyType.RSA.equals(keyType)) {
				return Signature.getInstance("SHA256withRSA");
			}
		} catch (NoSuchAlgorithmException e) {
			throw error("Could not find specified signature algorithm");
		}
		throw error("Unsupported Key Type", Map.of("kty", keyType));
	}

	protected COSEAlgorithmIdentifier convertKeyTypeToCOSEAlgorithmIdentifier(KeyType keyType) {
		if (KeyType.RSA.equals(keyType)) {
			return COSEAlgorithmIdentifier.RS256;
		} else if (KeyType.EC.equals(keyType)) {
			return COSEAlgorithmIdentifier.ES256;
		}

		throw error("Unsupported Key Type", Map.of("kty", keyType));
	}

	protected COSEKey getCOSEKey(PublicKey publicKey, KeyType keyType) {
		if (KeyType.RSA.equals(keyType)) {
			return RSACOSEKey.create((RSAPublicKey) publicKey, COSEAlgorithmIdentifier.RS256);
		} else if (KeyType.EC.equals(keyType)) {
			return EC2COSEKey.create((ECPublicKey) publicKey, COSEAlgorithmIdentifier.ES256);
		}

		throw error("Unsupported Key Type", Map.of("kty", keyType));
	}

	protected byte[] getSha256Digest(byte[] data) {
		try {
			return MessageDigest.getInstance("SHA-256").digest(data);
		} catch (NoSuchAlgorithmException e) {
			throw error("Specified digest algorithm is not found", e);
		}
	}


}
