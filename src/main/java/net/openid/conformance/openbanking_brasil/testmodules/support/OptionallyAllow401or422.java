package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.http.HttpStatus;

public class OptionallyAllow401or422 extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {

		int statusCode = env.getInteger("resource_endpoint_response_full", "status");

		if (statusCode == HttpStatus.SC_UNAUTHORIZED) {
			env.putString("status_401", "ok");
			env.removeNativeValue("status_422");
		} else if (statusCode == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
			env.putString("status_422", "ok");
			env.removeNativeValue("status_401");
		} else {
			throw error("endpoint returned an unexpected http status - either 401 or 422 accepted", args("http_status", statusCode));
		}

		logSuccess("endpoint returned the expected http status", args("http_status", statusCode));

		return env;
	}
}
