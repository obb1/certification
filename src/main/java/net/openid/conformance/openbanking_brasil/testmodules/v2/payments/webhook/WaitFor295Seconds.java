package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import net.openid.conformance.condition.common.AbstractWaitForSpecifiedSeconds;
import net.openid.conformance.testmodule.Environment;

public class WaitFor295Seconds extends AbstractWaitForSpecifiedSeconds {
	@Override
	protected long getExpectedWaitSeconds(Environment env) {
		return 295;
	}
}
