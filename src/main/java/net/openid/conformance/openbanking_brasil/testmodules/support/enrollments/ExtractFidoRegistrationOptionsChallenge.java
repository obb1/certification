package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;

public class ExtractFidoRegistrationOptionsChallenge extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Error parsing JWT response");
		}
	}

	/**
	 * RESPONSE_ENV_KEY - resource_endpoint_response_full - should be either fido-registration-options or fido-sign-options responses
	 */

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    @PostEnvironment(strings = "fido_challenge")
    public Environment evaluate(Environment env) {

		JsonObject body = bodyFrom(env).getAsJsonObject();

		String challenge = Optional.ofNullable(body.getAsJsonObject("data"))
			.map(data -> data.get("challenge"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not extract data.challenge from the registration options response", Map.of("body", body)));

		if (!isBase64WithoutPadding(challenge)) {
			throw error("data.challenge is not a valid base64 encoded without padding string");
		}

		env.putString("fido_challenge", challenge);
		logSuccess("Successfully extracted challenge from fido-registration-options response",
			args("challenge", challenge));

        return env;
    }

	private boolean isBase64WithoutPadding(String challenge) {
		// This method checks for the padding character "=" and tries to decode the challenge string
		try {
			return !challenge.contains("=") && Base64.getUrlDecoder().decode(challenge).length > 0;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
}
