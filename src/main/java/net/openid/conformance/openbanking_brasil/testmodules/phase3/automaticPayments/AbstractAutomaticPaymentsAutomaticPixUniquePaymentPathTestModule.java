package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.AbstractEnsureRecurringPaymentsRejectionReason;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractAutomaticPaymentsAutomaticPixUniquePaymentPathTestModule extends AbstractAutomaticPaymentsAutomaticPixTestModule {
	/**
	 * This class should be extended by automatic pix tests that follow the steps bellow (what defines which bracket path will be taken is the method isHappyPath()):
	 *     - Call the POST recurring-consents endpoint with automatic pix fields, with referenceStartDate as D+1, interval as SEMANAL, a fixed Amount {value}, and sending firstPayment information with amount as 1 BRL, date as {value}, and creditorAccount information as {value}
	 *     - Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION
	 *     - Redirect the user to authorize consent
	 *     - Call the GET recurring-consents endpoint
	 *     - Expect 201 - Validate Response and ensure status is AUTHORISED
	 *     - Call the POST recurring-payments endpoint with the information {value}, and creditorAccount number as {value}
	 *     {
	 *         - Expect 422 {value} or 201 - Validate Response
	 *         If a 201 is returned:
	 *         - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP
	 *         - Call the GET recurring-payments {recurringPaymentId}
	 *         - Expect 200 - Validate Response and ensure status is {value}, ensure that rejectionReason.code is {value}
	 *     } OR
	 *     {
	 *         - Expect 201 - Validate Response
	 *         - Poll the GET recurring-payments {recurringPaymentId} while the Status is RCVD, ACCP
	 *         - Call the GET recurring-payments {recurringPaymentId}
	 *         - Expect 200 - Validate Response and ensure status is {value}
	 *     }
	 *     - Call the GET recurring-consents endpoint
	 *     - Expect 201 - Validate Response and ensure status is {value}
	 * In order to properly create the recurring-consents and recurring-payments request bodies, the user will need to provide the following fields:
	 *     - resource.debtorAccountIspb
	 *     - resource.debtorAccountIssuer
	 *     - resource.debtorAccountNumber
	 *     - resource.debtorAccountType
	 *     - resource.contractDebtorName
	 *     - resource.contractDebtorIdentification
	 *     - resource.creditorAccountIspb
	 *     - resource.creditorAccountIssuer
	 *     - resource.creditorAccountNumber
	 *     - resource.creditorAccountAccountType
	 *     - resource.creditorName
	 *     - resource.creditorCpfCnp
	 */

	protected abstract boolean isHappyPath();
	protected abstract AbstractEnsurePaymentStatusWasX finalPaymentStatusCondition();
	protected abstract AbstractEnsurePaymentConsentStatusWas finalConsentStatusCondition();
	protected abstract String finalConsentStatus();
	protected abstract AbstractEnsureErrorResponseCodeFieldWas errorResponseCodeFieldCondition(); // Will be implemented by non-happy-path tests
	protected abstract AbstractEnsureRecurringPaymentsRejectionReason ensureRejectionReasonCondition(); // Will be implemented by non-happy-path tests

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		if (isHappyPath()) {
			return super.getPixPaymentSequence();
		}
		return super.getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class));
	}

	@Override
	protected void validateResponse() {
		if (isHappyPath()) {
			super.validateResponse();
		} else {
			int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
			if (status == HttpStatus.CREATED.value()) {
				super.validateResponse();
			} else if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				callAndContinueOnFailure(postPaymentValidator(), Condition.ConditionResult.FAILURE);
				env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				callAndContinueOnFailure(errorResponseCodeFieldCondition().getClass(), Condition.ConditionResult.FAILURE);
				env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
			}
		}

		fetchConsentToCheckStatus(finalConsentStatus(), finalConsentStatusCondition());
	}

	@Override
	protected void validateFinalState() {
		callAndContinueOnFailure(finalPaymentStatusCondition().getClass(), Condition.ConditionResult.FAILURE);
		if (!isHappyPath()) {
			callAndContinueOnFailure(ensureRejectionReasonCondition().getClass(), Condition.ConditionResult.FAILURE);
		}
	}

	@Override
	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpV2.class;
	}
}
