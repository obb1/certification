package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class RemovePaymentConsentDate extends AbstractCondition {
	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject consentRequestPayment = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.flatMap(consent -> Optional.ofNullable(consent.getAsJsonObject()))
			.flatMap(consent -> Optional.ofNullable(consent.getAsJsonObject("data")))
			.flatMap(data -> Optional.ofNullable(data.getAsJsonObject("payment")))
			.orElseThrow(() -> error("Could not find brazilPaymentConsent.data.payment in the resource"));

		consentRequestPayment.remove("date");
		logSuccess("Remove date from the payment consent payment", args("consentRequestPayment", consentRequestPayment));
		return env;
	}
}
