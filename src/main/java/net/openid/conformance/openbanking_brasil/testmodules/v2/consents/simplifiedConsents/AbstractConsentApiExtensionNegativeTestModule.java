package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus180AndSaveTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus365;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus730;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus90;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIdefiniteConsentExtensionExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ExtractSavedConsentExtensionExpirationTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateConsents422ExpiracaoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;


public abstract class AbstractConsentApiExtensionNegativeTestModule extends AbstractSimplifiedConsentRenewalTestModule {
	boolean secondExtension = false;

	boolean thirdExtension = false;

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		if (secondExtension && !thirdExtension) {
			thirdExtension = true;
			return CreateExtensionRequestTimeDayPlus365.class;
		} else if (thirdExtension) {
			return(thirdExtensionExpiryTimeCondition());
		} else {
			secondExtension = true;
			return CreateExtensionRequestTimeDayPlus180AndSaveTime.class;
		}
	}

	protected abstract Class<? extends Condition> thirdExtensionExpiryTimeCondition();

	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response - D+180", this::callPostConsentExtensionEndpoint);
		runInBlock("Validating post consent extension response expecting error - D+90", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, CreateExtensionRequestTimeDayPlus90.class));
		});
		runInBlock("Validating post consent extension response expecting error - D+180", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, ExtractSavedConsentExtensionExpirationTime.class));
		});
		runInBlock("Validating post consent extension response - D+365", this::callPostConsentExtensionEndpoint);
		runInBlock("Validating post consent extension response expecting error - D+730", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, CreateExtensionRequestTimeDayPlus730.class));
		});
		runInBlock("Validating post consent extension response - Indefinite Expiration", this::callPostConsentExtensionEndpoint);
		runInBlock("Validating post consent extension response expecting error - Indefinite Expiration", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422ExpiracaoInvalida.class, CreateIdefiniteConsentExtensionExpiryTime.class));
		});
		runInBlock("Validating get consent extension response", this::callGetConsentExtensionEndpoint);
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetContentTypeApplicationJson.class);
		callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
		callAndStopOnFailure(CreateIndefiniteConsentExpiryTime.class);
		callAndStopOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
		ensureConsentResponseStatus();
		callAndContinueOnFailure(setConsentCreationValidation(), Condition.ConditionResult.FAILURE);
		call(exec().unmapKey("resource_endpoint_response_full"));
	}

	protected void ensureConsentResponseStatus() {
		callAndStopOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
	}

}
