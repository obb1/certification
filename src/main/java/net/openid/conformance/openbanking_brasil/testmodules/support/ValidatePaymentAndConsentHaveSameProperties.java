package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class ValidatePaymentAndConsentHaveSameProperties extends AbstractCondition {


	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");
		JsonObject consentRequest = resource
										.getAsJsonObject("brazilPaymentConsent")
										.getAsJsonObject("data")
										.getAsJsonObject("payment");

		JsonElement paymentData = resource.getAsJsonObject("brazilPixPayment").get("data");

		if (env.getString("payment_is_array") == null) {
			JsonObject payment = paymentData.getAsJsonObject();
			checkPaymentParameters(consentRequest, payment, env);
		} else {
			JsonArray paymentArray = paymentData.getAsJsonArray();
			for (JsonElement paymentElement : paymentArray) {
				JsonObject payment = paymentElement.getAsJsonObject();
				checkPaymentParameters(consentRequest, payment, env);
			}
		}

		logSuccess("Both payment and consent request match correctly.");

		return env;
	}

	private Environment checkPaymentParameters(JsonObject consentRequest, JsonObject paymentData, Environment env) {
		JsonObject paymentRequest = paymentData.getAsJsonObject("payment");

		String paymentAmount = OIDFJSON.getString(paymentRequest.get("amount"));
		String paymentCurrency = OIDFJSON.getString(paymentRequest.get("currency"));
		String consentAmount = OIDFJSON.getString(consentRequest.get("amount"));
		String consentCurrency = OIDFJSON.getString(consentRequest.get("currency"));

		if (paymentAmount.equalsIgnoreCase(consentAmount)) {
			logSuccess("Payment and consent request have matching amounts");
		} else {
			throw error("Payment and consent do not have matching amounts");
		}

		if (paymentCurrency.equalsIgnoreCase(consentCurrency)) {
			logSuccess("Payment and consent request have matching currency");
		} else {
			throw error("Payment and consent do not have matching currency");
		}

		return env;
	}
}
