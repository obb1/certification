package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsErrorResponseCodeEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasLimitePeriodoQuantidadeExcedido extends AbstractEnsureErrorResponseCodeFieldWas {

	@Override
	protected List<String> getExpectedCodes() {
		return List.of(RecurringPaymentsErrorResponseCodeEnum.LIMITE_PERIODO_QUANTIDADE_EXCEDIDO.toString());
	}
}
