package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus60;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureRefreshTokenIsAJwt;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateConsents422RefreshTokenJwt;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;


public abstract class AbstractConsentApiExtensionJwtTestModule extends AbstractSimplifiedConsentRenewalTestModule {


	@Override
	protected void checkTokenValue() {
		callAndStopOnFailure(EnsureRefreshTokenIsAJwt.class);
	}

	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response expecting error - REFRESH_TOKEN_JWT", () -> {
			call(getPostConsentExtensionExpectingErrorSequence(EnsureConsentResponseCodeWas422.class, ValidateConsents422RefreshTokenJwt.class, CreateExtensionRequestTimeDayPlus60.class));
		});
	}
}
