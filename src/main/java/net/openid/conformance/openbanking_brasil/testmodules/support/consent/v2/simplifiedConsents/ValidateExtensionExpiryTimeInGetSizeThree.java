package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class ValidateExtensionExpiryTimeInGetSizeThree extends ValidateExtensionExpiryTimeInGetExtensionResponse{
	@Override
	protected int expectedResponseArraySize() {
		return 3;
	}
}

