package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime;

import java.time.LocalDateTime;

public class EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToTheFuture extends AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime {

	@Override
	protected LocalDateTime calculateStartDateTime(LocalDateTime currentDateTime) {
		return currentDateTime.plusDays(10);
	}
}
