package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpStatus;

import java.util.List;

public class EnsureResponseCodeWas401or403or422 extends AbstractCondition {

	List<Integer> EXPECTED_NON_422_STATUSES = List.of(
		HttpStatus.UNAUTHORIZED.value(),
		HttpStatus.FORBIDDEN.value()
	);

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		int status = env.getInteger("resource_endpoint_response_full", "status");
		logSuccess("Found response code", args("status",status));
		if(EXPECTED_NON_422_STATUSES.contains(status)) {
			env.removeNativeValue("422_status");
			logSuccess("401/403 response status, as expected", args("status",status));
		} else if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			env.putString("422_status", "ok");
			logSuccess("422 response status, as expected", args("status",status));
		} else {
			env.removeNativeValue("422_status");
			throw error("Was expecting a 401, 403 or 422 response", args("status",status));
		}
		return env;
	}
}
