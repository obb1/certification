package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1;

import org.springframework.http.HttpMethod;

public class PostFidoSignOptionsOASValidatorV1 extends AbstractEnrollmentsOASValidatorV1 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}/fido-sign-options";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
