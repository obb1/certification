package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

public class CreateSweepingRecurringConfigurationObjectWithAllFields extends AbstractCreateRecurringConfigurationObject {

	private static final String TRANSACTION_LIMIT = "1000000000.00";
	private static final int QUANTITY_LIMIT = 300;

	@Override
	protected JsonObject buildRecurringConfiguration(Environment env) {
		return addSweepingFields(new JsonObjectBuilder(), AMOUNT, TRANSACTION_LIMIT, QUANTITY_LIMIT)
			.build();
	}
}
