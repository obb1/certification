package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.AbstractGetConsentExtensionsOASValidator;

@ApiName("Get Consent Extensions V3")
public class GetConsentExtensionsOASValidatorV3 extends AbstractGetConsentExtensionsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/consents/swagger-consents-3.0.1.yml";
	}

}
