package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforcePresenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ChangeDebtorAccountTypeToConfigValue;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ChangeDebtorAccountTypeToSLRY;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.SetPaymentAuthorisationFlowToHybridFlow;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;


public abstract class AbstractPaymentsApiAccountTypeTestModule extends AbstractOBBrasilPaymentFunctionalTestModule{

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforcePresenceOfDebtorAccount.class);
		callAndStopOnFailure(ChangeDebtorAccountTypeToSLRY.class);
		callAndStopOnFailure(SetPaymentAuthorisationFlowToHybridFlow.class);
	}

	@Override
	protected void performPreAuthorizationSteps() {
		eventLog.startBlock("Executing Post Consent Flow with SLRY as debtorAccount");
		OpenBankingBrazilPreAuthorizationErrorAgnosticSteps steps = (OpenBankingBrazilPreAuthorizationErrorAgnosticSteps) new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
			.insertAfter(OptionallyAllow201Or422.class,
				condition(paymentConsentErrorValidator())
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.FAILURE)
					.skipIfStringMissing("validate_errors")
			)
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
				sequenceOf(condition(CreateRandomFAPIInteractionId.class),
					condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			);
		call(steps);
		callAndStopOnFailure(SaveAccessToken.class);
		callAndStopOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
		env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasParametroInvalido.class,Condition.ConditionResult.FAILURE);
		env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		eventLog.endBlock();

		eventLog.startBlock("Exectuing Post Consent flow with debtorAccount type set to config value");
		callAndStopOnFailure(ChangeDebtorAccountTypeToConfigValue.class);
		super.performPreAuthorizationSteps();
	}
}
