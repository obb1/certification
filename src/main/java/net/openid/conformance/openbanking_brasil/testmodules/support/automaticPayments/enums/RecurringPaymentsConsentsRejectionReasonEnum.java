package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum RecurringPaymentsConsentsRejectionReasonEnum {

	NAO_INFORMADO,
	FALHA_INFRAESTRUTURA,
	TEMPO_EXPIRADO_AUTORIZACAO,
	REJEITADO_USUARIO,
	CONTAS_ORIGEM_DESTINO_IGUAIS,
	CONTA_NAO_PERMITE_PAGAMENTO,
	SALDO_INSUFICIENTE,
	VALOR_ACIMA_LIMITE,
	AUTENTICACAO_DIVERGENTE;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
