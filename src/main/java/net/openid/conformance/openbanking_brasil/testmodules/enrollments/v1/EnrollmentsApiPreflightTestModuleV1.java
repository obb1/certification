package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v1;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.PreFlightCertCheckModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckBrazilOrganizationIdIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.validateField.ValidateEnrollmentsFieldV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_preflight_test-module_v1",
	displayName = "Pre-flight checks will validate the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. An SSA will be generated using the Open Banking Brasil Directory. Finally" +
		"a check of mandatory fields will be made",
	summary = "Pre-flight checks will validate the mTLS certificate before requesting an access token using the Directory client_id provided in the test configuration. An SSA will be generated using the Open Banking Brasil Directory. Finally" +
		"a check of mandatory fields will be made",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf",
		"directory.client_id"
	}
)
public class EnrollmentsApiPreflightTestModuleV1 extends PreFlightCertCheckModule {

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence)
			.replace(SetConsentsScopeOnTokenEndpointRequest.class, condition(SetPaymentsScopeOnTokenEndpointRequest.class));
	}

	@Override
	protected void runTests() {
		call(new ValidateRegisteredEndpoints(GetPaymentConsentsV4Endpoint.class));
		callAndContinueOnFailure(CheckBrazilOrganizationIdIsPresent.class, Condition.ConditionResult.FAILURE);
		super.runTests();
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		runInBlock("Pre-flight Consent field checks", () ->
			callAndContinueOnFailure(ValidateEnrollmentsFieldV1.class, Condition.ConditionResult.FAILURE)

		);
	}
}
