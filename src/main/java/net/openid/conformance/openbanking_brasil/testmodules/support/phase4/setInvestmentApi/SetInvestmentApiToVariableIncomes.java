package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi;

public class SetInvestmentApiToVariableIncomes extends AbstractSetInvestmentApi {

    @Override
    protected String investmentApi() {
        return "variable-incomes";
    }
}
