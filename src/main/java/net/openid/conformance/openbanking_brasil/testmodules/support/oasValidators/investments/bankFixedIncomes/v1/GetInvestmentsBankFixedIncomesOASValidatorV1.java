package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.investments.bankFixedIncomes.v1;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api url: https://openbanking-brasil.github.io/draft-openapi/swagger-apis/investments/1.0.1.yml
 * Api endpoint: /bank-fixed-incomes
 * Api version: 1.0.1
 */

@ApiName("Investments Bank Fixed Incomes V1")
public class GetInvestmentsBankFixedIncomesOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-investments-1.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/bank-fixed-incomes";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonArray dataArray = JsonPath.read(body, "$.data[*]");
		for(JsonElement dataElement : dataArray) {
			assertIndexerAdditionalInfo(dataElement.getAsJsonObject());
		}
	}

	protected void assertIndexerAdditionalInfo(JsonObject data) {
		this.assertField1IsRequiredWhenField2HasValue2(
			data.get("index").getAsJsonObject(),
			"indexerAdditionalInfo",
			"indexer",
			"OUTROS"
		);

	}

}
