package net.openid.conformance.openbanking_brasil.testmodules.support;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;


public class EnsureTransactionsDateTimeIsNoOlderThan7Days extends ValidateTransactionsDateTime {

	@Override
	protected boolean isDateInvalid(LocalDate currentDate, LocalDate transactionDate) {

		long daysBetween = ChronoUnit.DAYS.between(transactionDate, currentDate);

		return daysBetween < 0 || daysBetween > 7;

	}

	@Override
	protected String getErrorMessage() {
		return "Transaction is older than 7 days or in the future";
	}


}
