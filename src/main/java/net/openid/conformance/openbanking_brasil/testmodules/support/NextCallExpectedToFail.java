package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class NextCallExpectedToFail extends AbstractCondition {

    @Override
    @PostEnvironment(strings = "expects_failure")
    public Environment evaluate(Environment env) {
        env.putString("expects_failure", "true");
        logSuccess("The next call to an API is expected to fail");
        return env;
    }
}
