package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.FAPIBrazilAddConsentIdToClientScope;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractConsentIdFromResourceEndpointResponseFull;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.addExpirationToConsentRequest.AddExpirationPlus5MinutesToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus365;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateIndefiniteConsentExpiryTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateConsent422EstadoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas401Or403;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentExtendsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentExtendsOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPostConsentExtensionsExpectingErrorSequence;

public abstract class AbstractFvpConsentApiExtensionInvalidStatusTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> postConsentValidator();
	protected abstract Class<? extends Condition> getConsentValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetConsentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("Call POST Consents Endpoint with expirationDateTime as current time + 5 minutes - Expects 201");
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS).build();
		postAndValidateConsents();
		eventLog.endBlock();

		eventLog.startBlock("Call GET Consents - Expects 200 AWAITING_AUTHORISATION");
		getAndValidateConsents();
		eventLog.endBlock();

		eventLog.startBlock("Call the POST Extends Endpoint with expirationDateTime as  D+365 - Expects 401 or 403");
		postAndValidateConsentsExtension();
		eventLog.endBlock();
	}

	protected void postAndValidateConsents() {
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
		callAndStopOnFailure(AddExpirationPlus5MinutesToConsentRequest.class);
		callAndStopOnFailure(SetContentTypeApplicationJson.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(postConsentValidator(), Condition.ConditionResult.FAILURE);
		env.mapKey("endpoint_response", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("endpoint_response");
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(ExtractConsentIdFromResourceEndpointResponseFull.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");
		callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(FAPIBrazilAddConsentIdToClientScope.class, Condition.ConditionResult.FAILURE);
	}

	protected void getAndValidateConsents() {
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(getConsentValidator(), Condition.ConditionResult.FAILURE);
		env.mapKey("endpoint_response", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("endpoint_response");
		callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
	}

	protected void postAndValidateConsentsExtension() {
		call(new CallPostConsentExtensionsExpectingErrorSequence()
			.replace(CreateIndefiniteConsentExpiryTime.class, condition(CreateExtensionRequestTimeDayPlus365.class))
			.replace(EnsureConsentResponseCodeWas422.class, condition(EnsureConsentResponseCodeWas401Or403.class))
			.replace(PostConsentExtendsOASValidatorV3.class, condition(PostConsentExtendsOASValidatorV3n2.class))
			.skip(ValidateConsent422EstadoInvalido.class, "No error code to validate")
			);
	}

	@Override
	protected void insertConsentProdValues() {}
}
