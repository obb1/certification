package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.EnsureCreditorIdentificationIsCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument.AddManuLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToAddDefinedCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.EditRecurringPaymentsBodyToRemoveProxy;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.GenerateNewE2EIDBasedOnPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForFirstPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate.EditRecurringPaymentsBodyToSetDateToFirstPaymentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountToFirstPaymentAmount;

public abstract class AbstractAutomaticPaymentsAutomaticPixTestModule extends AbstractAutomaticPaymentsFunctionalTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(EnsureCreditorIdentificationIsCnpj.class);
		callAndStopOnFailure(AddManuLocalInstrument.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToRemoveProxy.class);
		configurePaymentDate();
		configureCreditorFields();
	}

	protected void configurePaymentDate() {
		callAndStopOnFailure(EditRecurringPaymentsBodyToSetDateToFirstPaymentDate.class);
		callAndStopOnFailure(SetPaymentReferenceForFirstPayment.class);
		callAndStopOnFailure(GenerateNewE2EIDBasedOnPaymentDate.class);
	}

	protected void configureCreditorFields() {
		callAndStopOnFailure(EditRecurringPaymentsConsentBodyToAddDefinedCreditor.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetFirstPaymentCreditorAccount.class);
		callAndStopOnFailure(EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj.class);
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountToFirstPaymentAmount();
	}
}
