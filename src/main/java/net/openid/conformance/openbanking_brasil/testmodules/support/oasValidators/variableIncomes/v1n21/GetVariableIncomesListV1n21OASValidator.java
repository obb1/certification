package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1n21;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetVariableIncomesListV1n21OASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/variableIncome/variable-income-v1.2.1.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected String getEndpointPath() {
		return "/investments";
	}


}
