package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public abstract class AbstractCustomSchedulePayment extends AbstractSchedulePayment {

	public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected JsonObject createScheduleObject() {
		return new JsonObjectBuilder()
			.addFields("custom", Map.of(
				"dates", getDates(),
				"additionalInformation", "additional information"
			))
			.build();
	}

	protected abstract List<Integer> getNumberOfDaysInTheFutureList();

	protected JsonArray getDates() {
		JsonArray dates = new JsonArray();
		for (Integer numberOfDaysInTheFuture : getNumberOfDaysInTheFutureList()) {
			dates.add(LocalDate.now(ZoneId.of("America/Sao_Paulo")).plusDays(numberOfDaysInTheFuture).format(FORMATTER));
		}
		return dates;
	}
}
