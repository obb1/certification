package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.RemovePaymentConsentDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasTempoExpiradoAutorizacaoV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasPartiallyAccepted;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckLocalTime;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.WaitForMidnight;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;

public abstract class AbstractPaymentsApiMultipleConsentsTimezoneConditionalTestModuleV4 extends AbstractPaymentsApiMultipleConsentsConditionalTestModule {

	protected abstract Class<? extends Condition> paymentInitiationErrorValidator();

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckStatus("PARTIALLY_ACCEPTED", new EnsurePaymentConsentStatusWasPartiallyAccepted());
		env.unmapKey("access_token");
		validateGetConsentResponse();

		eventLog.startBlock(currentClientString() + "Call token endpoint");
		createAuthorizationCodeRequest();
		requestAuthorizationCode();
		requestProtectedResource();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void setScheduledPaymentDateTime() {
		eventLog.startBlock("Set scheduled.single.date as D+1");
		callAndStopOnFailure(EnsureScheduledDateIsTomorrow.class);
		callAndStopOnFailure(RemovePaymentConsentDate.class);
		eventLog.endBlock();
	}

	@Override
	protected void updatePaymentConsent() {}

	@Override
	protected void requestProtectedResource() {
		eventLog.startBlock("Check if localTime is between 23h30 and 23h59");
		callAndStopOnFailure(CheckLocalTime.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
		eventLog.startBlock("Set the conformance suite to sleep until 00:00");
		callAndStopOnFailure(WaitForMidnight.class);
		eventLog.startBlock("Calling GET Consents Endpoint, expecting response code 200");
		call(new ObtainPaymentsAccessTokenWithClientCredentials(addTokenEndpointClientAuthentication).insertAfter(ExtractAccessTokenFromTokenResponse.class, condition(SaveAccessToken.class)));
		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		eventLog.startBlock("Validate status is REJECTED and rejectionReason is TEMPO_EXPIRADO_AUTORIZACAO");
		callAndStopOnFailure(EnsureConsentsRejectionReasonCodeWasTempoExpiradoAutorizacaoV3.class, Condition.ConditionResult.FAILURE);
	}
}
