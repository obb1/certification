package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForDateHeaderInResourceResponse;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingCurrentAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromBookingDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckExpectedBookingDateTimeResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.CopyResourceEndpointResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTransactionsDateTimeIsNoOlderThan7Days;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTransactionsDateTimeIsSetToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})

public abstract class AbstractAccountsApiTransactionsCurrentTestModule extends AbstractPhase2TestModule {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String FROM_BOOKING_DATE_PARAMETER = "fromBookingDate";
	private static final String TO_BOOKING_DATE_PARAMETER = "toBookingDate";


	protected abstract Class<? extends Condition> getAccountListValidator();
	protected abstract Class<? extends Condition> getAccountTransactionsValidator();

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class),
			condition(GetAccountsV2Endpoint.class));
	}

	@Override
	protected void configureClient(){
		super.configureClient();
	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(getAccountListValidator(), Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(AccountSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingCurrentAccountTransactions.class);

//		 Call without parameters
		runInBlock("Fetch Account Current transactions V2", () -> call(getPreCallProtectedResourceSequence()));
		runInBlock("Validate Account Current Transactions V2",
			() -> call(getValidationSequence()
				.then(condition(EnsureTransactionsDateTimeIsSetToToday.class).dontStopOnFailure()))
		);

		// Call with full range parameters | Anti Cheat
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString(FROM_BOOKING_DATE_PARAMETER, currentDate.minusDays(6).format(FORMATTER));
		env.putString(TO_BOOKING_DATE_PARAMETER, currentDate.format(FORMATTER));

		callAndStopOnFailure(PrepareUrlForFetchingCurrentAccountTransactions.class);
		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		runInBlock("Fetch Account Current transactions with full range date parameters",
			() -> call(getPreCallProtectedResourceSequence()
				.then(condition(CopyResourceEndpointResponse.class)))
		);
		env.mapKey("full_range_response", "resource_endpoint_response_full_copy");

		// Call with valid  parameters
		env.putString(FROM_BOOKING_DATE_PARAMETER, currentDate.minusDays(5).format(FORMATTER));
		env.putString(TO_BOOKING_DATE_PARAMETER, currentDate.format(FORMATTER));

		callAndStopOnFailure(PrepareUrlForFetchingCurrentAccountTransactions.class);
		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		runInBlock("Fetch Account Current transactions with valid date parameters", () -> call(getPreCallProtectedResourceSequence()));
		runInBlock("Validate Account Current Transactions",
			() -> call(getValidationSequence()
				.then(condition(CheckExpectedBookingDateTimeResponse.class))
				.then(condition(EnsureTransactionsDateTimeIsNoOlderThan7Days.class).dontStopOnFailure()))
		);

		// Call with invalid  parameters
		env.putString(FROM_BOOKING_DATE_PARAMETER, currentDate.minusDays(30).format(FORMATTER));
		env.putString(TO_BOOKING_DATE_PARAMETER, currentDate.minusDays(20).format(FORMATTER));

		callAndStopOnFailure(PrepareUrlForFetchingCurrentAccountTransactions.class);
		callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
		runInBlock("Fetch Account Current transactions with invalid date parameters",
			() -> call(getPreCallProtectedResourceSequence()
				.replace(EnsureResourceResponseCodeWas200.class, condition(EnsureResourceResponseCodeWas422.class)))
		);
		callAndStopOnFailure(getAccountTransactionCurrentValidator());
	}


	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ACCOUNTS).addScopes(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.OPEN_ID).build();
	}

	protected ConditionSequence getValidationSequence() {
		return sequenceOf(
			condition(getAccountTransactionsValidator())
		);
	}

	protected ConditionSequence getPreCallProtectedResourceSequence() {
		return sequenceOf(
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(CallProtectedResource.class),
			condition(EnsureResourceResponseCodeWas200.class),
			condition(CheckForDateHeaderInResourceResponse.class),
			condition(CheckForFAPIInteractionIdInResourceResponse.class),
			condition(EnsureResourceResponseReturnedJsonContentType.class)
		);
	}

	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountTransactionCurrentValidator();

}
