package net.openid.conformance.openbanking_brasil.testmodules;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessFinancialRelations;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessIdentifications;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.PrepareToGetBusinessQualifications;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyBusinessProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractPhase2V2TestModule;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractCustomerBusinessDataApiTestModule extends AbstractPhase2V2TestModule {

	protected abstract Class<? extends AbstractJsonAssertingCondition> getBusinessQualificationResponseValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getBusinessIdentificationResponseValidator();
	protected abstract Class<? extends AbstractJsonAssertingCondition> getBusinessRelationsResponseValidator();
	protected abstract ConditionSequence getConsentAndResourceEndpointSequence();

	@Override
	protected void configureClient(){
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(getConsentAndResourceEndpointSequence()));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder
			.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA)
			.addScopes(OPFScopesEnum.CUSTOMERS, OPFScopesEnum.OPEN_ID)
			.build();

		callAndStopOnFailure(PrepareToGetBusinessQualifications.class);
		callAndStopOnFailure(AddDummyBusinessProductTypeToConfig.class);
	}

	@Override
	protected void validateResponse() {
		runInBlock("Validating business qualifications response V2", () -> {
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getBusinessQualificationResponseValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Validating business identifications response V2", () -> {
			callAndStopOnFailure(PrepareToGetBusinessIdentifications.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getBusinessIdentificationResponseValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Validating corporate relationship response V2", () ->{
			callAndStopOnFailure(PrepareToGetBusinessFinancialRelations.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getBusinessRelationsResponseValidator(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void requestProtectedResource() {
		validateResponse();
	}

}
