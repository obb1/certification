package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsLargeBatchTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_recurring-payments-large-batch_test-module_v4",
	displayName = "Ensure that consent for recurring payments can be carried out and that the payment is successfully scheduled when a large batch of payments is sent",
	summary = "Ensure that consent for recurring payments can be carried out and that the payment is successfully scheduled when a large batch of payments is sent\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+1, and schedule.daily.quantity as 60\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is “AUTHORISED” and validate response\n" +
		"• Calls the POST Payments Endpoint with the 60 Payments, using the appropriate endToEndID for each of them\n" +
		"• Expects 201 - Validate Response\n" +
		"• Poll the Get Payments endpoint with the last PaymentID Created while payment status is RCVD or ACCP\n" +
		"• Call the GET Payments for the 5 Payments\n" +
		"• Expect Payment Scheduled in all of them (SCHD) - Validate Response",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiRecurringPaymentsLargeBatchTestModuleV4 extends AbstractPaymentsApiRecurringPaymentsLargeBatchTestModule {
	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
