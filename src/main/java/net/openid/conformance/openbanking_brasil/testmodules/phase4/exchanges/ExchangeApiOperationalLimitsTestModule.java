package net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddSpecifiedPageNumberParameterToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddSpecifiedPageSizeParameterToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureOperationalUserIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.OperationalLimitsToConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetSpecifiedValueToSpecifiedUrlParameter;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetExchangesV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords.EnsureNumberOfTotalRecordsIsAtLeast126FromMeta;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesEventsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractOperationID;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperation;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.PrepareUrlForFetchingOperationEvents;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.operationalLimits.AbstractOperationalLimitsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "exchange_api_operational-limits_test-module",
	displayName = "exchange_api_operational-limits_test-module",
	summary = "Test will make sure that the defined operational limits for all of the endpoints are respected. It will also confirm that the events endpoint can be used beyond the operational limit, as long as the next page is called.\n" +
		"Test will require the user to have set at least one ACTIVE resource each with at least 126 events to be returned on the event endpoint. The tested resource must be set linked to the brazilCpf and brazilCnpj for Operational Limits\n" +
		"Test will execute a POST token with a refresh_token grant at least 60 seconds before the token is about to be expired, by working with the \"expires_in\" set on the token endpoint. Test will not log all successful the validations done on the endpoints due to the high number of requests, however all failures will be provided.\n" +
		"\u2022 Call the POST Consents endpoint, using the brazilCpf/Cnpj for Operational Limits, with the Exchange API Permission Group - [EXCHANGES_READ RESOURCES_READ]\n" +
		"\u2022 Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
		"\u2022 Set on the the authorization request, in addition the consents scope, the exchange API scopes (exchanges)\n" +
		"\u2022 Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"\u2022 Call the GET Consents endpoint, confirm that the Consent is set to \"Authorised\" - Expects 200 and Validate Response Body\n" +
		"\u2022 Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
		"\u2022 Call the GET Operations List Endpoint 30 Times - Expect a 200 Response all times -Extract one Operation ID \n" +
		"\u2022 Call the GET Operation Events Endpoint with the Extracted operationId 29 times - Expect a 200 Response - Ensure at least 126 eventsare returned\n" +
		"\u2022 Call the GET Operation Events Endpoint with the Extracted Operation ID one time - Send query Parameters page-size=25, page=1,  - Expect a 200 Response, extract the next URI provided\n" +
		"\u2022 Do the following step 4 times, Call the GET Operations Events endpoint with the extracted next URI - Expect a 200 Response \n" +
		"\u2022 Call the GET Operations {operationId} Endpoint 4 Times with the Extracted Operation ID - Expect a 200 Response all times \n" +
		"\u2022 Call the Delete Consents Endpoints\n" +
		"\u2022 Expect a 204 without a body",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class ExchangeApiOperationalLimitsTestModule extends AbstractOperationalLimitsTestModule {
	private static final int NUMBER_OF_OPERATION_LIST_ENDPOINT_CALLS = 1;
	private static final int NUMBER_OF_OPERATION_EVENTS_ENDPOINT_CALLS = 1;
	private static final int NUMBER_OF_OPERATION_EVENTS_NEXT_LINK_CALLS = 1;
	private static final int NUMBER_OF_OPERATION_FETCHES = 4;


	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetExchangesV1Endpoint.class)
		)));
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putBoolean("continue_test", true);
		callAndContinueOnFailure(EnsureOperationalUserIsPresent.class, Condition.ConditionResult.WARNING);
		if (!env.getBoolean("continue_test")) {
			fireTestSkipped("Test skipped since no operational CPF was informed.");
		}
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder
			.addPermissionsBasedOnCategory(OPFCategoryEnum.EXCHANGES)
			.addScopes(OPFScopesEnum.EXCHANGES, OPFScopesEnum.OPEN_ID)
			.build();
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		callAndContinueOnFailure(OperationalLimitsToConsentRequest.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void validateResponse() {
		// validate Operations List Endpoint response
		validateResponse("Validating Operations List Endpoint response", GetExchangesOperationsListV1OASValidator.class);
		call(condition(ExtractOperationID.class));
		disableLogging();

		// call Operations List Endpoint 29 more times without logging
		for (int i = 0; i < NUMBER_OF_OPERATION_LIST_ENDPOINT_CALLS - 1; i++) {
			preCallProtectedResource(String.format("[%d] Calling Operations List Endpoint", i + 1));
		}
		enableLogging();


		// call Operations Events Endpoint 29 times validating only the first call
		callAndStopOnFailure(PrepareUrlForFetchingOperationEvents.class);
		env.putInteger("required_page_size", 126);
		env.putInteger("required_page_number", 1);
		callAndStopOnFailure(AddSpecifiedPageSizeParameterToProtectedResourceUrl.class);
		callAndStopOnFailure(AddSpecifiedPageNumberParameterToProtectedResourceUrl.class);
		for (int i = 0; i < NUMBER_OF_OPERATION_EVENTS_ENDPOINT_CALLS; i++) {
			eventLog.startBlock(String.format("[%d] Calling Operations Events Endpoint with extracted operation ID", i + 1));

			preCallProtectedResource();
			if (i == 0) {
				validateResponse("Validating Operations Events Endpoint response", GetExchangesEventsV1OASValidator.class);
				callAndStopOnFailure(EnsureNumberOfTotalRecordsIsAtLeast126FromMeta.class);
				disableLogging();
			}
			eventLog.endBlock();
		}
		enableLogging();


		// call Operations Events Endpoint 5 more times extracting next link
		for (int i = 0; i < NUMBER_OF_OPERATION_EVENTS_NEXT_LINK_CALLS; i++) {
			eventLog.startBlock(String.format("[%d] Calling Operations Events Endpoint next link with page parameters", i + 1));

			if (i == 0) {
				callAndStopOnFailure(PrepareUrlForFetchingOperationEvents.class);
				env.putInteger("required_page_size", 25);
				env.putInteger("required_page_number", 1);
				callAndStopOnFailure(AddSpecifiedPageSizeParameterToProtectedResourceUrl.class);
				callAndStopOnFailure(AddSpecifiedPageNumberParameterToProtectedResourceUrl.class);
			}

			preCallProtectedResource();

			if (i == 0) {
				validateResponse("Validating Operations Events Endpoint response", GetExchangesEventsV1OASValidator.class);
			}

			callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
			env.putString("value", "25");
			env.putString("parameter", "page-size");
			callAndStopOnFailure(SetSpecifiedValueToSpecifiedUrlParameter.class);
			eventLog.endBlock();
			disableLogging();
		}
		enableLogging();


		// fetch Operation 4 times validating only the first call
		callAndStopOnFailure(PrepareUrlForFetchingOperation.class);
		for (int i = 0; i < NUMBER_OF_OPERATION_FETCHES; i++) {
			eventLog.startBlock(String.format("[%d] Fetching Operation with extracted operation ID", i + 1));
			preCallProtectedResource();
			if (i == 0) {
				validateResponse("Validating Operation Endpoint response", GetExchangesOperationIdentificationV1OASValidator.class);
				disableLogging();
			}
			eventLog.endBlock();
		}
		enableLogging();

	}


	private void validateResponse(String message, Class<? extends Condition> validator) {
		runInBlock(message, () -> callAndStopOnFailure(validator));
	}

}
