package net.openid.conformance.openbanking_brasil.testmodules.v3n.consents;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.AbstractConsentApiTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_core_test-module_v3-2",
	displayName = "Validate the structure of all consent API resources V3-2",
	summary = "Validates the structure of all consent API resources V3-2\n" +
		"\u2022 Creates a Consent V3-2 with all of the existing permissions.\n" +
		"\u2022 Checks all of the fields sent on the consent API V3-2 are specification compliant\n" +
		"\u2022 Redirect the user to authorize the consent - Expect Successful Redirect\n" +
		"\u2022 Call the GET Consents API \n" +
		"\u2022 Expect a 200 - Make Sure Consent is “Authorised” - Validate all fields of the response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl", "consent.productType", "resource.consentSyncTime"
})

public class ConsentApiTestModuleV3n extends AbstractConsentApiTestModule {

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetConsentOASValidatorV3n2.class;
	}

}
