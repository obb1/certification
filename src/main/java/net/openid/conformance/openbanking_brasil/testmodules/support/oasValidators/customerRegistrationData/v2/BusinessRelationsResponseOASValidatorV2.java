package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;


@ApiName("Business Financial-relations V2.2.0")
public class BusinessRelationsResponseOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/customers/swagger-customers-2.2.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/business/financial-relations";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		JsonObject data = body.getAsJsonObject("data");
		this.assertAccountBranchCodeConstraint(data);
	}

	protected void assertAccountBranchCodeConstraint(JsonObject data) {
		JsonArray accounts = JsonPath.read(data, "$.accounts[*]");
		for(JsonElement accountElement : accounts) {
			JsonObject account = accountElement.getAsJsonObject();
			String accountType = OIDFJSON.getString(account.get("type"));
			if(!"CONTA_PAGAMENTO_PRE_PAGA".equals(accountType) && account.get("branchCode") == null) {
				throw error("data.accounts.branchCode is required when data.accounts.accountType is not \"CONTA_PAGAMENTO_PRE_PAGA\"");
			}
		}
	}
}
