package net.openid.conformance.openbanking_brasil.testmodules.support;


public abstract class ValidateTransactionsDateTime extends AbstractValidateDateField {

	@Override
	protected String getDateField() {
		return "transactionDateTime";
	}

	@Override
	protected String getDateFormatter() {
		return "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	}

}
