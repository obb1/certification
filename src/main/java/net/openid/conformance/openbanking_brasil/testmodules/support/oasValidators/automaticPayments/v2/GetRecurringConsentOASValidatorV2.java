package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import org.springframework.http.HttpMethod;

public class GetRecurringConsentOASValidatorV2 extends AbstractRecurringConsentOASValidatorV2 {

	@Override
	protected String getEndpointPath() {
		return "/recurring-consents/{recurringConsentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
