package net.openid.conformance.openbanking_brasil.testmodules.phase3;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly.Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture;

public abstract class AbstractPaymentsApiRecurringPaymentsWeeklyCoreTestModule extends AbstractPaymentApiSchedulingTestModule {

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule5WeeklyPaymentsForMondayStarting1DayInTheFuture();
	}

	@Override
	protected void configureDictInfo(){
		// Not necessary in this test.
	}
}
