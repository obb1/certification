package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.LoadConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SaveConsentsBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractAutomaticUnhappyPaymentsConsentsFunctionalTestModule extends AbstractAutomaticPaymentsConsentsFunctionalTestModule {

	protected abstract Class<? extends Condition> postPaymentConsentErrorValidator();
	protected abstract void performTestSteps();

	protected void performTestStep(String blockHeader,
								   Class<? extends Condition> consentsEditingCondition,
								   Class<? extends Condition> errorResponseCodeCondition) {
		runInBlock(blockHeader, () -> {
			callAndStopOnFailure(LoadConsentsBody.class);
			callAndStopOnFailure(consentsEditingCondition);
			call(postConsentSequence());
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postPaymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey("endpoint_response", "consent_endpoint_response_full");
			callAndContinueOnFailure(errorResponseCodeCondition, Condition.ConditionResult.FAILURE);
			env.unmapKey("endpoint_response");
		});
	}

	@Override
	protected void performPreAuthorizationSteps() {
		performTestSteps();
		fireTestFinished();
	}

	protected ConditionSequence postConsentSequence() {
		return new OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps(
			addTokenEndpointClientAuthentication,
			true
		);
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SaveConsentsBody.class);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return null;
	}
}
