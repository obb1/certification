package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus60;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.PrepareToGetConsentExtensions;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInGetSizeOne;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentExtensionsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentExtendsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractConsentApiExtensionJwtTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_extension-jwt-module_v3",
	displayName = "consents_api_extension-jwt-module_v3",
	summary = "Ensure a consent cannot be extended is the refresh_token is jwt, and the appropriate error message is sent\n" +
		"\u2022 Call the POST Consents Endpoint with expirationDateTime as current time + 5 minutes\n" +
		"\u2022 Expects 201 - Validate Response\n" +
		"\u2022 Redirect the user to authorize the consent\n" +
		"\u2022 Call the POST Token Endpoint Using the Authorization Code Grant\n" +
		"\u2022 Expects 201 - Validate if the refresh token sent back is a JWT\n" +
		"\u2022 Call the GET Consents\n" +
		"\u2022 Expects 200 - Validate Response and check if the status is AUTHORISED\n" +
		"\u2022 Call the POST Extends Endpoint with expirationDateTime as D+60, and all required headers\n" +
		"\u2022 Expects 422 - REFRESH_TOKEN_JWT",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl","consent.productType"
})
public class ConsentApiExtensionJwtTestModuleV3 extends AbstractConsentApiExtensionJwtTestModule {
	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetConsentV3Endpoint.class),
				condition(GetResourcesV3Endpoint.class)
			)
			)
		);
	}


	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetConsentOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeOne.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentCreationValidation() {
		return PostConsentOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		return CreateExtensionRequestTimeDayPlus60.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentExtensionValidator() {
		return PostConsentExtendsOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionValidator() {
		return GetConsentExtensionsOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtensions.class;
	}
}
