package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.AbstractGetUnarrangedAccountsOverdraftPaymentsOASValidator;


public class GetUnarrangedAccountsOverdraftPaymentsV2n4OASValidator extends AbstractGetUnarrangedAccountsOverdraftPaymentsOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/unarrangedAccountsOverdraft/unarranged-accounts-overdraft-v2.4.0.yml";
	}
}
