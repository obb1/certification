package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AbstractEditObjectBodyToManipulateCnpjOrCpf;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditRecurringPaymentRequestClaimsToSetDifferentIdentification extends AbstractEditObjectBodyToManipulateCnpjOrCpf {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject document = Optional.ofNullable(env.getElementFromObject("resource_request_entity_claims", "data.document"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Unable to find data.document in recurring-payments request payload"));

		String currentIdentification = OIDFJSON.getString(
			Optional.ofNullable(document.get("identification"))
				.orElseThrow(() -> error("Unable to find identification field for the document", args("document", document)))
		);

		String newIdentification = switch (currentIdentification.length()) {
			case CPF_DIGITS -> generateNewValidCpfFromDifferentRegion(currentIdentification);
			case CNPJ_DIGITS -> generateNewRandomCnpj();
			default -> throw error("The value inside the identification field is neither a CPF nor a CNPJ",
				args("identification", currentIdentification));
		};

		document.addProperty("identification", newIdentification);
		logSuccess("Updated data.document to have a different identification", args(
			"old identification", currentIdentification,
			"new identification", newIdentification,
			"document", document
		));

		return env;
	}
}
