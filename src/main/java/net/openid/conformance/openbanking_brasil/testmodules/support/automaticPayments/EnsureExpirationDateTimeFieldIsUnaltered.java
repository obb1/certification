package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class EnsureExpirationDateTimeFieldIsUnaltered extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY, strings = "expiration_date_time")
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		String expirationDateTime = Optional.ofNullable(response.getAsJsonObject("data"))
			.map(data -> data.get("expirationDateTime"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find expirationDateTime field in the response data", args("response", response)));

		String savedExpirationDateTime = env.getString("expiration_date_time");

		if (!savedExpirationDateTime.equals(expirationDateTime)) {
			throw error("The expirationDateTime field has been altered",
				args("expirationDateTime", expirationDateTime, "expected", savedExpirationDateTime));
		}

		logSuccess("The expirationDateTime field is unaltered");

		return env;
	}

	protected JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}
}
