package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class SetFromAndToTransactionDateToSavedTransactionDate extends AbstractCondition {

    @Override
    @PreEnvironment(strings = "transactionDate")
    @PostEnvironment(strings = {"fromTransactionDate", "toTransactionDate"})
    public Environment evaluate(Environment env) {
        String transactionDate = env.getString("transactionDate");
        env.putString("fromTransactionDate", transactionDate);
        env.putString("toTransactionDate", transactionDate);
        logSuccess("Transaction date query parameters have been set to the saved value", args(
            "fromTransactionDate", env.getString("fromTransactionDate"),
            "toTransactionDate", env.getString("toTransactionDate")
        ));
        return env;
    }
}
