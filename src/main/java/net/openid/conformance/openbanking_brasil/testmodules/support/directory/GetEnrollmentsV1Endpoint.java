package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetEnrollmentsV1Endpoint extends AbstractGetEnrollmentsFromAuthServer  {

	@Override
	protected String getEndpointRegex() {
		return "^(https:\\/\\/)(.*?)(\\/open-banking\\/enrollments\\/v\\d+\\/enrollments)$";
	}

	@Override
	protected String getApiFamilyType() {
		return "enrollments";
	}

	@Override
	protected String getApiVersionRegex() {
		return  "^(1.[0-9].[0-9])$";
	}

	@Override
	protected boolean isResource() {
		return true;
	}
}
