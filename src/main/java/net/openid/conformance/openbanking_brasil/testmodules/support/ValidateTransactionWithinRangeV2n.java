package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class ValidateTransactionWithinRangeV2n extends AbstractValidateTransactionWithinRange {

	@Override
	@PreEnvironment(strings = {"toBookingDate", "fromBookingDate"}, required = "resource_endpoint_response_full")
	@PostEnvironment(strings = "transactionDateTime")
	protected void processTransactionObject(JsonObject randomTransactionObject, String fromBookingDate, String toBookingDate, Environment env) {
		JsonElement transactionDateTimeElement = randomTransactionObject.get("transactionDateTime");

		if (transactionDateTimeElement != null && !transactionDateTimeElement.isJsonNull()) {
			String randomTransactionDateTime = OIDFJSON.getString(randomTransactionObject.get("transactionDateTime"));
			validateTransactionDateTime(randomTransactionDateTime, fromBookingDate, toBookingDate);
			logSuccess("Booking date parameters successfully validated to be within the specified range");
			env.putString("transactionDateTime", randomTransactionDateTime);
		} else {
			throw error("TransactionDateTime is missing in the transaction object", args("transaction", randomTransactionObject));
		}
	}
}
