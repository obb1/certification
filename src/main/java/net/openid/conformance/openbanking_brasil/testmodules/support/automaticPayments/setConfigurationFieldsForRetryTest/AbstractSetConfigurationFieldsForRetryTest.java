package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setConfigurationFieldsForRetryTest;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractSetConfigurationFieldsForRetryTest extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(strings = { "consent_id", "refresh_token" })
	public Environment evaluate(Environment env) {
		String consentId = env.getString("resource", getConsentIdPath(env));
		env.putString("consent_id", consentId);

		String refreshToken = env.getString("resource", getRefreshTokenPath(env));
		env.putString("refresh_token", refreshToken);

		logSuccess("Loaded configuration fields from environment",
			args("consent_id", consentId, "refresh_token", refreshToken));

		return env;
	}

	protected String getConsentIdPath(Environment env) {
		return getBasePath(env) + "ConsentId";
	}

	protected String getRefreshTokenPath(Environment env) {
		return getBasePath(env) + "RefreshToken";
	}

	protected abstract String getBasePath(Environment env);
}
