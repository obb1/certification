package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.PreFlightCertCheckModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckBrazilOrganizationIdIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.CheckConsentUrlForPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.checkConsentsVersion.AbstractCheckConsentsVersion;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractPreFlightCertCheckPaymentsModule extends PreFlightCertCheckModule {

    @Override
    protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
        return super.createGetAccessTokenWithClientCredentialsSequence(clientAuthSequence)
            .replace(SetConsentsScopeOnTokenEndpointRequest.class, condition(SetPaymentsScopeOnTokenEndpointRequest.class));
    }

    @Override
    protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
        callAndContinueOnFailure(CheckBrazilOrganizationIdIsPresent.class, Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(CheckConsentUrlForPayments.class);
        super.preConfigure(config, baseUrl, externalUrlOverride);
    }

    @Override
    protected void runTests() {
        super.runTests();

        runInBlock("Pre-flight Check version of consents API", () -> {
            callAndStopOnFailure(versionCheckingCondition().getClass());
        });
    }

    protected abstract AbstractCheckConsentsVersion versionCheckingCondition();
}
