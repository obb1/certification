package net.openid.conformance.openbanking_brasil.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.testmodules.support.ReplaceMTLSCertificatesWithMTLS4;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "dcr_api_fvp-revoked-certificate_test-module",
	displayName = "FAPI1-Advanced-Final: Brazil DCR expired certificate",
	summary = "Perform the DCR flow, but presenting a revoked certificate. The server must reject the registration attempt return a 400 to the request",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.discoveryUrl",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"mtls4.key",
		"mtls4.cert",
		"mtls4.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase"
	}
)
public class DcrRevokedCertificateTestModule extends AbstractFAPI1AdvancedFinalBrazilDCR {
	@Override
	protected void configureClient() {

		super.configureClient();
	}
	@Override
	protected boolean scopeContains(String requiredScope) {
		// Not needed as scope field is optional
		return false;
	}

	@Override
	protected void callRegistrationEndpoint() {
		callAndStopOnFailure(ReplaceMTLSCertificatesWithMTLS4.class);
		callAndStopOnFailure(ExtractMTLSCertificatesFromConfiguration.class);

		callAndStopOnFailure(CallDynamicRegistrationEndpointAllowingTLSFailure.class, "RFC7591-3.1", "OIDCR-3.2");

		call(exec().mapKey("endpoint_response", "dynamic_registration_endpoint_response"));

		callAndContinueOnFailure(EnsureHttpStatusCodeIs4xxOrFailedTLS.class, Condition.ConditionResult.FAILURE,"OIDCR-3.2");
		call(exec().unmapKey("endpoint_response"));

		eventLog.endBlock();
	}

	@Override
	protected void setupResourceEndpoint() {}

	@Override
	protected void performAuthorizationFlow() {
		fireTestFinished();
	}

	@Override
	protected void logFinalEnv() {
		//Not needed here
	}
}
