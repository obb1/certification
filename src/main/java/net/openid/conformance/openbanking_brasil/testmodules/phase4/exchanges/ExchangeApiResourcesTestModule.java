package net.openid.conformance.openbanking_brasil.testmodules.phase4.exchanges;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetExchangesV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.exchanges.v1.GetExchangesOperationsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractApiResourceTestModuleV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "exchange_api_resources_test-module",
	displayName = "exchange_api_resources_test-module",
	summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
		"\u2022 Call the POST Consents endpoint with the Exchange API Permission Group\n" +
		"\u2022 Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response\n" +
		"\u2022 Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"\u2022 Call the GET Consents endpoint\n" +
		"\u2022 Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
		"\u2022 Call the GET Resources API\n" +
		"\u2022 Expect a 200 success - Extract all of the AVAILABLE resources that have been returned with the tested investment API type - EXCHANGE - Validate Response Body\n" +
		"\u2022 Call the GET operations List Endpoint\n" +
		"\u2022 Expect a 200 - Validate Response - Make sure that all the fields that were set as AVAILABLE on the Resources API are returned on the Investments List Endpoint\n" +
		"\u2022 Call the Delete Consents Endpoints\n" +
		"\u2022 Expect a 204 without a body",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class ExchangeApiResourcesTestModule extends AbstractApiResourceTestModuleV2 {


	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		env.putString("metaOnlyRequestDateTime", "true");
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getApiEndpoint() {
		return GetExchangesV1Endpoint.class;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.EXCHANGES;
	}
	@Override
	protected OPFCategoryEnum getProductCategoryEnum() {
		return OPFCategoryEnum.EXCHANGES;
	}

	@Override
	protected Class<? extends Condition> apiValidator() {
		return GetExchangesOperationsListV1OASValidator.class;
	}

	@Override
	protected String apiName() {
		return "exchanges";
	}

	@Override
	protected String apiResourceId() {
		return "operationId";
	}

	@Override
	protected String getVersion() {
		return "1";
	}

	@Override
	protected String resourceType() {
		return EnumResourcesType.EXCHANGE.getType();
	}

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return GetResourcesOASValidatorV3.class;
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, condition(PostConsentOASValidatorV3n2.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure());
	}
}
