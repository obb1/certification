package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.CreatePaymentErrorEnumV2;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasErroIdempotencia extends AbstractEnsureErrorResponseCodeFieldWas {
	@Override
	protected List<String> getExpectedCodes() {
		return List.of(CreatePaymentErrorEnumV2.ERRO_IDEMPOTENCIA.toString());
	}
}
