package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import com.google.common.base.Strings;
import com.google.gson.*;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonUtils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Random;

public class CheckTransactionDateRange extends AbstractCondition {

    protected final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    protected final LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

    @Override
    @PreEnvironment(strings = {"fromTransactionDate", "toTransactionDate"}, required = "resource_endpoint_response_full")
    public Environment evaluate(Environment env) {
        String fromTransactionDate = env.getString("fromTransactionDate");
        String toTransactionDate = env.getString("toTransactionDate");

        String bodyJsonString = env.getString("resource_endpoint_response_full", "body");
        if (Strings.isNullOrEmpty(bodyJsonString)) {
            throw error("Body element is missing in the resource_endpoint_response_full");
        }
        JsonObject body;
        try {
            Gson gson = JsonUtils.createBigDecimalAwareGson();
            body = gson.fromJson(bodyJsonString, JsonObject.class);
        } catch (JsonSyntaxException e) {
            throw error("Body is not json", args("body", bodyJsonString));
        }

        JsonArray transactions = body.getAsJsonArray("data");
        if (transactions == null || transactions.isEmpty()) {
            throw error("No transactions returned unable to validate the defined behaviour with booking date query parameters",
                args("response", env.getObject("resource_endpoint_response_full"),
                    "body", body,
                    "data", transactions));
        }

        int amountOfTransactions = transactions.size();
        Random random = new Random();
        JsonElement randomTransaction = transactions.get(random.nextInt(amountOfTransactions));
        JsonObject randomTransactionObject = randomTransaction.getAsJsonObject();
        String randomTransactionDate = OIDFJSON.getString(randomTransactionObject.get("transactionDate"));
        validateTransactionDate(randomTransactionDate, fromTransactionDate, toTransactionDate);
        logSuccess("Transaction date parameters successfully validated to be within the specified range");
        env.putString("transactionDate", randomTransactionDate);

        return env;
    }

    protected void validateTransactionDate(String transactionDate, String fromTransactionDate, String toTransactionDate) {
        LocalDate fmtTransactionDate = LocalDate.parse(transactionDate, FORMATTER);
        LocalDate fmtFromTransactionDate = LocalDate.parse(fromTransactionDate, FORMATTER);
        LocalDate fmtToTransactionDate = LocalDate.parse(toTransactionDate, FORMATTER);

        if (fmtTransactionDate.isAfter(fmtToTransactionDate) || fmtTransactionDate.isBefore(fmtFromTransactionDate)) {
            throw error("Transaction returns is not within the range of the specified query parameters", Map.of("Date: ", transactionDate));
        }
    }
}
