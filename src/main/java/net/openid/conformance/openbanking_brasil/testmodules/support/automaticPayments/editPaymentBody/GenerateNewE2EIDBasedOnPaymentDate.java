package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Optional;

public class GenerateNewE2EIDBasedOnPaymentDate extends AbstractCondition {

	static private final String DATE_FORMAT = "^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$";

	@Override
	@PreEnvironment(required = "resource", strings = "ispb")
	@PostEnvironment(strings = "endToEndId")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));

		String date = Optional.ofNullable(data.get("date"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find date field in payments payload", args("data", data)));
		String ispb = env.getString("ispb");

		String endToEndId = generateEndToEndIdWithPaymentDate(date, ispb);
		data.addProperty("endToEndId", endToEndId);
		env.putString("endToEndId", endToEndId);
		logSuccess("Successfully generated a new endToEndId", args("data", data, "endToEndId", endToEndId));

		return env;
	}

	protected String generateEndToEndIdWithPaymentDate(String date, String ispb) {
		// This method gets a date in the format yyyy-MM-dd and transforms it to yyyyMMdd1500, as if the payment is going to be made in the middle of the day
		if (!date.matches(DATE_FORMAT)) {
			throw error("The date field from payments payload is not in a valid format",
				args("date", date, "expected format", "yyyy-MM-dd"));
		}

		String formattedDateTime = date.replaceAll("-", "") + "1500";
		String randomString = RandomStringUtils.randomAlphanumeric(11);

		return String.format("E%s%s%s", ispb, formattedDateTime, randomString);
	}
}
