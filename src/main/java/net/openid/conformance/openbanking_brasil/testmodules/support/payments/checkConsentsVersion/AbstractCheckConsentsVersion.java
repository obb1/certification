package net.openid.conformance.openbanking_brasil.testmodules.support.payments.checkConsentsVersion;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractCheckConsentsVersion extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {
        String consentUrl = env.getString("config", "resource.consentUrl");
        String validatorRegex = "^(https://)(.*?)(payments/v)([0-9])(/consents)";

        Pattern pattern = Pattern.compile(validatorRegex);
        Matcher matcher = pattern.matcher(consentUrl);

        if (matcher.find()) {
            int versionNumber = Integer.parseInt(matcher.group(4));

            if (versionNumber == expectedVersionNumber()) {
                logSuccess("Consents version matches expected value");
            } else {
                throw error("Consents version does not match expected value",
                    args("consents version", versionNumber, "expected version", expectedVersionNumber()));
            }
        } else {
            throw error("consentUrl is not valid, please ensure that url matches " + validatorRegex, args("consentUrl", consentUrl));
        }
        return env;
    }

    protected abstract int expectedVersionNumber();
}
