package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountOverdraft.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

@ApiName("ProductsNServices Unarranged Account Personal Overdraft V1")
public class GetUnarrangedAccountPersonalOverdraftOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-opendata-unarranged-1.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/personal-unarranged-account-overdraft";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
