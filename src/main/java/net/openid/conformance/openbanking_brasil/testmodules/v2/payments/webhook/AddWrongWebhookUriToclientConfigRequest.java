package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.runner.TestDispatcher;
import net.openid.conformance.testmodule.Environment;

public class AddWrongWebhookUriToclientConfigRequest extends AbstractCondition {
	@Override
	@PreEnvironment(required = "dynamic_registration_request")
	@PostEnvironment(required = "dynamic_registration_request")
	public Environment evaluate(Environment env) {
		String baseUrl = env.getString("base_url");
		baseUrl = baseUrl.replaceFirst(TestDispatcher.TEST_PATH, TestDispatcher.TEST_MTLS_PATH);
		String wrongWebhook = String.format("%s/open-banking/webhook/v1", baseUrl);
		JsonObject dynamicRegistrationRequest = env.getObject("dynamic_registration_request");
		JsonArray webhookUris = new JsonArray();
		webhookUris.add(wrongWebhook);
		dynamicRegistrationRequest.add("webhook_uris", webhookUris);
		log("Added webhook_uris to dynamic registration request", args("dynamic_registration_request", dynamicRegistrationRequest));
		return env;
	}
}
