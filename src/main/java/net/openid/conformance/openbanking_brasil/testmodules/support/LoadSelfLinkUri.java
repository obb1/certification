package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadSelfLinkUri extends AbstractCondition {

	@Override
	@PreEnvironment(strings = "saved_self_link_uri")
	public Environment evaluate(Environment env) {

		String selfLink = env.getString("saved_self_link_uri");
		env.putString("protected_resource_url", selfLink);

		log(String.format("Self link loaded for a second GET attempt: %s", selfLink));
		return env;
	}

}
