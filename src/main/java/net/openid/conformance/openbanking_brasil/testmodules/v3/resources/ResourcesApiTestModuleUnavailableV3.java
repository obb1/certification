package net.openid.conformance.openbanking_brasil.testmodules.v3.resources;

import com.google.gson.JsonObject;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForDateHeaderInResourceResponse;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeJson;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureResourceResponseReturnedJsonContentType;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.EnsureUnavailableResourceIsNotOnList;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.SaveUnavailableResourceData;
import net.openid.conformance.openbanking_brasil.resourcesAPI.v2.UpdateSavedResourceData;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureNotOnlyCustomerDataPermissionsWereReturned;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceLastUrlToPollFirstLink;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToNextOrStopPolling;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsBalancesOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2.GetFinancingsWarrantiesV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2.GetInvoiceFinancingsWarrantiesV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans.v2n3.GetLoansWarrantiesV2n3OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.unarrangedAccountsOverdraft.v2n2.GetUnarrangedAccountsOverdraftWarrantiesV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.resourcesAPI.v2.PrepareUrlForApiListForSavedResourceCall;
import net.openid.conformance.openbanking_brasil.testmodules.support.resourcesAPI.v2.PrepareUrlForSavedResourceCall;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ResourceApiV2PollingSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.preCallProtectedResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.UnavailableResourcesApiPollingTimeout;
import net.openid.conformance.openbanking_brasil.testmodules.v2.GenerateRefreshAccessTokenSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "resources_api_unavailable_test-module_v3",
	displayName = "Validates that the server has correctly implemented the expected behaviour for temporarily blocked resources",
	summary = "Validates that the server has correctly implemented the expected behaviour for temporarily blocked resources\n" +
		"\u2022 Creates a CONSENT with all the existing permissions including either business or personal data, depending on what has been provided on the test configuration\n" +
		"\u2022 Expects a Success 201\n" +
		"\u2022 Redirect the user to authorize the CONSENT - Redirect URI must contain all phase 2 scopes\n" +
		"\u2022 Expect a Successful authorization with an authorization code created\n" +
		"\u2022 POLL the GET RESOURCES API with the authorized consent for 10 minutes - Refresh Token to be called each 3 minutes\n" +
		"\u2022 Continue pooling until AT LEAST one Resource returned and is on the state TEMPORARILY_UNAVAILABLE/UNAVAILABLE\n" +
		"\u2022 Evaluate which Resource is on the TEMPORARILY_UNAVAILABLE/UNAVAILABLE state, fetch the resource id, create the base request URI for said resource\n" +
		"\u2022 Call either the CONTRACTS or the ACCOUNTS list API for this Resource\n" +
		"\u2022 Expect a 200 - Make sure the Server returns a 200 without that TEMPORARILY_UNAVAILABLE/UNAVAILABLE resource on it's list\n" +
		"\u2022 Depending on the unavailable resource, call one of the following APIs depending: (1) /contracts/{contractId}/warranties for credit operations, (2) /accounts/{creditCardAccountId}/bills for credit cards, or (3) /accounts/{accountId}/balances for accounts\n" +
		"\u2022 Expect a 403 - Validate that the field response.errors.code is STATUS_RESOURCE_TEMPORARILY_UNAVAILABLE/STATUS_RESOURCE_UNAVAILABLE",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class ResourcesApiTestModuleUnavailableV3 extends AbstractPhase2TestModule {

	private ClientAuthType clientAuthType;

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetResourcesV3Endpoint.class)

		);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);

		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_BUSINESS_PHASE2).addScopesBasedOnCategory(OPFCategoryEnum.ALL_PERSONAL_PHASE2).addScopes(OPFScopesEnum.OPEN_ID).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.ALL_PERSONAL_PHASE2).addScopesBasedOnCategory(OPFCategoryEnum.ALL_BUSINESS_PHASE2).addScopes(OPFScopesEnum.OPEN_ID).build();
		}

		clientAuthType = getVariant(ClientAuthType.class);
		super.onConfigure(config, baseUrl);
	}


	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().mapKey("resource_endpoint_response", "consent_endpoint_response"))
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, condition(PostConsentOASValidatorV3n2.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure())
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().unmapKey("resource_endpoint_response"));
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		env.mapKey("endpoint_response", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureNotOnlyCustomerDataPermissionsWereReturned.class, Condition.ConditionResult.WARNING);
		if (getResult() == Result.WARNING) {
			fireTestFinished();
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}

		} else {
			callAndContinueOnFailure(EnsureContentTypeJson.class, Condition.ConditionResult.FAILURE);
			env.unmapKey("endpoint_response");
			callAndContinueOnFailure(FAPIBrazilConsentEndpointResponseValidatePermissions.class, Condition.ConditionResult.INFO);
			callAndStopOnFailure(ExtractConsentIdFromConsentEndpointResponse.class);
			callAndContinueOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE, "FAPI-R-6.2.1-11", "FAPI1-BASE-6.2.1-11");
			eventLog.endBlock();
		}
	}

	@Override
	protected void requestProtectedResource() {
		eventLog.startBlock(currentClientString() + "Resource server endpoint tests");
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
		call(exec().mapKey("endpoint_response", "resource_endpoint_response_full"));
		callAndStopOnFailure(CheckForDateHeaderInResourceResponse.class);
		callAndStopOnFailure(CheckForFAPIInteractionIdInResourceResponse.class);
		callAndStopOnFailure(EnsureMatchingFAPIInteractionId.class);
		callAndStopOnFailure(EnsureResourceResponseReturnedJsonContentType.class);

		eventLog.endBlock();
		validateResponse();
	}

	@Override
	protected void validateResponse() {

		ResourceApiV2PollingSteps pollingSteps = new ResourceApiV2PollingSteps(env, getId(),
			eventLog, testInfo, getTestExecutionManager());
		runInBlock("Polling Resources API", () -> call(pollingSteps));

		callAndContinueOnFailure(GetResourcesOASValidatorV3.class, Condition.ConditionResult.FAILURE);

		ConditionSequenceRepeater sequenceRepeater = new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager,
			() -> new preCallProtectedResourceSteps()
				.then(condition(SaveUnavailableResourceData.class)
					.dontStopOnFailure()
					.onFail(Condition.ConditionResult.INFO),
					condition(SetProtectedResourceUrlToNextOrStopPolling.class),
					condition(SetProtectedResourceLastUrlToPollFirstLink.class))
		)
			.untilTrue("resource_found")
			.times(30)
			.trailingPause(20)
			.refreshSequence(() -> new GenerateRefreshAccessTokenSteps(clientAuthType), 9)
			.onTimeout(sequenceOf(condition(UnavailableResourcesApiPollingTimeout.class), condition(ChuckWarning.class)));
		sequenceRepeater.run();

		runInBlock("Ensure we cannot see the given unavailable resource in its api list.", () -> {
			callAndStopOnFailure(UpdateSavedResourceData.class);
			callAndStopOnFailure(PrepareUrlForApiListForSavedResourceCall.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureUnavailableResourceIsNotOnList.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Ensure we cannot access the unavailable resource.", () -> {
			callAndStopOnFailure(PrepareUrlForSavedResourceCall.class);
			callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas403.class, Condition.ConditionResult.FAILURE);
			JsonObject resource = env.getObject("resource_data");
			String type = OIDFJSON.getString(resource.get("type"));
			switch (type){
				case "ACCOUNT":
					callAndContinueOnFailure(GetAccountsBalancesOASValidatorV2n4.class,Condition.ConditionResult.FAILURE);
					break;
				case "CREDIT_CARD_ACCOUNT":
					callAndContinueOnFailure(GetCreditCardAccountsBillsOASValidatorV2.class,Condition.ConditionResult.FAILURE);
					break;
				case "LOAN":
					callAndContinueOnFailure(GetLoansWarrantiesV2n3OASValidator.class,Condition.ConditionResult.FAILURE);
					break;
				case "FINANCING":
					callAndContinueOnFailure(GetFinancingsWarrantiesV2n2OASValidator.class,Condition.ConditionResult.FAILURE);
					break;
				case "UNARRANGED_ACCOUNT_OVERDRAFT":
					callAndContinueOnFailure(GetUnarrangedAccountsOverdraftWarrantiesV2n2OASValidator.class,Condition.ConditionResult.FAILURE);
					break;
				case "INVOICE_FINANCING":
					callAndContinueOnFailure(GetInvoiceFinancingsWarrantiesV2n2OASValidator.class,Condition.ConditionResult.FAILURE);
					break;
				default:
					throw new TestFailureException(getId(), "Resource Type Not Expected");
			}
			callAndContinueOnFailure(GetLoansWarrantiesV2n3OASValidator.class, Condition.ConditionResult.FAILURE);
		});
	}
}
