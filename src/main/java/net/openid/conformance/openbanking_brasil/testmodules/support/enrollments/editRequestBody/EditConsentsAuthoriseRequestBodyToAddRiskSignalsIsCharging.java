package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditConsentsAuthoriseRequestBodyToAddRiskSignalsIsCharging extends AbstractCondition {

	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";

	@Override
	@PreEnvironment(required = REQUEST_CLAIMS_KEY)
	public Environment evaluate(Environment env) {
		JsonObject riskSignals = Optional.ofNullable(env.getElementFromObject(REQUEST_CLAIMS_KEY, "data.riskSignals"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Could not find riskSignals inside consents authorise request body"));

		riskSignals.addProperty("isCharging", true);

		logSuccess("Added \"isCharging\" field to consents authorise request body", args("riskSignals", riskSignals));

		return env;
	}
}
