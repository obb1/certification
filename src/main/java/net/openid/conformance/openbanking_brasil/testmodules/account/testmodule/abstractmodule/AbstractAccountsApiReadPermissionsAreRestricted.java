package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountBalances;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountLimits;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountResource;
import net.openid.conformance.openbanking_brasil.testmodules.account.PrepareUrlForFetchingAccountTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AccountSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromBookingDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFPermissionsEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public abstract class AbstractAccountsApiReadPermissionsAreRestricted extends AbstractPhase2TestModule {
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");


	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountListValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountBalancesValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountIdentificationValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountLimitsValidator();
	protected abstract Class<? extends OpenAPIJsonSchemaValidator> getAccountTransactionsValidator();


	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class),
			condition(GetAccountsV2Endpoint.class));
	}

	@Override
	protected void configureClient() {
		super.configureClient();

		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		env.putString("fromBookingDate", currentDate.minusDays(360).format(FORMATTER));
		env.putString("toBookingDate", currentDate.format(FORMATTER));

	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.ACCOUNTS, OPFScopesEnum.OPEN_ID).addPermissions(OPFPermissionsEnum.ACCOUNTS_READ, OPFPermissionsEnum.ACCOUNTS_TRANSACTIONS_READ, OPFPermissionsEnum.RESOURCES_READ).build();
		callAndStopOnFailure(FAPIBrazilOpenBankingCreateConsentRequest.class);
	}

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(getAccountListValidator(), Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(AccountSelector.class);
		callAndStopOnFailure(PrepareUrlForFetchingAccountResource.class);
		callProtectedResourceAndValidateResponse("Fetch Account V2", getAccountIdentificationValidator());
		runInBlock("Ensure we can call the account transactions API V2", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingAccountTransactions.class);
			callAndStopOnFailure(AddToAndFromBookingDateParametersToProtectedResourceUrl.class);
			callProtectedResourceAndValidateResponse(null, getAccountTransactionsValidator());
		});

		runInBlock("Ensure we cannot call the account balance API V2", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingAccountBalances.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getAccountBalancesValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});


		runInBlock("Ensure we cannot call the account limits API V2", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingAccountLimits.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(getAccountLimitsValidator(), Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
		});

		fireTestReviewNeeded();

	}

	protected void callProtectedResourceAndValidateResponse(String blockName, Class<? extends Condition> validator) {
		preCallProtectedResource(blockName);
		callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
	}
}
