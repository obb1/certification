package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractWaitUntilMidnight;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Supplier;

public class WaitForMidnight extends AbstractWaitUntilMidnight {

	private static final ZoneId SAO_PAULO = ZoneId.of("America/Sao_Paulo");
	public WaitForMidnight() {
		super(() -> ZonedDateTime.now(SAO_PAULO));
	}

	public WaitForMidnight(Supplier<ZonedDateTime> currentTimeSupplier) {
		super(currentTimeSupplier);
	}
}
