package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AbstractEditObjectBodyToManipulateCnpjOrCpf;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EditRecurringPaymentsConsentBodyToSetInvalidCreditor extends AbstractEditObjectBodyToManipulateCnpjOrCpf {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject creditor = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonArray("creditors"))
			.map(creditors -> creditors.get(0).getAsJsonObject())
			.orElseThrow(() -> error("Unable to find a creditor inside data.creditors in consents payload"));

		String currentIdentification = OIDFJSON.getString(
			Optional.ofNullable(creditor.get("cpfCnpj"))
				.orElseThrow(() -> error("Unable to find cpfCnpj field for the creditor", args("creditor", creditor)))
		);

		String newIdentification;
		switch (currentIdentification.length()) {
			case CPF_DIGITS:
				newIdentification = generateNewValidCpfFromDifferentRegion(currentIdentification);
				break;
			case CNPJ_DIGITS:
				newIdentification = generateNewRandomCnpj();
				break;
			default:
				throw error("The value inside the cpfCnpj field is neither a CPF nor a CNPJ",
					args("cpfCnpj", currentIdentification));
		}

		creditor.addProperty("cpfCnpj", newIdentification);
		logSuccess("Updated first creditor to have a different identification", args(
			"old identification", currentIdentification,
			"new identification", newIdentification,
			"creditor", creditor
		));

		return env;
	}
}
