package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.AbstractCreditOperationsXFapiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvoiceFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractCreditOperationsDiscontinuedCreditRightsApiXFapiTestModule extends AbstractCreditOperationsXFapiTestModule {

	@Override
	protected abstract Class<? extends Condition> getContractsListValidator();

	@Override
	protected abstract Class<? extends Condition> getContractIdentificationValidator();

	@Override
	protected abstract Class<? extends Condition> getContractWarrantiesValidator();

	@Override
	protected abstract Class<? extends Condition> getContractScheduledInstalmentsValidator();

	@Override
	protected abstract Class<? extends Condition> getContractPaymentsValidator();

	@Override
	protected OPFCategoryEnum getCategory() {
		return OPFCategoryEnum.CREDIT_OPERATIONS;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.INVOICE_FINANCINGS;
	}

	@Override
	protected EnumResourcesType getApiType() {
		return EnumResourcesType.INVOICE_FINANCING;
	}

	@Override
	protected Class<? extends Condition> getGetConsentValidator() {
		return GetConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetInvoiceFinancingsV2Endpoint.class)
		);
	}
}
