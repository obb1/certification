package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiContentTypeJwtRefreshTokenTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_contenttype-jwt-refreshtoken_test-module_v4",
	displayName = "Payments API v4 test module for refresh token rotation and accept header",
	summary = "This test is a PIX Payments current date test that aims to test both the refresh token rotation expected behavior and also how the instituion must handles the accept header when it's not sent as application/jwt\n" +
		"\u2022 Call POST token endpoint using client-credentials grant, however request the scopes=\"payments\" and \"openid\"\n" +
		"\u2022 Expect 201 - Created - Make sure token has been issued with only payments scope, ignoring the openid additional scope, and returning the scope optional object in line with https://www.rfc-editor.org/rfc/rfc6749#section-3.3\n" +
		"\u2022 Create consent with valid e-mail payload, localInstrument to be set to DICT\n" +
		"\u2022 Call the POST Consents endpoints, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects 201 - Make sure a JWT is returned - Validate response \n" +
		"\u2022 Redirects the user to authorize the created consent\n" +
		"\u2022 Call GET Consent\n" +
		"\u2022 Expects 200 - Validate if status is \"AUTHORISED\" and validate response\n" +
		"\u2022 Call the POST token endpoint with refresh token grant\n" +
		"\u2022 Expect a new access token to be generated - Make sure the refresh token hasn’t been rotated\n" +
		"\u2022 Call the POST Payments Endpoint with the token obtained from the refresh token grant, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects 201 - Make sure a JWT is returned - Validate response \n" +
		"\u2022 Call the GET Payments endpoint with token issued with client_credentials grant, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects either a 200 - make sure response is JWT\n" +
		"\u2022 Call the GET Payments endpoint with token issued with client_credentials grant, set header \"accept\": \"application/json\"\n" +
		"\u2022 Expects either a 200 or a 406 response - If 200 make sure response is JWT\n" +
		"\u2022 Call the GET Consents endpoint with token issued with client_credentials grant, set header \"accept\": \"*/*\"\n" +
		"\u2022 Expects either a 200 - make sure response is JWT - Make sure that the self link returned is exactly equal to the requested URI\n" +
		"\u2022 Call the GET Consents endpoint with token issued with client_credentials grant, set header \"accept\": \"application/json\"\n" +
		"\u2022 Expects either a 200 or a 406 response - If 200 make sure response is JWT",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiContentTypeJwtRefreshTokenTestModuleV4 extends AbstractPaymentsApiContentTypeJwtRefreshTokenTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected  Class<? extends Condition> postPaymentConsentValidator(){
		return PostPaymentsConsentOASValidatorV4.class;
	}
	@Override
	protected  Class<? extends Condition> postPaymentValidator(){
		return PostPaymentsPixOASValidatorV4.class;
	}
	@Override
	protected  Class<? extends Condition> getPaymentConsentValidator(){
		return GetPaymentsConsentOASValidatorV4.class;
	}
	@Override
	protected  Class<? extends Condition> getPaymentValidator(){
		return GetPaymentsPixOASValidatorV4.class;
	}
	@Override
	protected  Class<? extends Condition> paymentConsentErrorValidator(){
		return PostPaymentsConsentOASValidatorV4.class;
	}


}
