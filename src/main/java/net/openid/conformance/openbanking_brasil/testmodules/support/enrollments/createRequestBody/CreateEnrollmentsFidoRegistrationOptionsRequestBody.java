package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class CreateEnrollmentsFidoRegistrationOptionsRequestBody extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    @PostEnvironment(required = "resource_request_entity_claims", strings = "rp_id")
    public Environment evaluate(Environment env) {
        String commonName = getSubjectsCommonNameFromCertificate(env);

        JsonObject data = new JsonObjectBuilder()
            .addField("platform", "BROWSER")
            .addField("rp", commonName)
            .build();
        JsonObject requestBody = new JsonObject();
        requestBody.add("data", data);

        env.putObject("resource_request_entity_claims", requestBody);
		env.putString("rp_id", commonName);
        logSuccess("Fido registration options request body was created and added to environment",
            args("fido registration options request body", requestBody));

        return env;
    }

    private String getSubjectsCommonNameFromCertificate(Environment env) {
        String certificateString = env.getString("config", "mtls.cert");
        if (Strings.isNullOrEmpty(certificateString)) {
            throw error("Could not find mtls certificate in config");
        }

        try {
            byte[] certificateBytes = certificateString.getBytes();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(certificateBytes);

            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(byteArrayInputStream);

            String commonName = "";
            if (certificate != null) {
                X500Name x500name = X500Name.getInstance(certificate.getSubjectX500Principal().getEncoded());
                RDN[] cnArray = x500name.getRDNs(BCStyle.CN);
                if (cnArray.length == 0) {
                    throw error("Certificate does not contain CN");
                }
                commonName = IETFUtils.valueToString(cnArray[0].getFirst().getValue());
            }
            if (Strings.isNullOrEmpty(commonName)) {
                throw error("Could not find CN in the certificate");
            }

            return commonName;
        } catch (CertificateException err) {
            throw error("Could not parse certificate", err);
        }
    }
}
