package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.editAutomaticRecurringConfigurationObject;

import com.google.gson.JsonObject;

public class EditRecurringPaymentsAutomaticConsentBodyToAddBothFixedAndVariableAmounts extends AbstractEditRecurringPaymentsAutomaticConsentBody {

	@Override
	protected void updateAutomaticObject(JsonObject automatic) {
		automatic.addProperty("fixedAmount", "1.00");
		automatic.addProperty("maximumVariableAmount", "1.00");
		automatic.remove("minimumVariableAmount");
	}

	@Override
	protected String logMessage() {
		return "have both \"fixedAmount\" and \"maximumVariableAmount\" fields";
	}
}
