package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.RandomStringUtils;

public class CreateAutomaticRecurringConfigurationObjectSemanalRandomAccountNumberAndIspb extends AbstractCreateAutomaticRecurringConfigurationObject {

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected String getFixedAmount() {
		return "1.00";
	}

	@Override
	protected JsonObject buildRecurringConfiguration(Environment env) {
		JsonObject recurringConfiguration = super.buildRecurringConfiguration(env);
		JsonObject creditorAccount = recurringConfiguration.getAsJsonObject("automatic")
			.getAsJsonObject("firstPayment")
			.getAsJsonObject("creditorAccount");

		creditorAccount.addProperty("number", RandomStringUtils.randomNumeric(1, 21));
		creditorAccount.addProperty("ispb", RandomStringUtils.randomNumeric(8, 9));

		return recurringConfiguration;
	}
}
