package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiDebtorAccountTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.GetEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostConsentsAuthoriseOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostFidoRegistrationOptionsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostFidoSignOptionsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_debtor-account_test-module_v2",
	displayName = "enrollments_api_debtor-account_test-module_v2",
	summary = "Ensure a payment cannot be authorized if a different debtorAccount is set\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Execute a full enrollment journey sending an invalid debtorAccount at the POST enrollments request, the origin extracted above, make sure that debtorAccount is still invalid before redirection, extract the enrollment ID, and store the refresh_token generated ensuring it has the nrp-consents scope\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AUTHORISED\"\n" +
		"• Call POST consent with valid payload, but sending the same invalid debtorAccount number used in POST enrollments request.\n" +
		"• Expects 201 - Validate response\n" +
		"• Call the POST sign-options endpoint with the consentID created\n" +
		"• Expect 201 - Validate response and extract challenge\n" +
		"• Calls the POST Token endpoint with the refresh_token grant, ensuring a token with the nrp-consent scope was issued\n" +
		"• Call POST consent authorize with valid payload, signing the challenge with the compliant private key\n" +
		"• Expects 422 CONTA_DEBITO_DIVERGENTE_CONSENTIMENTO_VINCULO - Validate response\n" +
		"• Call GET Consent Endpoint\n" +
		"• Expects 200 - Validate if status is REJECTED, and rejectionReason is CONTA_NAO_PERMITE_PAGAMENTO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class EnrollmentsApiDebtorAccountTestModuleV2 extends AbstractEnrollmentsApiDebtorAccountTestModule {

	@Override
	protected Class<? extends Condition> postConsentsAuthoriseValidator() {
		return PostConsentsAuthoriseOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postFidoSignOptionsValidator() {
		return PostFidoSignOptionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetEnrollmentsV2Endpoint.class));
	}

	@Override
	public void cleanup() {}
}
