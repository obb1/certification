package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.editPaymentRequestClaims;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditRecurringPaymentRequestClaimsToRemoveCreditorAccount extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource_request_entity_claims", "data"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Unable to find data in recurring-payments request payload"));

		data.remove("creditorAccount");

		logSuccess("Updated recurring-payments request data to remove creditorAccount field", args("data", data));

		return env;
	}
}
