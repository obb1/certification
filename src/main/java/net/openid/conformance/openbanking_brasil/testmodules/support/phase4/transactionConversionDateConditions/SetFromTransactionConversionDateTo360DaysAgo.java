package net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions;

public class SetFromTransactionConversionDateTo360DaysAgo extends AbstractSetFromTransactionConversionDate {

    @Override
    protected int amountOfDaysToThePast() {
        return 360;
    }
}
