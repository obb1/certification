package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class LoadConsentsBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"resource", "savedBrazilPaymentConsent"})
	public Environment evaluate(Environment env) {
		JsonObject savedConsentsBody = env.getObject("savedBrazilPaymentConsent");
		env.putObject("resource", "brazilPaymentConsent",
			savedConsentsBody.deepCopy());
		logSuccess("Consents request body has been loaded");
		return env;
	}
}
