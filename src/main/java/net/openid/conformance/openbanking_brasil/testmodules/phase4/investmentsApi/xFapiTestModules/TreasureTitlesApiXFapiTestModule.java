package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitleBalancesV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitleIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitlesListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitlesTransactionsCurrentV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1.GetTreasureTitlesTransactionsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToTreasureTitles;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo360DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo6DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "treasure-titles_api_x-fapi_test-module",
	displayName = "treasure-titles_api_x-fapi_test-module",
	summary = "Ensure that the x-fapi-interaction-id is required at the request for all endpoints\n" +
		"• Call the POST Consents endpoint with the Investments Permission Group\n" +
		"• Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint\n" +
		"• Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"• Call the GET Investments Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments Endpoint with the x-fapi-interaction-id\n" +
		"• Expects 200 - Validate response and extract InvestmentId\n" +
		"• Call the GET Investments/{InvestmentId} Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments/{InvestmentId} Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET balances Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET balances Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction current Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction current Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class TreasureTitlesApiXFapiTestModule extends AbstractInvestmentsApiXFapiTestModule {

	@Override
	protected AbstractSetInvestmentApi setInvestmentsApi() {
		return new SetInvestmentApiToTreasureTitles();
	}

	@Override
	protected Class<? extends Condition> investmentsRootValidator() {
		return GetTreasureTitlesListV1OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsIdentificationValidator() {
		return GetTreasureTitleIdentificationV1OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsBalancesValidator() {
		return GetTreasureTitleBalancesV1OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsValidator() {
		return GetTreasureTitlesTransactionsV1OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsCurrentValidator() {
		return GetTreasureTitlesTransactionsCurrentV1OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> setToTransactionToToday() {
		return SetToTransactionDateToToday.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo360DaysAgo() {
		return SetFromTransactionDateTo360DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo6DaysAgo() {
		return SetFromTransactionDateTo6DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
		return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
	}

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.TREASURE_TITLES;
	}
}
