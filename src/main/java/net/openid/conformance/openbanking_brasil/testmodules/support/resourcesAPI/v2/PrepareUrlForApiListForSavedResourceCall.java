package net.openid.conformance.openbanking_brasil.testmodules.support.resourcesAPI.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class PrepareUrlForApiListForSavedResourceCall extends ResourceBuilder {

	@Override
	@PreEnvironment(required = "resource_data")
	public Environment evaluate(Environment env) {
		setAllowDifferentBaseUrl(true);

		JsonObject resource = env.getObject("resource_data");
		setApi(OIDFJSON.getString(resource.get("resource_api")));
		setEndpoint(OIDFJSON.getString(resource.get("resource_list_endpoint")));

		// Fix for OCI-2884 - remove when accounts V3 is available
		if (!OIDFJSON.getString(resource.get("resource_api")).equals("resources")  && !OIDFJSON.getString(resource.get("resource_api")).equals("consents")) {
			setVersion(2);
			log("Endpoint version set to v2");
		}

		return super.evaluate(env);
	}
}
