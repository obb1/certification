package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

public class GenerateClientExtensionResults extends AbstractFidoKeysCondition {

	// Registration Options Response OR Sign Options Response
	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	protected Optional<JsonElement> bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY);
		} catch (ParseException e) {
			throw error("Error parsing JWT response");
		}
	}

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	@PostEnvironment(required = "client_extension_results")
	public Environment evaluate(Environment env) {
		JsonObject data = bodyFrom(env)
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("It was impossible to extract data from the fido-registration-options response body"));
		JsonObject extensions = data.getAsJsonObject("extensions");

		if (extensions == null) {
			logSuccess("There is no \"extensions\" field in fido-registration-options response body");
			env.putObject("client_extension_results", new JsonObject());
			return env;
		}

		JsonObject extensionResults = new JsonObject();

		for (String extensionId : extensions.keySet()) {
			switch (extensionId) {
				case "appid":
				case "appidExclude":
				case "authnSel":
				case "credBlob":
					extensionResults.addProperty(extensionId, true);
					break;
				case "hmac-secret":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						extensionResults.addProperty(extensionId, true);
					}
					break;
				case "uvm":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						JsonArray uvm = new JsonArray();
						uvm.add(
							new JsonObjectBuilder()
								.addField("userVerificationMethod", 2)
								.addField("keyProtectionType", 4)
								.addField("matcherProtectionType", 2)
								.build()
						);
						uvm.add(
							new JsonObjectBuilder()
								.addField("userVerificationMethod", 4)
								.addField("keyProtectionType", 1)
								.addField("matcherProtectionType", 1)
								.build()
						);
						extensionResults.add(extensionId, uvm);
					}
					break;
				case "credProps":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						JsonObject credProps = new JsonObjectBuilder().addField("rk", true).build();
						extensionResults.add(extensionId, credProps);
					}
					break;
				case "largeBlob":
					JsonObject largeBlobInput = extensions.getAsJsonObject(extensionId);
					if (largeBlobInput.has("read") || largeBlobInput.has("write")) {
						throw error("The 'largeBlob' extension input should not contain neither 'read' nor 'write' fields during registration",
							args("largeBlob input", largeBlobInput));
					}
					JsonObject largeBlobOutput = new JsonObjectBuilder().addField("supported", true).build();
					extensionResults.add(extensionId, largeBlobOutput);
					break;
				case "txAuthSimple":
					extensionResults.addProperty(extensionId, OIDFJSON.getString(extensions.get(extensionId)));
					break;
				case "txAuthGeneric":
					byte[] content = OIDFJSON.getString(extensions.getAsJsonObject(extensionId).get("content")).getBytes();
					byte[] contentHash = getSha256Digest(content);
					String encodedContent = Base64.getUrlEncoder().withoutPadding().encodeToString(contentHash);
					extensionResults.addProperty(extensionId, encodedContent);
					break;
				case "exts":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						List<String> extensionsSupportedList = Arrays.asList(
							"appid", "txAuthSimple", "txAuthGeneric", "authnSel", "exts", "uvi", "loc", "uvm",
							"credProtect", "credBlob", "largeBlobKey", "minPinLength", "hmac-secret", "appidExclude",
							"credProps", "largeBlob", "payment"
						);
						JsonArray extensionsSupported = new Gson().toJsonTree(extensionsSupportedList).getAsJsonArray();
						extensionResults.add(extensionId, extensionsSupported);
					}
					break;
				case "uvi":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						extensionResults.addProperty(extensionId, "1234567");
					}
					break;
				case "loc":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						JsonObject loc = new JsonObjectBuilder()
							.addField("accuracy", 65)
							.addField("latitude", -22)
							.addField("longitude", -43)
							.addField("altitude", 65)
							.addField("altitudeAccuracy", 10)
							.addField("heading", 120)
							.addField("speed", 10)
							.build();
						extensionResults.add(extensionId, loc);
					}
					break;
				case "credProtect":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						extensionResults.addProperty(extensionId, 1);
					}
					break;
				case "largeBlobKey":
				case "payment":
					break;
				case "minPinLength":
					if (OIDFJSON.getBoolean(extensions.get(extensionId))) {
						extensionResults.addProperty(extensionId, 4);
					}
					break;
				default:
					throw error("The extension identifier listed is not a valid one", args("extension identifier", extensionId));
			}
		}

		env.putObject("client_extension_results", extensionResults);
		logSuccess("The clientExtensionResults field has been generated based on the fido-registration-options response",
			args("clientExtensionResults", extensionResults));

		return env;
	}
}
