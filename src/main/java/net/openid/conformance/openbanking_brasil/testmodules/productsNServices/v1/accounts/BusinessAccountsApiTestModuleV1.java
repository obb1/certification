package net.openid.conformance.openbanking_brasil.testmodules.productsNServices.v1.accounts;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.LogOnlyFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToGetProductsNChannelsApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.accounts.v1.GetBusinessAccountsOASValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "business-accounts_api_structural_test-module_v1",
	displayName = "Validate structure of ProductsNServices - BusinessAccounts API Api resources",
	summary =
		"\u2022 Make a unauthenticated request at the required Endpoint\n" +
		"\u2022 Expect 200 - Validate Response",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4

)
public class BusinessAccountsApiTestModuleV1 extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate ProductsNServices Business Accounts response", () -> {
			callAndStopOnFailure(PrepareToGetProductsNChannelsApi.class);
			preCallResource();
			callAndContinueOnFailure(DoNotStopOnFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(LogOnlyFailure.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetBusinessAccountsOASValidatorV1.class, Condition.ConditionResult.FAILURE);
		});
	}
}
