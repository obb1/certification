package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractFvpConsentApiNegativeTests;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-consents_api_negative_test-module_v3-2",
	displayName = "Runs various negative tests",
	summary = "Runs various negative tests\n" +
		"• Creates a Consent with only RESOURCES_READ permissions V3\n" +
		"• Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"• Creates consent with only customers identification permission group (\"CUSTOMERS_PERSONAL_IDENTIFICATIONS_READ\") or (\"CUSTOMERS_BUSINESS_IDENTIFICATIONS_READ\")\n" +
		"• Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"• Creates consent with incomplete credit card permission group (\"CREDIT_CARDS_ACCOUNTS_BILLS_READ\",\"CREDIT_CARDS_ACCOUNTS_READ\",\"RESOURCES_READ\")\n" +
		"• Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"• Creates consent with incomplete account permission group 1 (\"ACCOUNTS_READ\")\n" +
		"• Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"• Creates consent with incomplete account permission group 2 (\"ACCOUNTS_READ\",\"RESOURCES_READ\")\n" +
		"• Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"• Creates consent with incomplete Limits & Credit Operations Contract Data permissions group (\"RESOURCES_READ\", \"LOANS_READ\", \"LOANS_WARRANTIES_READ\", \"LOANS_SCHEDULED_INSTALMENTS_READ\", \"LOANS_PAYMENTS_READ\", \"FINANCINGS_READ\", \"FINANCINGS_WARRANTIES_READ\", \"FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"FINANCINGS_PAYMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_WARRANTIES_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_SCHEDULED_INSTALMENTS_READ\", \"UNARRANGED_ACCOUNTS_OVERDRAFT_PAYMENTS_READ\", \"INVOICE_FINANCINGS_READ\", \"INVOICE_FINANCINGS_WARRANTIES_READ\", \"INVOICE_FINANCINGS_SCHEDULED_INSTALMENTS_READ\", \"INVOICE_FINANCINGS_PAYMENTS_READ\", \"RESOURCES_READ\")\n" +
		"• Expects failure with a 422 COMBINACAO_PERMISSOES_INCORRETA - Validate error message\n" +
		"• Creates consent with non-existent permission group 2 (\"BAD_PERMISSION\")\n" +
		"• Expects failure with a 400 Error\n" +
		"• Creates consent api V3 request for DateTime greater than 1 year from now\n" +
		"• Expects failure with a 422 Error\n" +
		"• Creates consent api V3 request for DateTime in the past\n" +
		"• Expects failure with a 422 Error\n" +
		"• Creates consent api V3 request for DateTime poorly formed.\n" +
		"• Expects failure with a 400 Error",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl"
	}
)
public class FvpConsentApiNegativeTestsV3n extends AbstractFvpConsentApiNegativeTests {

	@Override
	protected Class<? extends Condition> postConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}
}
