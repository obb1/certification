package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors;

import com.google.gson.JsonArray;
import net.openid.conformance.testmodule.Environment;

public class EditRecurringPaymentsConsentBodyToSetFourCreditors extends AbstractEditRecurringPaymentsConsentBodyToSetCreditor {

	@Override
	protected void addCreditorsToArray(JsonArray creditors) {
		creditors.add(buildCreditor("PESSOA_JURIDICA", "98380199000125", "Lucio Pelucio Ltda."));
		creditors.add(buildCreditor("PESSOA_JURIDICA", "98380199000206", "Melo Caramelo Eireli"));
		creditors.add(buildCreditor("PESSOA_JURIDICA", "98380199000397", "Cachorro Reflexivo Ltda."));
		creditors.add(buildCreditor("PESSOA_JURIDICA", "98380199000478", "Amigo com Patas Ltda."));
	}

	@Override
	protected String logMessage() {
		return "4 creditors with different CNPJs";
	}

	@Override
	protected void additionalSteps(Environment env, JsonArray creditors) {
		env.putObject("first_creditor", creditors.get(0).getAsJsonObject());
		env.putObject("second_creditor", creditors.get(1).getAsJsonObject());
	}
}
