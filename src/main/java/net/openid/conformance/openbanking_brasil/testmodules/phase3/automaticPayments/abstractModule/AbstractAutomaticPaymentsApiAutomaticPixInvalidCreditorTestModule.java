package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixUnhappyConsentPathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectTrimestral;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.EditRecurringPaymentsConsentBodyToSetOnePessoaNaturalCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors.EditRecurringPaymentsConsentBodyToSetTwoPessoaJuridicaCreditors;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixInvalidCreditorTestModule extends AbstractAutomaticPaymentsAutomaticPixUnhappyConsentPathTestModule {

	@Override
	protected void performTestSteps() {
		performTestStep(
			", sending two PJ accounts at the creditor information",
			EditRecurringPaymentsConsentBodyToSetTwoPessoaJuridicaCreditors.class,
			EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class
		);

		performTestStep(
			", sending a PF at the creditor information",
			EditRecurringPaymentsConsentBodyToSetOnePessoaNaturalCreditor.class,
			EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class
		);
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectTrimestral();
	}
}
