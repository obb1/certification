package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public abstract class AbstractPostConsentExtendsOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/consents/{consentId}/extends";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}

	@Override
	protected ResponseEnvKey getResponseEnvKey() {
		return ResponseEnvKey.FullConsentResponseEnvKey;
	}
}
