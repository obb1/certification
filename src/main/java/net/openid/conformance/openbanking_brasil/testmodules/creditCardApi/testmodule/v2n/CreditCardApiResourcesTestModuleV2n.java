package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetCreditCardsAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3.GetResourcesOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractApiResourceTestModuleV2;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "credit-cards_api_resources_test-module_v2-1",
	displayName = "Validate structure of Credit Card API and Resources API resources V2",
	summary = "Makes sure that the Resource API and the API that is the scope of this test plan are returning the same available IDs\n" +
		"\u2022Create a consent with all the permissions needed to access the tested API\n" +
		"\u2022 Expects server to create the consent with 201\n" +
		"\u2022 Redirect the user to authorize at the financial institution\n" +
		"\u2022 Call the tested resource API V2\n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the id provided by this API\n" +
		"\u2022 Call the resources API V2\n" +
		"\u2022 Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class CreditCardApiResourcesTestModuleV2n extends AbstractApiResourceTestModuleV2 {

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return GetResourcesOASValidatorV3.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getApiEndpoint() {
		return GetCreditCardsAccountsV2Endpoint.class;
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.CREDIT_CARDS_ACCOUNTS;
	}

	@Override
	protected OPFCategoryEnum getProductCategoryEnum() {
		return OPFCategoryEnum.CREDIT_CARDS_ACCOUNTS;
	}

	@Override
	protected Class<? extends Condition> apiValidator() {
		return GetCreditCardAccountsListOASValidatorV2.class;
	}

	@Override
	protected String apiName() {
		return "credit card";
	}

	@Override
	protected String apiResourceId() {
		return "creditCardAccountId";
	}

	@Override
	protected String resourceType() {
		return EnumResourcesType.CREDIT_CARD_ACCOUNT.name();
	}

}

