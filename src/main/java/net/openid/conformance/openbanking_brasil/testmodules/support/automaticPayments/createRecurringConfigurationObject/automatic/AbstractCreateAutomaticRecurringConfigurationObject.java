package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractCreateAutomaticRecurringConfigurationObject extends AbstractCreateRecurringConfigurationObject {

	protected static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	protected JsonObject buildRecurringConfiguration(Environment env) {
		JsonObjectBuilder automaticBuilder = new JsonObjectBuilder()
			.addField("contractId", getContractId())
			.addField("contractDebtor.name", getContractDebtorName(env))
			.addFields("contractDebtor.document", Map.of(
				"identification", getContractDebtorDocumentIdentification(env),
				"rel", getContractDebtorDocumentRel(env)
			))
			.addField("isRetryAccepted", isRetryAccepted())
			.addField("referenceStartDate", getReferenceStartDate());

		if (getInterval() != null) {
			automaticBuilder.addField("interval", getInterval().toString());
		}

		if (getFixedAmount() != null) {
			automaticBuilder.addField("fixedAmount", getFixedAmount());
		}
		if (getMinimumVariableAmount() != null) {
			automaticBuilder.addField("minimumVariableAmount", getMinimumVariableAmount());
		}
		if (getMaximumVariableAmount() != null) {
			automaticBuilder.addField("maximumVariableAmount", getMaximumVariableAmount());
		}

		if (hasFirstPayment()) {
			automaticBuilder.addFields("firstPayment", Map.of(
				"type", "PIX",
				"date", getFirstPaymentDate(),
				"currency", "BRL",
				"amount", getFirstPaymentAmount()
			)).addFields("firstPayment.creditorAccount", Map.of(
				"ispb", getCreditorAccountField(env, "Ispb"),
				"issuer", getCreditorAccountField(env, "Issuer"),
				"accountType", getCreditorAccountField(env, "AccountType"),
				"number", getCreditorAccountField(env, "Number")
			));
		}

		return new JsonObjectBuilder().addField("automatic", automaticBuilder.build()).build();
	}

	protected String getContractId() {
		return "XE00038166201907261559y6j6";
	}

	protected abstract RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval();

	protected String getContractDebtorName(Environment env) {
		return Optional.ofNullable(env.getElementFromObject("config", "resource.contractDebtorName"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find contractDebtorName in the resource"));
	}

	protected String getContractDebtorDocumentIdentification(Environment env) {
		return Optional.ofNullable(env.getElementFromObject("config", "resource.contractDebtorIdentification"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find contractDebtorIdentification in the resource"));
	}

	protected String getContractDebtorDocumentRel(Environment env) {
		return switch (getContractDebtorDocumentIdentification(env).length()) {
			case 11 -> "CPF";
			case 14 -> "CNPJ";
			default -> throw error("Value set for contractDebtor identification is neither CPF nor CNPJ");
		};
	}

	protected boolean isRetryAccepted() {
		return false;
	}

	protected String getReferenceStartDate() {
		LocalDate today = LocalDate.now(ZoneOffset.UTC).plusDays(1);
		return today.format(DATE_FORMATTER);
	}

	protected String getFixedAmount() {
		return "0.50";
	}

	protected String getMinimumVariableAmount() {
		return null;
	}

	protected String getMaximumVariableAmount() {
		return null;
	}

	protected boolean hasFirstPayment() {
		return true;
	}

	protected String getFirstPaymentAmount() {
		return "1.00";
	}

	protected String getFirstPaymentDate() {
		LocalDate today = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		return today.format(DATE_FORMATTER);
	}

	protected String getCreditorAccountField(Environment env, String field) {
		return Optional.ofNullable(env.getElementFromObject("config", "resource.creditorAccount" + field))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error(String.format("Could not find creditorAccount%s in the resource", field)));
	}
}
