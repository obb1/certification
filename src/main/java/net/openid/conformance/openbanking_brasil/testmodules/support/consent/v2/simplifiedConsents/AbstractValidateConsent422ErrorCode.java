package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractValidateConsent422ErrorCode extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject response = env.getObject("consent_endpoint_response_full");
		JsonObject body = response.get("body_json").getAsJsonObject();
		JsonArray errors = body.getAsJsonArray("errors");
		if (errors == null) {
			throw error("Missing errors array in response");
		}
		if (errors.size() > 3){
			throw error("Maximum number of errors expected in response is 3", args("errors", errors));
		}
		errors.forEach(error -> {
			JsonObject errorObject = error.getAsJsonObject();
			String errorCode = OIDFJSON.getString(errorObject.get("code"));
			if(errorCode.equals(getErrorCode())) {
				logSuccess("Found expected error code", args("error code", errorCode));
			} else {
				throw error("Unexpected error code", args("expected error code", getErrorCode(), "obtained error code", errorCode));
			}
		});
		return env;
	}
	protected abstract String getErrorCode();
}
