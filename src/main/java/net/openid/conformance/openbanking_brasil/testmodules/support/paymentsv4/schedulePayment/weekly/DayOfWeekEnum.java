package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.weekly;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum DayOfWeekEnum {

	SEGUNDA_FEIRA,
	TERCA_FEIRA,
	QUARTA_FEIRA,
	QUINTA_FEIRA,
	SEXTA_FEIRA,
	SABADO,
	DOMINGO;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}

	public int getValue() {
		return ordinal() + 1;
	}
}
