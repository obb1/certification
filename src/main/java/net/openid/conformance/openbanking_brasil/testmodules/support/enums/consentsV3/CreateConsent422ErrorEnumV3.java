package net.openid.conformance.openbanking_brasil.testmodules.support.enums.consentsV3;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CreateConsent422ErrorEnumV3 {
	SEM_PERMISSOES_FUNCIONAIS_RESTANTES,
	INFORMACOES_PJ_NAO_INFORMADAS,
	PERMISSOES_PJ_INCORRETAS,
	PERMISSAO_PF_PJ_EM_CONJUNTO,
	COMBINACAO_PERMISSOES_INCORRETA;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
