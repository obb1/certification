package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setCreditors;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public abstract class AbstractEditRecurringPaymentsConsentBodyToSetCreditor extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(JsonElement::getAsJsonObject)
			.map(consent -> consent.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in consents payload"));

		JsonArray creditors = new JsonArray();
		addCreditorsToArray(creditors);
		data.add("creditors", creditors);

		additionalSteps(env, creditors);

		logSuccess("Updated consent request body to include " + logMessage(),
			args("data", data));

		return env;
	}

	protected JsonObject buildCreditor(String personType, String cnpj, String name) {
		return new JsonObjectBuilder()
			.addField("personType", personType)
			.addField("cpfCnpj", cnpj)
			.addField("name", name)
			.build();
	}

	protected abstract void addCreditorsToArray(JsonArray creditors);
	protected abstract String logMessage();
	protected void additionalSteps(Environment env, JsonArray creditors) {}
}
