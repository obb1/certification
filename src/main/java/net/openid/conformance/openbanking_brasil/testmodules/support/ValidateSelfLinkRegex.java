package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Optional;

public class ValidateSelfLinkRegex extends AbstractJsonAssertingCondition {

	public static final String RESPONSE_ENV_KEY = "endpoint_response";
	public static final String PAYMENTS_CONSENTS_BASE_REGEX = "^(https://)(.*?)(/open-banking/payments/v\\d+/consents/)%s(.*)";
	public static final String AUTOMATIC_PAYMENTS_CONSENTS_BASE_REGEX = "^(https://)(.*?)(/open-banking/automatic-payments/v\\d+/recurring-consents/)%s(.*)";
	public static final String PAYMENTS_BASE_REGEX = "^(https://)(.*?)(/open-banking/payments/v\\d+/pix/payments)%s(.*)";
	public static final String AUTOMATIC_PAYMENTS_BASE_REGEX = "^(https://)(.*?)(/open-banking/automatic-payments/v\\d+/pix/recurring-payments)%s(.*)";
	public static final String ENROLLMENTS_BASE_REGEX = "^(https://)(.*?)(/open-banking/enrollments/v\\d+/enrollments/)%s(.*)";
	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {

		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.orElse(env.getObject("resource_endpoint_response"))
			);

			String self = Optional.ofNullable(body.getAsJsonObject("links"))
				.map(links -> links.get("self"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("Could not extract links.self from response body", args("body", body)));

			String regex = "";
			if (env.getEffectiveKey(RESPONSE_ENV_KEY).contains("consent")) {
				String baseRegex = self.contains("automatic-payments") ? AUTOMATIC_PAYMENTS_CONSENTS_BASE_REGEX : PAYMENTS_CONSENTS_BASE_REGEX;
				String id = getConsentId(env, body, self);
				regex = String.format(baseRegex, id);
			} else if(self.contains("/enrollments/")){
				String baseRegex = ENROLLMENTS_BASE_REGEX;
				String id = extractEnrollmentIdFromBody(body);
				regex = String.format(baseRegex, id);
			} else {
				String baseRegex = self.contains("automatic-payments") ? AUTOMATIC_PAYMENTS_BASE_REGEX : PAYMENTS_BASE_REGEX;
				String id = extractPaymentRequestIdentifierFromBody(body, env);
				regex = String.format(baseRegex, id);
			}

			if (!self.matches(regex)) {
				throw error("Self link does not match the regex", args("regex", regex));
			}

			logSuccess("Self Link matches the regex", args("regex", regex));

		} catch (ParseException e) {
			throw error("Could not extract body from response");
		}
		return env;
	}

	private String extractPaymentRequestIdentifierFromBody(JsonObject body, Environment env) {
		JsonElement data = Optional.ofNullable(body.get("data"))
			.orElseThrow(() -> error("Could not extract data from payment body", args("body", body)));

		return data.isJsonArray() ?
			extractPaymentRequestIdentifierWhenArray(data.getAsJsonArray(), env)
			: extractPaymentRequestIdentifierWhenObject(data.getAsJsonObject());
	}

	private String extractPaymentRequestIdentifierWhenArray(JsonArray dataArray, Environment env) {
		if (dataArray.isEmpty()) {
			throw error("Data array is empty", args("data", dataArray));
		}

		JsonObject dataObject = dataArray.get(0).getAsJsonObject();
		String resourceUrl = env.getString("protected_resource_url");

		boolean urlQueriesByConsent = StringUtils.contains(resourceUrl, "consentId=") || StringUtils.contains(resourceUrl, "recurringConsentId=");
		if(!urlQueriesByConsent) {
			return extractPaymentRequestIdentifierWhenObject(dataObject);
		}

		// If the request queried by consent ID, the self link must also query by consent ID.
		String consentIdFieldName = dataObject.has("recurringConsentId") ? "recurringConsentId" : "consentId";
		String consentId = Optional.ofNullable(dataObject.get(consentIdFieldName))
			.map(OIDFJSON::getString)
			.orElseThrow(
				() -> error(
					String.format("Could not find %s in the first payment JSON object in the data array", consentIdFieldName),
					args("data", dataArray)
				)
			);
		return String.format("\\?%s=%s", consentIdFieldName, consentId);
	}

	private String extractPaymentRequestIdentifierWhenObject(JsonObject dataObject) {
		String paymentIdFieldName = dataObject.has("recurringPaymentId") ? "recurringPaymentId" : "paymentId";
		String paymentId = Optional.ofNullable(dataObject.get(paymentIdFieldName))
			.map(OIDFJSON::getString)
			.orElseThrow(
				() -> error(
					String.format("Could not find %s in the data JSON object", paymentIdFieldName),
					args("data", dataObject)
				)
			);
		return String.format("/%s", paymentId);
	}

	private String extractEnrollmentIdFromBody(JsonObject body) {
		JsonElement data = Optional.ofNullable(body.get("data"))
			.orElseThrow(() -> error("Could not extract data from body", args("body", body)));
		String enrollmentId = "";

		if (data.isJsonArray()) {
			JsonArray dataArray = data.getAsJsonArray();
			if (dataArray.isEmpty()) {
				throw error("Data array is empty", args("data", data));
			}
			enrollmentId = Optional.ofNullable(dataArray.get(0).getAsJsonObject().get("enrollmentId"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("Could not find enrollmentId in the first JSON object in the data array",
					args("data", dataArray)));

		} else {
			JsonObject dataObject = data.getAsJsonObject();
			enrollmentId = Optional.ofNullable(dataObject.get("enrollmentId"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("Could not find enrollmentId in the data JSON object",
					args("data", dataObject)));
		}
		return enrollmentId;
	}

	private String getConsentId(Environment env, JsonObject body, String selfLink) {
		String consentIdPath = selfLink.contains("automatic-payments") ? "$.data.recurringConsentId" : "$.data.consentId";
		String id;
		try {
			id = OIDFJSON.getString(JsonPath.parse(body).read(consentIdPath));
		} catch (PathNotFoundException e) {
			id = env.getString("consent_id");
		}

		if(StringUtils.isBlank(id)) {
			throw error("Could not find the consent ID inside consent response neither in the environment");
		}

		return id;
	}

}

