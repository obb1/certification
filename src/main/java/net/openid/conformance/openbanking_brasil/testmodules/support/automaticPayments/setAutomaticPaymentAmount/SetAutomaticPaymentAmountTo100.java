package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

public class SetAutomaticPaymentAmountTo100 extends AbstractSetAutomaticPaymentAmount {

	@Override
	protected String automaticPaymentAmount() {
		return "100.00";
	}
}
