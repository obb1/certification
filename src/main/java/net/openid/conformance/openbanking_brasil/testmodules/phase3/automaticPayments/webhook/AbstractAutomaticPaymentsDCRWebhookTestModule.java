package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddClientCredentialsGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddImplicitGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRedirectUriToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddRefreshTokenGrantTypeToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTlsClientAuthSubjectDnToDynamicRegistrationRequest;
import net.openid.conformance.condition.client.AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateEmptyDynamicRegistrationRequest;
import net.openid.conformance.condition.client.ExtractClientNameFromStoredConfig;
import net.openid.conformance.condition.client.ExtractJWKSDirectFromClientConfiguration;
import net.openid.conformance.condition.client.ExtractMTLSCertificatesFromConfiguration;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromOBResourceConfiguration;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromResourceConfiguration;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.condition.client.SetResponseTypeCodeIdTokenInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.SetResponseTypeCodeInDynamicRegistrationRequest;
import net.openid.conformance.condition.client.StoreOriginalClientConfiguration;
import net.openid.conformance.condition.client.ValidateMTLSCertificatesHeader;
import net.openid.conformance.condition.common.CheckDistinctKeyIdValueInClientJWKs;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalBrazilDCR;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ChuckWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateSelfLinkRegex;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddAutomaticPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringPaymentResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRiskSignalsManualObjectToPixPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.MoveStartDateTimeFromSweepingToRoot;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo300;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.validateVersionHeader.ValidateVersionHeader2d0d0;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.AddScopeToClientConfigurationFromRecurringConsentUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.ExtractRecurringPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringConsentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.webhook.validateWebhooksReceived.ValidateRecurringPaymentWebhooks;
import net.openid.conformance.openbanking_brasil.testmodules.support.deprecated.AddOpenIdScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.CallEnrollmentsEndpointWithBearerToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.paymentCheckPollStatus.CheckPaymentPollStatusRcvdOrAccpOrAcpdV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.warningMessages.TestTimedOut;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AddWebhookUrisToDynamicRegistrationRequest;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureSoftwareStatementContainsWebhook;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.WaitForSpecifiedTimeFromConfigInSeconds;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public abstract class AbstractAutomaticPaymentsDCRWebhookTestModule extends AbstractFAPI1AdvancedFinalBrazilDCR {

	protected abstract AbstractCreateRecurringConfigurationObject consentBodyCondition();
	protected abstract ConditionSequence getUrlsFromAuthServerCondition();
	protected abstract boolean isNewerVersion();
	protected abstract Class<? extends Condition> setPaymentConsentWebhookCreator();
	protected abstract Class<? extends Condition> setPaymentWebhookCreator();
	protected abstract Class<? extends Condition> setGetConsentValidator();
	protected abstract Class<? extends Condition> setPostConsentValidator();
	protected abstract Class<? extends Condition> setGetPaymentValidator();
	protected abstract Class<? extends Condition> setPostPaymentValidator();

	@Override
	public Object handleHttpMtls(String path, HttpServletRequest req, HttpServletResponse res, HttpSession session, JsonObject requestParts) {
		Boolean expectingRecurringPaymentWebhook = env.getBoolean("recurring_payment_webhook_received");
		Boolean expectingRecurringPaymentConsentWebhook = env.getBoolean("recurring_payment_consent_webhook_received");

		String consentsPattern = "^(open-banking/webhook/v\\d+/automatic-payments/v\\d+/recurring-consents/)(urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%/?#]+)$";
		boolean matchesConsent = path.matches(consentsPattern);

		String paymentsPattern = "^(open-banking/webhook/v\\d+/automatic-payments/v\\d+/pix/recurring-payments/)([a-zA-Z0-9][a-zA-Z0-9\\-]{0,99})$";
		boolean matchesPayment = path.matches(paymentsPattern);

		if (expectingRecurringPaymentConsentWebhook != null && matchesConsent && expectingRecurringPaymentConsentWebhook) {
			addWebhookToObjectInEnv(env, "recurring-consent", "webhooks_received_recurring_consent", path, requestParts);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else if (expectingRecurringPaymentWebhook != null && matchesPayment && expectingRecurringPaymentWebhook) {
			addWebhookToObjectInEnv(env, "recurring-payment", "webhooks_received_recurring_payment", path, requestParts);
			return new ResponseEntity<Object>("", HttpStatus.ACCEPTED);
		} else {
			throw new TestFailureException(getId(), "Got a webhook response we weren't expecting");
		}
	}

	public void addWebhookToObjectInEnv(Environment env, String endpoint, String key, String path, JsonObject requestParts) {
		JsonObject webhooksReceived = env.getObject(key);
		JsonArray webhooks = webhooksReceived.get("webhooks").getAsJsonArray();
		requestParts.addProperty("time_received", Instant.now().toString());
		requestParts.addProperty("type", endpoint);
		requestParts.addProperty("path", path);
		webhooks.add(requestParts);
		env.putObject(key, webhooksReceived);
	}

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddRecurringPaymentResourceUrlToConfig.class);
		callAndStopOnFailure(consentBodyCondition().getClass());
		callAndStopOnFailure(AddRecurringPaymentConsentRequestBodyToConfig.class);
		keepOldStartDateTime();
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);

		callAndStopOnFailure(ExtractTLSTestValuesFromResourceConfiguration.class);
		callAndContinueOnFailure(ExtractTLSTestValuesFromOBResourceConfiguration.class, Condition.ConditionResult.INFO);
	}

	protected void keepOldStartDateTime() {
		if (!isNewerVersion()) {
			callAndStopOnFailure(MoveStartDateTimeFromSweepingToRoot.class);
		}
	}

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(getUrlsFromAuthServerCondition()));
		callAndStopOnFailure(AddScopeToClientConfigurationFromRecurringConsentUrl.class);
		doDcr();
		callAndStopOnFailure(AddOpenIdScope.class);
		eventLog.startBlock("Waiting for webhook to be synced");
		callAndStopOnFailure(WaitForSpecifiedTimeFromConfigInSeconds.class);
		eventLog.endBlock();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		initialiseWebhookObject("recurring_consent");
		initialiseWebhookObject("recurring_payment");
		callAndStopOnFailure(AddAutomaticPaymentToTheResource.class);
		callAndStopOnFailure(setAutomaticPaymentAmount().getClass());
		callAndStopOnFailure(AddRiskSignalsManualObjectToPixPaymentBody.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class);
	}

	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountTo300();
	}

	protected void initialiseWebhookObject(String type){
		JsonObject webhooksReceived = new JsonObject();
		JsonArray webhooks = new JsonArray();
		webhooksReceived.add("webhooks", webhooks);
		env.putObject("webhooks_received_" + type, webhooksReceived);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps(
			addTokenEndpointClientAuthentication,
			false
		);
	}

	@Override
	protected void performPreAuthorizationSteps() {
		enablePaymentConsentWebhook();
		createConsentWebhook();
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(SaveAccessToken.class);
		validateConsentResponse(setPostConsentValidator());
		validateVersionHeader();
	}

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckStatus("AUTHORISED", new EnsurePaymentConsentStatusWasAuthorised());
		validateGetConsentResponse();
		super.performPostAuthorizationFlow();
	}

	@Override
	protected void requestProtectedResource() {
		enablePaymentWebhook();
		createPaymentWebhook();
		eventLog.startBlock(currentClientString() + "Call pix/payments endpoint");
		call(getPixPaymentSequence());
		eventLog.endBlock();
		eventLog.startBlock(currentClientString() + "Validate response");
		validateResponse();
		eventLog.endBlock();
		finalStepsRequestProtectedResource();
	}

	protected void finalStepsRequestProtectedResource() {
		waitForWebhookResponse();
		validatePaymentConsentWebhooks();
		validatePaymentWebhooks();
		checkFinalConsentStatus();
	}

	protected void checkFinalConsentStatus() {
		fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
	}

	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class));
	}

	protected void fetchConsentToCheckStatus(String status, AbstractEnsurePaymentConsentStatusWas statusCondition) {
		runInBlock(String.format("Checking the created consent - Expecting %s status", status), () -> {
			callAndStopOnFailure(AddJWTAcceptHeader.class);
			callAndStopOnFailure(ExpectJWTResponse.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(statusCondition.getClass(), Condition.ConditionResult.WARNING);
		});
	}

	protected void enablePaymentConsentWebhook(){
		env.putString("time_of_recurring_consent_request", Instant.now().toString());
		env.putBoolean("recurring_payment_consent_webhook_received", true);
	}

	protected void enablePaymentWebhook(){
		env.putString("time_of_recurring_payment_request", Instant.now().toString());
		env.putBoolean("recurring_payment_webhook_received", true);
	}

	public void createConsentWebhook(){
		callAndStopOnFailure(setPaymentConsentWebhookCreator());
		exposeEnvString("webhook_uri_consent_id");
	}

	public void createPaymentWebhook() {
		callAndStopOnFailure(setPaymentWebhookCreator());
		exposeEnvString("webhook_uri_payment_id");
	}

	protected void validateConsentResponse(Class<? extends Condition> consentResponseValidation) {
		callAndStopOnFailure(consentResponseValidation, Condition.ConditionResult.FAILURE);
	}

	protected ConditionSequence getPaymentValidationSequence(){
		ConditionSequence sequence = sequenceOf(
			condition(setGetPaymentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
		if (isNewerVersion()) {
			sequence.insertAfter(setGetPaymentValidator(), condition(ValidateVersionHeader2d0d0.class));
		}
		return sequence;
	}

	protected ConditionSequence postPaymentValidationSequence() {
		ConditionSequence sequence = sequenceOf(
			condition(setPostPaymentValidator())
				.dontStopOnFailure()
				.onFail(Condition.ConditionResult.FAILURE)
		);
		if (isNewerVersion()) {
			sequence.insertAfter(setPostPaymentValidator(), condition(ValidateVersionHeader2d0d0.class));
		}
		return sequence;
	}

	protected void waitForWebhookResponse() {
		long delaySeconds = 5;
		eventLog.startBlock(currentClientString() + "The test will now wait for the webhook to be received");
		for (int attempts = 0; attempts < 20; attempts++) {
			eventLog.log(getId(), "Waiting for webhook to be received at with an interval of " + delaySeconds + " seconds");
			setStatus(Status.WAITING);
			try {
				Thread.sleep(delaySeconds * 1000);
			} catch (InterruptedException e) {
				throw new TestFailureException(getId(), "Thread.sleep threw exception: " + e.getMessage());
			}
			setStatus(Status.RUNNING);

		}
		eventLog.endBlock();
	}

	protected void validatePaymentConsentWebhooks(){
		env.putBoolean("recurring_payment_consent_webhook_received", false);
		callAndStopOnFailure(ValidateRecurringConsentWebhooks.class);
	}

	protected void validateGetConsentResponse() {
		runInBlock("Validate GET Consent Response", () -> {
			validateConsentResponse(setGetConsentValidator());
			validateVersionHeader();
		});
	}

	protected void validatePaymentWebhooks(){
		env.putBoolean("recurring_payment_webhook_received", false);
		callAndStopOnFailure(ValidateRecurringPaymentWebhooks.class);
	}

	protected void doDcr(){
		ClientAuthType clientAuthType = getVariant(ClientAuthType.class);
		String clientAuth = clientAuthType.toString();
		if (clientAuthType == ClientAuthType.MTLS) {
			// the FAPI auth type variant doesn't use the name required for the registration request as per
			// https://datatracker.ietf.org/doc/html/rfc8705#section-2.1.1
			clientAuth = "tls_client_auth";
		}
		env.putString("client_auth_type", clientAuth);

		callAndContinueOnFailure(ValidateMTLSCertificatesHeader.class, Condition.ConditionResult.WARNING);
		callAndContinueOnFailure(ExtractMTLSCertificatesFromConfiguration.class, Condition.ConditionResult.FAILURE);

		// normally our DCR tests create a key on the fly to use, but in this case the key has to be registered
		// manually with the central directory so we must use user supplied keys
		callAndStopOnFailure(ExtractJWKSDirectFromClientConfiguration.class);

		callAndContinueOnFailure(CheckDistinctKeyIdValueInClientJWKs.class, Condition.ConditionResult.FAILURE, "RFC7517-4.5");

		getSsa();

		eventLog.startBlock("Perform Dynamic Client Registration");

		callAndStopOnFailure(StoreOriginalClientConfiguration.class);
		callAndStopOnFailure(ExtractClientNameFromStoredConfig.class);

		setupJwksUri();

		// create basic dynamic registration request
		callAndStopOnFailure(CreateEmptyDynamicRegistrationRequest.class);

		callAndStopOnFailure(AddAuthorizationCodeGrantTypeToDynamicRegistrationRequest.class);
		if (!jarm.isTrue()) {
			// implicit is only required when id_token is returned in frontchannel
			callAndStopOnFailure(AddImplicitGrantTypeToDynamicRegistrationRequest.class);
		}
		callAndStopOnFailure(AddRefreshTokenGrantTypeToDynamicRegistrationRequest.class);
		callAndStopOnFailure(AddClientCredentialsGrantTypeToDynamicRegistrationRequest.class);

		if (clientAuthType == ClientAuthType.MTLS) {
			callAndStopOnFailure(AddTlsClientAuthSubjectDnToDynamicRegistrationRequest.class);
		}

		addJwksToRequest();
		callAndStopOnFailure(AddTokenEndpointAuthMethodToDynamicRegistrationRequestFromEnvironment.class);
		if (jarm.isTrue()) {
			callAndStopOnFailure(SetResponseTypeCodeInDynamicRegistrationRequest.class);
		} else {
			callAndStopOnFailure(SetResponseTypeCodeIdTokenInDynamicRegistrationRequest.class);
		}
		validateSsa();
		callAndStopOnFailure(AddRedirectUriToDynamicRegistrationRequest.class);

		addSoftwareStatementToRegistrationRequest();

		addWebhookUrisToDynamicRegistrationRequest();

		callRegistrationEndpoint();
	}

	@Override
	protected void getSsa() {
		super.getSsa();
		validateWebhookOnSoftwareStatement();
	}

	protected void validateWebhookOnSoftwareStatement(){
		callAndStopOnFailure(EnsureSoftwareStatementContainsWebhook.class);
	}

	protected void addWebhookUrisToDynamicRegistrationRequest() {
		callAndStopOnFailure(AddWebhookUrisToDynamicRegistrationRequest.class);
	}

	protected void runInBlock(String blockText, Runnable actor) {
		eventLog.startBlock(blockText);
		actor.run();
		eventLog.endBlock();
	}

	protected void validateResponse() {
		callAndStopOnFailure(ExtractRecurringPaymentId.class);
		call(postPaymentValidationSequence());

		repeatSequence(this::getRepeatSequence)
			.untilTrue("payment_proxy_check_for_reject")
			.trailingPause(30)
			.times(5)
			.validationSequence(this::getPaymentValidationSequence)
			.onTimeout(sequenceOf(
				condition(TestTimedOut.class),
				condition(ChuckWarning.class)))
			.run();

		validateFinalState();

	}

	protected ConditionSequenceRepeater repeatSequence(Supplier<ConditionSequence> conditionSequenceSupplier) {
		return new ConditionSequenceRepeater(env, getId(), eventLog, testInfo, executionManager, conditionSequenceSupplier);
	}

	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
	}

	protected ConditionSequence getRepeatSequence() {
		return new ValidateSelfEndpoint()
			.replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			))
			.skip(SaveOldValues.class, "Not saving old values")
			.skip(LoadOldValues.class, "Not loading old values")
			.insertAfter(EnsureResourceResponseCodeWas200.class, condition(getPaymentPollStatusCondition()));
	}

	protected Class<? extends Condition> getPaymentPollStatusCondition() {
		return CheckPaymentPollStatusRcvdOrAccpOrAcpdV2.class;
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		forceGetCallToUseClientCredentialsToken(builder);

		super.call(builder);

		if (CallProtectedResource.class.equals(builder.getConditionClass()) ||
			CallEnrollmentsEndpointWithBearerToken.class.equals(builder.getConditionClass())) {
			validateSelfLink("resource_endpoint_response_full");
		}

		if (FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
			FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass()) ||
			FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders.class.equals(builder.getConditionClass())) {
			validateSelfLink("consent_endpoint_response_full");
		}
	}

	protected void useClientCredentialsAccessToken(){
		env.mapKey("access_token", "old_access_token");
	}

	protected void userAuthorisationCodeAccessToken(){
		env.unmapKey("access_token");
	}

	protected void forceGetCallToUseClientCredentialsToken(ConditionCallBuilder builder) {
		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			String resourceMethod = OIDFJSON.getString(Optional.ofNullable(
				env.getElementFromObject("resource", "resourceMethod")).orElse(new JsonPrimitive("")));
			if (Objects.equals(resourceMethod, "GET") && env.containsObject("old_access_token")) {
				eventLog.log("Load client_credential access token", env.getObject("old_access_token"));
				handleAccessToken();
			}
		}
	}

	protected void handleAccessToken() {
		callAndStopOnFailure(LoadOldAccessToken.class);
		callAndStopOnFailure(SaveAccessToken.class);
	}

	private boolean isEndpointCallSuccessful(int status) {
		return status == org.apache.http.HttpStatus.SC_CREATED || status == org.apache.http.HttpStatus.SC_OK;
	}

	protected void validateSelfLink(String responseFull) {

		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
			.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));

		String expectsFailure = Optional.ofNullable(env.getString("expects_failure")).orElse("false");

		if (!recursion.equals("true") && isEndpointCallSuccessful(status) && !expectsFailure.equals("true")) {

			env.putString("recursion", "true");
			call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
			env.mapKey("resource_endpoint_response_full", responseFull);

			ConditionSequence sequence = new ValidateSelfEndpoint().replace(CallProtectedResource.class, sequenceOf(
				condition(AddJWTAcceptHeader.class),
				condition(CallProtectedResource.class)
			));

			if (responseFull.contains("consent")) {
				sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));

				env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			} else {
				env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			}
			callAndContinueOnFailure(ValidateSelfLinkRegex.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY);

			call(sequence);
			env.unmapKey("resource_endpoint_response_full");
			env.putString("recursion", "false");
		}
		env.removeNativeValue("expects_failure");
	}

	protected void validateVersionHeader() {
		if (isNewerVersion()) {
			callAndContinueOnFailure(ValidateVersionHeader2d0d0.class, Condition.ConditionResult.FAILURE);
		}
	}
}
