package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.customerRegistrationData.v2;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.jayway.jsonpath.JsonPath;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.JsonHelper;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.testmodule.OIDFJSON;
import org.springframework.http.HttpMethod;

import java.util.Optional;

@ApiName("Natural Person Identity V2.2.0")
public class PersonalIdentificationResponseOASValidatorV2 extends OpenAPIJsonSchemaValidator {
	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/customers/swagger-customers-2.2.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/personal/identifications";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		for(JsonElement dataElement : body.getAsJsonArray("data")) {
			JsonObject data = dataElement.getAsJsonObject();
			assertMaritalStatusConstraint(data);
			assertCpfAndPassportConstraint(data);
			assertPhonesConstraint(data);
		}
	}

	protected void assertMaritalStatusConstraint(JsonObject data) {
		JsonElement maritalStatusCode = Optional.ofNullable(data.get("maritalStatusCode")).orElse(new JsonPrimitive(""));
		if("OUTRO".equals(OIDFJSON.getString(maritalStatusCode)) && data.get("maritalStatusAdditionalInfo") == null) {
			throw error("data.maritalStatusAdditionalInfo is mandatory when data.maritalStatusCode is \"OUTRO\"");
		}
	}

	protected void assertCpfAndPassportConstraint(JsonObject data) {
		boolean cpfIsPresent = JsonHelper.ifExists(data, "$.documents.cpfNumber");
		boolean passportIsPresent = JsonHelper.ifExists(data, "$.documents.passport");
		if(!cpfIsPresent && !passportIsPresent) {
			throw error("data.documents.cpfNumber is mandatory when data.documents.passport is not informed and vice versa");
		}
	}

	protected void assertPhonesConstraint(JsonObject data) {
		JsonArray phones = JsonPath.read(data, "$.contacts.phones[*]");
		for(JsonElement phoneElement : phones) {
			assertField1IsRequiredWhenField2HasValue2(
				phoneElement.getAsJsonObject(),
				"additionalInfo",
				"type",
				"OUTRO"
			);
		}
	}
}
