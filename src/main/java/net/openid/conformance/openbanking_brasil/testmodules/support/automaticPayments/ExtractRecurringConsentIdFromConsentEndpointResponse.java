package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ExtractRecurringConsentIdFromConsentEndpointResponse extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	@PostEnvironment(strings = "consent_id")
	public Environment evaluate(Environment env) {
		try {
			JsonObject body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);

			String recurringConsentId = OIDFJSON.getString(
				Optional.ofNullable(body.getAsJsonObject("data"))
					.map(data -> data.get("recurringConsentId"))
					.orElseThrow(() -> error("Could not find data.recurringConsentId"))
			);

			env.putString("consent_id", recurringConsentId);
			logSuccess("Extracted the recurring consent id", args("recurringConsentId", recurringConsentId));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}

		return env;
	}

}
