package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectQRESCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectQRCodeWithWrongEmailAddressProxyIntoConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRESCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasQRCodeInvalidoV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;


public abstract class AbstractPaymentsApiQRESMismatchedProxyTestModule extends AbstractPaymentConsentUnhappyPathTestModule {


	@Override
	protected void onAuthorizationCallbackResponse(){
		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);

		callAndContinueOnFailure(CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint.class, Condition.ConditionResult.WARNING, "OIDCC-3.1.2.6");

		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OAuth2-iss-2");

		fetchConsentToCheckStatus("REJECTED",new EnsureConsentStatusWasRejected());
		callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasQRCodeInvalidoV3.class, Condition.ConditionResult.FAILURE);

		fireTestFinished();
	}

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(SelectQRESCodeLocalInstrument.class);
        callAndStopOnFailure(SelectQRESCodePixLocalInstrument.class);
        callAndStopOnFailure(InjectQRCodeWithWrongEmailAddressProxyIntoConfig.class);
    }

    @Override
    protected void validatePaymentRejectionReasonCode() {
		// Test won't be reaching payment initiation.
    }

    @Override
    protected void validate422ErrorResponseCode() {
		// Test won't be reaching payment initiation.
    }

    @Override
    protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		// Test won't be reaching payment initiation.
		return null;
    }

}
