package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly;

public class Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture extends AbstractScheduleMonthlyPayment {

	@Override
	protected int getDayOfMonth() {
		return 31;
	}

	@Override
	protected int getNumberOfDaysToAddToCurrentDate() {
		return 1;
	}

	@Override
	protected int getQuantity() {
		return 5;
	}
}
