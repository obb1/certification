package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.security.SecureRandom;

/**
 * {@link GenerateFidoClientData} condition adds client_data object to the env
 **/
public class EditClientDataToSetRandomChallenge extends AbstractCondition {

	private static final String ALPHANUMERIC_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	@Override
	@PreEnvironment(required = "client_data")
	public Environment evaluate(Environment env) {
		JsonObject clientData = env.getObject("client_data");
		String currentChallenge = OIDFJSON.getString(clientData.get("challenge"));
		String newChallenge = generateRandomString(currentChallenge.length());

		clientData.addProperty("challenge", newChallenge);
		logSuccess("The client data has been edited to include a random string challenge",
			args("client data", clientData));
		return env;
	}

	protected static String generateRandomString(int length) {
		SecureRandom random = new SecureRandom();
		StringBuilder sb = new StringBuilder(length);

		for (int i = 0; i < length; i++) {
			int idx = random.nextInt(ALPHANUMERIC_CHARACTERS.length());
			char ch = ALPHANUMERIC_CHARACTERS.charAt(idx);
			sb.append(ch);
		}

		return sb.toString();
	}
}
