package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonUtils;

public class SetProtectedResourceUrlToNextOrStopPolling extends AbstractCondition {

    private static final Gson GSON = JsonUtils.createBigDecimalAwareGson();

    @Override
    @PreEnvironment(required = "resource_endpoint_response_full")
    @PostEnvironment(strings = "protected_resource_url")
    public Environment evaluate(Environment env) {
        String bodyJsonString = env.getString("resource_endpoint_response_full", "body");
        JsonObject body;
        try {
            body = GSON.fromJson(bodyJsonString, JsonObject.class);
        } catch (JsonSyntaxException e) {
            throw error("Body is not json", args("body", bodyJsonString));
        }

        JsonObject links = body.getAsJsonObject("links");
        if (!JsonHelper.ifExists(body, "$.links.next")) {
            env.putBoolean("is_last_page", true);
            logSuccess("The 'next' link has not been found, so it is the last page and we are not polling anymore");
        } else {
            env.putBoolean("is_last_page", false);
            env.putString("protected_resource_url", OIDFJSON.getString(links.get("next")));
            logSuccess("Resource url has been set to 'next'", args("url", env.getString("protected_resource_url")));
        }

        return env;
    }
}
