package net.openid.conformance.openbanking_brasil.testmodules.v3n;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRevokedByUser;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetBusinessIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPersonalIdentificationsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v2.GetConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v2.PostConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.consent.v2.OpenBankingBrazilPreAuthorizationConsentApiV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.RefreshTokenRequestExpectingErrorSteps;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.ClientAuthType;

import java.util.Objects;

public abstract class AbstractConsentsApiDeleteTestModule extends AbstractPhase2TestModule {


	@Override
	protected abstract Class<? extends Condition> getPostConsentValidator();

	protected abstract Class<? extends Condition> setPostConsentValidator();


	protected ClientAuthType clientAuthType;


	@Override
	protected void validateClientConfiguration() {
		super.validateClientConfiguration();
		callAndStopOnFailure(SetScopeInClientConfigurationToOpenId.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS, OPFScopesEnum.CUSTOMERS, OPFScopesEnum.OPEN_ID).build();
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		String productType = env.getString("config", "consent.productType");
		Condition getIdentification;

		if(Objects.equals(productType, "personal")){
			getIdentification = new GetPersonalIdentificationsV2Endpoint();
		}
		else if(Objects.equals(productType, "business")){
			getIdentification = new GetBusinessIdentificationsV2Endpoint();
		}
		else{
			throw new TestFailureException(getId(),"productType is not valid, it must be either personal or business");
		}

		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(getIdentification.getClass())
		);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		clientAuthType = getVariant(ClientAuthType.class);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		env.putString("proceed_with_test", "true");
		ConditionSequence preauthSteps = new OpenBankingBrazilPreAuthorizationConsentApiV2(addTokenEndpointClientAuthentication, false)
			.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
			.insertBefore(AddFAPIAuthDateToResourceEndpointRequest.class,condition(CreateRandomFAPIInteractionId.class))
			.replace(PostConsentOASValidatorV2.class, condition(setPostConsentValidator()))
			.replace(GetConsentOASValidatorV2.class, condition(getPostConsentValidator()))
			.skip(FAPIBrazilAddExpirationToConsentRequest.class, "Not adding ExpirationDateTime to request");

		return preauthSteps;
	}

	@Override
	protected void requestProtectedResource() {

		eventLog.startBlock("Try calling protected resource after user authentication");
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);
		callAndStopOnFailure(SaveProtectedResourceAccessToken.class);
		callAndStopOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		eventLog.startBlock("Deleting consent");
		callAndContinueOnFailure(LoadConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(EnsureConsentResponseCodeWas204.class);

		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(LoadConsentsAccessToken.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);

		callAndStopOnFailure(getPostConsentValidator(), Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(EnsureConsentResponseCodeWas200.class);
		callAndContinueOnFailure(EnsureConsentRevokedByUser.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(LoadProtectedResourceAccessToken.class);

		eventLog.startBlock("Try calling protected resource after consent is deleted");
		callAndStopOnFailure(CallProtectedResource.class);
		callAndContinueOnFailure(EnsureResourceResponseCodeWas401.class, Condition.ConditionResult.FAILURE);

		eventLog.startBlock("Trying issuing a refresh token");
		callAndStopOnFailure(ExtractRefreshTokenFromTokenResponse.class);
		call(new RefreshTokenRequestExpectingErrorSteps(isSecondClient(), addTokenEndpointClientAuthentication));
	}

	@Override
	protected void validateResponse() {
	}

	@Override
	protected void configureClient() {
		callAndStopOnFailure(AddProductTypeToPhase2Config.class);
		super.configureClient();
	}

}
