package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddAudAsPaymentConsentUriToRequestObject;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIatToRequestObject;
import net.openid.conformance.condition.client.AddIdempotencyKeyHeader;
import net.openid.conformance.condition.client.AddIssAsCertificateOuToRequestObject;
import net.openid.conformance.condition.client.AddJtiAsUuidToRequestObject;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateIdempotencyKey;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.EnsureHttpStatusCodeIs201;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.FAPIBrazilExtractClientMTLSCertificateSubject;
import net.openid.conformance.condition.client.FAPIBrazilGetKeystoreJwksUri;
import net.openid.conformance.condition.client.FAPIBrazilSignPaymentConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.support.InsertProceedWithTestString;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class SignedPaymentConsentSequence extends AbstractConditionSequence {

	@Override
	public void evaluate() {
		callAndStopOnFailure(InsertProceedWithTestString.class);
		callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
		callAndStopOnFailure(CreateIdempotencyKey.class);
		callAndStopOnFailure(AddIdempotencyKeyHeader.class);
		callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class);
		callAndStopOnFailure(CreateRandomFAPIInteractionId.class);
		callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class);
		callAndStopOnFailure(FAPIBrazilExtractClientMTLSCertificateSubject.class);
		call(exec().mapKey("request_object_claims", "consent_endpoint_request"));
		callAndStopOnFailure(AddAudAsPaymentConsentUriToRequestObject.class, "BrazilOB-6.1");
		callAndStopOnFailure(AddIssAsCertificateOuToRequestObject.class, "BrazilOB-6.1");
		callAndStopOnFailure(AddJtiAsUuidToRequestObject.class, "BrazilOB-6.1");
		callAndStopOnFailure(AddIatToRequestObject.class, "BrazilOB-6.1");
		call(exec().unmapKey("request_object_claims"));
		callAndStopOnFailure(FAPIBrazilSignPaymentConsentRequest.class);
		callAndStopOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class, Condition.ConditionResult.FAILURE);
		call(exec().mapKey("endpoint_response", "consent_endpoint_response_full"));
		call(exec().mapKey("endpoint_response_jwt", "consent_endpoint_response_jwt"));
		callAndContinueOnFailure(EnsureHttpStatusCodeIs201.class, Condition.ConditionResult.FAILURE);

		call(condition(EnsureContentTypeApplicationJwt.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1")
			.skipIfStringMissing("proceed_with_test"));

		call(condition(ExtractSignedJwtFromResourceResponse.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1")
			.skipIfStringMissing("proceed_with_test"));

		call(condition(FAPIBrazilValidateResourceResponseSigningAlg.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1")
			.skipIfStringMissing("proceed_with_test"));

		call(condition(FAPIBrazilValidateResourceResponseTyp.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1")
			.skipIfStringMissing("proceed_with_test"));

		call(condition(FAPIBrazilGetKeystoreJwksUri.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.skipIfStringMissing("proceed_with_test"));

		call(exec().mapKey("server", "org_server"));
		call(exec().mapKey("server_jwks", "org_server_jwks"));

		call(condition(FetchServerKeys.class)
			.skipIfStringMissing("proceed_with_test"));

		call(exec().unmapKey("server"));
		call(exec().unmapKey("server_jwks"));

		call(condition(ValidateResourceResponseSignature.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1")
			.skipIfStringMissing("proceed_with_test"));

		call(condition(ValidateResourceResponseJwtClaims.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1")
			.skipIfStringMissing("proceed_with_test"));

		callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE);

		call(exec().unmapKey("endpoint_response"));
		call(exec().unmapKey("endpoint_response_jwt"));
	}
}
