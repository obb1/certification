package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;

public abstract class AbstractInvestmentsApiTransactionDateTestModule extends AbstractInvestmentsApiTestModule {

    @Override
    protected void executeParticularTestSteps() {
        runInBlock("GET Investments Transactions endpoint without query parameters", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
            callAndStopOnFailure(CallProtectedResource.class);
        });
        runInBlock("Validating GET Investments Transactions response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(investmentsTransactionsValidator(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(checkAllDates(), Condition.ConditionResult.FAILURE);
        });

        getAndValidateTransactionsApi("D-360", "D+0",
            setFromTransactionTo360DaysAgo(), setToTransactionToToday());

        getAndValidateTransactionsApi("D-180", "D+0",
            setFromTransactionTo180DaysAgo(), setToTransactionToToday());

        getAndValidateTransactionsApi("D-360", "D-180",
            setFromTransactionTo360DaysAgo(), setToTransactionTo180DaysAgo());

        runInBlock("GET Investments Transactions endpoint with both query parameters set to save value", () -> {
            callAndStopOnFailure(setFromAndToTransactionToSavedTransaction());
            callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
            callAndStopOnFailure(CallProtectedResource.class);
        });
        runInBlock("Validating GET Investments Transactions response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(checkAllDates(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(checkTransactionRange(), Condition.ConditionResult.FAILURE);
        });
    }

    protected void getAndValidateTransactionsApi(String fromTransaction, String toTransaction,
                                     Class<? extends Condition> setFromTransaction,
                                     Class<? extends Condition> setToTransaction) {
        runInBlock(String.format("GET Investments Transactions endpoint with from%s=%s and to%s=%s",
            transactionParameterName(), fromTransaction, transactionParameterName(), toTransaction), () ->
        {
            callAndStopOnFailure(setFromTransaction);
            callAndStopOnFailure(setToTransaction);
            callAndStopOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl());
            callAndStopOnFailure(CallProtectedResource.class);
        });
        runInBlock("Validating GET Investments Transactions response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(investmentsTransactionsValidator(), Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(checkTransactionRange(), Condition.ConditionResult.FAILURE);
        });
    }

    protected abstract Class<? extends Condition> investmentsTransactionsValidator();
    protected abstract Class<? extends Condition> setFromTransactionTo180DaysAgo();
    protected abstract Class<? extends Condition> setToTransactionToToday();
    protected abstract Class<? extends Condition> setFromTransactionTo360DaysAgo();
    protected abstract Class<? extends Condition> setToTransactionTo180DaysAgo();
    protected abstract Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl();
    protected abstract Class<? extends Condition> setFromAndToTransactionToSavedTransaction();
    protected abstract Class<? extends Condition> checkTransactionRange();
    protected abstract Class<? extends Condition> checkAllDates();
    protected abstract String transactionParameterName();
}
