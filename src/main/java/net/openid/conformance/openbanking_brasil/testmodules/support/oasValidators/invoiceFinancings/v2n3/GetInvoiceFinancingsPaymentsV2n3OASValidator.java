package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.AbstractGetInvoiceFinancingsPaymentsOASValidator;


public class GetInvoiceFinancingsPaymentsV2n3OASValidator extends AbstractGetInvoiceFinancingsPaymentsOASValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/invoiceFinancings/invoice-financings-2.3.0.yml";
	}

}
