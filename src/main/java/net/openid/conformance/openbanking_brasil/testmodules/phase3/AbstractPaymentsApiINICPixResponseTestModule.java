package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.EnsureProxyPresentInConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddTransactionIdentification;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectINICCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectINICCodePixLocalInstrument;

public abstract class AbstractPaymentsApiINICPixResponseTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

    @Override
    protected void configureDictInfo() {
        callAndStopOnFailure(EnsureProxyPresentInConfig.class);
        callAndStopOnFailure(AddTransactionIdentification.class);
        callAndStopOnFailure(SelectINICCodeLocalInstrument.class);
        callAndStopOnFailure(SelectINICCodePixLocalInstrument.class);
    }
}
