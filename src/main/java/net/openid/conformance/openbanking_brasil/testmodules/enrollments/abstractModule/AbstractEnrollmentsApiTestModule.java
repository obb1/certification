package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CallTokenEndpoint;
import net.openid.conformance.condition.client.CheckForAccessTokenValue;
import net.openid.conformance.condition.client.CheckIfTokenEndpointResponseError;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.EnsureMatchingFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractExpiresInFromTokenEndpointResponse;
import net.openid.conformance.condition.client.FAPIBrazilCallPaymentConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.SetPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.condition.client.ValidateExpiresIn;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckIfLinksIsPresent;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateSelfLinkRegex;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.CallEnrollmentsEndpointWithBearerToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GetEnrollmentStatus;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.PreparePermissionsForEnrollmentsTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreatePatchEnrollmentsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateRiskSignalsRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.AbstractEnsureEnrollmentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAwaitingAccountHolderValidation;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAwaitingEnrollment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAwaitingRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.EnrollmentStatusEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareRejectionOrRevocationReasonForPatchRequest.PrepareRevocationReasonRevogadoManualmenteForPatchRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToFetchEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPatchEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.AddJWTAcceptHeaderRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;

import java.util.Objects;
import java.util.Optional;


public abstract class AbstractEnrollmentsApiTestModule extends AbstractOBBrasilFunctionalTestModule {

	/**
	 * This class makes the steps:
	 * - Call the POST enrollments endpoint
	 * - Expect a 201 response - Validate the response and check if the status is "AWAITING_RISK_SIGNALS"
	 * - Call the POST Risk Signals endpoint sending the appropriate signals
	 * - Expect a 204 response
	 * - Call the GET enrollments endpoint
	 * - Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION
	 * - Redirect the user to authorize the enrollment
	 * - Call the GET enrollments endpoint
	 * - Expect a 201 response - Validate the response and check if the status is "AWAITING_ENROLLMENT"
	 * If you need the rest of the enrollments flow, you might prefer using {@link AbstractCompleteEnrollmentsApiEnrollmentTestModule}
	 */

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(getConsentAndResourceEndpointSequence()));
		super.configureClient();
	}

    @Override
    protected void validateClientConfiguration() {
		callAndStopOnFailure(PreparePermissionsForEnrollmentsTest.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.PAYMENTS, OPFScopesEnum.NRP_CONSENTS, OPFScopesEnum.OPEN_ID).build();
        super.validateClientConfiguration();
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        callAndStopOnFailure(CreateEnrollmentsRequestBodyToRequestEntityClaims.class);
        callAndStopOnFailure(PrepareToPostEnrollments.class);
    }

    @Override
    protected void performPreAuthorizationSteps() {
        postAndValidateEnrollments();
		postAndValidateRiskSignals();
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasAwaitingAccountHolderValidation(),
			"AWAITING_ACCOUNT_HOLDER_VALIDATION");
    }

	protected void postAndValidateEnrollments() {
		call(createPostEnrollmentsSteps());
		callAndStopOnFailure(SaveAccessToken.class);
		runInBlock("Validate enrollments response", () -> {
			validateResourceResponse(postEnrollmentsValidator());
			callAndContinueOnFailure(EnsureEnrollmentStatusWasAwaitingRiskSignals.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void postAndValidateRiskSignals() {
		runInBlock("Post risk-signals - Expects 204", () -> {
			call(createPostRiskSignalsSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureMatchingFAPIInteractionId.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void getAndValidateEnrollmentsStatus(AbstractEnsureEnrollmentStatusWas ensureEnrollmentStatusWas, String statusName) {
		runInBlock("Get enrollment - Expects 200 " + statusName, () -> {
			fetchEnrollmentToCheckStatus(ensureEnrollmentStatusWas);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			validateResourceResponse(getEnrollmentsValidator());
		});
	}

    protected PostEnrollmentsSteps createPostEnrollmentsSteps() {
        return new PostEnrollmentsSteps(addTokenEndpointClientAuthentication);
    }

	protected PostEnrollmentsResourceSteps createPostRiskSignalsSteps() {
		return new PostEnrollmentsResourceSteps(new PrepareToPostRiskSignals(),
			CreateRiskSignalsRequestBodyToRequestEntityClaims.class,
			true);
	}

	protected void checkStatusAfterAuthorization() {
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasAwaitingEnrollment(), "AWAITING_ENROLLMENT");
	}

    @Override
    protected void performPostAuthorizationFlow() {
		checkStatusAfterAuthorization();

		userAuthorisationCodeAccessToken();
        eventLog.startBlock(currentClientString() + "Call token endpoint");
        createAuthorizationCodeRequest();
        requestAuthorizationCode();
        requestProtectedResource();
        onPostAuthorizationFlowComplete();
    }

    protected void fetchEnrollmentToCheckStatus(AbstractEnsureEnrollmentStatusWas ensureEnrollmentStatusWas) {
        callAndStopOnFailure(ExpectJWTResponse.class);
        callAndStopOnFailure(PrepareToFetchEnrollmentsRequest.class);
        callAndContinueOnFailure(CallEnrollmentsEndpointWithBearerToken.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(ensureEnrollmentStatusWas.getClass(), Condition.ConditionResult.FAILURE);
    }

    @Override
    protected void requestProtectedResource() {
        executeTestSteps();
    }

    @Override
    protected void validateResponse() {}

	protected void validateResourceResponse(Class<? extends Condition> validator) {
		callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
	}

    @Override
    protected void call(ConditionCallBuilder builder) {
        forceGetCallToUseClientCredentialsToken(builder);

        super.call(builder);

        if (CallProtectedResource.class.equals(builder.getConditionClass()) ||
            CallEnrollmentsEndpointWithBearerToken.class.equals(builder.getConditionClass())) {
            validateSelfLink("resource_endpoint_response_full");
        }

        if (FAPIBrazilCallPaymentConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
            FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass()) ||
            FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethodAnyHeaders.class.equals(builder.getConditionClass())) {
            validateSelfLink("consent_endpoint_response_full");
        }
    }

	protected void useClientCredentialsAccessToken() {
		env.mapKey("access_token", "old_access_token");
	}

	protected void userAuthorisationCodeAccessToken() {
		env.unmapKey("access_token");
	}

	protected void createNewClientCredentialsToken() {
		runInBlock("Create new client_credentials token to continue test", () -> {
			callAndStopOnFailure(CreateTokenEndpointRequestForClientCredentialsGrant.class);
			callAndStopOnFailure(SetPaymentsScopeOnTokenEndpointRequest.class);
			call(sequence(addTokenEndpointClientAuthentication));
			callAndStopOnFailure(CallTokenEndpoint.class);
			callAndStopOnFailure(CheckIfTokenEndpointResponseError.class);
			callAndStopOnFailure(CheckForAccessTokenValue.class);
			callAndStopOnFailure(ExtractAccessTokenFromTokenResponse.class);
			callAndContinueOnFailure(ExtractExpiresInFromTokenEndpointResponse.class, Condition.ConditionResult.FAILURE, "RFC6749-4.4.3", "RFC6749-5.1");
			call(condition(ValidateExpiresIn.class)
				.skipIfObjectMissing("expires_in")
				.onSkip(Condition.ConditionResult.INFO)
				.requirements("RFC6749-5.1")
				.onFail(Condition.ConditionResult.FAILURE)
				.dontStopOnFailure());
			callAndStopOnFailure(SaveAccessToken.class);
		});
	}

    protected void forceGetCallToUseClientCredentialsToken(ConditionCallBuilder builder) {
		if (CallProtectedResource.class.isAssignableFrom(builder.getConditionClass())) {

			String resourceMethod = "";
			if(CallProtectedResource.class.equals(builder.getConditionClass())){
				resourceMethod = OIDFJSON.getString(Optional.ofNullable(
					env.getElementFromObject("resource", "resourceMethod")).orElse(new JsonPrimitive("")));
			}
			if(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class.equals(builder.getConditionClass()) ||
				CallEnrollmentsEndpointWithBearerToken.class.equals(builder.getConditionClass())) {
				resourceMethod = Optional.ofNullable(env.getString("http_method")).orElse("");
			}

			if (Objects.equals(resourceMethod, "GET") && env.containsObject("old_access_token")) {
				eventLog.log("Load client_credential access token", env.getObject("old_access_token"));
				useClientCredentialsAccessToken();
			}
		}
    }

    private boolean isEndpointCallSuccessful(int status) {
        return status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK;
    }

    protected void validateSelfLink(String responseFull) {

        String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");

        int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
            .orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));

		boolean expectsSelfLink = Optional.ofNullable(env.getBoolean("expects_self_link")).orElse(true);
        String expectsFailure = Optional.ofNullable(env.getString("expects_failure")).orElse("false");

        if (!recursion.equals("true") && isEndpointCallSuccessful(status) && !expectsFailure.equals("true") && expectsSelfLink) {

            env.putString("recursion", "true");
            call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
            env.mapKey("resource_endpoint_response_full", responseFull);

			callAndStopOnFailure(CheckIfLinksIsPresent.class);
			if (!env.getBoolean("is_links_present")) {
				return;
			}

            ConditionSequence sequence = new ValidateSelfEndpoint().replace(CallProtectedResource.class, sequenceOf(
                condition(AddJWTAcceptHeaderRequest.class),
                condition(CallProtectedResource.class)
            ));

            if (responseFull.contains("consent")) {
                sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));

                env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
            } else {
                env.mapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
            }
            callAndContinueOnFailure(ValidateSelfLinkRegex.class, Condition.ConditionResult.FAILURE);
            env.unmapKey(ValidateSelfLinkRegex.RESPONSE_ENV_KEY);

            call(sequence);
            env.unmapKey("resource_endpoint_response_full");
            env.putString("recursion", "false");
        }
        env.removeNativeValue("expects_failure");
		env.removeNativeValue("expects_self_link");
    }

	@Override
	public void cleanup() {
		runInBlock("Patch enrollments - Expects 204 - Skiping if enrollment not AUTHORISED", () -> {
			callAndStopOnFailure(ExpectJWTResponse.class);
			callAndStopOnFailure(PrepareToFetchEnrollmentsRequest.class);
			callAndContinueOnFailure(CallEnrollmentsEndpointWithBearerToken.class, Condition.ConditionResult.INFO);
			callAndContinueOnFailure(GetEnrollmentStatus.class, Condition.ConditionResult.INFO);
			if(env.getString("enrollment_status") != null && env.getString("enrollment_status").equals(EnrollmentStatusEnum.AUTHORISED.toString())){
				useClientCredentialsAccessToken();
				callAndStopOnFailure(PrepareRevocationReasonRevogadoManualmenteForPatchRequest.class);
				PostEnrollmentsResourceSteps patchEnrollmentsSteps = new PostEnrollmentsResourceSteps(
					new PrepareToPatchEnrollments(),
					CreatePatchEnrollmentsRequestBodyToRequestEntityClaims.class,
					true
				);
				call(patchEnrollmentsSteps);
				callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class,Condition.ConditionResult.FAILURE);
				userAuthorisationCodeAccessToken();
			}
		});
	}

    protected abstract Class<? extends Condition> postEnrollmentsValidator();
    protected abstract Class<? extends Condition> getEnrollmentsValidator();
    protected abstract void executeTestSteps();
	protected abstract ConditionSequence getConsentAndResourceEndpointSequence();

}
