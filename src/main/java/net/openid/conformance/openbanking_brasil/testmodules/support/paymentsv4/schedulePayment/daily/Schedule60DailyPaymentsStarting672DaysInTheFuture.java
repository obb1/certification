package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily;

public class Schedule60DailyPaymentsStarting672DaysInTheFuture extends AbstractScheduleDailyPayment {

	@Override
	protected int getNumberOfDaysToAddToCurrentDate() {
		return 672;
	}

	@Override
	protected int getQuantity() {
		return 60;
	}
}
