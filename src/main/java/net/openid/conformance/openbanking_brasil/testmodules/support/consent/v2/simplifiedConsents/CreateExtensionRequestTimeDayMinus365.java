package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class CreateExtensionRequestTimeDayMinus365 extends AbstractSetConsentExtensionExpirationTimePast{
	@Override
	protected int getExtensionTimeInDays() {
		return 365;
	}
}
