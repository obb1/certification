package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class CreatePatchRecurringConsentsForEditionRequestBodyExpirationD270 extends AbstractCreatePatchRecurringConsentsForEditionRequestBody {

	@Override
	protected String getExpirationDateTime() {
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = currentDateTime.plusDays(270);
		return dateTime.format(DATE_TIME_FORMATTER);
	}

	@Override
	protected String getMaxVariableAmount() {
		return null;
	}
}
