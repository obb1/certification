package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2n1;

import org.springframework.http.HttpMethod;

public class PostConsentsAuthoriseOASValidatorV2n1 extends AbstractEnrollmentsOASValidatorV2n1 {

	@Override
	protected String getEndpointPathApi() {
		return "/consents";
	}

	@Override
	protected String getEndpointPathSuffix() {
		return "/{consentId}/authorise";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.POST;
	}
}
