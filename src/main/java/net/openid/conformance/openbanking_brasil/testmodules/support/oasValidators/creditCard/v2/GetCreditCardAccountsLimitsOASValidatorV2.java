package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetCreditCardAccountsLimitsOASValidatorV2 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/creditCard/swagger-credit-cards-2.3.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/accounts/{creditCardAccountId}/limits";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		body.getAsJsonArray("data").forEach(el -> {
			assertData(el.getAsJsonObject());
			asserLimitAmount(el.getAsJsonObject());
		});
	}

	private void assertData(JsonObject data){
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"limitAmount",
			"isLimitFlexible",
			false
		);
	}

	private void asserLimitAmount(JsonObject data){
		if (!data.has("limitAmount")) {
			return;
		}

		assertField1IsRequiredWhenField2HasValue2(
			data,
			"limitAmount.limitAmountReason",
			"limitAmount.amount",
			"0.00"
		);
	}
}
