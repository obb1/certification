package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.opendata.accounts.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

public class GetBusinessAccountsOASValidatorV1 extends AbstractGetOpenDataAccountsOASValidatorV1 {

	@Override
	protected String apiPrefix() {
		return "business";
	}

	@Override
	protected void assertSpecificConstraints(JsonObject obj) {
		String type = OIDFJSON.getString(obj.get("type"));
		JsonObject incomeRate = obj.getAsJsonObject("incomeRate");

		if (type.equals("CONTA_POUPANCA") && !incomeRate.has("savingAccount")) {
			throw error("savingAccount is required when type is CONTA_POUPANCA");
		}
	}
}
