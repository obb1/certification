package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsConsentsApiEnforceMANUTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_manu-fail_test-module_v4",
	displayName = "Payments Consents API test module for manu local instrument",
	summary = "Ensure error if a proxy or qrcode is sent when localInstrument is MANU (Ref Error 2.2.2.8)\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"• Calls POST Consents Endpoint with localInstrument as MANU and QRCode with email on the payload\n" +
		"• Expects 422 DETALHE_PAGAMENTO_INVALIDO - validate error message\n" +
		"• Calls POST Consents Endpoint with localInstrument as MANU and proxy with email on the payload\n" +
		"• Expects 422 DETALHE_PAGAMENTO_INVALIDO - validate error message\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpPaymentsConsentsApiEnforceMANUTestModuleV4 extends AbstractFvpPaymentsConsentsApiEnforceMANUTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
