package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class EnsureNoContractClientIdIsPresentInConfig extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String clientId = Optional.ofNullable(env.getElementFromObject("config", "resource.noContractClientId"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("The client_id for no contract test is not present in the config"));
		logSuccess("The client_id for no contract test is present in the config", args("client_id", clientId));
		return env;
	}
}
