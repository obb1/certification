package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class ConsentIdExtractor extends AbstractCondition {

	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	@PostEnvironment(strings = "consent_id")
	public Environment evaluate(Environment env) {
		try {
			String consentId = BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
				.map(OIDFJSON::toObject)
				.map(body -> body.getAsJsonObject("data"))
				.map(data -> data.get("consentId"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("Could not extract consentId from response body"));
			env.putString("consent_id", consentId);
			logSuccess("Found consent id", args("consentId", consentId));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}

		return env;
	}

}
