package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SavePaymentsRequestBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	@PostEnvironment(required = "savedBrazilPixPayment")
	public Environment evaluate(Environment env) {
		JsonObject paymentsBody = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.orElseThrow(() -> error("Unable to find payments body inside the resource",
				args("resource", env.getObject("resource"))));
		env.putObject("savedBrazilPixPayment", paymentsBody.deepCopy());
		logSuccess("Payments request body has been saved");
		return env;
	}
}
