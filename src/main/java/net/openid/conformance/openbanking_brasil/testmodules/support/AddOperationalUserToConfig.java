package net.openid.conformance.openbanking_brasil.testmodules.support;

public class AddOperationalUserToConfig extends AbstractAddVariantUserToConfig {
	@Override
	protected String getVariantUserKey() {
		return "brazilCpfOperational";
	}

	@Override
	protected String getVariantBusinessKey() {
		return "brazilCnpjOperationalBusiness";
	}
}
