package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CreatePaymentRequestEntityClaims;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.DeleteSavedAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ResetPaymentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.StoreScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidatePaymentAndConsentHaveSameProperties;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateNewE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.RemoveE2EIDFromPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.EnsureErrorResponseCodeFieldWasConsentimentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.sequence.ConditionSequence;

import java.util.Objects;

public abstract class AbstractPaymentsConsumedConsentsTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	private int batch = 1;
	@Override
	protected  void configureDictInfo(){
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SanitiseQrCodeConfig.class);
		callAndStopOnFailure(ValidatePaymentAndConsentHaveSameProperties.class);
		eventLog.startBlock("Storing authorisation endpoint");
		callAndStopOnFailure(StoreScope.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		eventLog.startBlock("Setting date to today");
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToPaymentsEndpoint.class);
		configureDictInfo();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		callAndStopOnFailure(DeleteSavedAccessToken.class);
		call(createOBBPreauthSteps().insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
			sequenceOf(condition(CreateRandomFAPIInteractionId.class),
				condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
		));
		callAndStopOnFailure(SaveAccessToken.class);

		runInBlock(currentClientString() + "Validate consents response", this::validatePostConsentResponse);

		eventLog.startBlock("Checking the created consent - Expecting AWAITING_AUTHORISATION status");
		callAndStopOnFailure(PaymentConsentIdExtractor.class);
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void requestProtectedResource() {
		env.mapKey("old_access_token", "authorization_code_access_token");
		callAndStopOnFailure(SaveAccessToken.class);
		eventLog.log("Saved the authorization_code access token", env.getObject("authorization_code_access_token"));
		env.unmapKey("old_access_token");
		ConditionSequence unhappyPixSequence = getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas422.class));
		ConditionSequence consumedConsentPixSequence = getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas422.class));
		switch (batch) {
			case 1: {
				batch++;
				eventLog.startBlock("Getting consent consumed through unhappy path (Missing endToEndId value)");
				callAndStopOnFailure(RemoveE2EIDFromPayment.class);
				call(unhappyPixSequence);
				env.mapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				validateErrorResponse(new EnsureErrorResponseCodeFieldWasParametroNaoInformado());
				env.unmapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY);
				eventLog.endBlock();
				eventLog.startBlock("Checking if consent was consumed");
				callAndStopOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class);
				callAndContinueOnFailure(EnsurePaymentConsentStatusWasConsumed.class, Condition.ConditionResult.FAILURE);
				eventLog.endBlock();
				eventLog.startBlock("Trying to make payment again (expects failure, as consent is consumed)");
				callAndStopOnFailure(ResetPaymentRequest.class);
				callAndStopOnFailure(GenerateNewE2EID.class);
				loadAuthorizationCode();
				call(consumedConsentPixSequence);
				env.mapKey(EnsureErrorResponseCodeFieldWasConsentimentoInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				validateErrorResponse(new EnsureErrorResponseCodeFieldWasConsentimentoInvalido());
				env.unmapKey(EnsureErrorResponseCodeFieldWasConsentimentoInvalido.RESPONSE_ENV_KEY);
				eventLog.endBlock();
				break;
			}
			case 2: {
				batch++;
				eventLog.startBlock("Getting consent consumed through happy path");
				ConditionSequence pixSequence = getPixPaymentSequence()
					.insertBefore(CreatePaymentRequestEntityClaims.class, condition(GenerateNewE2EID.class));
				call(pixSequence);
				eventLog.startBlock(currentClientString() + "Validate response");
				validateResponse();
				eventLog.endBlock();
				eventLog.startBlock("Checking if consent was consumed");
				callAndStopOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class);
				callAndContinueOnFailure(EnsurePaymentConsentStatusWasConsumed.class, Condition.ConditionResult.FAILURE);
				eventLog.endBlock();
				eventLog.startBlock("Trying to make payment again (expects failure, as consent is consumed)");
				loadAuthorizationCode();
				call(consumedConsentPixSequence);
				env.mapKey(EnsureErrorResponseCodeFieldWasConsentimentoInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
				validateErrorResponse(new EnsureErrorResponseCodeFieldWasConsentimentoInvalido());
				env.unmapKey(EnsureErrorResponseCodeFieldWasConsentimentoInvalido.RESPONSE_ENV_KEY);
				eventLog.endBlock();
				break;
			}
		}
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		if(batch==2) {
			eventLog.startBlock("2. Ensure error when the amount is different between consent and payment");
			validationStarted = false;
			callAndStopOnFailure(SetScope.class);
			performAuthorizationFlow();
		}
		else {
				fireTestFinished();
			}
		}

	protected void validateErrorResponse(AbstractEnsureErrorResponseCodeFieldWas errorFieldCondition) {
		callAndStopOnFailure(errorFieldCondition.getClass());
		callAndStopOnFailure(postPaymentValidator());
	}

	@Override
	protected void validateSelfLink(String responseFull) {
		if (Objects.equals(responseFull, "resource_endpoint_response_full") && env.containsObject("old_access_token")) {
			eventLog.log("Loaded client_credential access token", env.getObject("old_access_token"));
			callAndStopOnFailure(LoadOldAccessToken.class);
			callAndStopOnFailure(SaveAccessToken.class);
		}
		super.validateSelfLink(responseFull);
	}

	protected void loadAuthorizationCode() {
		env.mapKey("old_access_token", "authorization_code_access_token");
		callAndStopOnFailure(LoadOldAccessToken.class);
		callAndStopOnFailure(SaveAccessToken.class);
		eventLog.log("Loaded the authorization_code access token", env.getObject("access_token"));
		env.unmapKey("old_access_token");
	}
}
