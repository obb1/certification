package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

public class EditRecurringPaymentsBodyToSetDateToUserDefinedDate extends AbstractEditRecurringPaymentsBodyToSetDate {

	private String date;

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		date = env.getString("resource", "recurringPaymentDate");
		return super.evaluate(env);
	}

	@Override
	protected String getDate() {
		if (!date.matches("^(\\d{4})-(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])$")) {
			throw error("The value defined for resource.recurringPaymentDate is not valid", args("recurringPaymentDate", date));
		}
		return date;
	}
}
