package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.math.BigDecimal;
import java.util.Optional;

public class SetIncorrectAmountInPayment extends AbstractCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");
		JsonObject consentRequest = resource.getAsJsonObject("brazilPaymentConsent");
		JsonObject paymentRequest = resource.getAsJsonObject("brazilPixPayment");
		JsonElement paymentElement = env.getElementFromObject("resource", "brazilPixPayment.data");
		int execTimes = 1;
		JsonObject payment = null;

		JsonElement consentAmount = Optional.ofNullable(consentRequest
			.getAsJsonObject("data")
			.getAsJsonObject("payment")
			.get("amount")).orElseThrow(() -> error("Consent does not have an associated amount"));

		env.putString("previous_amount", OIDFJSON.getString(consentAmount));
		log("Previous amount: " + env.getString("previous_amount"));

		BigDecimal newAmount;
		try {
			newAmount = new BigDecimal(OIDFJSON.getString(consentAmount));
			newAmount = newAmount.add(new BigDecimal(100));
		} catch(Exception e){
			logFailure(String.format(
				"There was an error parsing an integer from the amount. This field may have been left empty." +
					"\nDebug message: {} , amount: {}",
				e.getMessage(), OIDFJSON.getString(consentAmount)
			));
			return env;
		}

		for (int i = 0; i < execTimes; i++) {
			if (paymentElement.isJsonArray()) {
				execTimes = paymentElement.getAsJsonArray().size();
				payment = paymentElement.getAsJsonArray().get(i).getAsJsonObject();
			}else{
				payment = paymentElement.getAsJsonObject();
			}
			payment
				.getAsJsonObject("payment")
				.addProperty("amount", newAmount.toString());
		}

		logSuccess("Successfully set the amount in the payment request to differ from the consent", paymentRequest);
		return env;
	}
}
