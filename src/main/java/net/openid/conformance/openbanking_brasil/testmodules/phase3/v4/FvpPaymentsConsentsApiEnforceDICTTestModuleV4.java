package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsConsentsApiEnforceDICTTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_dict_test-module_v4",
	displayName = "Payments Consents API test module for dict local instrument",
	summary = "Ensure error when localInstrument is DICT and QRCode is sent (Ref Error 2.2.2.8)\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"• Calls POST Consents Endpoint with localInstrument as DICT and QRCode on the payload\n" +
		"• Expects 422 DETALHE_PAGAMENTO_INVALIDO - Validate Error Message\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpPaymentsConsentsApiEnforceDICTTestModuleV4 extends AbstractFvpPaymentsConsentsApiEnforceDICTTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
