package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2ApiResourceTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvoiceFinancingsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractCreditOperationsDiscontinuedCreditRightsResourcesApiTestModule extends AbstractPhase2ApiResourceTestModule {

	@Override
	protected abstract Class<? extends Condition> apiValidator();

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetInvoiceFinancingsV2Endpoint.class)
		);
	}

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.INVOICE_FINANCINGS;
	}

	@Override
	protected OPFCategoryEnum getProductCategory() {
		return OPFCategoryEnum.CREDIT_OPERATIONS;
	}

	@Override
	protected String apiName() {
		return "invoice financing";
	}

	@Override
	protected String apiResourceId() {
		return "contractId";
	}

	@Override
	protected String resourceType() {
		return EnumResourcesType.INVOICE_FINANCING.name();
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
