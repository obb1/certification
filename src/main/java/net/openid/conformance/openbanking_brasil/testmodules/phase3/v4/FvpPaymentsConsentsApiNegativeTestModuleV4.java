package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractFvpPaymentsConsentsApiNegativeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-payments_api_consents_negative_test-module_v4",
	displayName = "Ensure an error status is sent on the POST Payments API in different inconsistent scenarios",
	summary = "Ensure an error status is sent on the POST Payments API in different inconsistent scenarios\n" +
		"• Create a client by performing a DCR against the provided server - Expect Success\n" +
		"1. Ensure error when Payment Type is \"BAD\"\n" +
		"• Calls POST Consents Endpoint with payment type as \"BAD\"\n" +
		"• Expects 422 PARAMETRO_INVALIDO - validate error message\n" +
		"2. Ensure error when an invalid person type is sent\n" +
		"• Calls POST Consents Endpoint with person type as \"INVALID\"\n" +
		"• Expects 422 PARAMETRO_INVALIDO - Validate error message\n" +
		"3. Ensure error when invalid currency is sent\n" +
		"•Calls POST Consents Endpoint with currency as \"XXX\"\n" +
		"•Expects 422 PARAMETRO_INVALIDO  - Validate error message\n" +
		"4. Ensure error when invalid date is sent\n" +
		"• Calls POST Consents Endpoint with past date value\n" +
		"• Expects 422 DATA_PAGAMENTO_INVALIDA - Validate error message\n" +
		"5. Ensure x-fapi-interaction-id is validated, and sent back when not requested\n" +
		"• Calls POST consents Endpoint with the x-fapi-interaction-id as \"123456\"\n" +
		"• Expects 400 or 422 PARAMETRO_INVALIDO - Validate Error message, ensure a valid x-fapi-interaction-id is sent on the response and its a UUID as RFC4122\n" +
		"• Delete the created client",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpPaymentsConsentsApiNegativeTestModuleV4 extends AbstractFvpPaymentsConsentsApiNegativeTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
