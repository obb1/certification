package net.openid.conformance.openbanking_brasil.testmodules.support.phase4;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class CheckCurrentTime extends AbstractCondition {
	@Override
	public Environment evaluate(Environment env) {
		log("Validating current time is between 9 PM and 11:59 PM in UTC-3");
		ZoneId timeZone = ZoneId.of("America/Sao_Paulo");
		ZonedDateTime now = ZonedDateTime.now(timeZone);
		LocalTime currentTime = now.toLocalTime();
		LocalTime startTime = LocalTime.of(21, 0);
		LocalTime endTime = LocalTime.of(23, 59, 59);

		if (currentTime.isAfter(startTime) && currentTime.isBefore(endTime)) {
			logSuccess("Current time is between 9 PM and 11:59 PM in UTC-3", args("current time", currentTime));
		} else {
			throw error("Current time is not between 9 PM and 11:59 PM in UTC-3", args("current time", currentTime));
		}
		return env;
	}
}
