package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;

public class CreateSweepingRecurringConfigurationObjectWithDailyLimits extends AbstractCreateRecurringConfigurationObject {

	private static final String TRANSACTION_LIMIT = "400.00";
	private static final String DAILY_TRANSACTION_LIMIT = "500.00";
	private static final int QUANTITY_LIMIT = 2;

	@Override
	protected JsonObject buildRecurringConfiguration(Environment env) {
		return new JsonObjectBuilder()
			.addFields("sweeping", Map.of(
				"totalAllowedAmount", AMOUNT,
				"transactionLimit", TRANSACTION_LIMIT,
				"startDateTime", getStartDateTime()
			))
			.addFields("sweeping.periodicLimits.day", Map.of(
				"quantityLimit", QUANTITY_LIMIT,
				"transactionLimit", DAILY_TRANSACTION_LIMIT
			))
			.build();
	}
}
