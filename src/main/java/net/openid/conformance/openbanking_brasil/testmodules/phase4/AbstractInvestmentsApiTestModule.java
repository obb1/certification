package net.openid.conformance.openbanking_brasil.testmodules.phase4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallConsentEndpointWithBearerToken;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.FAPIBrazilConsentEndpointResponseValidatePermissions;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.PaymentConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToDeleteConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveConsentsAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveOldValues;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToConsentSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvestmentsV1FromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.AddInvestmentsApiScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckScopeInConfigIsValid;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractInvestmentID;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateSelfEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.ConditionCallBuilder;
import net.openid.conformance.testmodule.TestFailureException;
import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;

import java.util.Optional;

public abstract class AbstractInvestmentsApiTestModule extends AbstractOBBrasilFunctionalTestModule {
	protected abstract OPFScopesEnum setScope();

	@Override
	protected void configureClient() {
		callAndStopOnFailure(setInvestmentsApi().getClass());
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetInvestmentsV1FromAuthServer.class),
			condition(GetConsentV3Endpoint.class)
		)));
		callAndStopOnFailure(CheckScopeInConfigIsValid.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddInvestmentsApiScope.class);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.INVESTMENTS).addScopes(setScope(), OPFScopesEnum.OPEN_ID).build();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndStopOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class);
		callAndContinueOnFailure(PostConsentOASValidatorV3n2.class, Condition.ConditionResult.FAILURE);

	}

	@Override
	protected void performPostAuthorizationFlow() {
		fetchConsentToCheckAuthorisedStatus();
		callAndContinueOnFailure(GetConsentOASValidatorV3n2.class, Condition.ConditionResult.FAILURE);
		super.performPostAuthorizationFlow();
	}


	@Override
	protected void requestProtectedResource() {
		preCallProtectedResource("GET Investment List to extract a investments ID");
		eventLog.startBlock("Validating GET Investment List response");
		call(validateGetInvestmentsResponse());
		eventLog.endBlock();
		executeParticularTestSteps();
	}

	protected ConditionSequence validateGetInvestmentsResponse() {
		return sequenceOf(
			condition(EnsureResourceResponseCodeWas200.class),
			condition(investmentsRootValidator()),
			condition(ExtractInvestmentID.class)
		);
	}

	protected ConditionSequence deleteConsent() {
		return sequenceOf(
			condition(LoadConsentsAccessToken.class),
			condition(PrepareToFetchConsentRequest.class),
			condition(PrepareToDeleteConsent.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
		);
	}

	@Override
	public void cleanup() {
		call(deleteConsent());
		env.mapKey("resource_endpoint_response_full", "consent_endpoint_response_full");
		callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		env.unmapKey("resource_endpoint_response_full");
	}

	@Override
	protected void call(ConditionCallBuilder builder) {
		super.call(builder);

		String resourceResponse = "resource_endpoint_response_full";
		String consentResponse = "consent_endpoint_response_full";
		String methodString = env.getString("http_method");
		HttpMethod method;
		if(methodString != null) {
			method = HttpMethod.valueOf(methodString);
			if (method.equals(HttpMethod.DELETE)) {
				return;
			}
		}

		if (CallProtectedResource.class.equals(builder.getConditionClass())) {
			if (env.getEffectiveKey(resourceResponse).equals(resourceResponse)) {
				validateLinksAndMeta(resourceResponse);
			} else {
				validateLinksAndMeta(consentResponse);
			}
		}
		if (CallConsentEndpointWithBearerToken.class.equals(builder.getConditionClass()) ||
			CallConsentEndpointWithBearerTokenAnyHttpMethod.class.equals(builder.getConditionClass())) {
			validateLinksAndMeta("consent_endpoint_response_full");
		}
	}

	private void validateLinksAndMeta(String responseFull) {

		int status = Optional.ofNullable(env.getInteger(responseFull, "status"))
			.orElseThrow(() -> new TestFailureException(getId(), String.format("Could not find %s", responseFull)));
		if (!isEndpointCallSuccessful(status)) {
			return;
		}


		String recursion = Optional.ofNullable(env.getString("recursion")).orElse("false");
		if (recursion.equals("false")) {
			callAndStopOnFailure(SaveOldValues.class);
			env.putString("recursion", "true");
			env.mapKey("resource_endpoint_response_full", responseFull);

			if (!responseFull.contains("consent") || env.getElementFromObject(responseFull, "body_json.links") != null) {
				validateLinks(responseFull);
			}

			env.unmapKey("resource_endpoint_response_full");
			callAndStopOnFailure(LoadOldValues.class);
			env.putString("recursion", "false");
		}
	}

	private void validateLinks(String responseFull) {
		call(exec().startBlock(responseFull.contains("consent") ? "Validate Consent Self link" : "Validate Self link"));
		env.mapKey("resource_endpoint_response_full", responseFull);

		ConditionSequence sequence = new ValidateSelfEndpoint();

		if (responseFull.contains("consent")) {
			sequence.replace(SetProtectedResourceUrlToSelfEndpoint.class, condition(SetProtectedResourceUrlToConsentSelfEndpoint.class));
		}

		call(sequence);
	}

	private boolean isEndpointCallSuccessful(int status) {
		if (status == HttpStatus.SC_CREATED || status == HttpStatus.SC_OK) {
			return true;
		}
		return false;
	}

	@Override
	protected void fetchConsentToCheckAuthorisedStatus() {
		eventLog.startBlock("Checking the created consent - Expecting AUTHORISED status");
		callAndStopOnFailure(PaymentConsentIdExtractor.class);
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAuthorised.class, Condition.ConditionResult.WARNING);
		eventLog.endBlock();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps()
			.insertAfter(FAPIBrazilConsentEndpointResponseValidatePermissions.class,
				condition(SaveConsentsAccessToken.class));
	}

	protected abstract void executeParticularTestSteps();

	protected abstract AbstractSetInvestmentApi setInvestmentsApi();

	protected abstract Class<? extends Condition> investmentsRootValidator();

	@Override
	protected void validateResponse() {}
}
