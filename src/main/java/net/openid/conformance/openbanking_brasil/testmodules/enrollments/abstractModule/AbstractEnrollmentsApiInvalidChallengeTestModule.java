package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditClientDataToSetRandomChallenge;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas.EnsureEnrollmentRejectionReasonWasRejeitadoFalhaFido;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasChallengeInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;

public abstract class AbstractEnrollmentsApiInvalidChallengeTestModule extends AbstractCompleteEnrollmentsApiEnrollmentTestModule {

	@Override
	protected void postAndValidateFidoRegistration() {
		runInBlock("Post fido-registration with invalid challenge - Expects 422 CHALLENGE_INVALIDO", () -> {
			prepareEnvironmentForPostFidoRegistration();
			call(createPostFidoRegistrationSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postFidoRegistrationValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasChallengeInvalido.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasChallengeInvalido.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasChallengeInvalido.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void prepareEnvironmentForPostFidoRegistration() {
		super.prepareEnvironmentForPostFidoRegistration();
		callAndContinueOnFailure(EditClientDataToSetRandomChallenge.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
		callAndContinueOnFailure(EnsureEnrollmentRejectionReasonWasRejeitadoFalhaFido.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void executeTestSteps() {}

	protected abstract Class<? extends Condition> postFidoRegistrationValidator();

}
