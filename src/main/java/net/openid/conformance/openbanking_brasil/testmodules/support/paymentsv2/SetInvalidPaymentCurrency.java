package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

public class SetInvalidPaymentCurrency extends AbstractSetPaymentCurrency {

	@Override
	protected String getCurrency() {
		return "XXX";
	}
}
