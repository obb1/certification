package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestment;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentBalances;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactionsCurrent;

public abstract class AbstractInvestmentsApiCoreTestModule extends AbstractInvestmentsApiTestModule {

	@Override
	protected void executeParticularTestSteps() {
		getAndValidateApi("Identification", () -> callAndStopOnFailure(PrepareUrlForFetchingInvestment.class), investmentsIdentificationValidator());

		getAndValidateApi("Balances", () -> callAndStopOnFailure(PrepareUrlForFetchingInvestmentBalances.class), investmentsBalancesValidator());

		getAndValidateApi("Transactions", this::prepareUrlForTransactions, investmentsTransactionsValidator());

		getAndValidateApi("Transactions-Current", this::prepareUrlForeTransactionsCurrent, investmentsTransactionsCurrentValidator());
	}

	protected void prepareUrlForTransactions() {
			callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(setFromTransactionTo360DaysAgo());
			callAndContinueOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl(), Condition.ConditionResult.FAILURE);

	}
	protected void prepareUrlForeTransactionsCurrent() {
			callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactionsCurrent.class);
			callAndStopOnFailure(setToTransactionToToday());
			callAndStopOnFailure(setFromTransactionTo6DaysAgo());
			callAndContinueOnFailure(addToAndFromTransactionParametersToProtectedResourceUrl(), Condition.ConditionResult.FAILURE);
	}

	protected void getAndValidateApi(String apiName, Runnable prepareUrl, Class<? extends Condition> validator) {
		runInBlock(String.format("GET Investments %s endpoint with extracted investmentId", apiName), () -> {
			prepareUrl.run();
			callAndStopOnFailure(CallProtectedResource.class);
		});
		runInBlock(String.format("Validating GET Investments %s response", apiName), () -> {
			callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(validator,Condition.ConditionResult.FAILURE);
		});
	}

	protected abstract Class<? extends Condition> investmentsIdentificationValidator();

	protected abstract Class<? extends Condition> investmentsBalancesValidator();

	protected abstract Class<? extends Condition> investmentsTransactionsValidator();

	protected abstract Class<? extends Condition> investmentsTransactionsCurrentValidator();

	protected abstract Class<? extends Condition> setToTransactionToToday();

	protected abstract Class<? extends Condition> setFromTransactionTo360DaysAgo();

	protected abstract Class<? extends Condition> setFromTransactionTo6DaysAgo();

	protected abstract Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl();

}
