package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.PostEnrollmentsFidoSignOptionsErrorEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasStatusConsentimentoInvalido extends AbstractEnsureErrorResponseCodeFieldWas {

    @Override
    protected List<String> getExpectedCodes() {
        return List.of(PostEnrollmentsFidoSignOptionsErrorEnum.STATUS_CONSENTIMENTO_INVALIDO.toString());
    }
}
