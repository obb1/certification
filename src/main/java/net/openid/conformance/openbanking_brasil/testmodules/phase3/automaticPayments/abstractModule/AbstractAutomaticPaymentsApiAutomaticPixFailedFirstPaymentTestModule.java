package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticPaymentsAutomaticPixHappyCorePathTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic.CreateAutomaticRecurringConfigurationObjectSemanal;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.AbstractSetPaymentReference;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.paymentReference.SetPaymentReferenceForSemanalPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.AbstractEnsureThereArePayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments.EnsureThereIsAtLeastOneSchdPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo50Cents;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.AbstractEnsurePaymentStatusWasX;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.AddMapFromPaymentIdToEndToEndId;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractAutomaticPaymentsApiAutomaticPixFailedFirstPaymentTestModule extends AbstractAutomaticPaymentsAutomaticPixHappyCorePathTestModule {

	@Override
	protected AbstractSetPaymentReference setPaymentReferenceForSecondPayment() {
		return new SetPaymentReferenceForSemanalPayment();
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateAutomaticRecurringConfigurationObjectSemanal();
	}

	@Override
	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountTo50Cents();
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		ConditionSequence sequence = super.getPixPaymentSequence();
		return isFirstPayment ?
			sequence.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class)) :
			sequence;
	}

	@Override
	protected void validateResponse() {
		call(postPaymentValidationSequence());

		int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));
		if (status == HttpStatus.CREATED.value()) {
			runRepeatSequence();
			validateFinalState();
			callAndStopOnFailure(AddMapFromPaymentIdToEndToEndId.class);
		} else {
			callAndStopOnFailure(EnsureResourceResponseCodeWas422.class);
			env.mapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(AbstractEnsureErrorResponseCodeFieldWas.RESPONSE_ENV_KEY);
		}

		stepsAfterFirstPayment();
	}

	@Override
	protected AbstractEnsurePaymentStatusWasX getEnsureFirstPaymentStatusCondition() {
		return new EnsurePaymentStatusWasRjct();
	}

	@Override
	protected AbstractEnsureThereArePayments getEnsureThereArePaymentsCondition() {
		return new EnsureThereIsAtLeastOneSchdPayment();
	}

	@Override
	protected void patchSecondPayment() {}

	@Override
	protected Class<? extends Condition> patchPaymentValidator() {
		return null;
	}
}
