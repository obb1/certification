package net.openid.conformance.openbanking_brasil.testmodules.support.consent.addExpirationToConsentRequest;

import java.time.temporal.ChronoUnit;

public class AddExpirationPlus30DaysToConsentRequest extends AbstractAddExpirationToConsentRequest {

	@Override
	protected int amountToAdd() {
		return 30;
	}

	@Override
	protected ChronoUnit timeUnit() {
		return ChronoUnit.DAYS;
	}
}
