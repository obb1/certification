package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiInvalidPublicKeyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.GetEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostFidoRegistrationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostFidoRegistrationOptionsOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_invalid-public-key_test-module_v2",
	displayName = "enrollments_api_invalid-public-key_test-module_v2",
	summary = "Ensure that enrollment can't be successfully executed when a Invalid Public key is sent:\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION\n" +
		"• Redirect the user to authorize the enrollment Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ENROLLMENT\"\n" +
		"• Call the POST fido-registration-options endpoint\n" +
		"• Expect a 201 response - Validate the response and extract the challenge\n" +
		"• Call the POST fido-registration endpoint, sending the attestationObject with a the credentialPublicKey as \"INVALID_PUBLIC_KEY\"\n" +
		"• Expect 422 - PUBLIC_KEY_INVALIDA - Validate Error response Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is REJECTED",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class EnrollmentsApiInvalidPublicKeyTestModuleV2 extends AbstractEnrollmentsApiInvalidPublicKeyTestModule {

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationValidator() {
		return PostFidoRegistrationOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV2Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2.class;
	}

	@Override
	public void cleanup() {
		//not required
	}
}
