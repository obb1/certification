package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsMonthlyCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_recurring-payments-monthly-core_test-module_v4",
	displayName = "Ensure that consent for monthly recurring payments can be carried out and that the payment is successfully scheduled even when its due date is the 31st, and the dates are adjusted accordingly",
	summary = "Ensure that consent for monthly recurring payments can be carried out and that the payment is successfully scheduled even when its due date is the 31st, and the dates are adjusted accordingly\n" +
		"• Call the POST Consents endpoints with the schedule.monthly.dayOfWeek field set as 31, schedule.monthly.startDate field set as D+1, and schedule.monthly.quantity as 5\n" +
		"• Expects 201 - Validate response\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent \n" +
		"• Expects 200 - Validate if status is “AUTHORISED” and validate response \n" +
		"• Calls the POST Payments Endpoint with the 5 Payments, using the apprpriate endtoendID for each of them, ensuring the day for the first payment is the next day 31 and not the startDate. Additionally, when dealing with months that do not have a 31st, send the date as the next day, which is the 1st of the next month. \n" +
		"• Expects 201 - Validate Response  \n" +
		"• Poll the Get Payments endpoint with the last PaymentID Created while payment status is RCVD or ACCP \n" +
		"• Call the GET Payments for the 5 Payments \n" +
		"• Expect Payment Scheduled in all of them (SCHD) - Validate Response\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiRecurringPaymentsMonthlyCoreTestModuleV4 extends AbstractPaymentsApiRecurringPaymentsMonthlyCoreTestModule {

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

}
