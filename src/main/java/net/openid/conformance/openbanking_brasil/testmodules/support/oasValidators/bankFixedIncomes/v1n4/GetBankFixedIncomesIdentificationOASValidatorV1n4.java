package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.AbstractGetBankFixedIncomesIdentificationOASValidator;

public class GetBankFixedIncomesIdentificationOASValidatorV1n4 extends AbstractGetBankFixedIncomesIdentificationOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/bank-fixed-incomes/bank-fixed-incomes-v1.0.4.yml";
	}
}
