package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractPermissionsCheckingFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddProductTypeToPhase2Config;
import net.openid.conformance.openbanking_brasil.testmodules.support.GetAuthServerFromParticipantsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvestmentsV1FromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas403;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckScopeInConfigIsValid;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractInvestmentID;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestment;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentBalances;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactionsCurrent;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentsRoot;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;

public abstract class AbstractInvestmentsApiWrongPermissionsTestModule extends AbstractPermissionsCheckingFunctionalTestModule {

    protected abstract AbstractSetInvestmentApi investmentApi();

	protected abstract OPFScopesEnum setScope();

    protected abstract Class<? extends Condition> apiValidator();

    @Override
    protected void configureClient() {
		JsonObject object = new JsonObject();
		object.keySet();

		callAndStopOnFailure(investmentApi().getClass());
		callAndStopOnFailure(GetAuthServerFromParticipantsEndpoint.class);
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(sequenceOf(
			condition(GetInvestmentsV1FromAuthServer.class),
			condition(GetConsentV3Endpoint.class)
		)));
		callAndStopOnFailure(CheckScopeInConfigIsValid.class);
        callAndStopOnFailure(AddProductTypeToPhase2Config.class);
        super.configureClient();
    }

    @Override
    protected void preFetchResources() {
        callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
        callAndContinueOnFailure(apiValidator(), Condition.ConditionResult.FAILURE);
        callAndStopOnFailure(ExtractInvestmentID.class);
    }

    @Override
    protected void prepareCorrectConsents() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.INVESTMENTS).addScopes(setScope(), OPFScopesEnum.OPEN_ID).build();
    }

    @Override
    protected void prepareIncorrectPermissions() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		String productType = env.getString("config", "consent.productType");
		if (productType.equals("business")) {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.BUSINESS_REGISTRATION_DATA).build();
		} else {
			scopesAndPermissionsBuilder.addPermissionsBasedOnCategory(OPFCategoryEnum.PERSONAL_REGISTRATION_DATA).build();
		}
    }

    @Override
    protected void requestResourcesWithIncorrectPermissions() {
        runEndpointExpectingFailure("Ensure we cannot call the GET investments list endpoint", new PrepareUrlForFetchingInvestmentsRoot());
        runEndpointExpectingFailure("Ensure we cannot call the GET investments identification endpoint", new PrepareUrlForFetchingInvestment());
        runEndpointExpectingFailure("Ensure we cannot call the GET investments balance endpoint", new PrepareUrlForFetchingInvestmentBalances());
        runEndpointExpectingFailure("Ensure we cannot call the GET investments transactions endpoint", new PrepareUrlForFetchingInvestmentTransactions());
        runEndpointExpectingFailure("Ensure we cannot call the GET investments transactions-current endpoint", new PrepareUrlForFetchingInvestmentTransactionsCurrent());
    }

    protected void runEndpointExpectingFailure(String blockText, ResourceBuilder urlPreparingCondition) {
        runInBlock(blockText, () -> {
            callAndStopOnFailure(urlPreparingCondition.getClass());
            call(sequence(CallProtectedResourceExpectingFailureSequence.class));
            callAndStopOnFailure(EnsureResourceResponseCodeWas403.class);
        });
    }
}
