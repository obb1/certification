package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AbstractCreateWebhookEndpoint;

public class WebhookWithEnrollmentIdV1 extends AbstractCreateWebhookEndpoint {

	@Override
	protected String getValueFromEnv() {
		return "enrollment_id";
	}

	@Override
	protected String getExpectedUrlPath() {
		return "/open-banking/enrollments/v1/enrollments/";
	}
}
