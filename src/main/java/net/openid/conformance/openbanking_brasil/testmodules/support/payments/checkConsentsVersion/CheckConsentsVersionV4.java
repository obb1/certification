package net.openid.conformance.openbanking_brasil.testmodules.support.payments.checkConsentsVersion;

public class CheckConsentsVersionV4 extends AbstractCheckConsentsVersion {

	@Override
	protected int expectedVersionNumber() {
		return 4;
	}
}
