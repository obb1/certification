package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.v3.AbstractCustomerDataXFapiTestModuleV3;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "accounts_api_x-fapi_test-module_v2-4",
	displayName = "accounts_api_x-fapi_test-module_v2",
	summary = """
		Test will ensure that the x-fapi-interaction-id is required at the request
		• Call the POST Consents endpoint with the Accounts API Permission Group
		• Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION
		• Redirect the user to Authorize the Consent - Expect a successful redirect
		• Call the GET Consents endpoint
		• Expects 200 - Validate response and ensure status is AUTHORISED
		• Call the GET Accounts Endpoint without the x-fapi-interaction-id
		• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back
		• Call the GET Accounts Endpoint with an invalid x-fapi-interaction-id
		• Expects 400 - Validate error message
		• Call the GET Accounts Endpoint with the x-fapi-interaction-id
		• Expects 200 - Validate response
	""",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class AccountApiXFapiTestModuleV24n extends AbstractCustomerDataXFapiTestModuleV3 {
	@Override
	protected boolean isConsents() {
		return false;
	}

	@Override
	protected Class<? extends Condition> getResourceValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}
	@Override
	protected OPFCategoryEnum getCategory() {
		return OPFCategoryEnum.ACCOUNTS;
	}
	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.ACCOUNTS;
	}


	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetAccountsV2Endpoint.class)
		);
	}

}
