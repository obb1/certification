package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditEnrollmentsRequestBodyToAddEnrollmentName extends AbstractCondition {

	private static final String REQUEST_CLAIMS_KEY = "resource_request_entity_claims";
	private static final String ENROLLMENT_NAME_KEY = "enrollment_name";
	private static final String ENROLLMENT_NAME_VALUE = "Vinculo JSR";

	@Override
	@PreEnvironment(required = REQUEST_CLAIMS_KEY)
	@PostEnvironment(strings = ENROLLMENT_NAME_KEY)
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(
			env.getElementFromObject(REQUEST_CLAIMS_KEY, "data")
		).map(JsonElement::getAsJsonObject).orElseThrow(() -> error("Could not find data inside the body"));

		data.addProperty("enrollmentName", ENROLLMENT_NAME_VALUE);
		env.putString(ENROLLMENT_NAME_KEY, ENROLLMENT_NAME_VALUE);
		logSuccess("Successfully added the \"enrollmentName\" property to the request body",
			args("request body", env.getObject(REQUEST_CLAIMS_KEY)));

		return env;
	}
}
