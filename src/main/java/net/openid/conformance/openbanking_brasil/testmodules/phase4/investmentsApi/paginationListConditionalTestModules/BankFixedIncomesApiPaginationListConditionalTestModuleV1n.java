package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesListOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "bank-fixed-incomes_api_pagination-list-cond_test-module_v1",
    displayName = "bank-fixed-incomes_api_pagination-list-cond_test-module_v1",
    summary = "Test will make sure that the pagination rules are being respected for the investments list endpoint, by changing the page-size and the page parameter of the URI and confirming that pagination is being correctly done. This is a conditional test that will be only executed if the brazilCpf/Cnpj for the Pagination List test is provided. Test will also require at least 51 investments to be returned\n" +
        "• Call the POST Consents endpoint, using the brazilCpf/Cnpj for Pagination List, the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ] - Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Investments List endpoint with page-size equal to 1000\n" +
        "• Expects a 200 response - Ensure that the totalRecords is equal os superior to 51 - Validate the Response_body\n" +
        "• Call the GET Investments List endpoint  with page-size equal to 25\n" +
        "• Expect a 200 response - Ensure that the number of returned transactions is equal to 25. Perform all required links validation, including, but not limited to, ensuring the next uri is different to the self uri, ensure the \"prev\" hasn't been sent - Validate the Response_body\n" +
        "• Call the GET Investments Transactions endpoint with the next link returned on the previous call\n" +
        "• Expects a 200 response - Ensure that the number of returned transactions is equal to 25.  Perform all required links validation, including, but not limited to, ensuring the next uri is different to the self uri, ensure the \"prev\" hasn't been sent, next and self uris are different and are pointing to pages 1, 2 and 3 respectively - Validate the Response_body\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf",
        "resource.brazilCpfPaginationList",
        "resource.brazilCnpjPaginationList"
    }
)
public class BankFixedIncomesApiPaginationListConditionalTestModuleV1n extends AbstractBankFixedIncomesApiPaginationListConditionalTest {

	@Override
    protected Class<? extends Condition> investmentsRootValidator() {
        return GetBankFixedIncomesListOASValidatorV1n4.class;
    }
}
