package net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPreFlightCertCheckPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "fvp-enrollments-api-pre-flight-test-v1",
	displayName = "Pre-Flights test that ensures that tested server accepted a DCR request and all the required tested URIs are registered within the Directory",
	summary = """
		Call the Tested Server Token endpoint with client_credentials grant - Make sure that server returns a 200
		Make sure The Provided Well-Known is registered within the Directory
		Call the Directory participants endpoint for either Sandbox or Production, depending on the used platform
		Make sure The Provided Well-Known is registered within the Directory
		Make sure that the server has registered a ApiFamilyType of value enrollments. Return the ApiEndpoint that ends with enrollments/v1/enrollments
		Make sure that the server has registered a ApiFamilyType of value payments-consents with ApiVersion of value “4.0.0\". Return the ApiEndpoint that ends with payments/v4/consents
		Make sure that the server has registered a ApiFamilyType of value payments-pix with ApiVersion of value “4.0.0\". Return the ApiEndpoint that ends with payments/v4/pix/payments
	""",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"directory.client_id",
		"resource.brazilCpf",
		"resource.brazilCnpj",
		"resource.consentUrl"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.consentSyncTime"
})
public class FvpEnrollmentsApiPreFlightTestModuleV1 extends AbstractPreFlightCertCheckPayments {
	@Override
	protected ConditionSequence getPaymentAndPaymentConsentEndpoints() {
		return sequenceOf(
			condition(GetEnrollmentsV1Endpoint.class),
			condition(GetPaymentConsentsV4Endpoint.class),
			condition(GetPaymentV4Endpoint.class)
		);
	}

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		call(new ValidateWellKnownUriSteps());
	}
}
