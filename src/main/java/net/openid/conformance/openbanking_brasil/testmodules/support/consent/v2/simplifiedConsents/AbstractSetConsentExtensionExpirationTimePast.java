package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;


import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public abstract class AbstractSetConsentExtensionExpirationTimePast extends AbstractCondition {
	@Override
	@PostEnvironment(strings = "consent_extension_expiration_time")
	public Environment evaluate(Environment env) {
		Instant expiryTime = Instant.now().minus(getExtensionTimeInDays(), ChronoUnit.DAYS);
		Instant expiryTimeNoFractionalSeconds = expiryTime.truncatedTo(ChronoUnit.SECONDS);

		String rfc3339ExpiryTime = DateTimeFormatter.ISO_INSTANT.format(expiryTimeNoFractionalSeconds);
		env.putString("consent_extension_expiration_time", rfc3339ExpiryTime);
		logSuccess("Created consent extension expiry time", args("consent_extension_expiration_time", rfc3339ExpiryTime));

		return env;
	}

	protected abstract int getExtensionTimeInDays();
}
