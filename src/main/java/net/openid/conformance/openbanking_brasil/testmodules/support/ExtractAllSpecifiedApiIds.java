package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Map;

public class ExtractAllSpecifiedApiIds extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full",strings = "apiIdName")
	@PostEnvironment(required = "extracted_api_ids")
	public Environment evaluate(Environment env) {

		JsonArray extractedApiIds = new JsonArray();
		JsonObject body;
		String specifiedApiIdName = env.getString("apiIdName");

		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		JsonArray data = body.getAsJsonArray("data");
		for (JsonElement jsonElement : data) {
			JsonObject jsonObject = jsonElement.getAsJsonObject();
			extractedApiIds.add(jsonObject.get(specifiedApiIdName));
		}

		if(!extractedApiIds.isEmpty()){
			JsonObject object = new JsonObject();
			object.add("extractedApiIds", extractedApiIds);
			env.putObject("extracted_api_ids", object);
			logSuccess("Extracted all API IDs:", Map.of("Extracted ID's", extractedApiIds));
		}else {
			throw error("No API IDs were extracted:", Map.of("data", data, "API ID", specifiedApiIdName));
		}

		return env;
	}
}
