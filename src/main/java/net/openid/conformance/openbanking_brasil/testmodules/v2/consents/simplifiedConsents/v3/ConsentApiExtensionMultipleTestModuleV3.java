package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentExtensionsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.GetConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentExtendsOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3.PostConsentOASValidatorV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractConsentApiExtensionMultipleTestModule;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_multiples-extensions_test-module_v3",
	displayName = "consents_api_multiples-extensions_test-module_v3",
	summary = "Ensure a consent can be extended multiple times\n" +
		"• Call the POST Consents Endpoint with expirationDateTime as current time + 5 minutes\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirect the user to authorize the consent\n" +
		"• Call the POST Token Endpoint Using the Authorization Code Grant\n" +
		"• Expects 201 - Validate if the refresh token sent back is not JWT\n" +
		"• Call the GET Consents\n" +
		"• Expects 200 - Validate Response and check if the status is AUTHORISED\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as  D+90, and all required headers\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as  D+180, and all required headers\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the POST Extends Endpoint without the expirationDateTime field\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the GET Extends Endpoint\n" +
		"• Expects 200 - Validate Response, check if there are three items on the data object and that the expirationDateTime for them are as requested previously and if totalPages and totalRecords are being sent",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl","consent.productType"
})
public class ConsentApiExtensionMultipleTestModuleV3 extends AbstractConsentApiExtensionMultipleTestModule {

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetConsentOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setConsentCreationValidation() {
		return PostConsentOASValidatorV3.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentExtensionValidator() {
		return PostConsentExtendsOASValidatorV3.class;
	}


	@Override
	protected Class<? extends Condition> setGetConsentExtensionValidator() {
		return GetConsentExtensionsOASValidatorV3.class;
	}

}
