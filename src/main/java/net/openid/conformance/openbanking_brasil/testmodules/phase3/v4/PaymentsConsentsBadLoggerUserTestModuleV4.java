package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrPaymentsConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp_payments_consents_api_bad-logged_test-module_v4",
	displayName = "FAPI1-Advanced-Final: Brazil DCR happy flow without authentication flow",
	summary = "This test will try to use the recently created DCR to access a Payments Consent V4 Call with a dummy, but well-formatted payload to make sure that the server will read the request but won’t be able to process it.\n" +
		"\u2022 Create a client by performing a DCR against the provided server - Expect Success\n" +
		"\u2022 Validate that the provided URI follows the expected pattern for payments-consents V4\n"+
		"\u2022 Generate a token with the client_id created using client_credentials grant with payments scope\n" +
		"\u2022 Use the token to call the POST Payments Consents API with a pre-defined payload\n" +
		"\u2022 If the Institution returns a 529 instead of a 201, wait 10 seconds and retry the endpoint call - Do this sequence 3 times. Return a failure if all three calls were a 529.\n"+
		"\u2022 Expect the server to accept the message and return a 201 - Validate the response_body and make sure the JWT has been correctly signed by the key registered on the A.S.\n",
	profile = "FAPI1-Advanced-Final",
	configurationFields = {
		"server.authorisationServerId",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"directory.client_id",
		"directory.apibase",
		"resource.consentUrl",
		"resource.brazilOrganizationId"
	}
)
public class PaymentsConsentsBadLoggerUserTestModuleV4 extends AbstractFvpDcrPaymentsConsentsTestModule {
	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetPaymentConsentsV4Endpoint.class;
	}
}
