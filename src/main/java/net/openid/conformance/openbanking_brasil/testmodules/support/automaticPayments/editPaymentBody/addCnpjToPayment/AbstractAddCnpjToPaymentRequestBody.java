package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.addCnpjToPayment;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public abstract class AbstractAddCnpjToPaymentRequestBody extends AbstractCondition {

	protected abstract String getKey();

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject document = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(paymentElement -> paymentElement.getAsJsonObject().getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("document"))
			.orElseThrow(() -> error("Unable to find data.document in payments payload"));

		JsonObject creditor = Optional.ofNullable(env.getObject(getKey()))
			.orElseThrow(() -> error("Unable to find creditor in the environment"));

		String identification = Optional.ofNullable(creditor.get("cpfCnpj"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find 'cpfCnpj' field inside the creditor", args("creditor", creditor)));

		document.addProperty("rel", "CNPJ");
		document.addProperty("identification", identification);

		logSuccess("data.document field has been updated with a new CNPJ", args("document", document));

		return env;
	}
}
