package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiNegativeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v1.PostRecurringPixOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_negative_test-module_v4",
	displayName = "Payments Consents API test module for dict local instrument",
	summary = "Ensure an error status in sent on different inconsistent scenarios (Reference Error 2.2.2.5/2.2.2.6)\n" +
		"1. Ensure error when currency is different between consents and payment\n" +
		"• Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"• Expects 201 - Validate Error\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST Payments Endpoint with a different currency\n" +
		"• Expects 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO \n" +
		"• Validate error message\n" +
		"• If no errors are found:\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"• Expects Definitive state (RJCT) with rejectionReason as PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"\n" +
		"2. Ensure error when the amount is different between consent and payment\n" +
		"• Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"• Expects 201\n" +
		"• Redirects the user to authorize the created consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST Payments Endpoint with different amount\n" +
		"• Expects 422 PAGAMENTO_DIVERGENTE_CONSENTIMENTO \n" +
		"• Validate error message\n" +
		"• If no errors are found:\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"• Expects Definitive state (RJCT) with rejectionReason as PAGAMENTO_DIVERGENTE_CONSENTIMENTO\n" +
		"\n" +
		"3. Ensure POST payments can only be called with authorization code flow\n" +
		"• Calls POST Consents Endpoint with valid payload\n" +
		"• Expects 201 - Validate Response\n" +
		"• Calls the POST Payments Endpoint with client_credentials token\n" +
		"• Expects 401\n" +
		"• Validate error message\n" +
		"\n" +
		"4. Ensure POST payment is successful when valid payload is sent\n" +
		"• Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"• Expects 201\n" +
		"• Redirects the user to authorize the consent\n" +
		"• Call GET Consent\n" +
		"• Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"• Calls the POST Payments Endpoint with valid payload\n" +
		"• Expects 201 - Validate Response\n" +
		"• Poll the Get Payments endpoint with the PaymentID Created while payment status is RCVD, ACCP or ACPD\n" +
		"• Expects Definitive  state (ACSC) - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiNegativeTestModuleV4 extends AbstractPaymentsApiNegativeTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

		@Override
	protected Class<? extends Condition> paymentInitiationErrorValidator() {
		return PostRecurringPixOASValidatorV1.class;
	}

}
