package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesBalancesOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesIdentificationOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesListOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesTransactionsOASValidatorV1n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "bank-fixed-incomes_api_x-fapi_test-module_v1",
	displayName = "bank-fixed-incomes_api_x-fapi_test-module_v1",
	summary = "Ensure that the x-fapi-interaction-id is required at the request for all endpoints\n" +
		"• Call the POST Consents endpoint with the Investments Permission Group\n" +
		"• Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint\n" +
		"• Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"• Call the GET Investments Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments Endpoint with the x-fapi-interaction-id\n" +
		"• Expects 200 - Validate response and extract InvestmentId\n" +
		"• Call the GET Investments/{InvestmentId} Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Investments/{InvestmentId} Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET balances Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET balances Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction current Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET transaction current Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class BankFixedIncomesApiXFapiTestModuleV1n extends AbstractBankFixedIncomesApiXFapiTest {

	@Override
	protected Class<? extends Condition> investmentsRootValidator() {
		return GetBankFixedIncomesListOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends Condition> investmentsIdentificationValidator() {
		return GetBankFixedIncomesIdentificationOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends Condition> investmentsBalancesValidator() {
		return GetBankFixedIncomesBalancesOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsValidator() {
		return GetBankFixedIncomesTransactionsOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsCurrentValidator() {
		return GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4.class;
	}

}
