package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ExtractMaximumVariableAmount extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	@PostEnvironment(strings = "maximumVariableAmount")
	public Environment evaluate(Environment env) {
		JsonObject response = extractResponseFromEnv(env);
		JsonObject data = response.getAsJsonObject("data");
		String maximumVariableAmount = Optional.ofNullable(data.getAsJsonObject("recurringConfiguration"))
			.map(recurringConfiguration -> recurringConfiguration.getAsJsonObject("automatic"))
			.map(automatic -> automatic.get("maximumVariableAmount"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not extract maximumVariableAmount field from response data"));

		env.putString("maximumVariableAmount", maximumVariableAmount);
		logSuccess("Successfully extracted maximumVariableAmount field value from response data",
			args("maximumVariableAmount", maximumVariableAmount));

		return env;
	}

	private JsonObject extractResponseFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}
}
