package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.setSweepingStartDateTime;

import java.time.LocalDateTime;

public class EditRecurringPaymentsConsentBodyToSetSweepingStartDateTimeToOneHourInTheFuture extends AbstractEditRecurringPaymentsConsentBodyToSetSweepingStartDateTime {

	@Override
	protected LocalDateTime calculateStartDateTime(LocalDateTime currentDateTime) {
		return currentDateTime.plusHours(1);
	}
}
