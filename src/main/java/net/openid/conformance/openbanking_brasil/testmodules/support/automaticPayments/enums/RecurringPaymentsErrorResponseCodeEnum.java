package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum RecurringPaymentsErrorResponseCodeEnum {

	SALDO_INSUFICIENTE,
    VALOR_ACIMA_LIMITE,
    VALOR_INVALIDO,
    LIMITE_PERIODO_VALOR_EXCEDIDO,
    LIMITE_PERIODO_QUANTIDADE_EXCEDIDO,
    CONSENTIMENTO_INVALIDO,
    CONSENTIMENTO_PENDENTE_AUTORIZACAO,
    PARAMETRO_NAO_INFORMADO,
    PARAMETRO_INVALIDO,
    NAO_INFORMADO,
    PAGAMENTO_DIVERGENTE_CONSENTIMENTO,
    DETALHE_PAGAMENTO_INVALIDO,
    PAGAMENTO_RECUSADO_DETENTORA,
    PAGAMENTO_RECUSADO_SPI,
    ERRO_IDEMPOTENCIA,
    LIMITE_VALOR_TOTAL_CONSENTIMENTO_EXCEDIDO,
    LIMITE_VALOR_TRANSACAO_CONSENTIMENTO_EXCEDIDO,
    LIMITE_TENTATIVAS_EXCEDIDO,
    FORA_PRAZO_PERMITIDO,
    DETALHE_TENTATIVA_INVALIDO;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
