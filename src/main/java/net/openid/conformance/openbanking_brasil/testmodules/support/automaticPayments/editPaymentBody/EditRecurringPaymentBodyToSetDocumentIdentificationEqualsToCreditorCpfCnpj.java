package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Optional;

public class EditRecurringPaymentBodyToSetDocumentIdentificationEqualsToCreditorCpfCnpj extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject resource = env.getObject("resource");

		String identification = Optional.ofNullable(resource.get("creditorCpfCnpj"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not find creditorCpfCnpj in the resource", args("resource", resource)));
		String rel = switch (identification.length()) {
			case 11 -> "CPF";
			case 14 -> "CNPJ";
			default -> throw error("The value in creditorCpfCnpj should be either a CPF or a CNPJ",
				args("creditorCpfCnpj", identification));
		};
		JsonObject document = new JsonObjectBuilder()
			.addField("identification", identification)
			.addField("rel", rel)
			.build();

		JsonObject data = Optional.ofNullable(resource.getAsJsonObject("brazilPixPayment"))
			.map(body -> body.getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));
		data.add("document", document);

		logSuccess("The document object has been updated to use the creditor identification as defined in the resource",
			args("data", data));

		return env;
	}
}
