package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.KeyType;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.AbstractFidoKeysCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractEnrollmentIdFromEnrollmentsEndpointResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractUserIdFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateKeyPairFromFidoRegistrationOptionsResponse;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.lang3.ArrayUtils;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;
import java.util.Map;
import java.util.Optional;

public class CreateFidoAuthorizeRequestBodyToRequestEntityClaims extends AbstractFidoKeysCondition {

	/**
	 * <b>request</b> JSON object requires the following env variables:
	 * <ul>
	 * <li>enrollment_id</li>
	 * <li>all env variables required to build <b>fidoAssertion</b> JSON object</li>
	 * </ul>
	 * <b>fidoAssertion</b> JSON object requires the following env variables:
	 * <ul>
	 * <li>fido_keys_jwk</li>
	 * <li>all env variables required to build <b>response</b> JSON object</li>
	 * </ul>
	 * <b>response</b> JSON object requires the following env variables:
	 * <ul>
	 * <li>rp_id</li>
	 * <li>client_data</li>
	 * <li>user_id</li>
	 * </ul>
	 * {@link GenerateFidoClientData} condition adds client_data object to the env
	 * {@link GenerateKeyPairFromFidoRegistrationOptionsResponse} condition adds fido_keys_jwk object to the env
	 * {@link ExtractEnrollmentIdFromEnrollmentsEndpointResponse} condition adds enrollment_id string to the env
	 * {@link CreateEnrollmentsFidoRegistrationOptionsRequestBody} condition adds rp_id string to the env
	 * {@link ExtractUserIdFromFidoRegistrationOptionsResponse} condition adds user_id string to the env
	 **/

	@Override
	@PreEnvironment(strings = "enrollment_id")
	@PostEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		final String enrollmentId = env.getString("enrollment_id");

		JsonObject riskSignals = CreateRiskSignalsRequestBodyToRequestEntityClaims.getRiskSignalsObject().getAsJsonObject("data");

		JsonObject request = new JsonObjectBuilder()
			.addFields("data", Map.of(
				"enrollmentId", enrollmentId,
				"riskSignals", riskSignals,
				"fidoAssertion", getFidoAssertionObject(env)
			))
			.build();

		env.putObject("resource_request_entity_claims", request);
		logSuccess("added authorize request body to the environment", Map.of("body", request));

		return env;

	}

	protected JsonObject getFidoAssertionObject(Environment env) {
		final JsonObject fidoKeysJwk = Optional.ofNullable(env.getObject("fido_keys_jwk"))
			.orElseThrow(() -> error("Could not find fido_keys_jwk in the environment"));

		String id = Optional.ofNullable(fidoKeysJwk.get("kid"))
			.map(OIDFJSON::getString)
			.map(this::encodeData)
			.orElseThrow(() -> error("Could not find kid in the fido_keys_jwk", Map.of("key", fidoKeysJwk)));

		return new JsonObjectBuilder()
			.addField("id", id)
			.addField("rawId", id)
			.addField("type", "public-key")
			.addField("response", getResponseObject(env, fidoKeysJwk))
			.build();
	}

	protected JsonObject getResponseObject(Environment env, JsonObject fidoKeysJwk) {

		final PrivateKey privateKey = getKeyPairFromJwkJson(fidoKeysJwk).getPrivate();
		final KeyType keyType = getKeyTypeFromJwkJson(fidoKeysJwk);


		final byte[] clientData = Optional.ofNullable(env.getObject("client_data"))
			.map(JsonElement::toString)
			.map(String::getBytes)
			.orElseThrow(() -> error("Could not find client_data in the environment"));

		final byte[] authenticatorData = getAuthenticatorData(env);

		final String userId = Optional.ofNullable(env.getString("user_id"))
			.orElseThrow(() -> error("Could not find user_id in the environment"));

		final String signature = getSignature(clientData, authenticatorData, privateKey, keyType);

		return new JsonObjectBuilder()
			.addField("clientDataJSON", encodeData(clientData))
			.addField("authenticatorData", encodeData(authenticatorData))
			.addField("signature", signature)
			.addField("userHandle", userId)
			.build();
	}


	protected String getSignature(byte[] clientData, byte[] authenticatorData, PrivateKey privateKey, KeyType keyType) {
		byte[] clientDataHash = getSha256Digest(clientData);
		final byte[] payload = ArrayUtils.addAll(authenticatorData, clientDataHash);
		final byte[] signedData = signData(payload, privateKey, keyType);
		return encodeData(signedData);
	}


	private byte[] signData(byte[] data, PrivateKey privateKey, KeyType keyType) {
		try {
			Signature signature = getSupportedSignatureInstance(keyType);
			signature.initSign(privateKey);
			signature.update(data);
			return signature.sign();
		} catch (InvalidKeyException | SignatureException e) {
			throw error("Could not sign authorisation payload");
		}
	}

	protected byte[] getAuthenticatorData(Environment env) {
		final String rpId = Optional.ofNullable(env.getString("rp_id"))
			.orElseThrow(() -> error("Could not find rp_id in the environment"));

		byte[] rpIdHash = getSha256Digest(rpId.getBytes(StandardCharsets.UTF_8));
		byte flags = Byte.parseByte("00000101", 2);
		byte[] signCount = new byte[4];
		byte[] payload = ArrayUtils.add(rpIdHash, flags);
		return ArrayUtils.addAll(payload, signCount);
	}

	protected String encodeData(String data){
		return encodeData(data.getBytes());
	}

	protected String encodeData(byte[] data) {
		return Base64.getUrlEncoder().withoutPadding().encodeToString(data);
	}
}
