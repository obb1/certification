package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsStatusEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractEnsureThereArePayments extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	protected abstract List<RecurringPaymentsStatusEnum> getExpectedStatuses();

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject body = extractBodyFromEnv(env);
		JsonArray data = body.getAsJsonArray("data");

		List<RecurringPaymentsStatusEnum> expectedStatuses = new ArrayList<>(getExpectedStatuses());

		boolean isExpectedAmount = isExactAmount() ? data.size() == getExpectedStatuses().size() : data.size() >= getExpectedStatuses().size();

		if (!isExpectedAmount) {
			throw error("The amount of payments returned is not what was expected",
				args("amount of payments", data.size(), "expected amount", expectedStatuses.size()));
		}

		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject();
			String status = OIDFJSON.getString(
				Optional.ofNullable(payment.get("status"))
					.orElseThrow(() -> error("Payment does not have status field", args("payment", payment)))
			);

			RecurringPaymentsStatusEnum statusEnum = convertStatusStringToEnum(status);

			boolean isPresent = expectedStatuses.remove(statusEnum);
			if (isExactAmount() && !isPresent) {
				throw error("Unexpected status found in payment", args("status", statusEnum, "payment", payment));
			}
		}

		if (!expectedStatuses.isEmpty()) {
			throw error("Not all of the expected statuses were found in the response", args("expected", getExpectedStatuses()));
		}

		return env;
	}

	protected JsonObject extractBodyFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	protected RecurringPaymentsStatusEnum convertStatusStringToEnum(String status) {
		try {
			return RecurringPaymentsStatusEnum.valueOf(status);
		} catch (IllegalArgumentException e) {
			throw error("Invalid status in payment", args("status", status));
		}
	}

	protected boolean isExactAmount() {
		return true;
	}
}
