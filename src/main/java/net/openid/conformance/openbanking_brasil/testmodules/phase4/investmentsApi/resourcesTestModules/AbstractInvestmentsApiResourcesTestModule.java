package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules;

import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetInvestmentsV1FromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckScopeInConfigIsValid;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.v2.AbstractApiResourceTestModuleV2;

public abstract class AbstractInvestmentsApiResourcesTestModule extends AbstractApiResourceTestModuleV2 {

    protected abstract AbstractSetInvestmentApi investmentApi();

    @Override
    protected void configureClient() {
        callAndStopOnFailure(investmentApi().getClass());
		super.configureClient();
		callAndStopOnFailure(CheckScopeInConfigIsValid.class);
    }

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getApiEndpoint() {
		return GetInvestmentsV1FromAuthServer.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}

	@Override
    protected String apiName() {
        return env.getString("investment-api");
    }

    @Override
    protected String apiResourceId() {
        return "investmentId";
    }
}
