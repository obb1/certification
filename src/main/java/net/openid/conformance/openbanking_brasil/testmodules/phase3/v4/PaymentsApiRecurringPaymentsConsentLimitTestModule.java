package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiRecurringPaymentsConsentLimitTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;


@PublishTestModule(
	testName = "payments_api_recurring-payments-consent-limit_test-module_v4",
	displayName = "payments_api_recurring-payments-consent-limit_test-module_v4",
	summary = "Ensure that consent for recurring payments cannot be created if it exceeds the consent limit rules\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+672 and schedule.daily.quantity as 60\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with the schedule.weekly.dayOfWeek field set as SEGUNDA_FEIRA, schedule.weekly.startDate field set as D+317 and schedule.weekly.quantity as 60\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with the schedule.monthly.dayOfMonth field set as 01, schedule.monthly.startDate field set as D+150 and schedule.monthly.quantity as 21\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with the schedule.custom.dates field set as D+100 and D+732\n" +
		"• Expects 422 - DATA_PAGAMENTO_INVALIDA\n" +
		"• Call the POST Consents endpoints with 2 identical items at schedule.custom.dates field, set as D+1 and D+1\n" +
		"• Expects 422 - PARAMETRO_INVALIDO\n" +
		"• Call the POST Consents endpoints with the schedule.daily.startDate field set as D+10 and schedule.daily.quantity as 61\n" +
		"• Expects 422 - PARAMETRO_INVALIDO\n" +
		"• Call the POST Consents endpoints with the schedule.weekly.dayOfWeek field set as SEGUNDA_FEIRA, schedule.weekly.startDate field set as D+10 and schedule.weekly.quantity as 61\n" +
		"• Expects 422 - PARAMETRO_INVALIDO",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.paymentAmount"
	}
)
public class PaymentsApiRecurringPaymentsConsentLimitTestModule extends AbstractPaymentsApiRecurringPaymentsConsentLimitTestModule {
	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
