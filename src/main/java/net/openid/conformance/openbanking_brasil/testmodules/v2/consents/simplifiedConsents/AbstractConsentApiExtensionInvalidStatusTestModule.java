package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.CreateExtensionRequestTimeDayPlus365;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.EnsureResponseCodeWas401or403;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.ValidateExtensionExpiryTimeInConsentResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;


public abstract class AbstractConsentApiExtensionInvalidStatusTestModule extends AbstractSimplifiedConsentRenewalNoRedirectTestModule {

	@Override
	protected abstract Class<? extends Condition> validatePostConsentResponse();

	@Override
	protected abstract Class<? extends Condition> validateGetConsentEndpointResponse();

	@Override
	protected void runTests() {
		super.runTests();
		call(getPostConsentExtensionExpectingErrorSequence(EnsureResponseCodeWas401or403.class, null,setExtensionExpirationTime()));
	}

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride){
		toggleSkipPagination(true);
		env.putString("metaOnlyRequestDateTime", "true");
		super.configure(config,baseUrl,externalUrlOverride);
	}

	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}
	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		return  CreateExtensionRequestTimeDayPlus365.class;
	}
	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

}
