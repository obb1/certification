package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1;

public class GetTreasureTitlesTransactionsCurrentV1OASValidator extends AbstractTreasureTitlesTransactionsOASValidatorV1 {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions-current";
	}


}
