package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

public class AddAutomaticPaymentToTheResource extends AbstractCondition {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmm");

	@Override
	@PostEnvironment(strings = {"endToEndId", "ispb"})
	public Environment evaluate(Environment env) {
		JsonObject resource = Optional.ofNullable(env.getObject("resource"))
			.orElseGet(() -> Optional.ofNullable(env.getElementFromObject("config", "resource"))
				.orElseThrow(() -> error("Could not find resource object"))
				.getAsJsonObject());

		JsonObject consentData = Optional.ofNullable(resource.getAsJsonObject("brazilPaymentConsent"))
			.map(brazilPaymentConsent -> brazilPaymentConsent.getAsJsonObject("data"))
			.orElseThrow(() -> error("Could not extract the consent data from the resource"));

		String identification;
		String rel;
		if (consentData.has("businessEntity")) {
			rel = "CNPJ";
			identification = OIDFJSON.getString(Optional.ofNullable(
				consentData.getAsJsonObject("businessEntity")
					.getAsJsonObject("document")
					.get("identification")
			).orElseThrow(() -> error("Could not find identification field inside businessEntity")));
		} else {
			rel = "CPF";
			identification = OIDFJSON.getString(Optional.ofNullable(
				consentData.getAsJsonObject("loggedUser")
					.getAsJsonObject("document")
					.get("identification")
			).orElseThrow(() -> error("Could not find identification field inside loggedUser")));
		}

		String endToEndId = generateEndToEndId(env);
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

		String localInstrument = Optional.ofNullable(env.getString("local_instrument"))
			.orElse(DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT);

		JsonObject payment = new JsonObjectBuilder()
			.addFields("data",
				Map.of(
					"endToEndId", endToEndId,
					"date", currentDate.toString(),
					"remittanceInformation", DictHomologKeys.PROXY_EMAIL_STANDARD_REMITTANCEINFORMATION,
					"cnpjInitiator", DictHomologKeys.PROXY_CNPJ_INITIATOR,
					"ibgeTownCode", DictHomologKeys.PROXY_EMAIL_STANDARD_IBGETOWNCODE,
					"localInstrument", localInstrument,
					"proxy", identification
				)
			)
			.addFields("data.payment",
				Map.of(
					"amount", "100.00",
					"currency", "BRL"
				)
			)
			.addFields("data.document",
				Map.of(
					"identification", identification,
					"rel", rel
				)
			)
			.build();

		env.putObject("resource", "brazilPixPayment", payment);
		logSuccess("Hardcoded brazilPixPayment object was added to the resource", payment);

		return env;
	}

	protected String generateEndToEndId(Environment env) {
		String ispb = DictHomologKeys.PROXY_CNPJ_INITIATOR.substring(0, 8);
		String randomString = RandomStringUtils.randomAlphanumeric(11);
		OffsetDateTime currentDateTime = OffsetDateTime.now(ZoneOffset.UTC);
		String formattedDateTime = currentDateTime.format(FORMATTER);
		String endToEndId = String.format("E%s%s%s", ispb, formattedDateTime, randomString);
		env.putString("endToEndId", endToEndId);
		env.putString("ispb", ispb);
		return endToEndId;
	}
}
