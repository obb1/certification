package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.v2.RecurringPaymentsErrorResponseCodeEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasForaPrazoPermitido extends AbstractEnsureErrorResponseCodeFieldWas {

	@Override
	protected List<String> getExpectedCodes() {
		return List.of(RecurringPaymentsErrorResponseCodeEnumV2.FORA_PRAZO_PERMITIDO.toString());
	}
}
