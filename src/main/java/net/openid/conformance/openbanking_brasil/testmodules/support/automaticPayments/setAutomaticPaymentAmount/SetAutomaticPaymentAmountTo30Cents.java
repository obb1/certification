package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

public class SetAutomaticPaymentAmountTo30Cents extends AbstractSetAutomaticPaymentAmount {

	@Override
	protected String automaticPaymentAmount() {
		return "0.30";
	}
}
