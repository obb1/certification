package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401or403;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;

public abstract class AbstractEnrollmentsApiPaymentsPreAccountHolderValidationTestModule extends AbstractEnrollmentsApiPaymentsPreEnrollmentTestModule {

	@Override
	protected void performAuthorizationFlow(){
		performPreAuthorizationSteps();
		executeTestSteps();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void postAndValidateConsentsAuthorise() {
		runInBlock("Call POST consents authorise with mocked fido assertion using client_credentials token - Expects 401 or 403", () -> {
			call(createConsentsAuthoriseSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas401or403.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postConsentsAuthoriseValidator(),Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void getAndValidateConsentAfterConsentsAuthorise() {
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(getConsentsValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void makeRefreshTokenCall() {}
}
