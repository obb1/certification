package net.openid.conformance.openbanking_brasil.testmodules.phase3;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJwt;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.StoreScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidatePaymentAndConsentHaveSameProperties;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.GenerateNewE2EID;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.MakeE2EIDInvalid;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.RemoveE2EIDFromPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractPaymentsApiE2EIDTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	private boolean secondTest = false;
	private boolean thirdTest = false;

	protected abstract void createPaymentErrorValidator();

	@Override
	protected void validateClientConfiguration() {
		callAndStopOnFailure(SanitiseQrCodeConfig.class);
		super.validateClientConfiguration();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		eventLog.startBlock("Validating consent and payment request are the same");
		callAndStopOnFailure(ValidatePaymentAndConsentHaveSameProperties.class);
		eventLog.startBlock("Storing authorisation endpoint");
		callAndStopOnFailure(StoreScope.class);
		callAndStopOnFailure(RemoveE2EIDFromPayment.class);
	}

	protected void fireSecondTest() {
		eventLog.startBlock("Starting second test with invalid E2EID");
		callAndStopOnFailure(GenerateNewE2EID.class);
		callAndStopOnFailure(MakeE2EIDInvalid.class);
		callAndStopOnFailure(SetScope.class);
	}

	protected void fireThirdTest() {
		eventLog.startBlock("Starting third test with valid E2EID");
		callAndStopOnFailure(GenerateNewE2EID.class);
		callAndStopOnFailure(SetScope.class);
	}

	@Override
	protected void requestProtectedResource() {
		if (!secondTest && !thirdTest) {
			eventLog.startBlock("Starting first test without E2EID");
			call(getPixUnhappyPaymentSequence());
			validateUnhappyResponse(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class);
		} else if (!thirdTest) {
			fireSecondTest();
			call(getPixUnhappyPaymentSequence());
			validateUnhappyResponse(EnsureErrorResponseCodeFieldWasParametroInvalido.class);
		} else {
			fireThirdTest();
			call(getPixPaymentSequence());
			validateResponse();
		}
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		if (!secondTest) {
			secondTest = true;
			callAndStopOnFailure(SetScope.class);
			performAuthorizationFlow();
		} else if (!thirdTest) {
			thirdTest = true;
			performAuthorizationFlow();
		} else {
			super.onPostAuthorizationFlowComplete();
		}
	}

	protected ConditionSequence getPixUnhappyPaymentSequence() {
		return getPixPaymentSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas400or422.class))
			.skip(EnsureContentTypeApplicationJwt.class, "Will be checked later if response code is 422")
			.skip(ExtractSignedJwtFromResourceResponse.class, "Will be checked later if response code is 422")
			.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, "Will be checked later if response code is 422")
			.skip(FAPIBrazilValidateResourceResponseTyp.class, "Will be checked later if response code is 422")
			.skip(FetchServerKeys.class, "Will be checked later if response code is 422")
			.skip(ValidateResourceResponseSignature.class, "Will be checked later if response code is 422")
			.skip(ValidateResourceResponseJwtClaims.class, "Will be checked later if response code is 422");
	}

	protected ConditionSequence getJwtValidationSequence() {
		return sequenceOf(
			exec().mapKey("endpoint_response", "resource_endpoint_response_full"),
			exec().mapKey("endpoint_response_jwt", "consent_endpoint_response_jwt"),
			condition(EnsureResponseWasJwt.class),
			condition(ExtractSignedJwtFromResourceResponse.class),
			condition(FAPIBrazilValidateResourceResponseSigningAlg.class),
			condition(FAPIBrazilValidateResourceResponseTyp.class),
			exec().mapKey("server", "org_server"),
			exec().mapKey("server_jwks", "org_server_jwks"),
			condition(FetchServerKeys.class),
			exec().unmapKey("server"),
			exec().unmapKey("server_jwks"),
			condition(ValidateResourceResponseSignature.class),
			condition(ValidateResourceResponseJwtClaims.class),
			exec().unmapKey("endpoint_response"),
			exec().unmapKey("endpoint_response_jwt")
		);
	}

	private int getResponseStatus() {
		return Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not extract status from response"));
	}

	protected void validateUnhappyResponse(Class<? extends Condition> errorValidator) {
		int status = getResponseStatus();


		if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			call(getJwtValidationSequence());
			env.mapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY,"resource_endpoint_response_full");
			callAndStopOnFailure(errorValidator);
			env.unmapKey(EnsureErrorResponseCodeFieldWasParametroNaoInformado.RESPONSE_ENV_KEY);
		} else {
			// 400
			callAndStopOnFailure(EnsureResponseWasJson.class);
		}

		createPaymentErrorValidator();
	}


}
