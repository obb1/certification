package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.financings.v2n2;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;


public class GetFinancingsWarrantiesV2n2OASValidator extends OpenAPIJsonSchemaValidator {


	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/financings/financings-v2.2.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/contracts/{contractId}/warranties";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


}
