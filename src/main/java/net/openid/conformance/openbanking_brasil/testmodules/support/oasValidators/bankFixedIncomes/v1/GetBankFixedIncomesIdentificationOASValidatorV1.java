package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.AbstractGetBankFixedIncomesIdentificationOASValidator;

public class GetBankFixedIncomesIdentificationOASValidatorV1 extends AbstractGetBankFixedIncomesIdentificationOASValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/bank-fixed-incomes/bank-fixed-incomes-v1.0.0.yml";
	}
}
