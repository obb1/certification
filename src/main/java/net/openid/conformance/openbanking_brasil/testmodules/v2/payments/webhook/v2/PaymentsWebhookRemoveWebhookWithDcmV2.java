package net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddBrazilPixPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePaymentConsentWebhookV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.CreatePaymentWebhookv2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentInitiationPixPaymentsValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.AbstractPaymentsWebhookRemoveWebhookWithDcm;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "dcr_api_dcm-pagto-remove-webhook_test-module",
	displayName = "dcr_api_dcm-pagto-remove-webhook_test-module",
	summary = "Ensure that the tested institution accepts a DCM request to remove the webhook notification endpoint. This test also ensures that a webhook is retried after a failure message is sent.\n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;\n"+
		"\u2022Obtain a SSA from the Directory\n" +
		"\u2022Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;, where the alias is to be obtained from the field alias on the test configuration\n" +
		"\u2022Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;”]\n" +
		"\u2022Expect a 201 - Validate Response\n" +
		"\u2022Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"\u2022Calls POST Consents Endpoint with localInstrument set as DICT\n" +
		"\u2022Expects 201 - Validate Response\n" +
		"\u2022Redirects the user to authorize the created consent\n" +
		"\u2022Call GET Consent\n" +
		"\u2022Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022Calls the POST Payments with localInstrument as DICT\n" +
		"\u2022Expects 201 - Validate response\n" +
		"\u2022Set the payments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v1/payments/v2/pix/payments/{paymentId} where the alias is to be obtained from the field alias on the test configuration\n" +
		"\u2022Set the payments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/&lt;alias&gt;/open-banking/webhook/v1/payments/v2/pix/consents/{consentsId} where the alias is to be obtained from the field alias on the test configuration\n" +
		"\u2022Expect an incoming message, for at most 60 seconds,which must be mtls protected, to be received on the webhook endpoint set for this test\n" +
		"\u2022Return a 503 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"\u2022Expect an incoming message, for at most 60 seconds, which must be mtls protected, to be received on the webhook endpoint set for this test\n" +
		"\u2022Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within  now and the start of the test\n" +
		"\u2022Call the Get Payments endpoint with the PaymentID \n" +
		"\u2022Expects status returned to be (ACSC) - Validate Response\n" +
		"\u2022Call the PUT Registration Endpoint without sending the field \"webhook_uris\"\n" +
		"\u2022Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"\u2022Calls POST Consents Endpoint with localInstrument set as DICT\n" +
		"\u2022Expects 201 - Validate Response\n" +
		"\u2022Redirects the user to authorize the created consent\n" +
		"\u2022Call GET Consent\n" +
		"\u2022Expects 200 - Validate if status is \"AUTHORISED\"\n" +
		"\u2022Calls the POST Payments with localInstrument as DICT\n" +
		"\u2022Expects 201 - Validate response\n" +
		"\u2022Validate that no webhook is called on the payments webhook endpoints \n" +
		"\u2022Call the Delete Registration Endpoint\n" +
		"\u2022Expect a 204 - No Content",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.webhookWaitTime"
	}
)
public class PaymentsWebhookRemoveWebhookWithDcmV2 extends AbstractPaymentsWebhookRemoveWebhookWithDcm {
	@Override
	protected void getPaymentAndPaymentConsentEndpoints() {
		call(new ValidateRegisteredEndpoints(
			sequenceOf(
				condition(GetPaymentV2Endpoint.class),
				condition(GetPaymentConsentsV2Endpoint.class)
			))
		);
	}

	@Override
	protected void setBrazilPixPaymentObject() {
		callAndStopOnFailure(AddBrazilPixPaymentToTheResource.class);
	}

	@Override
	protected Class<? extends Condition> setGetPaymentnValidator() {
		return PaymentInitiationPixPaymentsValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostPaymentValidator() {
		return PaymentInitiationPixPaymentsValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return PaymentConsentValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentValidator() {
		return PaymentConsentValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> setPaymentWebhookCreator() {
		return CreatePaymentWebhookv2.class;
	}

	@Override
	protected Class<? extends Condition> setPaymentConsentWebhookCreator() {
		return CreatePaymentConsentWebhookV2.class;
	}
}
