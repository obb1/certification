package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.field.ExtraField;
import org.springframework.http.HttpMethod;

public class GetAccountsBalancesOASValidatorV2n4 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/accounts/swagger-accounts-2.4.1.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/accounts/{accountId}/balances";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected ExtraField getExtraField() {
		return new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build();
	}
}
