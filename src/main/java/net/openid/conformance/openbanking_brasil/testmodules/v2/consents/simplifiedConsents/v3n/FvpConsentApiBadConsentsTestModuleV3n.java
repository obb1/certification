package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.AbstractGetXFromAuthServer;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractFvpConsentApiBadConsentsTestModule;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-consents_api_bad-consents_test-module_v3-2",
	displayName = "Validate that requests with incompatible consents return HTTP 400",
	summary = "Validate that requests with incompatible consents return HTTP 400\n" +
		"• Calls the Token Endpoint using the consents scope v3\n" +
		"• Creates a Consent sending personal and business permissions together\n" +
		"• Expect 422 PERMISSAO_PF_PJ_EM_CONJUNTO - Validate error message\n" +
		"• Creates a Consent without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message and verify that an x-fapi was sent back\n" +
		"• Creates a Consent with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message and verify that a valid x-fapi was sent back",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca"
	}
)
public class FvpConsentApiBadConsentsTestModuleV3n extends AbstractFvpConsentApiBadConsentsTestModule {

	@Override
	protected Class<? extends Condition> postConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends AbstractGetXFromAuthServer> getConsentEndpoint() {
		return GetConsentV3Endpoint.class;
	}
}
