package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.treasureTitles.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetTreasureTitlesListV1OASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/treasureTitles/treasure-titles-v1.0.0.yml";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected String getEndpointPath() {
		return "/investments";
	}


}
