package net.openid.conformance.openbanking_brasil.testmodules.support.yacs;

import com.google.gson.JsonElement;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public class EnsureDcrEndpointReturnedAuthMethodAsPrivateKeyJwt extends AbstractCondition {

	@Override
	@PreEnvironment(required = "dynamic_registration_endpoint_response")
	public Environment evaluate(Environment env) {
		try {
			String tokenEndpointAuthMethod = BodyExtractor.bodyFrom(env, "dynamic_registration_endpoint_response")
				.map(JsonElement::getAsJsonObject)
				.map(body -> body.get("token_endpoint_auth_method"))
				.map(OIDFJSON::getString)
				.orElseThrow(() -> error("There is no body.token_endpoint_auth_method inside the DCR endpoint response"));

			if (tokenEndpointAuthMethod.equals("private_key_jwt")) {
				logSuccess("DCR endpoint returned the 'token_endpoint_auth_method' field set as 'private_key_jwt', as expected");
			} else {
				throw error("DCR endpoint did not return the 'token_endpoint_auth_method' field set as 'private_key_jwt'",
					args("token_endpoint_auth_method", tokenEndpointAuthMethod));
			}
		} catch (ParseException e) {
			throw error("Could not parse body");
		}
		return env;
	}
}
