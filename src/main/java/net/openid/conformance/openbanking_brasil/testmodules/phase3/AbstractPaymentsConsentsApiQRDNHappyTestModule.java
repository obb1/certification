package net.openid.conformance.openbanking_brasil.testmodules.phase3;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.CreatePaymentRequestEntityClaims;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddHardcodedBrazilQrdnRemittanceToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.OptionallyAllow201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.CreatePaymentRequestEntityClaimsFromQrdnConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectPaymentConsentWithQrdnCode;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRDNCodeLocalInstrumentWithQrdnConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SelectQRDNCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SubsequentPixPaymentEditorCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.ValidateQrdnConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AddTransactionIdentificationFromConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasQRCodeInvalidoV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilPreAuthorizationErrorAgnosticSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3.GetPaymentsConsentValidatorV3;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;

import java.util.Optional;

public abstract class AbstractPaymentsConsentsApiQRDNHappyTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	private boolean secondPayment = false;

	@Override
	protected void performPreAuthorizationSteps() {
		env.mapKey("access_token", "saved_client_credentials");
		eventLog.log(getName(), "Payments scope present - protected resource assumed to be a payments endpoint");
		ConditionSequence steps;
		if (!secondPayment) {
			steps = new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
				.replace(FAPIBrazilCreatePaymentConsentRequest.class, condition(SelectPaymentConsentWithQrdnCode.class))
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(RememberOriginalScopes.class))
				.insertAfter(OptionallyAllow201Or422.class,
					condition(postPaymentConsentValidator())
						.dontStopOnFailure()
						.onFail(Condition.ConditionResult.FAILURE)
						.skipIfStringMissing("validate_errors"))
				.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
					sequenceOf(condition(CreateRandomFAPIInteractionId.class),
						condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
				);
			call(steps);
			callAndStopOnFailure(SaveAccessToken.class);
			//if error 422 and proceed empty, validate error again and skip test
			callAndStopOnFailure(CheckIfErrorResponseCodeFieldWasFormaPagamentoInvalida.class, Condition.ConditionResult.FAILURE);
			if (Optional.ofNullable(env.getBoolean("error_status_FPI")).orElse(false) ) {
				fireTestSkipped("422 - FORMA_PAGAMENTO_INVALIDA” implies that the institution does not support the used localInstrument set or the Payment time and the test scenario will be skipped. With the skipped condition the institution must not use this payment method on production until a new certification is re-issued");
			}

			eventLog.startBlock(currentClientString() + "Validate consents response");
			callAndContinueOnFailure(GetPaymentsConsentValidatorV3.class, Condition.ConditionResult.FAILURE);
			eventLog.endBlock();
		} else {
			steps = new OpenBankingBrazilPreAuthorizationErrorAgnosticSteps(addTokenEndpointClientAuthentication)
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class, condition(ResetScopesToConfigured.class))
				.replace(FAPIBrazilCreatePaymentConsentRequest.class, condition(SelectPaymentConsentWithQrdnCode.class))
				.insertAfter(OptionallyAllow201Or422.class,
					condition(postPaymentConsentValidator())
						.dontStopOnFailure()
						.onFail(Condition.ConditionResult.FAILURE)
						.skipIfStringMissing("validate_errors")
				)
				.insertAfter(AddFAPIAuthDateToResourceEndpointRequest.class,
					sequenceOf(condition(CreateRandomFAPIInteractionId.class),
						condition(AddFAPIInteractionIdToResourceEndpointRequest.class))
				);
			call(steps);
			callAndStopOnFailure(SaveAccessToken.class);
			int status = Optional.ofNullable(env.getInteger("consent_endpoint_response_full", "status"))
				.orElseThrow(() -> new TestFailureException(getId(), "Could not find consent_endpoint_response_full"));
			if (status == org.apache.http.HttpStatus.SC_CREATED) {
				callAndContinueOnFailure(GetPaymentsConsentValidatorV3.class, Condition.ConditionResult.FAILURE);
			} else {
				callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
				env.mapKey(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
				callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasDetalhePagamentoInvalido.class, Condition.ConditionResult.FAILURE);
				fireTestFinished();
			}
		}

	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		ConditionSequence steps = new CallPixPaymentsEndpointSequence()
			.replace(CreatePaymentRequestEntityClaims.class, condition(CreatePaymentRequestEntityClaimsFromQrdnConfig.class));

		return steps;
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectQRDNCodeLocalInstrumentWithQrdnConfig.class);
		callAndStopOnFailure(SelectQRDNCodePixLocalInstrument.class);
		callAndStopOnFailure(AddHardcodedBrazilQrdnRemittanceToTheResource.class);
		callAndStopOnFailure(AddTransactionIdentificationFromConfig.class);
		callAndStopOnFailure(ValidateQrdnConfig.class);
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		eventLog.startBlock("Try to re-use QR code");
		secondPayment = true;
		validationStarted = false;
		callAndContinueOnFailure(SubsequentPixPaymentEditorCondition.class, Condition.ConditionResult.FAILURE);
		performAuthorizationFlow();
	}

	@Override
	protected void onAuthorizationCallbackResponse(){
		if(!secondPayment){
			super.onAuthorizationCallbackResponse();
		}else {
			callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

			callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

			callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);

			callAndContinueOnFailure(CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint.class, Condition.ConditionResult.WARNING, "OIDCC-3.1.2.6");

			callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

			callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OAuth2-iss-2");

			fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
			callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasQRCodeInvalidoV3.class, Condition.ConditionResult.FAILURE);

			fireTestFinished();
		}
	}

}

