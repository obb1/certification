package net.openid.conformance.openbanking_brasil.testmodules.support.sequences;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilGetKeystoreJwksUri;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators.PaymentConsentValidatorV2;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class SignedPaymentConsentValidationSequence extends AbstractConditionSequence {
	@Override
	public void evaluate() {
		call(condition(EnsureContentTypeApplicationJwt.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1"));

		call(condition(ExtractSignedJwtFromResourceResponse.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1"));

		call(condition(FAPIBrazilValidateResourceResponseSigningAlg.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1"));

		call(condition(FAPIBrazilValidateResourceResponseTyp.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1"));

		call(condition(FAPIBrazilGetKeystoreJwksUri.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE));

		call(exec().mapKey("server", "org_server"));
		call(exec().mapKey("server_jwks", "org_server_jwks"));

		call(condition(FetchServerKeys.class));

		call(exec().unmapKey("server"));
		call(exec().unmapKey("server_jwks"));

		call(condition(ValidateResourceResponseSignature.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1"));

		call(condition(ValidateResourceResponseJwtClaims.class)
			.dontStopOnFailure()
			.onFail(Condition.ConditionResult.FAILURE)
			.requirement("BrazilOB-6.1"));

		call(exec().unmapKey("endpoint_response"));
		call(exec().unmapKey("endpoint_response_jwt"));

		callAndStopOnFailure(PaymentConsentValidatorV2.class);
	}
}
