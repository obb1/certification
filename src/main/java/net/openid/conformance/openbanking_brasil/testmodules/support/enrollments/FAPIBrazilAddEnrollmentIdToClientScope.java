package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class FAPIBrazilAddEnrollmentIdToClientScope extends AbstractCondition {

	@Override
	@PreEnvironment(required = "client", strings = "enrollment_id")
	public Environment evaluate(Environment env) {
		String enrollmentId = env.getString("enrollment_id");

		JsonObject client = env.getObject("client");

		String oldScope = Optional.ofNullable(client.get("scope"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("scope missing in client object"));

		String newScope = oldScope + " enrollment:" + enrollmentId;

		client.addProperty("scope", newScope);

		logSuccess("Added enrollmentId to client scope", args("client", client, "scope", newScope));

		return env;
	}

}
