package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.AbstractInvestmentsApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlPageSize1000;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1.GetVariableIncomeBrokerNotesV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1.GetVariableIncomesListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1.GetVariableIncomesTransactionsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.ExtractBrokerNoteId;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentBrokerNote;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.prepareUrlForFetchingInvestmentsConditions.PrepareUrlForFetchingInvestmentTransactions;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToVariableIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo360DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "variable-incomes_api_broker-note_test-module",
    displayName = "variable-incomes_api_broker-note_test-module",
    summary = "Test that the institution has correctly implemented the Broker Notes endpoint.\n" +
        "• Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
        "• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
        "• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
        "• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
        "• Call the GET Consents endpoint\n" +
        "• Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
        "• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
        "• Call the GET Investments List Endpoint\n" +
        "• Expect a 200 Response - Validate Response_body - Extract one investmentId\n" +
        "• Call the GET Investments Transactions Endpoint  with the Extracted investmentId - Send page-size= 1000 - Send query Params fromTransactionDate=D-360 and toTransactionDate=D+0\n" +
        "• Expect a 200 Response - Validate all fields of the API - Make sure the field \"brokerNoteId\" is returned for at least one of the transactions - Extract the brokerNoteId\n" +
        "• Call the GET Broker Note Details Endpoint with the Extracted investmentId and the Extracted brokerNoteId \n" +
        "• Expect a 200 Response - Validate all fields of the API\n" +
        "• Call the Delete Consents Endpoints\n" +
        "• Expect a 204 without a body",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    configurationFields = {
        "server.discoveryUrl",
        "client.client_id",
        "client.jwks",
        "mtls.key",
        "mtls.cert",
        "mtls.ca",
        "directory.discoveryUrl",
        "resource.brazilCpf"
    }
)
public class VariablesIncomesApiBrokerNoteTestModule extends AbstractInvestmentsApiTestModule {

    @Override
    protected void executeParticularTestSteps() {
        runInBlock("GET Investments Transactions Endpoint with the extracted investmentId", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentTransactions.class);
            callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
            callAndStopOnFailure(SetFromTransactionDateTo360DaysAgo.class);
            callAndStopOnFailure(SetToTransactionDateToToday.class);
            callAndStopOnFailure(AddToAndFromTransactionDateParametersToProtectedResourceUrl.class);
            callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
        });
        runInBlock("Validating GET Investments Transactions response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(GetVariableIncomesTransactionsV1OASValidator.class, Condition.ConditionResult.FAILURE);
            callAndStopOnFailure(ExtractBrokerNoteId.class);
        });

        runInBlock("GET Broker Note Details Endpoint with the extracted investmentId and the extracted brokerNoteId", () -> {
            callAndStopOnFailure(PrepareUrlForFetchingInvestmentBrokerNote.class);
            callAndContinueOnFailure(CallProtectedResource.class, Condition.ConditionResult.FAILURE);
        });
        runInBlock("Validating GET Broker Note Details response", () -> {
            callAndContinueOnFailure(EnsureResourceResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
            callAndContinueOnFailure(investmentsBrokerNoteValidator(), Condition.ConditionResult.FAILURE);
        });
    }

    @Override
    protected AbstractSetInvestmentApi setInvestmentsApi() {
        return new SetInvestmentApiToVariableIncomes();
    }

    @Override
    protected Class<? extends Condition> investmentsRootValidator() {
        return GetVariableIncomesListV1OASValidator.class;
    }

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.VARIABLE_INCOMES;
	}

	protected Class<? extends Condition> investmentsBrokerNoteValidator() {
		return GetVariableIncomeBrokerNotesV1OASValidator.class;
	}
}
