package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureNumberOfTotalRecords extends AbstractJsonAssertingCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment env) {
		JsonObject response;
		int totalRecords = 0;

		try {
			response = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		totalRecords = getTotalRecords(response);

		int totalRecordsComparisonAmount = getTotalRecordsComparisonAmount();
		switch (getComparisonMethod()) {
			case EQUAL:
				if (totalRecords == totalRecordsComparisonAmount) {
					logSuccess("Total records number is " + totalRecordsComparisonAmount);
				} else {
					throw error(String.format("Total records amount was not the expected %d, but %d", totalRecordsComparisonAmount, totalRecords));
				}
			case AT_LEAST:
				if (totalRecords >= totalRecordsComparisonAmount) {
					logSuccess("Total records number is at least " + totalRecordsComparisonAmount);
				} else {
					throw error(String.format("Total records amount was not more the expected %d, but less: %d", totalRecordsComparisonAmount, totalRecords));
				}
		}
		return env;
	}

	protected abstract int getTotalRecordsComparisonAmount();

	protected abstract TotalRecordsComparisonOperator getComparisonMethod();

	protected abstract int getTotalRecords(JsonObject response);


}
