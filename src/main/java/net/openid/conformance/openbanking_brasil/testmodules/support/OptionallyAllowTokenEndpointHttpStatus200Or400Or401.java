package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import org.apache.http.HttpStatus;

public class OptionallyAllowTokenEndpointHttpStatus200Or400Or401 extends AbstractCondition {

    @Override
    public Environment evaluate(Environment env) {

        Integer statusCode = env.getInteger("token_endpoint_response_http_status");

        if (statusCode == HttpStatus.SC_OK) {
            env.putString("status_ok", "ok");
            env.removeNativeValue("status_four_hundred");
        } else if (statusCode == HttpStatus.SC_BAD_REQUEST || statusCode == HttpStatus.SC_UNAUTHORIZED) {
            env.putString("status_four_hundred", "four_hundred");
            env.removeNativeValue("status_ok");
        } else {
            throw error("endpoint returned an unexpected http status - expected 200 or 400/401", args("http_status", statusCode));
        }

        logSuccess("endpoint returned the expected http status", args("http_status", statusCode));

        return env;

    }
}
