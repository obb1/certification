package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnforcePresenceOfDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProxyToRealPhoneNumber;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectRealCreditorAccountPhoneToPaymentConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.InjectRealCreditorAccountToPaymentPhone;


public abstract class AbstractPaymentsConsentsApiPhoneNumberProxyTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(EnforcePresenceOfDebtorAccount.class);
		// Setting consent to DICT / proxy to real phone number
		eventLog.startBlock("Setting payment consent payload to use real phone number + DICT");
		callAndContinueOnFailure(SelectDICTCodeLocalInstrument.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(InjectRealCreditorAccountPhoneToPaymentConsent.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(SetProxyToRealPhoneNumber.class, Condition.ConditionResult.FAILURE);

		// Setting payment to DICT / proxy to real phone number
		eventLog.startBlock("Setting payment payload to use real phone number + DICT");
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndContinueOnFailure(InjectRealCreditorAccountToPaymentPhone.class, Condition.ConditionResult.FAILURE);
	}

}
