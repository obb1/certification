package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureThereArePayments;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsStatusEnum;

import java.util.List;

public class EnsureThereAreTwoAcscPayments extends AbstractEnsureThereArePayments {

	@Override
	protected List<RecurringPaymentsStatusEnum> getExpectedStatuses() {
		return List.of(RecurringPaymentsStatusEnum.ACSC, RecurringPaymentsStatusEnum.ACSC);
	}
}
