package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.validators;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.LocalInstrumentsEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PatchPaymentCancellationFromEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PatchPaymentCancellationReasonEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentAccountTypesEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentRejectionReasonEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.enums.PaymentStatusEnumV2;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.ExtraField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Optional;

/**
 * Doc https://openbanking-brasil.github.io/openapi/swagger-apis/payments/2.0.0.yml
 * URL: /pix/payments/{paymentId}
 * URL: /pix/payments
 * Api git hash: 127e9783733a0d53bde1239a0982644015abe4f1
 */

@ApiName("Payment Initiation Pix V2")
public class PaymentInitiationPixPaymentsValidatorV2 extends AbstractJsonAssertingCondition {
	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = {RESPONSE_ENV_KEY, "resource_request_entity_claims"})
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment,RESPONSE_ENV_KEY);
		JsonElement requestBody = Optional.ofNullable(environment.getObject("resource_request_entity_claims"))
			.orElseThrow(() -> error("resource_request_entity_claims is missing in the environment"));

		JsonElement requestData = Optional.ofNullable(requestBody.getAsJsonObject().get("data"))
			.orElseThrow(() -> error("data is missing in the resource_request_entity_claims", args("resource_request_entity_claims", requestBody)));

		assertField(body,
			new ObjectField
				.Builder(ROOT_PATH)
				.setValidator(o -> assertInnerFields(o, requestData.getAsJsonObject(), environment))
				.build());

		return environment;
	}

	private void assertInnerFields(JsonObject body, JsonObject requestData, Environment environment) {
		parseResponseBody(body, ROOT_PATH);

		assertField(body,
			new StringField
				.Builder("paymentId")
				.setPattern("^[a-zA-Z0-9][a-zA-Z0-9\\-]{0,99}$")
				.setMinLength(1)
				.setMaxLength(100)
				.build());

		assertField(body,
			new StringField
				.Builder("endToEndId")
				.setPattern("^([E])([0-9]{8})([0-9]{4})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])(2[0-3]|[01][0-9])([0-5][0-9])([a-zA-Z0-9]{11})$")
				.setMinLength(32)
				.setMaxLength(32)
				.build());


		String requestEndToEndId;
		JsonElement reqEndToEndIdElement = requestData.get("endToEndId");
		if (reqEndToEndIdElement != null) {
			requestEndToEndId = OIDFJSON.getString(reqEndToEndIdElement);
		} else {
			requestEndToEndId = environment.getString("endToEndId");
			if (requestEndToEndId == null) {
				throw error("endToEndId is missing in both the payment request body and the environment");
			}
		}
		String endToEndId = OIDFJSON.getString(body.get("endToEndId"));

		if (!requestEndToEndId.equals(endToEndId)) {
			throw error("Response endToEndId is not to equal to the request endToEndId",
				args("RequestId", requestEndToEndId, "ResponseId", endToEndId));
		}


		assertField(body, new StringField
			.Builder("consentId")
			.setPattern("^urn:[a-zA-Z0-9][a-zA-Z0-9-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%\\/?#]+$")
			.setMaxLength(256)
			.build());

		assertField(body,
			new DatetimeField
				.Builder("creationDateTime")
				.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
				.build());

		assertField(body,
			new DatetimeField
				.Builder("statusUpdateDateTime")
				.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
				.build());


		assertField(body,
			new StringField
				.Builder("localInstrument")
				.setEnums(LocalInstrumentsEnumV2.toSet())
				.build());


		String localInstrument = OIDFJSON.getString(body.get("localInstrument"));

		StringField.Builder proxyBuilder = new StringField.Builder("proxy");

		if (localInstrument.equalsIgnoreCase(LocalInstrumentsEnumV2.MANU.toString())) {
			proxyBuilder.setMustNotBePresent();
		}

		assertField(body, proxyBuilder
			.setPattern("[\\w\\W\\s]*")
			.setMaxLength(77)
			.build());

		assertField(body,
			new StringField
				.Builder("ibgeTownCode")
				.setMinLength(7)
				.setMaxLength(7)
				.setPattern("^\\d{7}$")
				.setOptional()
				.build());

		assertField(body,
			new StringField
				.Builder("status")
				.setEnums(PaymentStatusEnumV2.toSet())
				.build());


		ObjectField.Builder rejectionReasonBuilder = new ObjectField.Builder("rejectionReason");

		String status = OIDFJSON.getString(body.get("status"));
		if (status.equalsIgnoreCase(PaymentStatusEnumV2.RJCT.toString())) {
			rejectionReasonBuilder.setOptional(false);
		} else {
			rejectionReasonBuilder.setMustNotBePresent();
		}
		assertField(body,
			rejectionReasonBuilder
				.setValidator(this::rejectionReason)
				.build());


		assertField(body,
			new StringField
				.Builder("cnpjInitiator")
				.setPattern("^\\d{14}$")
				.setMaxLength(14)
				.build());

		assertField(body,
			new ObjectField.
				Builder("payment")
				.setValidator(this::assertPayment)
				.build());

		Optional<JsonElement> optionalTransactionIdentification = Optional.ofNullable(requestData.get("transactionIdentification"));

		boolean isOptional = optionalTransactionIdentification.isEmpty();

		assertField(body,
			new StringField
				.Builder("transactionIdentification")
				.setPattern("^[a-zA-Z0-9]{1,35}$")
				.setMaxLength(35)
				.setOptional(isOptional)
				.build());

		if (!isOptional) {
			String transactionIdentification = OIDFJSON.getString(body.get("transactionIdentification"));
			String requestTransactionIdentification = OIDFJSON.getString(optionalTransactionIdentification.get());
			if (!transactionIdentification.equals(requestTransactionIdentification)) {
				throw error("Response transactionIdentification is not to equal to the request transactionIdentification",
					args("RequestId", requestTransactionIdentification, "ResponseId", transactionIdentification));
			}
		}

		assertField(body,
			new StringField
				.Builder("remittanceInformation")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(140)
				.setOptional()
				.build());


		assertField(body,
			new ObjectField
				.Builder("creditorAccount")
				.setValidator(this::assertCreditorAccount)
				.build());

		assertField(body,
			new ObjectField.
				Builder("cancellation")
				.setValidator(this::assertCancellation)
				.setOptional(isCancellationOptional())
				.build());


		assertField(body,
			new ObjectField
				.Builder("debtorAccount")
				.setValidator(this::assertCreditorAccount)
				.build());

		assertExtraFields(new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build());
	}

	protected boolean isCancellationOptional() {
		return true;
	}

	private void assertCancellation(JsonObject cancellation) {
		assertField(cancellation,
			new DatetimeField
				.Builder("cancelledAt")
				.setMaxLength(20)
				.setPattern(DatetimeField.ALTERNATIVE_PATTERN)
				.build());

		assertField(cancellation,
			new ObjectField.
				Builder("cancelledBy")
				.setValidator(this::assertCancelledBy)
				.build());

		assertField(cancellation,
			new StringField
				.Builder("reason")
				.setEnums(PatchPaymentCancellationReasonEnumV2.toSet())
				.build());

		assertField(cancellation,
			new StringField
				.Builder("cancelledFrom")
				.setEnums(PatchPaymentCancellationFromEnumV2.toSet())
				.build());

	}

	private void assertCancelledBy(JsonObject cancelledBy) {
		assertField(cancelledBy,
			new ObjectField
				.Builder("document")
				.setValidator(this::assertCancelledByDocument)
				.build());
	}

	private void assertCancelledByDocument(JsonObject document) {
		assertField(document,
			new StringField
				.Builder("identification")
				.setPattern("^\\d{11}$")
				.setMaxLength(11)
				.build());

		assertField(document,
			new StringField
				.Builder("rel")
				.setPattern("^[A-Z]{3}$")
				.setMaxLength(3)
				.build());
	}


	private void rejectionReason(JsonObject rejectionReason) {
		assertField(rejectionReason,
			new StringField
				.Builder("code")
				.setEnums(PaymentRejectionReasonEnumV2.toSet())
				.build());


		assertField(rejectionReason,
			new StringField
				.Builder("detail")
				.setPattern("[\\w\\W\\s]*")
				.setMaxLength(2048)
				.build());
	}

	private void assertPayment(JsonObject payment) {
		assertField(payment,
			new StringField
				.Builder("amount")
				.setMinLength(4)
				.setMaxLength(19)
				.setPattern("^((\\d{1,16}\\.\\d{2}))$")
				.build());

		assertField(payment,
			new StringField
				.Builder("currency")
				.setPattern("^([A-Z]{3})$")
				.setMaxLength(3)
				.build());
	}

	private void assertCreditorAccount(JsonObject creditorAccount) {

		assertField(creditorAccount,
			new StringField
				.Builder("ispb")
				.setPattern("^[0-9]{8}$")
				.setMaxLength(8)
				.setMinLength(8)
				.build());


		assertField(creditorAccount,
			new StringField
				.Builder("number")
				.setPattern("^[0-9]{1,20}$")
				.setMinLength(1)
				.setMaxLength(20)
				.build());

		assertField(creditorAccount,
			new StringField
				.Builder("accountType")
				.setEnums(PaymentAccountTypesEnumV2.toSet())
				.build());

		String accountType = OIDFJSON.getString(creditorAccount.get("accountType"));
		StringField.Builder issuerBuilder = new StringField.Builder("issuer");

		if (accountType.equalsIgnoreCase(PaymentAccountTypesEnumV2.TRAN.toString())) {
			issuerBuilder.setOptional();
		}

		assertField(creditorAccount, issuerBuilder
			.setPattern("^[0-9]{1,4}$")
			.setMinLength(1)
			.setMaxLength(4)
			.build());
	}
}
