package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckMatchingCallbackParameters;
import net.openid.conformance.condition.client.CheckStateInAuthorizationResponse;
import net.openid.conformance.condition.client.RejectAuthCodeInAuthorizationEndpointResponse;
import net.openid.conformance.condition.client.RejectStateInUrlQueryForHybridFlow;
import net.openid.conformance.condition.client.ValidateIssIfPresentInAuthorizationResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountToOutOfRangeOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasSaldoInsuficienteOrValorAcimaLimiteV3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;

public abstract class AbstractPaymentsNoFundsTestModule extends AbstractPaymentUnhappyPathTestModule {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SetPaymentAmountToOutOfRangeOnConsent.class);
	}

	protected Class<? extends Condition> getEnsureConsentsRejectionReasonCode() {
		return EnsureConsentsRejectionReasonCodeWasSaldoInsuficienteOrValorAcimaLimiteV3.class;
	}

	@Override
	protected void onAuthorizationCallbackResponse() {

		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

		// as https://tools.ietf.org/html/draft-ietf-oauth-iss-auth-resp is still a draft we only warn if the value is wrong,
		// and do not require it to be present.
		callAndContinueOnFailure(ValidateIssIfPresentInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");

		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);

		callAndStopOnFailure(RejectAuthCodeInAuthorizationEndpointResponse.class);

		performPostAuthorizationFlow();
	}

	@Override
	protected void performPostAuthorizationFlow() {

		callAndStopOnFailure(LoadOldAccessToken.class);
		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		callAndStopOnFailure(getEnsureConsentsRejectionReasonCode());
		fireTestFinished();
	}
}
