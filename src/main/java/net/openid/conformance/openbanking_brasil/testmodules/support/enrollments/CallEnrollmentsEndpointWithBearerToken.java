package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.util.Collections;
import java.util.Optional;

public class CallEnrollmentsEndpointWithBearerToken extends CallProtectedResource {

    @Override
    protected String getUri(Environment env) {
        return Optional.ofNullable(env.getString("enrollments_url"))
			.orElseThrow(() -> error("enrollments url missing"));
    }

    @Override
    protected HttpMethod getMethod(Environment env) {
        return HttpMethod.valueOf(
            Optional.ofNullable(env.getString("http_method")
            ).orElseThrow(() -> error("HTTP method not found")));
    }

    @Override
    protected Object getBody(Environment env) {
		Object body;
		if (getMethod(env)==HttpMethod.GET || getMethod(env)==HttpMethod.DELETE) {
			body = null;
		} else {
			body = env.getString("resource_request_entity");
		}
		return body;
    }

    @Override
    protected MediaType getContentType(Environment env) {
        return DATAUTILS_MEDIATYPE_APPLICATION_JWT;
    }

    @Override
    protected HttpHeaders getHeaders(Environment env) {
        HttpHeaders headers = super.getHeaders(env);
        headers.setAccept(Collections.singletonList(DATAUTILS_MEDIATYPE_APPLICATION_JWT));
		if (getMethod(env) == HttpMethod.POST || getMethod(env) == HttpMethod.PATCH) {
			headers.setContentType(getContentType(env));
		}
        return headers;
    }

    @Override
    protected Environment handleClientResponse(Environment env, JsonObject responseCode, String responseBody, JsonObject responseHeaders, JsonObject fullResponse) {
        env.putObject("resource_endpoint_response_full", fullResponse);
        env.putObject("resource_endpoint_response_headers", responseHeaders);
        logSuccess("Got a response from the enrollments endpoint", fullResponse);
        return env;
    }

    @Override
    @PreEnvironment(required = { "access_token", "resource", "resource_endpoint_request_headers" })
    @PostEnvironment(required = { "resource_endpoint_response_headers", "resource_endpoint_response_full" })
    public Environment evaluate(Environment env) {
        return callProtectedResource(env);
    }
}
