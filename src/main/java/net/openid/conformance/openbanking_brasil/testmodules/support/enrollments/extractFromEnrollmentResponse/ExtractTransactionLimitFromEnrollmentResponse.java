package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse;

public class ExtractTransactionLimitFromEnrollmentResponse extends AbstractExtractFromEnrollmentResponse {

	@Override
	protected String fieldName() {
		return "transactionLimit";
	}

	@Override
	protected String envVarName() {
		return "transaction_limit";
	}
}
