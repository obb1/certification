package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.AbstractCreditOperationsDiscontinuedCreditRightsApiXFapiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2.GetInvoiceFinancingsIdentificationV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2.GetInvoiceFinancingsListV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2.GetInvoiceFinancingsPaymentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2.GetInvoiceFinancingsScheduledInstalmentsV2n2OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.invoiceFinancings.v2n2.GetInvoiceFinancingsWarrantiesV2n2OASValidator;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "invoice-financings_api_x-fapi_test-module_v2-2",
	displayName = "Ensure that the x-fapi-interaction-id is required at the request for all endpoints",
	summary = "Ensure that the x-fapi-interaction-id is required at the request for all endpoints\n" +
		"• Call the POST Consents endpoint with the Credit Operations Permission Group\n" +
		"• Expect a 201 - Validate Response and ensure status is AWAITING_AUTHORISATION\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint\n" +
		"• Expects 200 - Validate response and ensure status is AUTHORISED\n" +
		"• Call the GET Contracts Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Contracts Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Contracts Endpoint with the x-fapi-interaction-id\n" +
		"• Expects 200 - Validate response and extract contractId\n" +
		"• Call the GET contracts/{contractId} Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET contracts/{contractId} Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Warranties Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Warranties Endpoint with invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Scheduled Installments Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Scheduled Installments Endpoint with an invalid x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure a valid x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Payments Endpoint without the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back\n" +
		"• Call the GET Payments Endpoint with an invalid the x-fapi-interaction-id\n" +
		"• Expects 400 - Validate error message ensure an x-fapi-interaction-id value was sent back",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class CreditOperationsDiscontinuedCreditRightsApiXFapiTestModuleV2n extends AbstractCreditOperationsDiscontinuedCreditRightsApiXFapiTestModule {

	@Override
	protected Class<? extends Condition> getContractsListValidator() {
		return GetInvoiceFinancingsListV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractIdentificationValidator() {
		return GetInvoiceFinancingsIdentificationV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractWarrantiesValidator() {
		return GetInvoiceFinancingsWarrantiesV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractScheduledInstalmentsValidator() {
		return GetInvoiceFinancingsScheduledInstalmentsV2n2OASValidator.class;
	}

	@Override
	protected Class<? extends Condition> getContractPaymentsValidator() {
		return GetInvoiceFinancingsPaymentsV2n2OASValidator.class;
	}

}
