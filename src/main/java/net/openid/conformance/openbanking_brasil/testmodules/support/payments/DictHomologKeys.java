package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

public class DictHomologKeys {
	public static final String PROXY_EMAIL = "cliente-a00001@pix.bcb.gov.br";
	public static final String PROXY_FAKE_EMAIL = "cliente-fake@pix.bcb.gov.br";
	public static final String PROXY_EMAIL_BANK_CODE = "998";
	public static final String PROXY_EMAIL_ACCOUNT_NUMBER = "12345678";
	public static final String PROXY_EMAIL_ACCOUNT_MISMATCHED_NUMBER = "12345679";
	public static final String PROXY_EMAIL_BRANCH_NUMBER = "0001";
	public static final String PROXY_EMAIL_ISPB = "99999004";
	public static final String PROXY_EMAIL_MISMATCHED_ISPB = "99999040";
	public static final String PROXY_EMAIL_CPF = "99991111140";
	public static final String PROXY_EMAIL_OWNER_NAME= "Joao Silva";
	public static final String PROXY_EMAIL_PERSON_TYPE = "PESSOA_NATURAL";
	public static final String PROXY_EMAIL_ACCOUNT_TYPE = "CACC";
	public static final String PROXY_EMAIL_STANDARD_TYPE = "PIX";
	public static final String PROXY_EMAIL_STANDARD_AMOUNT = "100.00";
	public static final String PROXY_EMAIL_STANDARD_CURRENCY = "BRL";
	public static final String PROXY_EMAIL_STANDARD_CURRENCY_CODE = "986";
	public static final String PROXY_EMAIL_STANDARD_LOCALINSTRUMENT = "DICT";
	public static final String PROXY_EMAIL_STANDARD_COUNTRY = "BR";
	public static final String PROXY_EMAIL_STANDARD_CITY = "BELO HORIZONTE";
	public static final String PROXY_EMAIL_STANDARD_IBGETOWNCODE = "5300108";
	public static final String PROXY_EMAIL_STANDARD_CODE = "OTHER";
	public static final String PROXY_EMAIL_STANDARD_ADDITIONALINFORMATION = "Conformance Suite Test";
	public static final String PROXY_EMAIL_STANDARD_REMITTANCEINFORMATION ="Conformance Suite Test";
	public static final String PROXY_QRDN_EDIT_PAYMENT_REMITTANCE_INFORMATION ="$$EDITPAYMENT$$:20201.00";
	public static final String PROXY_TRANSACTION_IDENTIFICATION = "E00038166201907261559y6j6";
	public static final String PROXY_PHONE_NUMBER = "+5561990010001";
	public static final String PROXY_PHONE_NUMBER_BANK_CODE = "998";
	public static final String PROXY_PHONE_NUMBER_ACCOUNT_NUMBER = "12345678";
	public static final String PROXY_PHONE_NUMBER_BRANCH_NUMBER = "0001";
	public static final String PROXY_PHONE_NUMBER_ISPB = "99999004";
	public static final String PROXY_PHONE_NUMBER_CPF = "11112222235";
	public static final String PROXY_PHONE_NUMBER_OWNER_NAME = "Joao Silva";
	public static final String PROXY_PHONE_NUMBER_PERSON_TYPE = "PESSOA_NATURAL";
	public static final String PROXY_PHONE_NUMBER_ACCOUNT_TYPE = "CACC";
	public static final String PROXY_CNPJ_INITIATOR = "44471172000119";
	public static final String PROXY_EMAIL_MISMATCHED_CPF = "09855612892";
	public static final String PROXY_TEMPORIZATION_EMAIL = "cliente-a00002@pix.bcb.gov.br";
	public static final String PROXY_TEMPORIZATION_EMAIL_NAME = "Joao Silva Silva";
	public static final String PROXY_TEMPORIZATION_EMAIL_PERSON_TYPE = "PESSOA_NATURAL";
	public static final String PROXY_TEMPORIZATION_EMAIL_CPF = "99992222263";
	public static final String PROXY_TEMPORIZATION_EMAIL_ACCOUNT_NUMBER = "11345678";
	public static final String PROXY_TEMPORIZATION_EMAIL_BANK_CODE = "998";
	public static final String PROXY_TEMPORIZATION_EMAIL_BRANCH_NUMBER = "0002";
	public static final String PROXY_TEMPORIZATION_EMAIL_ISPB = "99999004";
	public static final String PROXY_TEMPORIZATION_EMAIL_ACCOUNT_TYPE = "CACC";

	// The fields below are to be used on the production tests when sending real creditor details
	public static final String PROXY_PRODUCTION_PROXY = "lucca2c@hotmail.com";
	public static final String PROXY_PRODUCTION_NAME = "Lucca De Figueiredo Marques";
	public static final String PROXY_PRODUCTION_PERSON_TYPE = "PESSOA_NATURAL";
	public static final String PROXY_PRODUCTION_CPF = "12482514746";
	public static final String PROXY_PRODUCTION_ACCOUNT_NUMBER = "4735201";
	public static final String PROXY_PRODUCTION_BANK_CODE = "260";
	public static final String PROXY_PRODUCTION_BRANCH_NUMBER = "0001";
	public static final String PROXY_PRODUCTION_ISPB = "18236120";
	public static final String PROXY_PRODUCTION_ACCOUNT_TYPE = "TRAN";
}
