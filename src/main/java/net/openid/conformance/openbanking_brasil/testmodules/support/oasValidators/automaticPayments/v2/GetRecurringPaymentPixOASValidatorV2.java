package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2;

import org.springframework.http.HttpMethod;

public class GetRecurringPaymentPixOASValidatorV2 extends AbstractRecurringPaymentPixOASValidatorV2 {

	@Override
	protected String getEndpointPath() {
		return "/pix/recurring-payments/{recurringPaymentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
