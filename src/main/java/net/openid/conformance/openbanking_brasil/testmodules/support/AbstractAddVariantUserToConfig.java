package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.StringUtils;

/**
 * This class will replace "brazilCpf" and "brazilCnpj" with a variant configuration.
 */
public abstract class AbstractAddVariantUserToConfig extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String variantCpf = env.getString("config", String.format("resource.%s", getVariantUserKey()));

		if(StringUtils.isBlank(variantCpf)) {
			return env;
		}
		env.putString("config", "resource.brazilCpf", variantCpf);

		String variantCnpj = env.getString("config", String.format("resource.%s", getVariantBusinessKey()));
		if(StringUtils.isBlank(variantCnpj)) {
			// The participant informed a variant CPF, but not a variant CNPJ.
			// In that case, consider the test a personal one.
			JsonObject resource = env.getObject("config").get("resource").getAsJsonObject();
			resource.remove("brazilCnpj");
			return env;
		}
		env.putString("config", "resource.brazilCnpj", variantCnpj);

		return env;
	}

	protected abstract String getVariantUserKey();

	protected abstract String getVariantBusinessKey();
}
