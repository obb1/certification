package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument;

public class AddManuLocalInstrument extends AbstractAddLocalInstrument {

	@Override
	protected String localInstrument() {
		return "MANU";
	}
}
