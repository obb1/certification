package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule.AbstractAccountApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountTransactionsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsBalancesOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsIdentificationOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsLimitsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "accounts_api_core_consents_v3_test-module_v2-4",
	displayName = "Validate structure of all accounts API resources V2 with consents V3-2",
	summary = "Validates the structure of all account API resources V2 with consents V3-2\n" +
		"\u2022 Call the POST Consents API with will all of the existing permissions.\n" +
		"\u2022 Expect a 201 - Checks all of the fields sent on the consent API are specification compliant\n" +
		"\u2022 Redirect User to Authorize the Consent and obtain valid access token\n" +
		"\u2022 Call the GET Accounts List Endpoint\n" +
		"\u2022 Expects a 200 - Validate Response and extract the AccountId\n" +
		"\u2022 Call the GET Accounts {accountId} Endpoint\n" +
		"\u2022 Expects a 200 - Validate Response\n" +
		"\u2022 Call the GET Accounts {accountId} Balances Endpoint\n" +
		"\u2022 Expects a 200 - Validate Response\n" +
		"\u2022 Call the GET Accounts {accountId} Transactions Endpoint\n" +
		"\u2022 Expects a 200 - Validate Response\n" +
		"\u2022 Call the GET Accounts {accountId} overdraft-limits Endpoint\n" +
		"\u2022 Expects a 200 - Validate Response\n" +
		"\u2022 Call the DELETE Consents API\n" +
		"\u2022 Expects a 204\n"
		,
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class AccountApiConsentsV3TestModuleV2n extends AbstractAccountApiTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected Class<? extends Condition> getAccountListValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountTransactionsValidator() {
		return GetAccountTransactionsOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountIdentificationValidator() {
		return GetAccountsIdentificationOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountBalancesValidator() {
		return GetAccountsBalancesOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountLimitsValidator() {
		return GetAccountsLimitsOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
