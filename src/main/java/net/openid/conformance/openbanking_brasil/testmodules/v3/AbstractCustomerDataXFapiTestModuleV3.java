package net.openid.conformance.openbanking_brasil.testmodules.v3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddIpV4FapiCustomerIpAddressToResourceEndpointRequest;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.ExtractConsentIdFromConsentEndpointResponse;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.ensureConsentStatusWas.EnsureConsentWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFCategoryEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractCustomerDataXFapiTestModuleV3 extends AbstractPhase2TestModule {


	protected abstract boolean isConsents();
	protected abstract Class<? extends Condition> getResourceValidator();
	protected abstract OPFCategoryEnum getCategory();
	protected abstract OPFScopesEnum getScope();

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		super.onConfigure(config, baseUrl);
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(getScope(), OPFScopesEnum.OPEN_ID).addPermissionsBasedOnCategory(getCategory()).build();
	}

	@Override
	protected void setupResourceEndpoint() {
		env.putString("metaOnlyRequestDateTime", "true");
		if (isConsents()) {
			callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		} else {
			super.setupResourceEndpoint();
		}
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().mapKey("resource_endpoint_response", "consent_endpoint_response"))
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, condition(PostConsentOASValidatorV3n2.class).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure())
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, exec().unmapKey("resource_endpoint_response"));
	}


	protected void testConsents() {
		runInBlock("Calling GET consents v3 without x-fapi-interaction-id", () -> {
			env.removeObject("fapi_interaction_id");
			call(getGetConsentSequence()
				.skip(CreateRandomFAPIInteractionId.class, "not needed for this test")
				.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "not needed for this test"));

			callAndContinueOnFailure(EnsureConsentResponseCodeWas400.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(GetConsentOASValidatorV3n2.class, Condition.ConditionResult.FAILURE);
		});

		runInBlock("Calling GET consents v3 with x-fapi-interaction-id", () -> {
			call(getGetConsentSequence().then(getConsentValidationSequence()));
		});

		fireTestFinished();
	}


	@Override
	protected void performPostAuthorizationFlow() {
		runInBlock("Validating get consent response v3", () -> {
			call(getGetConsentSequence().then(getConsentValidationSequence()));
			callAndStopOnFailure(EnsureConsentWasAuthorised.class);
		});

		if (isConsents()) {
			testConsents();
		} else {
			super.performPostAuthorizationFlow();
		}
	}

	@Override
	protected void requestProtectedResource() {
		runInBlock("Calling resource endpoint without x-fapi-interaction-id", () -> {
			callAndStopOnFailure(CreateEmptyResourceEndpointRequestHeaders.class);
			callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
			callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
			callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
			callAndStopOnFailure(EnsureResourceResponseCodeWas400.class);
			callAndStopOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getResourceValidator(), Condition.ConditionResult.FAILURE);
		});

		runInBlock("Calling resource endpoint with an invalid x-fapi-interaction-id", () -> {
			env.putString("fapi_interaction_id", "a-bad-fapi-interaction-id");
			callAndStopOnFailure(AddFAPIInteractionIdToResourceEndpointRequest.class, "OIDCC-2.1.2");
			callAndStopOnFailure(AddFAPIAuthDateToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-3");
			callAndStopOnFailure(AddIpV4FapiCustomerIpAddressToResourceEndpointRequest.class, "FAPI1-BASE-6.2.2-4");
			callAndStopOnFailure(CallProtectedResource.class, "FAPI1-BASE-6.2.1-1", "FAPI1-BASE-6.2.1-3");
			callAndStopOnFailure(EnsureResourceResponseCodeWas400.class);
			callAndStopOnFailure(CheckForFAPIInteractionIdInResourceResponse.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(getResourceValidator(), Condition.ConditionResult.FAILURE);
		});

		super.requestProtectedResource();
	}

	@Override
	protected void validateResponse() {
		callAndStopOnFailure(getResourceValidator());
		eventLog.endBlock();
	}


	protected ConditionSequence getGetConsentSequence() {
		return sequenceOf(
			condition(PrepareToFetchConsentRequest.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
		);
	}

	protected ConditionSequence getConsentValidationSequence() {
		return sequenceOf(
			condition(GetConsentOASValidatorV3n2.class)
		);
	}
}
