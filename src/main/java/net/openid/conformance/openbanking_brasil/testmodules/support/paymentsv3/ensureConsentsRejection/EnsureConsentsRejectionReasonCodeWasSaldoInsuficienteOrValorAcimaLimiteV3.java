package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3.enums.PaymentConsentRejectionReasonEnumV3;

import java.util.List;

public class EnsureConsentsRejectionReasonCodeWasSaldoInsuficienteOrValorAcimaLimiteV3 extends AbstractEnsureConsentsRejectionReasonCodeWasX {

    @Override
    protected List<String> getExpectedRejectionCodes() {
        return List.of(PaymentConsentRejectionReasonEnumV3.SALDO_INSUFICIENTE.toString(), PaymentConsentRejectionReasonEnumV3.VALOR_ACIMA_LIMITE.toString());
    }
}
