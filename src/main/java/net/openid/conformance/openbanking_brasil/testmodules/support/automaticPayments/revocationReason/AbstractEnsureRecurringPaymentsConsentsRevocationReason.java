package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.revocationReason;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsRevocationReasonEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;

public abstract class AbstractEnsureRecurringPaymentsConsentsRevocationReason extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		try {
			String revocationReason = OIDFJSON.getString(
				BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.map(body -> body.getAsJsonObject().getAsJsonObject("data"))
					.map(data -> data.getAsJsonObject("revocation"))
					.map(revocation -> revocation.getAsJsonObject("reason"))
					.map(reason -> reason.get("code"))
					.orElseThrow(() -> error("Unable to find element data.revocation.reason.code in the response payload"))
			);
			if (revocationReason.equals(expectedRevocationReason().toString())) {
				logSuccess("revocation reason returned in the response matches the expected revocation reason");
			} else {
				throw error("revocation reason returned in the response does not match the expected revocation reason",
					args("revocation reason", revocationReason,
						"expected revocation reason", expectedRevocationReason().toString())
				);
			}
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
		return env;
	}

	protected abstract RecurringPaymentsConsentsRevocationReasonEnum expectedRevocationReason();
}
