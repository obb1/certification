package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.addLocalInstrument;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public abstract class AbstractAddLocalInstrument extends AbstractCondition {

	protected abstract String localInstrument();

	@Override
	@PostEnvironment(strings = "local_instrument")
	public Environment evaluate(Environment env) {
		env.putString("local_instrument", localInstrument());
		logSuccess("Added localInstrument to environment", args("localInstrument", localInstrument()));
		return env;
	}
}
