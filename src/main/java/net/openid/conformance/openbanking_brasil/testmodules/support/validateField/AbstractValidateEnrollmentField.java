package net.openid.conformance.openbanking_brasil.testmodules.support.validateField;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;

public abstract class AbstractValidateEnrollmentField extends AbstractValidateField {

	protected abstract int getEnrollmentVersion();

	@Override
	protected String getConsentRegex() {
		return String.format("^(https://)(.*?)(payments/v%d/consents)", getConsentVersion());
	}

	protected String getEnrollmentRegex() {
		return String.format("^(https://)(.*?)(enrollments/v%d/enrollments)", getEnrollmentVersion());
	}

	@Override
	protected void validateMainFields(JsonObject config) {
		super.validateMainFields(config);
		validateEnrollmentsUrl(config);
	}

	protected void validateEnrollmentsUrl(JsonObject config) {
		JsonElement enrollmentElement = findElementOrThrowError(config, "$.resource.enrollmentsUrl");
		String enrollments = OIDFJSON.getString(enrollmentElement);
		String regexValidator = getEnrollmentRegex();
		if (!enrollments.matches(regexValidator)) {
			throw error(String.format("enrollmentsUrl does not match the regex %s", regexValidator));
		}
	}
}
