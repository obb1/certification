package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createPatchConsentEdition;

import com.google.gson.JsonObject;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class CreatePatchRecurringConsentsForEditionRequestBodyExpirationTomorrowNoAmountNoRiskSignals extends AbstractCreatePatchRecurringConsentsForEditionRequestBody {

	@Override
	protected String getExpirationDateTime() {
		LocalDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
		LocalDateTime dateTime = currentDateTime.plusDays(1);
		return dateTime.format(DATE_TIME_FORMATTER);
	}

	@Override
	protected String getMaxVariableAmount() {
		return null;
	}

	@Override
	protected JsonObject getRiskSignals() {
		return null;
	}
}
