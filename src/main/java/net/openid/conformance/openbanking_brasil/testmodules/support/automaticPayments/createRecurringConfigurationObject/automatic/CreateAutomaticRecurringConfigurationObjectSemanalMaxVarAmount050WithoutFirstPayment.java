package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;

public class CreateAutomaticRecurringConfigurationObjectSemanalMaxVarAmount050WithoutFirstPayment extends AbstractCreateAutomaticRecurringConfigurationObject {

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected String getFixedAmount() {
		return null;
	}

	@Override
	protected boolean hasFirstPayment() {
		return false;
	}

	@Override
	protected String getMaximumVariableAmount() {
		return "0.50";
	}
}
