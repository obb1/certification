package net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.abstractmodule.AbstractAccountsApiOperationalLimitsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountTransactionsCurrentOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountTransactionsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsBalancesOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsIdentificationOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsLimitsOASValidatorV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.accounts.v2n4.GetAccountsListOASValidatorV2n4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "accounts_api_operational-limits_test-module_v2-4",
	displayName = "Accounts Api operational limits test module",
	summary = "This test will require the user to have set at least one ACTIVE resources each with at least 20 Transactions to be returned on the transactions endpoint. The tested resource must be sent on the field FirstAccountId (R_1) on the test configuration and on SecondAccountId (R_2), in case the company supports multiple accounts for the same user. If the server does not provide the SecondAccountId (R_2) the test will skip this part of the test \n" +
		"Make Sure that the fields “Client_id for Operational Limits Test” (client_id for OL), the CPF for Operational Limits (CPF for OL) and at least the FirstAccountId (R_1) have been provided \n" +
		"\u2022 Using the HardCoded clients provided on the test summary link, use the client_id for OL and the CPF/CNPJ for OL passed on the configuration and create a Consent Request sending the Accounts permission group - Expect Server to return a 201 - Save ConsentID (1) \n" +
		"\u2022 Return a Success if Consent Response is a 201 containing all permissions required on the scope of the test. Return a Warning and end the test if the consent request returns either a 422 or a 201 without Permission for this specific test.\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts API with the saved Resource ID (R_1) 4 Times - Expect a 200 response\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Transactions API with the saved Resource ID (R_1) 4 Times, send query parameters fromBookingDate as D-6 and toBookingDate as Today - Expect a 200 response - Make Sure That at least 20 Transactions have been returned \n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Balances API with the saved Resource ID (R_1) 420 Times  - Expect a 200 response. Every 100 calls the Access token is refreshed.\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Limits API with the saved Resource ID (R_1) 420 Times  - Expect a 200 response. Every 100 calls the Access token is refreshed.\n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Transactions-Current API with the saved Resource ID (R_1) 239 Times  - Expect a 200 response. Every 100 calls the Access token is refreshed.\n" +
		"\u2022 With the authorized consent id (1), refresh Access Token using the Refresh Token" +
		"\u2022 With the authorized consent id (1), call the GET Accounts Transactions-Current API with the saved Resource ID (R_1) one more time, send query parameters fromBookingDate as D-6 and toBookingDate as Today, with page-size=1 - Expect a 200 response - Fetch the links.next URI \n" +
		"\u2022 Call the GET Accounts Transactions-Current API 19 more times, always using the links.next returned and always forcing the page-size to be equal to 1 - Expect a 200 on every single call \n" +
		"\u2022 With the authorized consent id (1), call the GET Accounts API with the saved Resource ID (R_2) once - Expect a 200 response \n" +
		"\u2022 If the field SecondAccountId (R_2) has been returned, repeat  the exact same process done with the first tested resources (R_1) but now, execute it against the second returned Resource (R_2) \n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpfOperational",
		"resource.brazilCnpjOperationalBusiness",
		"resource.first_account_id",
		"resource.second_account_id"
	}
)
public class AccountsApiOperationalLimitsTestModuleV24n extends AbstractAccountsApiOperationalLimitsTestModulePhase2 {

	@Override
	protected Class<? extends Condition> getAccountListValidator() {
		return GetAccountsListOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountIdentificationResponseValidator() {
		return GetAccountsIdentificationOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountTransactionsValidator() {
		return GetAccountTransactionsOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountTransactionsCurrentValidator() {
		return GetAccountTransactionsCurrentOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountBalancesResponseValidator() {
		return GetAccountsBalancesOASValidatorV2n4.class;
	}

	@Override
	protected Class<? extends Condition> getAccountLimitsValidator() {
		return GetAccountsLimitsOASValidatorV2n4.class;
	}
}

