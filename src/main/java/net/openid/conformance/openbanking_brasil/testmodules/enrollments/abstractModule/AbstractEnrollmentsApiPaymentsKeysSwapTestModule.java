package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateClientExtensionResults;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SaveKeyPair;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults.AbstractEditRequestBodyToAddClientExtensionResults;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults.EditConsentsAuthoriseRequestBodyToAddClientExtensionResults;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.editRequestBodyToAddClientExtensionResults.EditFidoRegistrationRequestBodyToAddClientExtensionResults;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateKeyPairFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.LoadOldKidToKeyPair;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SaveKid;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasRisco;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasNaoInformadoV3;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractEnrollmentsApiPaymentsKeysSwapTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	@Override
	protected void postAndValidateFidoRegistrationOptions() {
		super.postAndValidateFidoRegistrationOptions();
		generateClientExtensionResults();
		callAndStopOnFailure(SaveResponse.class);
	}

	@Override
	protected void postAndValidateSignOptions() {
		super.postAndValidateSignOptions();
		generateClientExtensionResults();
	}

	@Override
	protected PostEnrollmentsResourceSteps createPostFidoRegistrationSteps() {
		PostEnrollmentsResourceSteps steps = super.createPostFidoRegistrationSteps();
		if (editFidoRegistrationRequestBody() != null) {
			steps.insertBefore(SignEnrollmentsRequest.class, condition(editFidoRegistrationRequestBody()));
		}
		return steps;
	}

	@Override
	protected ConditionSequence createConsentsAuthoriseSteps() {
		ConditionSequence steps = super.createConsentsAuthoriseSteps();
		if (editConsentsAuthoriseRequestBody() != null) {
			steps.insertBefore(SignEnrollmentsRequest.class, condition(editConsentsAuthoriseRequestBody()));
		}
		return steps;
	}

	protected void generateClientExtensionResults() {
		callAndStopOnFailure(GenerateClientExtensionResults.class);
	}

	protected Class<? extends AbstractEditRequestBodyToAddClientExtensionResults> editFidoRegistrationRequestBody() {
		return EditFidoRegistrationRequestBodyToAddClientExtensionResults.class;
	}

	protected Class<? extends AbstractEditRequestBodyToAddClientExtensionResults> editConsentsAuthoriseRequestBody() {
		return EditConsentsAuthoriseRequestBodyToAddClientExtensionResults.class;
	}

	@Override
	protected void postAndValidateConsentAuthorise() {
		runInBlock("Post consents authorise, sign challenge with wrong private key - Expects 422 RISCO", () -> {
			env.putBoolean("is_client_data_registration", false);
			callAndStopOnFailure(GenerateFidoClientData.class);
			callAndStopOnFailure(LoadOldResponse.class);
			callAndStopOnFailure(SaveKeyPair.class);
			callAndStopOnFailure(SaveKid.class);
			callAndStopOnFailure(GenerateKeyPairFromFidoRegistrationOptionsResponse.class);
			callAndStopOnFailure(LoadOldKidToKeyPair.class);
			call(createConsentsAuthoriseSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasRisco.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasRisco.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasRisco.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void getConsentAfterConsentAuthorise() {
		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		runInBlock("Validate consent rejection reason", () -> {
			callAndStopOnFailure(EnsureConsentsRejectionReasonCodeWasNaoInformadoV3.class);
		});
	}

	@Override
	protected void configureDictInfo() {}

	@Override
	protected void postAndValidatePayment() {}

	@Override
	protected void executeFurtherSteps() {}
}
