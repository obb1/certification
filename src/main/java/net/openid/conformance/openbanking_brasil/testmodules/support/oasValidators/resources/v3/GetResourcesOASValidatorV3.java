package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.resources.v3;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import net.openid.conformance.util.field.ExtraField;
import org.springframework.http.HttpMethod;

public class GetResourcesOASValidatorV3 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/resources/resources-v3.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/resources";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

	@Override
	protected ExtraField getExtraField() {
		return new ExtraField.Builder()
			.setPattern("^([A-Z]{4})(-)(.*)$")
			.build();
	}
}
