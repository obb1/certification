package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1;

import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

public class GetBankFixedIncomesBalancesOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/openBanking/bank-fixed-incomes/bank-fixed-incomes-v1.0.0.yml";
	}

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/balances";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}

}
