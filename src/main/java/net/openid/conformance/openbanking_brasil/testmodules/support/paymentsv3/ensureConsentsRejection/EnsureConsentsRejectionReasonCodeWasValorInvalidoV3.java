package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensureConsentsRejection;

import net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3.enums.PaymentConsentRejectionReasonEnumV3;

import java.util.List;

public class EnsureConsentsRejectionReasonCodeWasValorInvalidoV3 extends AbstractEnsureConsentsRejectionReasonCodeWasX {

    @Override
    protected List<String> getExpectedRejectionCodes() {
        return List.of(PaymentConsentRejectionReasonEnumV3.VALOR_INVALIDO.toString());
    }
}
