package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionCurrentTimezoneTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddToAndFromTransactionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckAllTransactionsDates;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.CheckTransactionDateRange;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo6DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetFromTransactionDateTo7DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionDateConditions.SetToTransactionDateToToday;

public abstract class AbstractBankFixedIncomesApiTransactionsCurrentTimezoneTest extends AbstractInvestmentsApiTransactionsCurrentTimezoneTestModule {

	@Override
	protected Class<? extends Condition> setFromTransactionTo6DaysAgo() {
		return SetFromTransactionDateTo6DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> setToTransactionToToday() {
		return SetToTransactionDateToToday.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo7DaysAgo() {
		return SetFromTransactionDateTo7DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
		return AddToAndFromTransactionDateParametersToProtectedResourceUrl.class;
	}

	@Override
	protected Class<? extends Condition> checkTransactionRange() {
		return CheckTransactionDateRange.class;
	}

	@Override
	protected Class<? extends Condition> checkAllDates() {
		return CheckAllTransactionsDates.class;
	}

	@Override
	protected String transactionParameterName() {
		return "TransactionDate";
	}

}
