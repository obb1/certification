package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensureErrorResponseCode;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.v2.PatchRecurringConsentsErrorResponseCodeEnumV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasFaltamSinaisObrigatoriosPlataforma extends AbstractEnsureErrorResponseCodeFieldWas {

	@Override
	protected List<String> getExpectedCodes() {
		return List.of(PatchRecurringConsentsErrorResponseCodeEnumV2.FALTAM_SINAIS_OBRIGATORIOS_PLATAFORMA.toString());
	}
}
