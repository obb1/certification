package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateAuthServerCertifications extends AbstractCondition {
	List<String> failureMessages = new ArrayList<>();
	boolean isFailure = false;

	@Override
	@PreEnvironment(required = {"authorisation_server"})
	public Environment evaluate(Environment env) {
		JsonObject authorisationServer = env.getObject("authorisation_server");
		JsonArray authServerCertifications = authorisationServer.getAsJsonArray("AuthorisationServerCertifications");
		if (authServerCertifications == null || authServerCertifications.isEmpty()) {
			throw error("AuthorisationServerCertifications not found");
		}
		JsonArray activeCertifications = new JsonArray();

		authServerCertifications.forEach(certification -> {
			String certificationExpirationDate = OIDFJSON.getString(certification.getAsJsonObject().get("CertificationExpirationDate"));
			LocalDate expirationDate = LocalDate.parse(certificationExpirationDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			if (isCertificationActive(expirationDate)) {
				activeCertifications.add(certification);
			}
		});
		if (activeCertifications.isEmpty()) {
			throw error("No active certifications found");
		}
		logSuccess("Found active certifications", args("active_certifications", activeCertifications));

		validateCertExists(AuthServerCertificationsEnum.BR_OF_ADV_OP_WITH_PRIVATE_KEY_PAR_FAPI_BR_V2.getValue(), activeCertifications);

		if (isFailure){
			throw error(String.join(";", failureMessages));
		}

		return env;
	}

	protected boolean isCertificationActive(LocalDate expirationDate) {
		return !expirationDate.isBefore(LocalDate.now().minusDays(60));
	}

	protected boolean isStringInsideJsonArray(String valueToFind, JsonArray jsonArray){
		for (JsonElement jsonElement : jsonArray) {
			if (JsonPrimitive.class.isAssignableFrom(jsonElement.getClass()) && OIDFJSON.getString(jsonElement).equals(valueToFind)) {
				return true;
			} else if (JsonObject.class.isAssignableFrom(jsonElement.getClass())) {
				JsonObject jsonObject = jsonElement.getAsJsonObject();
				Set<String> jsonObjectKeys = jsonObject.keySet();
				for (String jsonObjectKey : jsonObjectKeys) {
					if (JsonPrimitive.class.isAssignableFrom(jsonObject.get(jsonObjectKey).getClass()) && jsonObject.get(jsonObjectKey).getAsJsonPrimitive().isString() && OIDFJSON.getString(jsonObject.get(jsonObjectKey)).equals(valueToFind)){
						return true;
					}
				}
			}
		}
		return false;
	}

	protected void validateCertificationUri(JsonArray jsonArray, String profileName) {
		for (JsonElement jsonElement : jsonArray) {
			JsonObject certification = jsonElement.getAsJsonObject();
			if (OIDFJSON.getString(certification.get("ProfileVariant")).equals(profileName)){
				String regex = "(https:\\/\\/openid\\.net\\/(.*)\\/uploads\\/)((\\d{4}))(\\/)(\\d{2})(.*)(.zip)";
				String uriMonth = getValueFromRegexGroup(OIDFJSON.getString(certification.get("CertificationURI")), regex, 6);
				String uriYear = getValueFromRegexGroup(OIDFJSON.getString(certification.get("CertificationURI")), regex, 3);
				LocalDate date = LocalDate.parse(OIDFJSON.getString(certification.get("CertificationStartDate")), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				if (uriMonth != null && uriYear != null){
					validateCertUriDate(date, Integer.parseInt(uriMonth), Integer.parseInt(uriYear),jsonElement.getAsJsonObject());
				}
			}
		}
	}
	protected void validateCertExists(String certificationToFind, JsonArray jsonArray){
		if (!isStringInsideJsonArray(certificationToFind,jsonArray)){
			isFailure = true;
			String failureMessage = String.format("Security certification not found: certification: %s", certificationToFind);
			failureMessages.add(failureMessage);
			logFailure("Security certification not found", args("certification", certificationToFind, "certifications", jsonArray));
		} else if (isStringInsideJsonArray(certificationToFind,jsonArray)){
			logSuccess("Security certification found", args("certification", certificationToFind, "certifications", jsonArray));
			validateCertificationUri(jsonArray, certificationToFind);
		}
	}

	protected String getValueFromRegexGroup(String path, String regex, int groupNumber) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(path);
		if (matcher.matches()){
			logSuccess("ApiCertificationUri Regex matches", args("regex", regex, "actual", path));
			return matcher.group(groupNumber);
		}
		isFailure = true;
		String failureMessage = String.format("ApiCertificationUri Regex does not match: regex: %s, actual: %s", regex, path);
		failureMessages.add(failureMessage);
		logFailure("ApiCertificationUri Regex does not match", args("regex", regex, "actual", path));
		return null;
	}

	private void validateCertUriDate(LocalDate apiCertificationStartDate, int monthFromUri, int yearFromUri,JsonObject authServerCert) {
		String formattedCertificationStartDate = apiCertificationStartDate.format(DateTimeFormatter.ofPattern("MM-yyyy"));
		YearMonth uriYearMonth = YearMonth.of(yearFromUri, monthFromUri);
		YearMonth certificationStartDate = YearMonth.from(apiCertificationStartDate);
		YearMonth uriYearMonthPlusOne = uriYearMonth.plusMonths(1);
		if (certificationStartDate.isBefore(uriYearMonth) || certificationStartDate.equals(uriYearMonth) || certificationStartDate.isBefore(uriYearMonthPlusOne) || certificationStartDate.equals(uriYearMonthPlusOne)){
			logSuccess("ApiCertificationUri registration date is valid", args("month_from_uri", monthFromUri,"year_from_uri",yearFromUri,"api_resource",authServerCert,"date_from_certification_start_date", formattedCertificationStartDate));
		} else {
			isFailure = true;
			String failureMessage = String.format("ApiCertificationUri registration date is invalid, CertificationStartDate is equal to dateFromUri month or dateFromUri month +1 or year is incorrect: date_from_certification_start_date: %s CertificationId: %s" ,formattedCertificationStartDate, OIDFJSON.getString(authServerCert.get("CertificationId")));
			failureMessages.add(failureMessage);
			logFailure("ApiCertificationUri registration date is invalid, CertificationStartDate is equal to dateFromUri month or dateFromUri month +1 or year is incorrect", args("month_from_uri", monthFromUri,"year_from_uri",yearFromUri,"api_resource",authServerCert,"date_from_certification_start_date", formattedCertificationStartDate));
		}
	}
}
