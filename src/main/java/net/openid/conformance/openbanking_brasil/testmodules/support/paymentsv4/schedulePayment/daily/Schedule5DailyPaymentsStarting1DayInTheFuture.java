package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily;

public class Schedule5DailyPaymentsStarting1DayInTheFuture extends AbstractScheduleDailyPayment {

	@Override
	protected int getNumberOfDaysToAddToCurrentDate() {
		return 1;
	}

	@Override
	protected int getQuantity() {
		return 5;
	}
}
