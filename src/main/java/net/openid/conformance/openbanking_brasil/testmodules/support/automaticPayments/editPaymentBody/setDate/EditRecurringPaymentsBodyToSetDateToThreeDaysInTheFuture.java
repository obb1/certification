package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.setDate;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class EditRecurringPaymentsBodyToSetDateToThreeDaysInTheFuture extends AbstractEditRecurringPaymentsBodyToSetDate {

	@Override
	protected String getDate() {
		LocalDate futureDate = LocalDate.now(ZoneId.of("UTC-3")).plusDays(3);
		return futureDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
}
