package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromOBResourceConfiguration;
import net.openid.conformance.condition.client.ExtractTLSTestValuesFromResourceConfiguration;
import net.openid.conformance.condition.client.GetResourceEndpointConfiguration;
import net.openid.conformance.condition.client.SetProtectedResourceUrlToSingleResourceEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractOBBrasilPaymentFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetProtectedResourceUrlToPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddAutomaticPaymentToTheResource;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.AddRecurringPaymentResourceUrlToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetProtectedResourceUrlToRecurringPaymentsEndpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.MoveStartDateTimeFromSweepingToRoot;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editPaymentBody.AddRecurringConsentIdToPaymentBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.AbstractSetAutomaticPaymentAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount.SetAutomaticPaymentAmountTo300;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.validateVersionHeader.ValidateVersionHeader2d0d0;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateWellKnownUriSteps;
import net.openid.conformance.sequence.ConditionSequence;



public abstract class AbstractAutomaticPaymentsFunctionalTestModule extends AbstractOBBrasilPaymentFunctionalTestModule {

	protected abstract boolean isNewerVersion();

	@Override
	protected void configureClient() {
		call(new ValidateWellKnownUriSteps());
		call(new ValidateRegisteredEndpoints(getConsentAndResourceEndpointSequence()));
		super.configureClient();
	}

	protected abstract AbstractCreateRecurringConfigurationObject consentBodyCondition();

	@Override
	protected OPFScopesEnum returnScope() {
		return OPFScopesEnum.RECURRING_PAYMENTS;
	}

	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(AddRecurringPaymentResourceUrlToConfig.class);
		callAndStopOnFailure(consentBodyCondition().getClass());
		callAndStopOnFailure(AddRecurringPaymentConsentRequestBodyToConfig.class);
		keepOldStartDateTime();
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
		callAndStopOnFailure(SetProtectedResourceUrlToSingleResourceEndpoint.class);

		callAndStopOnFailure(ExtractTLSTestValuesFromResourceConfiguration.class);
		callAndContinueOnFailure(ExtractTLSTestValuesFromOBResourceConfiguration.class, Condition.ConditionResult.INFO);
	}

	protected void keepOldStartDateTime() {
		if (!isNewerVersion()) {
			callAndStopOnFailure(MoveStartDateTimeFromSweepingToRoot.class);
		}
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(addAutomaticPaymentToTheResource());
		callAndStopOnFailure(setAutomaticPaymentAmount().getClass());
		callAndStopOnFailure(PrepareToPostConsentRequest.class);
		callAndStopOnFailure(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class);
		configureDictInfo();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		validateVersionHeader();
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
		fetchConsentToCheckStatus("AWAITING_AUTHORISATION", new EnsurePaymentConsentStatusWasAwaitingAuthorisation());
		validateGetConsentResponse();
		updateAutomaticPaymentsRequestBody();
	}

	@Override
	protected void validateGetConsentResponse() {
		super.validateGetConsentResponse();
		validateVersionHeader();
	}

	@Override
	protected ConditionSequence postPaymentValidationSequence() {
		ConditionSequence sequence = super.postPaymentValidationSequence();
		if (isNewerVersion()) {
			sequence.insertAfter(postPaymentValidator(), condition(ValidateVersionHeader2d0d0.class));
		}
		return sequence;
	}

	@Override
	protected ConditionSequence getPaymentValidationSequence() {
		ConditionSequence sequence = super.getPaymentValidationSequence();
		if (isNewerVersion()) {
			sequence.insertAfter(getPaymentValidator(), condition(ValidateVersionHeader2d0d0.class));
		}
		return sequence;
	}

	protected void updateAutomaticPaymentsRequestBody() {
		callAndStopOnFailure(AddRecurringConsentIdToPaymentBody.class);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilAutomaticPaymentsPreAuthorizationSteps(
			addTokenEndpointClientAuthentication,
			false
		);
	}

	@Override
	protected void fetchConsentToCheckStatus(String status, AbstractEnsurePaymentConsentStatusWas statusCondition) {
		runInBlock(String.format("Checking the created consent - Expecting %s status", status), () -> {
			callAndStopOnFailure(AddJWTAcceptHeader.class);
			callAndStopOnFailure(ExpectJWTResponse.class);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(statusCondition.getClass(), Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return super.getPixPaymentSequence()
			.replace(SetProtectedResourceUrlToPaymentsEndpoint.class, condition(SetProtectedResourceUrlToRecurringPaymentsEndpoint.class));
	}

	protected Class<? extends Condition> addAutomaticPaymentToTheResource() {
		return AddAutomaticPaymentToTheResource.class;
	}

	protected AbstractSetAutomaticPaymentAmount setAutomaticPaymentAmount() {
		return new SetAutomaticPaymentAmountTo300();
	}

	protected void validateVersionHeader() {
		if (isNewerVersion()) {
			callAndContinueOnFailure(ValidateVersionHeader2d0d0.class, Condition.ConditionResult.FAILURE);
		}
	}

	@Override
	protected void configureDictInfo() {}

	protected abstract ConditionSequence getConsentAndResourceEndpointSequence();

}
