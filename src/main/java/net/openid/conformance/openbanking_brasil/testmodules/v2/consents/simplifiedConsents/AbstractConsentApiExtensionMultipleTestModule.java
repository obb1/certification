package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;


import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetResourcesV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.ValidateRegisteredEndpoints;


public abstract class AbstractConsentApiExtensionMultipleTestModule extends AbstractSimplifiedConsentRenewalTestModule {

	@Override
	protected abstract Class<? extends Condition> setConsentCreationValidation();

	@Override
	protected abstract Class<? extends Condition> setGetConsentValidator();

	@Override
	protected abstract Class<? extends Condition> setPostConsentExtensionValidator();

	@Override
	protected abstract Class<? extends Condition> setGetConsentExtensionValidator();

	protected boolean secondExtension = false;

	protected boolean thirdExtension = false;


	@Override
	protected Class<? extends Condition> setExtensionExpirationTime() {
		if (secondExtension && !thirdExtension) {
			thirdExtension = true;
			return CreateExtensionRequestTimeDayPlus180.class;
		} else if (thirdExtension) {
			return(thirdExtensionExpiryTimeCondition());
		} else {
			secondExtension = true;
			return CreateExtensionRequestTimeDayPlus90.class;
		}
	}


	@Override
	public void callExtensionEndpoints(){
		runInBlock("Validating post consent extension response", this::callPostConsentExtensionEndpoint);

		runInBlock("Validating post consent extension response", this::callPostConsentExtensionEndpoint);

		runInBlock("Validating post consent extension response", this::callPostConsentExtensionEndpoint);

		runInBlock("Validating get consent extension response", this::callGetConsentExtensionEndpoint);

		runInBlock("Validating get consent response", this::callGetConsentEndpoint);
		validateExpirationTimeReturned();
	}

	protected Class<? extends Condition> thirdExtensionExpiryTimeCondition() {
		return CreateEmptyConsentExpiryTime.class;
	}

	@Override
	protected void configureClient(){
		env.putString("metaOnlyRequestDateTime", "true");
		super.configureClient();
	}

	@Override
	protected void callGetConsentAndOrResourceUrlSequence(){
		call(new ValidateRegisteredEndpoints(
				sequenceOf(
					condition(GetConsentV3Endpoint.class),
					condition(GetResourcesV3Endpoint.class)
				)
			)
		);
	}

	@Override
	protected Class<? extends Condition> validateExpirationTimeReturned(){
		return ValidateExtensionExpiryTimeInConsentResponse.class;
	}

	@Override
	protected Class<? extends Condition> validateGetConsentExtensionEndpointResponse() {
		return ValidateExtensionExpiryTimeInGetSizeThree.class;
	}

	@Override
	protected Class<? extends Condition> buildConsentRequestBody() {
		return FAPIBrazilOpenBankingCreateConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setConsentExpirationTime() {
		return FAPIBrazilAddExpirationToConsentRequest.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionEndpoint() {
		return PrepareToGetConsentExtensions.class;
	}

}
