package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.loans;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

import java.util.List;


public abstract class AbstractGetLoansScheduledInstalmentsOASValidator extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getEndpointPath() {
		return "/contracts/{contractId}/scheduled-instalments";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}


	@Override
	protected void assertSchemaSuccessfulResponseAdditionalConstraints(JsonObject body) {
		assertData(body.getAsJsonObject("data"));
	}

	private void assertData(JsonObject data) {
		assertField1IsRequiredWhenField2HasValue2(
			data,
			"totalNumberOfInstalments",
			"typeNumberOfInstalments",
			List.of("DIA", "SEMANA", "MES", "ANO")
		);

		assertField1IsRequiredWhenField2HasNotValue2(
			data,
			"totalNumberOfInstalments",
			"typeContractRemaining",
			"SEM_PRAZO_REMANESCENTE"
		);

	}
}
