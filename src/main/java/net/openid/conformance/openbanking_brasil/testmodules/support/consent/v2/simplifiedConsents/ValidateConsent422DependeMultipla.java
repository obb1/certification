package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

public class ValidateConsent422DependeMultipla extends AbstractValidateConsent422ErrorCode {

	@Override
	protected String getErrorCode() {
		return "DEPENDE_MULTIPLA_ALCADA";
	}
}
