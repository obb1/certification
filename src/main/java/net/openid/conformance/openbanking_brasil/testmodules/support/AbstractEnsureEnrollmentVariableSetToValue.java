package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.extractFromEnrollmentResponse.AbstractExtractFromEnrollmentResponse;
import net.openid.conformance.testmodule.Environment;

import java.util.Map;

public abstract class AbstractEnsureEnrollmentVariableSetToValue extends AbstractExtractFromEnrollmentResponse {

	protected abstract String value();
	@Override
	public Environment evaluate(Environment env) {
		super.evaluate(env);
		if(!env.getString(envVarName()).equalsIgnoreCase(value()))
		{
			throw error("Value is incorrect",Map.of("Variable name",envVarName(),"Expected value",value()));
		}
		logSuccess("Value is correct",Map.of("Variable name",envVarName(),"Expected value",value()));

		return env;
	}
}
