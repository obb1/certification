package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.EnsureDebtorAccountNumberIsStillInvalid;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToChangeDebtorAccountNumber;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasContaDebitoDivergenteConsentimentoVinculo;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentConsentDebtorAccountNumber;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.ensureConsentsRejection.EnsureConsentsRejectionReasonCodeWasContaNaoPermitePagamentoV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsSteps;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractEnrollmentsApiDebtorAccountTestModule extends AbstractEnrollmentsApiPaymentsCoreTestModule {

	@Override
	protected PostEnrollmentsSteps createPostEnrollmentsSteps() {
		return (PostEnrollmentsSteps) super.createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToChangeDebtorAccountNumber.class));
	}

	@Override
	protected void postAndValidateEnrollments() {
		super.postAndValidateEnrollments();
		callAndContinueOnFailure(EnsureDebtorAccountNumberIsStillInvalid.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return super.createOBBPreauthSteps()
			.insertBefore(FAPIBrazilCreatePaymentConsentRequest.class, condition(SetPaymentConsentDebtorAccountNumber.class));
	}

	@Override
	protected void postAndValidateConsentAuthorise() {
		runInBlock("Post consents authorise - Expects 422 CONTA_DEBITO_DIVERGENTE_CONSENTIMENTO_VINCULO", () -> {
			env.putBoolean("is_client_data_registration", false);
			callAndStopOnFailure(GenerateFidoClientData.class);
			call(createConsentsAuthoriseSteps());
			callAndContinueOnFailure(postConsentsAuthoriseValidator(), Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasContaDebitoDivergenteConsentimentoVinculo.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasContaDebitoDivergenteConsentimentoVinculo.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasContaDebitoDivergenteConsentimentoVinculo.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void getConsentAfterConsentAuthorise() {
		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		callAndContinueOnFailure(EnsureConsentsRejectionReasonCodeWasContaNaoPermitePagamentoV4.class, Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void postAndValidatePayment() {}

	@Override
	protected void executeFurtherSteps() {}

	@Override
	protected void configureDictInfo() {}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return null;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return null;
	}

	protected abstract Class<? extends Condition> postConsentsAuthoriseValidator();
}
