package net.openid.conformance.openbanking_brasil.testmodules.support.consent.v2.simplifiedConsents;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public abstract class AbstractValidateConsentExpirationTimeReturned extends AbstractCondition {
	@Override
	@PreEnvironment(required = "consent_endpoint_response_full")
	public Environment evaluate(Environment env) {

		JsonObject body;
		try {
			body = OIDFJSON.toObject(
				BodyExtractor.bodyFrom(env, "consent_endpoint_response_full")
					.orElseThrow(() -> error("Could not extract body from response"))
			);
		} catch (ParseException e) {
			throw error("Could not parse body");
		}

		String expectedConsentExpirationTime = env.getString(getExpectedConsentExpirationTimeEnvKey());
		if (Strings.isNullOrEmpty(expectedConsentExpirationTime)) {
			throw error("Expected consent expiration time not found in environment");
		}

		String consentExpirationTime = OIDFJSON.getString(Optional.ofNullable(body.get("data").getAsJsonObject().get("expirationDateTime"))
			.orElseThrow(() -> error("Couldn't find data.expirationDateTime in the consent endpoint response")));

		if (!consentExpirationTime.equals(expectedConsentExpirationTime)) {
			throw error("Consent expiration time is not the expected value", args("expected", expectedConsentExpirationTime, "actual", consentExpirationTime));
		}
		logSuccess("Consent expiration time is " + expectedConsentExpirationTime);
		return env;
	}


	protected abstract String getExpectedConsentExpirationTimeEnvKey();
}
