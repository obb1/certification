package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;

public class ExtractPaymentIdFromLastPayment extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	@PostEnvironment(strings = "payment_id")
	public Environment evaluate(Environment env) {
		JsonObject body;
		try {
			body = BodyExtractor.bodyFrom(env, "resource_endpoint_response_full")
				.orElseThrow(() -> error("Could not extract body from response"))
				.getAsJsonObject();
		} catch (ParseException e) {
			throw error("Could not parse the response body");
		}

		JsonArray data = body.getAsJsonArray("data");
		JsonObject lastPayment = data.get(data.size() - 1).getAsJsonObject();
		String paymentId = OIDFJSON.getString(
			Optional.ofNullable(lastPayment.get("paymentId"))
				.orElseThrow(() -> error("Could not extract payment ID from last payment"))
		);
		env.putString("payment_id", paymentId);
		logSuccess("Extracted payment ID", args("payment_id", paymentId));

		return env;
	}
}
