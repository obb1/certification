package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoAttestationObjectWithInvalidPublicKey;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateKeyPairFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas.EnsureErrorResponseCodeFieldWasPublicKeyInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas422;

public abstract class AbstractEnrollmentsApiInvalidPublicKeyTestModule extends AbstractCompleteEnrollmentsApiEnrollmentTestModule {

	@Override
	protected void executeTestSteps() {}

	@Override
	protected void postAndValidateFidoRegistration() {
		runInBlock("Call POST fido-registration with invalid credentialPublicKey - Expects 422 PUBLIC_KEY_INVALIDA", () -> {
			userAuthorisationCodeAccessToken();
			prepareEnvironmentForPostFidoRegistration();
			call(createPostFidoRegistrationSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(postFidoRegistrationValidator(), Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeFieldWasPublicKeyInvalida.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeFieldWasPublicKeyInvalida.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeFieldWasPublicKeyInvalida.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void prepareEnvironmentForPostFidoRegistration() {
		callAndStopOnFailure(GenerateFidoClientData.class);
		callAndStopOnFailure(GenerateKeyPairFromFidoRegistrationOptionsResponse.class);
		callAndStopOnFailure(GenerateFidoAttestationObjectWithInvalidPublicKey.class);
	}

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
	}

	protected abstract Class<? extends Condition> postFidoRegistrationValidator();

}
