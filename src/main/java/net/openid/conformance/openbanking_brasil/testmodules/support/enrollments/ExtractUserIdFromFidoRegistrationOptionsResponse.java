package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Map;
import java.util.Optional;

public class ExtractUserIdFromFidoRegistrationOptionsResponse extends AbstractCondition {

	// Registration Options Response
	private static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	protected JsonElement bodyFrom(Environment environment) {
		try {
			return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Error parsing JWT response");
		}
	}


	/**
	 * RESPONSE_ENV_KEY - resource_endpoint_response_full - expects to contain Registration Options Response
	 */

	@Override
	@PreEnvironment(required = {RESPONSE_ENV_KEY})
	@PostEnvironment(strings = "user_id")
	public Environment evaluate(Environment env) {

		JsonObject body = bodyFrom(env).getAsJsonObject();
		String userId = Optional.ofNullable(body.getAsJsonObject("data"))
			.map(data -> data.getAsJsonObject("user"))
			.map(user -> user.get("id"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Could not extract data.user.id from the registration options response", Map.of("body", body)));

		env.putString("user_id", userId);
		logSuccess("Extracted user id from the registration options response", Map.of("user_id", userId));

		return env;
	}
}
