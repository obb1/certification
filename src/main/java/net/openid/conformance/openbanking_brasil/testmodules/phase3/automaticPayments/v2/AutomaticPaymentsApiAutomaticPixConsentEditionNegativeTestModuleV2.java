package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule.AbstractAutomaticPaymentsApiAutomaticPixConsentEditionNegativeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentPixRecurringV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetAutomaticPaymentRecurringConsentV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.GetRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PatchRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringConsentOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.automaticPayments.v2.PostRecurringPaymentPixOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "automatic-payments_api_automatic-pix-consent-edition-negative_test-module_v2",
	displayName = "automatic-payments_api_automatic-pix-consent-edition-negative_test-module_v2",
	summary = "Ensure consent edition can not be applied if it goes against any of the defined business rules\n" +
		"\n" +
		"• Call the POST recurring-consents endpoint with automatic pix fields, with referenceStartDate as D+1, sending the expirationDateTime as D+180, interval as SEMANAL, and not sending the maximumVariableAmount, minimumVariableAmount as 0.5 BRL, and not sending firstPayment information\n" +
		"• Expect 201 - Validate Response and ensure status as AWAITING_AUTHORISATION\n" +
		"• Redirect the user to authorize consent\n" +
		"• Call the GET recurring-consents endpoint\n" +
		"• Expect 201 - Validate Response and ensure status is AUTHORISED\n" +
		"• Call the PATCH recurring-consents endpoint with consent edition, sending creditor name as \"Openflix\", expirationDateTime as D-1 and not sending the maximumVariableAmount\n" +
		"• Expect 422 DETALHE_EDICAO_INVALIDO - Validate Response\n" +
		"• Call the PATCH recurring-consents endpoint with consent edition, sending creditor name as \"Openflix\", not sending the expirationDateTime and sending the maximumVariableAmount as 0.3 BRL\n" +
		"• Expect 422 CAMPO_NAO_PERMITIDO - Validate Response\n" +
		"• Call the PATCH recurring-consents endpoint with consent edition, sending creditor name as \"Openflix\", not sending the expirationDateTime or maximumVariableAmount, and not sending the loggedUser\n" +
		"• Expect 422 CAMPO_NAO_PERMITIDO - Validate Response\n" +
		"• Call the PATCH recurring-consents endpoint with consent edition, sending creditor name as \"Openflix\", expirationDateTime as D+1, not sending the maximumVariableAmount and not sending riskSignals\n" +
		"• Expect 422 FALTAM_SINAIS_OBRIGATORIOS_PLATAFORMA - Validate Response",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"resource.contractDebtorName",
		"resource.contractDebtorIdentification",
		"resource.creditorAccountIspb",
		"resource.creditorAccountIssuer",
		"resource.creditorAccountNumber",
		"resource.creditorAccountAccountType",
		"resource.creditorName",
		"resource.creditorCpfCnpj"
	}
)
public class AutomaticPaymentsApiAutomaticPixConsentEditionNegativeTestModuleV2 extends AbstractAutomaticPaymentsApiAutomaticPixConsentEditionNegativeTestModule {

	@Override
	protected Class<? extends Condition> patchPaymentConsentValidator() {
		return PatchRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetRecurringConsentOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetRecurringPaymentPixOASValidatorV2.class;
	}

	@Override
	protected boolean isNewerVersion() {
		return true;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetAutomaticPaymentRecurringConsentV2Endpoint.class), condition(GetAutomaticPaymentPixRecurringV2Endpoint.class));
	}
}
