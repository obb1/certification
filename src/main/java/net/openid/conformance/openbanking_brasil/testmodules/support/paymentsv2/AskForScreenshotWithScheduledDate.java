package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.testmodule.Environment;

public class AskForScreenshotWithScheduledDate extends AbstractCondition {
	@Override
	@PostEnvironment(strings = "payments_placeholder")
	public Environment evaluate(Environment env) {
		String message = createBrowserInteractionPlaceholder("Upload a screenshot showing the payment screen with scheduled date for +350 days");
		env.putString("payments_placeholder", message);
		return env;
	}
}
