package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public class AddRecurringConsentIdToQuery extends AbstractCondition {

	@Override
	@PreEnvironment(strings = {"protected_resource_url", "consent_id"})
	public Environment evaluate(Environment env) {

		String consentId = env.getString("consent_id");
		String protectedResourceUrl = env.getString("protected_resource_url");

		try {
			UriComponentsBuilder builder = UriComponentsBuilder.fromUri(new URI(protectedResourceUrl));
			protectedResourceUrl = builder.queryParam("recurringConsentId",consentId).toUriString();
			env.putString("protected_resource_url",protectedResourceUrl);

			logSuccess("Added recurringConsentId to resourceUrl as a query",
				args("protected_resource_url", protectedResourceUrl));
			return env;
		} catch (URISyntaxException e) {
			throw error(String.format("Could not %s", protectedResourceUrl),
				Map.of("consent_id", consentId));
		}
	}
}
