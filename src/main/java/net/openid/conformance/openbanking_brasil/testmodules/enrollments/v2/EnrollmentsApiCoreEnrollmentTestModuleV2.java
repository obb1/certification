package net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiCoreEnrollmentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.CheckEnrollmentNameConsistency;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.SignEnrollmentsRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody.EditEnrollmentsRequestBodyToAddEnrollmentName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.GetEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostEnrollmentsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2.PostFidoRegistrationOptionsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_core-enrollment_test-module_v2",
	displayName = "enrollments_api_core-enrollment_test-module_v2",
	summary = "Ensure that enrollment can be successfully executed:\n" +
		"• GET a SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array" +
		"• Call the POST enrollments endpoint sending the enrollmentName as \"Vinculo JSR\"\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION Redirect the user to authorize the enrollment\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_ENROLLMENT\"\n" +
		"• Call the POST fido-registration-options endpoint\n" +
		"• Expect a 201 response - Validate the response and extract the challenge\n" +
		"• Create the FIDO Credentials and create the attestationObject\n" +
		"• Call the POST fido-registration endpoint, sending the compliant attestationObject, with the origin extracted on the SSA at the ClientDataJson.origin\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AUTHORISED\" and the enrollmentName as \"Vinculo JSR\"",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class EnrollmentsApiCoreEnrollmentTestModuleV2 extends AbstractEnrollmentsApiCoreEnrollmentTestModule {

	@Override
	protected PostEnrollmentsSteps createPostEnrollmentsSteps() {
		return (PostEnrollmentsSteps) super.createPostEnrollmentsSteps()
			.insertBefore(SignEnrollmentsRequest.class, condition(EditEnrollmentsRequestBodyToAddEnrollmentName.class));
	}

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		super.getEnrollmentsAfterFidoRegistration();
		callAndStopOnFailure(CheckEnrollmentNameConsistency.class);
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV2Endpoint.class));
	}
}
