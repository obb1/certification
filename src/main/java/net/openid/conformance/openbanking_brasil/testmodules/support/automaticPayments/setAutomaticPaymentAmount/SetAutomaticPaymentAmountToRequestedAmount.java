package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class SetAutomaticPaymentAmountToRequestedAmount extends AbstractSetAutomaticPaymentAmount {

	@Override
	@PreEnvironment(strings = "requested_amount")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.map(JsonElement::getAsJsonObject)
			.map(body -> body.getAsJsonObject("data"))
			.map(body -> body.getAsJsonObject("payment"))
			.orElseThrow(() -> error("Unable to find data in payments payload"));

		data.addProperty("amount", env.getString("requested_amount"));

		logSuccess("The amount for the next automatic payment has been set", args("amount", env.getString("requested_amount")));
		return env;
	}

	@Override
	protected String automaticPaymentAmount() {
		return null;
	}
}
