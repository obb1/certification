package net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseDoesNotHaveLinks;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractFidoRegistrationOptionsChallenge;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractFirstUriFromSSASoftwareOriginUris;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ExtractUserIdFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoAttestationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateKeyPairFromFidoRegistrationOptionsResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsFidoRegistrationOptionsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateFidoRegistrationRequestBodyToRequestEntityClaims;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRevocationReasonWas.EnsureEnrollmentRevocationReasonWasRevogadoManualmente;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasAuthorised;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRevoked;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareRejectionOrRevocationReasonForPatchRequest.PrepareRevocationReasonRevogadoManualmenteForPatchRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistration;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostFidoRegistrationOptions;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas204;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.GetEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoRegistrationOptionsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PostEnrollmentsResourceSteps;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "enrollments_api_webhook-revoked_test-module",
	displayName = "enrollments_api_webhook-revoked_test-module",
	summary = "Ensure that the tested institution has correctly implemented the webhook notification endpoint and that this endpoint is correctly called when a enrollment is revoked. \n" +
		"For this test the institution will need to register on it’s software statement a webhook under the following format - https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>\n" +
		"• Obtain a SSA from the Directory\n" +
		"• Ensure that on the SSA the attribute software_api_webhook_uris contains the URI https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Ensure the field software_origin_uris is present in the SSA and extract the first uri from the array\n" +
		"• Call the Registration Endpoint, also sending the field \"webhook_uris\":[“https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>”]\n" +
		"• Expect a 201 - Validate Response\n" +
		"• Set the test to wait for X seconds, where X is the time in seconds provided on the test configuration for the attribute webhookWaitTime. If no time is provided, X is defaulted to 600 seconds\n" +
		"• Set the enrollments webhook notification endpoint to be equal to https://web.conformance.directory.openbankingbrasil.org.br/test-mtls/a/<alias>/open-banking/enrollments/v1/enrollments/{enrollmentId}, where the alias is to be obtained from the field alias on the test configuration\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is AWAITING_ACCOUNT_HOLDER_VALIDATION\n" +
		"• Redirect the user to authorize the enrollment\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ENROLLMENT\"\n" +
		"• Call the POST fido-registration-options endpoint\n" +
		"• Expect a 201 response - Validate the response and extract the challenge\n" +
		"• Create the FIDO Credentials and create the attestationObject\n" +
		"• Call the POST fido-registration endpoint, sending the compliant attestationObject, with the origin extracted on the SSA at the ClientDataJson.origin\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AUTHORISED\"\n" +
		"• Call the PATCH enrollments endpoint\n" +
		"• Expect a 204 response\n" +
		"• Expect an incoming message for enrollments, which must be mtls protected - Wait 60 seconds for both messages to be returned\n" +
		"• Return a 202 - Validate the contents of the incoming message, including the presence of the x-webhook-interaction-id header and that the timestamp value is within now and the start of the test\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"REVOKED\", cancelledFrom is INICIADORA and revocationReason is REVOGADO_MANUALMENTE",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.brazilOrganizationId",
		"resource.enrollmentsUrl",
		"directory.apibase",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType",
		"server.discoveryUrl",
		"directory.discoveryUrl"
	}
)
public class EnrollmentsApiWebhookRevokedTestModule extends AbstractEnrollmentsApiWebhookTestModule {

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetEnrollmentsV1Endpoint.class));
	}

	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV1.class;
	}

	@Override
	protected void getSsa() {
		super.getSsa();
		callAndStopOnFailure(ExtractFirstUriFromSSASoftwareOriginUris.class);
	}

	@Override
	protected void executeTestSteps() {
		postAndValidateFidoRegistrationOptions();
		postAndValidateFidoRegistration();
		getEnrollmentsAfterFidoRegistration();
		patchAndValidateEnrollments();
		waitForWebhookResponse();
		validateEnrollmentWebhooks();
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRevoked(), "REVOKED");
		callAndStopOnFailure(EnsureEnrollmentRevocationReasonWasRevogadoManualmente.class);
	}

	protected void postAndValidateFidoRegistrationOptions() {
		userAuthorisationCodeAccessToken();
		runInBlock("Post fido-registration-options - Expects 201", () -> {
			call(createPostFidoRegistrationOptionsSteps());
		});
		runInBlock("Validate fido-registration-options response", () -> {
			validateResourceResponse(postFidoRegistrationOptionsValidator());
			callAndContinueOnFailure(EnsureResponseDoesNotHaveLinks.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(ExtractFidoRegistrationOptionsChallenge.class);
			callAndStopOnFailure(ExtractUserIdFromFidoRegistrationOptionsResponse.class);
		});
	}

	protected PostEnrollmentsResourceSteps createPostFidoRegistrationOptionsSteps() {
		return new PostEnrollmentsResourceSteps(
			new PrepareToPostFidoRegistrationOptions(),
			CreateEnrollmentsFidoRegistrationOptionsRequestBody.class
		);
	}

	protected void postAndValidateFidoRegistration() {
		userAuthorisationCodeAccessToken();
		runInBlock("Post fido-registration - Expects 204", () -> {
			prepareEnvironmentForPostFidoRegistration();
			call(createPostFidoRegistrationSteps());
			callAndContinueOnFailure(EnsureResourceResponseCodeWas204.class, Condition.ConditionResult.FAILURE);
		});
	}

	protected void prepareEnvironmentForPostFidoRegistration() {
		callAndStopOnFailure(GenerateFidoClientData.class);
		callAndStopOnFailure(GenerateKeyPairFromFidoRegistrationOptionsResponse.class);
		callAndStopOnFailure(GenerateFidoAttestationObject.class);
	}

	protected PostEnrollmentsResourceSteps createPostFidoRegistrationSteps() {
		return new PostEnrollmentsResourceSteps(
			new PrepareToPostFidoRegistration(),
			CreateFidoRegistrationRequestBodyToRequestEntityClaims.class,
			true
		);
	}

	protected void getEnrollmentsAfterFidoRegistration() {
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasAuthorised(), "AUTHORISED");
	}

	@Override
	protected Class<? extends Condition> prepareReason() {
		return PrepareRevocationReasonRevogadoManualmenteForPatchRequest.class;
	}
}
