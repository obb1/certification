package net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule.AbstractEnrollmentsApiInvalidPublicKeyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetEnrollmentsV1Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyWithoutDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.GetEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostEnrollmentsOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoRegistrationOASValidatorV1;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v1.PostFidoRegistrationOptionsOASValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "fvp-enrollments_api_invalid-public-key_test-module_v1",
	displayName = "fvp-enrollments_api_invalid-public-key_test-module_v1",
	summary = "Ensure that enrollment can't be successfully executed when a Invalid Public key is sent:\n" +
		"• GET an SSA from the directory and ensure the field software_origin_uris, and extract the first uri from the array\n" +
		"• Call the POST enrollments endpoint\n" +
		"• Expect a 201 response - Validate the response and check if the status is \"AWAITING_RISK_SIGNALS\"\n" +
		"• Call the POST Risk Signals endpoint sending the appropriate signals\n" +
		"• Expect a 204 response\n" +
		"• Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ACCOUNT_HOLDER_VALIDATION\"\n" +
		"• Redirect the user to authorize the enrollment Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is \"AWAITING_ENROLLMENT\"\n" +
		"• Call the POST fido-registration-options endpoint\n" +
		"• Expect a 201 response - Validate the response and extract the challenge\n" +
		"• Call the POST fido-registration endpoint, sending the attestationObject with a the credentialPublicKey as \"INVALID_PUBLIC_KEY\"\n" +
		"• Expect 422 - PUBLIC_KEY_INVALIDA - Validate Error response Call the GET enrollments endpoint\n" +
		"• Expect a 200 response - Validate the response and check if the status is REJECTED\n" +
		"• Call the PATCH Enrollments {EnrollmentID}\n" +
		"• Expects 204\n"
	,
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.enrollmentsUrl",
		"resource.loggedUserIdentification",
		"resource.businessEntityIdentification",
		"resource.debtorAccountIspb",
		"resource.debtorAccountIssuer",
		"resource.debtorAccountNumber",
		"resource.debtorAccountType"
	}
)
public class FvpEnrollmentsApiInvalidPublicKeyTestModuleV1 extends AbstractEnrollmentsApiInvalidPublicKeyTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(CreateEnrollmentsRequestBodyWithoutDebtorAccount.class);
		callAndStopOnFailure(PrepareToPostEnrollments.class);
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationOptionsValidator() {
		return PostFidoRegistrationOptionsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> postFidoRegistrationValidator() {
		return PostFidoRegistrationOASValidatorV1.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceEndpointSequence() {
		return sequenceOf(condition(GetPaymentConsentsV4Endpoint.class), condition(GetPaymentV4Endpoint.class), condition(GetEnrollmentsV1Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> postEnrollmentsValidator() {
		return PostEnrollmentsOASValidatorV1.class;
	}

	@Override
	protected Class<? extends Condition> getEnrollmentsValidator() {
		return GetEnrollmentsOASValidatorV1.class;
	}

}
