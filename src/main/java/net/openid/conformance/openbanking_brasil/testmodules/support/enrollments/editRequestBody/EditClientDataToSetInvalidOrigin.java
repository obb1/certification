package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.GenerateFidoClientData;
import net.openid.conformance.testmodule.Environment;

/**
 * {@link GenerateFidoClientData} condition adds client_data object to the env
 **/
public class EditClientDataToSetInvalidOrigin extends AbstractCondition {

	private static final String INVALID_ORIGIN = "INVALID_ORIGIN";

	@Override
	@PreEnvironment(required = "client_data")
	public Environment evaluate(Environment env) {
		JsonObject clientData = env.getObject("client_data");
		clientData.addProperty("origin", INVALID_ORIGIN);
		logSuccess("The client data has been edited to include an invalid origin",
			args("client data", clientData));
		return env;
	}
}
