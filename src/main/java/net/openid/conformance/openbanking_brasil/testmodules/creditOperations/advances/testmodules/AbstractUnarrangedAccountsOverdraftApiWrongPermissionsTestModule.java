package net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.abstractModule.AbstractApiWrongPermissionsTestModulePhase2;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractGuarantees;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractInstallments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContractPayments;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceContracts;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.PrepareUrlForFetchingCreditAdvanceRoot;
import net.openid.conformance.openbanking_brasil.testmodules.support.CreditAdvanceSelector;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetUnarrangedAccountsOverdraftV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractUnarrangedAccountsOverdraftApiWrongPermissionsTestModule extends AbstractApiWrongPermissionsTestModulePhase2 {


	@Override
	protected abstract Class<? extends Condition> apiResourceContractListResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractGuaranteesResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractPaymentsResponseValidator();

	@Override
	protected abstract Class<? extends Condition> apiResourceContractInstallmentsResponseValidator();

	@Override
	protected OPFScopesEnum getScope() {
		return OPFScopesEnum.UNARRANGED_ACCOUNTS_OVERDRAFT;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(
			condition(GetConsentV3Endpoint.class),
			condition(GetUnarrangedAccountsOverdraftV2Endpoint.class)
		);
	}

	@Override
	protected Class<? extends Condition> contractSelector() {
		return CreditAdvanceSelector.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingRootEndpoint() {
		return PrepareUrlForFetchingCreditAdvanceRoot.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractEndpoint() {
		return PrepareUrlForFetchingCreditAdvanceContracts.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractWarrantiesEndpoint() {
		return PrepareUrlForFetchingCreditAdvanceContractGuarantees.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractPaymentsEndpoint() {
		return PrepareUrlForFetchingCreditAdvanceContractPayments.class;
	}

	@Override
	protected Class<? extends Condition> prepareUrlForFetchingContractScheduledInstalmentsEndpoint() {
		return PrepareUrlForFetchingCreditAdvanceContractInstallments.class;
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}
