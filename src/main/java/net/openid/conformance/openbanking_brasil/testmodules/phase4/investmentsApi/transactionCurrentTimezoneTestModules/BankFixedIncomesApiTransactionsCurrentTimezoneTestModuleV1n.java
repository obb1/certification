package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionCurrentTimezoneTestModules;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesListOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.bankFixedIncomes.v1n4.GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToBankFixedIncomes;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "bank-fixed-incomes_api_transactions-current-timezone_test-module_v1",
	displayName = "bank-fixed-incomes_api_transactions-current-timezone_test-module_v1",
	summary = "Test the transaction current endpoint by sending different parameters to the endpoint and ensuring that they are correctly returned.\n" +
		"To ensure that the server can process the the date, which is set as UTC-3, this test must be executed between 9pm UTC-3 and 11:59pm UTC-3\n" +
		"Call the POST Consents endpoint with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
		"Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
		"Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
		"Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"Call the GET Consents endpoint\n" +
		"Expects 200 - Validate response and confirm that the Consent is set to \"Authorised\"\n" +
		"Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
		"Call the GET Investments List Endpoint \n" +
		"Expect a 200 Response - Extract one InvestmentId - Validate the Response_body\n" +
		"Call GET Investments Transactions-Current API without any query parameters\n" +
		"Expect a 200 Response- Validate all fields of the API - Make sure if transactions are found that they are all from the current date - Test can also expect an empty list\n" +
		"Call GET Investments Transactions-Current API -  Send query Params fromTransactionDate=D-6 and toTransactionDate=D+0, with D being defined as UTC-3 \n" +
		"Expect a 200 Response - Validate all fields of the API - Make sure that at least one transaction is returned and that its transaction date is set between D-6 and D\n" +
		"Call GET Investments Transactions-Current API -  Send query Params fromTransactionDate=D-7 and toTransactionDate=D+0\n" +
		"Expect 422 Unprocessable Entity\n" +
		"Call the Delete Consents Endpoints\n" +
		"Expect a 204 without a body\n" +
		"Validate if the current time is set between 9pm UTC-3 and 11:59pm UTC-3 - Return failure if not as defined on the test summary",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class BankFixedIncomesApiTransactionsCurrentTimezoneTestModuleV1n extends AbstractBankFixedIncomesApiTransactionsCurrentTimezoneTest {

	@Override
	protected AbstractSetInvestmentApi setInvestmentsApi()  {
		return new SetInvestmentApiToBankFixedIncomes();
	}

	@Override
	protected Class<? extends Condition> investmentsRootValidator()  {
		return GetBankFixedIncomesListOASValidatorV1n4.class;
	}

	@Override
	protected Class<? extends Condition> investmentsTransactionsValidator() {
		return GetBankFixedIncomesTransactionsCurrentOASValidatorV1n4.class;
	}

	@Override
	protected OPFScopesEnum setScope(){
		return OPFScopesEnum.BANK_FIXED_INCOMES;
	}
}
