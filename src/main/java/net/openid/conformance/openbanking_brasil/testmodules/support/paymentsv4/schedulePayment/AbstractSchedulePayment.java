package net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public abstract class AbstractSchedulePayment extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject consentRequestPayment = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.flatMap(consent -> Optional.ofNullable(consent.getAsJsonObject()))
			.flatMap(consent -> Optional.ofNullable(consent.getAsJsonObject("data")))
			.flatMap(data -> Optional.ofNullable(data.getAsJsonObject("payment")))
			.orElseThrow(() -> error("Could not find brazilPaymentConsent.data.payment in the resource"));

		JsonObject schedule = createScheduleObject();

		consentRequestPayment.add("schedule", schedule);
		logSuccess("Added schedule object to the consent request payment", args("consentRequestPayment", consentRequestPayment));

		return env;
	}

	protected abstract JsonObject createScheduleObject();
}
