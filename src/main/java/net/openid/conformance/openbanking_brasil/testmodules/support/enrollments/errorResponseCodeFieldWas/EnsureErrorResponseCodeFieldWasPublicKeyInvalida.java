package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.PostEnrollmentsFidoRegistrationErrorEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasPublicKeyInvalida extends AbstractEnsureErrorResponseCodeFieldWas {

    @Override
    protected List<String> getExpectedCodes() {
        return List.of(PostEnrollmentsFidoRegistrationErrorEnum.PUBLIC_KEY_INVALIDA.toString());
    }
}
