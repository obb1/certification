package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.AbstractAutomaticUnhappyPaymentsConsentsFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.AbstractCreateRecurringConfigurationObject;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.CreateSweepingRecurringConfigurationObjectWithOnlyAmount;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody.EditRecurringPaymentsConsentBodyToSetInvalidCreditor;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroInvalido;

public abstract class AbstractAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModule extends AbstractAutomaticUnhappyPaymentsConsentsFunctionalTestModule {

	@Override
	protected void performTestSteps() {
		performTestStep(
			"POST consents sending the logged user not matching the creditor - Expects 422 PARAMETRO_INVALIDO",
			EditRecurringPaymentsConsentBodyToSetInvalidCreditor.class,
			EnsureErrorResponseCodeFieldWasParametroInvalido.class
		);
	}

	@Override
	protected AbstractCreateRecurringConfigurationObject consentBodyCondition() {
		return new CreateSweepingRecurringConfigurationObjectWithOnlyAmount();
	}
}
