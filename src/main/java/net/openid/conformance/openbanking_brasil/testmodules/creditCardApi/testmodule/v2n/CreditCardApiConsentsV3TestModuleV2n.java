package net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.abstractModule.AbstractCreditCardApiTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetCreditCardsAccountsV2Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsBillsTransactionsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsIdentificationOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsLimitsOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsListOASValidatorV2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.creditCard.v2.GetCreditCardAccountsTransactionsOASValidatorV2;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "credit-cards_api_core_consents_v3-2_test-module_v2-3",
	displayName = "Validate structure of all credit card API resources V2 with consents V3-2",
	summary = "Validates the structure of all credit card API resources V2 with consents V3-2\n" +
		"• Creates a Consent with the complete set of the credit cards permission group ([\"CREDIT_CARDS_ACCOUNTS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_READ\", \"CREDIT_CARDS_ACCOUNTS_BILLS_TRANSACTIONS_READ\", \"CREDIT_CARDS_ACCOUNTS_LIMITS_READ\", \"CREDIT_CARDS_ACCOUNTS_TRANSACTIONS_READ\", \"RESOURCES_READ\"])\n" +
		"• Expects a success 201 - Expects a success on Redirect as well \n" +
		"• Calls GET Credit Cards Accounts API V2\n" +
		"• Expects a 200 response \n" +
		"• Calls GET Credit Cards Accounts API V2 with AccountID specified\n" +
		"• Expects a 200 response\n" +
		"• Calls GET Credit Cards Accounts Limits API V2 with AccountID specified\n" +
		"• Expects a 200 response\n" +
		"• Calls GET Credit Cards Accounts Transactions API V2 with AccountID specified\n" +
		"• Expects a 200 response\n" +
		"• Calls GET Credit Cards Accounts Bills API V2 with AccountID specified\n" +
		"• Expects a 200 response\n" +
		"• Calls GET Credit Cards Accounts Bills Transactions V2 API with AccountID specified\n" +
		"• Expects a 200 response\n"+
		"5",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl"
})
public class CreditCardApiConsentsV3TestModuleV2n extends AbstractCreditCardApiTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected void configureClient() {
		super.configureClient();
	}


	@Override
	protected Class<? extends Condition> getCardBillsTransactionsValidator() {
		return GetCreditCardAccountsBillsTransactionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardBillsValidator() {
		return GetCreditCardAccountsBillsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardIdentificationValidator() {
		return GetCreditCardAccountsIdentificationOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardAccountsLimitsValidator() {
		return GetCreditCardAccountsLimitsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardAccountsTransactionsValidator() {
		return GetCreditCardAccountsTransactionsOASValidatorV2.class;
	}

	@Override
	protected Class<? extends Condition> getCardAccountsListValidator() {
		return GetCreditCardAccountsListOASValidatorV2.class;
	}

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class), condition(GetCreditCardsAccountsV2Endpoint.class));
	}

	@Override
	protected Class<? extends Condition> getPostConsentValidator() {
		return PostConsentOASValidatorV3n2.class;
	}
}


