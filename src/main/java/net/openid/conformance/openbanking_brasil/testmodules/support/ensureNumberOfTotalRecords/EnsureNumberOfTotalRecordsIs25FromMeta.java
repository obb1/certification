package net.openid.conformance.openbanking_brasil.testmodules.support.ensureNumberOfTotalRecords;

public class EnsureNumberOfTotalRecordsIs25FromMeta extends AbstractEnsureNumberOfTotalRecordsFromMeta {
	@Override
	protected TotalRecordsComparisonOperator getComparisonMethod() {
		return TotalRecordsComparisonOperator.EQUAL;
	}

	@Override
	protected int getTotalRecordsComparisonAmount() {
		return 25;
	}
}
