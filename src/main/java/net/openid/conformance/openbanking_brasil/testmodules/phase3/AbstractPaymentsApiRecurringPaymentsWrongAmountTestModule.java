package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExtractPaymentId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasConsumed;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetIncorrectAmountInPaymentsDataArray;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.daily.Schedule5DailyPaymentsStarting1DayInTheFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.CallPixPaymentsEndpointSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractPaymentsApiRecurringPaymentsWrongAmountTestModule extends AbstractPaymentApiSchedulingUnhappyTestModule {

	protected abstract Class<? extends Condition> paymentInitiationErrorValidator();

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SetIncorrectAmountInPaymentsDataArray.class);
	}

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule5DailyPaymentsStarting1DayInTheFuture();
	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		return new CallPixPaymentsEndpointSequence()
			.replace(EnsureResourceResponseCodeWas201.class, condition(EnsureResourceResponseCodeWas201Or422.class));
	}

	@Override
	protected AbstractEnsureErrorResponseCodeFieldWas errorFieldCondition() {
		return new EnsureErrorResponseCodeFieldWasPagamentoDivergenteConsentimento();
	}

	@Override
	protected void validateResponse() {
		int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));

		if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			callAndStopOnFailure(paymentInitiationErrorValidator());
			super.validateResponse();
		} else {
			callAndStopOnFailure(ExtractPaymentId.class);
			call(postPaymentValidationSequence());
			runRepeatSequence();
			validateFinalState();
			callAndContinueOnFailure(EnsurePaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento.class, Condition.ConditionResult.FAILURE);
		}

		fetchConsentToCheckStatus("CONSUMED", new EnsurePaymentConsentStatusWasConsumed());
	}

	@Override
	protected void validateFinalState() {
		callAndStopOnFailure(EnsurePaymentStatusWasRjct.class);
	}
}
