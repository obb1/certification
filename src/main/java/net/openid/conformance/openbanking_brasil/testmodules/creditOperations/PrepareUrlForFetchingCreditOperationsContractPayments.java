package net.openid.conformance.openbanking_brasil.testmodules.creditOperations;

import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesType;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingCreditOperationsContractPayments extends ResourceBuilder {

	@Override
	public Environment evaluate(Environment env) {

		String contractId = env.getString("contractId");
		String apiType = EnumResourcesType.valueOf(env.getString("api_type")).getType();

		setApi(apiType);
		setEndpoint("/contracts/" + contractId + "/payments");

		return super.evaluate(env);
	}
}
