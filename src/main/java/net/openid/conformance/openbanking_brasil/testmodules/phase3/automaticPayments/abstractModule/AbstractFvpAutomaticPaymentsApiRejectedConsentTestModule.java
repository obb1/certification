package net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.abstractModule;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddJWTAcceptHeader;
import net.openid.conformance.openbanking_brasil.testmodules.support.ExpectJWTResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToFetchConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CallPatchAutomaticPaymentsConsentsEndpointSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ExtractRecurringConsentIdFromConsentEndpointResponse;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.SetRecurringPaymentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedBy.EnsureRecurringPaymentsConsentsRejectedByUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectedFrom.EnsureRecurringPaymentsConsentsRejectedFromIniciadora;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.rejectionReason.EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.AbstractEnsurePaymentConsentStatusWas;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentConsentStatus.EnsurePaymentConsentStatusWasAwaitingAuthorisation;
import net.openid.conformance.openbanking_brasil.testmodules.support.yacs.InsertBrazilAutomaticPaymentRecurringConsentProdValues;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.EnsureConsentStatusWasRejected;

public abstract class AbstractFvpAutomaticPaymentsApiRejectedConsentTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> postPaymentConsentValidator();
	protected abstract Class<? extends Condition> getPaymentConsentValidator();
	protected abstract Class<? extends Condition> patchPaymentConsentValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetRecurringPaymentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void insertConsentProdValues() {
		callAndStopOnFailure(InsertBrazilAutomaticPaymentRecurringConsentProdValues.class);
	}

	@Override
	protected void runTests() {
		eventLog.startBlock("Call POST recurring-consents - Expects 201 AWAITING_AUTHORISATION");
		call(getPaymentsConsentSequence());
		callAndContinueOnFailure(postPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsurePaymentConsentStatusWasAwaitingAuthorisation.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(ExtractRecurringConsentIdFromConsentEndpointResponse.class);
		eventLog.endBlock();

		fetchConsentToCheckStatus("AWAITING_AUTHORISATION", new EnsurePaymentConsentStatusWasAwaitingAuthorisation());

		eventLog.startBlock("Call PATCH recurring-consents - Expects 200");
		call(new CallPatchAutomaticPaymentsConsentsEndpointSequence()
			.skip(FetchServerKeys.class, "Not needed")
			.skip(ValidateResourceResponseSignature.class, "Not needed")
			.skip(ValidateResourceResponseJwtClaims.class, "Not needed"));
		callAndContinueOnFailure(patchPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();

		fetchConsentToCheckStatus("REJECTED", new EnsureConsentStatusWasRejected());
		eventLog.startBlock("Validate rejection fields");
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRejectedByUsuario.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRejectedFromIniciadora.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureRecurringPaymentsConsentsRejectionReasonWasRejeitadoUsuario.class, Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
	}

	protected void fetchConsentToCheckStatus(String status, AbstractEnsurePaymentConsentStatusWas statusCondition) {
		eventLog.startBlock(String.format("Checking the created consent - Expecting %s status", status));
		callAndStopOnFailure(AddJWTAcceptHeader.class);
		callAndStopOnFailure(ExpectJWTResponse.class);
		callAndStopOnFailure(PrepareToFetchConsentRequest.class);
		callAndContinueOnFailure(FAPIBrazilCallPaymentConsentEndpointWithBearerTokenAnyMethod.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(EnsureConsentResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(statusCondition.getClass(), Condition.ConditionResult.FAILURE);
		validateGetConsentResponse();
		eventLog.endBlock();
	}

	protected void validateGetConsentResponse() {
		eventLog.startBlock("Validate GET Consent Response");
		env.mapKey("endpoint_response", "consent_endpoint_response_full");
		callAndStopOnFailure(ExtractSignedJwtFromResourceResponse.class);
		env.unmapKey("endpoint_response");
		callAndContinueOnFailure(getPaymentConsentValidator(), Condition.ConditionResult.FAILURE);
		eventLog.endBlock();
	}
}
