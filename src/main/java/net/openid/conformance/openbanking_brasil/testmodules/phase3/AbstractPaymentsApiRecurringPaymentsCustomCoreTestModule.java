package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.custom.CustomSchedulePaymentsWithStandardPayload;

public abstract class AbstractPaymentsApiRecurringPaymentsCustomCoreTestModule extends AbstractPaymentApiSchedulingTestModule {

	@Override
	protected void configureDictInfo() {}

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new CustomSchedulePaymentsWithStandardPayload();
	}
}
