package net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsBalancesV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsIdentificationV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsListV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsTransactionsCurrentV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.funds.v1.GetFundsTransactionsV1OASValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.AbstractSetInvestmentApi;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.setInvestmentApi.SetInvestmentApiToFunds;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo365DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetFromTransactionConversionDateTo6DaysAgo;
import net.openid.conformance.openbanking_brasil.testmodules.support.phase4.transactionConversionDateConditions.SetToTransactionConversionDateToToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "funds_api_operational-limits_test-module",
	displayName = "funds_api_operational-limits_test-module",
	summary = "Test will make sure that the defined operational limits for all of the endpoints are respected. It will also confirm that the transactions endpoint can be used beyond the operational limit, as long as the next page is called.\n" +
		"Test will require the user to have set at least one ACTIVE resource each with at least 26 Transactions to be returned on the transactions endpoint. The tested resource must be set linked to the brazilCpf and brazilCnpj for Operational Limits\n" +
		"Test will execute a POST token with a refresh_token grant at least 60 seconds before the token is about to be expired, by working with the \"expires_in\" set on the token endpoint. Test will not log all successful the validations done on the endpoints due to the high number of requests, however all failures will be provided.\n" +
		"• Call the POST Consents endpoint, using the brazilCpf/Cnpj for Operational Limits, with the Investments API Permission Group - [BANK_FIXED_INCOMES_READ, CREDIT_FIXED_INCOMES_READ, FUNDS_READ, VARIABLE_INCOMES_READ, TREASURE_TITLES_READ, RESOURCES_READ]\n" +
		"• Expect a 201 - Make sure status is on Awaiting Authorisation - Validate Response Body\n" +
		"• Set on the the authorization request, in addition the consents scope, the Investments API scopes (treasure-titles funds variable-incomes credit-fixed-incomes bank-fixed-incomes)\n" +
		"• Redirect the user to Authorize the Consent - Expect a successful redirect\n" +
		"• Call the GET Consents endpoint, confirm that the Consent is set to \"Authorised\" - Expects 200 and Validate Response Body\n" +
		"• Call the POST Token Endpoint - obtain a Token with the Authorization_code grant\n" +
		"• Call the GET Investments List Endpoint 30 Times - Expect a 200 Response all times - Extract one Investment ID\n" +
		"• Call the GET Investments Transactions Endpoint with the Extracted Investment ID 1 time - Send query Params fromTransactionDate=D-365 and toTransactionDate=D+0 - Expect a 200 Response\n" +
		"• Call the GET Investments Transactions endpoint with the self Link URI 3 times - Expect a 200 Response all times - In the last call, ensure a next link is present and extract it\n" +
		"• Call the GET Investments Transactions endpoint with the next Link 10 times - Expect a 200 - Extract one brokerNoteId\n" +
		"• Call the GET Investments Broker Note Endpoint with the Extracted brokerNoteId 30 times - Expect a 200 Response all times\n" +
		"• Call the GET Investments Identification Endpoint 4 times with the Extracted Investment ID - Expect a 200 Response all times\n" +
		"• Call the GET Investments Balances Endpoint 30 times with the Extracted Investment ID - Expect a 200 Response all times\n" +
		"• Call the GET Investments Transactions-Current Endpoint 30 times with the Extracted Investment ID - Send query Params fromTransactionDate=D-6 and toTransactionDate=D+0 - Expect a 200 Response all times\n" +
		"• Call the Delete Consents Endpoints\n" +
		"• Expect a 204 without a body",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
public class FundsApiOperationalLimitsTestModule extends AbstractInvestmentsApiOperationalLimitsTestModule {
	@Override
	protected OPFScopesEnum setScope() {
		return OPFScopesEnum.FUNDS;
	}

	@Override
	protected AbstractSetInvestmentApi setInvestmentsApi() {
		return new SetInvestmentApiToFunds();
	}

	@Override
	protected void investmentsRootValidator() {
		callAndContinueOnFailure(GetFundsListV1OASValidator.class,Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void investmentTransactionsValidator() {
		callAndContinueOnFailure(GetFundsTransactionsV1OASValidator.class,Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void investmentTransactionsCurrentValidator() {
		callAndContinueOnFailure(GetFundsTransactionsCurrentV1OASValidator.class,Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void investmentIdentificationValidator() {
		callAndContinueOnFailure(GetFundsIdentificationV1OASValidator.class,Condition.ConditionResult.FAILURE);
	}

	@Override
	protected void investmentBalancesValidator() {
		callAndContinueOnFailure(GetFundsBalancesV1OASValidator.class,Condition.ConditionResult.FAILURE);
	}

	@Override
	protected Class<? extends Condition> setToTransactionToToday() {
		return SetToTransactionConversionDateToToday.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo365DaysAgo() {
		return SetFromTransactionConversionDateTo365DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> setFromTransactionTo6DaysAgo() {
		return SetFromTransactionConversionDateTo6DaysAgo.class;
	}

	@Override
	protected Class<? extends Condition> addToAndFromTransactionParametersToProtectedResourceUrl() {
		return AddToAndFromTransactionConversionDateParametersToProtectedResourceUrl.class;
	}


}
