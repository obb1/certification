package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.productsNServices.invoiceFinancings.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.OpenAPIJsonSchemaValidator;
import org.springframework.http.HttpMethod;

/**
 * Api url: https://raw.githubusercontent.com/OpenBanking-Brasil/draft-openapi/main/swagger-apis/opendata-invoicefinancings/1.0.1.yml
 * Api endpoint: /business-invoice-financings
 * Api version: 1.0.1
 *
 */

@ApiName("ProductsNServices Business Invoice Financings V1")
public class GetProductsNServicesBusinessInvoiceFinancingsOASValidatorV1 extends OpenAPIJsonSchemaValidator {

	@Override
	protected String getPathToOpenApiSpec() {
		return "swagger/opendata/swagger-invoice-financings-1.0.1.yaml";
	}

	@Override
	protected String getEndpointPath() {
		return "/business-invoice-financings";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.GET;
	}
}
