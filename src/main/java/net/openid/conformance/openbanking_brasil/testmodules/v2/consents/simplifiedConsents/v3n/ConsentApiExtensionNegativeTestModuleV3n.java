package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentExtensionsOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.GetConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentExtendsOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.consents.v3n2.PostConsentOASValidatorV3n2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.AbstractConsentApiExtensionNegativeTest;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "consents_api_negative-extensions_test-module_v3-2",
	displayName = "consents_api_negative-extensions_test-module_v3-2",
	summary = "Ensure consent creation and extension follow business rules\n" +
		"• Call the POST Consents Endpoint with expirationDateTime as 2300-01-01T00:00:00Z\n" +
		"• Expects 422 - Validate Error Message\n" +
		"• Call the POST Consents Endpoint with expirationDateTime as current time + 5 minutes\n" +
		"• Expects 201 - Validate Response\n" +
		"• Redirect the user to authorize the consent\n" +
		"• Call the POST Token Endpoint Using the Authorization Code Grant\n" +
		"• Expects 201 - Validate if the refresh token sent back is not JWT\n" +
		"• Call the GET Consents\n" +
		"• Expects 200 - Validate Response and check if the status is AUTHORISED\n" +
		"• Call the GET Extends Endpoint\n" +
		"• Expects 200 - Validate Response and ensure an empty array is returned\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as D+180\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as D+90\n" +
		"• Expects 422 DATA_EXPIRACAO_INVALIDA - Validate Error Message\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as D+180\n" +
		"• Expects 422 DATA_EXPIRACAO_INVALIDA - Validate Error Message\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as D+365\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as D+730\n" +
		"• Expects 422 DATA_EXPIRACAO_INVALIDA - Validate Error Message\n" +
		"• Call the POST Extends Endpoint with expirationDateTime as 2300-01-01T00:00:00Z\n" +
		"• Expects 422 DATA_EXPIRACAO_INVALIDA - Validate Error Message\n" +
		"• Call the POST Extends Endpoint without expirationDateTime\n" +
		"• Expects 201 - Validate Response\n" +
		"• Call the GET Extends Endpoint\n" +
		"• Expects 200 - Validate Response, check if there are three items on the data object and that the expirationDateTime for them are as requested previously at the success responses, and that they are ordered in descending order by the request date (`data[].requestDateTime`). Therefore, the first item on the list will have the same expiration date as the current consent because it was the last renewal made.",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"directory.discoveryUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "resource.consentUrl","consent.productType"
})
public class ConsentApiExtensionNegativeTestModuleV3n extends AbstractConsentApiExtensionNegativeTest {

	@Override
	protected Class<? extends Condition> setGetConsentValidator() {
		return GetConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> setConsentCreationValidation() {
		return PostConsentOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> setPostConsentExtensionValidator() {
		return PostConsentExtendsOASValidatorV3n2.class;
	}

	@Override
	protected Class<? extends Condition> setGetConsentExtensionValidator() {
		return GetConsentExtensionsOASValidatorV3n2.class;
	}
}
