package net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractAssertLinksLogic extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	protected abstract void validateLinksLogic(JsonObject links);

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject body = getBodyFromEnv(env);
		JsonObject links = Optional.ofNullable(body.getAsJsonObject("links"))
			.orElseThrow(() -> error("Could not extract links from body", args("body", body)));

		validateLinksLogic(links);

		return env;
	}

	protected JsonObject getBodyFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	protected String extractMandatoryLink(JsonObject links, String linkName, String mandatoryWhen) {
		return Optional.ofNullable(links.get(linkName))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error(String.format("The %s link is mandatory %s", linkName, mandatoryWhen),
				args("links", links)));
	}

	protected void validateSuccessiveLinks(String firstLink, String firstLinkName, String secondLink, String secondLinkName) {
		if (extractPage(secondLink) != extractPage(firstLink) + 1) {
			throw error(String.format("The \"page\" parameter from %s should be equals to the one from %s + 1", secondLinkName, firstLinkName),
				args(firstLinkName, firstLink, secondLinkName, secondLink));
		}
	}

	protected int extractPage(String url) {
		Pattern pattern = Pattern.compile("page=(\\d+)");
		Matcher matcher = pattern.matcher(url);
		if (matcher.find()) {
			return Integer.parseInt(matcher.group(1));
		} else {
			return 1;
		}
	}
}
