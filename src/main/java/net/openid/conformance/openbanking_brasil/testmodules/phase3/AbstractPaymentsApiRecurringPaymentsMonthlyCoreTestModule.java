package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.AbstractSchedulePayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.schedulePayment.monthly.Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture;

public abstract class AbstractPaymentsApiRecurringPaymentsMonthlyCoreTestModule extends AbstractPaymentApiSchedulingTestModule{

	@Override
	protected AbstractSchedulePayment schedulingCondition() {
		return new Schedule5MonthlyPaymentsForThe31stStarting1DayInTheFuture();
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
	}

}
