package net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode;

import org.springframework.http.HttpStatus;

import java.util.List;

public class EnsureConsentResponseCodeWas400Or422 extends AbstractEnsureConsentResponseCode {

	@Override
	protected List<HttpStatus> getExpectedStatus() {
		return List.of(HttpStatus.BAD_REQUEST,HttpStatus.UNPROCESSABLE_ENTITY);
	}

}
