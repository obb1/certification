package net.openid.conformance.openbanking_brasil.testmodules.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.StringUtils;

public class EnsureOperationalUserIsPresent extends AbstractCondition {
	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String paginationUser = env.getString("config", "resource.brazilCpfOperational");

		if (StringUtils.isBlank(paginationUser)) {
			env.putBoolean("continue_test", false);
			throw error("brazilCpfOperational field is empty.");
		}

		logSuccess("brazilCpfOperational is present",
			args("paginationUser", paginationUser));
		return env;
	}
}
