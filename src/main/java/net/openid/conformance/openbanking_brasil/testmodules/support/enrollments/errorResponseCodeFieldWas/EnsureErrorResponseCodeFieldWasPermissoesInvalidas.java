package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.errorResponseCodeFieldWas;

import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.enums.PostEnrollmentsErrorEnum;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.AbstractEnsureErrorResponseCodeFieldWas;

import java.util.List;

public class EnsureErrorResponseCodeFieldWasPermissoesInvalidas extends AbstractEnsureErrorResponseCodeFieldWas {

    @Override
    protected List<String> getExpectedCodes() {
        return List.of(PostEnrollmentsErrorEnum.PERMISSOES_INVALIDAS.toString());
    }
}
