package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsApiPixSchedulingDatesUnhappyTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_pixscheduling-dates-unhappy_test-module_v4",
	displayName = "5 Negative Test Scenarios that change the payment date and schedule.single.date fields and expect errors on the POST Consents request",
	summary = "5 Negative Test Scenarios that change the payment date and schedule.single.date fields and expect errors on the POST Consents request\n" +
		"\u2022 Attempts to create a payment consent scheduled with the date element present together with schedule.single.date, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA\n" +
		"\u2022 Create consent with request payload with both the date and the schedule.single.date fields\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error DATA_PAGAMENTO_INVALIDA - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent scheduled for today, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA\n" +
		"\u2022 Create consent with request payload set with schedule.single.date field defined as today\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error  DATA_PAGAMENTO_INVALIDA - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent scheduled for a day in the past, with a payment date of yesterday, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA \n" +
		"\u2022 Create consent with request payload set with schedule.single.date field defined as yesterday D-1\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error DATA_PAGAMENTO_INVALIDA  - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent scheduled for a day too far in the future, with a payment date of today + 740 days, and expects a 422 response with the error DATA_PAGAMENTO_INVALIDA \n" +
		"\u2022 Create consent with request payload set with schedule.single.date field defined as yesterday D+740\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error DATA_PAGAMENTO_INVALIDA  - Validate Error response \n" +
		"\u2022 Attempts to create a payment consent wihtout the date or date scheduled fields, both which are optional, expecting error PARAMETRO_NAO_INFORMADO\n" +
		"\u2022 Create consent with request payload set without the date field provided\n" +
		"\u2022 Call the POST Consents endpoints\n" +
		"\u2022 Expects 422 with error PARAMETRO_NAO_INFORMADO  - Validate Error response ",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsApiPixSchedulingDatesUnhappyTestModuleV4 extends AbstractPaymentsApiPixSchedulingDatesUnhappyTestModule {

	@Override
	protected Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}
}
