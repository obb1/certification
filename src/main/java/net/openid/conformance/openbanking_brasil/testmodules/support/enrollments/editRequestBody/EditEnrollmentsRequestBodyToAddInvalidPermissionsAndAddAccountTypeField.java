package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.editRequestBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Optional;

public class EditEnrollmentsRequestBodyToAddInvalidPermissionsAndAddAccountTypeField extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(
			env.getElementFromObject("resource_request_entity_claims", "data")
		).orElseThrow(() -> error("Could not find data inside the body")).getAsJsonObject();

		JsonObject debtorAccount = Optional.ofNullable(
			env.getElementFromObject("resource_request_entity_claims", "data.debtorAccount")
		).orElseThrow(() -> error("Could not find debtorAccount inside the body")).getAsJsonObject();

		String[] permissionStrings = {"ACCOUNTS_READ", "ACCOUNTS_BALANCES_READ", "ACCOUNTS_BALANCES_READ",
			"ACCOUNTS_BALANCES_READ", "RESOURCES_READ"};

		JsonArray permissions = new JsonArray();
		for (String permissionString : permissionStrings) {
			permissions.add(permissionString);
		}

		data.add("permissions", permissions);
		debtorAccount.addProperty("accountType", "CACC");

		logSuccess("Successfully edited request body to include invalid permissions and to add back debtorAccount.accountType",
			args("request body", env.getObject("resource_request_entity_claims")));


		return env;
	}
}
