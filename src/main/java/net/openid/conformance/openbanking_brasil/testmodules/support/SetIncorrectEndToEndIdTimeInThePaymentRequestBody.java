package net.openid.conformance.openbanking_brasil.testmodules.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class SetIncorrectEndToEndIdTimeInThePaymentRequestBody extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonElement paymentRequestData = Optional.ofNullable(env.getElementFromObject("resource", "brazilPixPayment"))
			.flatMap(request -> Optional.ofNullable(request.getAsJsonObject().get("data")))
			.orElseThrow(() -> error("Could not extract brazilPixPayment.data from the resource"));

		if (paymentRequestData.isJsonArray()) {
			for (int i = 0; i < paymentRequestData.getAsJsonArray().size(); i++) {
				JsonObject data = paymentRequestData.getAsJsonArray().get(i).getAsJsonObject();
				createNewEndToEndId(data);
			}
			return env;
		}

		createNewEndToEndId(paymentRequestData.getAsJsonObject());

		return env;
	}

	private void createNewEndToEndId(JsonObject payment) {
		String originalEndToEndId = OIDFJSON.getString(Optional.ofNullable(payment.get("endToEndId"))
			.orElseThrow(() -> error("Could not extract endToEndId from the brazilPixPayment.data", args("data", payment))));

		String endToEndIdStart = originalEndToEndId.substring(0, 17);
		String endToEndIdEnd = originalEndToEndId.substring(21);
		String newEndToEndId = endToEndIdStart + "0100" + endToEndIdEnd;
		payment.addProperty("endToEndId", newEndToEndId);

		logSuccess("Incorrect end to end Id was added to the brazilPixPayment",
			args("original", originalEndToEndId, "new", newEndToEndId, "data", payment));
	}
}
