package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.CleanBrazilIds;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureDailyLimitSetTo1BRL;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureTransactionLimitSetTo1BRL;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody.CreateEnrollmentsRequestBodyWithoutDebtorAccount;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.prepareTo.PrepareToPostEnrollments;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsNrj.SetConsentIdInPaymentsRequestBody;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountTo05BRLOnConsent;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.setPaymentAmountTo.SetPaymentAmountTo05BRLOnPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;

public abstract class AbstractFvpEnrollmentsApiPaymentsUnmatchingFieldsTestModule extends AbstractEnrollmentsApiPaymentsUnmatchingFieldsTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(CleanBrazilIds.class, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		callAndStopOnFailure(CreateEnrollmentsRequestBodyWithoutDebtorAccount.class);
		callAndStopOnFailure(PrepareToPostEnrollments.class);
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SetPaymentAmountTo05BRLOnConsent.class);
		super.configureDictInfo();
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentErrorValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected void checkStatusAfterAuthorization() {
		super.checkStatusAfterAuthorization();
		callAndStopOnFailure(EnsureDailyLimitSetTo1BRL.class);
		callAndStopOnFailure(EnsureTransactionLimitSetTo1BRL.class);
	}

	@Override
	protected void getEnrollmentsAfterFidoRegistration() {
		super.getEnrollmentsAfterFidoRegistration();
		callAndStopOnFailure(EnsureDailyLimitSetTo1BRL.class);
		callAndStopOnFailure(EnsureTransactionLimitSetTo1BRL.class);
	}

	@Override
	protected void postAndValidatePayment() {
		userAuthorisationCodeAccessToken();
		runInBlock(currentClientString() + "Call pix/payments endpoint", () -> {
			callAndStopOnFailure(SetConsentIdInPaymentsRequestBody.class);
			callAndStopOnFailure(SetPaymentAmountTo05BRLOnPayment.class);
			call(getPixPaymentSequence());
		});
		runInBlock(currentClientString() + "Validate response", this::validatePaymentsResponse);
	}

}
