package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.enrollments.v2;

import org.springframework.http.HttpMethod;

public class PatchEnrollmentsOASValidatorV2 extends AbstractEnrollmentsOASValidatorV2 {

	@Override
	protected String getEndpointPathSuffix() {
		return "/{enrollmentId}";
	}

	@Override
	protected HttpMethod getEndpointMethod() {
		return HttpMethod.PATCH;
	}
}
