package net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v4.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PaymentRejectionReasonEnumV4 {
	SALDO_INSUFICIENTE, VALOR_ACIMA_LIMITE,
	VALOR_INVALIDO, COBRANCA_INVALIDA,
	NAO_INFORMADO, PAGAMENTO_DIVERGENTE_CONSENTIMENTO,
	DETALHE_PAGAMENTO_INVALIDO,
	PAGAMENTO_RECUSADO_DETENTORA,
	PAGAMENTO_RECUSADO_SPI, FALHA_INFRAESTRUTURA,
	FALHA_INFRAESTRUTURA_SPI, FALHA_INFRAESTRUTURA_DICT,
	FALHA_INFRAESTRUTURA_ICP, FALHA_INFRAESTRUTURA_PSP_RECEBEDOR,
	FALHA_INFRAESTRUTURA_DETENTORA, CONTAS_ORIGEM_DESTINO_IGUAIS,
	FALHA_AGENDAMENTO_PAGAMENTOS;


	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
