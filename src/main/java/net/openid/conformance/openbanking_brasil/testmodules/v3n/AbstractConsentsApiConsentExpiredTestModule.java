package net.openid.conformance.openbanking_brasil.testmodules.v3n;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.testmodules.phase2.AbstractPhase2TestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.consent.consentRejectionValidation.EnsureConsentRejectAspspMaxDateReached;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.v3.GetConsentV3Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasConsentimentoEmStatusRejeitado;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.sequence.client.OpenBankingBrazilPreAuthorizationSteps;

public abstract class AbstractConsentsApiConsentExpiredTestModule extends AbstractPhase2TestModule {

	@Override
	protected abstract Class<? extends Condition> getPostConsentValidator();

	@Override
	protected ConditionSequence getConsentAndResourceV2EndpointSequence() {
		return sequenceOf(condition(GetConsentV3Endpoint.class));
	}


	@Override
	protected void setupResourceEndpoint() {
		callAndStopOnFailure(GetResourceEndpointConfiguration.class);
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		return new OpenBankingBrazilPreAuthorizationSteps(false, false, addTokenEndpointClientAuthentication, false, false, false)
			.insertBefore(ExtractConsentIdFromConsentEndpointResponse.class, condition(getPostConsentValidator()).onFail(Condition.ConditionResult.FAILURE).dontStopOnFailure())
			.replace(FAPIBrazilAddExpirationToConsentRequest.class, condition(AddExpirationInThreeMinute.class));
	}

	@Override
	protected void requestProtectedResource() {
		runInBlock("Validating get consent response", () -> {
			callAndContinueOnFailure(WaitFor180Seconds.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(PrepareToFetchConsentRequest.class);
			callAndStopOnFailure(TransformConsentRequestForProtectedResource.class);
			call(createGetAccessTokenWithClientCredentialsSequence(addTokenEndpointClientAuthentication));
			preCallProtectedResource("Fetch consent");
			env.mapKey("consent_endpoint_response_full", "resource_endpoint_response_full");
			callAndStopOnFailure(EnsureConsentRejectAspspMaxDateReached.class);
			env.unmapKey("consent_endpoint_response_full");
		});

		runInBlock("Delete consent v3-1", () -> {
			callAndContinueOnFailure(PrepareToDeleteConsent.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureConsentResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
			env.mapKey(EnsureErrorResponseCodeWasConsentimentoEmStatusRejeitado.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
			callAndContinueOnFailure(EnsureErrorResponseCodeWasConsentimentoEmStatusRejeitado.class, Condition.ConditionResult.FAILURE);
			env.unmapKey(EnsureErrorResponseCodeWasConsentimentoEmStatusRejeitado.RESPONSE_ENV_KEY);
		});
	}

	@Override
	protected void validateResponse() {
		// not needed in this test
	}

	@Override
	protected void performPreAuthorizationSteps() {
		super.performPreAuthorizationSteps();
		callAndContinueOnFailure(WaitFor2Seconds.class, Condition.ConditionResult.FAILURE);
	}


	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainAccessTokenWithClientCredentials(clientAuthSequence);
	}


}
