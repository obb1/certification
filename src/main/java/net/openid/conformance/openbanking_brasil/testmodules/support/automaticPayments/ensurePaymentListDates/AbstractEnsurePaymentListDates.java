package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Optional;

public abstract class AbstractEnsurePaymentListDates extends AbstractCondition {

	public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonObject body = extractBodyFromEnv(env);
		JsonArray data = body.getAsJsonArray("data");
		if (data.size() != amountOfPaymentsExpected()) {
			throw error("The amount of payments present in the response is different from the expected",
				args("amount", data.size(), "expected", amountOfPaymentsExpected()));
		}

		for (JsonElement paymentElement : data) {
			JsonObject payment = paymentElement.getAsJsonObject();
			paymentDateValidation(payment);
		}

		logSuccess("Payment date validation successfully completed");

		return env;
	}

	protected JsonObject extractBodyFromEnv(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
				.map(JsonElement::getAsJsonObject)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	protected void paymentDateValidation(JsonObject payment) {
		String dateStr = Optional.ofNullable(payment.get("date"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Payment is missing the date field", args("payment", payment)));

		LocalDate date = LocalDate.parse(dateStr);
		LocalDate today = LocalDate.now(ZoneOffset.UTC);

		if (!validateDate(date, today)) {
			throw error("Payment date is not valid", args("payment", payment));
		}
	}

	protected abstract int amountOfPaymentsExpected();
	protected abstract boolean validateDate(LocalDate date, LocalDate today);
}
