package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.ensurePaymentListDates;

import java.time.LocalDate;

public class EnsureTwoPaymentsForTodayOrBefore extends AbstractEnsurePaymentListDates {

	@Override
	protected int amountOfPaymentsExpected() {
		return 2;
	}

	@Override
	protected boolean validateDate(LocalDate date, LocalDate today) {
		return date.equals(today) || date.isBefore(today);
	}
}
