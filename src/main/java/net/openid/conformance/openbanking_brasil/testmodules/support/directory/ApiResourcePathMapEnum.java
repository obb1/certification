package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

import java.util.HashMap;
import java.util.Map;

public enum ApiResourcePathMapEnum {
	PAYMENTS_CONSENTS("payments-consents", "payments"),
	PAYMENTS("payments-pix", "payments"),
	ACCOUNTS("accounts", "accounts"),

	CONSENTS("consents", "consents"),

	CREDIT_CARD_ACCOUNTS("credit-cards-accounts", "credit-card"),

	CUSTOMERS_BUSINESS("customers-business", "business"),

	CUSTOMERS_PERSONAL("customers-personal", "personal"),

	FINANCINGS("financings", "financings"),

	INVOICE_FINANCINGS("invoice-financings", "invoice-financings"),

	LOANS("loans", "loans"),

	RESOURCES("resources", "resources"),

	UNARRANGED_ACCOUNT_OVERDRAFT("unarranged-accounts-overdraft", "unarranged-overdraft"),
	CREDIT_FIXED_INCOMES("credit-fixed-incomes","credit-fixed-incomes"),

	BANK_FIXED_INCOMES("bank-fixed-incomes","bank-fixed-incomes"),

	VARIABLE_INCOMES("variable-incomes","variable-incomes"),

	TREASURE_TITLES("treasure-titles","treasure-titles"),

	FUNDS("funds","funds");

	private static final Map<String, String> map = new HashMap<>();

	static {
		for (ApiResourcePathMapEnum e : ApiResourcePathMapEnum.values()) {
			map.put(e.key, e.value);
		}
	}

	private final String key;
	private final String value;

	private ApiResourcePathMapEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public static String getValue(String key) {
		return map.get(key);
	}
}
