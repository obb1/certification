package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;


import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPaymentsConsentsApiNegativeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.GetPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsConsentOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.payments.v4.PostPaymentsPixOASValidatorV4;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv4.SetPaymentsDataToBeJsonArray;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "payments_api_consents_negative_test-module_v4",
	displayName = "Ensure an error status is sent on the POST Payments API in  different inconsistent scenarios",
	summary = "Ensure an error status is sent on the POST Payments API in  different inconsistent scenarios" +
		"\u2022 Ensure error when Payment Type is \"BAD\"\n" +
		"Calls POST Consents Endpoint with payment type as \"BAD\"\n" +
		"Expects 422 PARAMETRO_INVALIDO - validate error message\n" +
		"2. Ensure error when an invalid person type is sent\n" +
		"Calls POST Consents Endpoint with person type as \"INVALID\"\n" +
		"Expects 422 PARAMETRO_INVALIDO - Validate error message\n" +
		"3. Ensure error when invalid currency is sent\n" +
		"Calls POST Consents Endpoint with currency as \"XXX\"\n" +
		"Expects 422 PARAMETRO_INVALIDO  - Validate error message\n" +
		"4. Ensure error when invalid date is sent\n" +
		"Calls POST Consents Endpoint with past date value\n" +
		"Expects 422 DATA_PAGAMENTO_INVALIDA - Validate error message\n" +
		"5. Ensure x-fapi-interaction-id is validated, and sent back when not requested\n" +
		"Calls POST consents Endpoint with the x-fapi-interaction-id as \"123456\"\n" +
		"Expects 400 or 422 PARAMETRO_INVALIDO - Validate Error message, ensure a valid x-fapi-interaction-id is sent on the response and its a UUID as RFC4122\n" +
		"6. Ensure POST Consents is successful when valid payload is sent\n" +
		"Calls POST Consents Endpoint with valid payload, using DICT as the localInstrument\n" +
		"Expects 201 - Validate response and ensure x-fapi-interaction-id is the same as the one sent\n" +
		"Redirects the user to authorize the created consent- Call GET Consent\n" +
		"Expects 200 - Validate if status is \"AUTHORISED\"",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)

public class PaymentsConsentsApiNegativeTestModuleV4 extends AbstractPaymentsConsentsApiNegativeTestModule {

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(SetPaymentsDataToBeJsonArray.class);
		super.onConfigure(config, baseUrl);
	}

	@Override
	protected  Class<? extends Condition> paymentConsentErrorValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentConsentValidator() {
		return PostPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> postPaymentValidator() {
		return PostPaymentsPixOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentConsentValidator() {
		return GetPaymentsConsentOASValidatorV4.class;
	}

	@Override
	protected Class<? extends Condition> getPaymentValidator() {
		return GetPaymentsPixOASValidatorV4.class;
	}
}
