package net.openid.conformance.openbanking_brasil.testmodules.support.oasValidators.variableIncomes.v1;

public class GetVariableIncomesTransactionsV1OASValidator extends AbstractVariableIncomesTransactionsOASValidatorV1 {

	@Override
	protected String getEndpointPath() {
		return "/investments/{investmentId}/transactions";
	}


}
