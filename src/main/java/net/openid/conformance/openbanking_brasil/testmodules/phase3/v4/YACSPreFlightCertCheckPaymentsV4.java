package net.openid.conformance.openbanking_brasil.testmodules.phase3.v4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.AbstractPreFlightCertCheckPayments;
import net.openid.conformance.openbanking_brasil.testmodules.support.StopIfWarning;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentConsentsV4Endpoint;
import net.openid.conformance.openbanking_brasil.testmodules.support.directory.GetPaymentV4Endpoint;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "fvp-preflight-cert-check-payments-test-v4",
	displayName = "Pre-Flights test that ensures that tested server accepted a DCR request and all the required tested URIs are registered within the Directory",
	summary = """
		Make sure the brazilCpf has been provided
		Make sure a client_id has been generated on the DCR
		Call the Tested Server Token endpoint with client_credentials grant - Make sure that server returns a 200
		Call the Directory participants endpoint for either Sandbox or Production, depending on the used platform
		Make sure The Provided Well-Known is registered within the Directory
		Make sure that the server has registered a ApiFamilyType of value payments-consents with ApiVersion of value “4.0.0\". Return the ApiEndpoint that ends with payments/v4/consents +
		Make sure that the server has registered a ApiFamilyType of value payments-pix with ApiVersion of value “4.0.0\". Return the ApiEndpoint that ends with payments/v4/pix/payments
	""",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	configurationFields = {
		"server.authorisationServerId",
		"client.client_id",
		"client.jwks",
		"directory.client_id",
		"resource.brazilCpf",
		"resource.brazilCnpj",
		"resource.consentUrl"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"resource.consentSyncTime"
})
public class YACSPreFlightCertCheckPaymentsV4 extends AbstractPreFlightCertCheckPayments {

	@Override
	protected ConditionSequence getPaymentAndPaymentConsentEndpoints() {
		return sequenceOf(
			condition(GetPaymentConsentsV4Endpoint.class),
			condition(StopIfWarning.class),
			condition(GetPaymentV4Endpoint.class)
		);
	}
}
