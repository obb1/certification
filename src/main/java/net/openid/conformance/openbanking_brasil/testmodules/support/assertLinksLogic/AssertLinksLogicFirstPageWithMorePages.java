package net.openid.conformance.openbanking_brasil.testmodules.support.assertLinksLogic;

import com.google.gson.JsonObject;

public class AssertLinksLogicFirstPageWithMorePages extends AbstractAssertLinksLogic {

	@Override
	protected void validateLinksLogic(JsonObject links) {
		if (links.get("prev") != null) {
			throw error("First page should not contain \"prev\" link", args("links", links));
		}

		String self = extractMandatoryLink(links, "self", "for any response");
		String next = extractMandatoryLink(links, "next", "if it is not the last page");

		validateSuccessiveLinks(self, "self", next, "next");
	}
}
