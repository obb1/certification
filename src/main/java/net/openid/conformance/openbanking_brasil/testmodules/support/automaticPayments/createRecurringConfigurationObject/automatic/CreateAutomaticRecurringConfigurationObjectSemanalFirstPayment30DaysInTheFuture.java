package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.createRecurringConfigurationObject.automatic;

import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.enums.RecurringPaymentsConsentsAutomaticPixIntervalEnum;

import java.time.LocalDate;
import java.time.ZoneId;

public class CreateAutomaticRecurringConfigurationObjectSemanalFirstPayment30DaysInTheFuture extends AbstractCreateAutomaticRecurringConfigurationObject {

	@Override
	protected RecurringPaymentsConsentsAutomaticPixIntervalEnum getInterval() {
		return RecurringPaymentsConsentsAutomaticPixIntervalEnum.SEMANAL;
	}

	@Override
	protected String getFirstPaymentDate() {
		LocalDate today = LocalDate.now(ZoneId.of("America/Sao_Paulo"));
		return today.plusDays(30).format(DATE_FORMATTER);
	}
}
