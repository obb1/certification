package net.openid.conformance.openbanking_brasil.testmodules.support.directory;

public class GetAutomaticPaymentPixRecurringV1Endpoint extends AbstractGetXFromAuthServer {

		@Override
		protected String getEndpointRegex() {
			return "^(https://)(.*?)(/open-banking/automatic-payments/v\\d+/pix/recurring-payments)$";
		}

		@Override
		protected String getApiFamilyType() {
			return "payments-pix-recurring-payments";
		}

		@Override
		protected String getApiVersionRegex() {
			return "^(1.[0-9].[0-9])$";
		}

		@Override
		protected boolean isResource() {
			return true;
		}
	}
