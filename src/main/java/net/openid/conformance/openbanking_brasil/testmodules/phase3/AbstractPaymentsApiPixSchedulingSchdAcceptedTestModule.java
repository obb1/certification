package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.AskForScreenshotWithScheduledDate;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureScheduledPaymentDateIs.EnsureScheduledDateIsTodayPlus350;

public abstract class AbstractPaymentsApiPixSchedulingSchdAcceptedTestModule extends AbstractPaymentsPixSchedulingTestModule {

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
	}

	@Override
	protected void createPlaceholder() {
		callAndStopOnFailure(AskForScreenshotWithScheduledDate.class);
		env.putString("error_callback_placeholder", env.getString("payments_placeholder"));
	}

	@Override
	protected void performRedirect() {
		performRedirectWithPlaceholder();
	}


	@Override
	protected Class<? extends Condition> getPaymentScheduleDate() {
		return EnsureScheduledDateIsTodayPlus350.class;
	}

	@Override
	protected void executePostPollingSteps() {
		// No extra steps are needed
	}
}
