package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.editConsentBody;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

public class EditRecurringPaymentsConsentBodyToSetUserDefinedExpirationDateTime extends AbstractCondition {

	@Override
	@PreEnvironment(required = "resource")
	public Environment evaluate(Environment env) {
		JsonObject data = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.map(consentElement -> consentElement.getAsJsonObject().getAsJsonObject("data"))
			.orElseThrow(() -> error("Unable to find data field in consents payload"));

		String expirationDateTime = extractExpirationDateTimeFromEnv(env);
		data.addProperty("expirationDateTime", expirationDateTime);

		logSuccess("The expirationDateTime field has been set to the value defined by the user",
			args("data", data));

		return env;
	}

	private String extractExpirationDateTimeFromEnv(Environment env) {
		String expirationDateTime = env.getString("resource", "expirationDateTime");
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX");
			ZonedDateTime.parse(expirationDateTime, formatter);
			return expirationDateTime;
		} catch (DateTimeParseException e) {
			throw error("The value defined for resource.expirationDateTime is not valid", args("expirationDateTime", expirationDateTime));
		}
	}
}
