package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.createRequestBody;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonObjectBuilder;

import java.util.Map;
import java.util.Optional;

public class CreatePatchEnrollmentsRequestBodyToRequestEntityClaims extends AbstractCondition {

    @Override
    @PreEnvironment(required = "config")
    @PostEnvironment(required = "resource_request_entity_claims")
    public Environment evaluate(Environment env) {
        String identification = Optional.ofNullable(env.getString("config", "resource.loggedUserIdentification"))
            .orElseThrow(() -> error("Could not find identification in config"));

		JsonObjectBuilder patchRequestBuilder = new JsonObjectBuilder()
			.addFields("data.cancellation.cancelledBy.document", Map.of("identification", identification, "rel", "CPF"));

		if (!Strings.isNullOrEmpty(env.getString("rejection_reason"))) {
			patchRequestBuilder.addFields("data.cancellation.reason", Map.of("rejectionReason", env.getString("rejection_reason")));
		} else if (!Strings.isNullOrEmpty(env.getString("revocation_reason"))) {
			patchRequestBuilder.addFields("data.cancellation.reason", Map.of("revocationReason", env.getString("revocation_reason")));
		} else {
			throw error("There is no rejection nor revocation reason in the environment");
		}
        JsonObject patchRequest = patchRequestBuilder.build();

        env.putObject("resource_request_entity_claims", patchRequest);

        logSuccess("Patch enrollments request body was created and added to environment",
            args("patch request body", patchRequest));

        return env;
    }
}
