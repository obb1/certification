package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.condition.client.CreateTokenEndpointRequestForClientCredentialsGrant;
import net.openid.conformance.condition.client.EnsureContentTypeApplicationJwt;
import net.openid.conformance.condition.client.ExtractAccessTokenFromTokenResponse;
import net.openid.conformance.condition.client.ExtractSignedJwtFromResourceResponse;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseSigningAlg;
import net.openid.conformance.condition.client.FAPIBrazilValidateResourceResponseTyp;
import net.openid.conformance.condition.client.FetchServerKeys;
import net.openid.conformance.condition.client.ValidateResourceResponseJwtClaims;
import net.openid.conformance.condition.client.ValidateResourceResponseSignature;
import net.openid.conformance.openbanking_brasil.testmodules.RememberOriginalScopes;
import net.openid.conformance.openbanking_brasil.testmodules.ResetScopesToConfigured;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseWasJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.LoadOldAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.ResetPaymentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SaveAccessToken;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodeLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SelectDICTCodePixLocalInstrument;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetIncorrectAmountInPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetIncorrectCurrencyPayment;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.StoreScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidatePaymentAndConsentHaveSameProperties;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas201Or422;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureResourceResponseCodeWas401;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasAcsc;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensurePaymentStatus.EnsurePaymentStatusWasRjct;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.ensurePaymentsRejection.EnsurePaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;
import org.springframework.http.HttpStatus;

import java.util.Optional;

public abstract class AbstractPaymentsApiNegativeTestModule extends AbstractPaymentUnhappyPathTestModule {

	private int batch = 1;

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		ConditionSequence preAuthSteps = super.createOBBPreauthSteps();
		if (batch > 1) {
			preAuthSteps = preAuthSteps
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class,
					condition(ResetScopesToConfigured.class))
				.insertAfter(ExtractAccessTokenFromTokenResponse.class,
					condition(SaveAccessToken.class));
		} else {
			preAuthSteps = preAuthSteps
				.insertBefore(CreateTokenEndpointRequestForClientCredentialsGrant.class,
					condition(RememberOriginalScopes.class));
		}
		return preAuthSteps;
	}

	@Override
	protected void configureDictInfo() {
		callAndStopOnFailure(SelectDICTCodeLocalInstrument.class);
		callAndStopOnFailure(SelectDICTCodePixLocalInstrument.class);
		callAndStopOnFailure(ValidatePaymentAndConsentHaveSameProperties.class);
		eventLog.startBlock("Storing authorisation endpoint");
		callAndStopOnFailure(StoreScope.class);
		callAndStopOnFailure(SetIncorrectCurrencyPayment.class);
	}

	@Override
	protected void validatePaymentRejectionReasonCode() {
		if (batch < 3) {
			callAndContinueOnFailure(EnsurePaymentStatusWasRjct.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsurePaymentRejectionReasonCodeWasPagamentoDivergenteConsentimento.class, Condition.ConditionResult.FAILURE);
		}

	}

	@Override
	protected void validateResponse() {
		int status = Optional.ofNullable(env.getInteger("resource_endpoint_response_full", "status"))
			.orElseThrow(() -> new TestFailureException(getId(), "Could not find resource_endpoint_response_full"));

		if (status == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
			perform422ResponseActions();
		} else if (status == HttpStatus.UNAUTHORIZED.value()) {
			perform401ResponseActions();
		} else {
			performNon422ResponseActions();
		}
	}

	protected void perform401ResponseActions() {
		eventLog.log(getName(), "Validating 401 response");
		performErrorValidation();
	}

	@Override
	protected void validate422ErrorResponseCode() {
		if (batch == 1 || batch == 2) {
			env.mapKey(EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento.RESPONSE_ENV_KEY, "resource_endpoint_response_full");
			callAndStopOnFailure(EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento.class);
			env.unmapKey(EnsureErrorResponseCodeFieldWasPagamentaDivergenteConsentimento.RESPONSE_ENV_KEY);

		}

	}

	@Override
	protected Class<? extends Condition> getExpectedPaymentResponseCode() {
		if (batch == 1 || batch == 2) {
			return EnsureResourceResponseCodeWas201Or422.class;
		}

		if (batch == 3) {
			return EnsureResourceResponseCodeWas401.class;
		}
		return EnsureResourceResponseCodeWas201.class;

	}

	@Override
	protected ConditionSequence getPixPaymentSequence() {
		if (batch == 3) {
			return super.getPixPaymentSequence()
				.insertBefore(CallProtectedResource.class,
					sequenceOf(condition(LoadOldAccessToken.class), condition(SaveAccessToken.class)))
				.replace(EnsureContentTypeApplicationJwt.class, condition(EnsureResponseWasJson.class))
				.skip(ExtractSignedJwtFromResourceResponse.class, "JSON response is expected")
				.skip(FAPIBrazilValidateResourceResponseSigningAlg.class, "JSON response is expected")
				.skip(FAPIBrazilValidateResourceResponseTyp.class, "JSON response is expected")
				.skip(FetchServerKeys.class, "JSON response is expected")
				.skip(ValidateResourceResponseSignature.class, "JSON response is expected")
				.skip(ValidateResourceResponseJwtClaims.class, "JSON response is expected");
		}
		return super.getPixPaymentSequence();
	}

	@Override
	protected void validateFinalState() {
		if (batch >= 3) {
			callAndStopOnFailure(EnsurePaymentStatusWasAcsc.class);
		}
	}

	@Override
	protected void onPostAuthorizationFlowComplete() {
		switch (batch) {
			case 1: {
				eventLog.startBlock("2. Ensure error when the amount is different between consent and payment");
				validationStarted = false;
				batch++;
				callAndStopOnFailure(ResetPaymentRequest.class);
				callAndStopOnFailure(SetIncorrectAmountInPayment.class);
				callAndStopOnFailure(SetScope.class);
				performAuthorizationFlow();
				break;
			}
			case 2: {
				eventLog.startBlock("3. Ensure POST payments can only be called with authorization code flow");
				validationStarted = false;
				batch++;
				callAndStopOnFailure(ResetPaymentRequest.class);
				callAndStopOnFailure(SetScope.class);
				performAuthorizationFlow();
				break;
			}
			case 3: {
				eventLog.startBlock("4. Ensure POST payment is successful when valid payload is sent");
				validationStarted = false;
				batch++;
				callAndStopOnFailure(ResetPaymentRequest.class);
				callAndStopOnFailure(SetScope.class);
				performAuthorizationFlow();
				break;
			}
			default: {
				fireTestFinished();
				break;
			}
		}
	}
}
