package net.openid.conformance.openbanking_brasil.testmodules.validators.payments.v3.enums;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum CreateConsentErrorEnumV3 {
	FORMA_PAGAMENTO_INVALIDA,
	DATA_PAGAMENTO_INVALIDA,
	DETALHE_PAGAMENTO_INVALIDO,
	PARAMETRO_NAO_INFORMADO,
	PARAMETRO_INVALIDO,
	ERRO_IDEMPOTENCIA,
	NAO_INFORMADO;

	public static Set<String> toSet(){
		return Stream.of(values())
			.map(Enum::name)
			.collect(Collectors.toSet());
	}
}
