package net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.AddFAPIAuthDateToResourceEndpointRequest;
import net.openid.conformance.condition.client.AddFAPIInteractionIdToResourceEndpointRequest;
import net.openid.conformance.condition.client.CheckForFAPIInteractionIdInResourceResponse;
import net.openid.conformance.condition.client.CreateEmptyResourceEndpointRequestHeaders;
import net.openid.conformance.condition.client.CreateRandomFAPIInteractionId;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilCreateBadConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenBankingCreateConsentRequest;
import net.openid.conformance.condition.client.SetConsentsScopeOnTokenEndpointRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractFvpDcrXConsentsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallConsentEndpointWithBearerTokenAnyHttpMethod;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.SetContentTypeApplicationJson;
import net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.CreateInvalidFAPIInteractionId;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.AbstractEnsureConsentResponseCode;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas400;
import net.openid.conformance.openbanking_brasil.testmodules.support.ensureResponseCode.EnsureConsentResponseCodeWas422;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.ScopesAndPermissionsBuilder;
import net.openid.conformance.openbanking_brasil.testmodules.support.scopesAndPermissionsBuilder.enums.OPFScopesEnum;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractFvpConsentApiBadConsentsTestModule extends AbstractFvpDcrXConsentsTestModule {

	protected abstract Class<? extends Condition> postConsentValidator();

	@Override
	protected Class<? extends AbstractCondition> setConsentScopeOnTokenEndpointRequest() {
		return SetConsentsScopeOnTokenEndpointRequest.class;
	}

	@Override
	protected void runTests() {
		ScopesAndPermissionsBuilder scopesAndPermissionsBuilder = new ScopesAndPermissionsBuilder(env,eventLog);
		scopesAndPermissionsBuilder.addScopes(OPFScopesEnum.CONSENTS).build();

		eventLog.startBlock("Create incompatible consents");
		call(createConsentSequence(EnsureConsentResponseCodeWas422.class)
			.replace(FAPIBrazilOpenBankingCreateConsentRequest.class, condition(FAPIBrazilCreateBadConsentRequest.class)));
		env.mapKey(EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto.RESPONSE_ENV_KEY, "consent_endpoint_response_full");
		callAndStopOnFailure(EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto.class);
		env.unmapKey(EnsureErrorResponseCodeWasPermissoesPfPjEmConjunto.RESPONSE_ENV_KEY);
		eventLog.endBlock();

		eventLog.startBlock("Create consent without x-fapi-interaction-id");
		call(createConsentSequence(EnsureConsentResponseCodeWas400.class)
			.skip(CreateRandomFAPIInteractionId.class, "x-fapi-interaction-id will not be sent")
			.skip(AddFAPIInteractionIdToResourceEndpointRequest.class, "x-fapi-interaction-id will not be sent"));
		eventLog.endBlock();

		eventLog.startBlock("Create consent with an invalid x-fapi-interaction-id");
		call(createConsentSequence(EnsureConsentResponseCodeWas400.class)
			.replace(CreateRandomFAPIInteractionId.class, condition(CreateInvalidFAPIInteractionId.class)));
		eventLog.endBlock();
	}

	protected ConditionSequence createConsentSequence(Class<? extends AbstractEnsureConsentResponseCode> ensureConsentResponseCode) {
		return sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(FAPIBrazilOpenBankingCreateConsentRequest.class),
			condition(CreateEmptyResourceEndpointRequestHeaders.class),
			condition(FAPIBrazilAddExpirationToConsentRequest.class),
			condition(AddFAPIAuthDateToResourceEndpointRequest.class),
			condition(SetContentTypeApplicationJson.class),
			condition(CreateRandomFAPIInteractionId.class),
			condition(AddFAPIInteractionIdToResourceEndpointRequest.class),
			condition(CallConsentEndpointWithBearerTokenAnyHttpMethod.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(ensureConsentResponseCode),
			condition(postConsentValidator())
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE),
			condition(CheckForFAPIInteractionIdInResourceResponse.class)
				.dontStopOnFailure().onFail(Condition.ConditionResult.FAILURE)
		);
	}

	@Override
	protected void insertConsentProdValues() {}
}
