package net.openid.conformance.openbanking_brasil.testmodules.support.enrollments;

import com.google.gson.JsonObject;
import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.AbstractSignJWT;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.JsonUtils;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;
import java.util.UUID;

public class SignEnrollmentsRequestWithInvalidPrivateKey extends AbstractSignJWT {

	@Override
	@PreEnvironment(required = "resource_request_entity_claims")
	@PostEnvironment(strings = "resource_request_entity")
	public Environment evaluate(Environment env) {
		try {
			JsonObject claims = env.getObject("resource_request_entity_claims");
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(2048);
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			String kid = UUID.randomUUID().toString();

			JWK jwk = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
				.privateKey((RSAPrivateKey) keyPair.getPrivate())
				.keyID(kid)
				.keyUse(KeyUse.SIGNATURE)
				.algorithm(Algorithm.parse("RS256"))
				.build();

			JsonObject jwks = JsonUtils.createBigDecimalAwareGson().fromJson(new JWKSet(jwk).toString(false), JsonObject.class);
			log("Created random jwks", Map.of("jwks", jwks));
			return signJWT(env, claims, jwks, true);
		} catch (NoSuchAlgorithmException e) {
			throw error("Could not create random RSA key pair to sign enrollments request entity claims");
		}
	}

	@Override
	protected void logSuccessByJWTType(Environment env, JWTClaimsSet claimSet, JWK jwk, JWSHeader header, String jws, JsonObject verifiableObj) {
		env.putString("resource_request_entity", jws);
		logSuccess("Signed the request", args(
			"header", header.toString(),
			"claims", claimSet.toString(),
			"key", jwk.toJSONString()));
	}

}
