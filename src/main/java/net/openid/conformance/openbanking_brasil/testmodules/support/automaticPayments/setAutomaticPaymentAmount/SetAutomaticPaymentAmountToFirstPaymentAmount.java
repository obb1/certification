package net.openid.conformance.openbanking_brasil.testmodules.support.automaticPayments.setAutomaticPaymentAmount;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.Optional;

public class SetAutomaticPaymentAmountToFirstPaymentAmount extends AbstractSetAutomaticPaymentAmount {

	protected static String firstPaymentAmount;

	@Override
	@PreEnvironment(required = {"resource", "recurring_configuration_object"})
	public Environment evaluate(Environment env) {
		JsonObject recurringConfigObj = env.getObject("recurring_configuration_object");
		firstPaymentAmount = Optional.ofNullable(recurringConfigObj.getAsJsonObject("automatic"))
			.map(automatic -> automatic.getAsJsonObject("firstPayment"))
			.map(firstPayment -> firstPayment.get("amount"))
			.map(OIDFJSON::getString)
			.orElseThrow(() -> error("Unable to find firstPayment amount in recurring configuration object",
				args("recurring configuration object", recurringConfigObj)));

		return super.evaluate(env);
	}

	@Override
	protected String automaticPaymentAmount() {
		return firstPaymentAmount;
	}
}
