package net.openid.conformance.openbanking_brasil.testmodules.phase3;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPaymentConsentRequestBodyToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePixScheduleDateIsInPast;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsurePixScheduleDateIsTooFarInFuture;
import net.openid.conformance.openbanking_brasil.testmodules.support.ObtainPaymentsAccessTokenWithClientCredentials;
import net.openid.conformance.openbanking_brasil.testmodules.support.PrepareToPostConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsurePaymentDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsureScheduledPaymentDateIsToday;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.EnsureScheduledPaymentDateIsTomorrow;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.RemovePaymentDateFromConsentRequest;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasDataPagamentoInvalida;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.ensureErrorResponse.EnsureErrorResponseCodeFieldWasParametroNaoInformado;
import net.openid.conformance.openbanking_brasil.testmodules.support.sequences.PaymentConsentErrorTestingSequence;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.TestFailureException;

import java.util.Optional;

public abstract class AbstractPaymentsApiPixSchedulingDatesUnhappyTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	protected abstract Class<? extends Condition> paymentConsentErrorValidator();
	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndStopOnFailure(AddPaymentConsentRequestBodyToConfig.class);
	}
	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void runTests() {
		runInBlock("[1] Create consent with request payload with both the date and the schedule.single.date fields", () -> {
			saveBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsurePaymentDateIsTomorrow.class),
					condition(EnsureScheduledPaymentDateIsTomorrow.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[2] Create consent with request payload set with schedule.single.date field defined as today", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsureScheduledPaymentDateIsToday.class),
					condition(RemovePaymentDateFromConsentRequest.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[3] Create consent with request payload set with schedule.single.date field defined as yesterday D-1", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsurePixScheduleDateIsInPast.class),
					condition(RemovePaymentDateFromConsentRequest.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[4] Create consent with request payload set with schedule.single.date field defined as yesterday D+740", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				sequenceOf(condition(EnsurePixScheduleDateIsTooFarInFuture.class),
					condition(RemovePaymentDateFromConsentRequest.class)));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasDataPagamentoInvalida.class);
		});

		runInBlock("[5] Create consent with request payload set without the date field provided", () -> {
			restoreBrazilPaymentConsent();
			ConditionSequence consentCreationSeq = consentCreationConditionSequence();
			consentCreationSeq.insertAfter(FAPIBrazilCreatePaymentConsentRequest.class,
				condition(RemovePaymentDateFromConsentRequest.class));

			call(consentCreationSeq);
			validateResponse(EnsureErrorResponseCodeFieldWasParametroNaoInformado.class);
		});
	}


	protected void saveBrazilPaymentConsent() {
		JsonObject brazilPaymentConsent = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsent"))
			.orElseThrow(() -> new TestFailureException(getId(),"Could not extract brazilPaymentConsent")).getAsJsonObject();

		env.putObject("resource", "brazilPaymentConsentSave", brazilPaymentConsent.deepCopy());
	}

	protected void restoreBrazilPaymentConsent() {
		JsonObject brazilPaymentConsentSave = Optional.ofNullable(env.getElementFromObject("resource", "brazilPaymentConsentSave"))
			.orElseThrow(() -> new TestFailureException(getId(),"Could not extract brazilPaymentConsentSave")).getAsJsonObject();
		env.putObject("resource", "brazilPaymentConsent", brazilPaymentConsentSave.deepCopy());
	}

	private ConditionSequence consentCreationConditionSequence() {
		return sequenceOf(
			condition(PrepareToPostConsentRequest.class),
			condition(FAPIBrazilCreatePaymentConsentRequest.class),
			sequence(PaymentConsentErrorTestingSequence.class)
		);
	}

	private void validateResponse(Class<? extends Condition> ensureErrorResponse) {

		callAndContinueOnFailure(ensureErrorResponse, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(paymentConsentErrorValidator(), Condition.ConditionResult.FAILURE);
	}

}
