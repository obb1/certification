package net.openid.conformance.openbanking_brasil.testmodules.enrollments.abstractModule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentCancelledFromWas.EnsureEnrollmentCancelledFromWasDetentora;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentRejectionReasonWas.EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoRiskSignals;
import net.openid.conformance.openbanking_brasil.testmodules.support.enrollments.ensureEnrollmentStatusWas.EnsureEnrollmentStatusWasRejected;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv3.WaitFor5Minutes;

public abstract class AbstractEnrollmentsApiExpiredPreRiskSignalEnrollmentTestModule extends AbstractEnrollmentsApiTestModule {

	@Override
	protected void performAuthorizationFlow() {
		performPreAuthorizationSteps();
		onPostAuthorizationFlowComplete();
	}

	@Override
	protected void performPreAuthorizationSteps() {
		postAndValidateEnrollments();
		callAndStopOnFailure(WaitFor5Minutes.class);
		createNewClientCredentialsToken();
		getAndValidateEnrollmentsStatus(new EnsureEnrollmentStatusWasRejected(), "REJECTED");
		runInBlock("Validate enrollments fields related to cancellation", () -> {
			callAndContinueOnFailure(EnsureEnrollmentCancelledFromWasDetentora.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureEnrollmentRejectionReasonWasRejeitadoTempoExpiradoRiskSignals.class, Condition.ConditionResult.FAILURE);
		});
	}

	@Override
	protected void executeTestSteps() {}
}
