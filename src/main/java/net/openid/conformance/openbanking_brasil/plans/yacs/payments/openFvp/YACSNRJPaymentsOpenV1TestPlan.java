package net.openid.conformance.openbanking_brasil.plans.yacs.payments.openFvp;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v1.FvpEnrollmentsApiPreFlightTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiInvalidChallengeOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiInvalidOriginOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiInvalidPublicKeyOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiInvalidRpIdOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiInvalidStatusSignOptionsOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiPaymentsCoreOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiPaymentsKeysSwapOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiPaymentsPreEnrollmentOpenTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.openFvp.v1.FvpEnrollmentsApiPaymentsUnmatchingFieldsOpenTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;

@PublishTestPlan(
	testPlanName = "fvp-no_redirect_payments_open_test-plan-v1",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_NO_REDIRECT_PAYMENTS_V1,
	summary = "Open Finance Brasil Functional Production Tests - FVP - No Redirect Payments Version 1"
)
public class YACSNRJPaymentsOpenV1TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					FvpEnrollmentsApiPreFlightTestModuleV1.class,
					FvpEnrollmentsApiPaymentsCoreOpenTestModuleV1.class,
					FvpEnrollmentsApiInvalidChallengeOpenTestModuleV1.class,
					FvpEnrollmentsApiInvalidOriginOpenTestModuleV1.class,
					FvpEnrollmentsApiInvalidPublicKeyOpenTestModuleV1.class,
					FvpEnrollmentsApiInvalidRpIdOpenTestModuleV1.class,
					FvpEnrollmentsApiPaymentsPreEnrollmentOpenTestModuleV1.class,
					FvpEnrollmentsApiInvalidStatusSignOptionsOpenTestModuleV1.class,
					FvpEnrollmentsApiPaymentsKeysSwapOpenTestModuleV1.class,
					FvpEnrollmentsApiPaymentsUnmatchingFieldsOpenTestModuleV1.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}

