package net.openid.conformance.openbanking_brasil.plans.phase2.v3;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v3.PreFlightCheckV3Module;
import net.openid.conformance.openbanking_brasil.testmodules.v3.resources.ResourcesApiOperationalLimitsTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.resources.ResourcesApiTestModuleCorrect200V3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.resources.ResourcesApiTestModuleUnavailableV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.resources.ResourcesApiTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.resources.ResourcesApiXFapiTestModuleV3;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "resources_test-plan_v3",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION3,
	displayName = PlanNames.RESOURCES_API_PLAN_NAME_V3,
	summary = PlanNames.RESOURCES_API_PLAN_NAME_V3
)
public class ResourcesApiTestPlanV3 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV3Module.class,
					ResourcesApiTestModuleV3.class,
					ResourcesApiTestModuleCorrect200V3.class,
					ResourcesApiXFapiTestModuleV3.class,
					ResourcesApiTestModuleUnavailableV3.class,
					ResourcesApiOperationalLimitsTestModuleV3.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
