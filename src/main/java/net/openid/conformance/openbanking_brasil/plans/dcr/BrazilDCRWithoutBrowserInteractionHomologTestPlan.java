package net.openid.conformance.openbanking_brasil.plans.dcr;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.dcr.DcrBrcac2022SupportFvpTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.PaymentsConsentsServerTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;


@PublishTestPlan(
	testPlanName = "dcr-fvp-homologation_test-plan",
	displayName = PlanNames.OBB_DCR_WITHOUT_BROWSER_INTERACTION_HOMOLG_TEST_PLAN,
	profile = OBBProfile.OBB_PROFILE_DCR
)
public class BrazilDCRWithoutBrowserInteractionHomologTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {

		return List.of(
			new ModuleListEntry(
				List.of(
					DcrBrcac2022SupportFvpTestModule.class,
					PaymentsConsentsServerTestModule.class
				),
				List.of(new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString()))
			)
		);

	}
}
