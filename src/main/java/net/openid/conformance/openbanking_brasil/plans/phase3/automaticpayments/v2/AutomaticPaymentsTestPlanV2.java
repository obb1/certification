package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiExpirationDateTimeTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiInvalidScopeTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiMultipleConsentsCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiNegativeConsentsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiRejectedConsentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiRevokedConsentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiStartDateTimeTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsConsentEditionTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsConsentsCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsLimitsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsRootCnpjTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsTotalAllowedAmountTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiSweepingAccountsWrongCreditorTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-payments_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PAYMENTS_API_PHASE_3_V2_TEST_PLAN,
	summary = "Functional test for Open Finance Brasil Automatic Payments API. " +
		"These tests are designed to validate the payment initiation using Automatic Payments API and ensuring " +
		"structural integrity and content validation. The tests are based on the Automatic Payments v2 Specifications."
)
public class AutomaticPaymentsTestPlanV2 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiExpirationDateTimeTestModuleV2.class,
					AutomaticPaymentsApiInvalidScopeTestModuleV2.class,
					AutomaticPaymentsApiMultipleConsentsCoreTestModuleV2.class,
					AutomaticPaymentsApiNegativeConsentsTestModuleV2.class,
					AutomaticPaymentsApiRejectedConsentTestModuleV2.class,
					AutomaticPaymentsApiRevokedConsentTestModuleV2.class,
					AutomaticPaymentsApiStartDateTimeTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsConsentEditionTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsConsentsCoreTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsCoreTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsInvalidCnpjTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsLimitsTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsRootCnpjTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsTotalAllowedAmountTestModuleV2.class,
					AutomaticPaymentsApiSweepingAccountsWrongCreditorTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
