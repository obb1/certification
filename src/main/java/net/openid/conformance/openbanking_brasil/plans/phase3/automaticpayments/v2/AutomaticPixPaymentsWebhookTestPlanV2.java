package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v2.AutomaticPaymentsApiAutomaticPixWebhookTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-pix-payments-webhook_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PIX_PAYMENTS_API_PHASE_3_WEBHOOK_V2_TEST_PLAN,
	summary = "For servers that support MTLS client authentication, check that subjectdn can be updated using the dynamic client management endpoint."
)
public class AutomaticPixPaymentsWebhookTestPlanV2 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiAutomaticPixWebhookTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
