package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.BankFixedIncomesApiCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.BankFixedIncomesApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.BankFixedIncomesApiPaginationListConditionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.BankFixedIncomesApiPaginationTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.BankFixedIncomesApiPaginationTransactionsCurrentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.BankFixedIncomesApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.BankFixedIncomesApiTransactionDateTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.BankFixedIncomesApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.BankFixedIncomesApiXFapiTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "bank-fixed-incomes_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.BANK_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN,
	summary = PlanNames.BANK_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN
)
public class BankFixedIncomesApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					BankFixedIncomesApiCoreTestModule.class,
					BankFixedIncomesApiTransactionDateTestModule.class,
					BankFixedIncomesApiResourcesTestModule.class,
					BankFixedIncomesApiWrongPermissionsTestModule.class,
					BankFixedIncomesApiOperationalLimitsTestModule.class,
					BankFixedIncomesApiPaginationTestModule.class,
					BankFixedIncomesApiPaginationTransactionsCurrentTestModule.class,
					BankFixedIncomesApiPaginationListConditionalTestModule.class,
					BankFixedIncomesApiXFapiTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
