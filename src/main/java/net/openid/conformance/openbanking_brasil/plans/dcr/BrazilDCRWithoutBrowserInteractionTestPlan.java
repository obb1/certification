package net.openid.conformance.openbanking_brasil.plans.dcr;

import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRBadMTLSNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRClientDeletionNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRHappyFlowNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRHappyFlowVariant2NoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRHappyFlowVariantNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidJwksByValueNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidJwksUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidRedirectUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidRegistrationAccessTokenNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignatureNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRNoMTLSNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRNoRedirectUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRNoSoftwareStatementNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRTLSClientAuthNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigBadJwksUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidJwksByValueNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidRedirectUriNoAuthFlow;
import net.openid.conformance.fapi1advancedfinal.dcr_no_authorization_flow.FAPI1AdvancedFinalBrazilDCRUpdateClientConfigNoAuthFlow;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.DCRMultipleClientTest;
import net.openid.conformance.openbanking_brasil.testmodules.DcrNoSubjectTypeTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.DcrRevokedCertificateTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.DcrSandboxCredentialsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.dcr.DcrBrcac2022SupportFvpTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.directory.ApiResourceCertificationStatusTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.directory.DirectoryApiServerRegistrationTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FVPDCRAutomaticPaymentsConsentsBadLoggerUserTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FvpAutomaticPaymentsApiNegativeConsentsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FvpAutomaticPaymentsApiRejectedConsentTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.FvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiPixSchedulingDatesUnhappyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsConsentLimitTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsApiRecurringPaymentsWrongCustomQuantityTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiEnforceDICTTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiEnforceMANUTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiEnforceQRESTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiForceCheckBadSignatureTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsApiNegativeTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.FvpPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsBadLoggerUserTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentApiBadConsentsTestModuleV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentApiExtensionInvalidStatusTestModuleV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentApiNegativeTestsV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.FvpConsentsApiPermissionGroupsTestModuleV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.PaymentsConsentsServerTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.v2.yacs.FVPDCRConsentsBadLoggedUserV3;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantSelection;

import java.util.List;


@PublishTestPlan(
	testPlanName = "dcr-fvp_test-plan",
	displayName = PlanNames.OBB_DCR_WITHOUT_BROWSER_INTERACTION_TEST_PLAN,
	profile = OBBProfile.OBB_PROFILE_DCR
)
public class BrazilDCRWithoutBrowserInteractionTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {

		return List.of(
			new ModuleListEntry(
				List.of(
					FAPI1AdvancedFinalBrazilDCRHappyFlowNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRHappyFlowVariantNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRHappyFlowVariant2NoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRClientDeletionNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidRegistrationAccessTokenNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidSoftwareStatementSignatureNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoSoftwareStatementNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoMTLSNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRBadMTLSNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigBadJwksUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidJwksByValueNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRUpdateClientConfigInvalidRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRNoRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidRedirectUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidJwksUriNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRInvalidJwksByValueNoAuthFlow.class,
					FAPI1AdvancedFinalBrazilDCRTLSClientAuthNoAuthFlow.class,
					DcrSandboxCredentialsTestModule.class,
					DcrNoSubjectTypeTestModule.class,
					DCRMultipleClientTest.class,
					DcrBrcac2022SupportFvpTestModule.class,
					PaymentsConsentsServerTestModule.class,
					DirectoryApiServerRegistrationTestModuleV2.class,
					ApiResourceCertificationStatusTestModule.class,
					DcrRevokedCertificateTestModule.class,
					PaymentsConsentsBadLoggerUserTestModuleV4.class,
					FVPDCRAutomaticPaymentsConsentsBadLoggerUserTestModuleV1.class,
					FVPDCRConsentsBadLoggedUserV3.class,
					FvpPaymentsApiPixSchedulingDatesUnhappyTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsConsentLimitTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsWrongCustomQuantityTestModuleV4.class,
					FvpPaymentsConsentsApiEnforceDICTTestModuleV4.class,
					FvpPaymentsConsentsApiEnforceMANUTestModuleV4.class,
					FvpPaymentsConsentsApiEnforceQRESTestModuleV4.class,
					FvpPaymentsConsentsApiForceCheckBadSignatureTestModuleV4.class,
					FvpPaymentsConsentsApiNegativeTestModuleV4.class,
					FvpPaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV4.class,
					FvpAutomaticPaymentsApiSweepingAccountsInvalidCreditorTestModuleV1.class,
					FvpAutomaticPaymentsApiRejectedConsentTestModuleV1.class,
					FvpAutomaticPaymentsApiNegativeConsentsTestModuleV1.class,
					FvpConsentApiExtensionInvalidStatusTestModuleV3n.class,
					FvpConsentApiNegativeTestsV3n.class,
					FvpConsentApiBadConsentsTestModuleV3n.class,
					FvpConsentsApiPermissionGroupsTestModuleV3n.class
				),
				// Variants shall not be changed otherwise it will break the routine
				List.of(new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"))
			)
		);

	}
	public static String certificationProfileName(VariantSelection variant) {
		return "BR-OB Adv. OP DCR";
	}
}
