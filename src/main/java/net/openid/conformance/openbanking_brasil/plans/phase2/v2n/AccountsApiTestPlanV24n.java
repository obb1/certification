package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountApiXFapiTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountApiBookingDateTestV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountApiTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiMaxPageSizePagingTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiNegativeTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiOperationalLimitsTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiPageSizeTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiPageSizeTooLargeTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiReadPermissionsAreRestrictedV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiResourcesMultipleConsentsTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiTransactionsCurrentTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiUXScreenshotsV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsApiWrongPermissionsTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.account.testmodule.v2n.AccountsResourcesApiTestModuleV24n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "accounts_test-plan_v2-4",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.ACCOUNT_API_NAME_V2_4,
	summary = PlanNames.ACCOUNT_API_NAME_V2_4
)
public class AccountsApiTestPlanV24n implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new TestPlan.ModuleListEntry(
				List.of(
					PreFlightCheckV2Module.class,
					AccountsApiTransactionsCurrentTestModuleV24n.class,
					AccountsApiResourcesMultipleConsentsTestModuleV24n.class,
					AccountApiTestModuleV24n.class,
					AccountsApiWrongPermissionsTestModuleV24n.class,
					AccountsApiReadPermissionsAreRestrictedV24n.class,
					AccountsApiNegativeTestModuleV24n.class,
					AccountsApiUXScreenshotsV24n.class,
					AccountsApiPageSizeTestModuleV24n.class,
					AccountsApiPageSizeTooLargeTestModuleV24n.class,
					AccountsApiMaxPageSizePagingTestModuleV24n.class,
					AccountApiBookingDateTestV24n.class,
					AccountsResourcesApiTestModuleV24n.class,
					AccountsApiOperationalLimitsTestModuleV24n.class,
					AccountApiXFapiTestModuleV24n.class
				),
				List.of(
					new TestPlan.Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new TestPlan.Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
