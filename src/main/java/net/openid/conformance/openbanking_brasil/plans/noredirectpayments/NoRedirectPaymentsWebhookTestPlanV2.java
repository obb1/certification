package net.openid.conformance.openbanking_brasil.plans.noredirectpayments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook.v2.EnrollmentsApiWebhookRejectedTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.webhook.v2.EnrollmentsApiWebhookRevokedTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "no-redirect-payments-webhook_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_WEBHOOK_TEST_PLAN,
	summary = PlanNames.NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_WEBHOOK_TEST_PLAN
)
public class NoRedirectPaymentsWebhookTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					EnrollmentsApiWebhookRejectedTestModuleV2.class,
					EnrollmentsApiWebhookRevokedTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
