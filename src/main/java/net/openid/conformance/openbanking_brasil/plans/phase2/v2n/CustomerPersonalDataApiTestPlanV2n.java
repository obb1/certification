package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2.CustomerPersonalApiXFapiTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerPersonalApiOperationalLimitsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerPersonalConditionalEmployerDataTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerPersonalDataApiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerPersonalDataPortabilityApiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerPersonalWrongPermissionsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckCustomerV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "customer-personal_test-plan_v2-2",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.CUSTOMER_PERSONAL_DATA_API_PLAN_NAME_V2_2,
	summary = PlanNames.CUSTOMER_PERSONAL_DATA_API_PLAN_NAME_V2_2
)
public class CustomerPersonalDataApiTestPlanV2n implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckCustomerV2Module.class,
					CustomerPersonalDataApiTestModuleV2n.class,
					CustomerPersonalWrongPermissionsTestModuleV2n.class,
					CustomerPersonalApiOperationalLimitsTestModuleV2n.class,
					CustomerPersonalDataPortabilityApiTestModuleV2n.class,
					CustomerPersonalConditionalEmployerDataTestModuleV2n.class,
					CustomerPersonalApiXFapiTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
