package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionCurrentTimezoneTestModules.VariableIncomesApiTransactionsCurrentTimezoneTestModuleV1n21;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionsTimezoneTestModules.VariableIncomesApiTransactionsTimezoneTestModuleV1n21;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
    testPlanName = "variable-incomes-timezone_test-plan_V1-2-1",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    displayName = PlanNames.VARIABLE_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN_V1N21,
    summary = PlanNames.VARIABLE_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN_V1N21
)
public class VariableIncomesTimezoneTestPlanV1n21 implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
            new ModuleListEntry(
                List.of(
                    VariableIncomesApiTransactionsTimezoneTestModuleV1n21.class,
                    VariableIncomesApiTransactionsCurrentTimezoneTestModuleV1n21.class
                ),
                List.of(
                    new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
                    new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
                )
            )
        );
    }
}
