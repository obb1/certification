package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiMaxPageSizePagingTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiPageSizeTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiPageSizeTooLargeTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiResourcesTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiTransactionCurrentTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardApiWrongPermissionsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditCardApi.testmodule.v2n.CreditCardsApiOperationalLimitsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "credit-cards_test-plan_v2-1",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.CREDIT_CARDS_API_PLAN_NAME_V2_1,
	summary = PlanNames.CREDIT_CARDS_API_PLAN_NAME_V2_1
)

public class CreditCardApiTestPlanV2n implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV2Module.class,
					CreditCardApiTransactionCurrentTestModuleV2n.class,
					CreditCardApiTestModuleV2n.class,
					CreditCardApiWrongPermissionsTestModuleV2n.class,
					CreditCardApiPageSizeTestModuleV2n.class,
					CreditCardApiPageSizeTooLargeTestModuleV2n.class,
					CreditCardApiMaxPageSizePagingTestModuleV2n.class,
					CreditCardApiResourcesTestModuleV2n.class,
					CreditCardsApiOperationalLimitsTestModuleV2n.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
