package net.openid.conformance.openbanking_brasil.plans.phase3.payments.v4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentApiNoDebtorProvidedTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiAccountTypeTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiCancTemporizationTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiConsentsRejectionReasonTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiContentTypeJwtRefreshTokenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiE2EIDTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiINICPixResponseTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiIncorrectCPFProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiInvalidTokenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiMultipleConsentsConditionalTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiMultipleConsentsNoFundsConditionalTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiNegativeTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiPixSchedulingDatesUnhappyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiPixSchedulingEndToEndUnhappyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiPixSchedulingPatchDetentoraTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiPixSchedulingPatchIniciadoraTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiPixSchedulingPatchUnhappyV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiPixSchedulingSchdAcceptedTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiQRESGoodEmailProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiQRESGoodPhoneNumberProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiQRESMismatchedConsentPaymentTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiQRESMismatchedProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiQRESWithTransactionIdentifierTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiQRESWrongAmountProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRealEmailInvalidCreditorProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsConsentLimitTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsCustomCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsDailyCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsHigherQuantityTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsInvalidE2EidStartDateTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsLargeBatchTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsLowerQuantityTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsMonthlyCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsPatchTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsUnhappyDateAdjustmentTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsWeeklyCoreTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsWrongAmountTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiRecurringPaymentsWrongCustomQuantityTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiTemporizationConditionalTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiWrongEmailAddressProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiXFapiTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiDICTPixResponseTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiEnforceDICTTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiEnforceMANUTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiEnforceQRESTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiForceCheckBadSignatureTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiMANUPixResponseTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiNegativeTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiPhoneNumberProxyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsReuseJtiTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsumedConsentsTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsIdempotencyTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsNoFundsTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PreFlightCertCheckPaymentsModuleV4;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "payments_test-plan_v4",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.PAYMENTS_API_PHASE_3_V4_TEST_PLAN,
	summary = "Functional test for Open Finance Brasil payments API, including QR code and Pix Scheduling tests. " +
		"These tests are designed to validate the payment initiation of types MANU, DICT, INIC, QRES and QRDN ensuring " +
		"structural integrity and content validation. The tests are based on the Phase 3v4 Specifications."
)
public class PaymentsApiTestPlanV4 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCertCheckPaymentsModuleV4.class,
					PaymentApiNoDebtorProvidedTestModuleV4.class,
					PaymentsApiAccountTypeTestModuleV4.class,
					PaymentsApiConsentsRejectionReasonTestModuleV4.class,
					PaymentsApiContentTypeJwtRefreshTokenTestModuleV4.class,
					PaymentsApiE2EIDTestModuleV4.class,
					PaymentsApiIncorrectCPFProxyTestModuleV4.class,
					PaymentsApiINICPixResponseTestModuleV4.class,
					PaymentsApiInvalidTokenTestModuleV4.class,
					PaymentsApiNegativeTestModuleV4.class,
					PaymentsApiPixSchedulingDatesUnhappyTestModuleV4.class,
					PaymentsApiPixSchedulingEndToEndUnhappyTestModuleV4.class,
					PaymentsApiPixSchedulingPatchDetentoraTestModuleV4.class,
					PaymentsApiPixSchedulingPatchIniciadoraTestModuleV4.class,
					PaymentsApiPixSchedulingPatchUnhappyV4.class,
					PaymentsApiPixSchedulingSchdAcceptedTestModuleV4.class,
					PaymentsApiQRESGoodEmailProxyTestModuleV4.class,
					PaymentsApiQRESGoodPhoneNumberProxyTestModuleV4.class,
					PaymentsApiQRESMismatchedConsentPaymentTestModuleV4.class,
					PaymentsApiQRESMismatchedProxyTestModuleV4.class,
					PaymentsApiQRESWithTransactionIdentifierTestModuleV4.class,
					PaymentsApiQRESWrongAmountProxyTestModuleV4.class,
					PaymentsApiRealEmailInvalidCreditorProxyTestModuleV4.class,
					PaymentsApiWrongEmailAddressProxyTestModuleV4.class,
					PaymentsApiXFapiTestModuleV4.class,
					PaymentsConsentsApiDICTPixResponseTestModuleV4.class,
					PaymentsConsentsApiEnforceDICTTestModuleV4.class,
					PaymentsConsentsApiEnforceMANUTestModuleV4.class,
					PaymentsConsentsApiEnforceQRESTestModuleV4.class,
					PaymentsConsentsApiForceCheckBadSignatureTestModuleV4.class,
					PaymentsConsentsApiMANUPixResponseTestModuleV4.class,
					PaymentsConsentsApiNegativeTestModuleV4.class,
					PaymentsConsentsApiPhoneNumberProxyTestModuleV4.class,
					PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModuleV4.class,
					PaymentsConsentsReuseJtiTestModuleV4.class,
					PaymentsConsumedConsentsTestModuleV4.class,
					PaymentsIdempotencyTestModuleV4.class,
					PaymentsNoFundsTestModuleV4.class,
					PaymentsApiMultipleConsentsConditionalTestModuleV4.class,
					PaymentsApiMultipleConsentsNoFundsConditionalTestModuleV4.class,
					PaymentsApiTemporizationConditionalTestModuleV4.class,
					PaymentsApiCancTemporizationTestModuleV4.class,
					PaymentsApiRecurringPaymentsConsentLimitTestModule.class,
					PaymentsApiRecurringPaymentsCustomCoreTestModuleV4.class,
					PaymentsApiRecurringPaymentsDailyCoreTestModuleV4.class,
					PaymentsApiRecurringPaymentsHigherQuantityTestModuleV4.class,
					PaymentsApiRecurringPaymentsInvalidE2EidStartDateTestModuleV4.class,
					PaymentsApiRecurringPaymentsLargeBatchTestModuleV4.class,
					PaymentsApiRecurringPaymentsMonthlyCoreTestModuleV4.class,
					PaymentsApiRecurringPaymentsUnhappyDateAdjustmentTestModuleV4.class,
					PaymentsApiRecurringPaymentsWeeklyCoreTestModuleV4.class,
					PaymentsApiRecurringPaymentsWrongAmountTestModuleV4.class,
					PaymentsApiRecurringPaymentsLowerQuantityTestModuleV4.class,
					PaymentsApiRecurringPaymentsWrongCustomQuantityTestModuleV4.class,
					PaymentsApiRecurringPaymentsPatchTestModuleV4.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
