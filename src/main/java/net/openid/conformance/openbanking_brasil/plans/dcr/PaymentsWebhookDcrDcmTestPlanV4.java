package net.openid.conformance.openbanking_brasil.plans.dcr;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v4.PaymentsWebhookAcscDcrTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v4.PaymentsWebhookRemoveWebhookWithDcmV4;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v4.PaymentsWebhookSchdDcrTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v4.PaymentsWebhookWithDcmTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v4.PaymentsWebhookWithDcmWrongWebhookTestModuleV4;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "dcr-dcm-pagto_test-plan-v4",
	profile = OBBProfile.OBB_PROFILE_DCR,
	displayName = PlanNames.PAYMENTS_API_DCR_DCM_v4,
	summary = "For servers that support MTLS client authentication, check that subjectdn can be updated using the dynamic client management endpoint."
)
public class PaymentsWebhookDcrDcmTestPlanV4 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PaymentsWebhookAcscDcrTestModuleV4.class,
					PaymentsWebhookSchdDcrTestModuleV4.class,
					PaymentsWebhookWithDcmTestModuleV4.class,
					PaymentsWebhookRemoveWebhookWithDcmV4.class,
					PaymentsWebhookWithDcmWrongWebhookTestModuleV4.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
