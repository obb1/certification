package net.openid.conformance.openbanking_brasil.plans.yacs.automaticpayments;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.YACSAutomaticPaymentsApiSweepingAccountsConsentsCoreTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v1.YACSAutomaticPaymentsApiSweepingAccountsCoreTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;
@PublishTestPlan(
	testPlanName = "fvp-automatic-payments_test-plan-v1",
	profile = OBBProfile.OBB_PROFILE_PROD_RESTRICTED_FVP,
	displayName = PlanNames.OBB_PROD_FVP_AUTOMATIC_PAYMENTS_V1,
	summary = "Open Finance Brasil Functional Production Tests - FVP - Automatic Payments V1"
)
public class YACSAutomaticPaymentsProductionV1TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSAutomaticPaymentsApiSweepingAccountsConsentsCoreTestModuleV1.class,
					YACSAutomaticPaymentsApiSweepingAccountsCoreTestModuleV1.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}

