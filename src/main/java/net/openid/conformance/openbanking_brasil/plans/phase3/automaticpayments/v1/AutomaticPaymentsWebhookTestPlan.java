package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v1.AutomaticPaymentsApiWebhookAcscTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v1.AutomaticPaymentsApiWebhookMultipleConsentsTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v1.AutomaticPaymentsApiWebhookRejectedTestModuleV1;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.webhook.v1.AutomaticPaymentsApiWebhookRevokedTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-payments-webhook_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PAYMENTS_API_PHASE_3_WEBHOOK_V1_TEST_PLAN,
	summary = "For servers that support MTLS client authentication, check that subjectdn can be updated using the dynamic client management endpoint."
)
public class AutomaticPaymentsWebhookTestPlan implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiWebhookAcscTestModuleV1.class,
					AutomaticPaymentsApiWebhookRejectedTestModuleV1.class,
					AutomaticPaymentsApiWebhookRevokedTestModuleV1.class,
					AutomaticPaymentsApiWebhookMultipleConsentsTestModuleV1.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
