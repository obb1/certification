package net.openid.conformance.openbanking_brasil.plans.phase3.payments.v4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsApiQRDNWithQRESCodeTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiEnforceQRDNTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.PaymentsConsentsApiQRDNHappyTestModuleV4;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "payments-qrdn_test-plan_v4",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.PAYMENTS_API_QRDN_PHASE_3_V4_TEST_PLAN,
	summary = "Functional QRDN test for Open Finance Brasil payments API."  +
		"This tests is designed to validate the payment initiation with QRDN ensuring " +
		"structural integrity and content validation. This test is based on the Phase 3v4 Specifications. "
)
public class PaymentsApiQRDNTestPlanV4 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PaymentsConsentsApiQRDNHappyTestModuleV4.class,
					PaymentsApiQRDNWithQRESCodeTestModuleV4.class,
					PaymentsConsentsApiEnforceQRDNTestModuleV4.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
