package net.openid.conformance.openbanking_brasil.plans.noredirectpayments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiCoreEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiExcessiveDailyLimitSchdTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiExcessiveDailyLimitTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiExcessiveTransactionLimitTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiExpiredPostAuthorisationEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiExpiredPreAuthorisationEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiExpiredPreRiskSignalEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiInvalidChallengeTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiInvalidOriginTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiInvalidParametersTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiInvalidPublicKeyTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiInvalidRpIdTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiInvalidStatusOptionsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiInvalidStatusSignOptionsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiMultipleConsentsCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiPaymentsCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiPaymentsKeysSwapTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiPaymentsPreAccountHolderValidationTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiPaymentsPreEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiPaymentsUnmatchingFieldsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiPreflightTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiRejectedPostAuthorisationEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiRejectedPreAuthorisationEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiRevokedEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.v2.EnrollmentsApiRiskSignalsTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "no-redirect-payments_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_TEST_PLAN,
	summary = PlanNames.NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_TEST_PLAN
)
public class NoRedirectPaymentsTestPlanV2 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					EnrollmentsApiPreflightTestModuleV2.class,
					EnrollmentsApiExpiredPostAuthorisationEnrollmentTestModuleV2.class,
					EnrollmentsApiExpiredPreAuthorisationEnrollmentTestModuleV2.class,
					EnrollmentsApiExpiredPreRiskSignalEnrollmentTestModuleV2.class,
					EnrollmentsApiInvalidParametersTestModuleV2.class,
					EnrollmentsApiInvalidPublicKeyTestModuleV2.class,
					EnrollmentsApiInvalidStatusOptionsTestModuleV2.class,
					EnrollmentsApiInvalidStatusSignOptionsTestModuleV2.class,
					EnrollmentsApiPaymentsPreAccountHolderValidationTestModuleV2.class,
					EnrollmentsApiPaymentsPreEnrollmentTestModuleV2.class,
					EnrollmentsApiRejectedPostAuthorisationEnrollmentTestModuleV2.class,
					EnrollmentsApiRejectedPreAuthorisationEnrollmentTestModuleV2.class,
					EnrollmentsApiPaymentsCoreTestModuleV2.class,
					EnrollmentsApiInvalidOriginTestModuleV2.class,
					EnrollmentsApiInvalidChallengeTestModuleV2.class,
					EnrollmentsApiCoreEnrollmentTestModuleV2.class,
					EnrollmentsApiRevokedEnrollmentTestModuleV2.class,
					EnrollmentsApiInvalidRpIdTestModuleV2.class,
					EnrollmentsApiPaymentsKeysSwapTestModuleV2.class,
					EnrollmentsApiPaymentsUnmatchingFieldsTestModuleV2.class,
					EnrollmentsApiExcessiveTransactionLimitTestModuleV2.class,
					EnrollmentsApiExcessiveDailyLimitTestModuleV2.class,
					EnrollmentsApiExcessiveDailyLimitSchdTestModuleV2.class,
					EnrollmentsApiMultipleConsentsCoreTestModuleV2.class,
					EnrollmentsApiRiskSignalsTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
