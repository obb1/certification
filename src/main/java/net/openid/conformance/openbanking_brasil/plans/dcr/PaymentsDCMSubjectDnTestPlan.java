package net.openid.conformance.openbanking_brasil.plans.dcr;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v2.PaymentsWebhookRemoveWebhookWithDcmV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v2.PaymentsWebhookSchdDcrTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v2.PaymentsWebhookWithDcmTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v2.PaymentsWebhookWithDcmWrongWebhookTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.v2.payments.webhook.v2.PaymentsWebookAcscDcrTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "dcr-dcm-pagto_test-plan",
	profile = OBBProfile.OBB_PROFILE_DCR,
	displayName = PlanNames.PAYMENTS_API_DCR_DCM,
	summary = "For servers that support MTLS client authentication, check that subjectdn can be updated using the dynamic client management endpoint."
)
public class PaymentsDCMSubjectDnTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PaymentsWebookAcscDcrTestModuleV2.class,
					PaymentsWebhookSchdDcrTestModuleV2.class,
					PaymentsWebhookWithDcmTestModuleV2.class,
					PaymentsWebhookRemoveWebhookWithDcmV2.class,
					PaymentsWebhookWithDcmWrongWebhookTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
