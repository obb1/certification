package net.openid.conformance.openbanking_brasil.plans.yacs.payments;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiPaymentsCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiInvalidChallengeTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiInvalidOriginTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiInvalidPublicKeyTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiInvalidRpIdTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiInvalidStatusSignOptionsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiPaymentsKeysSwapTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiPaymentsPreEnrollmentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiPaymentsUnmatchingFieldsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.enrollments.fvp.v2.FvpEnrollmentsApiPreFlightTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;

@PublishTestPlan(
	testPlanName = "fvp-no_redirect_payments-test-plan-v2-1",
	profile = OBBProfile.OBB_PROFILE_PROD_RESTRICTED_FVP,
	displayName = PlanNames.OBB_PROD_FVP_NO_REDIRECT_PAYMENTS_V2,
	summary = "Open Finance Brasil Functional Production Tests - FVP - No Redirect Payments Version 2.1"
)
public class YACSNRJPaymentsV2TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					FvpEnrollmentsApiPreFlightTestModuleV2.class,
					FvpEnrollmentsApiPaymentsCoreTestModuleV2.class,
					FvpEnrollmentsApiInvalidChallengeTestModuleV2.class,
					FvpEnrollmentsApiInvalidOriginTestModuleV2.class,
					FvpEnrollmentsApiInvalidPublicKeyTestModuleV2.class,
					FvpEnrollmentsApiInvalidRpIdTestModuleV2.class,
					FvpEnrollmentsApiPaymentsPreEnrollmentTestModuleV2.class,
					FvpEnrollmentsApiInvalidStatusSignOptionsTestModuleV2.class,
					FvpEnrollmentsApiPaymentsKeysSwapTestModuleV2.class,
					FvpEnrollmentsApiPaymentsUnmatchingFieldsTestModuleV2.class
					),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}

