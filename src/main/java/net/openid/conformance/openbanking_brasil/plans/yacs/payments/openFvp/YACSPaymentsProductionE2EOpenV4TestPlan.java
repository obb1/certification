package net.openid.conformance.openbanking_brasil.plans.yacs.payments.openFvp;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.YACSPreFlightCertCheckPaymentsV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.FvpPaymentsApiRecurringPaymentsCustomCoreOpenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.FvpPaymentsApiRecurringPaymentsCustomNotCancelledOpenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.FvpPaymentsApiRecurringPaymentsMonthlyCoreOpenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.FvpPaymentsApiRecurringPaymentsPatchOpenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.FvpPaymentsApiRecurringPaymentsWeeklyCoreOpenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.PaymentApiNoDebtorProvidedOpenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.PaymentsApiRealEmailInvalidCreditorProxyOpenTestModuleV4;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.v4.openFvp.PaymentsApiWrongEmailAddressProxyOpenTestModuleV4;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;
@PublishTestPlan(
	testPlanName = "fvp-payments-e2e_open_test-plan-v4",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_PAYMENTS_V4_E2E,
	summary = "Open Finance Brasil Functional Production Tests - FVP - Payments V4 E2E"
)
public class YACSPaymentsProductionE2EOpenV4TestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSPreFlightCertCheckPaymentsV4.class,
					PaymentApiNoDebtorProvidedOpenTestModuleV4.class,
					PaymentsApiRealEmailInvalidCreditorProxyOpenTestModuleV4.class,
					PaymentsApiWrongEmailAddressProxyOpenTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsCustomCoreOpenTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsPatchOpenTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsWeeklyCoreOpenTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsMonthlyCoreOpenTestModuleV4.class,
					FvpPaymentsApiRecurringPaymentsCustomNotCancelledOpenTestModuleV4.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}

