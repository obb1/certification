package net.openid.conformance.openbanking_brasil.plans.phase3.automaticpayments.v2;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixAnualCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixConsentEditionAccountHolderTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixConsentEditionNegativeTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixConsentEditionPermissiveTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixConsentEditionRestrictiveTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixFailedFirstPaymentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixFirstPaymentInvalidCreditorTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixFixedAmountTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixInvalidCreditorTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixInvalidDatesLaterTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixInvalidDatesSoonerTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixInvalidParametersTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixMaximumVariableAmountTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixMensalCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixMinimumVariableAmountTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixNegativeConsentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixNoLimitsTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixRevokedTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixScheduledFirstPaymentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixSchedulingBeforeFirstPaymentTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixSemanalCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixSemestralCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixReferenceStartDateTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixTrimestralCoreTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixUnmatchingCreditorTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixUnmatchingLoggedUserTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.phase3.automaticPayments.v2.AutomaticPaymentsApiAutomaticPixWrongCreditorTestModuleV2;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "automatic-pix-payments_test-plan_v2",
	profile = OBBProfile.OBB_PROFIlE_PHASE3,
	displayName = PlanNames.AUTOMATIC_PIX_PAYMENTS_API_PHASE_3_V2_TEST_PLAN,
	summary = "Functional test for Open Finance Brasil Automatic Pix Payments API. " +
		"These tests are designed to validate the payment initiation using Automatic Pix Payments API and ensuring " +
		"structural integrity and content validation. The tests are based on the Automatic Payments v2 Specifications."
)
public class AutomaticPixPaymentsTestPlanV2 implements TestPlan {

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					AutomaticPaymentsApiAutomaticPixInvalidCreditorTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixInvalidParametersTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixNegativeConsentTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixSemanalCoreTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixMensalCoreTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixTrimestralCoreTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixSemestralCoreTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixAnualCoreTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixNoLimitsTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixRevokedTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixFailedFirstPaymentTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixWrongCreditorTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixFirstPaymentInvalidCreditorTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixScheduledFirstPaymentTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixInvalidDatesSoonerTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixInvalidDatesLaterTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixFixedAmountTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixMinimumVariableAmountTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixMaximumVariableAmountTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixReferenceStartDateTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixUnmatchingCreditorTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixSchedulingBeforeFirstPaymentTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixConsentEditionPermissiveTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixConsentEditionRestrictiveTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixConsentEditionAccountHolderTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixConsentEditionNegativeTestModuleV2.class,
					AutomaticPaymentsApiAutomaticPixUnmatchingLoggedUserTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
