package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n.LoansApiOperationalLimitsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n.LoansApiResourcesTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n.LoansApiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n.LoansApiWrongPermissionsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.loans.testmodules.v2n.LoansApiXFapiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "loans_test-plan_v2-3",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.LOANS_API_PLAN_NAME_V2_3,
	summary = PlanNames.LOANS_API_PLAN_NAME_V2_3
)
public class LoansApiTestPlanV2n implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV2Module.class,
					LoansApiTestModuleV2n.class,
					LoansApiWrongPermissionsTestModuleV2n.class,
					LoansApiResourcesTestModuleV2n.class,
					LoansApiOperationalLimitsTestModuleV2n.class,
					LoansApiXFapiTestModuleV2n.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
