package net.openid.conformance.openbanking_brasil.plans;

public class PlanNames {
	/** VERSION 1 **/
	/* Phase 1 - Open Data */
	public static final String ADMIN_API_TEST_PLAN  = "Functional Tests for Admin API - Based on Swagger version: 2.0.1";
	public static final String COMMON_API_OUTAGES_TEST_PLAN  = "Functional Tests for Common - Outages API - Based on Swagger version: 2.0.1";
	public static final String COMMON_API_STATUS_TEST_PLAN  = "Functional Tests for Common - Status API - Based on Swagger version: 2.0.1";
	public static final String BANKING_AGENTS_API_TEST_PLAN_V2 = "Functional Tests for Channels - BankingAgents API - Based on Swagger version: 2.0.1";
	public static final String BRANCHES_API_TEST_PLAN_V2 = "Functional Tests for Channels - Branches API - Based on Swagger version: 2.0.1";
	public static final String ELECTRONIC_CHANNELS_API_TEST_PLAN_V2 = "Functional Tests for Channels - Electronic Channels API - Based on Swagger version: 2.0.1";
	public static final String PHONE_CHANNELS_API_TEST_PLAN_V2 = "Functional Tests for Channels - Phone Channels API - Based on Swagger version: 2.0.1";
	public static final String SHARED_AUTOMATED_TELLER_MACHINES_API_TEST_PLAN_V2 = "Functional Tests for Channels - Shared Automated Teller Machines API - Based on Swagger version: 2.0.1";


	public static final String ACQUIRING_SERVICES_BUSINESS_API_TEST_PLAN = "Functional Tests for Acquiring Services - Business API - Based on Swagger version: 1.0.1";
	public static final String ACQUIRING_SERVICES_PERSONAL_API_TEST_PLAN = "Functional Tests for Acquiring Services - Personal API - Based on Swagger version: 1.0.1";

	public static final String BUSINESS_ACCOUNTS_API_TEST_PLAN_V1 = "Functional Tests BusinessAccounts API - Based on Swagger version: 1.0.1";
	public static final String BUSINESS_CREDIT_CARD_API_TEST_PLAN_V1 = "Functional Tests BusinessCredit Card API - Based on Swagger version: 1.0.1";
	public static final String BUSINESS_FINANCINGS_API_TEST_PLAN_V1 = "Functional Tests Business Financings API - Based on Swagger version: 1.0.1";
	public static final String BUSINESS_INVOICE_FINANCINGS_API_TEST_PLAN_V1 = "Functional Tests Business Invoice Financings API - Based on Swagger version: 1.0.1";
	public static final String BUSINESS_LOANS_API_TEST_PLAN_V1 = "Functional Tests BusinessLoans API - Based on Swagger version: 1.0.1";
	public static final String PERSONAL_ACCOUNTS_API_TEST_PLAN_V1 = "Functional Tests PersonalAccounts API - Based on Swagger version: 1.0.1";
	public static final String PERSONAL_CREDIT_CARD_API_TEST_PLAN_V1 = "Functional Tests PersonalCredit Card API - Based on Swagger version: 1.0.1";
	public static final String PERSONAL_FINANCINGS_API_TEST_PLAN_V1 = "Functional Tests Personal Financings API - Based on Swagger version: 1.0.1";
	public static final String PERSONAL_INVOICE_FINANCINGS_API_TEST_PLAN_V1 = "Functional Tests Personal Invoice Financings API - Based on Swagger version: 1.0.1";
	public static final String PERSONAL_LOANS_API_TEST_PLAN_V1 = "Functional Tests PersonalLoans API - Based on Swagger version: 1.0.1";
	public static final String UNARRANGED_ACCOUNT_BUSINESS_OVERDRAFT_API_TEST_PLAN_v1 = "Functional Tests Unarranged Account Business Overdraft API - Based on Swagger version: 1.0.1";
	public static final String UNARRANGED_ACCOUNT_PERSONAL_OVERDRAFT_API_TEST_PLAN_V1 = "Functional Tests Unarranged Account Personal Overdraft API - Based on Swagger version: 1.0.1";

	/* Phase 2 - Customer Data */


	public static final String CUSTOMER_DATA_DCR = "Functional Tests for DCR - DADOS Role";

	/* Phase 3 - Payment Initiation */

	public static final String OBB_DCR = "Functional Tests for DCR - Delete Not Executed";
	public static final String OBB_DCR_WITHOUT_BROWSER_INTERACTION_TEST_PLAN = "Functional Tests for DCR - FVP 1.0 - Live Tests";
	public static final String OBB_DCR_WITHOUT_BROWSER_INTERACTION_HOMOLG_TEST_PLAN = "Functional Tests for DCR - FVP 1.0 - Homologation Tests";
	public static final String NEW_STYLE_BRCAC_DCR = "Functional Tests for DCR - BRCAC new Format";
	public static final String PAYMENTS_API_DCR_DCM_v4 = "Functional Tests for DCR DCM - PAGTO Role - Webhook V4 - Based on Swagger version: 1.0.0";


	/** VERSION 2 **/
	public static final String CUSTOMER_BUSINESS_DATA_API_PLAN_NAME_V2_2 = "Functional Tests for Customer - Business API - Based on Swagger version:  2.2.0";
	/** VERSION 2.1 **/

	public static final String LOANS_API_PLAN_NAME_V2_3 = "Functional Tests for Loans API - Based on Swagger version: 2.3.0";
	public static final String LOANS_API_PLAN_NAME_V2_4 = "Functional Tests for Loans API - Based on Swagger version: 2.4.0";
	public static final String ACCOUNT_API_NAME_V2_4 = "Functional Tests for Accounts API - Based on Swagger version: 2.4.1";

	public static final String CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME_V2_2 = "Functional Tests for Invoice Financings API - Based on Swagger version: 2.2.0";
	public static final String CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME_V2_3 = "Functional Tests for Invoice Financings API - Based on Swagger version: 2.3.0";

	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2_2 = "Functional Tests for Unarranged Accounts Overdraft API - Based on Swagger version: 2.2.0";
	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2_3 = "Functional Tests for Unarranged Accounts Overdraft API - Based on Swagger version: 2.3.0";
	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2_4 = "Functional Tests for Unarranged Accounts Overdraft API - Based on Swagger version: 2.4.0";

	public static final String CREDIT_CARDS_API_PLAN_NAME_V2_1 = "Functional Tests for Credit Cards API - Based on Swagger version: 2.3.1";

	public static final String FINANCINGS_API_NAME_V2_2 = "Functional Tests for Financings API - Based on Swagger version: 2.2.0";
	public static final String FINANCINGS_API_NAME_V2_3 = "Functional Tests for Financings API - Based on Swagger version: 2.3.0";
	public static final String CUSTOMER_PERSONAL_DATA_API_PLAN_NAME_V2_2 = "Functional Tests for Customer - Personal API - Based on Swagger version: 2.2.0";

	/** VERSION 3 **/
	public static final String CONSENTS_API_NAME_V3 = "Functional Tests for Consents API - Based on Swagger version: 3.0.1";
	public static final String CONSENTS_API_NAME_V3_2 = "Functional Tests for Consents API - Based on Swagger version: 3.2.0";

	public static final String RESOURCES_API_PLAN_NAME_V3 = "Functional Tests for Resources API - Based on Swagger version: 3.0.0";

	public static final String PAYMENTS_API_DCR_DCM = "Functional Tests for DCR DCM - PAGTO Role - Webhook - Based on Swagger version: 1.0.0";




	/** VERSION 4 **/
	public static final String PAYMENTS_API_PHASE_3_V4_TEST_PLAN = "Functional Tests for Payments API - Based on Swagger version: v4.0.0-rc.2";
	public static final String PAYMENTS_API_TIMEZONE_PHASE_3_V4_TEST_PLAN = "Functional Timezone Tests for Payments API - Based on Swagger version: v4.0.0-rc.2";
	public static final String PAYMENTS_API_QRDN_PHASE_3_V4_TEST_PLAN ="Functional QRDN Tests for Payments API - Based on Swagger version: v4.0.0-rc.2";
	/** NRJ **/
	public static final String NO_REDIRECT_PAYMENTS_API_PHASE_3_V1_TEST_PLAN = "Functional Tests for No Redirect Payments API - Based on Swagger version: v1.3.1";
	public static final String NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_TEST_PLAN = "Functional Tests for No Redirect Payments API - Based on Swagger version: v2.0.0";
	public static final String NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_WEBHOOK_TEST_PLAN = "Functional Tests for No Redirect Payments API - Webhook - Based on Swagger version: v2.0.0";
	public static final String NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_1_TEST_PLAN = "Functional Tests for No Redirect Payments API - Based on Swagger version: v2.1.0";
	public static final String NO_REDIRECT_PAYMENTS_API_PHASE_3_V2_1_WEBHOOK_TEST_PLAN = "Functional Tests for No Redirect Payments API - Webhook - Based on Swagger version: v2.1.0";


	/** AUTOMATIC PAYMENTS **/
	public static final String AUTOMATIC_PAYMENTS_API_PHASE_3_V1_TEST_PLAN = "Functional Tests for Automatic Payments API - Based on Swagger version: v1.0.0";
	public static final String AUTOMATIC_PAYMENTS_API_PHASE_3_WEBHOOK_V1_TEST_PLAN = "Functional Tests for Automatic Payments API - Webhook - Based on Swagger version: v1.0.0";
	public static final String AUTOMATIC_PAYMENTS_API_PHASE_3_V1_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Automatic Payments API - Based on Swagger version: v1.0.0";

	public static final String AUTOMATIC_PAYMENTS_API_PHASE_3_V2_TEST_PLAN = "Functional Tests for Automatic Sweeping Payments API - Based on Swagger version: v2.0.0-rc.1";
	public static final String AUTOMATIC_PAYMENTS_API_PHASE_3_WEBHOOK_V2_TEST_PLAN = "Functional Tests for Automatic Sweeping Payments API - Webhook - Based on Swagger version: v2.0.0-rc.1";
	public static final String AUTOMATIC_PAYMENTS_API_PHASE_3_V2_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Automatic Sweeping Payments API - Based on Swagger version: v2.0.0-rc.1";

	public static final String AUTOMATIC_PIX_PAYMENTS_API_PHASE_3_V2_TEST_PLAN = "Functional Tests for Automatic Pix Payments API - Based on Swagger version: v2.0.0-rc.1";
	public static final String AUTOMATIC_PIX_PAYMENTS_API_PHASE_3_WEBHOOK_V2_TEST_PLAN = "Functional Tests for Automatic Pix Payments API - Webhook - Based on Swagger version: v2.0.0-rc.1";
	public static final String AUTOMATIC_PIX_PAYMENTS_API_PHASE_3_V2_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Automatic Pix Payments API - Based on Swagger version: v2.0.0-rc.1";
	public static final String AUTOMATIC_PIX_PAYMENTS_API_RETRY_PHASE_3_V2_TEST_PLAN = "Functional Tests for Automatic Pix Payments API Retry - Based on Swagger version: v2.0.0-rc.1";
	public static final String AUTOMATIC_PIX_PAYMENTS_API_RETRY_AUXILIARY_PHASE_3_V2_TEST_PLAN = "Functional Tests for Automatic Pix Payments API Retry Auxiliary - Based on Swagger version: v2.0.0-rc.1";

	/** FVP Tests **/
	public static final String OBB_PROD_FVP_PHASE_2 = "Production Functional Tests for Consents and Resources API - API Version 3";
	public static final String OBB_PROD_FVP_PHASE_2_HAPPY_PATH_V3 = "Production Functional Tests for Customer Data Happy Path - API Version 3";
	public static final String OBB_PROD_FVP_PAYMENTS = "Production Functional Tests for Payments API - API Version 4";
	public static final String OBB_PROD_HOMOLOG_PLAN = "Functional Tests with General Scope - DCR and Consents";
	public static final String OBB_PROD_FVP_DCR_TEST_PLAN = "Functional Tests for DCR - FVP 1.0 - Live Tests";
	public static final String OBB_PROD_FVP_PAYMENTS_V4_E2E = "Production Functional Tests for Payments E2E - API Version 4";
	public static final String OBB_PROD_FVP_AUTOMATIC_PAYMENTS_V1 = "Production Functional Tests for Automatic Payments - API Version 1";
	public static final String OBB_PROD_FVP_NO_REDIRECT_PAYMENTS_V1 = "Production Functional Tests for No Redirect Payments - API Version 1";
	public static final String OBB_PROD_FVP_NO_REDIRECT_PAYMENTS_V2 = "Production Functional Tests for No Redirect Payments - API Version 2.1";


	/** Phase 4 **/
	public static final String DIRECTORY_REGISTRATION_TEST_PLAN = "Functional Tests for Directory Registration";
	/** Phase 4B **/
	public static final String VARIABLE_INCOMES_API_PHASE_4B_TEST_PLAN = "Functional Tests for Variable Incomes API - Based on Swagger version: 1.2.0";
	public static final String VARIABLE_INCOMES_API_PHASE_4B_TEST_PLAN_V1N21 = "Functional Tests for Variable Incomes API - Based on Swagger version: 1.2.1";
	public static final String BANK_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN = "Functional Tests for Bank Fixed Incomes API - Based on Swagger version: 1.0.0";
	public static final String BANK_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN_V1 = "Functional Tests for Bank Fixed Incomes API - Based on Swagger version: 1.0.4";
	public static final String CREDIT_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN = "Functional Tests for Credit Fixed Incomes API - Based on Swagger version: 1.0.0";
	public static final String CREDIT_FIXED_INCOMES_API_PHASE_4B_TEST_PLAN_V1 = "Functional Tests for Credit Fixed Incomes API - Based on Swagger version: 1.0.3";
	public static final String FUNDS_API_PHASE_4B_TEST_PLAN = "Functional Tests for Funds API - Based on Swagger version: 1.0.0";
	public static final String TREASURE_TITLES_API_PHASE_4B_TEST_PLAN = "Functional Tests for Treasure Titles API - Based on Swagger version: 1.0.0";
	public static final String EXCHANGE_API_PHASE_4B_TEST_PLAN = "Functional Tests for Exchange API - Based on Swagger version: 1.0.0";

	public static final String VARIABLE_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Variable Incomes API - Based on Swagger version: 1.2.0";
	public static final String VARIABLE_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN_V1N21 = "Functional Timezone Tests for Variable Incomes API - Based on Swagger version: 1.2.1";
	public static final String BANK_FIXED_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Bank Fixed Incomes API - Based on Swagger version: 1.0.0";
	public static final String BANK_FIXED_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN_V1 = "Functional Timezone Tests for Bank Fixed Incomes API - Based on Swagger version: 1.0.4";
	public static final String CREDIT_FIXED_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Credit Fixed Incomes API - Based on Swagger version: 1.0.0";
	public static final String CREDIT_FIXED_INCOMES_API_PHASE_4B_TIMEZONE_TEST_PLAN_V1 = "Functional Timezone Tests for Credit Fixed Incomes API - Based on Swagger version: 1.0.3";
	public static final String FUNDS_API_PHASE_4B_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Funds API - Based on Swagger version: 1.0.0";
	public static final String TREASURE_TITLES_API_PHASE_4B_TIMEZONE_TEST_PLAN = "Functional Timezone Tests for Treasure Titles API - Based on Swagger version: 1.0.0";


	/** Phase 4a **/
	public static final String INSURANCE_AUTOMATIVE_API_PHASE_4A_TEST_PLAN = "Functional Tests for Insurance - Automotive Insurance API - Based on Swagger version: 1.0.0";
	public static final String INSURANCE_HOME_API_PHASE_4A_TEST_PLAN = "Functional Tests for Insurance - HomeInsurance API - Based on Swagger version: 1.0.0";
	public static final String INSURANCE_PERSONAL_API_PHASE_4A_TEST_PLAN_V2 = "Functional Tests for Insurance - PersonalInsurance API - Based on Swagger version: 2.0.0";

	public static final String CAPITALIZATION_BONDS_API_PHASE_4A_TEST_PLAN_V2 = "Functional Tests for Capitalization Bonds API - Based on Swagger version: 2.0.0";
	public static final String PENSION_RISK_COVERAGES_API_PHASE_4A_TEST_PLAN_V2 = "Functional Tests for Pension - Risk Coverages - Based on Swagger version: 2.0.0";
	public static final String PENSION_SURVIVAL_COVERAGES_API_PHASE_4A_TEST_PLAN_V2 = "Functional Tests for Pension - Survival Coverages - Based on Swagger version: 2.0.0";



}
