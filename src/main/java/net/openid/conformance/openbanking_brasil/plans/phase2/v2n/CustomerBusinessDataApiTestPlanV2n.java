package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2.BusinessEntityWithPersonalPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2.CustomerBusinessApiXFapiTestModuleV2;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerBusinessApiOperationalLimitsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerBusinessDataApiTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.testmodule.v2n.CustomerBusinessWrongPermissionsTestModuleV2n;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckCustomerV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "customer-business_test-plan_v2-2",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.CUSTOMER_BUSINESS_DATA_API_PLAN_NAME_V2_2,
	summary = PlanNames.CUSTOMER_BUSINESS_DATA_API_PLAN_NAME_V2_2
)
public class CustomerBusinessDataApiTestPlanV2n implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckCustomerV2Module.class,
					BusinessEntityWithPersonalPermissionsTestModule.class,
					CustomerBusinessDataApiTestModuleV2n.class,
					CustomerBusinessWrongPermissionsTestModuleV2n.class,
					CustomerBusinessApiOperationalLimitsTestModuleV2n.class,
					CustomerBusinessApiXFapiTestModuleV2.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
