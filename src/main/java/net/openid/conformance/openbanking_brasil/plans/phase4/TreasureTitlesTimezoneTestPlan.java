package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionCurrentTimezoneTestModules.TreasureTitlesApiTransactionsCurrentTimezoneTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionsTimezoneTestModules.TreasureTitlesApiTransactionsTimezoneTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
    testPlanName = "treasure-titles-timezone_test-plan",
    profile = OBBProfile.OBB_PROFIlE_PHASE4B,
    displayName = PlanNames.TREASURE_TITLES_API_PHASE_4B_TIMEZONE_TEST_PLAN,
    summary = PlanNames.TREASURE_TITLES_API_PHASE_4B_TIMEZONE_TEST_PLAN
)
public class TreasureTitlesTimezoneTestPlan implements TestPlan {

    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
            new ModuleListEntry(
                List.of(
                    TreasureTitlesApiTransactionsTimezoneTestModule.class,
                    TreasureTitlesApiTransactionsCurrentTimezoneTestModule.class
                ),
                List.of(
                    new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
                    new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
                )
            )
        );
    }
}
