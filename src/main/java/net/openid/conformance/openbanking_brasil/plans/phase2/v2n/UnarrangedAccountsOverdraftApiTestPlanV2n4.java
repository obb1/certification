package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n4.UnarrangedAccountsOverdraftApiOperationalLimitsTestModuleV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n4.UnarrangedAccountsOverdraftApiResourcesTestModuleV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n4.UnarrangedAccountsOverdraftApiTestModuleV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n4.UnarrangedAccountsOverdraftApiWrongPermissionsTestModuleV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.advances.testmodules.v2n4.UnarrangedAccountsOverdraftApiXFapiTestModuleV2n4;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "unarranged-accounts-overdraft_test-plan_v2-4",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2_4,
	summary = PlanNames.CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2_4
)
public class UnarrangedAccountsOverdraftApiTestPlanV2n4 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV2Module.class,
					UnarrangedAccountsOverdraftApiTestModuleV2n4.class,
					UnarrangedAccountsOverdraftApiOperationalLimitsTestModuleV2n4.class,
					UnarrangedAccountsOverdraftApiResourcesTestModuleV2n4.class,
					UnarrangedAccountsOverdraftApiWrongPermissionsTestModuleV2n4.class,
					UnarrangedAccountsOverdraftApiXFapiTestModuleV2n4.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
