package net.openid.conformance.openbanking_brasil.plans.productsNServicesApiTestPlans.v1.unarrangedAccountOverdraft;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.productsNServices.v1.unarrangedAccountOverdraft.UnarrangedAccountBusinessOverdraftApiTestModuleV1;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "unarranged_account_business_overdraft_test-plan_v1",
	profile = OBBProfile.OBB_PROFIlE_PHASE1_AND_PHASE4,
	displayName = PlanNames.UNARRANGED_ACCOUNT_BUSINESS_OVERDRAFT_API_TEST_PLAN_v1,
	summary = PlanNames.UNARRANGED_ACCOUNT_BUSINESS_OVERDRAFT_API_TEST_PLAN_v1
)
public class UnarrangedAccountBusinessOverdraftApiTestPlanV1 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					UnarrangedAccountBusinessOverdraftApiTestModuleV1.class
				),
				List.of(
					new Variant(ClientAuthType.class, "none")
				)
			)
		);
	}
}
