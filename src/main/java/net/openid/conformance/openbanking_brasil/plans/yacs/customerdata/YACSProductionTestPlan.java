package net.openid.conformance.openbanking_brasil.plans.yacs.customerdata;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v3.resources.ResourcesApiTestModuleV3;
import net.openid.conformance.openbanking_brasil.testmodules.v3.yacs.YACSPreFlightCertCheckV3Module;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.consents.ConsentApiTestModuleV3n;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Open Finance Brasil Functional Production Tests - FVP Phase 2 - Customer Data",
	profile = OBBProfile.OBB_PROFILE_PROD_FVP,
	displayName = PlanNames.OBB_PROD_FVP_PHASE_2,
	summary = "Structural and logical tests for OpenBanking Brasil-conformant Resources API"
)
public class YACSProductionTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					YACSPreFlightCertCheckV3Module.class,
					ConsentApiTestModuleV3n.class,
					ResourcesApiTestModuleV3.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
