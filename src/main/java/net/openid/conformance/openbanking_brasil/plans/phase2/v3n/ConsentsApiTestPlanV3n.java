package net.openid.conformance.openbanking_brasil.plans.phase2.v3n;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.v2.consents.simplifiedConsents.v3n.*;
import net.openid.conformance.openbanking_brasil.testmodules.v3.PreFlightCheckV3Module;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.ConsentsApiCrossClientTestModuleV3n;
import net.openid.conformance.openbanking_brasil.testmodules.v3n.consents.*;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "consents_test-plan_v3-2",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION3,
	displayName = PlanNames.CONSENTS_API_NAME_V3_2,
	summary = PlanNames.CONSENTS_API_NAME_V3_2
)

public class ConsentsApiTestPlanV3n implements TestPlan{

	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV3Module.class,
					ConsentApiTestModuleV3n.class,
					ConsentsApiCrossClientTestModuleV3n.class,
					ConsentsApiDeleteTestModuleV3n.class,
					ConsentsApiRevokedAspspTestModuleV3n.class,
					ConsentApiExtensionConsentRejectedTestModuleV3n.class,
					ConsentApiExtensionInvalidLoggedUserTestModuleV3n.class,
					ConsentApiExtensionInvalidStatusTestModuleV3n.class,
					ConsentApiExtensionMultipleCondTestModuleV3n.class,
					ConsentApiExtensionNoHeaderV3n.class,
					ConsentApiExtensionPastExpirationDateTestModuleV3n.class,
					ConsentApiExtensionTestModuleV3n.class,
					ConsentApiExtensionMultipleTestModuleV3n.class,
					ConsentApiExtensionNegativeTestModuleV3n.class,
					ConsentApiNegativeTestsV3n.class,
					ConsentsApiPermissionGroupsTestModuleV3n.class,
					ConsentsApiOperationalLimitsTestModuleV3n.class,
					ConsentsApiConsentExpiredTestModuleV3n.class,
					ConsentApiBadConsentsTestModuleV3n.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}

}









