package net.openid.conformance.openbanking_brasil.plans.phase2.v2n;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3.CreditOperationsDiscontinuedCreditRightsApiXFapiTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3.CreditOperationsDiscontinuedCreditRightsResourcesApiTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3.CreditOperationsDiscountedApiWrongPermissionsTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3.CreditOperationsDiscountedCreditRightsApiTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.creditOperations.discounted.testmodules.v2n3.InvoiceFinancingsApiOperationalLimitsTestModuleV2n3;
import net.openid.conformance.openbanking_brasil.testmodules.v2.PreFlightCheckV2Module;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "invoice-financings_test-plan_v2-3",
	profile = OBBProfile.OBB_PROFIlE_PHASE2_VERSION2,
	displayName = PlanNames.CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME_V2_3,
	summary = PlanNames.CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME_V2_3
)
public class CreditOperationsDiscountedCreditRightsApiTestPlanV2n3 implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV2Module.class,
					CreditOperationsDiscountedCreditRightsApiTestModuleV2n3.class,
					CreditOperationsDiscountedApiWrongPermissionsTestModuleV2n3.class,
					CreditOperationsDiscontinuedCreditRightsResourcesApiTestModuleV2n3.class,
					InvoiceFinancingsApiOperationalLimitsTestModuleV2n3.class,
					CreditOperationsDiscontinuedCreditRightsApiXFapiTestModuleV2n3.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}
}
