package net.openid.conformance.openbanking_brasil.plans.phase4;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.plans.PlanNames;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.coreTestModule.FundsApiCoreTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.PreFlightCheckV4Module;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.OperationalLimitsTestModule.FundsApiOperationalLimitsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationListConditionalTestModules.FundsApiPaginationListConditionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.FundsApiPaginationTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.paginationTestModules.FundsApiPaginationTransactionsCurrentTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.resourcesTestModules.FundsApiResourcesTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.transactionDateTestModule.FundsApiTransactionDateTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.wrongPermissionsTestModules.FundsApiWrongPermissionsTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.phase4.investmentsApi.xFapiTestModules.FundsApiXFapiTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIAuthRequestMethod;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "funds_test-plan",
	profile = OBBProfile.OBB_PROFIlE_PHASE4B,
	displayName = PlanNames.FUNDS_API_PHASE_4B_TEST_PLAN,
	summary = PlanNames.FUNDS_API_PHASE_4B_TEST_PLAN
)
public class FundsApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					PreFlightCheckV4Module.class,
					FundsApiCoreTestModule.class,
					FundsApiTransactionDateTestModule.class,
					FundsApiResourcesTestModule.class,
					FundsApiWrongPermissionsTestModule.class,
					FundsApiOperationalLimitsTestModule.class,
					FundsApiPaginationTestModule.class,
					FundsApiPaginationTransactionsCurrentTestModule.class,
					FundsApiPaginationListConditionalTestModule.class,
					FundsApiXFapiTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, FAPIResponseMode.PLAIN_RESPONSE.toString()),
					new TestPlan.Variant(ClientAuthType.class, ClientAuthType.PRIVATE_KEY_JWT.toString()),
					new TestPlan.Variant(FAPIAuthRequestMethod.class, FAPIAuthRequestMethod.PUSHED.toString())
				)
			)
		);
	}

}
