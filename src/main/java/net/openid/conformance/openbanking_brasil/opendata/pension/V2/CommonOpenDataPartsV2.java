package net.openid.conformance.openbanking_brasil.opendata.pension.V2;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.util.field.StringField;

public class CommonOpenDataPartsV2 {
	private final AbstractJsonAssertingCondition validator;

	private static class Fields extends ProductNServicesCommonFields {
	}

	public CommonOpenDataPartsV2(AbstractJsonAssertingCondition validator) {
		this.validator = validator;
	}

	public void assertParticipantIdentification(JsonObject participantIdentification) {
		validator.assertField(participantIdentification,
			new StringField
				.Builder("brand")
				.setMinLength(1)
				.setMaxLength(80)
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.build());

		validator.assertField(participantIdentification, Fields.name().setMinLength(1).setMaxLength(80).setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$").build());
		validator.assertField(participantIdentification, Fields.cnpjNumber().setPattern("^\\d{14}$").build());

		validator.assertField(participantIdentification,
			new StringField
				.Builder("urlComplementaryList")
				.setMaxLength(1024)
				.setPattern("^((https?:\\/\\/)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&\\/\\/=]*))$")
				.setOptional()
				.build());

	}
}
