package net.openid.conformance.openbanking_brasil.productsNServices.v1.financings;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api url: https://openbanking-brasil.github.io/openapi/swagger-apis/opendata-financings/1.0.0-beta.2.yml
 * Api endpoint: /business-financings
 * Api version: 1.0.0-beta.2
 */
@ApiName("ProductsNServices Business Financings V1")
public class BusinessFinancingsValidatorV1 extends AbstractJsonAssertingCondition {

	public static final Set<String> TYPES = SetUtils.createSet("FINANCIAMENTO_AQUISICAO_BENS_VEICULOS_AUTOMOTORES, FINANCIAMENTO_AQUISICAO_BENS_OUTROS_BENS, FINANCIAMENTO_MICROCREDITO, FINANCIAMENTO_RURAL_CUSTEIO, FINANCIAMENTO_RURAL_INVESTIMENTO, FINANCIAMENTO_RURAL_COMERCIALIZACAO, FINANCIAMENTO_RURAL_INDUSTRIALIZACAO, FINANCIAMENTO_IMOBILIARIO_SISTEMA_FINANCEIRO_HABITACAO_SFH, FINANCIAMENTO_IMOBILIARIO_SISTEMA_FINANCEIRO_HABITACAO_SFI");
	public static final Set<String> REQUIRED_WARRANTIES = SetUtils.createSet("CESSAO_DIREITOS_CREDITORIOS, CAUCAO, PENHOR, ALIENACAO_FIDUCIARIA, HIPOTECA, OPERACOES_GARANTIDAS_PELO_GOVERNO, OUTRAS_GARANTIAS_NAO_FIDEJUSSORIAS, SEGUROS_ASSEMELHADOS, GARANTIA_FIDEJUSSORIA, BENS_ARRENDADOS, GARANTIAS_INTERNACIONAIS, OPERACOES_GARANTIDAS_OUTRAS_ENTIDADES, ACORDOS_COMPENSACAO, NAO_EXIGE_GARANTIA");
	public static final Set<String> REFERENTIAL_RATE_INDEXER = SetUtils.createSet("SEM_INDEXADOR_TAXA, PRE_FIXADO, POS_FIXADO_TR_TBF, POS_FIXADO_TJLP, POS_FIXADO_LIBOR, POS_FIXADO_TLP, OUTRAS_TAXAS_POS_FIXADAS, FLUTUANTES_CDI, FLUTUANTES_SELIC, OUTRAS_TAXAS_FLUTUANTES, INDICES_PRECOS_IGPM, INDICES_PRECOS_IPCA, INDICES_PRECOS_IPCC, OUTROS_INDICES_PRECO, CREDITO_RURAL_TCR_PRE, CREDITO_RURAL_TCR_POS, CREDITO_RURAL_TRFC_PRE, CREDITO_RURAL_TRFC_POS, OUTROS_INDEXADORES");
	public static final Set<String> INTERVALS = Sets.newHashSet("1_FAIXA", "2_FAIXA", "3_FAIXA", "4_FAIXA");


	@Override
	@PreEnvironment(required = "resource_endpoint_response_full")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment,"resource_endpoint_response_full");
		assertHasField(body, ROOT_PATH);
		assertField(body,
			new ObjectArrayField
				.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.setMinItems(1)
				.build());

		logFinalStatus();
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new ObjectField
				.Builder("participant")
				.setValidator(this::assertParticipant)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("type")
				.setEnums(getTypes())
				.build());

		assertField(data,
			new ObjectField
				.Builder("fees")
				.setValidator(this::assertFees)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("interestRates")
				.setValidator(this::assertRates)
				.setMinItems(1)
				.setMaxItems(20)
				.build());

		assertField(data,
			new StringArrayField
				.Builder("requiredWarranties")
				.setMinItems(1)
				.setMaxItems(14)
				.setEnums(REQUIRED_WARRANTIES)
				.build());

		assertField(data,
			new StringField
				.Builder("termsConditions")
				.setMaxLength(2000)
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.build());
	}

	private void assertParticipant(JsonObject participant) {
		assertField(participant,
			new StringField
				.Builder("brand")
				.setMaxLength(80)
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.build());

		assertField(participant,
			new StringField
				.Builder("name")
				.setMaxLength(80)
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.build());

		assertField(participant,
			new StringField
				.Builder("cnpjNumber")
				.setPattern("^\\d{14}$")
				.setMinLength(14)
				.setMaxLength(14)
				.build());

		assertField(participant,
			new StringField
				.Builder("urlComplementaryList")
				.setMaxLength(1024)
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.setOptional()
				.build());
	}

	private void assertFees(JsonObject fees) {
		assertField(fees,
			new ObjectArrayField
				.Builder("services")
				.setValidator(this::assertServicesPrices)
				.setMaxItems(20)
				.setMinItems(1)
				.setOptional()
				.build());
	}

	private void assertServicesPrices(JsonObject services) {
		assertField(services,
			new StringField
				.Builder("name")
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.setMaxLength(250)
				.build());

		assertField(services,
			new StringField
				.Builder("code")
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.setMaxLength(100)
				.build());

		assertField(services,
			new StringField
				.Builder("chargingTriggerInfo")
				.setPattern("^(?!\\s)[\\w\\W\\s]*[^\\s]$")
				.setMaxLength(2000)
				.build());

		assertField(services,
			new ObjectArrayField
				.Builder("prices")
				.setValidator(this::assertPrices)
				.setMinItems(4)
				.setMaxItems(4)
				.build());

		assertField(services,
			new ObjectField
				.Builder("minimum")
				.setValidator(this::assertValue)
				.build());

		assertField(services,
			new ObjectField
				.Builder("maximum")
				.setValidator(this::assertValue)
				.build());
	}

	private void assertPrices(JsonObject prices) {
		assertField(prices,
			new StringField
				.Builder("interval")
				.setEnums(INTERVALS)
				.build());

		assertField(prices,
			new StringField
				.Builder("value")
				.setPattern("^(\\d{1,9}\\.\\d{2}){1}$")
				.setMaxLength(12)
				.build());

		assertField(prices,
			new StringField
				.Builder("currency")
				.setPattern("^(\\w{3}){1}$")
				.setMaxLength(3)
				.build());

		assertField(prices,
			new ObjectField
				.Builder("customers")
				.setValidator(customers -> {
					assertField(customers,
						new StringField
							.Builder("rate")
							.setPattern("^\\d{1}\\.\\d{6}$")
							.setMinLength(8)
							.setMaxLength(8)
							.build());
				})
				.build());
	}

	private void assertRates(JsonObject rates) {
		assertField(rates,
			new StringField
				.Builder("referentialRateIndexer")
				.setEnums(REFERENTIAL_RATE_INDEXER)
				.build());

		assertField(rates,
			new StringField
				.Builder("rate")
				.setPattern("^\\d{1}\\.\\d{6}$")
				.setMinLength(8)
				.setMaxLength(8)
				.build());

		assertField(rates,
			new ObjectArrayField
				.Builder("applications")
				.setValidator(this::assertApplications)
				.setMinItems(4)
				.setMaxItems(4)
				.build());

		assertField(rates,
			new StringField
				.Builder("minimumRate")
				.setPattern("^\\d{1}\\.\\d{6}$")
				.setMinLength(8)
				.setMaxLength(8)
				.build());

		assertField(rates,
			new StringField
				.Builder("maximumRate")
				.setPattern("^\\d{1}\\.\\d{6}$")
				.setMinLength(8)
				.setMaxLength(8)
				.build());
	}

	private void assertApplications(JsonObject applications) {
		assertField(applications,
			new StringField
				.Builder("interval")
				.setEnums(INTERVALS)
				.build());

		assertField(applications,
			new ObjectField
				.Builder("indexer")
				.setValidator(indexer -> {
					assertField(indexer,
						new StringField
							.Builder("rate")
							.setPattern("^\\d{1}\\.\\d{6}$")
							.setMaxLength(8)
							.setMinLength(8)
							.setOptional()
							.build());
				})
				.build());

		assertField(applications,
			new ObjectField
				.Builder("customers")
				.setValidator(customers -> {
					assertField(customers,
						new StringField
							.Builder("rate")
							.setPattern("^\\d{1}\\.\\d{6}$")
							.setMaxLength(8)
							.setMinLength(8)
							.build());
				})
				.build());
	}

	private void assertValue(JsonObject value) {
		assertField(value,
			new StringField
				.Builder("value")
				.setPattern("^(\\d{1,9}\\.\\d{2}){1}$")
				.setMinLength(4)
				.setMaxLength(12)
				.build());

		assertField(value,
			new StringField
				.Builder("currency")
				.setPattern("^(\\w{3}){1}$")
				.setMinLength(3)
				.setMaxLength(3)
				.build());
	}

	protected Set<String> getTypes() {
		return TYPES;
	}
}
