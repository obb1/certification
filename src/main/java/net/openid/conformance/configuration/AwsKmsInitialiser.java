package net.openid.conformance.configuration;

import com.google.common.base.Strings;
import com.raidiam.conformance.kms.jce.provider.KmsProvider;
import net.openid.conformance.condition.util.AbstractMtlsStrategy;
import net.openid.conformance.condition.util.AwsKmsMtlsStrategy;
import net.openid.conformance.extensions.KmsJWSSignerFactory;
import net.openid.conformance.extensions.MultiJWSSignerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.KmsClientBuilder;

import java.net.URI;
import java.security.Security;

@Configuration
public class AwsKmsInitialiser {

	private static final Logger LOG = LoggerFactory.getLogger(AwsKmsInitialiser.class);

	@Value("${fintechlabs.use.aws.kms:false}") boolean useAwsKms;
	@Value("${fintechlabs.use.aws.kms.lazyeval:true}") boolean lazilyEvaluate;

	@Value("${aws.region}")
	private String region;
	@Value("${aws.kms.endpoint}")
	private String kmsUrl;

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public KmsClient setup() {
			LOG.info("Initialising KMS client in region {}", region);
			KmsClientBuilder builder = KmsClient.builder()
				.region(Region.of(region));
			if(!Strings.isNullOrEmpty(kmsUrl)) {
				builder.endpointOverride(URI.create(kmsUrl));
			}
		KmsClient kmsClient = builder.build();
		if(!lazilyEvaluate) {
				// fail early if client not configured properly
				kmsClient.listAliases();
			}
		Security.insertProviderAt(new KmsProvider(kmsClient), 1);
			LOG.info("KMS security provider registered - provider name awsKms");
			AbstractMtlsStrategy.register("awsKms", new AwsKmsMtlsStrategy());
			MultiJWSSignerFactory signerFactory = (MultiJWSSignerFactory) MultiJWSSignerFactory.getInstance();
			signerFactory.registerAlternative(new KmsJWSSignerFactory(kmsClient));
			LOG.info("KMS JWS signer registered");
			return kmsClient;
	}

}
