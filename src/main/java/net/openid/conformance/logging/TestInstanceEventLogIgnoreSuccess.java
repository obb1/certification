package net.openid.conformance.logging;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.testmodule.OIDFJSON;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class TestInstanceEventLogIgnoreSuccess extends TestInstanceEventLog {

	private static class Log {
		private final String source;
		private JsonObject obj;
		private Map<String, Object> map;

		public Log(String source) {
			this.source = source;
		}


	}

	private final TestInstanceEventLog testInstanceEventLog;

	private final Queue<Log> blockLogs = new LinkedList<>();

	public TestInstanceEventLogIgnoreSuccess(TestInstanceEventLog testInstanceEventLog) {
		super(null, null, null);
		this.testInstanceEventLog = testInstanceEventLog;
	}

	@Override
	public synchronized String startBlock(String message) {
		blockLogs.clear();
		return testInstanceEventLog.startBlock(message);
	}

	@Override
	public synchronized String endBlock() {
		blockLogs.clear();
		return testInstanceEventLog.endBlock();
	}

	@Override
	public synchronized void log(String source, String msg) {
		//Not needed
	}

	@Override
	public synchronized void log(String source, JsonObject obj) {
		if (obj.has("result")) {
			String result = OIDFJSON.getString(obj.get("result"));
			if (!Strings.isNullOrEmpty(result) && !result.equals(Condition.ConditionResult.SUCCESS.toString()) &&
				!result.equals(Condition.ConditionResult.INFO.toString())) {
				printAllBlockLogsBeforeError();
				testInstanceEventLog.log(source, obj);
			} else {
				addLogToQueue(source, obj);
			}
		} else {
			addLogToQueue(source, obj);
		}
	}



	@Override
	public synchronized void log(String source, Map<String, Object> map) {
		if (source.equals("-START-BLOCK-")) {
			testInstanceEventLog.log(source, map);
		} else if (map.containsKey("result")) {
			String result = map.get("result").toString();
			if (result != null && !result.equals(Condition.ConditionResult.SUCCESS.toString()) &&
				!result.equals(Condition.ConditionResult.INFO.toString())) {
				printAllBlockLogsBeforeError();
				testInstanceEventLog.log(source, map);
			} else {
				addLogToQueue(source, map);
			}
		} else {
			addLogToQueue(source, map);
		}
	}

	private void addLogToQueue(String source, Map<String, Object> map) {
		Log log = new Log(source);
		log.map = map;
		blockLogs.add(log);
	}

	private void addLogToQueue(String source, JsonObject obj) {
		Log log = new Log(source);
		log.obj = obj;
		blockLogs.add(log);
	}


	private void printAllBlockLogsBeforeError() {
		while (blockLogs.peek() != null) {
			Log log = blockLogs.remove();
			if (log.map != null) {
				testInstanceEventLog.log(log.source, log.map);
			} else if (log.obj != null) {
				testInstanceEventLog.log(log.source, log.obj);
			}
		}
	}
}
