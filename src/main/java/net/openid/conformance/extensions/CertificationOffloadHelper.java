package net.openid.conformance.extensions;

import net.openid.conformance.token.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class CertificationOffloadHelper {

	private static final Logger LOG = LoggerFactory.getLogger(CertificationOffloadHelper.class);

	private TokenService tokenService;
	private String uri;
	private String credentials;
	private RestTemplate restTemplate;

	public CertificationOffloadHelper(RestTemplate restTemplate, String uri, String credentials, TokenService tokenService) {
		LOG.info("CertificationOffloadHelper will send certification packages to {}", uri);
		this.tokenService = tokenService;
		this.uri = uri;
		this.credentials = credentials;
		this.restTemplate = restTemplate;
	}

	public boolean initiateForAuthenticatedUser(String planId) {
		Map tokenForUser = tokenService.createToken(false);
		return initiateWithToken(planId, tokenForUser);
	}

	public boolean initiateWithToken(String planId, Map tokenForUser) {
		String token = (String) tokenForUser.get("token");
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("plan", planId);
		body.add("apiKey", token);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		if(credentials != null) {
			headers.setBearerAuth(credentials);
		}
		HttpEntity<MultiValueMap> requestEntity = new HttpEntity<>(body, headers);
		try {

			restTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);
		} catch (Exception e) {
			LOG.error("Failed to send certification package to {}", uri, e);
			return false;
		}
		return true;
	}

}
