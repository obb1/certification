package net.openid.conformance.extensions.yacs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static net.openid.conformance.testmodule.DataUtils.DATAUTILS_MEDIATYPE_APPLICATION_JWT_UTF8;


@Service
public class CachedSSAService {

	private static final Logger LOG = LoggerFactory.getLogger(CachedSSAService.class);

	@Cacheable(value = "ssa", key = "#apiBase + '-' + #orgId + '-' + #ssId")
	public String getSsa(String apiBase, String orgId, String ssId, String accessToken, RestTemplate restTemplate) {
		String resourceEndpoint = String.format("%sorganisations/%s/softwarestatements/%s/assertion", apiBase, orgId, ssId);

		LOG.info("Fetching SSA from {}", resourceEndpoint);

		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(DATAUTILS_MEDIATYPE_APPLICATION_JWT_UTF8));
		headers.set("Authorization", "Bearer " + accessToken);

		HttpEntity<String> request = new HttpEntity<>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(resourceEndpoint, HttpMethod.GET, request, String.class);

		return response.getBody();

	}

	@CacheEvict(value = "ssa", allEntries = true)
	@Scheduled(fixedRateString = "180000")
	public void evictSsaCache() {
		LOG.info("Evicting cached SSA");
	}


}
