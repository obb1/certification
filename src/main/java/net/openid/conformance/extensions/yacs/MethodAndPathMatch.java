package net.openid.conformance.extensions.yacs;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public class MethodAndPathMatch implements RequestMatch {

	private final Set<String> methods;
	private final Set<String> paths;

	public MethodAndPathMatch(String method, String path) {
		this(Set.of(method), Set.of(path));
	}

	public MethodAndPathMatch(Set<String> methods, Set<String> paths) {
		this.methods = methods;
		this.paths = paths;
	}

	@Override
	public boolean matches(HttpServletRequest request) {
		String method = request.getMethod();
		String path = request.getServletPath();
		if(path.endsWith("/")) {
			path = path.substring(0, path.length() -1);
		}

		return methods.contains(method) && paths.contains(path);
	}
}
