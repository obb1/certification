package net.openid.conformance.extensions.yacs;

public interface EnhancedVerification {

	EnhancedVerification ALWAYS = (r) -> new VerificationResult(VerificationResult.Decision.SOFT_PASS, "always allowed");

	VerificationResult allowed(BodyRepeatableHttpServletRequest request);

}
