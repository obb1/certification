package net.openid.conformance.extensions.yacs.pcm;

public class ParticipantIdentifiers {

	private String softwareStatementId;
	private String organizationId;

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public void setSoftwareStatementId(String softwareStatementId) {
		this.softwareStatementId = softwareStatementId;
	}

	public String getSoftwareStatementId() {
		return softwareStatementId;
	}

	public String getOrganizationId() {
		return organizationId;
	}

}
