package net.openid.conformance.extensions.yacs.pcm;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface PcmRepository extends PagingAndSortingRepository<PcmEvent, String> {
}
