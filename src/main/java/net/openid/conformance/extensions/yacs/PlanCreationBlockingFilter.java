package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import net.openid.conformance.util.JsonObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

public class PlanCreationBlockingFilter extends AbstractYACSFilter {

	private static final Logger LOG = LoggerFactory.getLogger(PlanCreationBlockingFilter.class);

	private final EnhancedVerification enhancedVerification;

	public PlanCreationBlockingFilter() {
		this(EnhancedVerification.ALWAYS);
	}

	public PlanCreationBlockingFilter(EnhancedVerification enhancedVerification) {
		super(new MethodAndPathMatch("POST", "/api/plan"));
		this.enhancedVerification = enhancedVerification;
	}

	@Override
	public boolean processRequest(BodyRepeatableHttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		LOG.info("New plan being created - performing enhanced verification of user");
		VerificationResult result = enhancedVerification.allowed(request);
		VerificationResult.Decision decision = result.getDecision();
		if(decision == VerificationResult.Decision.SOFT_PASS || decision == VerificationResult.Decision.HARD_PASS) {
			LOG.info("User verified - proceeding");
			return true;
		}
		LOG.info("User not verified - preventing");
		JsonObject errorDetails = new JsonObjectBuilder()
			.addField("link", result.getLink())
			.addField("linktext", result.getLinkExplanation())
			.addField("reason", result.getReason())
			.addField("explanation", result.getReasonExplanation())
			.build();
		String errorObj = Base64.getEncoder().encodeToString(errorDetails.toString().getBytes(Charset.defaultCharset()));
		response.setHeader("X-FAPI-Forbidden-yacs-blocked", errorObj);
		response.sendError(HttpServletResponse.SC_FORBIDDEN);
		return false;

	}
}
