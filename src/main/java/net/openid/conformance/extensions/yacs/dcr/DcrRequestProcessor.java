package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;

public interface DcrRequestProcessor {

	void process(JsonObject jsonObject);

}
