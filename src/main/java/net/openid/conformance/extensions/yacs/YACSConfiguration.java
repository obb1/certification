package net.openid.conformance.extensions.yacs;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jwt.SignedJWT;
import net.openid.conformance.extensions.AuthenticatedUserCacheClearing;
import net.openid.conformance.extensions.AuthenticatedUserRepository;
import net.openid.conformance.extensions.InMemoryAuthenticatedUserRepository;
import net.openid.conformance.extensions.InMemoryTestModuleRepository;
import net.openid.conformance.extensions.TestModuleCacheClearing;
import net.openid.conformance.extensions.YACSKmsOperations;
import net.openid.conformance.extensions.yacs.dcr.DcrClientRepository;
import net.openid.conformance.extensions.yacs.dcr.DcrHelper;
import net.openid.conformance.extensions.yacs.dcr.DcrPostHocTestInfoService;
import net.openid.conformance.extensions.yacs.dcr.DirectoryCredentials;
import net.openid.conformance.extensions.yacs.dcr.DirectoryDetails;
import net.openid.conformance.extensions.yacs.dcr.InMemoryDcrClientRepository;
import net.openid.conformance.extensions.yacs.dcr.OrphanCrushingMachine;
import net.openid.conformance.extensions.yacs.pcm.ConsentTracker;
import net.openid.conformance.extensions.yacs.pcm.EmittingPcmStrategy;
import net.openid.conformance.extensions.yacs.pcm.PcmClient;
import net.openid.conformance.extensions.yacs.pcm.PcmIntegration;
import net.openid.conformance.extensions.yacs.pcm.PcmInterceptorFactory;
import net.openid.conformance.extensions.yacs.pcm.PcmRepository;
import net.openid.conformance.info.SavedConfigurationService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.info.TestPlanService;
import net.openid.conformance.logging.EventLog;
import net.openid.conformance.logging.JsonObjectSanitiser;
import net.openid.conformance.logging.MapSanitiser;
import net.openid.conformance.openbanking_brasil.testmodules.logging.YACSDBExpireLogs;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.mitre.jwt.signer.service.JWTSigningAndValidationService;
import org.mitre.oauth2.model.RegisteredClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;
import software.amazon.awssdk.services.kms.KmsClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Configuration
@ConditionalOnProperty(name = "fintechlabs.extensions", havingValue = "YACS")
@EnableCaching
@EnableScheduling
public class YACSConfiguration {

	@Value("${oidc.gitlab.clientid}")
	private String gitlabClientId;

	@Value("${oidc.gitlab.secret}")
	private String gitlabClientSecret;

	@Value("${oidc.gitlab.iss:https://gitlab.com}")
	private String adminIss;

	@Value("${oidc.obbrazil.clientid}")
	private String obbrazilClientId;

	@Value("${oidc.obbrazil.secret}")
	private String obbrazilClientSecret;

	@Value("${oidc.obbrazil.iss:${fintechlabs.issuer}}")
	private String obbrazilIss;

	@Value("${oidc.redirecturi}")
	private String redirectURI;

	@Value("${fintechlabs.extensions.kmsKey}")
	private String kmsKeyId;

	@Value("${fintechlabs.directoryroots.uri}")
	private String directoryRootsUri;

	public static final String OBB_BRAZIL_CLIENT = "OBB Brasil Directory Client";

	@Bean
	public JWTSigningAndValidationService signingAndValidationService() {
		return new JWTSigningAndValidationService() {
			@Override
			public Map<String, JWK> getAllPublicKeys() {
				return null;
			}

			@Override
			public boolean validateSignature(SignedJWT jwtString) {
				return false;
			}

			@Override
			public void signJwt(SignedJWT jwt) {

			}

			@Override
			public JWSAlgorithm getDefaultSigningAlgorithm() {
				return null;
			}

			@Override
			public Collection<JWSAlgorithm> getAllSigningAlgsSupported() {
				return null;
			}

			@Override
			public void signJwt(SignedJWT jwt, JWSAlgorithm alg) {

			}

			@Override
			public String getDefaultSignerKeyId() {
				return null;
			}
		};
	}

	@Bean
	public DirectoryDetails directoryDetails(ExternalConfigBean configBean) throws FileNotFoundException {
		return configBean.directoryDetails();
	}

	@Bean
	public DcrHelper dcrHelper(DirectoryCredentials directoryCredentials,
							   DirectoryDetails directoryDetails,
							   DcrClientRepository clientRepository,
							   PcmRepository pcmRepository,
							   CachedDiscoveryDocumentService cachedDiscoveryDocumentService) {
		return new DcrHelper(directoryCredentials, directoryDetails, clientRepository, pcmRepository, cachedDiscoveryDocumentService);
	}

	@Bean
	@Primary
	public TestInfoService yacsTestInfoService(TestInfoService delegate,
											   DcrClientRepository clientRepository,
											   DcrHelper dcrHelper,
											   DirectoryCredentials directoryCredentials,
											   YACSIdTokenEncryptionService yacsIdTokenEncryptionService) {
		return new DcrPostHocTestInfoService(delegate, clientRepository, dcrHelper,
			directoryCredentials, yacsIdTokenEncryptionService);
	}

	@Bean
	public OrphanCrushingMachine orphanCrushingMachine(DcrClientRepository clientRepository,
													   DcrHelper dcrHelper) {
		OrphanCrushingMachine orphanCrushingMachine = new OrphanCrushingMachine(clientRepository, dcrHelper);
		Runtime.getRuntime().addShutdownHook(new Thread(new OrphanCrushingMachine.ShutdownHook(clientRepository, orphanCrushingMachine)));
		return orphanCrushingMachine;
	}

	@Bean
	@Primary
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public TestPlanService yacsTestPlanService(TestPlanService delegate,AuthenticatedUserRepository authenticatedUserRepository){
		return new YACSDBTestPlanService(authenticatedUserRepository, delegate);
	}

	@Bean
	@Primary
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public SavedConfigurationService yacsSavedConfigurationService(){
		return new YACSDBSavedConfigurationService();
	}

	@Bean
	public DcrClientRepository dcrClientRepository() {
		return new InMemoryDcrClientRepository();
	}

	@Bean
	public AuthenticatedUserRepository authenticatedUserRepository() {
		return new InMemoryAuthenticatedUserRepository();
	}

	@Bean
	public InMemoryTestModuleRepository inMemoryTestModuleRepository() {
		return new InMemoryTestModuleRepository();
	}

	@Bean
	public TestModuleCacheClearing testModuleCacheClearing(InMemoryTestModuleRepository repository) {
		return new TestModuleCacheClearing(repository);
	}
	@Bean
	public AuthenticatedUserCacheClearing authenticatedUserCacheClearing(AuthenticatedUserRepository repository) {
		return new AuthenticatedUserCacheClearing(repository);
	}
	@Bean
	public PlanCreationBlockingFilter planCreationBlockingFilter(AuthenticationFacade authenticationFacade,
																 ParticipantsService participantsService,
																 ExternalConfigBean configBean
		,@Value("${fintechlabs.devmode:false}")  boolean devmode) {

		EnhancedVerification enhancedVerification = new ChainedEnhancedVerification(List.of(
			new CpfEnhancedVerification(authenticationFacade, devmode),
			new OrgOwnershipEnhancedVerification(authenticationFacade, participantsService, devmode),
			new CertificationManagerVerification(authenticationFacade, devmode),
			new CertificationManagerPlanVerifier(authenticationFacade, configBean.certificationManagerPlans(), devmode)
		));
		PlanCreationBlockingFilter filter = new PlanCreationBlockingFilter(enhancedVerification);
		return filter;
	}

	@Bean
	public ConfigFilter configFilter(ParticipantsService ps, AuthenticationFacade af, DirectoryDetails directoryDetails,
									 @Value("${fintechlabs.yacs.directory.uri}") String participantsApiUri) {
		ConfigFilter filter = new ConfigFilter(ps, af, directoryDetails, directoryRootsUri, participantsApiUri);
		return filter;
	}

	@Bean
	public BeanPostProcessor postProcessor(@Value("${fintechlabs.devmode:false}") boolean devmode) {
		Map<String, RegisteredClient> clients = ImmutableMap.of(obbrazilIss, obbrazilClientConfig(), adminIss, azureClient());
		return new YACSBeanPostProcessor(clients, devmode);
	}

	@Bean
	public YACSIdTokenEncryptionService idTokenEncryptionKeyService(@Value("${fintechlabs.yacs.id_token_encryption_key_path}") String keyPath) {
		if(Strings.isNullOrEmpty(keyPath)){
			// if key path is not provided then nothing will be done
			return config -> {};
		}
		return new YACSIdTokenEncryptionServiceImpl(keyPath);
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public YACSEncryptAndDecryptOperations yacsEncryptAndDecryptOperations(KmsClient kmsClient){
		return new YACSEncryptAndDecryptOperations(new YACSKmsOperations(), kmsClient, kmsKeyId);
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.pcm", havingValue = "true")
	public PcmIntegration pcmIntegration(PcmRepository repository, DirectoryCredentials directoryCredentials,
										 @Value("${pcm.token.endpoint}") String pcmTokenEndpoint,
										 @Value("${pcm.report.endpoint}") String pcmReportEndpoint) throws FileNotFoundException {
		PcmIntegration pcmIntegration = new PcmIntegration(repository, new PcmClient(directoryCredentials, pcmTokenEndpoint, pcmReportEndpoint));
		return pcmIntegration;
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.pcm", havingValue = "true")
	public PcmInterceptorFactory.PcmStrategy pcmInterceptor(PcmRepository repository, ConsentTracker consentTracker, EndpointDecorationService endpointDecorationService, ServerInfoTemplate serverInfoTemplate) throws FileNotFoundException {
		PcmInterceptorFactory.PcmStrategy strategy = new EmittingPcmStrategy(repository, consentTracker, endpointDecorationService, serverInfoTemplate);
		PcmInterceptorFactory.getInstance().setStrategy(strategy);
		return strategy;
	}

	@Bean
	public ConsentTracker consentTracker() {
		return new ConsentTracker();
	}

	private RegisteredClient azureClient() {
		RegisteredClient rc = new RegisteredClient();
		rc.setClientId(gitlabClientId);
		rc.setClientSecret(gitlabClientSecret);
		rc.setScope(ImmutableSet.of("openid", "email"));
		rc.setRedirectUris(ImmutableSet.of(redirectURI));
		return rc;
	}

	private RegisteredClient obbrazilClientConfig() {
		RegisteredClient rc = new RegisteredClient();
		rc.setClientName(OBB_BRAZIL_CLIENT);
		rc.setClientId(obbrazilClientId);
		rc.setResponseTypes(Set.of("code", "id_token"));
		rc.setClientSecret(obbrazilClientSecret);
		rc.setScope(ImmutableSet.of("openid", "profile", "trust_framework_profile"));
		rc.setRedirectUris(ImmutableSet.of(redirectURI));
		return rc;
	}

	@Bean
	public TermsAndConditionsAuthenticationFilter tocFilter(AuthenticationFacade authenticationFacade) {
		return new TermsAndConditionsAuthenticationFilter(authenticationFacade);
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "false")
	public DirectoryCredentials directoryCredentials(ExternalConfigBean configBean) throws FileNotFoundException {
		DirectoryCredentials directoryCredentials = configBean.keyMaterialBearingCredentials();
		return directoryCredentials;
	}

	@Bean
	public ExternalConfigBean externalConfigBean(@Value("${yacs.directory.credentials}") String location) throws FileNotFoundException {
		Resource credentials = new FileSystemResource(location);
		if(!credentials.isFile()) {
			throw new RuntimeException(String.format("Credentials file %s does not exist or is not a file", location));
		}
		YAMLMapper mapper = new YAMLMapper();
		try {
			ExternalConfigBean map = mapper.createParser(new FileInputStream(location))
				.configure(JsonParser.Feature.ALLOW_MISSING_VALUES, true)
				.readValueAs(ExternalConfigBean.class);
			return map;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.use.aws.kms", havingValue = "true")
	public DirectoryCredentials kmsDirectoryCredentials(ExternalConfigBean configBean) throws FileNotFoundException {
		DirectoryCredentials directoryCredentials = configBean.kmsCredentials();
		return directoryCredentials;
	}

	@Bean
	@Primary
	public EventLog encryptingEventLog(JsonObjectSanitiser jsonObjectSanitiser, MapSanitiser mapSanitiser) {
		return new YACSDBExpireLogs(jsonObjectSanitiser, mapSanitiser);
	}

	@Bean
	public CachedDiscoveryDocumentService cachedDiscoveryDocumentService() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		return new CachedDiscoveryDocumentService();
	}
}
