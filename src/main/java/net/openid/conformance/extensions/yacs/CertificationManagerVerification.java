package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.security.AuthenticationFacade;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;

import java.text.ParseException;

public class CertificationManagerVerification implements EnhancedVerification {

	private static final Logger LOG = LoggerFactory.getLogger(CertificationManagerVerification.class);

	private final AuthenticationFacade authenticationFacade;
	private final boolean devMode;

	public CertificationManagerVerification(AuthenticationFacade authenticationFacade, boolean devMode) {
		this.authenticationFacade = authenticationFacade;
		this.devMode = devMode;
    }

    @Override
    public VerificationResult allowed(BodyRepeatableHttpServletRequest request) {
		if(devMode) {
			return VerificationResult.passed();
		}
		JsonObject jobConfig = request.body();
		JsonObject resource = jobConfig.getAsJsonObject("resource");
		if(resource==null) {
			return VerificationResult.generalRulesViolation();
		}

		Authentication contextAuthentication = authenticationFacade.getContextAuthentication();
		if(!contextAuthentication.getClass().isAssignableFrom(OIDCAuthenticationToken.class)) {
			LOG.error("Logged in authentication is not OIDC token");
			return VerificationResult.noComment();
		}
		OIDCAuthenticationToken oidcAuth = (OIDCAuthenticationToken) contextAuthentication;
		JWT idToken = oidcAuth.getIdToken();
		try {
			var trustProfile =  idToken.getJWTClaimsSet().getJSONObjectClaim("trust_framework_profile");
			if(trustProfile == null) {
				return VerificationResult.noComment();
			}
			Object conformanceManagerClaim = trustProfile.get("certification_manager");
			if(conformanceManagerClaim == null) {
				return VerificationResult.noComment();
			}
			boolean conformanceManager = (boolean) conformanceManagerClaim;
			return conformanceManager ? VerificationResult.hardPass() : VerificationResult.noComment();
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
    }
}
