package net.openid.conformance.extensions.yacs.pcm;

import net.openid.conformance.testmodule.Environment;
import org.apache.http.HttpResponseInterceptor;

public class PcmInterceptorFactory {

	private static PcmInterceptorFactory INSTANCE = new PcmInterceptorFactory();
	private PcmStrategy strategy  = (env, testId) -> (r, c) -> {};

	public static PcmInterceptorFactory getInstance() {
		return INSTANCE;
	}

	public HttpResponseInterceptor interceptorFor(Environment env, String testId) {
		return strategy.interceptorFor(env, testId);
	}

	public void setStrategy(PcmStrategy strategy) {
		this.strategy = strategy;
	}

	public interface PcmStrategy {

		HttpResponseInterceptor interceptorFor(Environment env, String testId);

	}

}
