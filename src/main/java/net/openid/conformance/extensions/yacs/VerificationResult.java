package net.openid.conformance.extensions.yacs;

public class VerificationResult {


	private static final String DEFAULT_LINK = "/fvpPlanCreationRestrictions.html";
	private static final String DEFAULT_LINK_EXPLANATION = "See here for more details";


	public enum Decision {
		SOFT_PASS,
		SOFT_FAIL,
		HARD_PASS,
		HARD_FAIL,
		NO_COMMENT
	}

	private final String reason;
	private final String reasonExplanation;
	private final String link;
	private final String linkExplanation;
	private final Decision decision;

	public VerificationResult(Decision decision, String reason) {
		this(decision, reason, null, DEFAULT_LINK, DEFAULT_LINK_EXPLANATION);
	}

	public VerificationResult(Decision decision, String reason, String reasonExplanation, String link, String linkExplanation) {
		this.decision = decision;
		this.reason = reason;
		this.reasonExplanation = reasonExplanation;
		this.link = link;
		this.linkExplanation = linkExplanation;
	}

	public VerificationResult(Decision decision) {
		this(decision, null);
	}

	public Decision getDecision() {
		return decision;
	}

	public String getReason() {
		return reason;
	}

	public String getReasonExplanation() {
		return reasonExplanation;
	}

	public String getLink() {
		return link;
	}

	public String getLinkExplanation() {
		return linkExplanation;
	}

	public static VerificationResult passed() {
		return new VerificationResult(Decision.SOFT_PASS, "passed");
	}

	public static VerificationResult hardPass() {
		return new VerificationResult(Decision.HARD_PASS, "passed");
	}

	public static VerificationResult noComment() {
		return new VerificationResult(Decision.NO_COMMENT, "");
	}

	public static VerificationResult generalRulesViolation() {
		return new VerificationResult(Decision.SOFT_FAIL,
			"You are not permitted to create this test plan as it targets an authorisation server not owned by your organisation or you are using a national identity which is not your own.");
	}

	public static VerificationResult certManagerOnly() {
		return new VerificationResult(Decision.HARD_FAIL, "Unfortunately it's not possible to create this Test Plan.", "You are attempting to execute a test that is not scheduled for Execution by The Central Structure. As a Regular PFVPC User, you do not have the necessary permissions to perform this action.",
			"https://gitlab.com/raidiam-conformance/open-finance/certification/-/wikis/FVP", "Please refer to the Platform Documentation or additional information around the available tests for Execution.");
	}

	public static VerificationResult generalRulesViolation(boolean hard) {
		Decision decision = hard ? Decision.HARD_FAIL : Decision.SOFT_FAIL;
		return new VerificationResult(decision, "You are not permitted to create this test plan as it targets an authorisation server not owned by your organisation or you are using a national identity which is not your own.");
	}

	public static VerificationResult aliasProblems() {
		return new VerificationResult(Decision.SOFT_FAIL, "If left blank alias will use one of the organisation for the user on the directory. The alias, if provided must match an org which uses the provided well-known");
	}

	public static VerificationResult generic() {
		return new VerificationResult(Decision.SOFT_FAIL);
	}


}
