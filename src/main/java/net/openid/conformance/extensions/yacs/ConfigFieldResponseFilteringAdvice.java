package net.openid.conformance.extensions.yacs;

import net.openid.conformance.info.TestPlanApi;
import net.openid.conformance.runner.TestRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@ConditionalOnProperty(name = "fintechlabs.extensions", havingValue = "YACS")
@RestControllerAdvice
public class ConfigFieldResponseFilteringAdvice implements ResponseBodyAdvice<Collection<Map<String, Object>>> {

	public static final List<String> PERMITTED_FIELDS = List.of(
		"server.discoveryUrl", "server.authorisationServerId", "resource.brazilCpf", "resource.brazilCnpj", "resource.name", "resource.consentSyncTime",
		"resource.creditorAccountIspb", "resource.creditorAccountIssuer", "resource.creditorAccountNumber",
		"resource.creditorAccountAccountType", "resource.creditorName", "resource.creditorCpfCnpj", "resource.creditorProxy",
		"resource.loggedUserIdentification", "resource.paymentAmount", "resource.debtorAccountIspb",
		"resource.debtorAccountIssuer", "resource.debtorAccountNumber", "resource.debtorAccountType", "directory.participants",
		"directory.keystore", "directory.apibase", "directory.directoryRootsUri", "directory.discoveryUrl"
	);

	@Override
	public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
		Method method = methodParameter.getMethod();
		Class<?> declaringClass = method.getDeclaringClass();
		String name = method.getName();
		boolean isInterestingClass = declaringClass.isAssignableFrom(TestPlanApi.class) || declaringClass.isAssignableFrom(TestRunner.class);
		boolean isInterestingMethod = name.equals("getAvailableTestPlans") || name.equals("getAvailableTests");
		boolean shouldAdvise = isInterestingClass && isInterestingMethod;
		return shouldAdvise;
	}

	@Override
	public Collection<Map<String, Object>> beforeBodyWrite(Collection<Map<String, Object>> body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
		body.stream()
			.forEach(this::clean);
		return body;
	}

	private void clean(Map<String, Object> entry) {
		Object configFields = entry.get("configurationFields");
		if (configFields instanceof Collection<?>) {
			Collection<String> fields = (Collection<String>) configFields;
			fields.retainAll(PERMITTED_FIELDS);
		} else if (configFields instanceof String[] fieldsArray) {
			List<String> fieldsList = new ArrayList<>(Arrays.asList(fieldsArray));
			fieldsList.retainAll(PERMITTED_FIELDS);
			entry.put("configurationFields", fieldsList.toArray(new String[0]));
		}
	}

}
