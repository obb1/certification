package net.openid.conformance.extensions.yacs;

import com.google.common.base.Strings;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.OctetSequenceKey;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.SignedJWT;
import org.mitre.jose.keystore.JWKSetKeyStore;
import org.mitre.jwt.signer.service.JWTSigningAndValidationService;
import org.mitre.jwt.signer.service.impl.JWKSetCacheService;
import org.springframework.web.client.RestTemplate;

import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This class is part of a circuitous mechanism for intercepting the validation of
 * id tokens which are signed using PS256. Because of the intricacies of Java Security Architecture
 * we have little control over which provider is used for any given algorithm. Thus, our KMS provider
 * is sometimes asked to perform actions with plain old RSA keys, which it cannot do, but by then it
 * has already been selected for the job. This class, and related classes, force the BouncyCastle
 * provider to be selected for dealing with these id tokens, which are coming from the directory
 */
public class DelegatingCacheService extends JWKSetCacheService {

	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public JWTSigningAndValidationService getValidator(String jwksUri) {
		String jsonString = restTemplate.getForObject(jwksUri, String.class);
		JWKSet jwkSet = null;
		try {
			jwkSet = JWKSet.parse(jsonString);
			JWKSetKeyStore keyStore = new JWKSetKeyStore(jwkSet);
			JWTSigningAndValidationService realValidator = super.getValidator(jwksUri);
			return new DelegatingJWTSAVS(realValidator, keyStore);
		} catch (ParseException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			return super.getValidator(jwksUri);
		}
	}

	public static class DelegatingJWTSAVS implements JWTSigningAndValidationService {

		private final JWTSigningAndValidationService delegate;
		private Map<String, JWSSigner> signers = new HashMap<>();
		private Map<String, JWSVerifier> verifiers = new HashMap<>();
		private Map<String, JWK> keys = new HashMap<>();

		public DelegatingJWTSAVS(JWTSigningAndValidationService delegate, JWKSetKeyStore keyStore) throws NoSuchAlgorithmException, InvalidKeySpecException {
			this.delegate = delegate;
			if (keyStore!= null && keyStore.getJwkSet() != null) {
				for (JWK key : keyStore.getKeys()) {
					if (!Strings.isNullOrEmpty(key.getKeyID())) {
						// use the key ID that's built into the key itself
						this.keys.put(key.getKeyID(), key);
					} else {
						// create a random key id
						String fakeKid = UUID.randomUUID().toString();
						this.keys.put(fakeKid, key);
					}
				}
			}
			buildSignersAndVerifiers();
		}

		@Override
		public Map<String, JWK> getAllPublicKeys() {
			return delegate.getAllPublicKeys();
		}

		@Override
		public boolean validateSignature(SignedJWT jwt) {

			for (JWSVerifier verifier : verifiers.values()) {
				try {
					if (jwt.verify(verifier)) {
						return true;
					}
				} catch (JOSEException e) {
				}
			}
			return false;
		}

		@Override
		public void signJwt(SignedJWT jwt) {
			delegate.signJwt(jwt);
		}

		@Override
		public JWSAlgorithm getDefaultSigningAlgorithm() {
			return delegate.getDefaultSigningAlgorithm();
		}

		@Override
		public Collection<JWSAlgorithm> getAllSigningAlgsSupported() {
			return delegate.getAllSigningAlgsSupported();
		}

		@Override
		public void signJwt(SignedJWT jwt, JWSAlgorithm alg) {
			delegate.signJwt(jwt, alg);
		}

		@Override
		public String getDefaultSignerKeyId() {
			return delegate.getDefaultSignerKeyId();
		}

		private void buildSignersAndVerifiers() throws NoSuchAlgorithmException, InvalidKeySpecException {
			for (Map.Entry<String, JWK> jwkEntry : keys.entrySet()) {

				String id = jwkEntry.getKey();
				JWK jwk = jwkEntry.getValue();

				try {
					if (jwk instanceof RSAKey) {
						// build RSA signers & verifiers

						if (jwk.isPrivate()) { // only add the signer if there's a private key
							RSASSASigner signer = new RSASSASigner((RSAKey) jwk);
							signer.getJCAContext().setProvider(Security.getProvider("BC"));
							signers.put(id, signer);
						}

						RSASSAVerifier verifier = new RSASSAVerifier((RSAKey) jwk);
						verifier.getJCAContext().setProvider(Security.getProvider("BC"));
						verifiers.put(id, verifier);

					} else if (jwk instanceof ECKey) {
						// build EC signers & verifiers

						if (jwk.isPrivate()) {
							ECDSASigner signer = new ECDSASigner((ECKey) jwk);
							signers.put(id, signer);
						}

						ECDSAVerifier verifier = new ECDSAVerifier((ECKey) jwk);
						verifiers.put(id, verifier);

					} else if (jwk instanceof OctetSequenceKey) {
						// build HMAC signers & verifiers

						if (jwk.isPrivate()) { // technically redundant check because all HMAC keys are private
							MACSigner signer = new MACSigner((OctetSequenceKey) jwk);
							signers.put(id, signer);
						}

						MACVerifier verifier = new MACVerifier((OctetSequenceKey) jwk);
						verifiers.put(id, verifier);

					} else {
					}
				} catch (JOSEException e) {
				}
			}

		}

	}

}
