package net.openid.conformance.extensions.yacs;

import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.client.service.AuthRequestOptionsService;
import org.mitre.openid.connect.config.ServerConfiguration;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static net.openid.conformance.extensions.yacs.YACSConfiguration.OBB_BRAZIL_CLIENT;

public class ClaimRequestingOptions implements AuthRequestOptionsService  {

	private Map<String, String> noOpts = new HashMap<>();
	private Map<String, String> addClaims = Map.of("claims", "{\"id_token\":{\"national_id\":null,\"email\":null}}");
	private Map<String, String> tokenOptions = new HashMap<>();

	@Override
	public Map<String, String> getOptions(ServerConfiguration server, RegisteredClient client, HttpServletRequest request) {
		if(OBB_BRAZIL_CLIENT.equals(client.getClientName())) {
			return addClaims;
		}
		return noOpts;
	}

	@Override
	public Map<String, String> getTokenOptions(ServerConfiguration server, RegisteredClient client, HttpServletRequest request) {
		return tokenOptions;
	}
}
