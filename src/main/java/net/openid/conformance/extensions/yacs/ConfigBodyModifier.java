package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;

public interface ConfigBodyModifier {

	void apply(JsonObject jsonObject);

}
