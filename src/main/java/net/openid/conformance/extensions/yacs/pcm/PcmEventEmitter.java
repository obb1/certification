package net.openid.conformance.extensions.yacs.pcm;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import net.openid.conformance.extensions.yacs.EndpointDecorationService;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.BaseHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.GetEnrollmentHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.PatchEnrollmentHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.PostConsentsAuthoriseHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.PostEnrollmentsHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.PostFidoRegistrationHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.PostFidoRegistrationOptionsHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.PostFidoSignOptionsHandler;
import net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers.PostRiskSignalsHandler;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ManagedHttpClientConnection;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLSession;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.time.Instant;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;


public class PcmEventEmitter implements HttpResponseInterceptor {

	private static final Logger LOG = LoggerFactory.getLogger(PcmEventEmitter.class);
	private static final String PCM_ROLE = "CLIENT";

	private final PcmRepository repository;
	private final Environment env;
	private final String testId;
	private final JsonObject oidcDiscoveryDoc;
	private final String discoveryUrl;
	private final String consentUrl;
	private final EndpointDecorationService endpointDecorationService;
	private List<TrackedConsent> myConsents;
	private Set<String> urlsToIgnore = new HashSet<>();
	private final Instant start = Instant.now();



	private final List<BaseHandler> handlers;

	public PcmEventEmitter(
		PcmRepository repository,
		EndpointDecorationService endpointDecorationService,
		ConsentTracker consentTracker,
		Environment env,
		String testId,
		ServerInfoTemplate serverInfoTemplate
	) {
		this.repository = repository;
		this.env = env;
		this.discoveryUrl = env.getString("config", "server.discoveryUrl");
		this.consentUrl = env.getString("config", "resource.consentUrl");
		urlsToIgnore.add(discoveryUrl);
		oidcDiscoveryDoc = env.getObject("server");
		inferUrlsToIgnore(oidcDiscoveryDoc);
		this.testId = testId;
		myConsents = consentTracker.getConsentsForTest(testId);
		this.endpointDecorationService = endpointDecorationService;

		this.handlers = List.of(
			new GetEnrollmentHandler(serverInfoTemplate),
			new PatchEnrollmentHandler(serverInfoTemplate),
			new PostConsentsAuthoriseHandler(serverInfoTemplate),
			new PostEnrollmentsHandler(serverInfoTemplate),
			new PostFidoRegistrationHandler(serverInfoTemplate),
			new PostFidoRegistrationOptionsHandler(serverInfoTemplate),
			new PostFidoSignOptionsHandler(serverInfoTemplate),
			new PostRiskSignalsHandler(serverInfoTemplate)
		);

	}

	private void inferUrlsToIgnore(JsonObject oidcDiscoveryDoc) {
		if(oidcDiscoveryDoc == null) {
			return;
		}
		String jwksUri = OIDFJSON.getString(oidcDiscoveryDoc.get("jwks_uri"));
		urlsToIgnore.add(jwksUri);
	}

	@Override
	public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
		try {
			intProcess(response, context);
		} catch (Exception e) {
			LOG.error("PCM Error: Error emitting PCM event {} {}", e.getMessage(), e.getCause());
		}
	}

	private void intProcess(HttpResponse response, HttpContext context) throws HttpException, IOException {

		HttpRequestWrapper originalRequest = (HttpRequestWrapper) context.getAttribute("http.request");
		HttpUriRequest actualRequest = (HttpUriRequest) originalRequest.getOriginal();
		URI uri = actualRequest.getURI();
		String url = uri.toString();
		if(urlsToIgnore.contains(url)) {
			return;
		}
		Instant end = Instant.now();
		long duration = end.toEpochMilli() - start.toEpochMilli();
		ManagedHttpClientConnection conn = (ManagedHttpClientConnection) context.getAttribute(HttpCoreContext.HTTP_CONNECTION);
		SSLSession sslSession = conn.getSSLSession();
		Certificate[] localCertificates = sslSession.getLocalCertificates();
		ParticipantIdentifiers clientIdentifiers = new ParticipantIdentifiers();

		if(!sslSession.isValid()) {
			LOG.info("SSL session is invalid | URL - {}", url);
			return;
		}

		String endpointUriPrefix = originalRequest.getTarget().getHostName();
		String endpoint = endpointDecorationService.decorateEndpoint(originalRequest.getRequestLine().getUri());

		LOG.info("Extracting identifiers from endpoint - {} | prefix - {}", endpoint, endpointUriPrefix);

		if(localCertificates != null) {
			X509Certificate certificate = (X509Certificate) localCertificates[0];
			extractIdentifiers(certificate, clientIdentifiers);
		}

		int statusCode = response.getStatusLine().getStatusCode();
		HttpEntity responseEntity = response.getEntity();
		String responseBody;
		if (responseEntity != null) {
			response.setEntity(new BufferedHttpEntity(responseEntity));
			responseBody = EntityUtils.toString(response.getEntity());
		} else {
			responseBody = "";
		}
		String httpMethod = originalRequest.getRequestLine().getMethod();
		if(consentUrl != null && url.startsWith(consentUrl) && !url.equals(consentUrl)) {
			endpoint = endpoint.substring(0, endpoint.lastIndexOf('/'));
		}
		String clientSSId = clientIdentifiers.getSoftwareStatementId();
		String clientOrgId = clientIdentifiers.getOrganizationId();
		String serverOrgId = getServerOrgId();
		Header fapiInteractionIdHeader = originalRequest.getFirstHeader("x-fapi-interaction-id");
		String fapiInteractionId = Optional.ofNullable(fapiInteractionIdHeader)
			.map(Header::getValue)
			.orElse(null);

		JsonObject additionalInfo = null;
		try {
			additionalInfo = generateAdditionalInfo(endpoint, httpMethod, responseBody, statusCode, originalRequest);
		} catch (Exception e) {
			LOG.error("Error parsing additional info {} {}", e.getMessage(), e.getCause());
		}
		PcmEvent pcmEvent = new PcmEvent(statusCode, httpMethod, endpointUriPrefix, fapiInteractionId,
			endpoint, clientSSId, clientOrgId, serverOrgId, duration, PCM_ROLE, additionalInfo);
		PcmEvent savedPcmEvent = repository.save(pcmEvent);
		LOG.info("PCM Event saved to the repository - {}", savedPcmEvent);
	}

	private String getServerOrgId() {
		return Optional.ofNullable(env.getElementFromObject("config", "alias"))
			.map(OIDFJSON::getString)
			.orElse(null);
	}

	private JsonObject generateAdditionalInfo(String decoratedEndpoint, String method, String response, int statusCode, HttpRequestWrapper requestBody) throws Exception {
		LOG.info("Starting additional info generation. Endpoint: {}, Method: {}, Status Code: {}", decoratedEndpoint, method, statusCode);
		JsonObject additionalInfo = new JsonObject();

		Optional<BaseHandler> handler = findMatchingHandler(method, decoratedEndpoint);

		handler.ifPresentOrElse(
			h -> {
				LOG.info("Matching handler found: {}", h.getClass().getSimpleName());
				h.initResponse(statusCode, response);
				h.handle(additionalInfo, requestBody);
				LOG.info("Additional info after handler {}: {}", h.getClass().getSimpleName(), additionalInfo);
			},
			() -> LOG.info("No matching handler was found for method {} and endpoint {}", method, decoratedEndpoint)
		);

		findConsent();
		findToken();
		findAuthRequest();

		LOG.info("Additional info generated: {}", additionalInfo);
		return additionalInfo;
	}

	private Optional<BaseHandler> findMatchingHandler(String httpMethod, String decoratedEndpoint) {
		return handlers.stream()
			.filter(h -> h.getDecoratedEndpoints().contains(decoratedEndpoint))
			.filter(h -> h.getMethod().toString().equals(httpMethod))
			.findFirst();
	}


	private void findConsent() throws ParseException {
		JsonObject consentEndpointResponseFull = env.getObject("consent_endpoint_response_full");
		if(consentEndpointResponseFull == null) {
			return;
		}

		String consentResponse = OIDFJSON.getString(consentEndpointResponseFull.get("body"));
		if(consentResponse == null) {
			return;
		}
		JsonElement statusCodeElement = consentEndpointResponseFull.get("status");
		if(statusCodeElement == null) {
			return;
		}
		int statusCode = OIDFJSON.getInt(statusCodeElement);
		if(statusCode != 200) {
			return;
		}
		String protectedResourceUrl = env.getString("protected_resource_url");
		if(BodyExtractor.isJwt(consentResponse)) {
			processJwt(consentResponse, protectedResourceUrl);
		} else {
			processJson(consentResponse, protectedResourceUrl);
		}

	}

	private void processJson(String consentResponse, String protectedResourceUrl) {

		JsonObject consentResponseObject = JsonParser.parseString(consentResponse).getAsJsonObject();
		JsonElement data = consentResponseObject.get("data");
		String consentId;
		if(data == null) {
			if(protectedResourceUrl == null) {
				return;
			}
			consentId = extractLastUrlSegment(protectedResourceUrl);
		} else {
			JsonObject dataObject = data.getAsJsonObject();
			JsonElement consentIdElement = dataObject.get("consentId");
			if(consentIdElement == null) {
				consentIdElement = dataObject.get("recurringConsentId");
			}
			if(consentIdElement == null) {
				consentIdElement = dataObject.get("enrollmentId");
			}
			if(consentIdElement == null) {
				return;
			}
			consentId = OIDFJSON.getString(consentIdElement);
		}

		Optional<TrackedConsent> maybeConsent = myConsents.stream().filter(c -> c.getConsentId().equals(consentId)).findFirst();
		if(maybeConsent.isPresent()) {
			return;
		}
		TrackedConsent consent = new TrackedConsent(testId);
		consent.setConsentId(consentId);
		myConsents.add(consent);
	}

	private void processJwt(String consentResponse, String protectedResourceUrl) {
		try {
			JWT jwt = JWTParser.parse(consentResponse);
			JWTClaimsSet jwtClaimsSet = jwt.getJWTClaimsSet();
			Map<String, Object> data;
			String consentId;
			if(!jwtClaimsSet.getClaims().containsKey("data")) {
				// Get consentId from protected resource URL if 4xx or 5xx is returned, and we can't get the consentId from the response body
				if(protectedResourceUrl == null) {
					return;
				}
				consentId = extractLastUrlSegment(protectedResourceUrl);
			} else {
				// Get consentId from data
				data = jwtClaimsSet.getJSONObjectClaim("data");
				Optional<Object> consentIdOptional = Optional.ofNullable(data.get("consentId"))
					.or(() -> Optional.ofNullable(data.get("recurringConsentId")))
					.or(() -> Optional.ofNullable(data.get("enrollmentId")));
				consentId = String.valueOf(consentIdOptional.orElse(null));
			}
			Optional<TrackedConsent> maybeConsent = myConsents.stream().filter(c -> c.getConsentId().equals(consentId)).findFirst();
			if(maybeConsent.isPresent()) {
				return;
			}
			TrackedConsent consent = new TrackedConsent(testId);
			consent.setConsentId(consentId);
			myConsents.add(consent);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public static String extractLastUrlSegment(String url) {
		String[] segments = url.split("/");
		return segments[segments.length - 1];
	}

	private void findToken() {
		String token = env.getString("token_endpoint_response", "access_token");
		String authCode = env.getString("token_endpoint_request_form_parameters", "code");
		if(authCode != null) {
			myConsents.stream()
				.filter(c -> authCode.equals(c.getAuthCode()))
				.findFirst()
				.ifPresent(c -> c.setAccessToken(token));
		}
	}

	private void findAuthRequest() {
		String authCode = env.getString("authorization_endpoint_response", "code");
		String scopes = env.getString("authorization_endpoint_request", "scope");
		if(authCode == null) {
			return;
		}
		myConsents.stream()
			.filter(c -> scopes.contains(c.getConsentId()))
			.findFirst()
			.ifPresent(c -> c.setAuthCode(authCode));
	}

	private static String getBase64PemString(X509Certificate certificate) throws IOException {
		StringWriter stringWriter = new StringWriter();
		try (JcaPEMWriter pemWriter = new JcaPEMWriter(stringWriter)) {
			pemWriter.writeObject(certificate);
		}
		return Base64.getEncoder().encodeToString(stringWriter.toString().getBytes());
	}

	public static void extractIdentifiers(X509Certificate certificate, ParticipantIdentifiers participantIdentifiers) {

		X500Principal x500Principal = certificate.getSubjectX500Principal();

		X500Name x500name = X500Name.getInstance(x500Principal.getEncoded());

		LOG.info("Extracting identifiers from certificate DN - {}", x500name.toString());

		try {
			String pemString = getBase64PemString(certificate);
			LOG.info("Base 64 PEM - {}", pemString);
		} catch (IOException ignored) {

		}

		RDN[] ouArray = x500name.getRDNs(BCStyle.OU);
		String ouAsString;
		JsonObject o = new JsonObject();
		if(ouArray.length == 0) {
			RDN[] oiArray = x500name.getRDNs(BCStyle.ORGANIZATION_IDENTIFIER);
			if(oiArray.length == 0) {
				LOG.info("No OU or OI found in certificate DN - {}", x500name.toString());
				return;
			}
			String oi = IETFUtils.valueToString(oiArray[0].getFirst().getValue());
			String[] split = oi.split("-", 2);
			if(split.length != 2) {
				return;
			}
			o.addProperty("org_type", split[0]);
			participantIdentifiers.setOrganizationId(split[1]);
		} else {
			RDN ou = ouArray[0];
			ouAsString = IETFUtils.valueToString(ou.getFirst().getValue());
			participantIdentifiers.setOrganizationId(ouAsString);
		}
		RDN[] uid = x500name.getRDNs(BCStyle.UID);
		String softwareId;
		if(uid.length == 0) {
			return;
		} else {
			softwareId = IETFUtils.valueToString(uid[0].getFirst().getValue());
		}

		participantIdentifiers.setSoftwareStatementId(softwareId);
		LOG.info("Extracted identifiers - {}", participantIdentifiers);
	}

}
