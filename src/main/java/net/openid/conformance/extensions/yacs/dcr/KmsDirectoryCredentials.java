package net.openid.conformance.extensions.yacs.dcr;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.Base64URL;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static net.openid.conformance.extensions.yacs.dcr.CertificateUtils.sanitise;

public class KmsDirectoryCredentials implements DirectoryCredentials {

	private static final String CLIENT_CERT_PROP = "credentials.client-certificate";
	private static final String CLIENT_KEY_PROP = "credentials.client-key";
	private static final String CLIENT_CA_PROP = "credentials.client-ca";
	private static final String SIGNING_KEY_PROP = "credentials.signing-key";
	private static final String SIGNING_CERT_PROP = "credentials.signing-cert";
	private static final String ORG_ID_PROP = "credentials.org-id";
	private static final String SS_ID_PROP = "credentials.software-statement-id";
	private static final String DIRECTORY_CLIENT_ID_PROP = "credentials.directory-client-id";
	private String directoryClientId;
	private String orgId;
	private String softwareStatementId;

	private String clientKey;
	private String clientCertificate;
	private String clientCaCertificate;
	private String signingKey;
	private String signingCert;
	private JsonObject signingJwks;

	public KmsDirectoryCredentials(Map<String, Object> properties) {
		parse(properties);
	}

	private void parse(Map<String, Object> properties) {
		clientCertificate = findIfPresent(CLIENT_CERT_PROP, properties, true);
		clientKey = findIfPresent(CLIENT_KEY_PROP, properties);
		clientCaCertificate = findIfPresent(CLIENT_CA_PROP, properties, true);
		signingKey = findIfPresent(SIGNING_KEY_PROP, properties);
		signingCert = findIfPresent(SIGNING_CERT_PROP, properties, true);
		orgId = findIfPresent(ORG_ID_PROP, properties);
		softwareStatementId = findIfPresent(SS_ID_PROP, properties);
		directoryClientId = findIfPresent(DIRECTORY_CLIENT_ID_PROP, properties);
		signingJwks = buildSigningJwks(signingKey);
	}

	private String findIfPresent(String key, Map<String, Object> properties) {
		return findIfPresent(key, properties, false);
	}

	private String findIfPresent(String key, Map<String, Object> properties, boolean decode) {
		Base64.Decoder decoder = Base64.getDecoder();
		String raw =  Optional.ofNullable(properties.get(key))
			.orElseThrow(() -> new RuntimeException(String.format("Property %s not found", key)))
			.toString();
		return decode ? new String(decoder.decode(raw)) : raw;
	}

	@Override
	public String orgId() {
		return orgId;
	}

	@Override
	public String softwareStatementId() {
		return softwareStatementId;
	}

	@Override
	public String directoryClientId() {
		return directoryClientId;
	}

	@Override
	public PrivateKey clientKey() {
		return null;
	}

	@Override
	public X509Certificate clientCert() {
		String certData = sanitise(clientCertificate);
		byte[] certBytes = Base64.getDecoder().decode(certData);
		try {
			return CertificateUtils.generateCertificateFromDER(certBytes);
		} catch (CertificateException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<X509Certificate> clientCa() {
		List<X509Certificate> chain = Lists.newArrayList(clientCert());
		String caData = clientCaCertificate;
		if(caData == null) {
			return chain;
		}
		caData = sanitise(caData);
		byte[] caBytes = Base64.getDecoder().decode(caData);
		try {
			chain.addAll(CertificateUtils.generateCertificateChainFromDER(caBytes));
		} catch (CertificateException e) {
			throw new RuntimeException(e);
		}
		return chain;
	}

	@Override
	public JWK signingKey() {
		return null;
	}

	/* Because the conformance suite validates the private key field on creation of a
	 * plan, to ensure it really is a private key, we have to inject a dummy one. This
	 * private key is not used for anything, as we use KMS for the MTLS connections, and
	 * the key material here was generated specifically to live here, and is of no use
	 * to anyone else.
	 */
	@Override
	public String clientKeyData() {
		return "-----BEGIN RSA PRIVATE KEY-----\n" +
			"MIIEogIBAAKCAQEAv9JW4AI34NZFZPBU68g8E4MNd74ogybHMJBGV1/wcWYXvjVv\n" +
			"Yx3f5vfLPIV8+UPzH69vVYz4+f/sPU5Y9UE2nGaMyC3+e8PVLvLVc2L/EWAajx+9\n" +
			"ivaGgSJ9gnb3lZpPC3IgudSTDR2hXfCg+NQkGLHc02x+Bs3vwLp5jEJeNEc7oC4S\n" +
			"Y5d9mbge2JM+MqT79nNbmNXTJUwet+W10/CDgediDuwzEGXlpFJE62PaMQUiOdN7\n" +
			"kIBjtz9AyhAH+B59mEeyyoaV+V2EucM0sOZVgOx3CCJO1dxqef5L4luQYYLwhLcI\n" +
			"CDbqcdtrWv0FrsDtDkgMg4E/xUg0L6fsW5g8JwIDAQABAoIBAA764CnyQmbelODD\n" +
			"G/6lzns/mselJAjJ+HhEbBnati5E6MnD5oT5CklBRK/fRNk6gzwQOUOXFMKBR/Op\n" +
			"sKzGgPtJzui7HsqPipB2ClFOg9jZhnly4xx9EBf5l4+etssKF+4u5XtdrHYzwCxT\n" +
			"5F5KQt9i1Hn2WQky/+wvI9reO/kdDFI+/XHBmfvQvFWFmFPQxs364h2Mh9J7HmMz\n" +
			"JhLGvEZXLv/gBiDlRh9GLV7At5QSqF2iuKzlEv/q596iMUq3i5GpZojAU4yZGJA5\n" +
			"om2h7GaAw18kCXtkUAzuu6xTrEEwjA6nfbDW5b6j9Fwka2cEt6K5K3qrAYDCvf/H\n" +
			"F8SjNfECgYEA6oXCFoDguzBmu99BW8yL1LmRDVXnVX9NTXCTnQZWWSI7d/3aph+7\n" +
			"Srcrq8ST/RRrJNOGOeFFmEx4YkgrWHbI5FbKT5/AE61oTU4D22NcsnS9OzKrfLJU\n" +
			"pwN4EuTt2nDBQhQc7xyy2AXS/WMFmL2mw36JOirxfgyETWxUKza5SHUCgYEA0WN8\n" +
			"Gw96piEnXNvP11LvgYkHg/X4IELo3ZFF1BR9TTvI287kO4/1Yi1CxuxI5PmtE0sj\n" +
			"BkfNQdBSO/eJQOqiNttmxMwyCTQEKgyzT7xdBJB6Xo1dlHF4leIG8U8X2Rxu/Jw9\n" +
			"2BQAl2WlPaUYXHal+/i/AvkZmh4xTmCiTtAWvqsCgYA9E9DUY4QjvBtFWoN/EOpp\n" +
			"4FoyiujQj3LmdCc9geUDGbM7MNW85g61CrdI6M1R3/t3f885S74ASAoiLoF7bJQt\n" +
			"Q50013oCperAF01R2bLOlzChNjULKLZ7OKAQ66Y2eZ8bQnm2nFbHg9zLOPrAKMRB\n" +
			"rba3e2+hUEk0kL3q8zmV8QKBgGzEK8R2vvQrBqJywD0OpMD56ECA9Zn0h7gIbWhk\n" +
			"GE6VPpoHl4DlH8cW0bg5JwhiNahvAEuT4qhMSVtDEzX4a7jpMTILC/bB4WPiYwaJ\n" +
			"RxxG3HnfHvwsEUJyPb79odqyrWhk1K84fLIoJ7Sk3MmzDDeeF8K4mLb49g3LnqQn\n" +
			"7ZivAoGAGIVi+HPF1YnyzhoTKBXqEGil0LZgqRzGqGuiZfj2xV3fFLm5amZVFWgW\n" +
			"X+0qiRjaozDxSWdRABb6XzqPYTju/PYQBU+ULbC1CBikhW+gB7kuvtudFcXZRvhQ\n" +
			"91ibeQwjtGVbFWwBuoJ3yYNCiIxDiqurDOZNV/KbSIpz6Ax2P9Y=\n" +
			"-----END RSA PRIVATE KEY-----";
	}

	@Override
	public String clientCertData() {
		return clientCertificate;
	}

	@Override
	public String clientCaData() {
		return clientCaCertificate;
	}

	@Override
	public KeyManager[] mtlsKeymanagers() {
		try {
			return buildKeyManagers();
		} catch (CertificateException | KeyStoreException | IOException | NoSuchAlgorithmException |
				 UnrecoverableKeyException e) {
			throw new RuntimeException(e);
		}
	}

	private KeyManager[] buildKeyManagers() throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, UnrecoverableKeyException {

		byte[] certBytes = Base64.getDecoder().decode(sanitise(clientCertificate));

		X509Certificate cert = CertificateUtils.generateCertificateFromDER(certBytes);

		List<X509Certificate> chain = Lists.newArrayList(cert);
		if (clientCaCertificate != null) {
			chain.addAll( CertificateUtils.generateCertificateChainFromDER(clientCaCertificate.getBytes(StandardCharsets.UTF_8)));
		}

		KeyStore keystore = KeyStore.getInstance("KMS");
		keystore.load(null);
		keystore.setCertificateEntry(clientKey, cert);
		keystore.setKeyEntry(clientKey, null, chain.toArray(new Certificate[0]));

		if(!verify(cert.getPublicKey(), keystore.getKey(clientKey, null))) {
			throw new RuntimeException("The configured mtls certificate for YACS doesn't fit the configured KMS key");
		}

		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		keyManagerFactory.init(keystore, null);

		return keyManagerFactory.getKeyManagers();
	}

	private JsonObject buildSigningJwks(String signingKey) {

		try {
			String signingCertData = sanitise(signingCert);
			String alias = signingKey.startsWith("alias") ? signingKey : "alias/".concat(signingKey);
			Base64URL aliasUrl = Base64URL.encode(alias);
			byte[] certBytes = Base64.getDecoder().decode(signingCertData);
			X509Certificate cert = CertificateUtils.generateCertificateFromDER(certBytes);
			RSAKey jwk = (RSAKey) JWK.parse(cert);
			JsonObject jwkOb = new JsonObject();
			jwkOb.addProperty("kty", "RSA");
			jwkOb.addProperty("alg", "PS256");
			jwkOb.addProperty("use", "sig");
			jwkOb.addProperty("kid", calculateKid(signingCert));
			jwkOb.addProperty("a", alias);
			jwkOb.addProperty("d", aliasUrl.toString());
			jwkOb.addProperty("e", jwk.getPublicExponent().toString());
			jwkOb.addProperty("n", jwk.getModulus().toString());
			JsonObject jwkSet = new JsonObject();
			JsonArray keys = new JsonArray();
			keys.add(jwkOb);
			jwkSet.add("keys", keys);
			return jwkSet;
		} catch (CertificateException | NoSuchAlgorithmException | JOSEException e) {
			throw new RuntimeException(e);
		}
	}

	private String calculateKid(String signingCert) throws CertificateException, NoSuchAlgorithmException {
		signingCert = sanitise(signingCert);
		byte[] certBytes = Base64.getDecoder().decode(signingCert);
		X509Certificate cert = CertificateUtils.generateCertificateFromDER(certBytes);
		MessageDigest sha256 = MessageDigest.getInstance("SHA-256"); //Use a sha-256 MessageDigest instance
		byte[] x5tS256 = sha256.digest(cert.getEncoded()); //cert is X509Certificate - Hash the bytes of the entire certificate
		String encodedx5tS256 = Base64.getUrlEncoder().withoutPadding()
			.encodeToString(x5tS256);
		return encodedx5tS256;
	}

	private boolean verify(Key publicKey, Key privateKey) {
		RSAPublicKey rsaPublicKey = (RSAPublicKey) publicKey;
		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;
		BigInteger privateModulus = rsaPrivateKey.getModulus();
		BigInteger publicModulus = rsaPublicKey.getModulus();

		return privateModulus.equals(publicModulus);

	}

	@Override
	public JsonObject signingJwks() {
		return signingJwks;
	}

	@Override
	public void populateMtlsConfig(JsonObject mtls) {
		mtls.addProperty("cert", clientCertificate);
		mtls.addProperty("key", clientKeyData());
		mtls.addProperty("ca", clientCaCertificate);
		JsonObject mtlsAlias = new JsonObject();
		mtlsAlias.addProperty("key", clientKey);
		mtls.add("mtls_alias", mtlsAlias);
	}
}
