package net.openid.conformance.extensions.yacs.pcm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.extensions.yacs.dcr.DirectoryCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;

public class PcmClient {

	private static final Logger LOG = LoggerFactory.getLogger(PcmClient.class);
	private static final ObjectMapper MAPPER = new ObjectMapper()
		.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
		.setDateFormat(new StdDateFormat().withColonInTimeZone(true));

	private final RestTemplate restTemplate;
	private final DirectoryCredentials credentials;
	private final String tokenEndpoint;
	private final String pcmEndpoint;
	private PcmToken pcmToken;

	public PcmClient(DirectoryCredentials directoryCredentials, String pcmTokenEndpoint, String pcmReportEndpoint) {
		this.credentials = directoryCredentials;
		this.tokenEndpoint = pcmTokenEndpoint;
		this.pcmEndpoint = pcmReportEndpoint;
		HttpClient httpClient = null;
		try {
			httpClient = createHttpClient(credentials);
		} catch (Exception e) {
			LOG.error("Failed to create http client");
			throw new RuntimeException(e);
		}
		restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));
	}


	public boolean sendEvent(PcmEvent event) throws PcmCommsFailedException {
		List<PcmEvent> events = List.of(event);
		if(!ensureTokenReady()) {
			LOG.error("No PCM token available - not sending events");
			return false;
		}
		LOG.info("Sending {0} PCM events");
		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(pcmToken.getAccessToken());

		String body = null;
		try {
			body = MAPPER.writeValueAsString(events);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}

		HttpEntity<String> request = new HttpEntity<>(body, headers);

		try {
			LOG.info("Sending PCM event request - {}", body);
			ResponseEntity<String> response = restTemplate.exchange(pcmEndpoint, HttpMethod.POST, request, String.class);
			LOG.info("PCM event sent: {}", response.getStatusCode());

			if(!response.getStatusCode().equals(HttpStatus.OK)) {
				LOG.info("Response body - {}", response.getBody());
			}
		} catch (HttpClientErrorException e) {
			LOG.error(e.getResponseBodyAsString(), e);
			return false;
		}

		LOG.info("Sent");
		return true;
	}

	private boolean ensureTokenReady() throws PcmCommsFailedException {
		if(pcmToken == null) {
			pcmToken = getToken(tokenEndpoint, credentials.directoryClientId());
		}
		if(pcmToken == null) {
			throw new PcmCommsFailedException();
		}
		if(pcmToken.isExpired()) {
 			pcmToken = getToken(tokenEndpoint, credentials.directoryClientId());
		}
		return true;
	}

	public PcmToken getToken(String tokenEndpoint, String clientId, String... scopes) {
		LOG.info("Preparing to retrieve token for PCM");
		HttpHeaders headers = new HttpHeaders();

		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
		requestBody.add("grant_type", "client_credentials");
		requestBody.add("client_id", clientId);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestBody, headers);

		String jsonString = null;

		try {
			jsonString = restTemplate.postForObject(tokenEndpoint, request, String.class);
		} catch (Exception e) {
			LOG.error("Unable to obtain PCM token from token endpoint", e);
			return null;
		}

		LOG.info("Token for PCM retrieved");

		JsonObject token = JsonParser.parseString(jsonString).getAsJsonObject();
		return new PcmToken(token);
	}

	private HttpClient createHttpClient(DirectoryCredentials credentials)
		throws NoSuchAlgorithmException,
		KeyManagementException {

		HttpClientBuilder builder = HttpClientBuilder.create()
			.useSystemProperties();

		int timeout = 60;
		RequestConfig config = RequestConfig.custom()
			.setConnectTimeout(timeout * 1000)
			.setConnectionRequestTimeout(timeout * 1000)
			.setSocketTimeout(timeout * 1000).build();
		builder.setDefaultRequestConfig(config);

		KeyManager[] km = credentials.mtlsKeymanagers();

		TrustManager[] trustAllCerts = {
			new X509TrustManager() {

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}
			}
		};
		SSLContext sc = SSLContext.getInstance("TLS");
		sc.init(km, trustAllCerts, new SecureRandom());

		builder.setSSLContext(sc);

		String[] suites = {"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"};

		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sc,
			new String[] { "TLSv1.2" },
			suites,
			NoopHostnameVerifier.INSTANCE);

		builder.setSSLSocketFactory(sslConnectionSocketFactory);

		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
			.register("https", sslConnectionSocketFactory)
			.register("http", new PlainConnectionSocketFactory())
			.build();

		HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
		builder.setConnectionManager(ccm);

		builder.disableRedirectHandling();

		builder.disableAutomaticRetries();

		HttpClient httpClient = builder.build();
		return httpClient;
	}


	public static class PcmCommsFailedException extends Exception {

		public static final long serialVersionUID = 48322L;

	}
}
