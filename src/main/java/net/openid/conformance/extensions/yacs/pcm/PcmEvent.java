package net.openid.conformance.extensions.yacs.pcm;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import net.openid.conformance.logging.GsonObjectToBsonDocumentConverter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "pcmevents")
public class PcmEvent {

	@Id
	private String _id;

	private Date timestamp = new Date();
	private int statusCode;
	private String httpMethod;
	private String endpointUriPrefix;
	private String fapiInteractionId;
	private String endpoint;
	private String clientSSId;
	private String clientOrgId;
	private String serverOrgId;
	private long processTimespan;
	private String role;
	private org.bson.Document additionalInfo;

	public PcmEvent(int statusCode, String httpMethod, String endpointUriPrefix,
					String fapiInteractionId, String endpoint, String clientSSId,
					String clientOrgId, String serverOrgId, long processTimespan, String role,
					JsonObject additionalInfo) {
		this.timestamp = new Date();
		this.statusCode = statusCode;
		this.httpMethod = httpMethod;
		this.endpointUriPrefix = endpointUriPrefix;
		this.fapiInteractionId = fapiInteractionId == null ? "" : fapiInteractionId;
		this.endpoint = endpoint;
		this.clientSSId = clientSSId;
		this.clientOrgId = clientOrgId;
		this.serverOrgId = serverOrgId;
		this.processTimespan = processTimespan;
		this.role = role;
		this.additionalInfo = org.bson.Document.parse(new GsonBuilder().serializeNulls().create().toJson(
			GsonObjectToBsonDocumentConverter.convertFieldsToStructure(additionalInfo)));
	}

	public PcmEvent() {
	}


	String getId() {
		return _id;
	}
	public Date getTimestamp() {
		return timestamp;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public String getEndpointUriPrefix() {
		return endpointUriPrefix;
	}

	public String getFapiInteractionId() {
		return fapiInteractionId;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public String getClientSSId() {
		return clientSSId;
	}

	public String getClientOrgId() {
		return clientOrgId;
	}

	public long getProcessTimespan() {
		return processTimespan;
	}

	public String getServerOrgId() {
		return serverOrgId;
	}

	public String getRole() {
		return role;
	}

	public org.bson.Document getAdditionalInfo() {
		return additionalInfo;
	}

	@Override
	public String toString() {
		return "PcmEvent{" +
			"_id='" + _id + '\'' +
			", timestamp=" + timestamp +
			", statusCode=" + statusCode +
			", httpMethod='" + httpMethod + '\'' +
			", endpointUriPrefix='" + endpointUriPrefix + '\'' +
			", fapiInteractionId='" + fapiInteractionId + '\'' +
			", endpoint='" + endpoint + '\'' +
			", clientSSId='" + clientSSId + '\'' +
			", clientOrgId='" + clientOrgId + '\'' +
			", serverOrgId='" + serverOrgId + '\'' +
			", processTimespan=" + processTimespan +
			", role='" + role + '\'' +
			", additionalInfo=" + additionalInfo +
			'}';
	}
}
