package net.openid.conformance.extensions.yacs;

import net.openid.conformance.extensions.yacs.dcr.DirectoryCredentials;
import net.openid.conformance.extensions.yacs.dcr.DirectoryDetails;
import net.openid.conformance.extensions.yacs.dcr.KeyMaterialBearingDirectoryCredentials;
import net.openid.conformance.extensions.yacs.dcr.KmsDirectoryCredentials;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExternalConfigBean {

	private Map<String, Object> normalisedProperties = new HashMap<>();

	public void setCredentials(Map<String, String> credentials) {
		credentials.entrySet().stream()
			.forEach(e -> {
				normalisedProperties.put("credentials." + e.getKey(), e.getValue());
			});
	}

	public void setDirectory(Map<String, String> directory) {
		directory.entrySet().stream()
			.forEach(e -> {
				normalisedProperties.put("directory." + e.getKey(), e.getValue());
			});
	}

	public void setPlanrestrictions(Map<String, List<String>> planrestrictions) {
		planrestrictions.entrySet().stream()
			.forEach(e -> {
				normalisedProperties.put("planrestrictions." + e.getKey(), e.getValue());
			});
	}

	public DirectoryCredentials keyMaterialBearingCredentials() {
		return new KeyMaterialBearingDirectoryCredentials(normalisedProperties);
	}

	public DirectoryCredentials kmsCredentials() {
		return new KmsDirectoryCredentials(normalisedProperties);
	}

	public CertificationManagerPlans certificationManagerPlans() {
		return new CertificationManagerPlans(normalisedProperties);
	}

	public DirectoryDetails directoryDetails() {
		return new DirectoryDetails(normalisedProperties);
	}
}
