package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.KeyUse;
import net.openid.conformance.testmodule.OIDFJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Optional;


public class YACSIdTokenEncryptionServiceImpl implements YACSIdTokenEncryptionService {
	private static final Logger LOG = LoggerFactory.getLogger(YACSIdTokenEncryptionServiceImpl.class);

	private final JsonElement idTokenEncryptionPrivateKeyObject;


	public YACSIdTokenEncryptionServiceImpl(String keyPath) {

		FileSystemResource keyResource = new FileSystemResource(keyPath);

		if (!keyResource.isFile()) {
			throw new RuntimeException("id token encryption key file is not a file or does not exist");
		}

		try {
			byte[] keyBytes = Files.readAllBytes(Paths.get(keyResource.getURI()));
			String keyJson = new String(keyBytes, StandardCharsets.UTF_8);
			JWK idTokenEncryptionPrivateKey = JWK.parse(keyJson);

			if (!idTokenEncryptionPrivateKey.getKeyUse().equals(KeyUse.ENCRYPTION)) {
				throw new RuntimeException("Provided key is not for encryption");
			}

			if (!idTokenEncryptionPrivateKey.isPrivate()) {
				throw new RuntimeException("Provided key is not private");
			}

			this.idTokenEncryptionPrivateKeyObject = JsonParser.parseString(idTokenEncryptionPrivateKey.toJSONString());

			LOG.info("Loaded id token encryption private key");
		} catch (ParseException e) {
			throw new RuntimeException("Could not parse id token encryption private key", e);
		} catch (IOException e) {
			throw new RuntimeException("Could not read id token encryption key file", e);
		}
	}


	@Override
	public void addKeyToConfig(JsonObject config) {
		JsonObject client = getOrCreateJsonObjectFrom(config, "client");
		JsonObject jwks = getOrCreateJsonObjectFrom(client, "jwks");
		JsonObject orgJwks = getOrCreateJsonObjectFrom(client, "org_jwks");
		replaceKeysFor(jwks);
		replaceKeysFor(orgJwks);
	}

	private JsonObject getOrCreateJsonObjectFrom(JsonObject parent, String key) {
		return Optional.ofNullable(parent.getAsJsonObject(key)).orElseGet(() -> {
			JsonObject o = new JsonObject();
			parent.add(key, o);
			return o;
		});
	}

	private void replaceKeysFor(JsonObject parent){
		Optional.ofNullable(parent.getAsJsonArray("keys")).ifPresentOrElse(
			this::replaceEncryptionKeys,
			() -> {
				JsonArray k = new JsonArray();
				parent.add("keys", k);
				replaceEncryptionKeys(k);
			}
		);
	}

	private void replaceEncryptionKeys(JsonArray keysArray) {

		Iterator<JsonElement> iterator = keysArray.iterator();
		while (iterator.hasNext()) {
			JsonElement key = iterator.next();
			if (KeyUse.ENCRYPTION.getValue().equals(OIDFJSON.getString(key.getAsJsonObject().get("use")))) {
				iterator.remove();
			}
		}

		keysArray.add(this.idTokenEncryptionPrivateKeyObject);
	}


}
