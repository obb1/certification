package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import com.nimbusds.jose.jwk.JWK;

import javax.net.ssl.KeyManager;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;

public interface DirectoryCredentials {

	String orgId();
	String softwareStatementId();
	String directoryClientId();

	PrivateKey clientKey();
	X509Certificate clientCert();
	List<X509Certificate> clientCa();

	JWK signingKey();

	String clientKeyData();
	String clientCertData();
	String clientCaData();

	KeyManager[] mtlsKeymanagers();

	JsonObject signingJwks();

	void populateMtlsConfig(JsonObject mtls);

}
