package net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers;

import com.google.gson.JsonObject;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.http.HttpMethod;

import java.net.URI;
import java.util.List;

public class PostConsentsAuthoriseHandler extends BaseHandler {

	public PostConsentsAuthoriseHandler(ServerInfoTemplate serverInfoTemplate) {
		super(serverInfoTemplate);
	}

	@Override
	public List<String> getDecoratedEndpoints() {
		return List.of(
			"/open-banking/enrollments/v1/consents/{consentId}/authorise",
			"/open-banking/enrollments/v2/consents/{consentId}/authorise"
		);
	}

	@Override
	public HttpMethod getMethod() {
		return HttpMethod.POST;
	}

	@Override
	public void handle(JsonObject additionalInfo, HttpRequestWrapper request) {
		HttpUriRequest actualRequest = (HttpUriRequest) request.getOriginal();
		URI uri = actualRequest.getURI();
		String url = uri.toString();

		addEnrollmentIdFromRequestBody(additionalInfo, request);
		addPersonType(additionalInfo);
		addErrorCodes(additionalInfo);
		addClientIp(additionalInfo);
		addConsentId(additionalInfo, url);
	}
}
