package net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.ui.ServerInfoTemplate;
import net.openid.conformance.util.JWTUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

public abstract class BaseHandler {
	private static final Logger LOG = LoggerFactory.getLogger(BaseHandler.class);

	private JsonObject responseBody;
	private static final Pattern URN_PATTERN = Pattern.compile(
		"^urn:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,31}:[a-zA-Z0-9()+,\\-.:=@;$_!*'%/?#]+$"
	);
	private int statusCode;

	protected final ServerInfoTemplate serverInfoTemplate;

	/**
	 * Instantiates the handler and logs its type.
	 */
	public BaseHandler(ServerInfoTemplate serverInfoTemplate) {
		this.serverInfoTemplate = serverInfoTemplate;
		LOG.info("Instantiated handler: {}", getClass().getSimpleName());
	}

	public abstract void handle(JsonObject additionalInfo, HttpRequestWrapper requestBody);
	public abstract List<String> getDecoratedEndpoints();
	public abstract HttpMethod getMethod();

	/**
	 * Initializes the response body using the provided body string.
	 * The body string may be a JWT or a plain JSON document.
	 *
	 * @param statusCode the HTTP status code.
	 * @param bodyString the response body as a String.
	 */
	public void initResponse(int statusCode, String bodyString) {
		try {
			this.statusCode = statusCode;
			if (BodyExtractor.isJwt(bodyString)) {
				JsonObject decodedJwt = JWTUtil.jwtStringToJsonObjectForEnvironment(bodyString);
				this.responseBody = decodedJwt.getAsJsonObject("claims");
			} else {
				this.responseBody = new Gson().fromJson(bodyString, JsonObject.class);
			}
			LOG.info("Handler {}: Initialized response body: {}", getClass().getSimpleName(), responseBody);
		} catch (ParseException e) {
			LOG.error("Handler {}: Could not parse the response body: {}", getClass().getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Retrieves the "data" JSON object from the response body.
	 *
	 * @return Optional containing the "data" JSON document extracted from the response body.
	 */
	protected Optional<JsonObject> getDataFromResponseBody() {
		if (responseBody == null) {
			LOG.error("Handler {}: responseBody is null", getClass().getSimpleName());
			return Optional.empty();
		}
		JsonObject data = responseBody.getAsJsonObject("data");
		if (data == null) {
			LOG.error("Handler {}: responseBody.getAsJsonObject('data') is null", getClass().getSimpleName());
			return Optional.empty();
		}
		return Optional.of(data);
	}

	/**
	 * Adds enrollmentId to the additionalInfo by extracting it from the response body.
	 */
	protected void addEnrollmentId(JsonObject additionalInfo) {
		extractEnrollmentIdFromResponseBody().ifPresentOrElse(
			enrollmentId -> additionalInfo.addProperty("enrollmentId", enrollmentId),
			() -> LOG.info("Handler {}: Could not find enrollmentId in the response body {}", getClass().getSimpleName(), responseBody)
		);
	}

	/**
	 * Adds personType to the additionalInfo based on the presence of data.businessEntity in the response body.
	 */
	protected void addPersonType(JsonObject additionalInfo) {
		String personType = extractPersonTypeFromResponseBody();
		additionalInfo.addProperty("personType", personType);
	}

	/**
	 * Adds localInstrument to the additionalInfo by extracting it from the response body.
	 */
	protected void addLocalInstrument(JsonObject additionalInfo) {
		extractLocalInstrumentFromResponseBody().ifPresentOrElse(
			localInstrument -> additionalInfo.addProperty("localInstrument", localInstrument),
			() -> LOG.info("Handler {}: Could not find localInstrument in the response body {}", getClass().getSimpleName(), responseBody)
		);
	}
	/**
	 * Adds cancellationReason to the additionalInfo by extracting it from the response body.
	 */
	protected void addCancellationReasonFromResponseBody(JsonObject additionalInfo) {
		extractCancellationReasonFromResponseBody().ifPresentOrElse(
			cancellationReason -> additionalInfo.addProperty("cancellationReason", cancellationReason),
			() -> LOG.info("Handler {}: Could not find cancellationReason in the response body", getClass().getSimpleName())
		);
	}

	/**
	 * Adds cancellationReason to the additionalInfo by extracting it from the request body.
	 *
	 * @param additionalInfo The JsonObject to which the cancellationReason will be added.
	 * @param request The HttpRequestWrapper representing the request.
	 */
	protected void addCancellationReasonFromRequestBody(JsonObject additionalInfo, HttpRequestWrapper request) {
		extractCancellationReasonFromRequestBody(request).ifPresentOrElse(
			cancellationReason -> additionalInfo.addProperty("cancellationReason", cancellationReason),
			() -> LOG.info("Handler {}: Could not find cancellationReason in the request body", getClass().getSimpleName())
		);
	}

	/**
	 * Adds cancelledFrom to the additionalInfo by extracting it from the response body.
	 */
	protected void addCancelledFrom(JsonObject additionalInfo) {
		extractCancelledFromResponseBody().ifPresentOrElse(
			cancelledFrom -> additionalInfo.addProperty("cancelledFrom", cancelledFrom),
			() -> LOG.info("Handler {}: Could not find cancelledFrom in the response body {}", getClass().getSimpleName(), responseBody)
		);
	}

	/**
	 * Adds authorisationFlow to the additionalInfo by extracting it from the response body.
	 */
	protected void addAuthorisationFlow(JsonObject additionalInfo) {
		extractAuthorisationFlowFromResponseBody().ifPresentOrElse(
			authorisationFlow -> additionalInfo.addProperty("authorisationFlow", authorisationFlow),
			() -> LOG.info("Handler {}: Could not find authorisationFlow in the response body {}", getClass().getSimpleName(), responseBody)
		);
	}

	/**
	 * Adds reason to the additionalInfo by extracting it from the response body.
	 */
	protected void addReason(JsonObject additionalInfo) {
		extractReasonFromResponseBody().ifPresentOrElse(
			reason -> additionalInfo.addProperty("reason", reason),
			() -> LOG.info("Handler {}: Could not find reason in the response body {}", getClass().getSimpleName(), responseBody)
		);
	}

	/**
	 * Adds platform to the additionalInfo by extracting it from the request body.
	 *
	 * @param additionalInfo The JsonObject to which the platform will be added.
	 * @param request The HttpRequestWrapper representing the request.
	 */
	protected void addPlatform(JsonObject additionalInfo, HttpRequestWrapper request) {
		extractPlatformFromRequestBody(request).ifPresentOrElse(
			platform -> additionalInfo.addProperty("platform", platform),
			() -> LOG.info("Handler {}: Could not find platform in the request body", getClass().getSimpleName())
		);
	}

	/**
	 * Adds rp to the additionalInfo by extracting it from the request body.
	 *
	 * @param additionalInfo The JsonObject to which the rp will be added.
	 * @param request The HttpRequestWrapper representing the request.
	 */
	protected void addRp(JsonObject additionalInfo, HttpRequestWrapper request) {
		extractRpFromRequestBody(request).ifPresentOrElse(
			rp -> additionalInfo.addProperty("rp", rp),
			() -> LOG.info("Handler {}: Could not find rp in the request body", getClass().getSimpleName())
		);
	}

	/**
	 * Adds status to the additionalInfo by extracting it from the response body.
	 */
	protected void addStatus(JsonObject additionalInfo) {
		extractStatusFromResponseBody().ifPresentOrElse(
			status -> additionalInfo.addProperty("status", status),
			() -> LOG.info("Handler {}: Could not find status in the response body {}", getClass().getSimpleName(), responseBody)
		);
	}

	/**
	 * Adds consentId to the additionalInfo by extracting it from the URL.
	 *
	 * @param additionalInfo The JsonObject to which the consentId will be added.
	 * @param url The request URL.
	 */
	protected void addConsentId(JsonObject additionalInfo, String url) {
		extractConsentIdFromUri(url).ifPresentOrElse(
			consentId -> additionalInfo.addProperty("consentId", consentId),
			() -> LOG.info("Handler {}: Could not find consentId in the Url {}", getClass().getSimpleName(), url)
		);
	}

	/**
	 * Adds authenticatorAttachment to the additionalInfo by extracting it from the request body.
	 * If not present, adds it as an empty string.
	 *
	 * @param additionalInfo The JsonObject to which the authenticatorAttachment will be added.
	 * @param request The HttpRequestWrapper representing the request.
	 */
	protected void addAuthenticatorAttachment(JsonObject additionalInfo, HttpRequestWrapper request) {
		extractAuthenticatorAttachmentFromRequestBody(request).ifPresentOrElse(
			attachment -> additionalInfo.addProperty("authenticatorAttachment", attachment),
			() -> {
				LOG.info("Handler {}: authenticatorAttachment not found in the request body", getClass().getSimpleName());
				additionalInfo.addProperty("authenticatorAttachment", "");
			}
		);
	}

	/**
	 * Adds errorCodes to the additionalInfo if the response status code is 4XX or 5XX.
	 */
	protected void addErrorCodes(JsonObject additionalInfo) {
		if (statusCode >= 400) { // 4XX or 5XX
			List<String> errorCodes = extractErrorCodesFromResponseBody();
			if (!errorCodes.isEmpty()) {
				JsonArray errorCodesArray = new JsonArray();
				for (String code : errorCodes) {
					errorCodesArray.add(code);
				}
				additionalInfo.add("errorCodes", errorCodesArray);
			}
		}
	}

	/**
	 * Adds clientIp to the additionalInfo by extracting it from the server info.
	 */
	protected void addClientIp(JsonObject additionalInfo) {
		String clientIp = extractClientIp().orElse("");
		additionalInfo.addProperty("clientIp", clientIp);
	}

	/**
	 * Adds riskSignalsEnumList to the additionalInfo by extracting it from the request body.
	 *
	 * @param additionalInfo The JsonObject to which the riskSignalsEnumList will be added.
	 * @param maybeRequestBody Optional containing the request body as a JsonObject.
	 */
	protected void addRiskSignalsEnumList(JsonObject additionalInfo, Optional<JsonObject> maybeRequestBody) {
		if (maybeRequestBody.isEmpty()) {
			LOG.error("Handler {}: Request body is empty", getClass().getSimpleName());
			return;
		}
		JsonObject requestBodyJson = maybeRequestBody.get();
		if (requestBodyJson.isJsonNull()) {
			LOG.error("Handler {}: Request body is null", getClass().getSimpleName());
			return;
		}
		JsonObject dataObject = requestBodyJson.getAsJsonObject("data");
		if (dataObject != null) {
			Set<String> propertyNames = dataObject.keySet();
			JsonArray riskSignalsEnumList = new JsonArray();
			for (String propertyName : propertyNames) {
				riskSignalsEnumList.add(propertyName);
			}
			additionalInfo.add("riskSignalsEnumList", riskSignalsEnumList);
		} else {
			LOG.error("Handler {}: Could not find 'data' object in the request body", getClass().getSimpleName());
		}
	}

	/**
	 * Adds enrollmentId from the URI to the additionalInfo.
	 *
	 * @param additionalInfo The JsonObject to which the enrollmentId will be added.
	 * @param requestUri The request URI as a String.
	 */
	protected void addEnrollmentIdFromUri(JsonObject additionalInfo, String requestUri) {
		extractUrnIdFromUri(requestUri).ifPresentOrElse(
			enrollmentId -> additionalInfo.addProperty("enrollmentId", enrollmentId),
			() -> LOG.info("Handler {}: Could not find enrollmentId in the request URI {}", getClass().getSimpleName(), requestUri)
		);
	}

	/**
	 * Adds enrollmentId to the additionalInfo by extracting it from the request body.
	 *
	 * @param additionalInfo The JsonObject to which the enrollmentId will be added.
	 * @param request The HttpRequestWrapper representing the request.
	 */
	protected void addEnrollmentIdFromRequestBody(JsonObject additionalInfo, HttpRequestWrapper request) {
		extractEnrollmentIdFromRequestBody(request).ifPresentOrElse(
			enrollmentId -> additionalInfo.addProperty("enrollmentId", enrollmentId),
			() -> LOG.info("Handler {}: Could not find enrollmentId in the request body", getClass().getSimpleName())
		);
	}

	// --- Helper methods for extraction ---

	/**
	 * Extracts the consentId from the given request URI.
	 * It looks for a segment that matches the URN_PATTERN.
	 *
	 * @param requestUri The request URI as a String.
	 * @return An Optional containing the consentId if found; otherwise, empty.
	 */
	private Optional<String> extractConsentIdFromUri(String requestUri) {
		try {
			URI uri = new URI(requestUri);
			String path = uri.getPath();
			String[] segments = path.split("/");
			for (String segment : segments) {
				if (URN_PATTERN.matcher(segment).matches()) {
					return Optional.of(segment);
				}
			}
			return Optional.empty();
		} catch (URISyntaxException e) {
			LOG.error("Handler {}: Invalid URI for consent extraction: {}", getClass().getSimpleName(), requestUri);
			return Optional.empty();
		}
	}

	/**
	 * Extracts the client IP from the server info.
	 *
	 * @return An Optional containing the client IP if available; otherwise, empty.
	 */
	private Optional<String> extractClientIp() {
		String externalIp = serverInfoTemplate.getServerInfo().get("external_ip");
		if (externalIp != null) {
			return Optional.of(externalIp.trim());
		} else {
			LOG.error("Handler {}: External IP is not available in server info.", getClass().getSimpleName());
		}
		return Optional.empty();
	}

	/**
	 * Extracts the enrollmentId from the response body.
	 *
	 * @return An Optional containing the enrollmentId if found in the response body; otherwise, empty.
	 */
	private Optional<String> extractEnrollmentIdFromResponseBody() {
		return getDataFromResponseBody().flatMap(data -> Optional.ofNullable(data.get("enrollmentId")).map(OIDFJSON::getString));
	}

	/**
	 * Extracts the personType from the response body.
	 * Returns "PJ" if a businessEntity is present, otherwise "PF".
	 *
	 * @return The personType as a String.
	 */
	private String extractPersonTypeFromResponseBody() {
		return getDataFromResponseBody().map(data -> data.has("businessEntity") ? "PJ" : "PF").orElse("PF");
	}

	/**
	 * Extracts the localInstrument from the response body.
	 *
	 * @return An Optional containing the localInstrument if found in the response body; otherwise, empty.
	 */
	private Optional<String> extractLocalInstrumentFromResponseBody() {
		return getDataFromResponseBody().flatMap(data -> Optional.ofNullable(data.get("localInstrument")).map(OIDFJSON::getString));
	}

	/**
	 * Extracts the cancelledFrom from the response body.
	 *
	 * @return An Optional containing the cancelledFrom if found in the response body; otherwise, empty.
	 */
	private Optional<String> extractCancelledFromResponseBody() {
		return getDataFromResponseBody().flatMap(data -> Optional.ofNullable(data.get("cancelledFrom")).map(OIDFJSON::getString));
	}

	/**
	 * Extracts the authorisationFlow from the response body.
	 *
	 * @return An Optional containing the authorisationFlow if found in the response body; otherwise, empty.
	 */
	private Optional<String> extractAuthorisationFlowFromResponseBody() {
		return getDataFromResponseBody().flatMap(data -> Optional.ofNullable(data.get("authorisationFlow")).map(OIDFJSON::getString));
	}

	/**
	 * Extracts the reason from the response body.
	 *
	 * @return An Optional containing the reason if found in the response body; otherwise, empty.
	 */
	private Optional<String> extractReasonFromResponseBody() {
		return getDataFromResponseBody().flatMap(data -> {
			JsonObject cancelation = data.getAsJsonObject("cancelation");
			if (cancelation == null) {
				LOG.error("Handler {}: data.getAsJsonObject('cancelation') is null when extracting reason", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject reason = cancelation.getAsJsonObject("reason");
			if (reason == null) {
				LOG.error("Handler {}: cancelation.getAsJsonObject('reason') is null", getClass().getSimpleName());
				return Optional.empty();
			}
			return Optional.ofNullable(reason.get("rejectionReason"))
				.map(OIDFJSON::getString)
				.or(() -> Optional.ofNullable(reason.get("revocationReason"))
					.map(OIDFJSON::getString));
		});
	}

	/**
	 * Extracts the status from the response body.
	 *
	 * @return An Optional containing the status if found in the response body; otherwise, empty.
	 */
	private Optional<String> extractStatusFromResponseBody() {
		return getDataFromResponseBody().flatMap(data -> Optional.ofNullable(data.get("status")).map(OIDFJSON::getString));
	}

	/**
	 * Extracts the platform from the request body.
	 *
	 * @param request The HttpRequestWrapper representing the request.
	 * @return An Optional containing the platform if found in the request body; otherwise, empty.
	 */
	private Optional<String> extractPlatformFromRequestBody(HttpRequestWrapper request) {
		Optional<JsonObject> maybeRequestBody = extractRequestBody(request);
		if (maybeRequestBody.isPresent()) {
			JsonObject requestBody = maybeRequestBody.get();
			if (requestBody.isJsonNull()) {
				LOG.error("Handler {}: Request body is null", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject dataObject = requestBody.getAsJsonObject("data");
			if (dataObject == null) {
				LOG.error("Handler {}: 'data' object not found in request body", getClass().getSimpleName());
				return Optional.empty();
			}
			if (dataObject.has("platform") && !dataObject.get("platform").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(dataObject.get("platform")));
			} else {
				LOG.info("Handler {}: platform not found in data object", getClass().getSimpleName());
				return Optional.empty();
			}
		}
		return Optional.empty();
	}

	/**
	 * Extracts the rp from the request body.
	 *
	 * @param request The HttpRequestWrapper representing the request.
	 * @return An Optional containing the rp if found in the request body; otherwise, empty.
	 */
	private Optional<String> extractRpFromRequestBody(HttpRequestWrapper request) {
		Optional<JsonObject> maybeRequestBody = extractRequestBody(request);
		if (maybeRequestBody.isPresent()) {
			JsonObject requestBody = maybeRequestBody.get();
			if (requestBody.isJsonNull()) {
				LOG.error("Handler {}: Request body is null", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject dataObject = requestBody.getAsJsonObject("data");
			if (dataObject == null) {
				LOG.error("Handler {}: 'data' object not found in request body", getClass().getSimpleName());
				return Optional.empty();
			}
			if (dataObject.has("rp") && !dataObject.get("rp").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(dataObject.get("rp")));
			} else {
				LOG.info("Handler {}: rp not found in data object", getClass().getSimpleName());
				return Optional.empty();
			}
		}
		return Optional.empty();
	}

	/**
	 * Extracts the authenticatorAttachment from the request body.
	 *
	 * @param request The HttpRequestWrapper representing the request.
	 * @return An Optional containing the authenticatorAttachment if found in the request body; otherwise, empty.
	 */
	private Optional<String> extractAuthenticatorAttachmentFromRequestBody(HttpRequestWrapper request) {
		Optional<JsonObject> maybeRequestBody = extractRequestBody(request);
		if (maybeRequestBody.isPresent()) {
			JsonObject requestBody = maybeRequestBody.get();
			if (requestBody.isJsonNull()) {
				LOG.error("Handler {}: Request body is null", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject dataObject = requestBody.getAsJsonObject("data");
			if (dataObject == null) {
				LOG.error("Handler {}: 'data' object not found in request body", getClass().getSimpleName());
				return Optional.empty();
			}
			if (dataObject.has("authenticatorAttachment") && !dataObject.get("authenticatorAttachment").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(dataObject.get("authenticatorAttachment")));
			} else {
				LOG.info("Handler {}: authenticatorAttachment not present in data object", getClass().getSimpleName());
				return Optional.empty();
			}
		}
		return Optional.empty();
	}

	/**
	 * Extracts the enrollmentId from the request body.
	 *
	 * @param request The HttpRequestWrapper representing the request.
	 * @return An Optional containing the enrollmentId if found in the request body; otherwise, empty.
	 */
	private Optional<String> extractEnrollmentIdFromRequestBody(HttpRequestWrapper request) {
		Optional<JsonObject> maybeRequestBody = extractRequestBody(request);
		if (maybeRequestBody.isPresent()) {
			JsonObject requestBody = maybeRequestBody.get();
			if (requestBody.isJsonNull()) {
				LOG.error("Handler {}: Request body is null", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject dataObject = requestBody.getAsJsonObject("data");
			if (dataObject == null) {
				LOG.error("Handler {}: 'data' object not found in request body", getClass().getSimpleName());
				return Optional.empty();
			}
			if (dataObject.has("enrollmentId") && !dataObject.get("enrollmentId").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(dataObject.get("enrollmentId")));
			} else {
				LOG.info("Handler {}: enrollmentId not found in data object", getClass().getSimpleName());
				return Optional.empty();
			}
		}
		return Optional.empty();
	}

	/**
	 * Extracts the request body from the HTTP request.
	 *
	 * @param request the HTTP request wrapper.
	 * @return Optional containing the request body as a JsonObject, if present.
	 */
	protected Optional<JsonObject> extractRequestBody(HttpRequestWrapper request) {
		if (request instanceof HttpEntityEnclosingRequest) {
			HttpEntityEnclosingRequest entityRequest = (HttpEntityEnclosingRequest) request;
			HttpEntity entity = entityRequest.getEntity();
			if (entity != null) {
				try {
					String requestBodyString = EntityUtils.toString(entity, StandardCharsets.UTF_8);
					if (BodyExtractor.isJwt(requestBodyString)) {
						JsonObject decodedJwt = JWTUtil.jwtStringToJsonObjectForEnvironment(requestBodyString);
						return Optional.of(decodedJwt.getAsJsonObject("claims"));
					} else {
						return Optional.of(JsonParser.parseString(requestBodyString).getAsJsonObject());
					}
				} catch (IOException | ParseException e) {
					LOG.error("Handler {}: Error parsing request body: {}", getClass().getSimpleName(), e.getMessage(), e);
				}
			} else {
				LOG.warn("Handler {}: Request entity is null. Cannot extract the request body.", getClass().getSimpleName());
				return Optional.empty();
			}
		}
		return Optional.empty();
	}

	/**
	 * Extracts the error codes from the response body.
	 *
	 * @return An Optional list of error codes (as Strings) if present; otherwise, an empty list.
	 */
	private List<String> extractErrorCodesFromResponseBody() {
		List<String> errorCodes = new ArrayList<>();
		JsonArray errors = responseBody != null ? responseBody.getAsJsonArray("errors") : null;
		if (errors != null) {
			errors.forEach(error -> Optional.ofNullable(error.getAsJsonObject().get("code"))
				.map(OIDFJSON::getString)
				.ifPresent(errorCodes::add));
		}
		return errorCodes;
	}

	/**
	 * Extracts the URN (e.g., consentId) from the given request URI.
	 * It scans each path segment and returns the first segment that matches the URN_PATTERN.
	 *
	 * @param requestUri The request URI as a String.
	 * @return An Optional containing the URN if found; otherwise, empty.
	 */
	private Optional<String> extractUrnIdFromUri(String requestUri) {
		try {
			URI uri = new URI(requestUri);
			String path = uri.getPath();
			String[] segments = path.split("/");
			for (String segment : segments) {
				if (URN_PATTERN.matcher(segment).matches()) {
					return Optional.of(segment);
				}
			}
			return Optional.empty();
		} catch (URISyntaxException e) {
			LOG.error("Handler {}: Invalid URI for urn id extraction: {}", getClass().getSimpleName(), requestUri);
			return Optional.empty();
		}
	}

	/**
	 * Extracts the cancellation reason from the response body.
	 * @return An Optional containing the cancellation reason if found; otherwise, empty.
	 */
	private Optional<String> extractCancellationReasonFromResponseBody() {
		return getDataFromResponseBody().flatMap(data -> {
			JsonObject cancellation = data.getAsJsonObject("cancellation");
			if (cancellation == null) {
				LOG.info("Handler {}: 'cancellation' object not found in response body", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject reason = cancellation.getAsJsonObject("reason");
			if (reason == null) {
				LOG.info("Handler {}: 'reason' object not found in cancellation in response body", getClass().getSimpleName());
				return Optional.empty();
			}
			if (reason.has("rejectionReason") && !reason.get("rejectionReason").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(reason.get("rejectionReason")));
			} else if (reason.has("revocationReason") && !reason.get("revocationReason").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(reason.get("revocationReason")));
			} else {
				LOG.info("Handler {}: Neither rejectionReason nor revocationReason found in cancellation.reason", getClass().getSimpleName());
				return Optional.empty();
			}
		});
	}

	/**
	 * Extracts the cancellation reason from the request body.
	 * It looks inside the "cancellation" object within the "data" section of the request.
	 * The "reason" field is a oneOf type: it returns either "rejectionReason" or "revocationReason" if present.
	 * @param request The HttpRequestWrapper representing the request.
	 * @return An Optional containing the cancellation reason if found; otherwise, empty.
	 */
	private Optional<String> extractCancellationReasonFromRequestBody(HttpRequestWrapper request) {
		Optional<JsonObject> maybeRequestBody = extractRequestBody(request);
		if (maybeRequestBody.isPresent()) {
			JsonObject requestBody = maybeRequestBody.get();
			if (requestBody.isJsonNull()) {
				LOG.error("Handler {}: Request body is null", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject dataObject = requestBody.getAsJsonObject("data");
			if (dataObject == null) {
				LOG.error("Handler {}: 'data' object not found in request body", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject cancellation = dataObject.getAsJsonObject("cancellation");
			if (cancellation == null) {
				LOG.info("Handler {}: 'cancellation' object not found in request body", getClass().getSimpleName());
				return Optional.empty();
			}
			JsonObject reason = cancellation.getAsJsonObject("reason");
			if (reason == null) {
				LOG.info("Handler {}: 'reason' object not found in cancellation in request body", getClass().getSimpleName());
				return Optional.empty();
			}
			if (reason.has("rejectionReason") && !reason.get("rejectionReason").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(reason.get("rejectionReason")));
			} else if (reason.has("revocationReason") && !reason.get("revocationReason").isJsonNull()) {
				return Optional.of(OIDFJSON.getString(reason.get("revocationReason")));
			} else {
				LOG.info("Handler {}: Neither rejectionReason nor revocationReason found in cancellation.reason in request body", getClass().getSimpleName());
				return Optional.empty();
			}
		}
		return Optional.empty();
	}

}
