package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nimbusds.jose.util.JSONObjectUtils;
import com.nimbusds.jwt.JWT;
import net.openid.conformance.extensions.yacs.dcr.DcrException;
import net.openid.conformance.extensions.yacs.dcr.DirectoryDetails;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.DictHomologKeys;
import net.openid.conformance.security.AuthenticationFacade;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.JsonObjectBuilder;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

public class ConfigFilter extends AbstractYACSFilter {

	private ParticipantsService participantsService;
	private AuthenticationFacade authenticationFacade;

	private final DirectoryDetails directoryDetails;

	private final Logger logger = LoggerFactory.getLogger(ConfigFilter.class);
	private String participantsApiUri;

	private final String directoryRootsUri;
	public ConfigFilter(ParticipantsService participantsService, AuthenticationFacade authenticationFacade,
						DirectoryDetails directoryDetails, String directoryRootsUri, String participantsApiUri) {
		super(new MethodAndPathMatch("POST", "/api/plan"));
		this.participantsService = participantsService;
		this.authenticationFacade = authenticationFacade;
		this.directoryDetails = directoryDetails;
		this.directoryRootsUri = directoryRootsUri;
		this.participantsApiUri = participantsApiUri;
	}

	@Override
	public boolean processRequest(BodyRepeatableHttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		boolean committed = response.isCommitted();
		if(committed) {
			logger.warn("Response already committed - not proceeding");
			return false;
		}
		if(!authenticationFacade.isUser() && !authenticationFacade.isAdmin()) {
			logger.warn("Authenticated user does not have PVFPC role - not proceeding");
			response.sendError(HttpStatus.FORBIDDEN.value(), "You do not have permission to access this resource");
			return false;
		}
		JsonObject body = request.body();
		inferOrgId(body);
		if (planNameContains(request.getParameterValues("planName"),"Open Finance Brasil Functional Production Tests - FVP Phase 3 - Payments")
			|| planNameContains(request.getParameterValues("planName"),"fvp-development_test-plan")
			|| planNameContains(request.getParameterValues("planName"),"fvp-payments-e2e_test-plan-v3")
			|| planNameContains(request.getParameterValues("planName"),"fvp-payments-e2e_test-plan-v4")
			|| planNameContains(request.getParameterValues("planName"),"fvp-no_redirect_payments-test-plan-v1")
			|| planNameContains(request.getParameterValues("planName"),"fvp-homologation_test-plan")){
			logger.info("Payments test plan, inserting Brasil consent");
			insertBrasilPaymentConsent(body);
		}
		if (planNameContains(request.getParameterValues("planName"),"fvp-automatic-payments_test-plan-v1")){
			logger.info("Payments test plan, inserting Brasil consent");
			insertBrasilAutomaticPaymentConsent(body);
		}
		insertDirectoryObject(body);
		request.updateBody(body);
		return true;
	}

	private void inferOrgId(JsonObject config) {
		JsonObject serverConfig = config.getAsJsonObject("server");
		if(serverConfig == null) {
			return;
		}
		JsonElement authorisationServerIdElement = serverConfig.get("authorisationServerId");
		if (authorisationServerIdElement != null && !OIDFJSON.getString(authorisationServerIdElement).isEmpty()) {
			fillAliasAndDiscoveryUrl(config);
			return;
		}
		if(config.has("alias")) {
			insertOrgId(config, OIDFJSON.getString(config.get("alias")));
			return;
		}
		if(!serverConfig.has("discoveryUrl")){
			return;
		}
		String wellKnown = OIDFJSON.getString(serverConfig.get("discoveryUrl"));
		OIDCAuthenticationToken auth = (OIDCAuthenticationToken) authenticationFacade.getContextAuthentication();
		JWT idToken = auth.getIdToken();
		try {
			var trustFrameworkProfile = idToken.getJWTClaimsSet().getJSONObjectClaim("trust_framework_profile");
			if(trustFrameworkProfile == null) {
				return;
			}
			var orgAccessDetails = JSONObjectUtils.getJSONObject(trustFrameworkProfile, "org_access_details");
			if(orgAccessDetails == null) {
				return;
			}
			Set<String> orgIds = orgAccessDetails.keySet();
			Set<Map> orgs = participantsService.orgsFor(orgIds);
			for(Map org: orgs) {
				String orgId = String.valueOf(org.get("OrganisationId"));
				List<Map> authServersForOrg = (List<Map>) org.get("AuthorisationServers");
				for(Map authServer: authServersForOrg) {
					String oidcDiscovery = String.valueOf(authServer.get("OpenIDDiscoveryDocument"));
					if(oidcDiscovery.equals(wellKnown)) {
						config.addProperty("alias", orgId);
						insertOrgId(config, orgId);
						return;
					}
				}

			}

		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	private void insertOrgId(JsonObject config, String organisationId){
		if (config.keySet().contains("resource")){
			JsonObject resource = config.getAsJsonObject("resource");
			resource.addProperty("brazilOrganizationId",organisationId);
		} else {
			JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder();
			jsonObjectBuilder.addField("brazilOrganizationId",organisationId);

			JsonObject resource = jsonObjectBuilder.build();
			config.add("resource",resource);
		}
	}
	private void insertDirectoryObject(JsonObject config){
		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addField("participants",participantsApiUri)
			.addField("keystore", directoryDetails.getKeystoreUri().concat("/"))
			.addField("apibase", directoryDetails.getDirectoryUri().concat("/"))
			.addField("directoryRootsUri",directoryRootsUri)
			.addField("discoveryUrl",directoryDetails.getDiscoveryUri());

		JsonObject newConfigObject = jsonObjectBuilder.build();

		config.add("directory", newConfigObject);
	}

	private void insertBrasilPaymentConsent(JsonObject config){
		logger.info("Preparing to inject Brasil consent into config");
		JsonObject resource = config.getAsJsonObject("resource");
		String brasilCpf = getStringFromObject(resource,"brazilCpf");
		String businessEntityIdentification = getStringFromObject(resource,"brazilCnpj");
		LocalDate currentDate = LocalDate.now(ZoneId.of("America/Sao_Paulo"));

		Random random = new Random();
		float leftLimit = 0.01F;
		float rightLimit = 1F;
		double generatedFloat = leftLimit + random.nextFloat() * (rightLimit - leftLimit);

		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addFields("data.loggedUser.document", Map.of(
				"identification", String.valueOf(brasilCpf),
				"rel", "CPF")
			)
			.addFields("data.creditor",
				Map.of(
					"personType", DictHomologKeys.PROXY_PRODUCTION_PERSON_TYPE,
					"cpfCnpj", DictHomologKeys.PROXY_PRODUCTION_CPF,
					"name", DictHomologKeys.PROXY_PRODUCTION_NAME)
			)
			.addFields("data.payment", Map.of(
				"type", "PIX",
				"currency", DictHomologKeys.PROXY_EMAIL_STANDARD_CURRENCY,
				"amount", String.format("%.2f", generatedFloat),
				"date", currentDate.toString())
			)
			.addFields("data.payment.details", Map.of(
				"localInstrument", DictHomologKeys.PROXY_EMAIL_STANDARD_LOCALINSTRUMENT,
				"proxy", DictHomologKeys.PROXY_PRODUCTION_PROXY)
			)
			.addFields("data.payment.details.creditorAccount", Map.of(
				"ispb", DictHomologKeys.PROXY_PRODUCTION_ISPB,
				"issuer", DictHomologKeys.PROXY_PRODUCTION_BRANCH_NUMBER,
				"number", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_NUMBER,
				"accountType", DictHomologKeys.PROXY_PRODUCTION_ACCOUNT_TYPE)
			);

		if (businessEntityIdentification != null){
			jsonObjectBuilder
				.addFields("data.businessEntity.document", Map.of(
					"identification", businessEntityIdentification,
					"rel", "CNPJ"
				));
		}

		JsonObject consentRequest = jsonObjectBuilder.build();

		resource.add("brazilPaymentConsentFvp",consentRequest);

		config.add("resource",resource);
		logger.info("Brasil consent added to config");
	}

	private void insertBrasilAutomaticPaymentConsent(JsonObject config){
		logger.info("Preparing to inject Brasil consent into config");
		JsonArray creditors = new JsonArray();
		JsonObject resource = config.getAsJsonObject("resource");
		String brazilCpf = getStringFromObject(resource,"brazilCpf");
		String name = getStringFromObject(resource,"name");
		var businessEntityIdentification = Optional.ofNullable(getStringFromObject(resource,"brazilCnpj"));

		creditors.add(new JsonObjectBuilder()
			.addField("personType", businessEntityIdentification.isPresent() ? "PESSOA_JURIDICA" : "PESSOA_NATURAL")
			.addField("cpfCnpj", businessEntityIdentification.orElse(brazilCpf))
			.addField("name", name).build());

		JsonObjectBuilder jsonObjectBuilder = new JsonObjectBuilder()
			.addFields("data.loggedUser.document", Map.of(
				"identification",brazilCpf,
				"rel", "CPF")
			)
			.addField("data.creditors", creditors
			);

		businessEntityIdentification.ifPresent(businessId -> {
			jsonObjectBuilder.addFields("data.businessEntity.document", Map.of(
				"identification", businessId,
				"rel", "CNPJ"
			));
		});
		JsonObject consentRequest = jsonObjectBuilder.build();

		resource.add("brazilAutomaticPaymentConsentFvp",consentRequest);

		config.add("resource",resource);
		logger.info("Brasil consent added to config");
	}

	protected void fillAliasAndDiscoveryUrl(JsonObject config){
		String orgId;
		String authServerDiscoveryUrl;
		JsonObject serverConfig = config.getAsJsonObject("server");
		JsonElement authorisationServerIdElement = serverConfig.get("authorisationServerId");
		/**We know authorisationServerIdElement is not empty after OrgOwnershipEnhancedVerification*/

		String authorisationServerId = OIDFJSON.getString(authorisationServerIdElement);
		Map<String, Object> matchingServer = participantsService.authorisationServerForId(authorisationServerId)
			.orElseThrow(() -> new DcrException("Authorisation Server not found for ID: " + authorisationServerId));

		orgId = (String) matchingServer.get("OrganisationId");
		authServerDiscoveryUrl = (String) matchingServer.get("OpenIDDiscoveryDocument");

		config.addProperty("alias", orgId);
		JsonObject resource = getOrCreate("resource", config);
		resource.addProperty("brazilOrganizationId", orgId);

		serverConfig.addProperty("discoveryUrl", authServerDiscoveryUrl);

		logger.info("Found orgId '{}' and discovery URL '{}' in participants endpoint, inserted orgId into alias", orgId, authServerDiscoveryUrl);
	}

	private JsonObject getOrCreate(String name, JsonObject parent) {
		JsonObject child = parent.getAsJsonObject(name);
		if(child == null) {
			child = new JsonObject();
			parent.add(name, child);
		}
		return child;
	}

	private String getStringFromObject(JsonObject jsonObject, String key){
		if (jsonObject.keySet().contains(key) && jsonObject.get(key).isJsonPrimitive()){
			return OIDFJSON.getString(jsonObject.get(key));
		}
		return null;
	}


	private boolean planNameContains(String[] planNameParameters, String planName){
		for (String parameter: planNameParameters) {
			if (parameter.equals(planName)){
				return true;
			}
		}
		return false;
	}
}

