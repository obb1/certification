package net.openid.conformance.extensions.yacs;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("rawtypes")
@Service
public class CachedParticipantsService {
	private static final Logger LOG = LoggerFactory.getLogger(CachedParticipantsService.class);
	private final RestTemplate restTemplate = new RestTemplate();


	private final String participantsApiUri;

	public CachedParticipantsService(@Value("${fintechlabs.yacs.directory.uri}") String participantsApiUri) {
		this.participantsApiUri = participantsApiUri;
	}


	@Cacheable("participants")
	public Map<String, Map> participants() {
		LOG.info("Refreshing participants data");
		ResponseEntity<String> forEntity = restTemplate.getForEntity(participantsApiUri, String.class);
		String e = forEntity.getBody();
		Map[] all =  new Gson().fromJson(e, Map[].class);
		Map<String, Map> lookup = new HashMap<>();
		for(Map m: all) {
			String orgId = (String) m.get("OrganisationId");
			orgId = orgId.replaceAll("-", "_");
			lookup.put(orgId, m);
		}

		return lookup;
	}

	@CacheEvict(value = "participants", allEntries = true)
	@Scheduled(fixedRateString = "600000")
	public void emptyParticipantsCache() {
		LOG.debug("Emptying participants cache");

	}
}
