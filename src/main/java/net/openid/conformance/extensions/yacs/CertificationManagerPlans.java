package net.openid.conformance.extensions.yacs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CertificationManagerPlans {

	private static final String PLAN_PROPERTY = "planrestrictions.certmanager-only";
	private List<String> planNames = new ArrayList<>();

	public CertificationManagerPlans(Map<String, Object> properties) {
		parse(properties);
	}

	private void parse(Map<String, Object> properties) {
		planNames = (List<String>) properties.get(PLAN_PROPERTY);
	}


	public List<String> getPlans() {
		return planNames;
	}
}
