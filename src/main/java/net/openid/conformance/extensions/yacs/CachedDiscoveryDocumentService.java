package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

public class CachedDiscoveryDocumentService {

	private static final Logger LOG = LoggerFactory.getLogger(CachedDiscoveryDocumentService.class);
	private final RestTemplate restTemplate;

	public CachedDiscoveryDocumentService() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		this.restTemplate = createRestTemplate();

	}

	@Cacheable("discoveryDocument")
	public JsonObject getDiscoveryDocument(String directoryDiscoveryUri, boolean mtlsAliases) {
		LOG.info("Fetching discovery document from {}", directoryDiscoveryUri);

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(MediaType.parseMediaTypes("application/json"));
		HttpEntity<String> request = new HttpEntity<>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(directoryDiscoveryUri, HttpMethod.GET, request, String.class);

		if (!response.getStatusCode().is2xxSuccessful()) {
			throw new RuntimeException("Failed to retrieve discovery document from " + directoryDiscoveryUri
				+ ". Status: " + response.getStatusCode());
		}

		JsonObject discoveryDocument = JsonParser.parseString(response.getBody()).getAsJsonObject();

		if (mtlsAliases && discoveryDocument.has("mtls_endpoint_aliases")) {
			discoveryDocument = discoveryDocument.getAsJsonObject("mtls_endpoint_aliases");
		}

		return discoveryDocument;
	}

	@CacheEvict(value = "discoveryDocument", allEntries = true)
	@Scheduled(fixedRateString = "600000")
	public void evictDiscoveryDocumentCache() {
		LOG.debug("Evicting cached discovery document.");
	}

	private final RestTemplate createRestTemplate()
		throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
			.loadTrustMaterial(null, acceptingTrustStrategy)
			.build();

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		CloseableHttpClient httpClient = HttpClients.custom()
			.setSSLSocketFactory(csf)
			.build();

		HttpComponentsClientHttpRequestFactory requestFactory =
			new HttpComponentsClientHttpRequestFactory();

		requestFactory.setHttpClient(httpClient);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse response) throws IOException {
				return false;
			}
		});
		return restTemplate;
	}

}
