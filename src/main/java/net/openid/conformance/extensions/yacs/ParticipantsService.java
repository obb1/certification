package net.openid.conformance.extensions.yacs;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface ParticipantsService {
	Set<Map> orgsFor(Set<String> orgIds);

	Optional<Map<String, Object>> authorisationServerForId(String authorisationServerId);
}
