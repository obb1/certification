package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;

public class PrivateKeyJWTAuthProcessor implements DcrRequestProcessor {
	@Override
	public void process(JsonObject dcrRequest) {
		dcrRequest.addProperty("token_endpoint_auth_method", "private_key_jwt");
	}
}
