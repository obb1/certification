package net.openid.conformance.extensions.yacs;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ChainedEnhancedVerification implements EnhancedVerification {
	private final List<EnhancedVerification> chain;

	public ChainedEnhancedVerification(List<EnhancedVerification> chain) {
		this.chain = chain;
	}

	@Override
	public VerificationResult allowed(BodyRepeatableHttpServletRequest request) {
		List<VerificationResult> results = chain.stream()
				.map(e -> e.allowed(request))
				.collect(Collectors.toList());
		Optional<VerificationResult> hardFail = results.stream()
				.filter(r -> r.getDecision() == VerificationResult.Decision.HARD_FAIL).findFirst();
		if(hardFail.isPresent()) {
			return hardFail.get();
		}
		Optional<VerificationResult> hardPass = results.stream()
				.filter(r -> r.getDecision() == VerificationResult.Decision.HARD_PASS).findFirst();
		if(hardPass.isPresent()) {
			return hardPass.get();
		}
		Optional<VerificationResult> softFail = results.stream()
				.filter(r -> r.getDecision() == VerificationResult.Decision.SOFT_FAIL).findFirst();
		return softFail.orElse(VerificationResult.passed());
	}
}
