package net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers;

import com.google.gson.JsonObject;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.springframework.http.HttpMethod;

import java.util.List;


public class PostFidoRegistrationOptionsHandler extends BaseHandler {

	public PostFidoRegistrationOptionsHandler(ServerInfoTemplate serverInfoTemplate) {
		super(serverInfoTemplate);
	}


	@Override
	public List<String> getDecoratedEndpoints() {
		return List.of(
			"/open-banking/enrollments/v1/enrollments/{enrollmentId}/fido-registration-options",
			"/open-banking/enrollments/v2/enrollments/{enrollmentId}/fido-registration-options"
		);
	}

	@Override
	public HttpMethod getMethod() {
		return HttpMethod.POST;
	}

	@Override
	public void handle(JsonObject additionalInfo, HttpRequestWrapper request) {
		addEnrollmentId(additionalInfo);
		addRp(additionalInfo, request);
		addPlatform(additionalInfo,request);
		addErrorCodes(additionalInfo);
		addClientIp(additionalInfo);
	}
}
