package net.openid.conformance.extensions.yacs.pcm;

import net.openid.conformance.extensions.yacs.EndpointDecorationService;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.HttpResponseInterceptor;

public class EmittingPcmStrategy implements PcmInterceptorFactory.PcmStrategy {

	private final PcmRepository repository;
	private final ConsentTracker consentTracker;
	private final EndpointDecorationService endpointDecorationService;
	private final ServerInfoTemplate serverInfoTemplate;


	public EmittingPcmStrategy(PcmRepository repository, ConsentTracker consentTracker, EndpointDecorationService endpointDecorationService,ServerInfoTemplate serverInfoTemplate) {
		this.repository = repository;
		this.consentTracker = consentTracker;
		this.endpointDecorationService = endpointDecorationService;
		this.serverInfoTemplate = serverInfoTemplate;
	}

	@Override
	public HttpResponseInterceptor interceptorFor(Environment env, String testId) {
		return new PcmEventEmitter(repository, endpointDecorationService, consentTracker, env, testId,serverInfoTemplate);
	}

}
