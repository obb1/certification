package net.openid.conformance.extensions.yacs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings({"rawtypes"})
public class ParticipantsServiceImpl implements ParticipantsService {

	private static final Logger LOG = LoggerFactory.getLogger(ParticipantsServiceImpl.class);

	private final CachedParticipantsService cachedParticipantsService;

	public ParticipantsServiceImpl(CachedParticipantsService cachedParticipantsService) {
		this.cachedParticipantsService = cachedParticipantsService;
	}

	@Override
	public Set<Map> orgsFor(Set<String> orgIds) {
		LOG.info("Looking up orgs for {}", orgIds);
		Map<String, Map> allOrgs = cachedParticipantsService.participants();
		return allOrgs.entrySet()
			.stream()
			.filter(kv -> orgIds.contains(kv.getKey()))
			.map(Map.Entry::getValue)
			.collect(Collectors.toSet());
	}

	@Override
	public Optional<Map<String, Object>> authorisationServerForId(String authorisationServerId) {
		LOG.info("Looking up authorisation server for ID {}", authorisationServerId);
		Map<String, Map> allOrgs = cachedParticipantsService.participants();
		return allOrgs.values()
			.stream()
			.map(org -> org.get("AuthorisationServers"))
			.filter(authorisationServers -> authorisationServers instanceof List)
			.flatMap(authorisationServers -> ((List<?>) authorisationServers).stream())
			.filter(server -> server instanceof Map)
			.map(server -> (Map<String, Object>) server)
			.filter(server -> authorisationServerId.equals(server.get("AuthorisationServerId")))
			.findFirst();
	}
}

