package net.openid.conformance.extensions.yacs.dcr;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static net.openid.conformance.extensions.yacs.dcr.CertificateUtils.sanitise;

public class KeyMaterialBearingDirectoryCredentials implements DirectoryCredentials {

	private static final String CLIENT_CERT_PROP = "credentials.client-certificate";
	private static final String CLIENT_KEY_PROP = "credentials.client-key";
	private static final String CLIENT_CA_PROP = "credentials.client-ca";
	private static final String SIGNING_KEY_PROP = "credentials.signing-key";
	private static final String SIGNING_CERT_PROP = "credentials.signing-cert";
	private static final String ORG_ID_PROP = "credentials.org-id";
	private static final String SS_ID_PROP = "credentials.software-statement-id";
	private static final String DIRECTORY_CLIENT_ID_PROP = "credentials.directory-client-id";

	private String directoryClientId;
	private String orgId;
	private String softwareStatementId;

	private String clientKey;
	private String clientCertificate;
	private String clientCaCertificate;
	private String signingKey;
	private String signingCert;
	private JsonObject signingJwks;


	public KeyMaterialBearingDirectoryCredentials(Map<String, Object> properties) {
		parse(properties);
		signingJwks = buildSigningJwksObject(signingKey);
	}

	private void parse(Map<String, Object> properties) {
		clientCertificate = findIfPresent(CLIENT_CERT_PROP, properties, true);
		clientKey = findIfPresent(CLIENT_KEY_PROP, properties, true);
		clientCaCertificate = findIfPresent(CLIENT_CA_PROP, properties, true);
		signingKey = findIfPresent(SIGNING_KEY_PROP, properties, true);
		signingCert = findIfPresent(SIGNING_CERT_PROP, properties, true);
		orgId = findIfPresent(ORG_ID_PROP, properties);
		softwareStatementId = findIfPresent(SS_ID_PROP, properties);
		directoryClientId = findIfPresent(DIRECTORY_CLIENT_ID_PROP, properties);
	}

	private JsonObject buildSigningJwksObject(String signingKey) {
		try {
			JWK jwk = JWK.parseFromPEMEncodedObjects(signingKey);
			JsonObject jwkAsJson = JsonParser.parseString(jwk.toJSONString()).getAsJsonObject();
			jwkAsJson.addProperty("alg", "PS256");
			jwkAsJson.addProperty("use", "sig");
			if(signingCert != null) {
				jwkAsJson.addProperty("kid", calculateKid(signingCert));
			}
			JsonObject jwkSet = new JsonObject();
			JsonArray keys = new JsonArray();
			keys.add(jwkAsJson);
			jwkSet.add("keys", keys);
			return jwkSet;
		} catch (JOSEException | CertificateException | NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private String calculateKid(String signingCert) throws CertificateException, NoSuchAlgorithmException {
		signingCert = sanitise(signingCert);
		byte[] certBytes = Base64.getDecoder().decode(signingCert);
		X509Certificate cert = CertificateUtils.generateCertificateFromDER(certBytes);
		MessageDigest sha256 = MessageDigest.getInstance("SHA-256"); //Use a sha-256 MessageDigest instance
		byte[] x5tS256 = sha256.digest(cert.getEncoded()); //cert is X509Certificate - Hash the bytes of the entire certificate
		String encodedx5tS256 = Base64.getUrlEncoder().withoutPadding()
			.encodeToString(x5tS256);
		return encodedx5tS256;
	}

	private String findIfPresent(String key, Map<String, Object> properties) {
		return findIfPresent(key, properties, false);
	}

	private String findIfPresent(String key, Map<String, Object> properties, boolean decode) {
		Base64.Decoder decoder = Base64.getDecoder();
		String raw =  Optional.ofNullable(properties.get(key))
			.orElseThrow(() -> new RuntimeException(String.format("Property %s not found", key)))
			.toString();
		return decode ? new String(decoder.decode(raw)) : raw;
	}

	@Override
	public String orgId() {
		return orgId;
	}

	@Override
	public String softwareStatementId() {
		return softwareStatementId;
	}

	@Override
	public String directoryClientId() {
		return directoryClientId;
	}

	@Override
	public PrivateKey clientKey() {
		String keyData = sanitise(clientKey);
		byte[] keyBytes = Base64.getDecoder().decode(keyData);
		try {
			return CertificateUtils.generatePrivateKeyFromDER(keyBytes);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public X509Certificate clientCert() {
		String certData = sanitise(clientCertificate);
		byte[] certBytes = Base64.getDecoder().decode(certData);
		try {
			return CertificateUtils.generateCertificateFromDER(certBytes);
		} catch (CertificateException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<X509Certificate> clientCa() {
		List<X509Certificate> chain = Lists.newArrayList(clientCert());
		String caData = clientCaCertificate;
		if(caData == null) {
			return chain;
		}
		caData = sanitise(caData);
		byte[] caBytes = Base64.getDecoder().decode(caData);
		try {
			chain.addAll(CertificateUtils.generateCertificateChainFromDER(caBytes));
		} catch (CertificateException e) {
			throw new RuntimeException(e);
		}
		return chain;
	}

	@Override
	public JWK signingKey() {
		return null;
	}

	@Override
	public String clientKeyData() {
		return clientKey;
	}

	@Override
	public String clientCertData() {
		return clientCertificate;
	}

	@Override
	public String clientCaData() {
		return clientCaCertificate;
	}

	@Override
	public KeyManager[] mtlsKeymanagers() {
		X509Certificate cert = clientCert();
		RSAPrivateKey key = (RSAPrivateKey) clientKey();

		List<X509Certificate> chain = clientCa();

		KeyStore keystore = null;
		try {
			keystore = KeyStore.getInstance("JKS");
			keystore.load(null);
			keystore.setCertificateEntry("cert-alias", cert);
			keystore.setKeyEntry("key-alias", key, "changeit".toCharArray(), chain.toArray(new Certificate[chain.size()]));

			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keystore, "changeit".toCharArray());

			return keyManagerFactory.getKeyManagers();
		} catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException |
				 UnrecoverableKeyException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public JsonObject signingJwks() {
		return signingJwks;
	}

	@Override
	public void populateMtlsConfig(JsonObject mtls) {
		mtls.addProperty("cert", clientCertificate);
		mtls.addProperty("key", clientKey);
		mtls.addProperty("ca", clientCaCertificate);
	}
}
