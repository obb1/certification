package net.openid.conformance.extensions.yacs.dcr;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import net.openid.conformance.extensions.yacs.YACSIdTokenEncryptionService;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.testmodule.TestModule;
import net.openid.conformance.util.JsonObjectBuilder;
import net.openid.conformance.variant.ClientAuthType;
import net.openid.conformance.variant.VariantSelection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static net.openid.conformance.testmodule.TestModule.Status.FINISHED;
import static net.openid.conformance.testmodule.TestModule.Status.INTERRUPTED;

public class DcrPostHocTestInfoService implements TestInfoService {

	private final DirectoryCredentials directoryCredentials;
	private final YACSIdTokenEncryptionService yacsIdTokenEncryptionService;
	@Value("${fintechlabs.base_url}")
	private String baseUrl;

	private final TestInfoService delegate;
	private final DcrClientRepository clientRepository;
	private final DcrHelper dcrHelper;
	private final Logger logger = LoggerFactory.getLogger(DcrPostHocTestInfoService.class);

	public DcrPostHocTestInfoService(TestInfoService delegate,
									 DcrClientRepository clientRepository,
									 DcrHelper dcrHelper,
									 DirectoryCredentials directoryCredentials,
									 YACSIdTokenEncryptionService yacsIdTokenEncryptionService) {
		this.delegate = delegate;
		this.dcrHelper = dcrHelper;
		this.clientRepository = clientRepository;
		this.directoryCredentials = directoryCredentials;
		this.yacsIdTokenEncryptionService = yacsIdTokenEncryptionService;
	}

	private static final List<String> TESTS_EXCLUDED_FROM_DCR = ImmutableList.of(
		"consents_api_bad-logged_test-module_v2",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_happy-flow-1_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_happy-flow-2_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_client-delete_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_invalid-registration-accesstoken_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_invalid-ss-signature_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_no-software-statement_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_no-mtls_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_bad-mtls_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_client-config_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_client_config-bad-jwks-uri_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_client-config-invalid-jwks-value_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_client-config-invalid-redirect-uri_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_no-redirect-uri_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_invalid-redirect-uri_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_invalid-jwks-uri_test-module",
		"dcr_api_fvp-fapi1-advanced-final-brazil-dcr-no-authorization-flow_invalid-jwks-value_test-module",
		"dcr_api_fvp-happy-path_test_module",
		"dcr_api_fvp-sandbox-credentials_test-module",
		"dcr_api_fvp-no-subject-type_test-module",
		"dcr_api_fvp-multiple-clients_test-module",
		"fvp-payments-consents-server-certificate-v2",
		"directory_api_server-registration_test-module",
		"directory_api_server-registration_test_module_v2",
		"fvp-preflight-cert-check-payments-test-v3",
		"dcr_api_fvp-unhappy-tls-client-auth_test_module",
		"directory_api_certification-status_test-module",
		"fvp-consents_api_bad-consents_test-module_v3-2",
		"fvp-consents_api_extension-invalid-status_test-module_v3-2",
		"fvp-consents_api_negative_test-module_v3-2",
		"fvp-consents_api_permission-groups_test-module_v3-2",
		"fvp_consents_api_bad-logged_test-module_v3-2",
		"fvp-automatic-payments_api_negative-consents_test-module_v1",
		"fvp-automatic-payments_api_rejected-consent_test-module_v1",
		"fvp-automatic-payments_api_sweeping-accounts-invalid-creditor_test-module_v1",
		"fvp-payments_api_consents_negative_test-module_v4",
		"fvp-payments_api_dict_test-module_v4",
		"fvp-payments_api_force-check-signature_test-module_v4",
		"fvp-payments_api_json-accept-header-jwt-returned_test-module_v4",
		"fvp-payments_api_manu-fail_test-module_v4",
		"fvp-payments_api_pixscheduling-dates-unhappy_test-module_v4",
		"fvp-payments_api_qres-code-enforcement_test-module_v4",
		"fvp-payments_api_recurring-payments-consent-limit_test-module_v4",
		"fvp-payments_api_recurring-payments-wrong-custom-quantity_test-module_v4",
		"fvp_automatic_payments_consents_api_bad-logged_test-module_v1",
		"fvp_payments_consents_api_bad-logged_test-module_v4"
	);

	@Override
	public void createTest(String id, String testName, VariantSelection variant,
						   VariantSelection variantFromPlanDefinition, String url,
						   JsonObject config, String alias, Instant started,
						   String testPlanId, String description, String summary, String publish) {
		try {
			injectDcrClient(id, variant, config, testName);
		} catch(DcrException d) {
			throw d;
		}
		catch(RuntimeException t) {
			logger.error("Failed to inject DCR client", t);

			DcrException.UserReport report = new DcrException.UserReport();
			report.setEndpoint("Unknown (Runtime Exception)");
			report.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);

			Map<String, Object> exceptionDetails = Map.of(
				"exceptionType", t.getClass().getName(),
				"message", t.getMessage(),
				"cause", t.getCause().toString()
			);

			report.setResponseBody(exceptionDetails);
			report.setRequestBody(Map.of("context", "Error occurred while injecting DCR client"));

			report.setResponseHeaders(new HttpHeaders());
			report.setRequestHeaders(new HttpHeaders());

			throw new DcrException(report);
		}
		JsonObjectBuilder newConfig = new JsonObjectBuilder();
		//config not needed for info, no need to store the values of it
		newConfig.addField("restOfConfig", "obscured for security reasons");
		if (config.getAsJsonObject("server") != null){
			newConfig.addField("server.discoveryUrl", OIDFJSON.getString(config.getAsJsonObject("server").get("discoveryUrl")));
		}
		if(config.getAsJsonObject("resource") != null && config.getAsJsonObject("resource").get("brazilCnpj") != null) {
			String brasilCnPj = OIDFJSON.getString(config.getAsJsonObject("resource").get("brazilCnpj"));
			newConfig.addField("resource.brazilCnpj", brasilCnPj);
		}
		config = newConfig.build();
		delegate.createTest(id, testName, variant, variantFromPlanDefinition, url, config, alias, started, testPlanId, description, summary, publish);
	}

	@Override
	public void updateTestResult(String id, TestModule.Result result) {
		delegate.updateTestResult(id, result);
	}

	@Override
	public void updateTestStatus(String id, TestModule.Status status) {
		if(status.equals(FINISHED) || status.equals(INTERRUPTED)) {
			ClientHolder savedClient = clientRepository.getSavedClient(id);
			if (savedClient == null) {
				logger.info("No saved client found for test {}", id);
			} else {
				try {
					dcrHelper.unregisterClient(savedClient, id);
					clientRepository.deleteSavedClient(id);
				} catch(ClientDeletionException e) {
					throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		}
		delegate.updateTestStatus(id, status);
	}

	@Override
	public ImmutableMap<String, String> getTestOwner(String id) {
		return delegate.getTestOwner(id);
	}

	@Override
	public boolean publishTest(String id, String publish) {
		return delegate.publishTest(id, publish);
	}

	@Override
	public void createIndexes() {
		delegate.createIndexes();
	}

	@Override
	public boolean deleteTests(List<String> id) {
		return delegate.deleteTests(id);
	}

	private void injectDcrClient(String id, VariantSelection variant, JsonObject config, String testName) {
		String orgId = OIDFJSON.getString(config.get("alias"));
		String authServerDiscoveryUrl = OIDFJSON.getString(config.get("server").getAsJsonObject().get("discoveryUrl"));
		JsonObject resource = getOrCreate("resource", config);
		resource.addProperty("brazilOrganizationId", orgId);
		if (!TESTS_EXCLUDED_FROM_DCR.contains(testName)) {
			logger.info("Injecting DCR client for test {}", id);
			String redirectUri = String.format("%s/test/a/%s/callback", baseUrl, orgId);
			Map<String, String> variantDetails = variant.getVariant();
			String clientAuth = variantDetails.get("client_auth_type");
			String webhookUri = null;
			if (dcrRequiresWebhook(testName)) {
				webhookUri = String.format("%s/test-mtls/fvp/w", baseUrl);
			}
			ClientAuthType clientAuthType;
			switch (clientAuth) {
				case "private_key_jwt":
					clientAuthType = ClientAuthType.PRIVATE_KEY_JWT;
					break;
				default:
					clientAuthType = ClientAuthType.MTLS;
					break;
			}
			JsonObject dcrResult = dcrHelper.registerClient(authServerDiscoveryUrl, clientAuthType, redirectUri, webhookUri, orgId);
			JsonObject dynamicClient = dcrResult.getAsJsonObject("client");
			String clientId = OIDFJSON.getString(dynamicClient.get("client_id"));
			JsonObject credentials = dcrResult.getAsJsonObject("credentials");
			JsonObject clientConfig = getOrCreate("client", config);
			JsonObject directoryConfig = getOrCreate("directory", config);
			clientConfig.addProperty("client_id", clientId);
			clientConfig.add("jwks", credentials.get("jwks").getAsJsonObject());
			clientConfig.add("org_jwks", credentials.get("jwks").getAsJsonObject());
			JsonObject mtls = getOrCreate("mtls", config);
			directoryCredentials.populateMtlsConfig(mtls);
			directoryConfig.addProperty("client_id", OIDFJSON.getString(credentials.get("directoryClientId")));
			config.addProperty("dcr_client_id", clientId);
			clientRepository.saveClient(id, dcrResult);
			logger.info("Injected DCR client for test {}", id);
		} else {
			logger.info("DCR client not required for test {}, injecting directory credentials", id);
			JsonObject mtls = getOrCreate("mtls", config);
			directoryCredentials.populateMtlsConfig(mtls);
			JsonObject clientConfig = getOrCreate("client", config);
			clientConfig.add("jwks",directoryCredentials.signingJwks());
			clientConfig.add("org_jwks",directoryCredentials.signingJwks());
			JsonObject directoryConfig = getOrCreate("directory", config);
			directoryConfig.addProperty("client_id", directoryCredentials.directoryClientId());
		}

		yacsIdTokenEncryptionService.addKeyToConfig(config);

	}
	private boolean dcrRequiresWebhook(String testName){
		List<String> requiredTests = Arrays.asList("payments_api_webhook-consents-rejected_test-module");

		return requiredTests.contains(testName);
	}
	private JsonObject getOrCreate(String name, JsonObject parent) {
		JsonObject child = parent.getAsJsonObject(name);
		if(child == null) {
			child = new JsonObject();
			parent.add(name, child);
		}
		return child;
	}

}
