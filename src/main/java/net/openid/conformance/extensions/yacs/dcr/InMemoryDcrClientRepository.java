package net.openid.conformance.extensions.yacs.dcr;

import com.google.gson.JsonObject;
import net.openid.conformance.testmodule.OIDFJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryDcrClientRepository implements DcrClientRepository {

	private static final Logger LOG = LoggerFactory.getLogger(InMemoryDcrClientRepository.class);

	private Map<String, ClientHolder> clients = new HashMap<>();

	@Override
	public void saveClient(String testId, JsonObject clientConfig) {
		JsonObject dynamicClient = clientConfig.getAsJsonObject("client");
		String clientId = OIDFJSON.getString(dynamicClient.get("client_id"));
		String uri = OIDFJSON.getString(dynamicClient.get("registration_client_uri"));
		LOG.info("Adding DCR-obtained client {} for test {} at {}", clientId, testId, uri);
		clients.put(testId, new ClientHolder(clientConfig));
	}

	@Override
	public ClientHolder getSavedClient(String testId) {
		return Optional.ofNullable(clients.get(testId))
			.orElse(null);
	}

	@Override
	public Map<String, ClientHolder> clientsOlderThan(Instant instant) {
		Map<String, ClientHolder> orphanedClients = clients.entrySet()
			.stream()
			.filter(e -> e.getValue().getCreatedAt().isBefore(instant))
			.collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()));
		return orphanedClients;
	}

	@Override
	public void deleteSavedClient(String id) {
		ClientHolder removed = clients.remove(id);
		JsonObject client = removed.getDynamicClient();
		String clientId = OIDFJSON.getString(client.get("client_id"));
		String uri = OIDFJSON.getString(client.get("registration_client_uri"));
		LOG.info("Removing DCR-obtained client {} for test {} at {}", clientId, id, uri);
	}

	@Override
	public void setClientIrremovable(String id, boolean irremovable) {
		ClientHolder client = clients.get(id);
		if (client == null){
			LOG.info("Unable to find client with id: {}", id);
			return;
		}
		String clientId = OIDFJSON.getString(client.getDynamicClient().get("client_id"));
		String uri = OIDFJSON.getString(client.getDynamicClient().get("registration_client_uri"));
		LOG.info("Setting orphaned DCR client {} to irremovable for {}", clientId, uri);
		client.setIrremovable(irremovable);
	}

	@Override
	public boolean isClientIrremovable(String id) {
		return Optional.ofNullable(clients.get(id))
			.map(ClientHolder::isIrremovable)
			.orElse(null);
	}

	@Override
	public void incrementDeleteRetryCount(String testId) {
		ClientHolder client = clients.get(testId);
		if (client == null){
			LOG.info("Unable to find client with id: {}", testId);
			return;
		}
		int retryCount = client.getDeleteRetryCount();
		String uri = OIDFJSON.getString(client.getDynamicClient().get("registration_client_uri"));
		if (retryCount < 3){
			client.incrementRetryCount();
			LOG.info("Failed attempt(s) at deleting client, incrementing counter up to a max of 3. Current count: {}", client.getDeleteRetryCount());
		}
		retryCount = client.getDeleteRetryCount();
		if (retryCount == 3) {
			LOG.info("Failed deleting client 3 times, client will now be marked as irremovable and removed from the cache on the next run, Client was registered at {}", uri);
			client.setIrremovable(true);
		}
	}


}
