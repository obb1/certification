package net.openid.conformance.extensions.yacs.pcm;

public class TrackedConsent {

	private final String testId;
	private String consentId;
	private String accessToken;
	private String authCode;

	TrackedConsent(String testId) {
		this.testId = testId;
	}

	public String getConsentId() {
		return consentId;
	}

	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTestId() {
		return testId;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthCode() {
		return authCode;
	}
}
