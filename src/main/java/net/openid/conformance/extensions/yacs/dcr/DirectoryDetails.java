package net.openid.conformance.extensions.yacs.dcr;

import java.util.Map;
import java.util.Optional;

public class DirectoryDetails {

	private static final String SS_URI_TEMPLATE = "%s/organisations/%s/softwarestatements/%s/assertion";

	private static final String ORG_ID_PROP = "credentials.org-id";
	private static final String SS_ID_PROP = "credentials.software-statement-id";
	private static final String DIRECTORY_DISCOVERY_URI_PROP = "directory.discovery-uri";
	private static final String DIRECTORY_BASE_URI_PROP = "directory.base-uri";
	private static final String DIRECTORY_KEYSTORE_URI_PROP = "directory.keystore-base";

	private String directoryUri;
	private String discoveryUri;
	private String orgId;
	private String softwareStatementId;
	private String keystoreUri;

	public DirectoryDetails(Map<String, Object> properties) {
		this.directoryUri = String.valueOf(findIfPresent(DIRECTORY_BASE_URI_PROP, properties));
		this.discoveryUri = String.valueOf(findIfPresent(DIRECTORY_DISCOVERY_URI_PROP, properties));
		this.keystoreUri = String.valueOf(findIfPresent(DIRECTORY_KEYSTORE_URI_PROP, properties));
		this.orgId = String.valueOf(findIfPresent(ORG_ID_PROP, properties));
		this.softwareStatementId = String.valueOf(findIfPresent(SS_ID_PROP, properties));
	}

	public String getDiscoveryUri() {
		return discoveryUri;
	}

	public String getOrgId() {
		return orgId;
	}

	public String getSoftwareStatementId() {
		return softwareStatementId;
	}

	public String getDirectoryUri() {
		return directoryUri;
	}

	public String softwareStatementUri() {
		return String.format(SS_URI_TEMPLATE, directoryUri,
			orgId, softwareStatementId);
	}

	public String getKeystoreUri() {
		return keystoreUri;
	}

	private Object findIfPresent(String key, Map<String, Object> properties) {
		return Optional.ofNullable(properties.get(key)).orElseThrow(() -> new RuntimeException(String.format("Property %s not found", key)));
	}
}
