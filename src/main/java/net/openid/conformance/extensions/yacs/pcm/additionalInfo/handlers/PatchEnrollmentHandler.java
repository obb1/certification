package net.openid.conformance.extensions.yacs.pcm.additionalInfo.handlers;

import com.google.gson.JsonObject;
import net.openid.conformance.ui.ServerInfoTemplate;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.http.HttpMethod;

import java.net.URI;
import java.util.List;


public class PatchEnrollmentHandler extends BaseHandler {

	public PatchEnrollmentHandler(ServerInfoTemplate serverInfoTemplate) {
		super(serverInfoTemplate);
	}

	@Override
	public List<String> getDecoratedEndpoints() {
		return List.of(
			"/open-banking/enrollments/v1/enrollments/{enrollmentId}",
			"/open-banking/enrollments/v2/enrollments/{enrollmentId}"
		);
	}


	@Override
	public HttpMethod getMethod() {
		return HttpMethod.PATCH;
	}
	@Override
	public void handle(JsonObject additionalInfo, HttpRequestWrapper request) {
		HttpUriRequest actualRequest = (HttpUriRequest) request.getOriginal();
		URI uri = actualRequest.getURI();
		String url = uri.toString();

		addEnrollmentIdFromUri(additionalInfo,url);
		addReason(additionalInfo);
		addErrorCodes(additionalInfo);
		addPersonType(additionalInfo);
		addCancellationReasonFromRequestBody(additionalInfo,request);
		addCancelledFrom(additionalInfo);
		addClientIp(additionalInfo);
	}
}
