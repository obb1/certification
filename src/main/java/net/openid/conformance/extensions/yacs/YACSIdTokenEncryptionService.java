package net.openid.conformance.extensions.yacs;

import com.google.gson.JsonObject;

public interface YACSIdTokenEncryptionService {
	void addKeyToConfig(JsonObject config);
}
