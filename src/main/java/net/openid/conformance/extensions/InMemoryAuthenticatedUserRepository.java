package net.openid.conformance.extensions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryAuthenticatedUserRepository implements AuthenticatedUserRepository {
	private static final Logger LOG = LoggerFactory.getLogger(InMemoryAuthenticatedUserRepository.class);
	private Map<CachedAuthenticatedUser, AuthenticatedUserInfoHolder > authenticatedUsers = new HashMap<>();
	@Override
	public void incrementEndpointCalled(CachedAuthenticatedUser cachedAuthenticatedUser, EndpointLimits endpointLimits) {
		//check if user exists, if not insert new authenticated user,if they do increment endpoint called
		AuthenticatedUserInfoHolder authenticatedUserInfoHolder = authenticatedUsers.get(cachedAuthenticatedUser);
		if (authenticatedUserInfoHolder == null){
			LOG.info("Adding new authenticated user: {}", cachedAuthenticatedUser);
			authenticatedUserInfoHolder = new AuthenticatedUserInfoHolder();
			authenticatedUsers.put(cachedAuthenticatedUser, authenticatedUserInfoHolder);
		}
		LOG.info("Incrementing endpoint usage for authenticated user: {}", cachedAuthenticatedUser);
		authenticatedUserInfoHolder.incrementEndpointCalled(endpointLimits.name(), cachedAuthenticatedUser.getSub(), endpointLimits.getExecutionLimit(), endpointLimits.getExecutionIntervalLimitSeconds());
	}

	@Override
	public Map<String, Integer> getUserUsage(CachedAuthenticatedUser cachedAuthenticatedUser) {
		AuthenticatedUserInfoHolder authenticatedUserInfoHolder = authenticatedUsers.get(cachedAuthenticatedUser);
		if(authenticatedUserInfoHolder == null){
			return null;
		}else{
			Map<String, Integer> endpointUsage = new HashMap<>();
			for(Map.Entry<String, EndpointUsageHolder> entry : authenticatedUserInfoHolder.getEndpointUsage().entrySet()){
				endpointUsage.put(entry.getKey(), entry.getValue().getCount());
			}
			return endpointUsage;
		}
	}

	@Override
	public CachedAuthenticatedUser getAuthenticatedUser(CachedAuthenticatedUser cachedAuthenticatedUser) {
		return authenticatedUsers.get(cachedAuthenticatedUser) != null ? cachedAuthenticatedUser : null;
	}

	@Override
	public boolean getLimitReached(CachedAuthenticatedUser cachedAuthenticatedUser, EndpointLimits endpointLimits) {
		AuthenticatedUserInfoHolder authenticatedUserInfoHolder = authenticatedUsers.get(cachedAuthenticatedUser);
		if(authenticatedUserInfoHolder == null){
			return false;
		}else{
			EndpointUsageHolder endpointUsageHolder = authenticatedUserInfoHolder.getEndpointUsage().get(endpointLimits.name());
			if(endpointUsageHolder == null){
				return false;
			}else{
				return endpointUsageHolder.isLimitExceeded();
			}
		}
	}

	@Override
	public void removeAuthenticatedUserOlderThan(Instant instant) {
		Map<CachedAuthenticatedUser, AuthenticatedUserInfoHolder> usersOlderThan = getAuthenticatedUsersOlderThan(instant);
		for(Map.Entry<CachedAuthenticatedUser, AuthenticatedUserInfoHolder> entry : usersOlderThan.entrySet()){
			LOG.info("Removing authenticated user: {}", entry.getKey());
			authenticatedUsers.remove(entry.getKey());
		}
	}

	private Map<CachedAuthenticatedUser, AuthenticatedUserInfoHolder> getAuthenticatedUsersOlderThan(Instant instant) {
		return authenticatedUsers.entrySet()
			.stream()
			.filter(e -> e.getValue().getLastUpdatedAt().isBefore(instant))
			.collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()));
	}
	private static class AuthenticatedUserInfoHolder{
		private Instant createdAt = Instant.now();

		private Instant lastUpdatedAt;

		public Instant getLastUpdatedAt() {
			return lastUpdatedAt;
		}

		private Map<String, EndpointUsageHolder> endpointUsage = new HashMap<>();

		public void incrementEndpointCalled(String endpointRequesting, String email, int executionLimit, int executionIntervalLimitInSeconds){
			lastUpdatedAt = Instant.now();
			if(endpointUsage != null && endpointUsage.containsKey(endpointRequesting)){
				LOG.info("Incrementing endpoint usage for endpoint: {}", endpointRequesting);
				endpointUsage.get(endpointRequesting).incrementCount(email, executionLimit, executionIntervalLimitInSeconds);
			}else{
				LOG.info("Adding new endpoint usage for endpoint: {}", endpointRequesting);
				EndpointUsageHolder endpointUsageHolder = new EndpointUsageHolder();
				endpointUsageHolder.incrementCount(email, executionLimit, executionIntervalLimitInSeconds);
				endpointUsage.put(endpointRequesting, endpointUsageHolder);
			}
		}

		public Map<String, EndpointUsageHolder> getEndpointUsage() {
			return endpointUsage;
		}
		public Instant getCreatedAt() {
			return createdAt;
		}
	}

	private static class EndpointUsageHolder{
		private Instant createdAt = Instant.now();

		private Instant lastUpdatedAt = Instant.now();
		private int count = 0;
		private boolean limitExceeded = false;

		public void incrementCount(String email, int executionLimit, int executionIntervalLimitInSeconds){
			count++;
			lastUpdatedAt = Instant.now();
			//if user has exceeded the limits set and log out the users email if they have
			//check if they have requested the endpoint X amount of times in the last X seconds using the executionLimit and executionIntervalLimitInSeconds
			Duration duration = Duration.between(createdAt, lastUpdatedAt);
			if(count >= executionLimit && duration.getSeconds() < executionIntervalLimitInSeconds){
				limitExceeded = true;
				LOG.info("User {} has exceeded the limit of {} requests in {} seconds, request count {}", email, executionLimit, executionIntervalLimitInSeconds, count);
			} else if (duration.getSeconds() > executionIntervalLimitInSeconds){
				//reset the count and createdAt as the last request was more than X seconds ago
				limitExceeded = false;
				count = 1;
				createdAt = Instant.now();
				LOG.info("Resetting count as the last request was more than {} seconds ago", executionIntervalLimitInSeconds);
			}
		}

		public boolean isLimitExceeded() {
			return limitExceeded;
		}
		public Instant getLastUpdatedAt() {
			return lastUpdatedAt;
		}
		public Instant getCreatedAt() {
			return createdAt;
		}

		public int getCount() {
			return count;
		}
	}
}
