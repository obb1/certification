package net.openid.conformance.extensions;

import com.google.gson.JsonObject;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.gson.JsonPrimitive;
import net.openid.conformance.testmodule.OIDFJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InMemoryTestModuleRepository implements TestModuleRepository {
	private Map<String, TestInfoHolder> testModules = new HashMap<>();

	private static final Logger LOG = LoggerFactory.getLogger(InMemoryTestModuleRepository.class);

	@Override
	public void saveTestModuleDetails(String testId, JsonObject testModuleInfo) {
		LOG.info("Adding test module info for test: {}", testId);
		testModules.put(testId, new TestInfoHolder(testModuleInfo));
	}

	@Override
	public JsonObject getCachedObjectForTest(String testId) {
		LOG.info("Getting JsonObject for test: {}", testId);
		return Optional.ofNullable(testModules.get(testId))
			.map(TestInfoHolder::getTestModuleInfo)
			.orElse(null);
	}

	@Override
	public Map<String, JsonObject> getTestModulesOlderThan(Instant instant) {
		Map<String, JsonObject> testModuleObjects = testModules.entrySet()
			.stream()
			.filter(e -> e.getValue().createdAt.isBefore(instant))
			.collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue().getTestModuleInfo()));
		return testModuleObjects;
	}

	@Override
	public void removeOlderThan(Instant instant) {
		Map<String, JsonObject> testModulesOlderThan = getTestModulesOlderThan(instant);
		for (Map.Entry<String, JsonObject> entry : testModulesOlderThan.entrySet()) {
			LOG.info("Clearing cache for test: {}", entry.getKey());
			testModules.remove(entry.getKey());
		}
	}

	@Override
	public String getTestIdFromJsonPrimitiveValue(String value, String jsonKey) {
		LOG.info("Looking for testId using value: {} and jsonKey: {}", value, jsonKey);
		String testId = null;
		for (Map.Entry<String, TestInfoHolder> entry : testModules.entrySet()) {
			if (entry.getValue().getTestModuleInfo().keySet().contains(jsonKey) && JsonPrimitive.class.isAssignableFrom(entry.getValue().getTestModuleInfo().get(jsonKey).getClass()) && OIDFJSON.getString(entry.getValue().getTestModuleInfo().get(jsonKey)).equals(value)) {
				LOG.info("Found testId {} for value: {} and jsonKey: {}", entry.getKey(), value, jsonKey);
				testId = entry.getKey();
			}
		}
		return testId;
	}


	private static class TestInfoHolder {

		private Instant createdAt = Instant.now();
		private JsonObject testModuleInfo;


		public TestInfoHolder(JsonObject testModuleInfo) {
			this.testModuleInfo = testModuleInfo;
		}

		public Instant getCreatedAt() {
			return createdAt;
		}

		public JsonObject getTestModuleInfo() {
			return testModuleInfo;
		}

	}
}
