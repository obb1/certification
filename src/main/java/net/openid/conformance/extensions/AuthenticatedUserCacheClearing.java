package net.openid.conformance.extensions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalAmount;

public class AuthenticatedUserCacheClearing {

	private static final TemporalAmount THIRTY_MINUTES = Duration.ofMinutes(30);
	private static final Logger LOG = LoggerFactory.getLogger(AuthenticatedUserCacheClearing.class);

	private final AuthenticatedUserRepository authenticatedUserRepository;


	public AuthenticatedUserCacheClearing(AuthenticatedUserRepository authenticatedUserRepository) {
		this.authenticatedUserRepository = authenticatedUserRepository;
	}

	@Scheduled(fixedRateString = "PT30M")
	public void clearAuthenticatedUserCache(){
		//removing users who we have not recorded any activity for in the last 30 minutes
		LOG.info("Clearing authenticated user cache");
		authenticatedUserRepository.removeAuthenticatedUserOlderThan(Instant.now().minus(THIRTY_MINUTES));
		LOG.info("Cleared authenticated user cache");
	}
}
