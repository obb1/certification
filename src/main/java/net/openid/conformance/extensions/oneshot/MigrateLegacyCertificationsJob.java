package net.openid.conformance.extensions.oneshot;

import net.openid.conformance.extensions.CertificationOffloadHelper;
import net.openid.conformance.info.Plan;
import net.openid.conformance.token.DBTokenService;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.Base64Utils;

import java.security.SecureRandom;
import java.util.List;
import java.util.Map;

public class MigrateLegacyCertificationsJob implements OneShotJob {

	private static final Logger LOG = LoggerFactory.getLogger(MigrateLegacyCertificationsJob.class);

	private final CertificationOffloadHelper helper;
	private MongoTemplate mongoTemplate;
	private int batchSize = 100;

	public MigrateLegacyCertificationsJob(MongoTemplate mongoTemplate,
										  CertificationOffloadHelper helper) {
		this.mongoTemplate = mongoTemplate;
		this.helper = helper;
	}

	@Override
	public void execute() {
		Query where = new Query();
		where.addCriteria(Criteria.where("publish").in("everything", "summary"));
		where.addCriteria(Criteria.where("immutable").is(true));
		where.addCriteria(Criteria.where("offloaded").exists(false));
		where.limit(batchSize);
		where.limit(batchSize);
		boolean finished = false;
		while(!finished) {
			List<Plan> plans = mongoTemplate.find(where, Plan.class);
			LOG.info("Found " + plans.size() + " plans to offload");
			finished = plans.isEmpty();
			for(Plan plan : plans) {
				Map<String, String> owner = plan.getOwner();
				boolean success = helper.initiateWithToken(plan.getId(), createToken(owner));
				if(success) {
					Update update = new Update();
					update.set("offloaded", true);
					mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(plan.getId())), update, Plan.class);
					LOG.info("Offloaded {}", plan.getId());
				} else {
					LOG.info("Failed to offload {}", plan.getId());
				}
				try {
					Thread.sleep(30000L);
				} catch (InterruptedException e) {
					// whatever
				}
			}
		}
		LOG.info("All published plans offloaded");
	}

	/**
	 * Create a token for the given owner
	 *
	 * Because we are sending this to the certification offload process, it needs a token which belongs to the owner
	 * of the plan being offloaded. As the request will be handled in a different thread, we cannot simply inject an
	 * admin user into the authentication facade
	 *
	 * @param owner
	 * @return
	 */
	private Document createToken(Map<String, String> owner) {
		String sub = owner.get("sub");
		String iss = owner.get("iss");
		String id = RandomStringUtils.randomAlphanumeric(13);

		byte[] tokenBytes = new byte[64];
		new SecureRandom().nextBytes(tokenBytes);

		Document token = new Document()
			.append("_id", id)
			.append("owner", Map.of("sub", sub, "iss",iss))
			.append("info", Map.of("sub", sub, "iss",iss))
			.append("token", Base64Utils.encodeToString(tokenBytes))
			.append("expires", System.currentTimeMillis() + (60 * 60 * 1000));

		mongoTemplate.insert(token, DBTokenService.COLLECTION);
		return token;
	}

}
