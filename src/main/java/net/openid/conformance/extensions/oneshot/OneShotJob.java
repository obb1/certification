package net.openid.conformance.extensions.oneshot;

public interface OneShotJob {

	void execute();

}
