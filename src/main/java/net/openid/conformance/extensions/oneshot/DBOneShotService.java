package net.openid.conformance.extensions.oneshot;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class DBOneShotService implements OneShotService {

	public static final String COLLECTION = "ONESHOT_JOBS";

	private MongoTemplate mongoTemplate;

	public DBOneShotService(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public void saveExecution(String id) {
		OneShotInfo info = new OneShotInfo(id);
		info.setExecuted(true);
		mongoTemplate.save(info);
	}

	@Override
	public boolean hasExecuted(String id) {
		OneShotInfo info = mongoTemplate.findById(id, OneShotInfo.class);
		return info != null && info.isExecuted();
	}

}
