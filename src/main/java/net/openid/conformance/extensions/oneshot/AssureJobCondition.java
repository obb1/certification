package net.openid.conformance.extensions.oneshot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Optional;

public class AssureJobCondition implements Condition {

	private static Logger LOG = LoggerFactory.getLogger(AssureJobCondition.class);

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		boolean annotated = metadata.isAnnotated(AssureJobRun.class.getName());
		if(!annotated) {
			return false;
		}
		MergedAnnotation<AssureJobRun> annotation = metadata.getAnnotations().get(AssureJobRun.class);
		Optional<String> brandRequested = Optional.of(annotation.getString("brand")).filter(s -> !s.isEmpty());
		Optional<String> baseUrlRequested = Optional.of(annotation.getString("conformanceUrl")).filter(s -> !s.isEmpty());
		Optional<String> jobNameRequested = Optional.of(annotation.getString("jobName")).filter(s -> !s.isEmpty());
		Environment env = context.getEnvironment();
		boolean allowed = false;
		allowed = checkBrand(env, brandRequested);
		if(!allowed) {
			return false;
		}
		allowed = checkConformanceUrl(env, baseUrlRequested);
		if(!allowed) {
			return false;
		}
		allowed = checkJobName(env, jobNameRequested);
		if(allowed) {
			LOG.info("Job {} is allowed to run", jobNameRequested.get());
		}
		return allowed;
	}

	private boolean checkJobName(Environment env, Optional<String> jobNameRequested) {
		if(!jobNameRequested.isPresent()) {
			return true;
		}
		String oneshotJobsConfigured = env.getProperty("FINTECHLABS_EXTENSIONS_ONESHOT_JOBS");
		if(oneshotJobsConfigured == null) {
			return false;
		}
		String[] jobs = oneshotJobsConfigured.split(",");
		for(String job : jobs) {
			if(job.equals(jobNameRequested.get())) {
				return true;
			}
		}
		return false;
	}

	private boolean checkConformanceUrl(Environment env, Optional<String> baseUrlRequested) {
		if(!baseUrlRequested.isPresent()) {
			return true;
		}
		String baseUrlConfigured = env.getProperty("fintechlabs.base_url");
		return baseUrlRequested.get().equals(baseUrlConfigured);
	}

	private boolean checkBrand(Environment env, Optional<String> brandRequested) {
		if(!brandRequested.isPresent()) {
			return true;
		}
		String brandConfigured = env.getProperty("fintechlabs.brand");
		return brandRequested.get().equals(brandConfigured);
	}
}
