package net.openid.conformance.extensions;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CertificationOffloadFilter extends OncePerRequestFilter {

	private static final AntPathRequestMatcher matcher = new AntPathRequestMatcher("/api/plan/{plan}/certificationpackage");
	private final CertificationOffloadHelper helper;

	public CertificationOffloadFilter(CertificationOffloadHelper helper) {
		this.helper = helper;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		RequestMatcher.MatchResult result = matcher.matcher(request);
		String planId = result.getVariables().get("plan");
		filterChain.doFilter(request, response);
		helper.initiateForAuthenticatedUser(planId);
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		boolean matches = matcher.matches(request);
		return !matches;
	}
}
