package net.openid.conformance.extensions;

import net.openid.conformance.token.TokenService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ExtensionConfig {

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.extensions.offload.certifications", havingValue = "true")
	public CertificationOffloadFilter certificationArchivingAdvice(
		CertificationOffloadHelper helper) {
		return new CertificationOffloadFilter(helper);
	}

	@Bean
	@ConditionalOnProperty(name = "fintechlabs.extensions.offload.certifications", havingValue = "true")
	public CertificationOffloadHelper offloadHelper(TokenService tokenService,
													@Value("${certification_offload_uri}") String uri,
													@Value("${certification_offload_credentials:null}") String credentials) {
		return new CertificationOffloadHelper(new RestTemplate(), uri, credentials, tokenService);
	}


}
