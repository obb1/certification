package net.openid.conformance.extensions;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import net.openid.conformance.util.JsonObjectBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/health")
public class HealthCheckApi {

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(
		@ApiResponse(responseCode = "200", description = "Service is alive")
	)
	@Operation(summary = "Health check for the service")
	public ResponseEntity<Object> getHealthCheck(){
		JsonObjectBuilder respone = new JsonObjectBuilder().addField("status", "UP");
		return new ResponseEntity<>(respone.build(), HttpStatus.OK);
	}
}

