package net.openid.conformance.condition.client;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

public class EnsureHttpStatusCodeIs4xxOrFailedTLS extends AbstractCondition {

	public static final String RESPONSE_SSL_ERROR_KEY = "dynamic_registration_endpoint_response_ssl_error";

	@Override
	public Environment evaluate(Environment env) {
		Integer httpStatus = env.getInteger("endpoint_response", "status");
		String endpointName = env.getString("endpoint_response", "endpoint_name");
		Boolean ssl_error = env.getBoolean(RESPONSE_SSL_ERROR_KEY);

		if (ssl_error == null) {
			throw error("Missing environment variable: " + RESPONSE_SSL_ERROR_KEY);
		}

		if (ssl_error) {
			logSuccess("TSL has failed.");
			return env;
		}

		if (httpStatus == null) {
			throw error("Http status can not be null.");
		}

		if (endpointName == null) {
			throw error("endpoint_name cannot be null.");
		}

		if (httpStatus >= 400 && httpStatus <= 499) {
			logSuccess(endpointName + " endpoint http status code was " + httpStatus);
			return env;
		}
		if (httpStatus == 201){
			JsonObject client = (JsonObject) env.getElementFromObject("dynamic_registration_endpoint_response", "body_json");
			if (client == null) {
				throw error("No json response from dynamic registration endpoint");
			}

			env.putObject("client", client.deepCopy());

			JsonElement clientId = client.get("client_id");
			if (clientId == null) {
				throw error("no client id in dynamic registration response");
			}

			logSuccess("Extracted client from dynamic registration response",
				args("client_id", clientId));
		}
		throw error(endpointName + "endpoint returned a different http status than expected", args("actual", httpStatus, "expected", "400 to 499"));
	}
}
