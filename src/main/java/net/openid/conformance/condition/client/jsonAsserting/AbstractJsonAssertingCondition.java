package net.openid.conformance.condition.client.jsonAsserting;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;
import net.openid.conformance.util.field.BooleanField;
import net.openid.conformance.util.field.DatetimeField;
import net.openid.conformance.util.field.DoubleField;
import net.openid.conformance.util.field.ExtraField;
import net.openid.conformance.util.field.Field;
import net.openid.conformance.util.field.IntArrayField;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.LatitudeField;
import net.openid.conformance.util.field.LongitudeField;
import net.openid.conformance.util.field.NumberArrayField;
import net.openid.conformance.util.field.NumberField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class AbstractJsonAssertingCondition extends AbstractJsonAssertingUtils {

	public static final String ROOT_PATH = "$.data";
	private int errorCount;
	private boolean dontStopOnFailure;

	private Map<String, JsonElement> responseElements;

	@Override
	public abstract Environment evaluate(Environment environment);


	protected JsonElement bodyFrom(Environment environment, String envKey) {
		initAdditionalProperties(environment);
		try {
			return BodyExtractor.bodyFrom(environment, envKey)
				.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

	private void initAdditionalProperties(Environment env) {
		String statusString = env.getEffectiveKey("doNotStopOnFailure");
		if (statusString != null) {
			this.dontStopOnFailure = Boolean.parseBoolean(statusString);
		}
		String logOnlyFailureProp = env.getEffectiveKey("logOnlyFailure");
		if (statusString != null) {
			this.logOnlyFailure = Boolean.parseBoolean(logOnlyFailureProp);
		}
	}

	public void parseResponseBody(JsonObject body, String bodyName) {
		this.responseElements = new JsonAssertingResponseBodyParser().parseBody(bodyName, body);
		this.responseElements.remove(bodyName);
	}

	public void assertExtraFields(ExtraField field) {
		if (responseElements == null) {
			throw error("Attempting assert extra fields without parsing the response before");
		}
		if(!responseElements.isEmpty() && field.isMustNotBePresent()) {
			throw error(ErrorMessagesUtils.createAdditionalFieldsMustNotBePresentMessage(responseElements.keySet().toString(), getApiName()));
		}
		this.responseElements.forEach((fullPath, element) -> {
			String[] pathEntries = fullPath.split("\\.");
			String keyName = pathEntries[pathEntries.length - 1];
			assertExtraField(keyName, field);
		});
	}

	public void assertField(JsonElement jsonObject, Field field) {
		if (this.dontStopOnFailure) {
			try {
				assertElement(jsonObject, field);
			} catch (ConditionError ignored) {
				errorCount++;
			}
		} else {
			assertElement(jsonObject, field);
		}
	}

	private void assertElement(JsonElement jsonObject, Field field) {
		currentField = field.getPath();
		String fullPath = getPath();

		if (responseElements != null) {
			responseElements.remove(fullPath);
		}

		if (!ifExists(jsonObject, field.getPath())) {
			if (field.isOptional() || field.isMustNotBePresent()) {
				return;
			} else {
				throw error(ErrorMessagesUtils.createElementNotFoundMessage(field.getPath(), getApiName()),
					args("currentElement", jsonObject));
			}
		} else if (field.isMustNotBePresent()) {
			throw error(ErrorMessagesUtils.createMustNotBePresentMessage(field.getPath(), getApiName()),
				args("currentElement", jsonObject));
		}

		JsonElement elementByPath = findByPath(jsonObject, field.getPath());
		if (field.isNullable() && elementByPath.isJsonNull()) {
			return;
		}

		if (elementByPath.isJsonNull()) {
			throw error(ErrorMessagesUtils.createElementCantBeNullMessage(field.getPath(), getApiName()),
				args("path", getPath(), "currentElement", elementByPath));
		}

		if (field instanceof ObjectField) {
			assertObjectField(elementByPath, jsonObject, field);
		} else if (field instanceof ObjectArrayField) {
			assertArrayField(elementByPath, field);
		} else if (field instanceof StringField || field instanceof DatetimeField) {
			assertHasStringField(jsonObject, field.getPath());
			String value = getJsonValueAsString(elementByPath, field.getPath());
			assertPatternAndMaxMinLength(value, field);
			if (field instanceof DatetimeField) {
				assertPatternAndTimeRange(value, (DatetimeField) field, jsonObject);
			}
		} else if (field instanceof IntField) {
			assertHasIntField(jsonObject, field.getPath());
			String value = getJsonValueAsString(elementByPath, field.getPath());
			assertMinAndMaxValue(value, field);
			assertPatternAndMaxMinLength(value, field);
		} else if (field instanceof BooleanField) {
			assertHasBooleanField(jsonObject, field.getPath());
		} else if (field instanceof LatitudeField) {
			assertHasStringField(jsonObject, field.getPath());
			assertLatitude(elementByPath, field);
			String value = getJsonValueAsString(elementByPath, field.getPath());
			assertPatternAndMaxMinLength(value, field);
		} else if (field instanceof LongitudeField) {
			assertHasStringField(jsonObject, field.getPath());
			assertLongitude(elementByPath, field);
			String value = getJsonValueAsString(elementByPath, field.getPath());
			assertPatternAndMaxMinLength(value, field);
		} else if (field instanceof DoubleField) {
			assertHasDoubleField(jsonObject, field.getPath());
			String value = getDoubleValueAsString(jsonObject, field.getPath());
			assertMinAndMaxValue(value, field);
			assertPatternAndMaxMinLength(value, field);
		} else if (field instanceof StringArrayField) {
			assertHasStringArrayField(jsonObject, field.getPath());
			OIDFJSON.getStringArray(elementByPath).forEach(v ->
				assertPatternAndMaxMinLength(v, field));
			assertMinAndMaxItems(elementByPath.getAsJsonArray(), field);
		} else if (field instanceof IntArrayField) {
			assertHasIntArrayField(jsonObject, field.getPath());
			OIDFJSON.getNumberArray(elementByPath).forEach(v -> {
				assertMinAndMaxValue(v.toString(), field);
				assertPatternAndMaxMinLength(v.toString(), field);
			});
			assertMinAndMaxItems(elementByPath.getAsJsonArray(), field);
		} else if (field instanceof NumberField) {
			assertHasNumberField(jsonObject, field.getPath());
			String value = getJsonValueAsString(elementByPath, field.getPath());
			assertMinAndMaxValue(value, field);
			assertPatternAndMaxMinLength(value, field);
		} else if (field instanceof NumberArrayField) {
			assertHasNumberArrayField(jsonObject, field.getPath());
			OIDFJSON.getNumberArray(elementByPath).forEach(v -> {
				assertMinAndMaxValue(v.toString(), field);
				assertPatternAndMaxMinLength(v.toString(), field);
			});
			assertMinAndMaxItems(elementByPath.getAsJsonArray(), field);
		}
	}

	private void assertObjectField(JsonElement elementByPath, JsonElement baseObj, Field field) {
		if (!elementByPath.isJsonObject()) {
			throw error(ErrorMessagesUtils.createObjectClassCastExpMessage(field.getPath(), getApiName()),
				args("path",
					getPath(), "jsonElement", elementByPath));
		}
		parentPath += field.getPath() + ".";
		if (field.getValidator() == null) {
			logInfo(String.format("Field: '%s'. ObjectField. Validator property is empty and inner fields will not be validated", field.getPath()),
				args("path", field.getPath(), "jsonElement", elementByPath));
			parentPath = parentPath.replaceFirst(field.getPath() + "\\.", "");
			return;
		}

		assertJsonObject(baseObj, field, field.getValidator());
		parentPath = parentPath.replaceFirst(field.getPath() + "\\.", "");
	}

	private void assertJsonObject(JsonElement body, Field field, Consumer<JsonObject> consumer) {
		JsonObject object = (JsonObject) findByPath(body, field.getPath());
		if (field.getMinProperties() != 0 && object.size() < field.getMinProperties()) {
			throw error(ErrorMessagesUtils.createObjectLessRequiredMinProperties(field.getPath(), getApiName()),
				args("requiredValue", field.getMinProperties(), "currentValue", object.getAsJsonObject().size()));
		}
		consumer.accept(object.getAsJsonObject());
	}

	private void assertArrayField(JsonElement elementByPath, Field field) {
		ObjectArrayField objectArrayField = (ObjectArrayField) field;
		if (!elementByPath.isJsonArray()) {
			throw error(ErrorMessagesUtils.createArrayClassCastExpMessage(objectArrayField.getPath(), getApiName()));
		}
		JsonArray array = elementByPath.getAsJsonArray();
		if (objectArrayField.mustNotBeEmpty() & array.size() == 0) {
			throw error(ErrorMessagesUtils.createArrayMustNotBeEmptyMessage(objectArrayField.getPath(), getApiName()));
		}
		parentPath += (parentPath.contains(field.getPath())) ? "" : field.getPath() + ".";
		assertMinAndMaxItems(array, objectArrayField);
		if (field.getValidator() == null) {
			logInfo(String.format("Field: '%s'. ObjectArrayField. Validator property is empty and inner fields will not be validated", field.getPath()),
				args("path", field.getPath(), "jsonElement", elementByPath));
			parentPath = parentPath.replaceFirst(field.getPath() + "\\.", "");
			return;
		}
		array.forEach(object -> {
			if (field.getMinProperties() != 0 && object.isJsonObject()
				&& object.getAsJsonObject().size() < field.getMinProperties()) {
				throw error(ErrorMessagesUtils.createObjectLessRequiredMinProperties(field.getPath(), getApiName()),
					args("requiredValue", field.getMinProperties(), "currentValue", object.getAsJsonObject().size()));
			}
		});
		Stream<JsonElement> stream = StreamSupport.stream(array.spliterator(), false);
		stream.limit(objectArrayField.getInnerValidationLimit().orElse(array.size()))
			.forEach(json -> field.getValidator().accept(json.getAsJsonObject()));
		parentPath = parentPath.replaceFirst(field.getPath() + "\\.", "");
	}

	public void assertGeographicCoordinates(JsonObject body) {
		assertField(body,
			new ObjectField
				.Builder("geographicCoordinates")
				.setOptional()
				.setValidator(geo -> {
					assertField(geo,
						new LatitudeField.Builder()
							.setOptional()
							.build());
					assertField(geo,
						new LongitudeField.Builder()
							.setOptional()
							.build());
				})
				.build());
	}

	protected void logFinalStatus() {
		logSuccess(ErrorMessagesUtils.createTotalElementsFoundMessage(totalElements, getApiName()));
		if (errorCount > 0) {
			throw error(ErrorMessagesUtils.createTotalErrorsFoundMessage(errorCount, getApiName()));
		}
	}

	private void logInfo(String msg, Map<String, Object> map) {
		Map<String, Object> copy = new HashMap<>(map); // don't modify the underlying map
		copy.put("msg", msg);
		copy.put("result", ConditionResult.INFO);
		if (!getRequirements().isEmpty()) {
			copy.put("requirements", getRequirements());
		}
		log(copy);
	}

	private boolean ifExists(JsonElement jsonObject, String path) {
		try {
			JsonPath.read(jsonObject, path);
			return true;
		} catch (PathNotFoundException e) {
			return false;
		}
	}
}
